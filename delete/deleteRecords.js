$(document).ready(function(){
$('.delete_employee').click(function(e){
e.preventDefault();
var empid = $(this).attr('data-emp-id');
var parent = $(this).parent("td").parent("tr");
bootbox.dialog({
message: "Are you sure you want to Delete ?",
title: "<i class='fa fa-trash'></i> Delete !",
buttons: {
success: {
label: "No",
className: "btn-success",
callback: function() {
$('.bootbox').modal('hide');
}
},
danger: {
label: "Yes!",
className: "btn-danger",
callback: function() {
	var empid1 = empid;
	var myarr = empid1.split("/");
	var empid2 = myarr[0] + "/" + myarr[1] + "/" + myarr[2] + "/" + myarr[3] + "/" + myarr[4] + "/" + myarr[5];
	var myvar = myarr[6];
$.ajax({
type: 'POST',
url: empid2,
data: 'empid='+myvar
})
.done(function(response){
// bootbox.alert(response);
parent.fadeOut('slow');
})
// .fail(function(){
// bootbox.alert('Error....');
// })
}
}
}
});
});
});