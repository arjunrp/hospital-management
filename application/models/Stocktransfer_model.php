<?php

class Stocktransfer_model extends CI_Model {

	function __construct() {
		parent::__construct();

		$this->load->database();
	}
// <a class='".$status." btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."stocktransfer/delete/".$row->ve_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a>
	function list_all()
	{
		$output 		=	"";
		$status 	 	=	"1";
		$this->db->select('voucher_entry.*,dp1.dp_department as dp_1,dp2.dp_department as dp_2');
    	$this->db->from('voucher_entry');
    	$this->db->join('department dp1','dp1.dp_id = voucher_entry.ve_supplier', 'left');
    	$this->db->join('department dp2','dp2.dp_id = voucher_entry.ve_customer', 'left');
    	$this->db->order_by('voucher_entry.ve_vno','desc');
    	$this->db->where('voucher_entry.ve_type','st');
    	$query=$this->db->get();
		foreach($query->result() as $row)
		{

			$output 	.=	"<tr><td>".$row->ve_vno."</td>";
			$output 	.=	"<td>".date("d-m-Y",strtotime($row->ve_date))."</td>";
			$output 	.=	"<td>".$row->dp_1."</td>";
			$output 	.=	"<td>".$row->dp_2."</td>";
			 

			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."stocktransfer/stransfer_View/".$row->ve_id."'class='btn btn-primary view-btn-edit' title='View'><i class='fa fa-eye'></i></a> </td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	}


	public function set_purchase_stock($stockpid,$purchase_unit_qty,$ve_customer,$ved_batch,$ve_supplier,$ve_date){
		
		$this->db->select('stock_id,stock_qty,stock_date,created_at');
		$this->db->from('stock');
		$this->db->where('stock_product_id',$stockpid);
		$this->db->where('stock_dept',$ve_supplier);
		$this->db->where('stock_batch',$ved_batch);
		$this->db->where('stock_status','standby');
		$this->db->order_by("stock_id", "desc");
		$this->db->limit(1);
		$result  		=   $this->db->get();
		$number_of_rows = 	$result->num_rows();

		if($number_of_rows != 0){
			$sid						=	$result->row()->stock_id;
			$sqty						=	$result->row()->stock_qty;
			$sdate 						=	$result->row()->stock_date;
			$data['stock_last'] 		=	$sqty;
			$data['stock_moved'] 		=	$purchase_unit_qty;
			$data['stock_qty'] 			=	$sqty - $purchase_unit_qty;
			$data['stock_date']			=	$ve_date;	
			$data['stock_product_id']	=	$stockpid;
			$data['stock_batch']		=	$ved_batch;
			$data['stock_dept']			=	$ve_supplier;
			$data['stock_status']		=	"standby";	
			$data['created_at']			=	$result->row()->created_at;
			if($sdate == $ve_date)
			{
				$this->db->update('stock',$data, array('stock_id' => $sid));
			}
			else
			{
				$this->db->insert('stock',$data);	
				$data1['stock_status']		=	"transfer";	
				$this->db->update('stock',$data1, array('stock_id' => $sid));
			}
			$parent_id = $sid;
		}



		$this->db->select('stock_id,stock_qty,stock_date,created_at');
		$this->db->from('stock');
		$this->db->where('stock_product_id',$stockpid);
		$this->db->where('stock_dept',$ve_customer);
		$this->db->where('stock_batch',$ved_batch);
		$this->db->order_by("stock_id", "desc");
		$this->db->where('stock_status','standby');
		$this->db->limit(1);
		$results  	 =  $this->db->get();
		$number_rows =  $results->num_rows();

		if($number_rows != 0){
			$sid						=	$results->row()->stock_id;
			$sqty						=	$results->row()->stock_qty;
			$sdate 						=	$results->row()->stock_date;
			$datas['stock_parent']		=	$parent_id;
			$datas['stock_qty'] 		=	$sqty + $purchase_unit_qty;
			$datas['stock_date']		=	$ve_date;	
			$datas['stock_product_id']	=	$stockpid;
			$datas['stock_batch']		=	$ved_batch;
			$datas['stock_dept']		=	$ve_customer;
			$datas['stock_status']		=	"standby";	
			$datas['created_at']		=	$results->row()->created_at;
			if($sdate == $ve_date)
			{
				$this->db->update('stock',$datas, array('stock_id' => $sid));
			}
			else
			{
				$this->db->insert('stock',$datas);	
				$datas1['stock_status']		=	"transfer";	
				$this->db->update('stock',$datas1, array('stock_id' => $sid));
			}
		}
		else if($number_rows == 0)
		{
			$datas['stock_qty '] 		=	0	+	$purchase_unit_qty;
			$datas['stock_date ']		=	$ve_date;	
			$datas['stock_product_id']	=	$stockpid;
			$datas['stock_batch']		=	$ved_batch;
			$datas['stock_dept']		=	$ve_customer;
			$datas['stock_status']		=	"standby";
			$datas['created_at']		=	date('Y-m-d H:i:s.') . gettimeofday()['usec'];	
			$this->db->insert('stock',$datas);
		}


	}

	public function get_voucher_details($ve_id){

	  $this->db->select('voucher_entry_detail.*,voucher_entry.ve_supplier,voucher_entry.ve_customer');
      $this->db->from('voucher_entry_detail');
      $this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid', 'left');
      $this->db->where('voucher_entry_detail.ved_veid',$ve_id);
      $this->db->order_by('voucher_entry_detail.ved_id','asc');
      $query=$this->db->get();
      return $query->result_array();
	}

	public function delete_stock($stockpid,$purchase_unit_qty,$ve_customer,$ved_batch,$ve_supplier,$ve_date){
		
		$this->db->select('*');
		$this->db->from('stock');
		$this->db->where('stock_product_id',$stockpid);
		$this->db->where('stock_dept',$ve_supplier);
		$this->db->where('stock_batch',$ved_batch);
		$this->db->where('stock_date >=',$ve_date);
		$this->db->order_by("stock_id", "asc");
		$query  		=    $this->db->get();
		$result 		=    $query->result_array();
		foreach ($result as $value) {
		$sid						=	$value['stock_id'];
		$sqty						=	$value['stock_qty'];
		$data['stock_qty'] 			=	$sqty + $purchase_unit_qty;
		$this->db->update('stock',$data, array('stock_id' => $sid));
		}

		$this->db->select('*');
		$this->db->from('stock');
		$this->db->where('stock_product_id',$stockpid);
		$this->db->where('stock_dept',$ve_customer);
		$this->db->where('stock_batch',$ved_batch);
		$this->db->where('stock_date >=',$ve_date);
		$this->db->order_by("stock_id", "asc");
		$query1  		=    $this->db->get();
		$results 		=    $query1->result_array();
		foreach ($results as $values) {
		$sid						=	$values['stock_id'];
		$sqty						=	$values['stock_qty'];
		$datas['stock_qty'] 		=	$sqty - $purchase_unit_qty;
		$this->db->update('stock',$datas, array('stock_id' => $sid));
		}

	}


	public function getPurchaseId()
     {
     	$this->db->select('CAST(ve_vno as SIGNED INTEGER) as ve_vno');
		$this->db->from('voucher_entry');
		$this->db->order_by('ve_vno','desc');
		$this->db->where('ve_type','st');
		$this->db->limit(1);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return '1';
		}
		else
		{
			$result 	= trim($query->row()->ve_vno) + 1;
			// $result++;
			return $result;
		}

	}



	public function getProduct(){
		// $this->db->select('voucher_entry_detail.ved_itemid,voucher_entry_detail.ved_item,voucher_entry_detail.ved_batch,voucher_entry_detail.ved_expiry,voucher_entry_detail.ved_unit,voucher_entry_detail.ved_uqty,stock.stock_qty');
		// $this->db->from('stock');
		// $this->db->join('voucher_entry_detail','voucher_entry_detail.ved_itemid = stock.stock_product_id AND voucher_entry_detail.ved_batch = stock.stock_batch', 'inner');
		// $this->db->group_by('stock.stock_product_id');
		// $this->db->group_by('stock.stock_batch');
		// // $this->db->order_by('stock.stock_id','desc');
		// $this->db->order_by('stock.created_at','asc');
		// $this->db->where('stock.stock_qty > ','0');
		// $this->db->where('stock.stock_dept','15');

		$this->db->select('e1.ved_itemid,e1.ved_item,e1.ved_batch,e1.ved_expiry,e1.ved_unit,e1.ved_uqty,c.stock_qty');
		$this->db->from('stock c');
		$this->db->join('voucher_entry_detail e1','e1.ved_itemid = c.stock_product_id ', 'left');
		$this->db->join('stock e2','c.stock_product_id = e2.stock_product_id AND e2.stock_id > c.stock_id', 'left');
		// $this->db->group_by('c.stock_product_id');
		// $this->db->group_by('c.stock_batch');
		$this->db->where('e2.stock_id',NULL);

		$query=$this->db->get();
		return $query->result_array();
		// print_r($query->result_array());
	}



  public function getSTransferview($pid){
	$this->db->select('voucher_entry.*,dp1.dp_department as dp_1,dp2.dp_department as dp_2,voucher_entry_detail.*');
    $this->db->from('voucher_entry');
    $this->db->join('voucher_entry_detail','voucher_entry_detail.ved_veid = voucher_entry.ve_id', 'inner');
    $this->db->join('department dp1','dp1.dp_id = voucher_entry.ve_supplier', 'left');
    $this->db->join('department dp2','dp2.dp_id = voucher_entry.ve_customer', 'left');
    // $this->db->join('users','users.u_emp_id = voucher_entry.ve_user', 'inner'); 
    $this->db->where('voucher_entry.ve_id',$pid);
    $this->db->where('voucher_entry.ve_type','st');
    $query=$this->db->get();
    return $query->result_array();
  }

	
	public function getCompany(){
		$this->db->select('*');
		$this->db->from('company');
		$query=$this->db->get();
		return $query->result_array();
	}

	public function getDepartment(){
		$this->db->select('*');
		$this->db->from('department');
		$this->db->where('dp_status',1);
		$query=$this->db->get();
		return $query->result_array();
	}

	}

	?>
