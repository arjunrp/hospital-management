<?php

class Expence_model extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";

		$this->db->select('excategory.category,voucher_entry.*');
		$this->db->from('voucher_entry');
		$this->db->join('excategory','excategory.id = voucher_entry.ve_customer', 'inner');
		$this->db->where('voucher_entry.ve_type','exp');
		$this->db->order_by('voucher_entry.ve_date','desc');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{
			$output 	.=	"<tr><td>".$row->ve_vno."</td>";
			$output 	.=	"<td>".$row->category."</td>";
			$output 	.=	"<td>".date("d-m-Y",strtotime($row->ve_date))."</td>";
			$output 	.=	"<td>".$row->ve_bill_no."</td>";
			$output 	.=	"<td>".$row->ve_apayable."</td>";
			 
			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='".$this->config->item('admin_url')."expence/edit/".$row->ve_id."' class='btn btn-info view-btn-edit' title='Edit' ><i class='fa fa-pencil-square-o'></i></a>
			 
		   <a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."expence/delete/".$row->ve_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}


function get_category()
	{
		$this->db->select('*');
		$this->db->from('excategory');
		$query 			=	$this->db->get();
		return $query->result_array();
	}
  
	function get_one_banner($id)
	{
		$this->db->select('excategory.category,voucher_entry.*');
		$this->db->from('voucher_entry');
		$this->db->join('excategory','excategory.id = voucher_entry.ve_customer', 'inner');
		$this->db->where('voucher_entry.ve_type','exp');
		$this->db->where('voucher_entry.ve_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}

	public function getExpenseId()
     {
		$this->db->select('ve_vno');
		$this->db->from('voucher_entry');
		$this->db->order_by('ve_vno','desc');
		$this->db->where('ve_status','1');
		$this->db->where('ve_type','exp');
		$this->db->limit(1);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return '1';
		}
		else
		{
			$result 	= $query->row()->ve_vno;
			$result++;
			return $result;
		}
	}
}