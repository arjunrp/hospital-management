<?php

class Ccounter_model extends CI_Model {

	function __construct() {
		parent::__construct();

		$this->load->database();
	}
	
	function list_all()
	{
		$output 		=	"";
		$pay_path 		=	"";
		$this->db->select('voucher_entry.*,department.dp_department');
    	$this->db->from('voucher_entry');
    	$this->db->join('department','department.dp_id = voucher_entry.ve_supplier', 'inner');
    	$this->db->order_by('voucher_entry.ve_id','desc');
    	$this->db->where('voucher_entry.ve_status','cc');
    	$query=$this->db->get();
		// foreach($query->result() as $row)
		// {
		// 	if($row->ve_type=="lbi" || $row->ve_type=="si" || $row->ve_type=="dis" || $row->ve_type=="ad" || $row->ve_type=="scani" || $row->ve_type=="xrayi") { $ip_no = $row->ve_customer;  }
		// 	else { $ip_no = "0";  }

		// 	if($row->ve_type=="lbi" || $row->ve_type=="lbo")
		// 	{
		// 		$pay_path = "insert/".$row->ve_id."/".$row->ve_date."/".$row->ve_apayable;
		// 		$dp_department = $row->dp_department;
		// 	}
		// 	if($row->ve_type=="scani" || $row->ve_type=="scano")
		// 	{
		// 		$pay_path = "insert/".$row->ve_id."/".$row->ve_date."/".$row->ve_apayable;
		// 		$dp_department = $row->dp_department;
		// 	}
		// 	if($row->ve_type=="xrayi" || $row->ve_type=="xrayo")
		// 	{
		// 		$pay_path = "insert/".$row->ve_id."/".$row->ve_date."/".$row->ve_apayable;
		// 		$dp_department = $row->dp_department;
		// 	}
		// 	if($row->ve_type=="si" || $row->ve_type=="so")
		// 	{
		// 		$pay_path = "insert/".$row->ve_id."/".$row->ve_date."/".$row->ve_apayable;
		// 		$dp_department = $row->dp_department;
		// 	}
		// 	if($row->ve_type=="dis")
		// 	{
		// 		$pay_path = "update/".$row->ve_id."/".$row->ve_date."/".$row->ve_customer."/".$row->ve_apayable;
		// 		$dp_department = "Discharge - ".$row->dp_department;
		// 	}
		// 	if($row->ve_type=="opbl")
		// 	{
		// 		$pay_path = "insert/".$row->ve_id."/".$row->ve_date."/".$row->ve_apayable;
		// 		$dp_department = $row->dp_department;
		// 	}
		// 	if($row->ve_type=="ad")
		// 	{
		// 		$pay_path = "insert/".$row->ve_id."/".$row->ve_date."/".$row->ve_apayable;
		// 		$dp_department = "Advance - ".$row->dp_department;
		// 	}
		// 	if($row->ve_type=="extra")
		// 	{
		// 		$pay_path = "insert1/".$row->ve_id."/".$row->ve_date."/".$row->ve_apayable;
		// 		$dp_department = "Billing - ".$row->dp_department;
		// 	}
		// 	$output 	.=	"<tr><td>".date("d-m-Y",strtotime($row->ve_date))."</td>";
		// 	$output 	.=	"<td>".$row->ve_vno."</td>";
		// 	$output 	.=	"<td>".$dp_department."</td>";
		// 	$output 	.=	"<td>".$row->ve_apayable."</td>";

		// 	$output 	.=	"<td class='btn-group  btn-group-xs'><a onclick='return confirm('Are you sure you want to delete this item?');' href='". $this->config->item('admin_url')."ccounter/".$pay_path."'class='btn btn-success view-btn-edit' title='Pay'><i class='fa fa-floppy-o'></i></a> </td>"; 

		// 	$output		.=	"</tr>";
		// }
		return $query->result_array();
	}

	public function getCustomer(){
		$query=$this->db->get_where('patient',array('p_status'=>1));
		return $query->result_array();	
	}

	public function getProduct(){
		$this->db->select('voucher_entry_detail.*,stock.stock_qty,product.pd_unitprice,product.pd_price,stock.stock_id');
		$this->db->from('stock');
		$this->db->join('voucher_entry_detail','voucher_entry_detail.ved_itemid = stock.stock_product_id AND voucher_entry_detail.ved_batch = stock.stock_batch', 'inner');
		$this->db->join('product','voucher_entry_detail.ved_itemid = product.pd_code', 'inner');
		$this->db->group_by('stock.stock_product_id');
		$this->db->group_by('stock.stock_batch');
		$this->db->order_by('stock.created_at','asc');
    	$this->db->where('stock.stock_qty > ','0');
    	$this->db->where('stock.stock_dept','3');
    	$this->db->where('stock.stock_status','standby');
		$query=$this->db->get();
		return $query->result_array();
		
	}

	public function getProducts(){

		$this->db->select('voucher_entry_detail.*,stock.stock_qty,product.pd_unitprice,product.pd_price');
		$this->db->from('stock');
		$this->db->join('voucher_entry_detail','voucher_entry_detail.ved_itemid = stock.stock_product_id AND voucher_entry_detail.ved_batch = stock.stock_batch', 'inner');
		$this->db->join('product','voucher_entry_detail.ved_itemid = product.pd_code', 'inner');
		$this->db->group_by('stock.stock_product_id');
		$this->db->group_by('stock.stock_batch');
		$this->db->order_by('stock.stock_id','asc');
		$this->db->where('stock.stock_qty > ','0');
		$this->db->where('stock.stock_dept','3');

	// $this->db->select('c.stock_qty, e1.*');
 //    $this->db->from('stock c');
 //    $this->db->join('product e1','c.stock_product_id = e1.id', 'left');
 //    $this->db->join('stock e2','c.stock_product_id = e2.stock_product_id AND e2.stock_id > c.stock_id', 'left');
 //    $this->db->where('e2.stock_id',NULL);
    $query=$this->db->get();
    return $query->result_array();

	}



public function getCashview($sid){
	$this->db->select('voucher_entry.*,voucher_entry_detail.*,department.dp_department');
    $this->db->from('voucher_entry');
    $this->db->join('voucher_entry_detail','voucher_entry_detail.ved_veid = voucher_entry.ve_id', 'inner');
    $this->db->join('users','users.u_emp_id = voucher_entry.ve_user', 'inner'); 
    $this->db->join('department','department.dp_id = voucher_entry.ve_supplier', 'inner');
    $this->db->where('voucher_entry.ve_id',$sid);
    $query=$this->db->get();
    return $query->result_array();
}

public function getAdvanceview($sid){
	$this->db->select('voucher_entry.*,patient.p_title,patient.p_name,patient.p_phone,department.dp_department');
    $this->db->from('voucher_entry');
    $this->db->join('patient','patient.p_mrd_no = voucher_entry.ve_mrd', 'inner');
    $this->db->join('users','users.u_emp_id = voucher_entry.ve_user', 'inner'); 
    $this->db->join('department','department.dp_id = voucher_entry.ve_supplier', 'inner');
    $this->db->where('voucher_entry.ve_id',$sid);
    $query=$this->db->get();
    return $query->result_array();
}

public function get_patientsip(){
		$this->db->select('ip.*,patient.p_title,patient.p_name,patient.p_phone,department.	dp_department,users.u_name');
		$this->db->from('ip');
		$this->db->join('patient','patient.p_mrd_no = ip.ip_mrd', 'inner');
		$this->db->join('department','department.dp_id = ip.ip_department', 'inner');
		$this->db->join('users','users.u_emp_id = ip.ip_doctor', 'inner');
		$query=$this->db->get();
		return $query->result_array();
	}

	public function getTodayreport() {

	$today = date("Y-m-d");
	$this->db->select('sum(ve_apaid) as apaid,voucher_entry.ve_cash_date,voucher_entry.ve_type,department.dp_department,min(ve_vno) as mi_veno,max(ve_vno) as mx_veno');
    $this->db->from('voucher_entry');
    $this->db->join('department','department.dp_id = voucher_entry.ve_supplier', 'inner');
    $this->db->where('voucher_entry.ve_cash_date',$today);
    $this->db->group_by('voucher_entry.ve_supplier');
    $this->db->where('voucher_entry.ve_type !=','preg');
    $this->db->where('voucher_entry.ve_type !=','prenew');
    $this->db->group_by('voucher_entry.ve_type');
    $this->db->group_by('voucher_entry.ve_user');
    // $this->db->where('voucher_entry.ve_supplier !=','17');
    $query=$this->db->get();
    return $query->result_array();
  }

  public function getToday_reg_report() {

	$today = date("Y-m-d");
	$this->db->select('voucher_entry.*,patient.p_title,patient.p_name,patient.p_phone');
    $this->db->from('voucher_entry');
    $this->db->join('patient','patient.p_mrd_no = voucher_entry.ve_mrd', 'inner');
    $where = '(voucher_entry.ve_type="preg" or voucher_entry.ve_type = "prenew")';
    $this->db->where($where);
    $this->db->where('voucher_entry.ve_date',$today);
    $this->db->group_by('voucher_entry.ve_id');
    $query=$this->db->get();
    return $query->result_array();
  }

   public function getTotalCreport_IP($from_date,$to_date,$dept) {
    $this->db->select('voucher_entry.*,department.dp_department,patient.p_name as ve_patient,patient.p_phone as ve_phone');
    $this->db->from('voucher_entry');
    $this->db->join('patient','patient.p_mrd_no = voucher_entry.ve_mrd', 'inner');
    $this->db->join('department','department.dp_id = voucher_entry.ve_supplier', 'inner');
    $this->db->where('ve_cash_date !=','0000-00-00');
    $this->db->where('ve_cash_date BETWEEN "'.$from_date. '" and "'. $to_date.'"');
    $this->db->order_by('ve_cash_date','desc');
    $this->db->where('ve_supplier',$dept);
    $query=$this->db->get();

    if($query->num_rows() == 0)
    {
        return 0;
    }
    else
    {
        return $query->result_array();
    }


  }

  public function getTotalCreport($from_date,$to_date,$dept) {
	$this->db->select('voucher_entry.*,department.dp_department');
    $this->db->from('voucher_entry');
    $this->db->join('department','department.dp_id = voucher_entry.ve_supplier', 'inner');
    $this->db->where('ve_cash_date !=','0000-00-00');
    $this->db->where('ve_cash_date BETWEEN "'.$from_date. '" and "'. $to_date.'"');
	$this->db->order_by('ve_cash_date','desc');

    if($dept == "0")
    {
    	$query=$this->db->get();
    }
    else if($dept == "x")
    {
    	$this->db->where('ve_type !=','preg');
        $this->db->where('ve_type !=','prenew');
    	$query=$this->db->get();
    }
    else if($dept == "6")
    {
        $this->db->where('ve_type !=','preg');
        $this->db->where('ve_type !=','prenew');
        $this->db->where('ve_supplier','17');
        $query=$this->db->get();
    }
    else
    {
    	$this->db->where('ve_supplier',$dept);
    	$query=$this->db->get();
    }

    if($query->num_rows() == 0)
    {
    	return 0;
    }
    else
    {
    	return $query->result_array();
    }


  }

  public function getTotalCreport_OP($from_date,$to_date,$dept) {

	$this->db->select('voucher_entry.*,patient.p_name as ve_patient,patient.p_phone as ve_phone,department.dp_department');
    $this->db->from('voucher_entry');
    $this->db->join('patient','patient.p_mrd_no = voucher_entry.ve_mrd', 'inner');
    $this->db->join('department','department.dp_id = voucher_entry.ve_supplier', 'inner');
    $this->db->where('ve_date BETWEEN "'.$from_date. '" and "'. $to_date.'"');
	$this->db->order_by('ve_cash_date','desc');
	$this->db->group_by('ve_id');

    if($dept != 0)
    {
    	$this->db->where('ve_supplier',$dept);
    	$query=$this->db->get();
    }
    else
    {
    	$query=$this->db->get();
    }

    if($query->num_rows() == 0)
    {
    	return 0;
    }
    else
    {
    	return $query->result_array();
    }

  }

   public function getSummaryCreport($from_date,$to_date,$dept) {
    $this->db->select('sum(ve_apaid) as apaid,voucher_entry.ve_cash_date,department.dp_department,min(ve_vno) as mi_veno,max(ve_vno) as mx_veno,voucher_entry.ve_type');
    $this->db->from('voucher_entry');
    $this->db->join('department','department.dp_id = voucher_entry.ve_supplier', 'inner');
    $this->db->where('ve_cash_date BETWEEN "'.$from_date. '" and "'. $to_date.'"');
    $this->db->group_by('voucher_entry.ve_supplier');
    // $this->db->group_by('voucher_entry.ve_type');
    // $this->db->having('voucher_entry.ve_type = sr',true);

    if($dept == "0")
    {
        $this->db->group_by('voucher_entry.ve_type');
        $this->db->group_by('voucher_entry.ve_user');
        $query=$this->db->get();
    }
    else if($dept == "x")
    {
        $this->db->where('ve_type !=','preg');
        $this->db->where('ve_type !=','prenew');
        $this->db->group_by('voucher_entry.ve_type');
        $this->db->group_by('voucher_entry.ve_user');
        $query=$this->db->get();
    }
    else if($dept == "6")
    {
        $this->db->where('ve_type !=','preg');
        $this->db->where('ve_type !=','prenew');
        $this->db->where('ve_supplier','17');
        $query=$this->db->get();
    }

    else
    {
        $this->db->where('voucher_entry.ve_supplier',$dept);
        $query=$this->db->get();
    }

    if($query->num_rows() == 0)
    {
        return 0;
    }
    else
    {
        return $query->result_array();
    }


  }

  public function getSummaryCreport_Pharma($from_date,$to_date,$b_type) {

	$this->db->select('sum(ve_apaid) as apaid,voucher_entry.ve_cash_date,department.dp_department,min(ve_vno) as mi_veno,max(ve_vno) as mx_veno,voucher_entry.ve_type');
    $this->db->from('voucher_entry');
    $this->db->join('department','department.dp_id = voucher_entry.ve_supplier', 'inner');
    $this->db->where('ve_cash_date BETWEEN "'.$from_date. '" and "'. $to_date.'"');
    $this->db->where('voucher_entry.ve_supplier','3');
    // $this->db->group_by('voucher_entry.ve_supplier');
    // $this->db->having('voucher_entry.ve_type = sr',true);

    if($b_type == "sa")
    {
        // $this->db->group_by('voucher_entry.ve_type');
        $this->db->group_by('voucher_entry.ve_user');
        $where = '(ve_type="si" or ve_type = "so")';
        $this->db->where($where);
    	$query=$this->db->get();
    }
    else
    {
        $this->db->group_by('voucher_entry.ve_type');
        $where = '(ve_type="opbl" or ve_type = "sr")';
        $this->db->where($where);
        $query=$this->db->get();
    }

    if($query->num_rows() == 0)
    {
        return 0;
    }
    else
    {
        return $query->result_array();
    }

  }

   public function getSummaryCreport_OP($from_date,$to_date,$dept) {

   $this->db->select('sum(ve_apaid) as apaid,voucher_entry.ve_cash_date,department.dp_department,min(ve_vno) as mi_veno,max(ve_vno) as mx_veno,voucher_entry.ve_type');
    $this->db->from('voucher_entry');
    $this->db->join('department','department.dp_id = voucher_entry.ve_supplier', 'inner');
    $this->db->where('ve_date BETWEEN "'.$from_date. '" and "'. $to_date.'"');
    $this->db->order_by('ve_cash_date','desc');
    // $this->db->group_by('voucher_entry.ve_id');
    // $this->db->group_by('voucher_entry.ve_type');
    $this->db->where('ve_supplier',$dept);
    $this->db->where('ve_type != ','opbl');
    $query=$this->db->get();

    if($query->num_rows() == 0)
    {
        return 0;
    }
    else
    {
        return $query->result_array();
    }



 //   	$this->db->select('sum(ve_apaid) as apaid,voucher_entry.ve_date,department.dp_department,min(ve_vno) as mi_veno,max(ve_vno) as mx_veno,voucher_entry.ve_type');
 //    $this->db->from('voucher_entry');
 //    $this->db->join('department','department.dp_id = voucher_entry.ve_supplier', 'inner');
 //    $this->db->where('ve_date BETWEEN "'.$from_date. '" and "'. $to_date.'"');
 //    $this->db->group_by('voucher_entry.ve_supplier');
 //    $this->db->where('voucher_entry.ve_supplier !=','17');

	// $query=$this->db->get();
 //    return $query->result_array();

  }

  public function getDepartment(){
		$this->db->select('*');
		$this->db->from('department');
		$this->db->where('dp_status',1);
		$this->db->where('dp_type','nt');
		$query=$this->db->get();
		return $query->result_array();
	}

	public function get_department($dept){
		$this->db->select('dp_department');
		$this->db->from('department');
		$this->db->where('dp_id',$dept);
		$query=$this->db->get();
		return $query->row()->dp_department;
	}

    public function getAdvReturnId(){
        $this->db->select('CAST(ve_vno as SIGNED INTEGER) as ve_vno');
        $this->db->from('voucher_entry');
        $this->db->order_by('ve_vno','desc');
        $this->db->where('ve_type','adr');
        $this->db->limit(1);
        $query          =   $this->db->get();
        if($query->num_rows()==0)
        {
            return '1';
        }
        else
        {
            $result     = $query->row()->ve_vno;
            $result++;
            return $result;
        }
    }

	}

	?>
