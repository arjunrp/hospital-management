<?php

class Room_shift_model extends CI_Model 
{
	function get_patient_rooms($rs_ip)
	{
		// <a href='".$this->config->item('admin_url')."room_shift/edit/".$row->rs_id."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
		$output	=	"";

		$this->db->select('room_shift.*,room.rm_no,bed.bd_no');
		$this->db->from('room_shift');
		$this->db->join('room','room.rm_id = room_shift.rs_rmno', 'inner');
		$this->db->join('bed','bed.bd_id = room_shift.rs_bdno', 'inner');
		$this->db->order_by('room_shift.rs_id','asc');
		$this->db->where('room_shift.rs_ip',$rs_ip);
		$query 			=	$this->db->get();
		if($query->num_rows()>0) {
		foreach($query->result() as $row)
		{
			if($row->rs_dis=="0000-00-00 00:00:00") { $rs_dis = 'Nil'; $rs_time = 'Nil'; } 
			else { $rs_dis =  date("d-m-Y",strtotime($row->rs_dis)); $rs_time =  date("h:i A",strtotime($row->rs_dis)); }
			$output 	.=	"<tr><td>".$row->rm_no."</td>";
			$output 	.=	"<td>".$row->bd_no."</td>";
			$output 	.=	"<td>".date("d-m-Y",strtotime($row->rs_adm))."</td>";
			$output 	.=	"<td>".date("h:i A",strtotime($row->rs_adm))."</td>";
			$output 	.=	"<td>".$rs_dis."</td>";
			$output 	.=	"<td>".$rs_time."</td>";


			$output 	.=	"<td class='btn-group  btn-group-xs' > 
			<a href='".$this->config->item('admin_url')."room_shift/edit/".$row->rs_id."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
			<a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."room_shift/delete/".$row->rs_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	} 
	else { return false; }
	} 

	function get_ip($id)
	{

		$this->db->select('ip.*,users.u_name,patient.p_title,patient.p_name,patient.p_phone,department.dp_department');
		$this->db->from('ip');
		$this->db->join('users','users.u_emp_id = ip.ip_doctor', 'inner');
		$this->db->join('department','department.dp_id = ip.ip_department', 'inner');
		$this->db->join('patient','patient.p_mrd_no = ip.ip_mrd', 'inner');
		$this->db->where('ip.ip_ipno',$id);
		$query 			=	$this->db->get();
		return $query->result_array();
	}

	function get_rooms()
	{

		$this->db->select('*');
		$this->db->from('room');
		$this->db->where('rm_status','1');
		$query 			=	$this->db->get();
		return $query->result_array();
	}
	
	function get_all_beds()
	{

		$this->db->select('room.*,bed.*');
		$this->db->from('bed');
		$this->db->join('room','room.rm_id = bed.bd_rmid','inner');
		$this->db->order_by('rm_id','asc');
		$query 			=	$this->db->get();
		return $query->result_array();
	}

	function get_roomshift($rs_ip)
	{
		$this->db->select('rs_id');
    	$this->db->from('room_shift');
    	$this->db->where('rs_ip',$rs_ip);
    	$this->db->where('rs_dis','0000-00-00');
    	$this->db->order_by('rs_adm','desc');
    	$this->db->limit(1);
		$query 			=	$this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->row()->rs_id;
		}
		else
		{
			return 0;
		}
	}

  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('room_shift');
		$this->db->where('rs_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}


	function get_rm_no($rs_rmno)
	{
		$this->db->select('rm_no');
    	$this->db->from('room'); 
    	$this->db->where('rm_id',$rs_rmno);
		$query 			=	$this->db->get();
		return $query->row()->rm_no;
	}

	function get_bed_no($rs_rmno)
	{
		$this->db->select('*');
		$this->db->from('bed');
		$this->db->where('bd_rmid',$rs_rmno);
		$query 			=	$this->db->get();
		return $query->result_array();
	}


	function get_opno($op_shift,$op_doctor)
	{
		$bk_date = date("Y-m-d");
		$this->db->select('op_opno');
		$this->db->from('op');
		$this->db->where('op_date',$bk_date);
		$this->db->where('op_shift',$op_shift);
		$this->db->where('op_doctor',$op_doctor);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return 'B_01';
		}
		else
		{
			$result 	= $query->row()->op_opno;
			$result++;
			return $result;
		}
	}

	function get_docfee($op_doctor)
	{
		$this->db->select('u_fees');
		$this->db->from('users');
		$this->db->where('u_emp_id',$op_doctor);
		$query 			=	$this->db->get();
		return $query->row()->u_fees;
	}

	function today_book()
	{
		$output	=	"";
		$shift 	=	"";
		$bk_date = date("Y-m-d");

		$this->db->select('booking.*,users.u_department,users.u_fees');
		$this->db->from('booking');
		$this->db->join('users','users.u_emp_id = booking.bk_doc', 'inner');
		$this->db->order_by('booking.bk_no','asc');
		$this->db->where('booking.bk_today',$bk_date);
		$query 			=	$this->db->get();
		return $query->result_array();
	}

	public function getDepartment(){
		$this->db->select('*');
		$this->db->from('department');
		$this->db->where('dp_status',1);
		$this->db->where('dp_type','t');
		$query=$this->db->get();
		return $query->result_array();
	}

}