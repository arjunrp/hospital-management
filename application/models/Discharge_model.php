<?php

class Discharge_model extends CI_Model 
{
    function list_all()
    {
        $output         =   "";
        $sl_no          =   0;

        $this->db->select('ip.ip_mrd,ip.ip_ipno,ip.ip_discharge,patient.p_title,patient.p_name,patient.p_phone,voucher_entry.ve_apayable,voucher_entry.ve_vno,voucher_entry.ve_pstaus');
        $this->db->from('ip');
        $this->db->join('patient','patient.p_mrd_no = ip.ip_mrd', 'inner'); 
        $this->db->join('voucher_entry','voucher_entry.ve_customer = ip.ip_ipno', 'inner'); 
        $this->db->order_by('ip.ip_discharge','desc');
        $this->db->where('voucher_entry.ve_type','dis');
        $this->db->group_by('voucher_entry.ve_id');
        $query          =   $this->db->get();

        foreach($query->result() as $row)
        {
            if($row->ve_pstaus=="NP") { $fp = "<font color='#b20000'>Not Completed</font>";  }
            elseif($row->ve_pstaus=="FP") { $fp = "<font color='#228B22'>Completed</font>";  }

            $output     .=  "<tr><td>".$row->ve_vno."</td>";
            $output     .=  "<td>".date("d-m-Y",strtotime($row->ip_discharge))."</td>";
            $output     .=  "<td>".$row->ip_mrd."</td>";
            $output     .=  "<td>".$row->ip_ipno."</td>";
            $output     .=  "<td>".$row->p_title." ".$row->p_name."</td>";
            $output     .=  "<td>".$row->p_phone."</td>";
            $output     .=  "<td>".$fp."</td>";

            $output     .=  "<td class='btn-group  btn-group-xs' ><a href='".$this->config->item('admin_url')."discharge/view/".$row->ip_ipno."'class='btn btn-primary view-btn-edit' title='View'><i class='fa fa-eye'></i></a>
            <a href='".$this->config->item('admin_url')."discharge/d_return/".$row->ip_ipno."'class='btn btn-danger view-btn-edit' title='Return'><i class='fa fa-history'></i></a>
            </td>"; 
            $output     .=  "</tr>";
        }
        return $output;
    }

    function list_pending()
    {
        $output         =   "";
        $this->db->select('ip.ip_mrd,ip.ip_ipno,ip.ip_discharge,patient.p_title,patient.p_name,patient.p_phone,voucher_entry.ve_apayable,voucher_entry.ve_vno,voucher_entry.ve_pstaus,voucher_entry.ve_id');
        $this->db->from('ip');
        $this->db->join('patient','patient.p_mrd_no = ip.ip_mrd', 'inner'); 
        $this->db->join('voucher_entry','voucher_entry.ve_customer = ip.ip_ipno', 'inner'); 
        $this->db->order_by('ip.ip_discharge','desc');
        $this->db->where('voucher_entry.ve_status','pending');
        $this->db->group_by('voucher_entry.ve_id');
        $query=$this->db->get();

        foreach($query->result() as $row)
        {
            $output     .=  "<tr><td>".$row->ve_vno."</td>";
            $output     .=  "<td>".date("d-m-Y",strtotime($row->ip_discharge))."</td>";
            $output     .=  "<td>".$row->ip_mrd."</td>";
            $output     .=  "<td>".$row->ip_ipno."</td>";
            $output     .=  "<td>".$row->p_title." ".$row->p_name."</td>";
            $output     .=  "<td>".$row->p_phone."</td>";
            $output     .=  "<td>".$row->ve_apayable."</td>";
            $output     .=  "<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."discharge/discharge_Pending/".$row->ve_id."'class='btn btn-primary view-btn-edit' title='Move to Cash'><i class='fa fa-creative-commons'></i></a> </td>"; 
            $output     .=  "</tr>";
        }
        return $output;
    }

    function get_discharge_no()
    {
        $this->db->select('CAST(SUBSTRING(ve_vno, 4, length(ve_vno)-3) as UNSIGNED) as ve_vno');
        $this->db->from('voucher_entry');
        $this->db->where('ve_type','dis');
        $this->db->order_by('ve_vno','desc');
        $this->db->limit(1);
        $query      = $this->db->get();
        if($query->num_rows()==0)
        {
            $data = 'DS_001';
        }
        else
        {
            $end2   = $query->row()->ve_vno;
            $end2++;
            $data = "DS_".$end2;
        }
        return $data;
    }

    function get_discharge_rno()
    {
        $this->db->select('CAST(SUBSTRING(ve_vno, 4, length(ve_vno)-3) as UNSIGNED) as ve_vno');
        $this->db->from('voucher_entry');
        $this->db->where('ve_type','dsr');
        $this->db->order_by('ve_vno','desc');
        $this->db->limit(1);
        $query      = $this->db->get();
        if($query->num_rows()==0)
        {
            $data = 'DSR_001';
        }
        else
        {
            $end2   = $query->row()->ve_vno;
            $end2++;
            $data = "DSR_".$end2;
        }
        return $data;
    }

    function get_room($ipno){

    $this->db->select('room_shift.*,room.*');
    $this->db->from('ip'); 
    $this->db->join('room_shift','room_shift.rs_ip = ip.ip_ipno', 'inner'); 
    $this->db->join('room','room.rm_id = room_shift.rs_rmno', 'inner'); 
    $this->db->where('ip.ip_ipno',$ipno);
    // $this->db->where('room_shift.rs_days !=','0');
    $this->db->group_by('room_shift.rs_id');

    $query=$this->db->get();
    $data = $query->result_array();
    if($query->num_rows()==0) { $data ="0"; }
    return $data;
  }

  function get_voucher_bills($ipno){

    $this->db->select('*');
    $this->db->from('voucher_entry'); 
    $this->db->where('ve_customer',$ipno);
    $this->db->where('ve_type','lbi');
    $where = '(ve_status="cr" or ve_status = "acr")';
    $this->db->where($where);
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    return $data;
  }

  function get_scanning_bills($ipno){

    $this->db->select('*');
    $this->db->from('voucher_entry'); 
    $this->db->where('ve_customer',$ipno);
    $this->db->where('ve_type',"scani");
    $where = '(ve_status="cr" or ve_status = "acr")';
    $this->db->where($where);
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    return $data;
  }

  function get_xray_bills($ipno){

    $this->db->select('*');
    $this->db->from('voucher_entry'); 
    $this->db->where('ve_customer',$ipno);
    $this->db->where('ve_type',"xrayi");
    $where = '(ve_status="cr" or ve_status = "acr")';
    $this->db->where($where);
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    return $data;
  }

  function get_pharmacy_bills($ipno){

    $this->db->select('*');
    $this->db->from('voucher_entry'); 
    $this->db->where('ve_customer',$ipno);
    $this->db->where('ve_type',"si");
    $where = '(ve_status="cr" or ve_status = "acr")';
    $this->db->where($where);
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    return $data;
  }

  function get_pharmacy_returns($ipno){

    $this->db->select('*');
    $this->db->from('voucher_entry'); 
    $this->db->where('ve_customer',$ipno);
    $this->db->where('ve_type',"sr");
    $where = '(ve_status="cr" or ve_status = "acr")';
    $this->db->where($where);
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    return $data;
  }

   function get_advances($ipno){

    $this->db->select('*');
    $this->db->from('voucher_entry'); 
    $this->db->where('ve_customer',$ipno);
    $this->db->where('ve_type',"ad");
    $this->db->where('ve_status',"acc");
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    return $data;
  }

  function get_discharge($ipno){

    $this->db->select('voucher_entry.*,users.u_name,department.dp_department,patient.p_name,patient.p_title,patient.p_phone');
    $this->db->from('voucher_entry'); 
    $this->db->join('ip','ip.ip_ipno = voucher_entry.ve_customer', 'inner'); 
    $this->db->join('users','users.u_emp_id = ip.ip_doctor', 'inner'); 
    $this->db->join('department','department.dp_id = ip.ip_department', 'inner');
    $this->db->join('patient','patient.p_mrd_no = ip.ip_mrd', 'inner');
    $this->db->where('voucher_entry.ve_customer',$ipno);
    $this->db->where('voucher_entry.ve_type',"dis");
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    return $data;
  }

  function get_additional_items($ipno){

    $this->db->select('voucher_entry_detail.*');
    $this->db->from('voucher_entry'); 
    $this->db->join('voucher_entry_detail','voucher_entry_detail.ved_veid = voucher_entry.ve_id', 'inner'); 
    $this->db->where('voucher_entry.ve_customer',$ipno);
    $this->db->where('voucher_entry.ve_type',"dis");
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    return $data;
  }


    public function get_additional_bill(){
    $this->db->select('*');
    $this->db->from('additional_bill');
    $this->db->where('ab_status',1);
    $query=$this->db->get();
    return $query->result_array();
    }

    public function get_admin_pswd(){
    $this->db->select('u_disc_pswd');
    $this->db->from('users');
    $this->db->where('u_designation',4);
    $query=$this->db->get();
    return $query->row()->u_disc_pswd;
    }



}