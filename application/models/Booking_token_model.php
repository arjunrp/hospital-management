<?php

class Booking_token_model extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('doctor.*,users.u_name');
		$this->db->from('doctor');
		$this->db->join('users','users.u_emp_id = doctor.dc_docid', 'inner');
		$this->db->order_by('doctor.dc_id','asc');
		$this->db->where('doctor.dc_status','1');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{
			$sl_no++;
			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->u_name."</td>";
			$output 	.=	"<td>".$row->dc_morntok."</td>";
			$output 	.=	"<td>".$row->dc_evngtok."</td>";

			$output 	.=	"<td class='btn-group  btn-group-xs' ><a href='".$this->config->item('admin_url')."booking_token/edit/".$row->dc_docid."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
			<a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."booking_token/delete/".$row->dc_docid."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	}

  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('doctor');
		$this->db->where('dc_docid',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}

	function get_doctor()
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('u_type','4');
		$query 			=	$this->db->get();
		return $query->result_array();
	}

}