<?php

class Opregister_model extends CI_Model 
{
	// <a href='".$this->config->item('admin_url')."opregister/edit/".$row->op_id."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
	function list_all()
	{
		$output	=	"";
		$slno	=	"";
		$shift 	=	"";
		$bk_date = date("Y-m-d");

		$this->db->select('op.*,users.u_name,patient.p_title,patient.p_name,patient.p_phone');
		$this->db->from('op');
		$this->db->join('users','users.u_emp_id = op.op_doctor', 'inner');
		// $this->db->join('department','department.dp_id = ip.ip_department', 'inner');
		$this->db->join('patient','patient.p_mrd_no = op.op_mrd', 'inner');
		$this->db->order_by('op.op_opno','asc');
		$this->db->where('op.op_date',$bk_date);
		$query 			=	$this->db->get();

		if($query->num_rows()>0) {
		foreach($query->result() as $row)
		{

			$slno++;
			if($row->op_shift=="m") { $shift = "Morning"; }
			else if($row->op_shift=="e") { $shift = "Evening"; }
			$output 	.=	"<tr><td>".$row->op_opno."</td>";
			$output 	.=	"<td>".date("d-m-Y",strtotime($row->op_date))."</td>";
			$output 	.=	"<td>".$row->u_name."</td>";
			$output 	.=	"<td>".$shift."</td>";
			$output 	.=	"<td>".$row->op_mrd."</td>";
			$output 	.=	"<td>".$row->p_title." ".$row->p_name."</td>";
			$output 	.=	"<td>".$row->p_phone."</td>";


			$output 	.=	"<td class='btn-group  btn-group-xs' ><a href='".$this->config->item('admin_url')."opregister/edit/".$row->op_id."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
			<a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."opregister/delete/".$row->op_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	} 
	else { return false; }
	} 

  
	function get_one_banner($id)
	{
		$this->db->select('patient.*,op.*,users.u_name,department.dp_department');
		$this->db->from('op');
		$this->db->join('patient','patient.p_mrd_no = op.op_mrd','inner');
		$this->db->join('users','users.u_emp_id = op.op_doctor','inner');
		$this->db->join('department','department.dp_id = op.op_department','inner');
		$this->db->where('op.op_id',$id);
		$query 			=	$this->db->get();
		return $query->result_array();
	}
	function get_one_banners($id)
	{
		$this->db->select('patient.*,op.*');
		$this->db->from('op');
		$this->db->join('patient','patient.p_mrd_no = op.op_mrd','inner');
		$this->db->where('op.op_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}

	function get_token_op($id)
	{
		$this->db->select('patient.*,department.*,users.*,op.*');
		$this->db->from('op');
		$this->db->join('patient','patient.p_mrd_no = op.op_mrd','inner');
		$this->db->join('department','department.dp_id = op.op_department','inner');
		$this->db->join('users','users.u_emp_id = op.op_doctor','inner');
		$this->db->where('op.op_mrd',$id);
		$this->db->order_by('op.op_date','desc');
		$this->db->limit(1);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}

	function get_token()
	{
		$bk_date = date("Y-m-d",strtotime('tomorrow'));
		$this->db->select('bk_no');
		$this->db->from('booking');
		$this->db->where('bk_date',$bk_date);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return '01';
		}
		else
		{
			$result 	= $query->row()->bk_no;
			$result++;
			return $result;
		}
	}

	function get_patients()
	{
		$this->db->select('*');
		$this->db->from('patient');
		$this->db->where('p_status','1');
		$query 			=	$this->db->get();
		return $query->result_array();
	}

	function get_patient($op_mrd)
	{
		$this->db->select('*');
		$this->db->from('patient');
		$this->db->where('p_mrd_no',$op_mrd);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}


	function get_opno($op_shift,$op_doctor)
	{
		$bk_date = date("Y-m-d");
		$this->db->select('op_opno');
		$this->db->from('op');
		$this->db->where('op_date',$bk_date);
		$this->db->where('op_shift',$op_shift);
		$this->db->where('op_doctor',$op_doctor);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return 'B_01';
		}
		else
		{
			$result 	= $query->row()->op_opno;
			$result++;
			return $result;
		}
	}

	function get_docfee($op_doctor)
	{
		$this->db->select('u_fees');
		$this->db->from('users');
		$this->db->where('u_emp_id',$op_doctor);
		$query 			=	$this->db->get();
		return $query->row()->u_fees;
	}

	function today_book()
	{
		$output	=	"";
		$shift 	=	"";
		$bk_date = date("Y-m-d");

		$this->db->select('booking.*,users.u_department,users.u_fees,patient.*');
		$this->db->from('booking');
		$this->db->join('users','users.u_emp_id = booking.bk_doc', 'inner');
		$this->db->join('patient','booking.bk_mrd = patient.p_mrd_no', 'inner');
		$this->db->order_by('booking.bk_no','asc');
		$this->db->where('booking.bk_date',$bk_date);
		$query 			=	$this->db->get();
		return $query->result_array();
	}

	function today_books($id)
	{

		$this->db->select('booking.*,department.dp_department,users.u_name,users.u_department,users.u_fees,patient.*');
		$this->db->from('booking');
		$this->db->join('users','users.u_emp_id = booking.bk_doc', 'inner');
		$this->db->join('department','department.dp_id = booking.bk_dept', 'inner');
		$this->db->join('patient','booking.bk_mrd = patient.p_mrd_no', 'inner');
		$this->db->order_by('booking.bk_no','asc');
		$this->db->where('booking.bk_id',$id);
		$query 			=	$this->db->get();
		return $query->result_array();
	}

	function today_nbooks($id)
	{
		$this->db->select('*');
		$this->db->from('patient');
		$this->db->where('p_mrd_no',$id);
		$query 			=	$this->db->get();
		return $query->result_array();
	}

}