<?php

class Addbilling_model extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('voucher_entry.*');
		$this->db->from('voucher_entry');
		$this->db->order_by('voucher_entry.ve_date','desc');
		$this->db->where('voucher_entry.ve_type','extra');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{
			if($row->ve_pstaus=="NP") { $fp = "<font color='#b20000'>Not Completed</font>";  }
            elseif($row->ve_pstaus=="FP") { $fp = "<font color='#228B22'>Completed</font>";  }

			$output 	.=	"<tr><td>".$row->ve_vno."</td>";
			$output 	.=	"<td>".date("d-m-Y",strtotime($row->ve_date))."</td>";
			$output 	.=	"<td>".$row->ve_mrd."</td>";
			$output 	.=	"<td>".$row->ve_patient."</td>";
			$output 	.=	"<td>".$row->ve_phone."</td>";
            $output     .=  "<td>".$fp."</td>";

			$output 	.=	"<td class='btn-group  btn-group-xs' ><a href='".$this->config->item('admin_url')."addbilling/view/".$row->ve_id."'class='btn btn-primary view-btn-edit' title='View'><i class='fa fa-eye'></i></a></td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	}

	function get_op_bill_no()
	{
		$this->db->select('CAST(ve_vno as SIGNED INTEGER) as ve_vno');
		$this->db->from('voucher_entry');
		$this->db->where('ve_type','extra');
        $this->db->order_by('ve_vno','desc');
        $this->db->limit(1);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return '1';
		}
		else
		{
			$result 	= $query->row()->ve_vno;
			$result++;
			return $result;
		}
	}


  function get_additional_items($ve_id){

    $this->db->select('voucher_entry_detail.*,voucher_entry.*,department.dp_department');
    $this->db->from('voucher_entry'); 
    $this->db->join('voucher_entry_detail','voucher_entry_detail.ved_veid = voucher_entry.ve_id', 'inner');  
    $this->db->join('department','department.dp_id = voucher_entry.ve_supplier', 'inner');
    $this->db->where('voucher_entry.ve_id',$ve_id);
    // $this->db->where('voucher_entry.ve_type',"extra");
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    return $data;
  }


	public function get_additional_bill(){
	$this->db->select('*');
	$this->db->from('additional_bill');
	$this->db->where('ab_status',1);
	$query=$this->db->get();
	return $query->result_array();
	}

    public function get_admin_pswd(){
    $this->db->select('u_disc_pswd');
    $this->db->from('users');
    $this->db->where('u_designation',4);
    $query=$this->db->get();
    return $query->row()->u_disc_pswd;
    }



}