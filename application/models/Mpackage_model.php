<?php

class Mpackage_model extends CI_Model {

	function __construct() {
		parent::__construct();

		$this->load->database();
	}

	function list_all()
	{
		$output 		=	"";
		$this->db->select('medtp.*,suppliers.sp_vendor');
    	$this->db->from('medtp');
    	$this->db->join('suppliers','suppliers.sp_id = medtp.mt_supplier', 'inner');
    	$this->db->order_by('medtp.mt_pkgno','desc');
    	$query=$this->db->get();
		foreach($query->result() as $row)
		{
			$output 	.=	"<tr><td>".$row->mt_pkgno."</td>";
			$output 	.=	"<td>".$row->mt_pakcage."</td>";
			$output 	.=	"<td>".$row->sp_vendor."</td>";
			 

			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."mpackage/view/".$row->mt_id."'class='btn btn-primary view-btn-edit' title='View'><i class='fa fa-eye'></i></a> 
			<a href='".$this->config->item('admin_url')."mpackage/edit/".$row->mt_id."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
			 <a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."mpackage/delete/".$row->mt_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	}

	public function getPackageId()
     {

		$this->db->select('mt_pkgno');
		$this->db->from('medtp');
		$this->db->order_by('mt_pkgno','desc');
		$this->db->where('mt_status','1');
		$this->db->limit(1);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return '1';
		}
		else
		{
			$result 	= $query->row()->mt_pkgno;
			$result++;
			return $result;
		}
	}


  public function getPackageview($pid){
		$this->db->select('medtp.*,suppliers.*,medtp_detail.*,users.u_emp_id');
    $this->db->from('medtp');
    $this->db->join('medtp_detail','medtp_detail.mtd_mtid = medtp.mt_id', 'inner'); 
    $this->db->join('suppliers','suppliers.sp_id = medtp.mt_supplier', 'inner');
    $this->db->join('users','users.u_emp_id = medtp.mt_user', 'inner'); 
    $this->db->where('medtp.mt_id',$pid);
    $query=$this->db->get();
    return $query->result_array();
  }


	
	public function getCompany(){
		$this->db->select('*');
		$this->db->from('company');
		$query=$this->db->get();
		return $query->result_array();
	}

	}

	?>
