<?php

class Scanname_model extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;
// $sql="SELECT E.*,EC.* FROM mw_tests AS E INNER JOIN mw_test_name AS EC WHERE E.ts_id=EC.tn_testid AND E.ts_status=1 AND EC.tn_status=1 order by EC.tn_testid asc";

		$this->db->select('scan.*,sc_name.*');
    	$this->db->from('sc_name');
    	$this->db->join('scan','scan.sc_id = sc_name.sn_scanid', 'left');
    	$this->db->order_by('scan.sc_test','asc');
		$query 			=	$this->db->get();

		// $query 			=	$this->db->query($sql);
		// $this->db->select('*');
		// $this->db->from('expence');

		foreach($query->result() as $row)
		{
			$sl_no++;
			if($row->sn_status == '1')
			{
				$image = "<i class='fa fa-check'></i>";
				$sn_status = 0;
			}
			else
			{
				$image = "<i class='fa fa-ban'></i>";
				$status = 1;
			}
			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->sc_test."</td>";
			$output 	.=	"<td>".$row->sn_name."</td>";
			$output 	.=	"<td>".$row->sn_price."</td>";
			 
			 
			// $output 	.=	"<td><img src='".$this->config->item('image_url')."features/small/".$row->feature_image."'></td>";
			 
			// $output 	.=	"<td><a href='".$this->config->item('admin_url')."expence/active/".$row->id."/".$status."'>".$image."</a></td>";
			// $output 	.=	"<td class='btn-group  btn-group-xs'><a href='".$this->config->item('admin_url')."expence/edit/".$row->id."' class='btn btn-info view-btn-edit' title='Edit' ><i class='fa fa-pencil-square-o'></i></a> 
			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='".$this->config->item('admin_url')."scanname/edit/".$row->sn_id."' class='btn btn-primary view-btn-edit' title='Edit' ><i class='fa fa-pencil-square-o'></i></a>
			 
		   <a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."scanname/delete/".$row->sn_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>";
			// <a  href='".$this->config->item('admin_url')."expence/delete/".$row->id."'class='btn btn-danger' title='Delete'><i class='fa fa-times-circle-o'></i></a>";
			// $output		.=	"<td><a href='".$this->config->item('admin_url')."expence/delete/".$row->id."'><i class='fa fa-trash'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}


function get_category()
	{
		$this->db->select('*');
		$this->db->from('scan');
		$query 			=	$this->db->get();
		return $query->result_array();
	}
  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('sc_name');
		$this->db->where('sn_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}
}