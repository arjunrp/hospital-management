<?php

class Accountmodel extends CI_Model 
{

	function get_account($fdate,$tdate,$product,$r_type)
	{
		$output 		=	"";
		$sum_total 		=	0;
		$sgst_total 	=	0;
		$cgst_total 	=	0;
		$gsum_total 	=	0;
		$p_brand 					= 	$this->input->post('p_brand');
		$p_type 					= 	$this->input->post('p_type');

		if($r_type==1)
		{
			$this->db->select('voucher_entry_detail.*,sum(ved_qty) as qty,sum(ved_free) as free,sum(ved_total) as total,sum(ved_sgsta) as sgst,sum(ved_cgsta) as cgst,sum(ved_gtotal) as grand_total');
			$this->db->from('voucher_entry_detail');
			$this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid', 'inner');
			$this->db->join('product','product.pd_code = voucher_entry_detail.ved_itemid', 'inner');
			$this->db->group_by('voucher_entry_detail.ved_date');
			$this->db->group_by('voucher_entry_detail.ved_itemid');
			$this->db->group_by('voucher_entry_detail.ved_batch');
			$this->db->order_by('voucher_entry_detail.ved_date','desc');
			$this->db->where('ved_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			$this->db->where('voucher_entry.ve_type','p');
			if($product == 'br')
			{
				$this->db->where('product.pd_brandid',$p_brand); 
				$query=$this->db->get();
			}
			else if($product == 'ty')
			{
				$this->db->where('product.pd_type',$p_type); 
				$query=$this->db->get();
			}
			else if($product == 'brty')
			{
				$this->db->where('product.pd_brandid',$p_brand);
				$this->db->where('product.pd_type',$p_type);
				$query=$this->db->get();
			}
			else if($product == '0')
			{
					$query 			=	$this->db->get();
			}

			else
			{
					$this->db->where('voucher_entry_detail.ved_itemid',$product);
					$query 			=	$this->db->get();
			}

					foreach($query->result() as $row)
			{
					$output 	.=	"<tr><td>".date("d-m-Y",strtotime($row->ved_date))."</td>"; 
					$output 	.=	"<td>".$row->ved_item."</td>";
					$output 	.=	"<td>".$row->ved_batch."</td>";
					$output 	.=	"<td>".$row->ved_price."</td>";
					$output 	.=	"<td>".$row->qty/$row->ved_uqty." ".$row->ved_unit." (".$row->free/$row->ved_uqty." ".$row->ved_unit.")</td>";
					$output 	.=	"<td>".$row->total."</td>";
					$output 	.=	"<td>".$row->sgst." (".$row->ved_sgstp."%)</td>";
					$output 	.=	"<td>".$row->cgst." (".$row->ved_cgstp."%)</td>";
					$output 	.=	"<td>".$row->grand_total."</td>";
					$output		.=	"</tr>";
					$sum_total 	 =	$sum_total + $row->total;
					$cgst_total  =	$cgst_total + $row->cgst;
					$sgst_total  =	$sgst_total + $row->sgst;
					$gsum_total  =	$gsum_total + $row->grand_total;
			}

					$output		.=	"</table><table class='table table-bordered'><thead><tr><th width='85%'>Total Purchase Amount</th><th>Rs. ".$sum_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total CGST</th><th>Rs. ".$cgst_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total SGST</th><th>Rs. ".$sgst_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total (Inc. GST)</th><th>Rs. ".$gsum_total."</th></tr></thead>"; 
			return $output;
		}

		if($r_type==0)
		{
			$temp  	= array();
    		$temp2 	= array();
    		$tep2 	= array();
    		$sum   	= array();

			$this->db->select('*');
			$this->db->from('balance');
			$this->db->order_by('balance_date','asc');
			$this->db->where('balance_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			$query                 =    $this->db->get();
    		$data                  =    $query->result_array();
    foreach($data as $key => $value)
    {   

        if(in_array($value['balance_date'],$temp)){

            $temp2[$value['balance_date']]['psdate'] 			=  $value['balance_date'];
            $temp2[$value['balance_date']]['purchase_sum']  	= $sum[$value['balance_date']]['purchase_sum'] + $this->getPurchase($value['balance_date']);
            $temp2[$value['balance_date']]['sales_sum']     	= $sum[$value['balance_date']]['sales_sum']    + $this->getSales($value['balance_date']);
            $temp2[$value['balance_date']]['expense']  			= $this->getExpense($value['balance_date']);
            $temp2[$value['balance_date']]['prBalance']  		= $sum[$value['balance_date']]['prBalance'] + $this->getPRBalance($value['balance_date']);
            $temp2[$value['balance_date']]['srBalance']     	= $sum[$value['balance_date']]['srBalance']    + $this->getSRBalance($value['balance_date']);
            $temp2[$value['balance_date']]['openingBalance']  	= $this->getOpeningBalance($value['balance_date']);
            $temp2[$value['balance_date']]['closingBalance']  	= $this->getClosingBalance($value['balance_date']);


        } else {


            $temp2[$value['balance_date']]['psdate'] 		=  $value['balance_date'];
            $temp2[$value['balance_date']]['purchase_sum']  = $this->getPurchase($value['balance_date']);
            $temp2[$value['balance_date']]['sales_sum']     = $this->getSales($value['balance_date']);
            $temp2[$value['balance_date']]['expense']  		= $this->getExpense($value['balance_date']);
            $temp2[$value['balance_date']]['prBalance']  	= $this->getPRBalance($value['balance_date']);
            $temp2[$value['balance_date']]['srBalance']  	= $this->getSRBalance($value['balance_date']);
            $temp2[$value['balance_date']]['openingBalance']= $this->getOpeningBalance($value['balance_date']);
            $temp2[$value['balance_date']]['closingBalance']= $this->getClosingBalance($value['balance_date']);

        } 

    }
		 return $temp2;
			
		}

		if($r_type==2)
		{

			$this->db->select('voucher_entry_detail.*,sum(ved_qty) as qty,sum(ved_total) as total,sum(ved_sgsta) as sgst,sum(ved_cgsta) as cgst,sum(ved_gtotal) as grand_total');
			$this->db->from('voucher_entry_detail');
			$this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid', 'inner');
			$this->db->join('product','product.pd_code = voucher_entry_detail.ved_itemid', 'inner');
			$this->db->group_by('voucher_entry_detail.ved_date');
			$this->db->group_by('voucher_entry_detail.ved_itemid');
			$this->db->group_by('voucher_entry_detail.ved_batch');
			$this->db->order_by('voucher_entry_detail.ved_date','desc');
			$this->db->where('ved_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			$this->db->where('voucher_entry.ve_type','si');
			$this->db->or_where('voucher_entry.ve_type','so');

			if($product == 'br')
			{
				$this->db->where('product.pd_brandid',$p_brand); 
				$query=$this->db->get();
			}
			else if($product == 'ty')
			{
				$this->db->where('product.pd_type',$p_type); 
				$query=$this->db->get();
			}
			else if($product == 'brty')
			{
				$this->db->where('product.pd_brandid',$p_brand);
				$this->db->where('product.pd_type',$p_type);
				$query=$this->db->get();
			}
			else if($product == '0')
			{
					$query 			=	$this->db->get();
			}

			else
			{
					$this->db->where('voucher_entry_detail.ved_itemid',$product);
					$query 			=	$this->db->get();
			}

			foreach($query->result() as $row)
			{
			 		$output 	.=	"<tr><td>".$row->ved_date."</td>"; 
					$output 	.=	"<td>".$row->ved_item."</td>";
					$output 	.=	"<td>".$row->ved_batch."</td>";
					$output 	.=	"<td>".$row->ved_price."</td>";
					$output 	.=	"<td>".$row->qty/$row->ved_uqty." ".$row->ved_unit."</td>";
					$output 	.=	"<td>".$row->total."</td>";
					$output 	.=	"<td>".$row->sgst." (".$row->ved_sgstp."%)</td>";
					$output 	.=	"<td>".$row->cgst." (".$row->ved_cgstp."%)</td>";
					$output 	.=	"<td>".$row->grand_total."</td>";
					$output		.=	"</tr>";
					$sum_total 	 =	$sum_total + $row->total;
					$cgst_total  =	$cgst_total + $row->cgst;
					$sgst_total  =	$sgst_total + $row->sgst;
					$gsum_total  =	$gsum_total + $row->grand_total;
			}

					$output		.=	"</table><table class='table table-bordered'><thead><tr><th width='85%'>Total Sale Amount</th><th>Rs. ".$sum_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total CGST</th><th>Rs. ".$cgst_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total SGST</th><th>Rs. ".$sgst_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total (Inc. GST)</th><th>Rs. ".$gsum_total."</th></tr></thead>"; 

		return $output;
			}

			if($r_type==3)
		{

			$this->db->select('voucher_entry_detail.*,sum(ved_qty) as qty,sum(ved_total) as total,sum(ved_sgsta) as sgst,sum(ved_cgsta) as cgst,sum(ved_gtotal) as grand_total');
			$this->db->from('voucher_entry_detail');
			$this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid', 'inner');
			$this->db->join('product','product.pd_code = voucher_entry_detail.ved_itemid', 'inner');
			$this->db->group_by('voucher_entry_detail.ved_date');
			$this->db->group_by('voucher_entry_detail.ved_itemid');
			$this->db->group_by('voucher_entry_detail.ved_batch');
			$this->db->order_by('voucher_entry_detail.ved_date','desc');
			$this->db->where('ved_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			$this->db->where('voucher_entry.ve_type','pr');

			if($product == 'br')
			{
				$this->db->where('product.pd_brandid',$p_brand); 
				$query=$this->db->get();
			}
			else if($product == 'ty')
			{
				$this->db->where('product.pd_type',$p_type); 
				$query=$this->db->get();
			}
			else if($product == 'brty')
			{
				$this->db->where('product.pd_brandid',$p_brand);
				$this->db->where('product.pd_type',$p_type);
				$query=$this->db->get();
			}
			else if($product == '0')
			{
					$query 			=	$this->db->get();
			}

			else
			{
					$this->db->where('voucher_entry_detail.ved_itemid',$product);
					$query 			=	$this->db->get();
			}

			foreach($query->result() as $row)
			{
			 		$output 	.=	"<tr><td>".$row->ved_date."</td>"; 
					$output 	.=	"<td>".$row->ved_item."</td>";
					$output 	.=	"<td>".$row->ved_batch."</td>";
					$output 	.=	"<td>".$row->ved_price."</td>";
					$output 	.=	"<td>".$row->qty/$row->ved_uqty." ".$row->ved_unit."</td>";
					$output 	.=	"<td>".$row->total."</td>";
					$output 	.=	"<td>".$row->sgst." (".$row->ved_sgstp."%)</td>";
					$output 	.=	"<td>".$row->cgst." (".$row->ved_cgstp."%)</td>";
					$output 	.=	"<td>".$row->grand_total."</td>";
					$output		.=	"</tr>";
					$sum_total 	 =	$sum_total + $row->total;
					$cgst_total  =	$cgst_total + $row->cgst;
					$sgst_total  =	$sgst_total + $row->sgst;
					$gsum_total  =	$gsum_total + $row->grand_total;
			}

					$output		.=	"</table><table class='table table-bordered'><thead><tr><th width='85%'>Total Purchase Return Amount</th><th>Rs. ".$sum_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total CGST</th><th>Rs. ".$cgst_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total SGST</th><th>Rs. ".$sgst_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total (Inc. GST)</th><th>Rs. ".$gsum_total."</th></tr></thead>"; 

		return $output;
			}

			if($r_type==4)
		{

			$this->db->select('voucher_entry_detail.*,sum(ved_qty) as qty,sum(ved_total) as total,sum(ved_sgsta) as sgst,sum(ved_cgsta) as cgst,sum(ved_gtotal) as grand_total');
			$this->db->from('voucher_entry_detail');
			$this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid', 'inner');
			$this->db->join('product','product.pd_code = voucher_entry_detail.ved_itemid', 'inner');
			$this->db->group_by('voucher_entry_detail.ved_date');
			$this->db->group_by('voucher_entry_detail.ved_itemid');
			$this->db->group_by('voucher_entry_detail.ved_batch');
			$this->db->order_by('voucher_entry_detail.ved_date','desc');
			$this->db->where('ved_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			$this->db->where('voucher_entry.ve_type','sr');

			if($product == 'br')
			{
				$this->db->where('product.pd_brandid',$p_brand); 
				$query=$this->db->get();
			}
			else if($product == 'ty')
			{
				$this->db->where('product.pd_type',$p_type); 
				$query=$this->db->get();
			}
			else if($product == 'brty')
			{
				$this->db->where('product.pd_brandid',$p_brand);
				$this->db->where('product.pd_type',$p_type);
				$query=$this->db->get();
			}
			else if($product == '0')
			{
					$query 			=	$this->db->get();
			}

			else
			{
					$this->db->where('voucher_entry_detail.ved_itemid',$product);
					$query 			=	$this->db->get();
			}

			foreach($query->result() as $row)
			{
			 		$output 	.=	"<tr><td>".$row->ved_date."</td>"; 
					$output 	.=	"<td>".$row->ved_item."</td>";
					$output 	.=	"<td>".$row->ved_batch."</td>";
					$output 	.=	"<td>".$row->ved_price."</td>";
					$output 	.=	"<td>".$row->qty/$row->ved_uqty." ".$row->ved_unit."</td>";
					$output 	.=	"<td>".$row->total."</td>";
					$output 	.=	"<td>".$row->sgst." (".$row->ved_sgstp."%)</td>";
					$output 	.=	"<td>".$row->cgst." (".$row->ved_cgstp."%)</td>";
					$output 	.=	"<td>".$row->grand_total."</td>";
					$output		.=	"</tr>";
					$sum_total 	 =	$sum_total + $row->total;
					$cgst_total  =	$cgst_total + $row->cgst;
					$sgst_total  =	$sgst_total + $row->sgst;
					$gsum_total  =	$gsum_total + $row->grand_total;
			}

					$output		.=	"</table><table class='table table-bordered'><thead><tr><th width='85%'>Total Sale Return Amount</th><th>Rs. ".$sum_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total CGST</th><th>Rs. ".$cgst_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total SGST</th><th>Rs. ".$sgst_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total (Inc. GST)</th><th>Rs. ".$gsum_total."</th></tr></thead>"; 

		return $output;
			}
}

function getPurchase($purchase_date)
{    
    $this->db->select_sum('ve_apaid');
    $this->db->from('voucher_entry');
    $this->db->where('ve_date',$purchase_date);
    $this->db->where('ve_type','p');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->row()->ve_apaid;
    	return $rowcount;
	}
}

function getSales($purchase_date)
{    
    $this->db->select_sum('ve_apaid');
    $this->db->from('voucher_entry');
    $this->db->where('ve_date',$purchase_date);
    $this->db->where('ve_type','si');
    $this->db->or_where('ve_type','so');
    $query = $this->db->get();
    $rowcount = $query->row()->ve_apaid;
    return $rowcount;
}


function getExpense($purchase_date)
{    
    $this->db->select('sum(ve_apaid) as amount,excategory.category');
    $this->db->from('voucher_entry');
    $this->db->join('excategory', 'excategory.id = voucher_entry.ve_customer', 'inner');
    $this->db->where('voucher_entry.ve_date', $purchase_date);
    $this->db->group_by('voucher_entry.ve_customer');
    $this->db->where('voucher_entry.ve_type','exp');
    $query = $this->db->get();
    $rowcount = $query->result_array();
    return $rowcount;
}

function getClosingBalance($purchase_date)
{
    $this->db->select('*');
    $this->db->from('balance');
    $this->db->where('balance_date',$purchase_date);
    $this->db->order_by("balance_id", "asc");
    $this->db->limit(1);
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	return  $query->row()->balance_amount; 
	}
}
function getOpeningBalance($purchase_date)
{
    $this->db->select('*');
    $this->db->from('balance');
    $this->db->where('balance_date <',$purchase_date);
    $this->db->order_by("balance_id", "desc");
    $this->db->limit(1);
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	return  $query->row()->balance_amount; 
	}
}

function getPRBalance($purchase_date)
{
    $this->db->select_sum('ve_apaid');
    $this->db->from('voucher_entry');
    $this->db->where('ve_date',$purchase_date);
    $this->db->where('ve_type','pr');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->row()->ve_apaid;
    	return $rowcount;
	}
}

function getSRBalance($purchase_date)
{
    $this->db->select_sum('ve_apaid');
    $this->db->from('voucher_entry');
    $this->db->where('ve_date',$purchase_date);
    $this->db->where('ve_type','sr');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->row()->ve_apaid;
    	return $rowcount;
	}
}


function get_account_main($fdate,$tdate,$product,$r_type)
	{
		$output 		=	"";
		$p_brand = $this->input->post('p_brand');
		$p_type  = $this->input->post('p_type');

		if($r_type==1)
		{
			$this->db->select('voucher_entry.*');
			$this->db->from('voucher_entry');
			$this->db->join('voucher_entry_detail','voucher_entry_detail.ved_veid = voucher_entry.ve_id', 'inner');
			$this->db->join('product','product.pd_code = voucher_entry_detail.ved_itemid', 'inner');
			$this->db->group_by('voucher_entry_detail.ved_veid');
			$this->db->where('ve_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			$this->db->where('voucher_entry.ve_type','p');
			if($product == 'br')
			{
				$this->db->where('product.pd_brandid',$p_brand); 
				$query=$this->db->get();
			}
			else if($product == 'ty')
			{
				$this->db->where('product.pd_type',$p_type); 
				$query=$this->db->get();
			}
			else if($product == 'brty')
			{
				$this->db->where('product.pd_brandid',$p_brand);
				$this->db->where('product.pd_type',$p_type);
				$query=$this->db->get();
			}
			else if($product == '0')
			{
					$query 			=	$this->db->get();
			}

			else
			{
					$this->db->where('voucher_entry_detail.ved_itemid',$product);
					$query 			=	$this->db->get();
			}	
			return $query->result_array();
		}

		if($r_type==2)
		{
			$this->db->select('voucher_entry.*');
			$this->db->from('voucher_entry');
			$this->db->join('voucher_entry_detail','voucher_entry_detail.ved_veid = voucher_entry.ve_id', 'inner');
			$this->db->join('product','product.pd_code = voucher_entry_detail.ved_itemid', 'inner');
			$this->db->group_by('voucher_entry_detail.ved_veid');
			$this->db->where('ve_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			$this->db->where('voucher_entry.ve_type','si');
			$this->db->or_where('voucher_entry.ve_type','so');
			if($product == 'br')
			{
				$this->db->where('product.pd_brandid',$p_brand); 
				$query=$this->db->get();
			}
			else if($product == 'ty')
			{
				$this->db->where('product.pd_type',$p_type); 
				$query=$this->db->get();
			}
			else if($product == 'brty')
			{
				$this->db->where('product.pd_brandid',$p_brand);
				$this->db->where('product.pd_type',$p_type);
				$query=$this->db->get();
			}
			else if($product == '0')
			{
					$query 			=	$this->db->get();
			}

			else
			{
					$this->db->where('voucher_entry_detail.ved_itemid',$product);
					$query 			=	$this->db->get();
			}	
			return $query->result_array();
		}

		if($r_type==3)
		{
			$this->db->select('voucher_entry.*');
			$this->db->from('voucher_entry');
			$this->db->join('voucher_entry_detail','voucher_entry_detail.ved_veid = voucher_entry.ve_id', 'inner');
			$this->db->join('product','product.pd_code = voucher_entry_detail.ved_itemid', 'inner');
			$this->db->group_by('voucher_entry_detail.ved_veid');
			$this->db->where('ve_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			$this->db->where('voucher_entry.ve_type','pr');
			if($product == 'br')
			{
				$this->db->where('product.pd_brandid',$p_brand); 
				$query=$this->db->get();
			}
			else if($product == 'ty')
			{
				$this->db->where('product.pd_type',$p_type); 
				$query=$this->db->get();
			}
			else if($product == 'brty')
			{
				$this->db->where('product.pd_brandid',$p_brand);
				$this->db->where('product.pd_type',$p_type);
				$query=$this->db->get();
			}
			else if($product == '0')
			{
					$query 			=	$this->db->get();
			}

			else
			{
					$this->db->where('voucher_entry_detail.ved_itemid',$product);
					$query 			=	$this->db->get();
			}	
			return $query->result_array();
		}

		if($r_type==4)
		{
			$this->db->select('voucher_entry.*');
			$this->db->from('voucher_entry');
			$this->db->join('voucher_entry_detail','voucher_entry_detail.ved_veid = voucher_entry.ve_id', 'inner');
			$this->db->join('product','product.pd_code = voucher_entry_detail.ved_itemid', 'inner');
			$this->db->group_by('voucher_entry_detail.ved_veid');
			$this->db->where('ve_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			$this->db->where('voucher_entry.ve_type','sr');
			if($product == 'br')
			{
				$this->db->where('product.pd_brandid',$p_brand); 
				$query=$this->db->get();
			}
			else if($product == 'ty')
			{
				$this->db->where('product.pd_type',$p_type); 
				$query=$this->db->get();
			}
			else if($product == 'brty')
			{
				$this->db->where('product.pd_brandid',$p_brand);
				$this->db->where('product.pd_type',$p_type);
				$query=$this->db->get();
			}
			else if($product == '0')
			{
					$query 			=	$this->db->get();
			}

			else
			{
					$this->db->where('voucher_entry_detail.ved_itemid',$product);
					$query 			=	$this->db->get();
			}	
			return $query->result_array();
		}
}



function list_small($product)
{
	$this->db->select('pd_product');
	$this->db->from('product');
	$this->db->where('pd_code',$product);
	$query 			=	$this->db->get();
	return $query->row()->pd_product;		
}

function get_account_detail($fdate,$tdate,$product,$r_type)
	{
		$output 		=	"";
		$sum_total 		=	0;
		$sgst_total 	=	0;
		$cgst_total 	=	0;
		$gsum_total 	=	0;
		$p_brand = $this->input->post('p_brand');
		$p_type = $this->input->post('p_type');

		if($r_type==1)
		{
			$this->db->select('voucher_entry_detail.*');
			$this->db->from('voucher_entry_detail');
			$this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid', 'inner');
			$this->db->join('product','product.pd_code = voucher_entry_detail.ved_itemid', 'inner');	
			$this->db->order_by('voucher_entry_detail.ved_date','desc');
			$this->db->where('ved_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			$this->db->where('voucher_entry.ve_type','p');
			if($product == 'br')
			{
				$this->db->where('product.pd_brandid',$p_brand); 
				$query=$this->db->get();
			}
			else if($product == 'ty')
			{
				$this->db->where('product.pd_type',$p_type); 
				$query=$this->db->get();
			}
			else if($product == 'brty')
			{
				$this->db->where('product.pd_brandid',$p_brand);
				$this->db->where('product.pd_type',$p_type);
				$query=$this->db->get();
			}
			else if($product == '0')
			{
					$query 			=	$this->db->get();
			}

			else
			{
					$this->db->where('voucher_entry_detail.ved_itemid',$product);
					$query 			=	$this->db->get();
			}

					foreach($query->result() as $row)
			{
					$output 	.=	"<tr><td>".$row->ved_date."</td>"; 
					$output 	.=	"<td>".$row->ved_item."</td>";
					$output 	.=	"<td>".$row->ved_batch."</td>";
					$output 	.=	"<td>".$row->ved_price."</td>";
					$output 	.=	"<td>".$row->ved_qty/$row->ved_uqty." ".$row->ved_unit." (".$row->ved_free/$row->ved_uqty." ".$row->ved_unit.")</td>";
					$output 	.=	"<td>".$row->ved_total."</td>";
					$output 	.=	"<td>".$row->ved_sgsta." (".$row->ved_sgstp."%)</td>";
					$output 	.=	"<td>".$row->ved_cgsta." (".$row->ved_cgstp."%)</td>";
					$output 	.=	"<td>".$row->ved_gtotal."</td>";
					$output		.=	"</tr>";
					$sum_total 	 =	$sum_total + $row->ved_total;
					$cgst_total  =	$cgst_total + $row->ved_cgsta;
					$sgst_total  =	$sgst_total + $row->ved_sgsta;
					$gsum_total  =	$gsum_total + $row->ved_gtotal;
			}

					$output		.=	"</table><table class='table table-bordered'><thead><tr><th width='85%'>Total Purchase Amount</th><th>Rs. ".$sum_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total CGST</th><th>Rs. ".$cgst_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total SGST</th><th>Rs. ".$sgst_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total (Inc. GST)</th><th>Rs. ".$gsum_total."</th></tr></thead>"; 
			return $output;
		}

		if($r_type==0)
		{
			$temp  	= array();
    		$temp2 	= array();
    		$tep2 	= array();
    		$sum   	= array();

			$this->db->select('*');
			$this->db->from('balance');
			$this->db->order_by('balance_date','asc');
			$this->db->where('balance_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			$query                 =    $this->db->get();
    		$data                  =    $query->result_array();
    foreach($data as $key => $value)
    {   

        if(in_array($value['balance_date'],$temp)){

            $temp2[$value['balance_date']]['psdate'] 			=  $value['balance_date'];
            $temp2[$value['balance_date']]['purchase_sum']  	= $sum[$value['balance_date']]['purchase_sum'] + $this->getPurchasedetail($value['balance_date']);
            $temp2[$value['balance_date']]['sales_sum']     	= $sum[$value['balance_date']]['sales_sum']    + $this->getSalesdetail($value['balance_date']);
             $temp2[$value['balance_date']]['preturn_sum']     	= $sum[$value['balance_date']]['sales_sum']    + $this->getPReturndetail($value['balance_date']);
             $temp2[$value['balance_date']]['sreturn_sum']     	= $sum[$value['balance_date']]['sales_sum']    + $this->getSReturndetail($value['balance_date']);

            $temp2[$value['balance_date']]['expense']  			= $this->getExpense($value['balance_date']);
            $temp2[$value['balance_date']]['openingBalance']  	= $this->getOpeningBalance($value['balance_date']);
            $temp2[$value['balance_date']]['closingBalance']  	= $this->getClosingBalance($value['balance_date']);


        } else {


            $temp2[$value['balance_date']]['psdate'] 			=  $value['balance_date'];
            $temp2[$value['balance_date']]['purchase_sum']  	= $this->getPurchasedetail($value['balance_date']);
            $temp2[$value['balance_date']]['sales_sum']     	= $this->getSalesdetail($value['balance_date']);
            $temp2[$value['balance_date']]['preturn_sum']     	= $this->getPReturndetail($value['balance_date']);
            $temp2[$value['balance_date']]['sreturn_sum']     	= $this->getSReturndetail($value['balance_date']);

            $temp2[$value['balance_date']]['expense']  			= $this->getExpense($value['balance_date']);
            $temp2[$value['balance_date']]['openingBalance']  	= $this->getOpeningBalance($value['balance_date']);
            $temp2[$value['balance_date']]['closingBalance']  	= $this->getClosingBalance($value['balance_date']);

        } 

    }
		 return $temp2;
			
		}

		if($r_type==2)
		{
			$this->db->select('voucher_entry_detail.*');
			$this->db->from('voucher_entry_detail');
			$this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid', 'inner');
			$this->db->join('product','product.pd_code = voucher_entry_detail.ved_itemid', 'inner');	
			$this->db->order_by('voucher_entry_detail.ved_date','desc');
			$this->db->where('ved_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			$this->db->where('voucher_entry.ve_type','si');
			$this->db->or_where('voucher_entry.ve_type','so');
			if($product == 'br')
			{
				$this->db->where('product.pd_brandid',$p_brand); 
				$query=$this->db->get();
			}
			else if($product == 'ty')
			{
				$this->db->where('product.pd_type',$p_type); 
				$query=$this->db->get();
			}
			else if($product == 'brty')
			{
				$this->db->where('product.pd_brandid',$p_brand);
				$this->db->where('product.pd_type',$p_type);
				$query=$this->db->get();
			}
			else if($product == '0')
			{
					$query 			=	$this->db->get();
			}

			else
			{
					$this->db->where('voucher_entry_detail.ved_itemid',$product);
					$query 			=	$this->db->get();
			}

					foreach($query->result() as $row)
			{
				if($row->ved_unit!="No.s")
				{
					$ved_qty  = $row->ved_qty/$row->ved_uqty;
				}
				else
				{
					$ved_qty  = $row->ved_qty;
				}

					$output 	.=	"<tr><td>".$row->ved_date."</td>"; 
					$output 	.=	"<td>".$row->ved_item."</td>";
					$output 	.=	"<td>".$row->ved_batch."</td>";
					$output 	.=	"<td>".$row->ved_price."</td>";
					$output 	.=	"<td>".$ved_qty." ".$row->ved_unit."</td>";
					$output 	.=	"<td>".$row->ved_total."</td>";
					$output 	.=	"<td>".$row->ved_sgsta." (".$row->ved_sgstp."%)</td>";
					$output 	.=	"<td>".$row->ved_cgsta." (".$row->ved_cgstp."%)</td>";
					$output 	.=	"<td>".$row->ved_gtotal."</td>";
					$output		.=	"</tr>";
					$sum_total 	 =	$sum_total + $row->ved_total;
					$cgst_total  =	$cgst_total + $row->ved_cgsta;
					$sgst_total  =	$sgst_total + $row->ved_sgsta;
					$gsum_total  =	$gsum_total + $row->ved_gtotal;
			}

					$output		.=	"</table><table class='table table-bordered'><thead><tr><th width='85%'>Total Sale Amount</th><th>Rs. ".$sum_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total CGST</th><th>Rs. ".$cgst_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total SGST</th><th>Rs. ".$sgst_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total (Inc. GST)</th><th>Rs. ".$gsum_total."</th></tr></thead>"; 
			return $output;
		}

		if($r_type==3)
		{
			$this->db->select('voucher_entry_detail.*');
			$this->db->from('voucher_entry_detail');
			$this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid', 'inner');
			$this->db->join('product','product.pd_code = voucher_entry_detail.ved_itemid', 'inner');	
			$this->db->order_by('voucher_entry_detail.ved_date','desc');
			$this->db->where('ved_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			$this->db->where('voucher_entry.ve_type','pr');
			if($product == 'br')
			{
				$this->db->where('product.pd_brandid',$p_brand); 
				$query=$this->db->get();
			}
			else if($product == 'ty')
			{
				$this->db->where('product.pd_type',$p_type); 
				$query=$this->db->get();
			}
			else if($product == 'brty')
			{
				$this->db->where('product.pd_brandid',$p_brand);
				$this->db->where('product.pd_type',$p_type);
				$query=$this->db->get();
			}
			else if($product == '0')
			{
					$query 			=	$this->db->get();
			}

			else
			{
					$this->db->where('voucher_entry_detail.ved_itemid',$product);
					$query 			=	$this->db->get();
			}

					foreach($query->result() as $row)
			{
					$output 	.=	"<tr><td>".$row->ved_date."</td>"; 
					$output 	.=	"<td>".$row->ved_item."</td>";
					$output 	.=	"<td>".$row->ved_batch."</td>";
					$output 	.=	"<td>".$row->ved_price."</td>";
					$output 	.=	"<td>".$row->ved_qty/$row->ved_uqty." ".$row->ved_unit."</td>";
					$output 	.=	"<td>".$row->ved_total."</td>";
					$output 	.=	"<td>".$row->ved_sgsta." (".$row->ved_sgstp."%)</td>";
					$output 	.=	"<td>".$row->ved_cgsta." (".$row->ved_cgstp."%)</td>";
					$output 	.=	"<td>".$row->ved_gtotal."</td>";
					$output		.=	"</tr>";
					$sum_total 	 =	$sum_total + $row->ved_total;
					$cgst_total  =	$cgst_total + $row->ved_cgsta;
					$sgst_total  =	$sgst_total + $row->ved_sgsta;
					$gsum_total  =	$gsum_total + $row->ved_gtotal;
			}

					$output		.=	"</table><table class='table table-bordered'><thead><tr><th width='85%'>Total Purchase Return Amount</th><th>Rs. ".$sum_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total CGST</th><th>Rs. ".$cgst_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total SGST</th><th>Rs. ".$sgst_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total (Inc. GST)</th><th>Rs. ".$gsum_total."</th></tr></thead>"; 
			return $output;
		}

		if($r_type==4)
		{
			$this->db->select('voucher_entry_detail.*');
			$this->db->from('voucher_entry_detail');
			$this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid', 'inner');
			$this->db->join('product','product.pd_code = voucher_entry_detail.ved_itemid', 'inner');	
			$this->db->order_by('voucher_entry_detail.ved_date','desc');
			$this->db->where('ved_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			$this->db->where('voucher_entry.ve_type','sr');
			if($product == 'br')
			{
				$this->db->where('product.pd_brandid',$p_brand); 
				$query=$this->db->get();
			}
			else if($product == 'ty')
			{
				$this->db->where('product.pd_type',$p_type); 
				$query=$this->db->get();
			}
			else if($product == 'brty')
			{
				$this->db->where('product.pd_brandid',$p_brand);
				$this->db->where('product.pd_type',$p_type);
				$query=$this->db->get();
			}
			else if($product == '0')
			{
					$query 			=	$this->db->get();
			}

			else
			{
					$this->db->where('voucher_entry_detail.ved_itemid',$product);
					$query 			=	$this->db->get();
			}

					foreach($query->result() as $row)
			{
				if($row->ved_unit!="No.s")
				{
					$ved_qty  = $row->ved_qty/$row->ved_uqty;
				}
				else
				{
					$ved_qty  = $row->ved_qty;
				}

					$output 	.=	"<tr><td>".$row->ved_date."</td>"; 
					$output 	.=	"<td>".$row->ved_item."</td>";
					$output 	.=	"<td>".$row->ved_batch."</td>";
					$output 	.=	"<td>".$row->ved_price."</td>";
					$output 	.=	"<td>".$ved_qty." ".$row->ved_unit."</td>";
					$output 	.=	"<td>".$row->ved_total."</td>";
					$output 	.=	"<td>".$row->ved_sgsta." (".$row->ved_sgstp."%)</td>";
					$output 	.=	"<td>".$row->ved_cgsta." (".$row->ved_cgstp."%)</td>";
					$output 	.=	"<td>".$row->ved_gtotal."</td>";
					$output		.=	"</tr>";
					$sum_total 	 =	$sum_total + $row->ved_total;
					$cgst_total  =	$cgst_total + $row->ved_cgsta;
					$sgst_total  =	$sgst_total + $row->ved_sgsta;
					$gsum_total  =	$gsum_total + $row->ved_gtotal;
			}

					$output		.=	"</table><table class='table table-bordered'><thead><tr><th width='85%'>Total Sale Amount</th><th>Rs. ".$sum_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total CGST</th><th>Rs. ".$cgst_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total SGST</th><th>Rs. ".$sgst_total."</th></tr></thead>
					<thead><tr><th width='85%'>Total (Inc. GST)</th><th>Rs. ".$gsum_total."</th></tr></thead>"; 
			return $output;
		}
}

function getPurchasedetail($purchase_date)
{    
    $this->db->select('ved_item,ved_batch,sum(ved_gtotal) as pamount');
    $this->db->from('voucher_entry_detail');
    $this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid', 'inner');
    $this->db->group_by('voucher_entry_detail.ved_veid');
    $this->db->group_by('voucher_entry_detail.ved_item');
    $this->db->group_by('voucher_entry_detail.ved_batch');
    $this->db->where('voucher_entry_detail.ved_date',$purchase_date);
    $this->db->where('voucher_entry.ve_type','p');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->result_array();
    	return $rowcount;
	}
}

function getSalesdetail($purchase_date)
{    
    $this->db->select('voucher_entry_detail.ved_item,voucher_entry_detail.ved_batch,sum(ved_gtotal) as samount');
    $this->db->from('voucher_entry_detail');
    $this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid', 'inner');
    $this->db->group_by('voucher_entry_detail.ved_veid');
    $this->db->group_by('voucher_entry_detail.ved_item');
    $this->db->group_by('voucher_entry_detail.ved_batch');
    $this->db->where('voucher_entry_detail.ved_date',$purchase_date);
    $this->db->where('voucher_entry.ve_type','si');
    $this->db->or_where('voucher_entry.ve_type','so');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->result_array();
    	return $rowcount;
	}
}
function getPReturndetail($purchase_date)
{    
    $this->db->select('voucher_entry_detail.ved_item,voucher_entry_detail.ved_batch,sum(ved_gtotal) as pramount');
    $this->db->from('voucher_entry_detail');
    $this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid', 'inner');
    $this->db->group_by('voucher_entry_detail.ved_veid');
    $this->db->group_by('voucher_entry_detail.ved_item');
    $this->db->group_by('voucher_entry_detail.ved_batch');
    $this->db->where('voucher_entry_detail.ved_date',$purchase_date);
    $this->db->where('voucher_entry.ve_type','pr');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->result_array();
    	return $rowcount;
	}
}
function getSReturndetail($purchase_date)
{    
    $this->db->select('voucher_entry_detail.ved_item,voucher_entry_detail.ved_batch,sum(ved_gtotal) as sramount');
    $this->db->from('voucher_entry_detail');
    $this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid', 'inner');
    $this->db->group_by('voucher_entry_detail.ved_veid');
    $this->db->group_by('voucher_entry_detail.ved_item');
    $this->db->group_by('voucher_entry_detail.ved_batch');
    $this->db->where('voucher_entry_detail.ved_date',$purchase_date);
    $this->db->where('voucher_entry.ve_type','sr');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->result_array();
    	return $rowcount;
	}
}

public function getCompany(){
		$this->db->select('*');
		$this->db->from('company');
		$query=$this->db->get();
		return $query->result_array();
	}

	public function getCgst(){
		$this->db->select('pd_cgst');
		$this->db->from('product');
		$this->db->group_by('pd_cgst');
		$query=$this->db->get();
		return $query->result_array();
	}
	public function getSgst(){
		$this->db->select('pd_sgst');
		$this->db->from('product');
		$this->db->group_by('pd_sgst');
		$query=$this->db->get();
		return $query->result_array();
	}

	


}