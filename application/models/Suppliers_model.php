<?php

class Suppliers_model extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('*');
		$this->db->from('suppliers');
		$this->db->order_by('sp_id','asc');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{
			$sl_no++;

			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->sp_vendor."</td>";
			$output 	.=	"<td>".$row->sp_phone."</td>";
		    $output 	.=	"<td>".$row->sp_email."</td>";

			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."suppliers/view/".$row->sp_id."'class='btn btn-primary view-btn-edit' title='View'><i class='fa fa-eye'></i></a>
			 <a href='".$this->config->item('admin_url')."suppliers/edit/".$row->sp_id."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
			  <a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."suppliers/delete/".$row->sp_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}

  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('suppliers');
		$this->db->where('sp_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}


}