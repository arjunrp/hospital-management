<?php

class Company_model extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('*');
		$this->db->from('company');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{
			$sl_no++;
			if($row->status == '1')
			{
				$image = "<i class='fa fa-check'></i>";
				$status = 0;
			}
			else
			{
				$image = "<i class='fa fa-ban'></i>";
				$status = 1;
			}
			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->company."</td>";
		    $output 	.=	"<td>".$row->address."</td>";
		    $output 	.=	"<td>".$row->street."</td>";
		    $output 	.=	"<td>".$row->city."</td>";
	        $output 	.=	"<td>".$row->zip."</td>";
	        $output 	.=	"<td>".$row->country."</td>";
	        $output 	.=	"<td>".$row->state."</td>";
			$output 	.=	"<td>".$row->phone."</td>";
		    $output 	.=	"<td>".$row->email."</td>";
		    $output 	.=	"<td>".$row->fax."</td>";
		    $output 	.=	"<td>".$row->website."</td>";
			 
			 
			// $output 	.=	"<td><a href='".$this->config->item('admin_url')."vendor/active/".$row->id."/".$status."'>".$image."</a></td>";
			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."company/view/".$row->id."'class='btn btn-success view-btn-edit' title='View'><i class='fa fa-search'></i></a>
			 <a href='".$this->config->item('admin_url')."company/edit/".$row->id."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
			 <a href='".$this->config->item('admin_url')."company/delete/".$row->id."'class='btn btn-danger' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}


	function list_small($id)
	
	{
		$this->db->select('*');
		$this->db->from('company');
		$this->db->where('id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}


	function get_one_banners()
	{
		$this->db->select('*');
		$this->db->from('company');
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}

		function get_country()
	{
			$this->db->select('*');
			$this->db->from('countries'); 
			$query=$this->db->get();
			return $query->result_array();
	}

function get_state()
	{
			$this->db->select('*');
			$this->db->from('states'); 
			$query=$this->db->get();
			return $query->result_array();
	}
function get_city()
	{
			$this->db->select('*');
			$this->db->from('cities'); 
			$query=$this->db->get();
			return $query->result_array();
	}




	function get_states($states)
	{
			$query=$this->db->get_where('states',array('country_id'=>$states));
			return $query->result_array();
	}
	function get_citys($citys)
	{
			$query=$this->db->get_where('cities',array('state_id'=>$citys));
			return $query->result_array();
	}

	

function purchase_count()
{
            $tdate=date("Y-m-d");			
			$this->db->select('*');
			$this->db->from('purchase'); 
			$this->db->where('purchase_date',$tdate);
			$query=$this->db->get();
			$count=$query->num_rows();
			return $count;
}
function sale_count()
{
            $tdate=date("Y-m-d");			
			$this->db->select('*');
			$this->db->from('sales'); 
			$this->db->where('sale_date',$tdate);
			$query=$this->db->get();
			$count=$query->num_rows();
			return $count;
}
}