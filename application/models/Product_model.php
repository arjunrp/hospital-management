<?php

class Product_model extends CI_Model 
{
	function list_all()
	{

		// $this->db->select('product.*,brand.br_brand,mcontents.mc_content');
		// $this->db->from('product');
		// $this->db->join('mcontents', 'mcontents.mc_id = product.pd_contentid', 'inner'); 
		// // $this->db->join('suppliers', 'suppliers.sp_id = product.pd_supplier', 'inner');
		// $this->db->join('brand', 'brand.br_id = product.pd_brandid', 'inner');  
		// $this->db->order_by('product.pd_id','asc'); 
		// if($row->pd_type=="m") { $pd_type = "Medical"; }
		// 	if($row->pd_type=="g") { $pd_type = "General"; }  
		// 	$e = explode(",", $row->pd_supplier);
		// 	foreach ($e as $e) {
		// 	$this->db->select('sp_vendor');
		// 	$this->db->from('suppliers');
		// 	$this->db->where_in('sp_id',$e);
		// 	$query1 =$this->db->get();
		// 	$pd_supplier[]=$query1->row()->sp_vendor;
		// 	$pdsupplier= implode(',',$pd_supplier);
  //           } 
  //           			unset($pd_supplier);

			// $output 	.=	"<td>".$row->br_brand."</td>";
			// $output 	.=	"<td>".$row->mc_content."</td>";
			// $output 	.=	"<td>".$pdsupplier."</td>";
			// $output 	.=	"<td>".$row->pd_price."</td>";

		$output 		=	"";
		$sl_no 			=	0;
		$e 				=	"";
		$pd_supplier 	=	"";
		$end 			=	"";
		$pdsupplier 	=	"";
		$pd_type		=	"";

		$this->db->select('*');
		$this->db->from('product');
		$this->db->order_by('pd_code','asc'); 
		$query=$this->db->get();
		foreach($query->result() as $row)
		{
			if($row->pd_type=="m") { $pd_type = "Medical"; }
			if($row->pd_type=="g") { $pd_type = "General"; } 
			
			$sl_no++;
			$output 	.=	"<tr><td>".$row->pd_code."</td>";
			$output 	.=	"<td>".$row->pd_product."</td>";
			$output 	.=	"<td>".$pd_type."</td>";
			 

			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."product/view/".$row->pd_code."'class='btn btn-primary view-btn-edit' title='View'><i class='fa fa-eye'></i></a>
			 <a href='".$this->config->item('admin_url')."product/edit/".$row->pd_code."' class='btn btn-info view-btn-edit' title='Edit' ><i class='fa fa-pencil-square-o'></i></a> 
			 <a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."product/delete/".$row->pd_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	}

     public function get_product_code()
     {

		$this->db->select('pd_code');
		$this->db->from('product');
		$this->db->order_by('pd_id','desc');
		$this->db->where('pd_status','1');
		$this->db->limit(1);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return 'PRD_001';
		}
		else
		{
			$result = explode('_', $query->row()->pd_code);
			$end2   = $result[sizeof($result)-1];
			$end2++;
			return "PRD_".$end2;
		}
	}

	public function getContent(){
		$this->db->select('*');
		$this->db->from('mcontents');
		$this->db->where('mc_status','1');
		$query 			=	$this->db->get();
		return $query->result_array();
		 }

	public function getSupplier(){
		$this->db->select('*');
		$this->db->from('suppliers');
		$this->db->where('sp_status','1');
		$query 			=	$this->db->get();
		return $query->result_array();
		 }

	public function getBrand(){
		$this->db->select('*');
		$this->db->from('brand');
		$this->db->where('br_status','1');
		$query 			=	$this->db->get();
		return $query->result_array();
		 }
  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('product');
		$this->db->where('pd_code',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}

}