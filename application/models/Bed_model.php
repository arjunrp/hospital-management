<?php

class Bed_model extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('bed.*,room.rm_no');
		$this->db->from('bed');
		$this->db->join('room','room.rm_id = bed.bd_rmid', 'inner'); 
		$this->db->order_by('bed.bd_id','asc');
		$this->db->where('bed.bd_status','1');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{
			$sl_no++;
			$output 	.=	"<tr><td>".$row->bd_no."</td>";
			$output 	.=	"<td>".$row->rm_no."</td>";

			$output 	.=	"<td class='btn-group  btn-group-xs' ><a href='".$this->config->item('admin_url')."bed/edit/".$row->bd_id."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
			<a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."bed/delete/".$row->bd_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	}

  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('bed');
		$this->db->where('bd_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}

	function get_bed_no()
	{
		$this->db->select('bd_no');
		$this->db->from('bed');
		$this->db->where('bd_status',1);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return '0';
		}
		else
		{
			$result 	= $query->row()->bd_no;
			$result++;
			return $result;
		}
	}

	function get_room_no()
	{
		$this->db->select('*');
		$this->db->from('room');
		$this->db->where('rm_status',1);
		$query 			=	$this->db->get();
		return $query->result_array();
	}


	public function getCompany(){
		$this->db->select('*');
		$this->db->from('company');
		$query=$this->db->get();
		return $query->result_array();
	}


}