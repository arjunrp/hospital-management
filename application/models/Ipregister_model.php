<?php

class Ipregister_model extends CI_Model 
{
	function list_all()
	{
		$output	=	"";
		// $bk_date = date("Y-m-d");

		$this->db->select('ip.*,users.u_name,patient.p_title,patient.p_name,patient.p_phone,department.dp_department,bed.bd_no,room.rm_no');
		$this->db->from('ip');
		$this->db->join('users','users.u_emp_id = ip.ip_doctor', 'inner');
		$this->db->join('department','department.dp_id = ip.ip_department', 'inner');
		$this->db->join('patient','patient.p_mrd_no = ip.ip_mrd', 'inner');
		$this->db->join('room_shift','room_shift.rs_ip = ip.ip_ipno', 'inner');
		$this->db->join('room','room.rm_id = room_shift.rs_rmno', 'inner');
		$this->db->join('bed','bed.bd_id = room_shift.rs_bdno', 'inner');
		$this->db->order_by('ip.ip_ipno','asc');
		$this->db->where('ip.ip_dis','1');
		$this->db->where('room_shift.rs_dis','0000-00-00');
		$query 			=	$this->db->get();
		if($query->num_rows()>0) {
		foreach($query->result() as $row)
		{
			$output 	.=	"<tr><td>".$row->ip_ipno."</td>";
			$output 	.=	"<td>".$row->ip_date."</td>";
			$output 	.=	"<td>".$row->u_name."</td>";
			$output 	.=	"<td>".$row->dp_department."</td>";
			$output 	.=	"<td>".$row->ip_mrd."</td>";
			$output 	.=	"<td>".$row->p_title." ".$row->p_name."</td>";
			$output 	.=	"<td>".$row->p_phone."</td>";
			$output 	.=	"<td>".$row->rm_no." (".$row->bd_no.")</td>";


			$output 	.=	"<td style='text-align:center' class='btn-group-xs' ><a href='".$this->config->item('admin_url')."room_shift/add/".$row->ip_ipno."'class='btn btn-primary view-btn-edit' title='Room Shifting'><i class='fa fa-exchange'></i></a></td>";
			$output 	.=	"<td style='text-align:center' class='btn-group  btn-group-xs' >
			<a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."ipregister/delete/".$row->ip_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	} 
	else { return false; }
	} 

	function today_ip()
	{
		$output	=	"";
		// $bk_date = date("Y-m-d");
// ,users.u_name,patient.p_title,patient.p_name,patient.p_phone,department.dp_department
		
		$this->db->select('ip.*,users.u_name,patient.p_title,patient.p_name,patient.p_phone,department.dp_department');
		$this->db->from('ip');
		$this->db->join('users','users.u_emp_id = ip.ip_doctor', 'inner');
		$this->db->join('department','department.dp_id = ip.ip_department', 'inner');
		$this->db->join('patient','patient.p_mrd_no = ip.ip_mrd', 'inner');
		$this->db->order_by('ip.ip_ipno','asc');
		// $this->db->where('ip.ip_date',$bk_date);
		$this->db->where('ip.ip_dis','1');
		$query 			=	$this->db->get();
		if($query->num_rows()>0) {
		foreach($query->result() as $row)
		{
			$output 	.=	"<tr><td>".$row->ip_ipno."</td>";
			$output 	.=	"<td>".$row->ip_date."</td>";
			$output 	.=	"<td>".$row->u_name."</td>";
			$output 	.=	"<td>".$row->dp_department."</td>";
			$output 	.=	"<td>".$row->ip_mrd."</td>";
			$output 	.=	"<td>".$row->p_title." ".$row->p_name."</td>";
			$output 	.=	"<td>".$row->p_phone."</td>";


			$output 	.=	"<td style='text-align:center' class='btn-group-xs' ><a href='".$this->config->item('admin_url')."room_shift/add/".$row->ip_ipno."'class='btn btn-primary view-btn-edit' title='Room Shifting'><i class='fa fa-exchange'></i></a></td>";
			$output 	.=	"<td style='text-align:center' class='btn-group  btn-group-xs' ><a href='".$this->config->item('admin_url')."ipregister/edit/".$row->ip_id."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
			<a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."ipregister/delete/".$row->ip_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	} 
	else { return false; }
		
	}

  
	function get_one_banner($id)
	{
		$this->db->select('ip.*,department.dp_department,department.dp_id,patient.p_name,patient.p_phone,patient.p_age,patient.p_sex,patient.p_address,patient.p_street,patient.p_guardian');
		$this->db->from('ip');
		$this->db->join('patient','patient.p_mrd_no = ip.ip_mrd', 'inner');
		$this->db->join('department','department.dp_id = ip.ip_department', 'inner');
		$this->db->where('ip.ip_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}


	function getDoctors($ip_department)
	{
		$this->db->select('users.*');
    	$this->db->from('users');
    	$this->db->join('user_type','user_type.ut_id = users.u_type', 'inner'); 
    	$this->db->where('users.u_department',$ip_department);
    	$this->db->where('user_type.ut_user_type','Doctor');
		$query 			=	$this->db->get();
		return $query->result_array();
	}

	function get_patient($op_mrd)
	{
		$this->db->select('*');
		$this->db->from('patient');
		$this->db->where('p_mrd_no',$op_mrd);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}


	function get_ip_no()
	{

		$this->db->select('CAST(SUBSTRING(ip_ipno, 4, length(ip_ipno)-3) as UNSIGNED) as ip_ipno');
      	$this->db->from('ip');
      	$this->db->order_by('ip_ipno','desc');
      	$this->db->limit(1);
      	$query      = $this->db->get();
      	if($query->num_rows()==0)
      	{
        	$data = 'IP_001';
      	}
      	else
      	{
        	$end2   = $query->row()->ip_ipno;
        	$end2++;
        	$data = "IP_".$end2;
      	}
    	return $data;

		// $this->db->select('ip_ipno');
		// $this->db->from('ip');
		// $this->db->order_by('ip_ipno','desc');
		// $this->db->limit(1);
		// $query 			=	$this->db->get();
		// if($query->num_rows()==0)
		// {
		// 	return '1';
		// }
		// else
		// {
		// 	$result 	= $query->row()->ip_ipno;
		// 	$result++;
		// 	return $result;
		// }
	}

	function get_docfee($op_doctor)
	{
		$this->db->select('u_fees');
		$this->db->from('users');
		$this->db->where('u_emp_id',$op_doctor);
		$query 			=	$this->db->get();
		return $query->row()->u_fees;
	}

	function today_book()
	{
		$output	=	"";
		$shift 	=	"";
		$bk_date = date("Y-m-d");

		$this->db->select('booking.*,users.u_department,users.u_fees');
		$this->db->from('booking');
		$this->db->join('users','users.u_emp_id = booking.bk_doc', 'inner');
		$this->db->order_by('booking.bk_no','asc');
		$this->db->where('booking.bk_today',$bk_date);
		$query 			=	$this->db->get();
		return $query->result_array();
	}

	public function getDepartment(){
		$this->db->select('*');
		$this->db->from('department');
		$this->db->where('dp_status',1);
		$this->db->where('dp_type','t');
		$query=$this->db->get();
		return $query->result_array();
	}

}