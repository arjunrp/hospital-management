<?php

class Users_model extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('users.*,designation.dg_designation,user_type.ut_user_type,department.dp_department');
		$this->db->from('users');
		$this->db->join('designation','designation.dg_id = users.u_designation', 'inner'); 
		$this->db->join('user_type','user_type.ut_id = users.u_type', 'inner'); 
		$this->db->join('department','department.dp_id = users.u_department', 'inner'); 
		$this->db->order_by('users.u_emp_id','asc');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{
			$sl_no++;
			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->u_emp_id."</td>";
			$output 	.=	"<td>".$row->u_name."</td>";
		    $output 	.=	"<td>".$row->ut_user_type."</td>";
		    $output 	.=	"<td>".$row->dg_designation."</td>";
		    $output 	.=	"<td>".$row->dp_department."</td>";
			 $output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."users/view/".$row->u_emp_id."'class='btn btn-primary view-btn-edit' title='View'><i class='fa fa-eye'></i></a>
			 <a href='".$this->config->item('admin_url')."users/edit/".$row->u_emp_id."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
			 <a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."users/delete/".$row->u_emp_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>";
			 
			$output		.=	"</tr>";
		}
		return $output;
	}





function list_small($product)
{
	$this->db->select('product');
	$this->db->from('product');
	$this->db->where('id',$product);
	$query 			=	$this->db->get();
	return $query->row()->product;		
}

  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('u_emp_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}

	public function getU_type(){
		$this->db->select('*');
		$this->db->from('user_type');
		$this->db->where('ut_status',1);
		$query=$this->db->get();
		return $query->result_array();
	}
	public function getDesignation(){
		$this->db->select('*');
		$this->db->from('designation');
		$this->db->where('dg_status',1);
		$query=$this->db->get();
		return $query->result_array();
	}
	public function getDepartment(){
		$this->db->select('*');
		$this->db->from('department');
		$this->db->where('dp_status',1);
		$query=$this->db->get();
		return $query->result_array();
	}

	public function getCompany(){
		$this->db->select('*');
		$this->db->from('company');
		$query=$this->db->get();
		return $query->result_array();
	}

	function get_emp_id()
	{
	$this->db->select('u_emp_id');
	$this->db->from('users');
	$this->db->order_by('u_emp_id','desc');
	$this->db->where('u_status','1');
	$this->db->limit(1);
	$query 			=	$this->db->get();
	if($query->num_rows()==0)
	{
		return '1';
	}
	else
	{
		$result 	= $query->row()->u_emp_id;
		$result++;
		return $result;
	}
}

	public function getUsertype($user_type){
		$this->db->select('ut_user_type');
        $this->db->from('user_type'); 
        $this->db->where('ut_id',$user_type);
        $query=$this->db->get();
        if($query->num_rows()==0)
	   {
		return '';   
	   }else{
	   	return $query->row()->ut_user_type;
	   }
	}

}