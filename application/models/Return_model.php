<?php

class Return_model extends CI_Model {

	function __construct() {
		parent::__construct();

		$this->load->database();
	}


	function getSReturn()
	{
		$output 		=	"";
		$status 	 	=	"1";
		$this->db->select('voucher_entry.*');
    	$this->db->from('voucher_entry');
    	// $this->db->join('patient','patient.p_mrd_no = voucher_entry.ve_mrd', 'left');
    	$this->db->order_by('voucher_entry.ve_vno','desc');
    	$this->db->where('voucher_entry.ve_type','sr');
    	$query=$this->db->get();
		foreach($query->result() as $row)
		{

			$output 	.=	"<tr><td>".$row->ve_vno."</td>";
			$output 	.=	"<td>".date("d-m-Y",strtotime($row->ve_date))."</td>";
			$output 	.=	"<td>".$row->ve_mrd."</td>";
			$output 	.=	"<td>".$row->ve_patient."</td>";
			$output 	.=	"<td>".$row->ve_phone."</td>";
			 

			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."preturn/return_SView/".$row->ve_id."'class='btn btn-primary view-btn-edit' title='View'><i class='fa fa-eye'></i></a> 
			
			 <a class='".$status." btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."preturn/sdelete/".$row->ve_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	}

	function getPReturn()
	{
		$output 		=	"";
		$status 	 	=	"1";
		$this->db->select('voucher_entry.*,suppliers.sp_id,suppliers.sp_vendor,suppliers.sp_phone');
    	$this->db->from('voucher_entry');
    	$this->db->join('suppliers','suppliers.sp_id = voucher_entry.ve_supplier', 'left');
    	$this->db->order_by('voucher_entry.ve_vno','desc');
    	$this->db->where('voucher_entry.ve_type','pr');
    	$query=$this->db->get();
		foreach($query->result() as $row)
		{

			$output 	.=	"<tr><td>".$row->ve_vno."</td>";
			$output 	.=	"<td>".date("d-m-Y",strtotime($row->ve_date))."</td>";
			$output 	.=	"<td>".$row->sp_id."</td>";
			$output 	.=	"<td>".$row->sp_vendor."</td>";
			$output 	.=	"<td>".$row->sp_phone."</td>";
			 

			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."preturn/return_PView/".$row->ve_id."'class='btn btn-primary view-btn-edit' title='View'><i class='fa fa-eye'></i></a> 

			 <a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."preturn/pdelete/".$row->ve_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	}
	
	public function set_preturn_stock($stockpid,$purchase_unit_qty,$ve_date,$ved_batch,$ve_customer){
		
		$this->db->select('*');
		$this->db->from('stock');
		$this->db->where('stock_product_id', $stockpid);
		$this->db->where('stock_dept',$ve_customer);
		$this->db->where('stock_batch',$ved_batch);
		$this->db->order_by("stock_id", "desc");
		$this->db->where('stock_status','standby');
		$this->db->limit(1);
		$result  		=    $this->db->get();
		$number_of_rows = $result->num_rows();

		if($number_of_rows != 0){
			$sid						=	$result->row()->stock_id;
			$sqty						=	$result->row()->stock_qty;
			$sdate 						=	$result->row()->stock_date;
			$data['stock_qty'] 			=	$sqty - $purchase_unit_qty;
			$data['stock_date']			=	$ve_date;	
			$data['stock_product_id']	=	$stockpid;
			$data['stock_batch']		=	$ved_batch;
			$data['stock_dept']			=	$ve_customer;
			$data['stock_status']		=	"standby";	
			$data['created_at']			=	$result->row()->created_at;
			if($sdate == $ve_date)
			{
				$this->db->update('stock',$data, array('stock_id' => $sid));
			}
			else
			{
				$this->db->insert('stock',$data);	
				$data1['stock_status']		=	"transfer";	
				$this->db->update('stock',$data1, array('stock_id' => $sid));
			}
		}
		else
		{
			$data['stock_qty '] 		=	0	-	$purchase_unit_qty;
			$data['stock_date ']		=	$ve_date;	
			$data['stock_product_id']	=	$stockpid;
			$data['stock_batch']		=	$ved_batch;
			$data['stock_dept']			=	$ve_customer;
			$data['stock_status']		=	"standby";
			$data['created_at']			=	date('Y-m-d H:i:s.') . gettimeofday()['usec'];	
			$this->db->insert('stock',$data);
		}
	}

	public function set_sreturn_stock($stockpid,$purchase_unit_qty,$ve_date,$ved_batch){
		
		$this->db->select('*');
		$this->db->from('stock');
		$this->db->where('stock_product_id', $stockpid);
		$this->db->where('stock_dept','3');
		$this->db->where('stock_batch',$ved_batch);
		$this->db->order_by("stock_id", "desc");
		$this->db->where('stock_status','standby');
		$this->db->limit(1);
		$result  		=    $this->db->get();
		$number_of_rows = $result->num_rows();

		if($number_of_rows != 0){
			$sid						=	$result->row()->stock_id;
			$sqty						=	$result->row()->stock_qty;
			$sdate 						=	$result->row()->stock_date;
			$data['stock_qty'] 			=	$sqty + $purchase_unit_qty;
			$data['stock_date']			=	$ve_date;	
			$data['stock_product_id']	=	$stockpid;
			$data['stock_batch']		=	$ved_batch;
			$data['stock_dept']			=	"3";
			$data['stock_status']		=	"standby";	
			$data['created_at']			=	$result->row()->created_at;
			if($sdate == $ve_date)
			{
				$this->db->update('stock',$data, array('stock_id' => $sid));
			}
			else
			{
				$this->db->insert('stock',$data);	
				$data1['stock_status']		=	"transfer";	
				$this->db->update('stock',$data1, array('stock_id' => $sid));
			}
		}
		else
		{
			$data['stock_qty '] 		=	0	+	$purchase_unit_qty;
			$data['stock_date ']		=	$ve_date;	
			$data['stock_product_id']	=	$stockpid;
			$data['stock_batch']		=	$ved_batch;
			$data['stock_dept']			=	"3";
			$data['stock_status']		=	"standby";
			$data['created_at']			=	date('Y-m-d H:i:s.') . gettimeofday()['usec'];	
			$this->db->insert('stock',$data);
		}
	}

	public function delete_purchase_stock($stockpid,$purchase_unit_qty,$purchase_date,$ved_batch,$ve_customer){
		
		$this->db->select('*');
		$this->db->from('stock');
		$this->db->where('stock_product_id', $stockpid);
		$this->db->where('stock_dept',$ve_customer);
		$this->db->where('stock_batch',$ved_batch);
		$this->db->where('stock_date >=',$purchase_date);
		$this->db->order_by("stock_id", "asc");
		$query  		=    $this->db->get();
		$result 		=    $query->result_array();
		foreach ($result as $value) {
		$sid						=	$value['stock_id'];
		$sqty						=	$value['stock_qty'];
		$data['stock_qty'] 			=	$sqty + $purchase_unit_qty;
		$this->db->update('stock',$data, array('stock_id' => $sid));
		}

	}
	public function delete_sale_stock($stockpid,$purchase_unit_qty,$purchase_date,$ved_batch){
		
		$this->db->select('*');
		$this->db->from('stock');
		$this->db->where('stock_product_id', $stockpid);
		$this->db->where('stock_dept','3');
		$this->db->where('stock_batch',$ved_batch);
		$this->db->where('stock_date >=',$purchase_date);
		$this->db->order_by("stock_id", "asc");
		$query  		=    $this->db->get();
		$result 		=    $query->result_array();
		foreach ($result as $value) {
		$sid						=	$value['stock_id'];
		$sqty						=	$value['stock_qty'];
		$data['stock_qty'] 			=	$sqty - $purchase_unit_qty;
		$this->db->update('stock',$data, array('stock_id' => $sid));
		}

	}

	public function update_purchase_stock($stockpid,$purchased_unit_qty,$ve_date,$ved_batch,$ve_customer){
		
		$this->db->select('*');
		$this->db->from('stock');
		$this->db->where('stock_product_id', $stockpid);
		$this->db->where('stock_dept',$ve_customer);
		$this->db->where('stock_batch',$ved_batch);
		$this->db->where('stock_date >=',$ve_date);
		$this->db->order_by("stock_id", "asc");
		$query  				= $this->db->get();
		$number_of_rows 		= $query->num_rows();
		if($number_of_rows != 0){
		$result 		=    $query->result_array();
		foreach ($result as $value) {
		$sid						=	$value['stock_id'];
		$sqty						=	$value['stock_qty'];
		$data['stock_qty'] 			=	$sqty - $purchase_unit_qty;
		$this->db->update('stock',$data, array('stock_id' => $sid));
		}
	}
	else
		{
			$data['stock_qty '] 		=	0	-	$purchase_unit_qty;
			$data['stock_date ']		=	$ve_date;	
			$data['stock_product_id']	=	$stockpid;
			$data['stock_batch']		=	$ved_batch;
			$data['stock_dept']			=	$ve_customer;
			$data['stock_status']		=	"standby";

			$this->db->insert('stock',$data);
		}

	}

	public function dlt_purchase_balance($amount_paid,$purchase_date){
		
		$this->db->select('*');
		$this->db->from('balance');
		$this->db->where('balance_date >=',$purchase_date);
		$this->db->order_by("balance_id", "desc");
		$result  		=    $this->db->get();
		$query			=    $result->result_array();
 
			foreach ($query as $value) 
			{
			$bid						=	$value['balance_id'];
			$bamount					=	$value['balance_amount'];
			$data['balance_amount '] 	=	$bamount - $amount_paid;
			$this->db->update('balance',$data, array('balance_id' => $bid));
			}
	}

	// public function dlt_sale_balance($amount_paid,$purchase_date){
		
	// 	$this->db->select('*');
	// 	$this->db->from('balance');
	// 	$this->db->where('balance_date >=',$purchase_date);
	// 	$this->db->order_by("balance_id", "desc");
	// 	$result  		=    $this->db->get();
	// 	$query			=    $result->result_array();
 
	// 		foreach ($query as $value) 
	// 		{
	// 		$bid						=	$value['balance_id'];
	// 		$bamount					=	$value['balance_amount'];
	// 		$data['balance_amount '] 	=	$bamount + $amount_paid;
	// 		$this->db->update('balance',$data, array('balance_id' => $bid));
	// 		}
	// }

	public function up_purchase_balance($amount_paid,$purchase_date){
		
		$this->db->select('*');
		$this->db->from('balance');
		$this->db->where('balance_date >=',$purchase_date);
		$this->db->order_by("balance_id", "desc");
		$result  		=    $this->db->get();
		$query			=    $result->result_array();
 
			foreach ($query as $value) 
			{
			$bid						=	$value['balance_id'];
			$bamount					=	$value['balance_amount'];
			$data['balance_amount '] 	=	$bamount + $amount_paid;
			$this->db->update('balance',$data, array('balance_id' => $bid));
			}
	}

	public function up_sale_balance($amount_paid,$purchase_date){
		
		$this->db->select('*');
		$this->db->from('balance');
		$this->db->where('balance_date >=',$purchase_date);
		$this->db->order_by("balance_id", "desc");
		$result  		=    $this->db->get();
		$query			=    $result->result_array();
 
			foreach ($query as $value) 
			{
			$bid						=	$value['balance_id'];
			$bamount					=	$value['balance_amount'];
			$data['balance_amount '] 	=	$bamount - $amount_paid;
			$this->db->update('balance',$data, array('balance_id' => $bid));
			}
	}

	public function getSReturnId(){
		$this->db->select('CAST(ve_vno as SIGNED INTEGER) as ve_vno');
		$this->db->from('voucher_entry');
		$this->db->order_by('ve_vno','desc');
		$this->db->where('ve_type','sr');
		$this->db->limit(1);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return '1';
		}
		else
		{
			$result 	= $query->row()->ve_vno;
			$result++;
			return $result;
		}
	}
	public function getPReturnId(){
		$this->db->select('CAST(ve_vno as SIGNED INTEGER) as ve_vno');
		$this->db->from('voucher_entry');
		$this->db->order_by('ve_vno','desc');
		$this->db->where('ve_type','pr');
		$this->db->limit(1);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return '1';
		}
		else
		{
			$result 	= $query->row()->ve_vno;
			$result++;
			return $result;
		}
	}

	public function getProduct(){
		$query=$this->db->get_where('product',array('typeid'=>2));
		return $query->result_array();
	}

	public function getPProducts(){
	$this->db->select('voucher_entry.ve_bill_no,suppliers.sp_vendor,suppliers.sp_id,suppliers.sp_phone');
    $this->db->from('voucher_entry');
    $this->db->join('suppliers','suppliers.sp_id = voucher_entry.ve_supplier', 'left');
    $this->db->order_by('voucher_entry.ve_id','desc');
    $this->db->where('voucher_entry.ve_type','p');
    $query=$this->db->get();
    return $query->result_array();
	}
	public function getSProducts(){
	$this->db->select('voucher_entry.ve_vno,patient.p_title,patient.p_name,patient.p_mrd_no,patient.p_phone');
    $this->db->from('voucher_entry');
    $this->db->join('patient','patient.p_mrd_no = voucher_entry.ve_mrd', 'left');
    $this->db->order_by('voucher_entry.ve_id','desc');
    $this->db->where('voucher_entry.ve_type','si');
    $query=$this->db->get();
    return $query->result_array();
	}

	public function getPReturnview($sid)
	{	

		$this->db->select('voucher_entry_detail.*,voucher_entry.*,suppliers.sp_id,suppliers.sp_vendor,suppliers.sp_phone');
    	$this->db->from('voucher_entry');
    	$this->db->join('suppliers','suppliers.sp_id = voucher_entry.ve_supplier', 'left');
    	$this->db->join('voucher_entry_detail','voucher_entry_detail.ved_veid = voucher_entry.ve_id', 'inner');
    	$this->db->order_by('voucher_entry.ve_vno','desc');
    	$this->db->where('voucher_entry.ve_type','pr');
    	$this->db->where('voucher_entry.ve_id',$sid);
    	$query=$this->db->get();
    	return $query->result_array();
	}

	public function getSReturnview($sid)
	{	
		$this->db->select('voucher_entry_detail.*,voucher_entry.*');
    	$this->db->from('voucher_entry');
    	// $this->db->join('patient','patient.p_mrd_no = voucher_entry.ve_mrd', 'left');
    	$this->db->join('voucher_entry_detail','voucher_entry_detail.ved_veid = voucher_entry.ve_id', 'inner');
    	$this->db->order_by('voucher_entry.ve_vno','desc');
    	$this->db->where('voucher_entry.ve_type','sr');
    	$this->db->where('voucher_entry.ve_id',$sid);
    	$query=$this->db->get();
    	return $query->result_array();
	}

}

	?>
