<?php

class Opbilling_model extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('voucher_entry.*');
		$this->db->from('voucher_entry');
		// $this->db->join('patient','patient.p_mrd_no = op.op_mrd', 'inner'); 
		// $this->db->join('voucher_entry','voucher_entry.ve_customer = op.op_id', 'inner'); 
		$this->db->order_by('voucher_entry.ve_date','desc');
		$this->db->where('voucher_entry.ve_type','opbl');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{
			if($row->ve_pstaus=="NP") { $fp = "<font color='#b20000'>Not Completed</font>";  }
            elseif($row->ve_pstaus=="FP") { $fp = "<font color='#228B22'>Completed</font>";  }

			$output 	.=	"<tr><td>".$row->ve_vno."</td>";
			$output 	.=	"<td>".date("d-m-Y",strtotime($row->ve_date))."</td>";
			$output 	.=	"<td>".$row->ve_mrd."</td>";
            $output     .=  "<td>".$row->ve_patient."</td>";
			$output 	.=	"<td>".$row->ve_phone."</td>";
            $output     .=  "<td>".$fp."</td>";

			$output 	.=	"<td class='btn-group  btn-group-xs' ><a href='".$this->config->item('admin_url')."opbilling/view/".$row->ve_id."'class='btn btn-primary view-btn-edit' title='View'><i class='fa fa-eye'></i></a></td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	}

	function get_op_bill_no()
	{
		$this->db->select('CAST(ve_vno as SIGNED INTEGER) as ve_vno');
		$this->db->from('voucher_entry');
		$this->db->where('ve_type','opbl');
        $this->db->order_by('ve_vno','desc');
        $this->db->limit(1);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return '1';
		}
		else
		{
			$result 	= $query->row()->ve_vno;
			$result++;
			return $result;
		}
	}

	function get_room($ipno){

    $this->db->select('room_shift.*,room.*');
    $this->db->from('ip'); 
    $this->db->join('room_shift','room_shift.rs_ip = ip.ip_ipno', 'inner'); 
    $this->db->join('room','room.rm_id = room_shift.rs_rmno', 'inner'); 
    $this->db->where('ip.ip_ipno',$ipno);

    $query=$this->db->get();
    $data = $query->result_array();
    if($query->num_rows()==0) { $data ="0"; }
    return $data;
  }

  function get_voucher_bills($ipno){

    $this->db->select('*');
    $this->db->from('voucher_entry'); 
    $this->db->where('ve_customer',$ipno);
    $this->db->where('ve_type',"lbi");
    $this->db->where('ve_status',"cr");
    $this->db->or_where('ve_status',"acr");
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    return $data;
  }

  function get_pharmacy_bills($ipno){

    $this->db->select('*');
    $this->db->from('voucher_entry'); 
    $this->db->where('ve_customer',$ipno);
    $this->db->where('ve_type',"si");
    $this->db->where('ve_status',"cr");
    $this->db->or_where('ve_status',"acr");
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    return $data;
  }

  function get_pharmacy_returns($ipno){

    $this->db->select('*');
    $this->db->from('voucher_entry'); 
    $this->db->where('ve_customer',$ipno);
    $this->db->where('ve_type',"sr");
    $this->db->where('ve_status',"cr");
    $this->db->or_where('ve_status',"acr");
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    return $data;
  }

   function get_advances($ipno){

    $this->db->select('*');
    $this->db->from('voucher_entry'); 
    $this->db->where('ve_customer',$ipno);
    $this->db->where('ve_type',"ad");
    $this->db->where('ve_status',"acc");
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    return $data;
  }

  function get_additional_items($ve_id){

    $this->db->select('voucher_entry_detail.*,voucher_entry.*');
    $this->db->from('voucher_entry'); 
    $this->db->join('voucher_entry_detail','voucher_entry_detail.ved_veid = voucher_entry.ve_id', 'inner'); 
    // $this->db->join('op','op.op_id = voucher_entry.ve_customer', 'inner');
    // $this->db->join('users','users.u_emp_id = op.op_doctor', 'inner'); 
    // $this->db->join('department','department.dp_id = op.op_department   ', 'inner');
    // $this->db->join('patient','patient.p_mrd_no = op.op_mrd', 'inner');
    $this->db->where('voucher_entry.ve_id',$ve_id);
    $this->db->where('voucher_entry.ve_type',"opbl");
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    return $data;
  }


	public function get_additional_bill(){
	$this->db->select('*');
	$this->db->from('additional_bill');
	$this->db->where('ab_status',1);
	$query=$this->db->get();
	return $query->result_array();
	}

    public function get_admin_pswd(){
    $this->db->select('u_disc_pswd');
    $this->db->from('users');
    $this->db->where('u_designation',4);
    $query=$this->db->get();
    return $query->row()->u_disc_pswd;
    }



}