<?php

class Subcategory_model extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$sql="SELECT C.*,SC.* FROM category AS C INNER JOIN subcategory AS SC WHERE C.id=SC.categoryid AND C.status=1 AND SC.status=1 order by SC.id ASC";
		// $this->db->select('*');
		// $this->db->from('subcategory');
		// $query 			=	$this->db->get();
		$query 			=	$this->db->query($sql);

		foreach($query->result() as $row)
		{
			$sl_no++;
			if($row->status == '1')
			{
				$image = "<i class='fa fa-check'></i>";
				$status = 0;
			}
			else
			{
				$image = "<i class='fa fa-ban'></i>";
				$status = 1;
			}
			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->category."</td>";
			$output 	.=	"<td>".$row->subcategory."</td>";
			 
			// $output 	.=	"<td><img src='".$this->config->item('image_url')."features/small/".$row->feature_image."'></td>";
			 
			// $output 	.=	"<td><a href='".$this->config->item('admin_url')."subcategory/active/".$row->id."/".$status."'>".$image."</a></td>";
			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='".$this->config->item('admin_url')."subcategory/edit/".$row->id."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a> 
			 
<a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."subcategory/delete/".$row->id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>";
			 // <a href='".$this->config->item('admin_url')."subcategory/delete/".$row->id."'class='btn btn-danger' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}

	function get_category()
	{
		$this->db->select('*');
		$this->db->from('category');
		$query 			=	$this->db->get();
		return $query->result_array();
	}
  
	function get_one_banner($id)
	{
		// $sql="SELECT C.*,SC.* FROM category AS C INNER JOIN subcategory AS SC WHERE C.id=SC.categoryid AND SC.id=$id";
		$this->db->select('*');
		$this->db->from('subcategory');
		$this->db->where('id',$id);
		$query 			=	$this->db->get();
		// $query 			=	$this->db->query($sql);
		$row 			=	$query->row();
		return $row;
	}
}