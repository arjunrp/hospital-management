<?php

class Testsname_model extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;
// $sql="SELECT E.*,EC.* FROM mw_tests AS E INNER JOIN mw_test_name AS EC WHERE E.ts_id=EC.tn_testid AND E.ts_status=1 AND EC.tn_status=1 order by EC.tn_testid asc";

		$this->db->select('tests.*,test_name.*');
    	$this->db->from('test_name');
    	$this->db->join('tests','tests.ts_id = test_name.tn_testid', 'left');
    	$this->db->order_by('tests.ts_test','asc');
		$query 			=	$this->db->get();

		// $query 			=	$this->db->query($sql);
		// $this->db->select('*');
		// $this->db->from('expence');

		foreach($query->result() as $row)
		{
			$sl_no++;
			if($row->tn_status == '1')
			{
				$image = "<i class='fa fa-check'></i>";
				$tn_status = 0;
			}
			else
			{
				$image = "<i class='fa fa-ban'></i>";
				$status = 1;
			}
			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->ts_test."</td>";
			$output 	.=	"<td>".$row->tn_name."</td>";
			$output 	.=	"<td>".$row->tn_price."</td>";
			 
			 
			// $output 	.=	"<td><img src='".$this->config->item('image_url')."features/small/".$row->feature_image."'></td>";
			 
			// $output 	.=	"<td><a href='".$this->config->item('admin_url')."expence/active/".$row->id."/".$status."'>".$image."</a></td>";
			// $output 	.=	"<td class='btn-group  btn-group-xs'><a href='".$this->config->item('admin_url')."expence/edit/".$row->id."' class='btn btn-info view-btn-edit' title='Edit' ><i class='fa fa-pencil-square-o'></i></a> 
			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='".$this->config->item('admin_url')."testsname/edit/".$row->tn_id."' class='btn btn-primary view-btn-edit' title='Edit' ><i class='fa fa-pencil-square-o'></i></a>
			 
		   <a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."testsname/delete/".$row->tn_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>";
			// <a  href='".$this->config->item('admin_url')."expence/delete/".$row->id."'class='btn btn-danger' title='Delete'><i class='fa fa-times-circle-o'></i></a>";
			// $output		.=	"<td><a href='".$this->config->item('admin_url')."expence/delete/".$row->id."'><i class='fa fa-trash'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}


function get_category()
	{
		$this->db->select('*');
		$this->db->from('tests');
		$query 			=	$this->db->get();
		return $query->result_array();
	}
  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('test_name');
		$this->db->where('tn_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}
}