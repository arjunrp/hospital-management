<?php

class Allreportmodel extends CI_Model 
{

	function sreturn_blnce($fdate,$tdate,$product)
	{
		$output 		=	"";
		$sumprice		=	0;
		$sumgst			=	0;
		$sumtot			=	0;

			$this->db->select('preturn.preturn_date,preturn_details.*,sum(preturn_details.preturn_details_qty) as sumqty,sum(preturn_details.preturn_details_sprice) as sumprice,sum(preturn_details.preturn_details_gstamnt) as sumgst,sum(preturn_details.preturn_details_amount) as sumtot');
			$this->db->from('preturn_details');
			$this->db->join('product','product.id = preturn_details.preturn_details_item_id', 'inner');
			$this->db->join('preturn','preturn.preturn_id = preturn_details.preturn_id', 'inner');
			$this->db->group_by('preturn.preturn_date');
			$this->db->group_by('preturn_details.preturn_details_item_id');
			$this->db->order_by('preturn.preturn_date','desc');
			$this->db->where('preturn.preturn_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			$this->db->where('preturn.preturn_type','2');
			if($product=='br')
			{
			$p_catg = $this->input->post('p_brand');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.brandid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='gr')
			{
			$p_tcatg = $this->input->post('p_type');
			$p_catg = $this->input->post('p_group');
			$p_scatg = $this->input->post('p_scategory');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_scatg>0)
			{
			$this->db->where('product.subcategoryid',$p_scatg);  
			}
			$this->db->where('product.groupid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='ty')
			{
			$p_gcatg = $this->input->post('p_group');
			$p_catg = $this->input->post('p_type');
			$p_scatg = $this->input->post('p_scategory');
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			if($p_scatg>0)
			{ 
			$this->db->where('product.subcategoryid',$p_scatg); 
			}
			$this->db->where('product.typeid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='sc')
			{
			$p_catg = $this->input->post('p_scategory');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.subcategoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='ca')
			{
			$p_catg = $this->input->post('p_category');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.categoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product==0)
			{
					$query 			=	$this->db->get();
			}

			else if($product>0)
			{
					$this->db->where('preturn_details.preturn_details_item_id',$product);
					$query 			=	$this->db->get();
			}

					foreach($query->result() as $row)
			{
					$output 	.=	"<tr><td>".$row->preturn_date."</td>"; 
					$output 	.=	"<td>".$row->preturn_details_item."</td>";
					$output 	.=	"<td>".$row->preturn_details_price."</td>";
					$output 	.=	"<td>".$row->sumqty." ".$row->preturn_details_unit."</td>";
					$output 	.=	"<td>".$row->sumprice."</td>";
					$output 	.=	"<td>".$row->sumgst." ( ".$row->preturn_details_gst." % )</td>";
					$output 	.=	"<td>".$row->sumtot."</td>";
					$output		.=	"</tr>";
					$sumprice		=	$sumprice	+	$row->sumprice;
					$sumgst			=	$sumgst		+	$row->sumgst;
					$sumtot			=	$sumtot		+	$row->sumtot;
			}
					$output		.=	"<tbody>
                <tr><th style='border-top:1px solid' colspan='4'>Total Amount</th><th style='border-top:1px solid'>Rs. ".$sumprice."</th>
                <th style='border-top:1px solid'>Rs. ".$sumgst."</th><th style='border-top:1px solid'>Rs. ".$sumtot."</th></tr>
              </tbody>";
			return $output;
		}

		function preturn_blnce($fdate,$tdate,$product)
	{
		$output 		=	"";
		$sumtot 		= 	0;

			$this->db->select('preturn.preturn_date,preturn_details.*,sum(preturn_details.preturn_details_qty) as sumqty,sum(preturn_details.preturn_details_amount) as sumtot');
			$this->db->from('preturn_details');
			$this->db->join('product','product.id = preturn_details.preturn_details_item_id', 'inner');
			$this->db->join('preturn','preturn.preturn_id = preturn_details.preturn_id', 'inner');
			$this->db->group_by('preturn.preturn_date');
			$this->db->group_by('preturn_details.preturn_details_item_id');
			$this->db->order_by('preturn.preturn_date','desc');
			$this->db->where('preturn.preturn_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			$this->db->where('preturn.preturn_type','1');
			if($product=='br')
			{
			$p_catg = $this->input->post('p_brand');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.brandid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='gr')
			{
			$p_tcatg = $this->input->post('p_type');
			$p_catg = $this->input->post('p_group');
			$p_scatg = $this->input->post('p_scategory');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_scatg>0)
			{
			$this->db->where('product.subcategoryid',$p_scatg);  
			}
			$this->db->where('product.groupid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='ty')
			{
			$p_gcatg = $this->input->post('p_group');
			$p_catg = $this->input->post('p_type');
			$p_scatg = $this->input->post('p_scategory');
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			if($p_scatg>0)
			{ 
			$this->db->where('product.subcategoryid',$p_scatg); 
			}
			$this->db->where('product.typeid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='sc')
			{
			$p_catg = $this->input->post('p_scategory');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.subcategoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='ca')
			{
			$p_catg = $this->input->post('p_category');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.categoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product==0)
			{
					$query 			=	$this->db->get();
			}

			else if($product>0)
			{
					$this->db->where('preturn_details.preturn_details_item_id',$product);
					$query 			=	$this->db->get();
			}

					foreach($query->result() as $row)
			{
					$output 	.=	"<tr><td>".$row->preturn_date."</td>"; 
					$output 	.=	"<td>".$row->preturn_details_item."</td>";
					$output 	.=	"<td>".$row->preturn_details_price."</td>";
					$output 	.=	"<td>".$row->sumqty." ".$row->preturn_details_unit."</td>";
					$output 	.=	"<td>".$row->sumtot."</td>";
					$output		.=	"</tr>";
					$sumtot 	 =  $sumtot+$row->sumtot;

			}

					$output		.=	"</table><table class='table table-bordered'><thead><tr><th width='80%'>Total Amount</th><th>Rs. ".$sumtot."</th></tr></thead>";
			return $output;
		}

		function sreturn_blnce_details($fdate,$tdate,$product)
	{
		$output 		=	"";
		$sumprice		=	0;
		$sumgst			=	0;
		$sumtot			=	0;

			$this->db->select('preturn.preturn_date,preturn_details.*');
			$this->db->from('preturn_details');
			$this->db->join('preturn','preturn.preturn_id = preturn_details.preturn_id', 'inner');
			$this->db->order_by('preturn.preturn_date','desc');
			$this->db->where('preturn.preturn_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			$this->db->where('preturn.preturn_type','2');
			if($product=='br')
			{
			$p_catg = $this->input->post('p_brand');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.brandid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='gr')
			{
			$p_tcatg = $this->input->post('p_type');
			$p_catg = $this->input->post('p_group');
			$p_scatg = $this->input->post('p_scategory');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_scatg>0)
			{
			$this->db->where('product.subcategoryid',$p_scatg);  
			}
			$this->db->where('product.groupid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='ty')
			{
			$p_gcatg = $this->input->post('p_group');
			$p_catg = $this->input->post('p_type');
			$p_scatg = $this->input->post('p_scategory');
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			if($p_scatg>0)
			{ 
			$this->db->where('product.subcategoryid',$p_scatg); 
			}
			$this->db->where('product.typeid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='sc')
			{
			$p_catg = $this->input->post('p_scategory');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.subcategoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='ca')
			{
			$p_catg = $this->input->post('p_category');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.categoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product==0)
			{
					$query 			=	$this->db->get();
			}

			else if($product>0)
			{
					$this->db->where('preturn_details.preturn_details_item_id',$product);
					$query 			=	$this->db->get();
			}

					foreach($query->result() as $row)
			{
					$output 	.=	"<tr><td>".$row->preturn_date."</td>"; 
					$output 	.=	"<td>".$row->preturn_details_item."</td>";
					$output 	.=	"<td>".$row->preturn_details_price."</td>";
					$output 	.=	"<td>".$row->preturn_details_qty." ".$row->preturn_details_unit."</td>";
					$output 	.=	"<td>".$row->preturn_details_sprice."</td>";
					$output 	.=	"<td>".$row->preturn_details_gstamnt." ( ".$row->preturn_details_gst. " % )</td>";
					$output 	.=	"<td>".$row->preturn_details_amount."</td>";
					$output		.=	"</tr>";
					$sumprice		=	$sumprice	+	$row->preturn_details_sprice;
					$sumgst			=	$sumgst		+	$row->preturn_details_gstamnt;
					$sumtot			=	$sumtot		+	$row->preturn_details_amount;
			}
					$output		.=	"<tbody>
                <tr><th style='border-top:1px solid' colspan='4'>Total Amount</th><th style='border-top:1px solid'>Rs. ".$sumprice."</th>
                <th style='border-top:1px solid'>Rs. ".$sumgst."</th><th style='border-top:1px solid'>Rs. ".$sumtot."</th></tr>
              </tbody>";
			return $output;
		}

		function preturn_blnce_details($fdate,$tdate,$product)
	{
		$output 		=	"";
		$sumtot 		= 	0;

			$this->db->select('preturn.preturn_date,preturn_details.*');
			$this->db->from('preturn_details');
			$this->db->join('preturn','preturn.preturn_id = preturn_details.preturn_id', 'inner');
			$this->db->order_by('preturn.preturn_date','desc');
			$this->db->where('preturn.preturn_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			$this->db->where('preturn.preturn_type','1');
			if($product=='br')
			{
			$p_catg = $this->input->post('p_brand');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.brandid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='gr')
			{
			$p_tcatg = $this->input->post('p_type');
			$p_catg = $this->input->post('p_group');
			$p_scatg = $this->input->post('p_scategory');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_scatg>0)
			{
			$this->db->where('product.subcategoryid',$p_scatg);  
			}
			$this->db->where('product.groupid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='ty')
			{
			$p_gcatg = $this->input->post('p_group');
			$p_catg = $this->input->post('p_type');
			$p_scatg = $this->input->post('p_scategory');
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			if($p_scatg>0)
			{ 
			$this->db->where('product.subcategoryid',$p_scatg); 
			}
			$this->db->where('product.typeid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='sc')
			{
			$p_catg = $this->input->post('p_scategory');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.subcategoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='ca')
			{
			$p_catg = $this->input->post('p_category');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.categoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product==0)
			{
					$query 			=	$this->db->get();
			}

			else if($product>0)
			{
					$this->db->where('preturn_details.preturn_details_item_id',$product);
					$query 			=	$this->db->get();
			}

					foreach($query->result() as $row)
			{
					$output 	.=	"<tr><td>".$row->preturn_date."</td>"; 
					$output 	.=	"<td>".$row->preturn_details_item."</td>";
					$output 	.=	"<td>".$row->preturn_details_price."</td>";
					$output 	.=	"<td>".$row->preturn_details_qty." ".$row->preturn_details_unit."</td>";
					$output 	.=	"<td>".$row->preturn_details_amount."</td>";
					$output		.=	"</tr>";
					$sumtot 	 =  $sumtot  +	$row->preturn_details_amount;

			}

					$output		.=	"</table><table class='table table-bordered'><thead><tr><th width='80%'>Total Amount</th><th>Rs. ".$sumtot."</th></tr></thead>";
			return $output;
		}

	function get_account($fdate,$tdate,$product,$r_type)
	{
		$output 		=	"";
		$sump 			=	0;

		if($r_type==1)
		{
			$this->db->select('purchase_detail.*,sum(purchase_detail.purchase_product_qty) as sumq,sum(purchase_detail.purchase_discount) as sumd,sum(purchase_detail.purchase_total) as sump');
			$this->db->from('purchase_detail');
			$this->db->join('product','product.id = purchase_detail.purchase_item_id', 'inner');
			$this->db->group_by('purchase_detail.purchase_date');
			$this->db->group_by('purchase_detail.purchase_item_id');
			$this->db->order_by('purchase_detail.purchase_date','desc');
			$this->db->where('purchase_detail.purchase_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			if($product=='br')
			{
			$p_catg = $this->input->post('p_brand');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.brandid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='gr')
			{
			$p_tcatg = $this->input->post('p_type');
			$p_catg = $this->input->post('p_group');
			$p_scatg = $this->input->post('p_scategory');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_scatg>0)
			{
			$this->db->where('product.subcategoryid',$p_scatg);  
			}
			$this->db->where('product.groupid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='ty')
			{
			$p_gcatg = $this->input->post('p_group');
			$p_catg = $this->input->post('p_type');
			$p_scatg = $this->input->post('p_scategory');
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			if($p_scatg>0)
			{ 
			$this->db->where('product.subcategoryid',$p_scatg); 
			}
			$this->db->where('product.typeid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='sc')
			{
			$p_catg = $this->input->post('p_scategory');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.subcategoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='ca')
			{
			$p_catg = $this->input->post('p_category');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.categoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product==0)
			{
					$query 			=	$this->db->get();
			}

			else if($product>0)
			{
					$this->db->where('purchase_detail.purchase_item_id',$product);
					$query 			=	$this->db->get();
			}

					foreach($query->result() as $row)
			{
					$output 	.=	"<tr><td>".$row->purchase_date."</td>"; 
					$output 	.=	"<td>".$row->purchase_item."</td>";
					$output 	.=	"<td>".$row->purchase_unit_price."</td>";
					$output 	.=	"<td>".$row->sumq."</td>";
					$output 	.=	"<td>".$row->sumd."</td>";
					$output 	.=	"<td>".$row->sump."</td>";
					$output		.=	"</tr>";
					$sump 		 =	$sump + $row->sump;
			}

					$output		.=	"</table><table class='table table-bordered'><thead><tr><th width='85%'>Total Purchase Amount</th><th>Rs. ".$sump."</th></tr></thead>"; 
			return $output;
		}

		if($r_type==0)
		{
			$temp  	= array();
    		$temp2 	= array();
    		$tep2 	= array();
    		$sum   	= array();

			$this->db->select('*');
			$this->db->from('balance');
			$this->db->order_by('balance_date','asc');
			$this->db->where('balance_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			$query                 =    $this->db->get();
    		$data                  =    $query->result_array();
    foreach($data as $key => $value)
    {   

        if(in_array($value['balance_date'],$temp)){

            $temp2[$value['balance_date']]['psdate'] 		=  $value['balance_date'];
            $temp2[$value['balance_date']]['purchase_sum']  	= $sum[$value['balance_date']]['purchase_sum'] + $this->getPurchase($value['balance_date']);
            $temp2[$value['balance_date']]['sales_sum']     	= $sum[$value['balance_date']]['sales_sum']    + $this->getSales($value['balance_date']);
            $temp2[$value['balance_date']]['expense']  		= $this->getExpense($value['balance_date']);
            $temp2[$value['balance_date']]['prBalance']  	= $sum[$value['balance_date']]['prBalance'] + $this->getPRBalance($value['balance_date']);
            $temp2[$value['balance_date']]['srBalance']     	= $sum[$value['balance_date']]['srBalance']    + $this->getSRBalance($value['balance_date']);
            $temp2[$value['balance_date']]['openingBalance']  	= $this->getOpeningBalance($value['balance_date']);
            $temp2[$value['balance_date']]['closingBalance']  	= $this->getClosingBalance($value['balance_date']);


        } else {


            $temp2[$value['balance_date']]['psdate'] 		=  $value['balance_date'];
            $temp2[$value['balance_date']]['purchase_sum']  = $this->getPurchase($value['balance_date']);
            $temp2[$value['balance_date']]['sales_sum']     = $this->getSales($value['balance_date']);
            $temp2[$value['balance_date']]['expense']  		= $this->getExpense($value['balance_date']);
            $temp2[$value['balance_date']]['prBalance']  	= $this->getPRBalance($value['balance_date']);
            $temp2[$value['balance_date']]['srBalance']  	= $this->getSRBalance($value['balance_date']);
            $temp2[$value['balance_date']]['openingBalance']= $this->getOpeningBalance($value['balance_date']);
            $temp2[$value['balance_date']]['closingBalance']= $this->getClosingBalance($value['balance_date']);

        } 

    }
		 return $temp2;
			
		}

		if($r_type==2)
		{
			$sumg 			=	0;
			$sumt1 			=	0;
			$sumtot 		=	0;
			$this->db->select('sale_details.*,sum(sale_details.sale_item_qty) as sumq1,sum(sale_details.sale_discount) as sumd1,sum(sale_details.gst_amount) as sumg,sum(sale_details.sale_gst_total) as sumt1,sum(sale_details.sale_total) as sumtot');
			$this->db->from('sale_details');
			$this->db->join('product', 'product.id = sale_details.sale_item_id', 'inner');
			$this->db->order_by('sale_details.sale_date','desc');
			$this->db->group_by('sale_details.sale_item_id');
			$this->db->group_by('sale_details.sale_date');
			$this->db->where('sale_details.sale_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');

			if($product=='br')
			{
			$p_catg = $this->input->post('p_brand');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.brandid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='gr')
			{
			$p_tcatg = $this->input->post('p_type');
			$p_catg = $this->input->post('p_group');
			$p_scatg = $this->input->post('p_scategory');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_scatg>0)
			{
			$this->db->where('product.subcategoryid',$p_scatg);  
			}
			$this->db->where('product.groupid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='ty')
			{
			$p_gcatg = $this->input->post('p_group');
			$p_catg = $this->input->post('p_type');
			$p_scatg = $this->input->post('p_scategory');
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			if($p_scatg>0)
			{ 
			$this->db->where('product.subcategoryid',$p_scatg); 
			}
			$this->db->where('product.typeid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='sc')
			{
			$p_catg = $this->input->post('p_scategory');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.subcategoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='ca')
			{
			$p_catg = $this->input->post('p_category');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.categoryid',$p_catg); 
			$query=$this->db->get();
			}

			else if($product==0)
			{
    				$query                 =    $this->db->get();
    		}
    		else if($product>=0)
    		{
    			$this->db->where('sale_details.sale_item_id',$product);
				$query                 =    $this->db->get();
    		}

			foreach($query->result() as $row)
			{
			$output 	.=	"<tr><td>".$row->sale_date."</td>"; 
			$output 	.=	"<td>".$row->sale_item."</td>";
			$output 	.=	"<td>".$row->sale_unit_price."</td>";
			$output 	.=	"<td>".$row->sumq1." ".$row->sale_item_unit."</td>";
			$output 	.=	"<td>".$row->sumd1."</td>";
			$output 	.=	"<td>".$row->sumtot."</td>";
			$output 	.=	"<td>".round($row->sumg,2)."</td>";
			$output 	.=	"<td>".round($row->sumt1,2)."</td>";
			$output		.=	"</tr>";
			$sumg 		 =	$sumg 	+ round($row->sumg,2);
			$sumt1 		 =	$sumt1 	+ $row->sumtot;
			$sumtot 	 =	$sumtot + round($row->sumt1,2);
		}
					$output		.=	"</table><table class='table table-bordered'><thead><tr><th width='85%'>Total Amount(Ex. GST)</th><th>Rs. ".$sumt1."</th></tr>
					<tr><th width='85%'>Total GST</th><th>Rs. ".$sumg."</th></tr><tr><th width='85%'>Total Sale Amount</th><th>Rs. ".$sumtot."</th></tr></thead>"; 

		return $output;
			}
}

function getPurchase($purchase_date)
{    
    $this->db->select_sum('amount_paid');
    $this->db->from('purchase');
    $this->db->where('purchase_date',$purchase_date);
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->row()->amount_paid;
    	return $rowcount;
	}
}

function getSales($purchase_date)
{    
    $this->db->select_sum('amount_paid');
    $this->db->from('sales');
    $this->db->where('sale_date', $purchase_date);
    $query = $this->db->get();
    $rowcount = $query->row()->amount_paid;
    return $rowcount;
}


function getExpense($purchase_date)
{    
    $this->db->select('sum(expence.amount) as amount,excategory.category');
    $this->db->from('expence');
    $this->db->join('excategory', 'excategory.id = expence.categoryid', 'inner');
    $this->db->where('expence.exdate', $purchase_date);
    $this->db->group_by('expence.categoryid');
    $query = $this->db->get();
    $rowcount = $query->result_array();
    return $rowcount;
}

function getClosingBalance($purchase_date)
{
    $this->db->select('*');
    $this->db->from('balance');
    $this->db->where('balance_date',$purchase_date);
    $this->db->order_by("balance_id", "asc");
    $this->db->limit(1);
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	return  $query->row()->balance_amount; 
	}
}
function getOpeningBalance($purchase_date)
{
    $this->db->select('*');
    $this->db->from('balance');
    $this->db->where('balance_date <',$purchase_date);
    $this->db->order_by("balance_id", "desc");
    $this->db->limit(1);
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	return  $query->row()->balance_amount; 
	}
}

function getPRBalance($purchase_date)
{
    $this->db->select_sum('preturn_amount');
    $this->db->from('preturn');
    $this->db->where('preturn_date',$purchase_date);
    $this->db->where('preturn_type','1');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->row()->preturn_amount;
    	return $rowcount;
	}
}

function getSRBalance($purchase_date)
{
     $this->db->select_sum('preturn_amount');
    $this->db->from('preturn');
    $this->db->where('preturn_date',$purchase_date);
    $this->db->where('preturn_type','2');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->row()->preturn_amount;
    	return $rowcount;
	}
}


function get_account_main($fdate,$tdate,$product,$r_type)
	{
		$output 		=	"";

		if($r_type==1)
		{
			$this->db->select('purchase_detail.purchase_total as sumgsttot,purchase.amount as sumam,purchase.discount as sumdi,purchase.amount_payable as sumay,purchase.amount_paid as sumad,purchase.amount_balance as sumab');
			$this->db->from('purchase');
			$this->db->join('purchase_detail','purchase_detail.purchase_id = purchase.purchase_id', 'inner');
			$this->db->join('product','product.id = purchase_detail.purchase_item_id', 'inner');
			$this->db->group_by('purchase_detail.purchase_id');
			$this->db->where('purchase.purchase_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			if($product=='br')
			{
			$p_catg = $this->input->post('p_brand');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.brandid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='gr')
			{
			$p_tcatg = $this->input->post('p_type');
			$p_catg = $this->input->post('p_group');
			$p_scatg = $this->input->post('p_scategory');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_scatg>0)
			{
			$this->db->where('product.subcategoryid',$p_scatg);  
			}
			$this->db->where('product.groupid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='ty')
			{
			$p_gcatg = $this->input->post('p_group');
			$p_catg = $this->input->post('p_type');
			$p_scatg = $this->input->post('p_scategory');
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			if($p_scatg>0)
			{ 
			$this->db->where('product.subcategoryid',$p_scatg); 
			}
			$this->db->where('product.typeid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='sc')
			{
			$p_catg = $this->input->post('p_scategory');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.subcategoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='ca')
			{
			$p_catg = $this->input->post('p_category');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.categoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product==0)
			{
					$query 			=	$this->db->get();
			}

			else if($product>0)
			{
					$this->db->where('purchase_detail.purchase_item_id',$product);
					$query 			=	$this->db->get();
			}	
			return $query->result_array();
		}

		if($r_type==2)
		{
			$this->db->select('sales.sale_amount as sumtot,sale_details.sale_total as sumtot1,sale_details.sale_gst_total as sumgsttot,sale_details.gst_amount as sumgst1,sales.total_wgst as sumam,sales.sale_discount as sumdi,sales.gst_total as sumgst,sales.amount_payable as sumay,sales.round_off as sumrf,sales.amount_paid as sumad,sales.amount_balance as sumab');
			$this->db->from('sales');
			$this->db->join('sale_details','sale_details.sale_id = sales.sales_id', 'inner');
			$this->db->join('product','product.id = sale_details.sale_item_id', 'inner');
			
			$this->db->where('sales.sale_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			if($product=='br')
			{
			$p_catg = $this->input->post('p_brand');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.brandid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='gr')
			{
			$p_tcatg = $this->input->post('p_type');
			$p_catg = $this->input->post('p_group');
			$p_scatg = $this->input->post('p_scategory');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_scatg>0)
			{
			$this->db->where('product.subcategoryid',$p_scatg);  
			}
			$this->db->where('product.groupid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='ty')
			{
			$p_gcatg = $this->input->post('p_group');
			$p_catg = $this->input->post('p_type');
			$p_scatg = $this->input->post('p_scategory');
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			if($p_scatg>0)
			{ 
			$this->db->where('product.subcategoryid',$p_scatg); 
			}
			$this->db->where('product.typeid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='sc')
			{
			$p_catg = $this->input->post('p_scategory');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.subcategoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='ca')
			{
			$p_catg = $this->input->post('p_category');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.categoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product==0)
			{
					$this->db->group_by('sale_details.sale_id');
					$query 			=	$this->db->get();
			}

			else if($product>0)
			{
					$this->db->where('sale_details.sale_item_id',$product);
					$query 			=	$this->db->get();
			}	
			return $query->result_array();
		}
}



function list_small($product)
{
	$this->db->select('product');
	$this->db->from('product');
	$this->db->where('id',$product);
	$query 			=	$this->db->get();
	return $query->row()->product;		
}

function get_account_detail($fdate,$tdate,$product,$r_type)
	{
		$output 		=	"";
		$sump 			=	0;
		$sumg 			=	0;
		$sumt1 			=	0;
		$sumtot 		=	0;
		if($r_type==1)
		{
			$this->db->select('purchase_detail.*');
			$this->db->from('purchase_detail');
			$this->db->join('product', 'product.id = purchase_detail.purchase_item_id', 'inner');
			$this->db->order_by('purchase_detail.purchase_date','desc');
			$this->db->where('purchase_detail.purchase_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			if($product=='br')
			{
			$p_catg = $this->input->post('p_brand');
			$this->db->where('product.brandid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='gr')
			{
			$p_catg = $this->input->post('p_group');
			$this->db->where('product.groupid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='ty')
			{
			$p_catg = $this->input->post('p_type');
			$this->db->where('product.typeid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='sc')
			{
			$p_catg = $this->input->post('p_scategory');
			$this->db->where('product.subcategoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='ca')
			{
			$p_catg = $this->input->post('p_category');
			$this->db->where('product.categoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product==0)
			{
					$query 			=	$this->db->get();
			}

			else if($product>0)
			{
					$this->db->where('purchase_detail.purchase_item_id',$product);
					$query 			=	$this->db->get();
			}

					foreach($query->result() as $row)
			{
					$output 	.=	"<tr><td>".$row->purchase_date."</td>"; 
					$output 	.=	"<td>".$row->purchase_item."</td>";
					$output 	.=	"<td>".$row->purchase_unit_price."</td>";
					$output 	.=	"<td>".$row->purchase_product_qty." ".$row->purchase_product_unit."</td>";
					$output 	.=	"<td>".$row->purchase_discount."</td>";
					$output 	.=	"<td>".$row->purchase_total."</td>";
					$output		.=	"</tr>";
					$sump 		 =	$sump + $row->purchase_total;
			}
					$output		.=	"</table><table class='table table-bordered'><thead><tr><th width='85%'>Total Purchase Amount</th><th>Rs. ".$sump."</th></tr></thead>";
			return $output;
		}

		if($r_type==0)
		{
			$temp  	= array();
    		$temp2 	= array();
    		$tep2 	= array();
    		$sum   	= array();

			$this->db->select('*');
			$this->db->from('balance');
			$this->db->order_by('balance_date','asc');
			$this->db->where('balance_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			$query                 =    $this->db->get();
    		$data                  =    $query->result_array();
    foreach($data as $key => $value)
    {   

        if(in_array($value['balance_date'],$temp)){

            $temp2[$value['balance_date']]['psdate'] 		=  $value['balance_date'];
            $temp2[$value['balance_date']]['purchase_sum']  	= $sum[$value['balance_date']]['purchase_sum'] + $this->getPurchasedetail($value['balance_date']);
            $temp2[$value['balance_date']]['sales_sum']     	= $sum[$value['balance_date']]['sales_sum']    + $this->getSalesdetail($value['balance_date']);
             $temp2[$value['balance_date']]['preturn_sum']     	= $sum[$value['balance_date']]['sales_sum']    + $this->getPReturndetail($value['balance_date']);
             $temp2[$value['balance_date']]['sreturn_sum']     	= $sum[$value['balance_date']]['sales_sum']    + $this->getSReturndetail($value['balance_date']);

            $temp2[$value['balance_date']]['expense']  		= $this->getExpense($value['balance_date']);
            $temp2[$value['balance_date']]['openingBalance']  	= $this->getOpeningBalance($value['balance_date']);
            $temp2[$value['balance_date']]['closingBalance']  	= $this->getClosingBalance($value['balance_date']);


        } else {


            $temp2[$value['balance_date']]['psdate'] 		=  $value['balance_date'];
            $temp2[$value['balance_date']]['purchase_sum']  	= $this->getPurchasedetail($value['balance_date']);
            $temp2[$value['balance_date']]['sales_sum']     	= $this->getSalesdetail($value['balance_date']);
             $temp2[$value['balance_date']]['preturn_sum']     	= $this->getPReturndetail($value['balance_date']);
             $temp2[$value['balance_date']]['sreturn_sum']     	= $this->getSReturndetail($value['balance_date']);

            $temp2[$value['balance_date']]['expense']  		= $this->getExpense($value['balance_date']);
            $temp2[$value['balance_date']]['openingBalance']  	= $this->getOpeningBalance($value['balance_date']);
            $temp2[$value['balance_date']]['closingBalance']  	= $this->getClosingBalance($value['balance_date']);

        } 

    }
		 return $temp2;
			
		}

		if($r_type==2)
		{
			$this->db->select('sale_details.*');
			$this->db->from('sale_details');
			$this->db->join('product', 'product.id = sale_details.sale_item_id', 'inner');
			$this->db->order_by('sale_details.sale_date','desc');
			$this->db->where('sale_details.sale_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');

			if($product=='br')
			{
			$p_catg = $this->input->post('p_brand');
			$this->db->where('product.brandid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='gr')
			{
			$p_catg = $this->input->post('p_group');
			$this->db->where('product.groupid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='ty')
			{
			$p_catg = $this->input->post('p_type');
			$this->db->where('product.typeid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='sc')
			{
			$p_catg = $this->input->post('p_scategory');
			$this->db->where('product.subcategoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='ca')
			{
			$p_catg = $this->input->post('p_category');
			$this->db->where('product.categoryid',$p_catg); 
			$query=$this->db->get();
			}

			else if($product==0)
			{
    				$query                 =    $this->db->get();
    		}
    		else if($product>=0)
    		{
    			$this->db->where('sale_details.sale_item_id',$product);
				$query                 =    $this->db->get();
    		}

			foreach($query->result() as $row)
			{
			$output 	.=	"<tr><td>".$row->sale_date."</td>"; 
			$output 	.=	"<td>".$row->sale_item."</td>";
			$output 	.=	"<td>".$row->sale_unit_price."</td>";
			$output 	.=	"<td>".$row->sale_item_qty." ".$row->sale_item_unit."</td>";
			$output 	.=	"<td>".$row->sale_discount."</td>";
			$output 	.=	"<td>".$row->sale_total."</td>";
			$output 	.=	"<td>".$row->gst_amount."</td>";
			$output 	.=	"<td>".$row->sale_gst_total."</td>";
			$output		.=	"</tr>";
			$sumg 		 =	$sumg 	+ round($row->gst_amount,2);
			$sumt1 		 =	$sumt1 	+ $row->sale_total;
			$sumtot 	 =	$sumtot + round($row->sale_gst_total,2);
		}
			$output		.=	"</table><table class='table table-bordered'><thead><tr><th width='85%'>Total Amount(Ex. GST)</th><th>Rs. ".$sumt1."</th></tr>
					<tr><th width='85%'>Total GST</th><th>Rs. ".$sumg."</th></tr><tr><th width='85%'>Total Sale Amount</th><th>Rs. ".$sumtot."</th></tr></thead>";
		return $output;
			}
}

function getClosingStock($sale_item_id,$sale_date)
{
    $this->db->select('*');
    $this->db->from('stock');
    $this->db->where('stock_product_id',$sale_item_id);
    $this->db->where('stock_date',$sale_date);
    $this->db->order_by("stock_id", "desc");
    $this->db->limit(1);
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	return  $query->row()->stock_qty; 
	}
}
function getOpeningStock($sale_item_id,$sale_date)
{
    $this->db->select('*');
    $this->db->from('stock');
    $this->db->where('stock_product_id',$sale_item_id);
    $this->db->where('stock_date <',$sale_date);
    $this->db->order_by("stock_id", "desc");
    $this->db->limit(1);
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	return  $query->row()->stock_qty; 
	}
}

function getPurchasedetail($purchase_date)
{    
    $this->db->select('*,sum(purchase_product_qty) as purchase_product_qty,sum(purchase_total) as pamount');
    $this->db->from('purchase_detail');
    $this->db->group_by('purchase_item_id');
    $this->db->where('purchase_date',$purchase_date);
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->result_array();
    	return $rowcount;
	}
}

function getSalesdetail($purchase_date)
{    
    $this->db->select('*,sum(sale_item_qty) as sale_item_qty,sum(sale_total) as sale_total,sum(gst_amount) as gst_amount,sum(sale_gst_total) as samount');
    $this->db->from('sale_details');
    $this->db->group_by('sale_item_id');
    $this->db->where('sale_date', $purchase_date);
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->result_array();
    	return $rowcount;
	}
}
function getPReturndetail($purchase_date)
{    
    $this->db->select('preturn_details.*,sum(preturn_details.preturn_details_qty) as preturn_details_qtyp,sum(preturn_details.preturn_details_amount) as pramount');
    $this->db->from('preturn_details');
    $this->db->join('preturn','preturn.preturn_id = preturn_details.preturn_id','inner');
    $this->db->group_by('preturn_details.preturn_id');
    $this->db->group_by('preturn_details.preturn_details_item_id');
    $this->db->where('preturn.preturn_date',$purchase_date);
    $this->db->where('preturn.preturn_type','1');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->result_array();
    	return $rowcount;
	}
}
function getSReturndetail($purchase_date)
{    
    $this->db->select('preturn_details.*,sum(preturn_details.preturn_details_qty) as preturn_details_qtys,sum(preturn_details.preturn_details_sprice) as preturn_details_sprices,sum(preturn_details.preturn_details_gstamnt) as preturn_details_gstamnts,sum(preturn_details.preturn_details_amount) as sramount');
    $this->db->from('preturn_details');
    $this->db->join('preturn','preturn.preturn_id = preturn_details.preturn_id','inner');
    $this->db->group_by('preturn_details.preturn_id');
    $this->db->group_by('preturn_details.preturn_details_item_id');
    $this->db->where('preturn.preturn_date',$purchase_date);
    $this->db->where('preturn.preturn_type','2');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->result_array();
    	return $rowcount;
	}
}

public function getCompany(){
		$this->db->select('*');
		$this->db->from('company');
		$query=$this->db->get();
		return $query->result_array();
	}

	


}