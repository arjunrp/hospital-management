<?php

class Tests_model extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('*');
		$this->db->from('tests');
		$this->db->order_by('ts_id','asc');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{
			$sl_no++;
			if($row->ts_status == '1')
			{
				$image = "<i class='fa fa-check'></i>";
				$status = 0;
			}
			else
			{
				$image = "<i class='fa fa-ban'></i>";
				$status = 1;
			}
			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->ts_test."</td>";
			 
			 
			// $output 	.=	"<td><a href='".$this->config->item('admin_url')."excategory/active/".$row->id."/".$status."'>".$image."</a></td>";
			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='".$this->config->item('admin_url')."tests/edit/".$row->ts_id."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
            <a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."tests/delete/".$row->ts_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}

  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('tests');
		$this->db->where('ts_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}
}