<?php

class Mrd_room_model extends CI_Model 
{


	function list_all()
	{
		$output="";
		$this->db->select('mrd_room.*,users.u_name,patient.p_title,patient.p_name');
		$this->db->from('mrd_room');
		$this->db->join('users','users.u_emp_id = mrd_room.mr_doc', 'inner');
		$this->db->join('patient','patient.p_mrd_no = mrd_room.mr_mrd', 'inner');
		$this->db->order_by('mrd_room.mr_id','asc');
		$this->db->where('mrd_room.mr_status','2');
		$query 			=	$this->db->get();
		if($query->num_rows()>0) {
		foreach($query->result() as $row)
		{
			if($row->mr_shift=="m") {	$mr_shift = "Morning";	}
			else if($row->mr_shift=="e") {	$mr_shift = "Evening";	}
			else {	$mr_shift = "None";	}
			$output 	.=	"<tr><td>".$row->mr_tno."</td>";
			$output 	.=	"<td>".$row->mr_date."</td>";
			$output 	.=	"<td>".$row->mr_mrd."</td>";
			$output 	.=	"<td>".$row->u_name."</td>";
			$output 	.=	"<td>".$row->p_title." ".$row->p_name."</td>";
			$output 	.=	"<td>".$mr_shift."</td>";
			$output 	.=	"<td>".$row->mr_remark."</td>";


			$output 	.=	"<td class='btn-group  btn-group-xs' ><a href='".$this->config->item('admin_url')."mrd_room/edit/".$row->mr_id."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
			</td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	} 
	else { return false; }
	} 
  
	function get_one_banner($p_mrd_no)
	{
		$this->db->select('*');
		$this->db->from('patient');
		$this->db->where('p_mrd_no',$p_mrd_no);

		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}



		function get_country()
	{
			$this->db->select('*');
			$this->db->from('countries'); 
			$query=$this->db->get();
			return $query->result_array();
	}


function get_city()
	{
			$this->db->select('*');
			$this->db->from('cities'); 
			$query=$this->db->get();
			return $query->result_array();
	}

	function get_states($states)
	{
			$query=$this->db->get_where('states',array('country_id'=>$states));
			return $query->result_array();
	}
	function get_citys($citys)
	{
			$query=$this->db->get_where('cities',array('state_id'=>$citys));
			return $query->result_array();
	}

function get_mrd()
{
	$this->db->select('p_mrd_no');
	$this->db->from('patient');
	$this->db->order_by('p_mrd_no','desc');
	$this->db->where('p_status','1');
	$this->db->limit(1);
	$query 			=	$this->db->get();
	if($query->num_rows()==0)
	{
		return 1;
	}
	else
	{
		$result 	= $query->row()->p_mrd_no+1;
		return $result;
	}
}

function get_reg_fee()
	{
			$this->db->select('reg_fee');
			$this->db->from('company'); 
			$query=$this->db->get();
			return $query->row()->reg_fee;
	}
function get_renewfee()
	{
			$this->db->select('renew_fee');
			$this->db->from('company'); 
			$query=$this->db->get();
			return $query->row()->renew_fee;
	}


	function getCompany()
	{
			$this->db->select('*');
			$this->db->from('company'); 
			$query=$this->db->get();
			return $query->result_array();
	}



	 function list_to_give_m()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('mrd_room.*,users.u_name,patient.p_title,patient.p_name');
		$this->db->from('mrd_room');
		$this->db->join('users','users.u_emp_id = mrd_room.mr_doc','inner');
		$this->db->join('patient','patient.p_mrd_no = mrd_room.mr_mrd', 'inner');
		$this->db->where('mr_status !=','1');
		$this->db->where('mr_status !=','2');
		$this->db->where('mr_shift','m');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{

			if($row->mr_shift=="m") {	$mr_shift = "Morning";	}
			else if($row->mr_shift=="e") {	$mr_shift = "Evening";	}
			else {	$mr_shift = "None";	}
			$output 	.=	"<tr><td>".$row->mr_tno."</td>";
			$output 	.=	"<td>".$row->mr_date."</td>";
			$output 	.=	"<td>".$row->mr_mrd."</td>";
			$output 	.=	"<td>".$row->u_name."</td>";
			$output 	.=	"<td>".$row->p_title." ".$row->p_name."</td>";
			$output 	.=	"<td>".$row->mr_remark."</td>";

			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."mrd_room/mrd_take/".$row->mr_id."'class='btn btn-primary view-btn-edit' title='Take MRD'><i class='fa fa-upload'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}

	function list_to_give_e()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('mrd_room.*,users.u_name,patient.p_title,patient.p_name');
		$this->db->from('mrd_room');
		$this->db->join('users','users.u_emp_id = mrd_room.mr_doc','inner');
		$this->db->join('patient','patient.p_mrd_no = mrd_room.mr_mrd', 'inner');
		$this->db->where('mr_status !=','1');
		$this->db->where('mr_status !=','2');
		$this->db->where('mr_shift','e');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{

			if($row->mr_shift=="m") {	$mr_shift = "Morning";	}
			else if($row->mr_shift=="e") {	$mr_shift = "Evening";	}
			else {	$mr_shift = "None";	}
			$output 	.=	"<tr><td>".$row->mr_tno."</td>";
			$output 	.=	"<td>".$row->mr_date."</td>";
			$output 	.=	"<td>".$row->mr_mrd."</td>";
			$output 	.=	"<td>".$row->u_name."</td>";
			$output 	.=	"<td>".$row->p_title." ".$row->p_name."</td>";
			$output 	.=	"<td>".$row->mr_remark."</td>";

			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."mrd_room/mrd_take/".$row->mr_id."'class='btn btn-primary view-btn-edit' title='Take MRD'><i class='fa fa-upload'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}

	function list_to_get_m()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('mrd_room.*,users.u_name,patient.p_title,patient.p_name');
		$this->db->from('mrd_room');
		$this->db->join('users','users.u_emp_id = mrd_room.mr_doc','inner');
		$this->db->join('patient','patient.p_mrd_no = mrd_room.mr_mrd', 'inner');
		$this->db->where('mr_status','2');
		$this->db->where('mr_shift','m');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{

			if($row->mr_shift=="m") {	$mr_shift = "Morning";	}
			else if($row->mr_shift=="e") {	$mr_shift = "Evening";	}
			else {	$mr_shift = "None";	}

			$output 	.=	"<tr><td>".$row->mr_tno."</td>";
			$output 	.=	"<td>".$row->mr_date."</td>";
			$output 	.=	"<td>".$row->mr_mrd."</td>";
			$output 	.=	"<td>".$row->u_name."</td>";
			$output 	.=	"<td>".$row->p_title." ".$row->p_name."</td>";
			$output 	.=	"<td>".$row->mr_remark."</td>";

			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."mrd_room/mrd_return/".$row->mr_id."'class='btn btn-primary view-btn-edit' title='Return MRD'><i class='fa fa-download'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}

	function list_to_get_e()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('mrd_room.*,users.u_name,patient.p_title,patient.p_name');
		$this->db->from('mrd_room');
		$this->db->join('users','users.u_emp_id = mrd_room.mr_doc','inner');
		$this->db->join('patient','patient.p_mrd_no = mrd_room.mr_mrd', 'inner');
		$this->db->where('mr_status','2');
		$this->db->where('mr_shift','e');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{

			if($row->mr_shift=="m") {	$mr_shift = "Morning";	}
			else if($row->mr_shift=="e") {	$mr_shift = "Evening";	}
			else {	$mr_shift = "None";	}

			$output 	.=	"<tr><td>".$row->mr_tno."</td>";
			$output 	.=	"<td>".$row->mr_date."</td>";
			$output 	.=	"<td>".$row->mr_mrd."</td>";
			$output 	.=	"<td>".$row->u_name."</td>";
			$output 	.=	"<td>".$row->p_title." ".$row->p_name."</td>";
			$output 	.=	"<td>".$row->mr_remark."</td>";

			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."mrd_room/mrd_return/".$row->mr_id."'class='btn btn-primary view-btn-edit' title='Return MRD'><i class='fa fa-download'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}


  
	function get_one_banner_mrd($id)
	{
		$this->db->select('*');
		$this->db->from('mrd_room');
		$this->db->where('mr_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}

	function mrd_adjust($mr_id)
	{
		$this->db->select('mrd_room.*,patient.p_title,patient.p_name,patient.p_phone');
		$this->db->from('mrd_room');
		$this->db->join('patient','patient.p_mrd_no = mrd_room.mr_mrd','inner');
		$this->db->where('mr_id',$mr_id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}

}