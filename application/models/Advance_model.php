<?php

class Advance_model extends CI_Model 
{
	function list_all()
	{
		 $output 		=	"";
		$sl_no 			=	0;

		$this->db->select('voucher_entry.*,patient.p_title,patient.p_name');
		$this->db->from('voucher_entry');
		$this->db->join('patient','patient.p_mrd_no=voucher_entry.ve_mrd','inner');
		$this->db->order_by('ve_id','desc');
		$this->db->where('ve_type','ad');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{
			$sl_no++;

			if($row->ve_pstaus=="NP") { $fp = "<font color='#b20000'>Not Completed</font>";  }
			elseif($row->ve_pstaus=="FP") { $fp = "<font color='#228B22'>Completed</font>";  }

			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->ve_vno."</td>";
			$output 	.=	"<td>".date("d-m-Y",strtotime($row->ve_date))."</td>";
		    $output 	.=	"<td>".$row->ve_customer."</td>";
		    $output 	.=	"<td>".$row->p_title." ".$row->p_name."</td>";
			$output 	.=	"<td>".$fp."</td>"; 
			 
			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='".$this->config->item('admin_url')."advance/view/".$row->ve_id."' class='btn btn-primary view-btn-edit' title='View' ><i class='fa fa-eye'></i></a>
			<a href='".$this->config->item('admin_url')."advance/getPrint/".$row->ve_id."' class='btn btn-success view-btn-edit' title='Print' ><i class='fa fa-floppy-o'></i></a>
			 <a href='".$this->config->item('admin_url')."advance/edit/".$row->ve_id."' class='btn btn-info view-btn-edit'' title='Edit' ><i class='fa fa-pencil-square-o'></i></a>
		   <a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."advance/delete/".$row->ve_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}


	function get_one_banner($id)
	{
		$this->db->select('voucher_entry.*,patient.p_title,patient.p_name,patient.p_phone');
		$this->db->from('voucher_entry');
		$this->db->join('patient','patient.p_mrd_no=voucher_entry.ve_mrd','inner');
		$this->db->where('ve_type','ad');
		$this->db->where('ve_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}

	public function getAdvanceId()
     {
		$this->db->select('CAST(ve_vno as SIGNED INTEGER) as ve_vno');
		$this->db->from('voucher_entry');
		$this->db->order_by('ve_vno','desc');
		// $this->db->where('ve_status','1');
		$this->db->where('ve_type','ad');
		$this->db->limit(1);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return '1';
		}
		else
		{
			$result 	= $query->row()->ve_vno;
			$result++;
			return $result;
		}
	}



}