<?php

class Priviligemodel extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('*');
		$this->db->from('customer');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{
			$sl_no++;
			if($row->status == '1')
			{
				$image = "<i class='fa fa-check'></i>";
				$status = 0;
			}
			else
			{
				$image = "<i class='fa fa-ban'></i>";
				$status = 1;
			}
			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->customer."</td>"; 
			// $output 	.=	"<td>".$row->address."</td>";
			// $output 	.=	"<td>".$row->street."</td>";
			// $output 	.=	"<td>".$row->city."</td>";
			// $output 	.=	"<td>".$row->zip."</td>";
			// $output 	.=	"<td>".$row->country."</td>";
			// $output 	.=	"<td>".$row->state."</td>";
			$output 	.=	"<td>".$row->phone."</td>";
			$output 	.=	"<td>".$row->email."</td>";
			// $output 	.=	"<td>".$row->fax."</td>";
			// $output 	.=	"<td>".$row->website."</td>";
			// $output 	.=	"<td><img src='".$this->config->item('image_url')."features/small/".$row->feature_image."'></td>";
			 
			// $output 	.=	"<td><a href='".$this->config->item('admin_url')."customer/active/".$row->id."/".$status."'>".$image."</a></td>";
			 $output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."priviliges/view/".$row->id."'class='btn btn-success view-btn-edit' title='View'><i class='fa fa-search'></i></a>
			 <a href='". $this->config->item('admin_url')."priviliges/edit/".$row->id."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a> 
			 <a href= '".$this->config->item('admin_url')."priviliges/delete/".$row->id."' class='btn btn-danger' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}

	function list_small($id)
	{
		$this->db->select('*');
		$this->db->from('customer');
		$this->db->where('id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}

  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('customer');
		$this->db->where('id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}
}