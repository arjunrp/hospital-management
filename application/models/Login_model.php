<?php

class Login_model extends CI_Model {

    function __construct() {
        parent::__construct();

        $this->load->database();
    }

    function login() {
       $username = $this->input->post('username');
       $password = $this->input->post('password');
       $query    = $this->db->get_where('users',array('u_email'=>$username,'u_password'=>$password));

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $rows) {
                $newdata = array(
                    
                    'username' => $rows->u_name,
                    'password' => $rows->u_password,
                    'id'       => $rows->u_emp_id,
                    'email'    => $rows->u_email,
                    'date'     => date("d-m-Y h:i:sa"),
                    'type'     => $rows->u_type,
                    'logo'     => $rows->u_image,
                    'u_type'   => $rows->u_type,
                    'u_dept'   => $rows->u_department,
                   
                    'logged_in' => TRUE,
                );
            }
            $this->session->set_userdata($newdata);
            return true;
        }else{
        return false;}
    }

}

?>
