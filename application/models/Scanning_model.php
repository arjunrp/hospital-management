<?php

class Scanning_model extends CI_Model 
{
	function list_all()
	{
		// <a href='".$this->config->item('admin_url')."investigation/edit/".$row->ve_id."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
		$output	=	"";
		$bk_date = date("Y-m-d");
		$ip_op 	= "";

		$this->db->select('voucher_entry.*');
		$this->db->from('voucher_entry');
		$this->db->order_by('voucher_entry.ve_date','desc');
		$this->db->where('voucher_entry.ve_type','scano');
		$this->db->or_where('voucher_entry.ve_type','scani');
		$query 			=	$this->db->get();
		foreach($query->result() as $row)
		{

			if($row->ve_pstaus=="NP") { $fp = "<font color='#b20000'>Not Completed</font>";  }
			elseif($row->ve_pstaus=="FP") { $fp = "<font color='#228B22'>Completed</font>";  }
			
			if($row->ve_type=="scani") { $ip_op 	= ""; }
			else if($row->ve_type=="scano") { $ip_op 	= "OP"; }

			if($row->ve_bill_no=="updated") { $status 	= "disabled"; $status1 	= ""; }
			else if($row->ve_bill_no=="") 	{ $status 	= ""; $status1 	= "disabled"; }

			$output 	.=	"<tr><td>".$row->ve_vno."</td>";
			$output 	.=	"<td>".date("d-m-Y h:i A",strtotime($row->ve_date))."</td>";
			$output 	.=	"<td>".$row->ve_doctor."</td>";
			$output 	.=	"<td>".$row->ve_mrd."</td>";
			$output 	.=	"<td>".$row->ve_patient."</td>";
			$output 	.=	"<td>".$row->ve_phone."</td>";
			$output 	.=	"<td>".$ip_op."</td>";
			$output 	.=	"<td>".$fp."</td>";

			$output 	.=	"<td style='text-align:center' class='btn-group-xs'><a href='".$this->config->item('admin_url')."scanning/field_value/".$row->ve_id."' $status class='btn btn-warning view-btn-edit' title='Field Value'><i class='fa fa-search'></i></a></td>";
			$output 	.=	"<td style='text-align:center' class='btn-group  btn-group-xs' ><a href='".$this->config->item('admin_url')."scanning/view/".$row->ve_id."' $status1 class='btn btn-primary view-btn-edit' title='View'><i class='fa fa-eye'></i></a>
			<a href='".$this->config->item('admin_url')."scanning/getBillPrint?Printid=".$row->ve_id."'class='btn btn-success view-btn-edit' title='Print'><i class='fa fa-print'></i></a>
			
			<a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."scanning/delete/".$row->ve_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	} 


  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('voucher_entry');
		$this->db->where('ve_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}

	function getVoucherDid($ve_id)
	{
		$this->db->select('ved_id');
		$this->db->from('voucher_entry_detail');
		$this->db->where('ved_veid',$ve_id);
		$query 			=	$this->db->get();
		return $query->result_array();
	}


	function get_category($inv_id)
	{
			$temp  	= array();
    		$sum   	= array();

			$this->db->select('voucher_entry_detail.*,scan_content.*');
			$this->db->from('voucher_entry');
			$this->db->join('voucher_entry_detail','voucher_entry_detail.ved_veid = voucher_entry.ve_id', 'inner'); 
			$this->db->join('scan_content','voucher_entry_detail.ved_itemid = scan_content.scnt_scname', 'inner'); 
			$this->db->where('voucher_entry.ve_id',$inv_id);
			// $this->db->where('investigation_detail.ind_status','1');
			$query                 =    $this->db->get();
    		$data                  =    $query->result_array();
    		// print_r($data);

    foreach($data as $key => $value)
    {   
        if(in_array($value['ved_itemid'],$temp)){
        	$temp2[$value['ved_itemid']]['ved_id'] 			=  $value['ved_id'];
        	$temp2[$value['ved_itemid']]['ved_itemid'] 		=  $value['ved_itemid'];
            $temp2[$value['ved_itemid']]['ved_item'] 		=  $value['ved_item'];
            $temp2[$value['ved_itemid']]['scnt_content']  	= $sum[$value['ved_itemid']]['scnt_content'] + $this->getScancontents($value['ved_itemid']);
        } else {
        	$temp2[$value['ved_itemid']]['ved_id'] 			=  $value['ved_id'];
        	$temp2[$value['ved_itemid']]['ved_itemid'] 		=  $value['ved_itemid'];
            $temp2[$value['ved_itemid']]['ved_item'] 		=  $value['ved_item'];
            $temp2[$value['ved_itemid']]['scnt_content']  	=  $this->getScancontents($value['ved_itemid']);
        } 

    }
		 return $temp2;
	}

	function get_categorys($inv_id)
	{
			$temp  	= array();
    		$sum   	= array();

			$this->db->select('voucher_entry_detail.*,investigation_detail.*');
			$this->db->from('voucher_entry');
			$this->db->join('voucher_entry_detail','voucher_entry_detail.ved_veid = voucher_entry.ve_id', 'inner'); 
			$this->db->join('investigation_detail','voucher_entry_detail.ved_id = investigation_detail.ind_inid', 'inner'); 
			$this->db->where('voucher_entry.ve_id',$inv_id);
			$this->db->where('investigation_detail.ind_status','1');
			$query                 =    $this->db->get();
    		$data                  =    $query->result_array();
    		// print_r($data);
    		// exit();
    foreach($data as $key => $value)
    {   
        if(in_array($value['ved_itemid'],$temp)){
        	$temp2[$value['ved_itemid']]['ind_id'] 			=  $value['ind_id'];
        	$temp2[$value['ved_itemid']]['ved_itemid'] 		=  $value['ved_itemid'];
            $temp2[$value['ved_itemid']]['ved_item'] 		=  $value['ved_item'];
            $temp2[$value['ved_itemid']]['scnt_content']  	= $sum[$value['ved_itemid']]['scnt_content'] + $this->getScancontentss($value['ved_id']);
        } else {
        	$temp2[$value['ved_itemid']]['ind_id'] 			=  $value['ind_id'];
        	$temp2[$value['ved_itemid']]['ved_itemid'] 		=  $value['ved_itemid'];
            $temp2[$value['ved_itemid']]['ved_item'] 		=  $value['ved_item'];
            $temp2[$value['ved_itemid']]['scnt_content']  	=  $this->getScancontentss($value['ved_id']);
        } 

    }
		 return $temp2;
	}

	function getScancontents($ved_id)
{    
    $this->db->select('*');
    $this->db->from('scan_content');
    $this->db->where('scnt_scname',$ved_id);
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->result_array();
    	return $rowcount;
	}
}

function getScancontentss($ved_id)
{    
    $this->db->select('investigation_detail.*,scan_content.*');
    $this->db->from('investigation_detail');
    $this->db->join('scan_content','scan_content.scnt_id = investigation_detail.ind_contentid', 'inner');
    $this->db->where('investigation_detail.ind_inid',$ved_id);
    $this->db->where('investigation_detail.ind_status','1');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->result_array();
    	return $rowcount;
	}
}


	function get_patient($op_mrd)
	{
		$this->db->select('*');
		$this->db->from('patient');
		$this->db->where('p_mrd_no',$op_mrd);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}


	function getScanId()
     {

  //    	$this->db->select('ve_vno');
  //     	$this->db->from('voucher_entry');
  //     	$this->db->order_by('ve_vno','desc');
		// $this->db->where('ve_type','scano');
		// $this->db->or_where('ve_type','scani');
  //     	$this->db->limit(1);
  //     	$query      = $this->db->get();
  //     	if($query->num_rows()==0)
  //     	{
  //       	$data = '1';
  //     	}
  //     	else
  //     	{
  //       	$end2   = $query->row()->ve_vno;
  //       	$end2++;
  //       	$data = $end2;
  //     	}

  //   	return $data;

		$this->db->select('ve_vno');
		$this->db->from('voucher_entry');
		$this->db->order_by('ve_vno','desc');
		$this->db->where('ve_type','scano');
		$this->db->or_where('ve_type','scani');
		$this->db->limit(1);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return '1';
		}
		else
		{
			$result 	= $query->row()->ve_vno;
			$result++;
			return $result;
		}
	}

	function get_scan_bill($inv_id)
	{
		$this->db->select('voucher_entry.*');
		$this->db->from('voucher_entry');
		// $this->db->join('patient','patient.p_mrd_no = voucher_entry.ve_mrd', 'inner');
		$this->db->where('voucher_entry.ve_id',$inv_id);
		$query 			=	$this->db->get();
		return $query->result_array();
	}

	public function getScanview($pid){

	$this->db->select('ve_type');
    $this->db->from('voucher_entry');
    $this->db->where('ve_id',$pid);
    $query=$this->db->get();
    $ve_type = $query->row()->ve_type;

    if($ve_type=="scani")
    {
    	$this->db->select('voucher_entry.*,voucher_entry_detail.*,scan.sc_test');
    	$this->db->from('voucher_entry');
   		$this->db->join('voucher_entry_detail','voucher_entry_detail.ved_veid = voucher_entry.ve_id', 'inner');
    	// $this->db->join('patient','patient.p_mrd_no = voucher_entry.ve_mrd', 'inner');
    	// $this->db->join('ip','ip.ip_ipno = voucher_entry.ve_customer', 'inner');
    	// $this->db->join('users','users.u_emp_id = ip.ip_doctor', 'inner');
    	$this->db->join('sc_name','sc_name.sn_id = voucher_entry_detail.ved_itemid', 'inner');
    	$this->db->join('scan','scan.sc_id = sc_name.sn_scanid', 'inner');
    	$this->db->where('voucher_entry.ve_id',$pid);
   		 // $this->db->where('voucher_entry.ve_type','lbo');
    	$this->db->where('voucher_entry.ve_type','scani');
    	$query=$this->db->get();
    	return $query->result_array();

    }

    if($ve_type=="scano")
    {
    	$this->db->select('voucher_entry.*,voucher_entry_detail.*,scan.sc_test');
    	$this->db->from('voucher_entry');
   		$this->db->join('voucher_entry_detail','voucher_entry_detail.ved_veid = voucher_entry.ve_id', 'inner');
    	// $this->db->join('patient','patient.p_mrd_no = voucher_entry.ve_mrd', 'inner');
    	// $this->db->join('op','op.op_id = voucher_entry.ve_customer', 'inner');
    	// $this->db->join('users','users.u_emp_id = op.op_doctor', 'inner');
    	$this->db->join('sc_name','sc_name.sn_id = voucher_entry_detail.ved_itemid', 'inner');
    	$this->db->join('scan','scan.sc_id = sc_name.sn_scanid', 'inner');
    	$this->db->where('voucher_entry.ve_id',$pid);
   		 $this->db->where('voucher_entry.ve_type','scano');
    	// $this->db->where('voucher_entry.ve_type','lbi');
    	$query=$this->db->get();
    	return $query->result_array();

    }

	
  }

	public function getDepartment(){
		$this->db->select('*');
		$this->db->from('department');
		$this->db->where('dp_status',1);
		$this->db->where('dp_type','t');
		$query=$this->db->get();
		return $query->result_array();
	}

	public function get_patientsip(){
		$this->db->select('ip.*,patient.p_title,patient.p_name,patient.p_phone,patient.p_age,patient.p_sex,department.dp_department,users.u_name');
		$this->db->from('ip');
		$this->db->join('patient','patient.p_mrd_no = ip.ip_mrd', 'inner');
		$this->db->join('department','department.dp_id = ip.ip_department', 'inner');
		$this->db->join('users','users.u_emp_id = ip.ip_doctor', 'inner');
		$this->db->where('ip.ip_dis','1');
		$query=$this->db->get();
		return $query->result_array();
	}
	public function get_patientsop(){
		
		$today 	=	date("Y-m-d");
		$this->db->select('op.*,patient.p_title,patient.p_name,patient.p_phone,patient.p_age,patient.p_sex,department.dp_department,users.u_name');
		$this->db->from('op');
		$this->db->join('patient','patient.p_mrd_no = op.op_mrd', 'inner');
		$this->db->join('users','users.u_emp_id = op.op_doctor', 'inner');
		$this->db->join('department','department.dp_id = op.op_department', 'inner');
		$this->db->where('op.op_date',$today);
		// $this->db->where('dp_type','t');
		$query=$this->db->get();
		return $query->result_array();
	}

}