<?php

class Patientmodel extends CI_Model 
{

	function get_current_page_records($limit, $start, $st = NULL, $st1 = NULL, $st2 = NULL, $st3 = NULL, $st4 = NULL)
    {
        if ($st == "NIL") 	$st  = "";
        if ($st1 == "NIL") 	$st1 = "";
        if ($st2 == "NIL") 	$st2 = "";
        if ($st3 == "NIL") 	$st3 = "";
        if ($st4 == "NIL")  $st4 = "";
        $sql = "select * from mw_patient where p_mrd_no like '%$st%' AND p_name like '%$st1%' AND p_phone like '%$st2%' AND p_address like '%$st3%' AND p_guardian like '%$st4%' limit " . $start . ", " . $limit;
        $query = $this->db->query($sql);
        if($query->num_rows()!=0)
        {
        	return $query->result();	
        }
    }

    function get_current_page_records_name($limit, $start, $st = NULL)
    {
        if ($st == "NIL") $st = "";
        $sql = "select * from mw_patient where p_name like '%$st%' limit " . $start . ", " . $limit;
        $query = $this->db->query($sql);
        if($query->num_rows()!=0)
        {
        	return $query->result();	
        }
    }

    function get_current_page_records_phone($limit, $start, $st = NULL)
    {
        if ($st == "NIL") $st = "";
        $sql = "select * from mw_patient where p_phone like '%$st%' limit " . $start . ", " . $limit;
        $query = $this->db->query($sql);
        if($query->num_rows()!=0)
        {
        	return $query->result();	
        }
    }

    function get_current_page_records_address($limit, $start, $st = NULL)
    {
        if ($st == "NIL") $st = "";
        $sql = "select * from mw_patient where p_address like '%$st%' limit " . $start . ", " . $limit;
        $query = $this->db->query($sql);
        if($query->num_rows()!=0)
        {
        	return $query->result();	
        }
    }
    
    function get_total($st = NULL,$st1 = NULL,$st2 = NULL,$st3 = NULL,$st4 = NULL)
    {
        if ($st == "NIL") $st 	= "";
        if ($st1 == "NIL") $st1 = "";
        if ($st2 == "NIL") $st2 = "";
        if ($st3 == "NIL") $st3 = "";
        if ($st4 == "NIL") $st4 = "";
        $sql = "select * from mw_patient where p_mrd_no like '%".$st."%' AND p_name like '%".$st1."%' AND p_phone like '%".$st2."%' AND p_address like '%".$st3."%' AND p_guardian like '%".$st4."%'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_total_name($st = NULL)
    {
        if ($st == "NIL") $st = "";
        $sql = "select * from mw_patient where p_name like '%$st%'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    
    function get_total_phone($st = NULL)
    {
        if ($st == "NIL") $st = "";
        $sql = "select * from mw_patient where p_phone like '%$st%'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_total_address($st = NULL)
    {
        if ($st == "NIL") $st = "";
        $sql = "select * from mw_patient where p_address like '%$st%'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

	// public function record_count() {
 //        return $this->db->where('p_status','1')->from("patient")->count_all_results();
 //    }

 //    public function fetch_data($limit, $id) {
 //        $this->db->limit($limit);
 //        $this->db->where('p_id', $id);
 //        $query = $this->db->get("patient");
 //        if ($query->num_rows() > 0) {
 //            foreach ($query->result() as $row) {
 //                $data[] = $row;
 //            }
         
 //            return $data;
 //        }
 //        return false;
 //   }

	function cancelled_mrd()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('p_mrd_no');
		$this->db->from('patient');
		$this->db->order_by('p_mrd_no','asc');
		$this->db->where('p_status','0');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{

			$output 	.=	"<tr><td>".$row->p_mrd_no."</td>"; 
			$output 	.=	"<td class='btn-xs'><a href='". $this->config->item('admin_url')."patient/cancelled/".$row->p_mrd_no."'class='btn btn-info view-btn-edit' title='Use this MRD'><i class='fa fa-eye'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}

	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;
		$i 				=	1;
		$x 				=	array();
		$y 				=	array();
		$new1 = array();

		$this->db->select('*');
		$this->db->from('patient');
		$this->db->order_by('p_mrd_no','asc');
		$this->db->where('p_status','1');
		// $this->db->where('YEAR(p_date) =','2017');
		$this->db->limit(100);
		$query 	=	$this->db->get();

		foreach($query->result() as $row)
		{
			$sl_no++;
			$output 	.=	"<tr><td>".$row->p_mrd_no."</td>"; 
			$output 	.=	"<td>".$row->p_title." ".$row->p_name."</td>"; 
			$output 	.=	"<td>".$row->p_phone."</td>";
			$output 	.=	"<td>".$row->p_age."</td>";
			$output 	.=	"<td>".$row->p_blood."</td>";
			 $output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."patient/view/".$row->p_mrd_no."'class='btn btn-primary view-btn-edit' title='View'><i class='fa fa-eye'></i></a>
			 <a href='". $this->config->item('admin_url')."patient/edit/".$row->p_mrd_no."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>  
			  <a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."patient/delete/".$row->p_mrd_no."' href='javascript:void(0)' title='Deelete'><i class='fa fa-times-circle-o'></i></a></td>";
			$output		.=	"</tr>";
		}
		// return $output;
		return $query->result_array();
	}

	function list_all_merge()
	{
		$output 		=	"";


		$this->db->select('p_mrd_no,p_title,p_name,p_phone,p_age,p_blood');
		$this->db->from('patient');
		$this->db->order_by('p_mrd_no','asc');
		$this->db->where('p_status','1');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{

			$output 	.=	"<tr><td>".$row->p_mrd_no."</td>"; 
			$output 	.=	"<td>".$row->p_title." ".$row->p_name."</td>"; 
			$output 	.=	"<td>".$row->p_phone."</td>";
			$output 	.=	"<td>".$row->p_age."</td>";
			$output 	.=	"<td>".$row->p_blood."</td>";
			$output		.=	"</tr>";
		}
		return $output;
	}



  
	function get_one_banner($p_mrd_no)
	{
		$this->db->select('*');
		$this->db->from('patient');
		$this->db->where('p_mrd_no',$p_mrd_no);

		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}



		function get_country()
	{
			$this->db->select('*');
			$this->db->from('countries'); 
			$query=$this->db->get();
			return $query->result_array();
	}


function get_city()
	{
			$this->db->select('*');
			$this->db->from('cities'); 
			$query=$this->db->get();
			return $query->result_array();
	}

	function get_states($states)
	{
			$query=$this->db->get_where('states',array('country_id'=>$states));
			return $query->result_array();
	}
	function get_citys($citys)
	{
			$query=$this->db->get_where('cities',array('state_id'=>$citys));
			return $query->result_array();
	}

function get_mrd_even()
{

	$query = $this->db->query('SELECT p_mrd_no FROM mw_patient WHERE (p_mrd_no % 2) = 0 ORDER BY p_mrd_no DESC LIMIT 1');
	//$query 			=	$his->db->get();

	if($query->num_rows()==0)
	{
		return 2;
	}
	else
	{
		$result 	= $query->row()->p_mrd_no+2;
		return $result;
	}
}


function get_mrd_odd()
{

	$query = $this->db->query('SELECT p_mrd_no FROM mw_patient WHERE (p_mrd_no % 2) != 0 ORDER BY p_mrd_no DESC LIMIT 1');
	//$query 			=	$his->db->get();

	if($query->num_rows()==0)
	{
		return 1;
	}
	else
	{
		$result 	= $query->row()->p_mrd_no+2;
		return $result;
	}
}

function get_reg_fee()
	{
			$this->db->select('reg_fee');
			$this->db->from('company'); 
			$query=$this->db->get();
			return $query->row()->reg_fee;
	}
function get_renewfee()
	{
			$this->db->select('renew_fee');
			$this->db->from('company'); 
			$query=$this->db->get();
			return $query->row()->renew_fee;
	}

	function check_mrd_dupe($title)
	{
			$this->db->select('p_mrd_no');
			$this->db->from('patient');
			$this->db->where('p_mrd_no',$title); 
			$this->db->where('p_status','0');
			$query=$this->db->get();
			if($query->num_rows()==0)
			{ return 0; }
			else { return 1; }
	}


	function getCompany()
	{
			$this->db->select('*');
			$this->db->from('company'); 
			$query=$this->db->get();
			return $query->result_array();
	}

	public function getPatientRgview($sid){
	$this->db->select('voucher_entry.*,patient.p_title,patient.p_name,patient.p_phone');
    $this->db->from('voucher_entry');
    $this->db->join('patient','patient.p_mrd_no = voucher_entry.ve_mrd', 'inner');
    $this->db->join('users','users.u_emp_id = voucher_entry.ve_user', 'inner'); 
    $this->db->where('voucher_entry.ve_mrd',$sid);
    $this->db->where('ve_type','preg');
    $query=$this->db->get();
    return $query->result_array();
}
public function getPatientRwview($sid){
	$this->db->select('voucher_entry.*,patient.p_title,patient.p_name,patient.p_phone');
    $this->db->from('voucher_entry');
    $this->db->join('patient','patient.p_mrd_no = voucher_entry.ve_mrd', 'inner');
    // $this->db->join('users','users.u_emp_id = voucher_entry.ve_user', 'inner'); 
    $this->db->where('voucher_entry.ve_mrd',$sid);
    $this->db->where('ve_type','prenew');
    $this->db->order_by('voucher_entry.ve_id','desc');
    $query=$this->db->get();
    return $query->result_array();
}

		public function getPatientRegno(){
		$this->db->select('ve_vno');
		$this->db->from('voucher_entry');
		$this->db->order_by('ve_id','desc');
		$this->db->where('ve_status','1');
		$this->db->where('ve_type','preg');
		$this->db->limit(1);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return '1';
		}
		else
		{
			$result 	= $query->row()->ve_vno + 1;
			// $result++;
			return $result;
		}

	}

	public function getPatientRenewno(){
		$this->db->select('ve_vno');
		$this->db->from('voucher_entry');
		$this->db->order_by('ve_id','desc');
		$this->db->where('ve_status','1');
		$this->db->where('ve_type','prenew');
		$this->db->limit(1);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return '1';
		}
		else
		{
			$result 	= $query->row()->ve_vno + 1;
			// $result++;
			return $result;
		}

	}

	 function list_to_give()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('*,department.dp_department');
		$this->db->from('mrd_room');
		$this->db->join('department','department.dp_id = mrd_room.mr_tdepartment','inner');
		$this->db->where('mr_status !=','1');
		$this->db->where('mr_status !=','2');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{

			$output 	.=	"<tr><td>".$row->mr_mrd."</td>";
			$output 	.=	"<td>".$row->dp_department."</td>";
			$output 	.=	"<td>".$row->mr_tdate."</td>";

			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."patient/mrd_take/".$row->mr_id."'class='btn btn-primary view-btn-edit' title='Take MRD'><i class='fa fa-upload'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}

	function list_to_get()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('*,department.dp_department');
		$this->db->from('mrd_room');
		$this->db->join('department','department.dp_id = mrd_room.mr_department','inner');
		$this->db->where('mr_status','2');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{


			$output 	.=	"<tr><td>".$row->mr_mrd."</td>";
			$output 	.=	"<td>".$row->dp_department."</td>";
			$output 	.=	"<td>".$row->mr_date."</td>";

			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."patient/mrd_return/".$row->mr_id."'class='btn btn-primary view-btn-edit' title='Return MRD'><i class='fa fa-download'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}


  
	function get_one_banner_mrd($id)
	{
		$this->db->select('*');
		$this->db->from('mrd_room');
		$this->db->where('mr_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}

}