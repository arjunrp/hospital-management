<?php

class Stock_adjust_model extends CI_Model {

	function __construct() {
		parent::__construct();

		$this->load->database();
	}
 
			// <a href='".$this->config->item('admin_url')."stock_adjust/edit/".$row->ve_id."'class='".$status." btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
	function list_all()
	{
		$output 		=	"";
		$status 	 	=	"1";
		$this->db->select('voucher_entry.*,dp1.dp_department');
    	$this->db->from('voucher_entry');
    	$this->db->join('department dp1','dp1.dp_id = voucher_entry.ve_supplier', 'left');
    	$this->db->order_by('voucher_entry.ve_vno','desc');
    	$this->db->where('voucher_entry.ve_type','sta');
    	$query=$this->db->get();
		foreach($query->result() as $row)
		{

			$output 	.=	"<tr><td>".$row->ve_vno."</td>";
			$output 	.=	"<td>".date("d-m-Y",strtotime($row->ve_date))."</td>";
			$output 	.=	"<td>".$row->dp_department."</td>";
			 

			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."stock_adjust/stock_adjust_View/".$row->ve_id."'class='btn btn-primary view-btn-edit' title='View'><i class='fa fa-eye'></i></a>
			 <a class='".$status." btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."stock_adjust/delete/".$row->ve_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	}
	
	public function set_sadjust_stock($stockpid,$purchase_unit_qty,$ved_batch,$ve_supplier,$ve_date){
		
		$this->db->select('stock_id,stock_qty,stock_date,created_at');
		$this->db->from('stock');
		$this->db->where('stock_product_id',$stockpid);
		$this->db->where('stock_dept',$ve_supplier);
		$this->db->where('stock_batch',$ved_batch);
		$this->db->where('stock_status','standby');
		$this->db->order_by("stock_id", "desc");
		$this->db->limit(1);
		$result  		=   $this->db->get();
		$number_of_rows = 	$result->num_rows();

		if($number_of_rows > 0)
		{
			$sid						=	$result->row()->stock_id;
			$sqty						=	$result->row()->stock_qty;
			$sdate 						=	$result->row()->stock_date;
			$data['stock_qty'] 			=	$sqty + $purchase_unit_qty;	
			$data['stock_date']			=	$ve_date;
			$data['stock_batch']		=	$ved_batch;
			$data['stock_dept']			=	$ve_supplier;
			$data['stock_product_id']	=	$stockpid;
			$data['created_at']			=	$result->row()->created_at;
			if($sdate == $ve_date)
			{
				$data['stock_last']			= 	$sqty;
				$data['stock_moved']		=	$purchase_unit_qty;
				$data['stock_status']		=	'standby';
				$this->db->update('stock',$data, array('stock_id' => $sid));
			}
			else
			{
				$data['stock_last']			= 	$sqty;
				$data['stock_moved']		=	$purchase_unit_qty;
				$data['stock_status']		=	'standby';
				$this->db->insert('stock',$data);	

				$data1['stock_status']		=	"transfer";	
				$this->db->update('stock',$data1, array('stock_id' => $sid));
			}
		}
		else
		{
			$data['stock_qty '] 		=	0	+	$purchase_unit_qty;
			$data['stock_date ']		=	$ve_date;	
			$data['stock_product_id']	=	$stockpid;
			$data['stock_batch']		=	$ved_batch;
			$data['stock_dept']			=	$ve_supplier;
			$data['stock_status']		=	"standby";
			$data['created_at']			=	date('Y-m-d H:i:s.') . gettimeofday()['usec'];	
			$this->db->insert('stock',$data);
		}
	}

	public function delete_sadjust_stock($stockpid,$purchase_item_qty,$purchase_date,$ved_batch,$ve_supplier){
		
		$this->db->select('*');
		$this->db->from('stock');
		$this->db->where('stock_product_id',$stockpid);
		$this->db->where('stock_dept',$ve_supplier);
		$this->db->where('stock_batch',$ved_batch);
		$this->db->where('stock_date >=',$purchase_date);
		$this->db->order_by("stock_id", "asc");
		$query  		=    $this->db->get();
		$result 		=    $query->result_array();
		foreach ($result as $value) {
		$sid						=	$value['stock_id'];
		$sqty						=	$value['stock_qty'];
		$data['stock_qty'] 			=	$sqty - $purchase_item_qty;

		$this->db->update('stock',$data, array('stock_id' => $sid));
		}

	}

	public function update_sadjust_stock($stockpid,$purchase_item_qty,$purchase_date,$ved_batch,$ve_supplier){
		
		$this->db->select('*');
		$this->db->from('stock');
		$this->db->where('stock_product_id', $stockpid);
		$this->db->where('stock_dept',$ve_supplier);
		$this->db->where('stock_batch',$ved_batch);
		$this->db->where('stock_date >=',$purchase_date);
		$this->db->order_by("stock_id", "asc");
		$query  				= $this->db->get();
		$number_of_rows 		= $query->num_rows();
		if($number_of_rows != 0){
		$result 		=    $query->result_array();
		foreach ($result as $value) {
		$sid						=	$value['stock_id'];
		$sqty						=	$value['stock_qty'];
		$data['stock_qty'] 			=	$sqty + $purchase_item_qty;
		$this->db->update('stock',$data, array('stock_id' => $sid));
		}
	}
	else
		{
			$data['stock_qty '] 		=	0	+	$purchase_item_qty;
			$data['stock_date ']		=	$purchase_date;	
			$data['stock_product_id']	=	$stockpid;
			$data['stock_batch']		=	$ved_batch;
			$data['stock_dept']			=	$ve_supplier;
			$data['stock_status']		=	"standby";
			$datas['created_at']		=	date('Y-m-d H:i:s.') . gettimeofday()['usec'];	

			$this->db->insert('stock',$data);
		}

	}

	public function getSAId(){
		$this->db->select('ve_vno');
		$this->db->from('voucher_entry');
		$this->db->order_by('ve_vno','desc');
		$this->db->where('ve_type','sta');
		$this->db->limit(1);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return '1';
		}
		else
		{
			$result 	= $query->row()->ve_vno;
			$result++;
			return $result;
		}

	}

	public function getProducts($department){

	$this->db->select('voucher_entry_detail.*,stock.stock_qty');
		$this->db->from('stock');
		$this->db->join('voucher_entry_detail','voucher_entry_detail.ved_itemid = stock.stock_product_id AND voucher_entry_detail.ved_batch = stock.stock_batch', 'inner');
		$this->db->group_by('stock.stock_product_id');
    	$this->db->group_by('stock.stock_batch');
    	$this->db->order_by('stock.created_at','asc');
    	$this->db->where('stock.stock_qty > ','0');
    	$this->db->where('stock.stock_dept',$department);
    	$this->db->where('stock.stock_status','standby');
    	$query=$this->db->get();
    	return $query->result_array();
	}

	public function getCProducts(){

	$this->db->select('voucher_entry_detail.*,stock.stock_qty');
		$this->db->from('stock');
		$this->db->join('voucher_entry_detail','voucher_entry_detail.ved_itemid = stock.stock_product_id AND voucher_entry_detail.ved_batch = stock.stock_batch', 'inner');
		$this->db->group_by('stock.stock_product_id');
    	$this->db->group_by('stock.stock_batch');
    	$this->db->order_by('stock.created_at','asc');
    	$this->db->where('stock.stock_qty > ','0');
    	$this->db->where('stock.stock_status','standby');
    	$query=$this->db->get();
    	return $query->result_array();
	}



public function getStadjustview($sid){
	$this->db->select('voucher_entry.*,voucher_entry_detail.*,users.u_emp_id');
    $this->db->from('voucher_entry');
    $this->db->join('voucher_entry_detail','voucher_entry_detail.ved_veid = voucher_entry.ve_id', 'inner');
    $this->db->join('users','users.u_emp_id = voucher_entry.ve_user', 'inner'); 
    $this->db->where('voucher_entry.ve_id',$sid);
    $this->db->where('voucher_entry.ve_type','sta');
    $query=$this->db->get();
    return $query->result_array();
}

function productGetbydept()
  {
    $this->db->select('voucher_entry_detail.ved_itemid,voucher_entry_detail.ved_item,voucher_entry_detail.ved_batch,voucher_entry_detail.ved_expiry,voucher_entry_detail.ved_unit,voucher_entry_detail.ved_uqty,stock.stock_qty,stock.created_at,product.pd_qty,product.pd_unit');
    $this->db->from('stock');
    $this->db->join('voucher_entry_detail','voucher_entry_detail.ved_itemid = stock.stock_product_id AND voucher_entry_detail.ved_batch = stock.stock_batch', 'inner');
    $this->db->join('product','stock.stock_product_id = product.pd_code', 'inner');
    $this->db->group_by('stock.stock_product_id');
    $this->db->group_by('stock.stock_batch');
    $this->db->order_by('stock.created_at','asc');
    $this->db->where('stock.stock_qty > ','0');
    $this->db->where('stock.stock_dept','15');
    $this->db->where('stock.stock_status','standby');

    $query=$this->db->get();
    return $query->result_array();

  }


	}

	?>
