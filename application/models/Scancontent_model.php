<?php

class Scancontent_model extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;
    // $sql="SELECT E.*,EC.* FROM mw_test_name AS E INNER JOIN mw_test_content AS EC WHERE E.tn_id=EC.tc_tnameid AND E.tn_status=1 AND EC.tc_status=1 order by EC.tc_id asc";

		$this->db->select('sc_name.*,scan_content.*');
    	$this->db->from('scan_content');
    	$this->db->join('sc_name','sc_name.sn_id =scan_content.scnt_scname', 'left');
    	$this->db->order_by('sc_name.sn_id','asc');
    	$query 			=	$this->db->get();
		// $this->db->select('*');
		// $this->db->from('expence');
		// 

		foreach($query->result() as $row)
		{
			$sl_no++;
			if($row->scnt_status == '1')
			{
				$image = "<i class='fa fa-check'></i>";
				$scnt_status = 0;
			}
			else
			{
				$image = "<i class='fa fa-ban'></i>";
				$status = 1;
			}
			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->sn_name."</td>";
			$output 	.=	"<td>".$row->scnt_content."</td>";
			$output 	.=	"<td>".$row->scnt_men."</td>";
			$output 	.=	"<td>".$row->scnt_women."</td>";
			$output 	.=	"<td>".$row->scnt_chid."</td>";
			 
			 
			// $output 	.=	"<td><img src='".$this->config->item('image_url')."features/small/".$row->feature_image."'></td>";
			 
			// $output 	.=	"<td><a href='".$this->config->item('admin_url')."expence/active/".$row->id."/".$status."'>".$image."</a></td>";
			// $output 	.=	"<td class='btn-group  btn-group-xs'><a href='".$this->config->item('admin_url')."expence/edit/".$row->id."' class='btn btn-info view-btn-edit' title='Edit' ><i class='fa fa-pencil-square-o'></i></a> 
			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='".$this->config->item('admin_url')."scancontent/edit/".$row->scnt_id."' class='btn btn-primary view-btn-edit' title='Edit' ><i class='fa fa-pencil-square-o'></i></a>
			 
		   <a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."scancontent/delete/".$row->scnt_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>";
			// <a  href='".$this->config->item('admin_url')."expence/delete/".$row->id."'class='btn btn-danger' title='Delete'><i class='fa fa-times-circle-o'></i></a>";
			// $output		.=	"<td><a href='".$this->config->item('admin_url')."expence/delete/".$row->id."'><i class='fa fa-trash'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}


function get_category()
	{
		$this->db->select('*');
		$this->db->from('sc_name');
		$query 			=	$this->db->get();
		return $query->result_array();
	}
  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('scan_content');
		$this->db->where('scnt_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}
}