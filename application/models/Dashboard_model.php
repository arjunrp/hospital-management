<?php

class Dashboard_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }
	

 public function count_all()
{
        $mnth=date("m");
        $yr=date("Y");
        $this->db->select('*');
		$this->db->from('sales');
        $this->db->where('MONTH(sale_date)',$mnth);
        $this->db->where('YEAR(sale_date)',$yr);
        $query          =   $this->db->get();
        return $query->num_rows();
}


public function count_pur()
{
        $mnth=date("m");
        $yr=date("Y");
        $this->db->select('*');
		$this->db->from('purchase');
        $this->db->where('MONTH(purchase_date)',$mnth);
        $this->db->where('YEAR(purchase_date)',$yr);
        $query          =   $this->db->get();
        return $query->num_rows();
}




public function count_expence()
{
 
        $mnth=date("m");
        $yr=date("Y");
        $this->db->select('sum(amount) as amount');
        $this->db->from('expence');
        $this->db->where('MONTH(exdate)',$mnth);
        $this->db->where('YEAR(exdate)',$yr);
        $query          =   $this->db->get();
        $this->db->select('sum(amount_paid) as amount2');
        $this->db->from('purchase');
        $this->db->where('MONTH(purchase_date)',$mnth);
        $this->db->where('YEAR(purchase_date)',$yr);
        $query1          =   $this->db->get();
        $this->db->select('sum(preturn_amount) as pamount');
        $this->db->from('preturn');
        $this->db->where('MONTH(preturn_date)',$mnth);
        $this->db->where('YEAR(preturn_date)',$yr);
        $this->db->where('preturn_type','1');
        $query2          =   $this->db->get();
        return $query->row()->amount+$query1->row()->amount2-$query2->row()->pamount;
     

}
    

public function count_sale()
{
        $mnth=date("m");
        $yr=date("Y");
        $this->db->select('sum(amount_paid) as amount_paid');
        $this->db->from('sales');
        $this->db->where('MONTH(sale_date)',$mnth);
        $this->db->where('YEAR(sale_date)',$yr);
        $query          =   $this->db->get();
        $this->db->select('sum(preturn_amount) as pamount');
        $this->db->from('preturn');
        $this->db->where('MONTH(preturn_date)',$mnth);
        $this->db->where('YEAR(preturn_date)',$yr);
        $this->db->where('preturn_type','2');
        $query1          =   $this->db->get();
        return $query->row()->amount_paid-$query1->row()->pamount; 
}

public function credit_sum()
{
        $mnth=date("m");
        $yr=date("Y");
        $this->db->select('sum(amount_balance) as balance,sum(amount_payable) as amount');
        $this->db->from('purchase');
        $this->db->where('MONTH(purchase_date)',$mnth);
        $this->db->where('YEAR(purchase_date)',$yr);
        $this->db->where('paid','NP');
        $query              = $this->db->get();
        $data1['balance']   = $query->row()->balance;
        $data1['amount']    = $query->row()->amount; 
        
        if($data1['balance']==NULL) {
            $data1['balance']   =   '0';
            $data1['amount']    =   '100';
        }
        return $data1;
}

public function debit_sum()
{
        $mnth=date("m");
        $yr=date("Y");
        $this->db->select('sum(amount_balance) as balance1,sum(amount_payable) as amount1');
        $this->db->from('sales');
        $this->db->where('MONTH(sale_date)',$mnth);
        $this->db->where('YEAR(sale_date)',$yr);
        $this->db->where('paid','NP');
        $query               = $this->db->get();
        $data2['balance']   = $query->row()->balance1;
        $data2['amount']    = $query->row()->amount1; 
        if($data2['balance']==NULL) {
            $data2['balance']   =   '0';
            $data2['amount']    =   '100';
        }
        return $data2;
}


public function graph_report()
{
            $temp   = array();
            $temp2  = array();
            $tep2   = array();
            $sum    = array();
            $count=array();

            $thatday=date('Y-m-01', strtotime('-5 month'));
            $theday=date('Y-m-d');
            $mnth=date("m");


            $this->db->select('*');
            $this->db->from('balance');
            $this->db->order_by('balance_date','asc');
            $this->db->where('balance_date BETWEEN "'.$thatday. '" and "'. $theday.'"');
            // $this->db->where('balance_date <=',$theday);
            $query                 =    $this->db->get();
            $data                  =    $query->result_array();
    foreach($data as $key => $value)
    {   

        if(in_array(date('m',strtotime($value['balance_date'])) && date('Y',strtotime($value['balance_date'])),$temp)){

             $temp2[date('m',strtotime($value['balance_date']))]['graph_month']       = date('M',strtotime($value['balance_date']));
             $temp2[date('m',strtotime($value['balance_date']))]['graph_year']       = date('y',strtotime($value['balance_date']));
            $temp2[date('m',strtotime($value['balance_date']))]['cost_sum']       = $this->getPurchase(date('m',strtotime($value['balance_date'])),date('Y',strtotime($value['balance_date'])))+$this->getExpense(date('m',strtotime($value['balance_date'])),date('Y',strtotime($value['balance_date'])))-$this->getPReturn(date('m',strtotime($value['balance_date'])),date('Y',strtotime($value['balance_date'])));
            $temp2[date('m',strtotime($value['balance_date']))]['revenue_sum']       = $this->getSale(date('m',strtotime($value['balance_date'])),date('Y',strtotime($value['balance_date'])))-$this->getSReturn(date('m',strtotime($value['balance_date'])),date('Y',strtotime($value['balance_date'])));
            $temp2[date('m',strtotime($value['balance_date']))]['profit_sum']       = -(date('m',strtotime($value['balance_date']))['cost_sum']) + date('m',strtotime($value['balance_date']))['revenue_sum'] ;


        } else {
             $temp2[date('m',strtotime($value['balance_date']))]['graph_month']       = date('M',strtotime($value['balance_date']));
             $temp2[date('m',strtotime($value['balance_date']))]['graph_year']       = date('y',strtotime($value['balance_date']));
            $temp2[date('m',strtotime($value['balance_date']))]['cost_sum']       = $this->getPurchase(date('m',strtotime($value['balance_date'])),date('Y',strtotime($value['balance_date'])))+$this->getExpense(date('m',strtotime($value['balance_date'])),date('Y',strtotime($value['balance_date'])))-$this->getPReturn(date('m',strtotime($value['balance_date'])),date('Y',strtotime($value['balance_date'])));
             $temp2[date('m',strtotime($value['balance_date']))]['revenue_sum']       = $this->getSale(date('m',strtotime($value['balance_date'])),date('Y',strtotime($value['balance_date'])))-$this->getSReturn(date('m',strtotime($value['balance_date'])),date('Y',strtotime($value['balance_date'])));
            $temp2[date('m',strtotime($value['balance_date']))]['profit_sum']       = $temp2[date('m',strtotime($value['balance_date']))]['revenue_sum'] - $temp2[date('m',strtotime($value['balance_date']))]['cost_sum'] ;

        } 

    }
         return $temp2;
            
        }



function getPurchase($mnth,$yr)
{    
    $this->db->select('sum(amount_paid) as amount_paid');
    $this->db->from('purchase');
    // $this->db->where('purchase_date',$_date);
    $this->db->where('MONTH(purchase_date)="'.$mnth. '" and YEAR(purchase_date)="'. $yr.'"');

    $query = $this->db->get();
    if($query->num_rows()==0)
    {
        return 0;
    }
    else
    {
        $rowcount = $query->row()->amount_paid;
        return $rowcount;
    }
}

function getSale($mnth,$yr)
{    
    $this->db->select('sum(amount_paid) as amount_paid');
    $this->db->from('sales');
    $this->db->where('MONTH(sale_date)="'.$mnth. '" and YEAR(sale_date)="'. $yr.'"');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
        return 0;
    }
    else
    {
        $rowcount = $query->row()->amount_paid;
        return $rowcount;
    }
}


function getExpense($mnth,$yr)
{    
    $this->db->select('sum(amount) as amount');
    $this->db->from('expence');
    $this->db->where('MONTH(exdate)="'.$mnth. '" and YEAR(exdate)="'. $yr.'"');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
        return 0;
    }
    else
    {
        $rowcount = $query->row()->amount;
        return $rowcount;
    }
}

function getSReturn($mnth,$yr)
{    
    $this->db->select('sum(preturn_amount) as rsamount');
    $this->db->from('preturn');
    $this->db->where('MONTH(preturn_date)="'.$mnth. '" and YEAR(preturn_date)="'. $yr.'"');
    $this->db->where('preturn_type','2');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
        return 0;
    }
    else
    {
        $rowcount = $query->row()->rsamount;
        return $rowcount;
    }
}
function getPReturn($mnth,$yr)
{    
    $this->db->select('sum(preturn_amount) as rpamount');
    $this->db->from('preturn');
    $this->db->where('MONTH(preturn_date)="'.$mnth. '" and YEAR(preturn_date)="'. $yr.'"');
    $this->db->where('preturn_type','1');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
        return 0;
    }
    else
    {
        $rowcount = $query->row()->rpamount;
        return $rowcount;
    }
}


}
?>
