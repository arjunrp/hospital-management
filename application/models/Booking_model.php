<?php

class Booking_model extends CI_Model 
{
	function list_all()
	{
		$output	=	"";
		$shift 	=	"";
		$bk_date = date("Y-m-d");
		$this->db->select('booking.*,users.u_name');
		$this->db->from('booking');
		$this->db->join('users','users.u_emp_id = booking.bk_doc', 'inner');
		$this->db->where('booking.bk_status','1');
		$this->db->where('booking.bk_date >=',$bk_date);
		$this->db->order_by('booking.bk_doc','asc');
		$query 			=	$this->db->get();
		foreach($query->result() as $row)
		{

			$user_type = $this->session->type;
			$hide 	=	"";
			$hide1 	=	"";
			if($row->bk_mrd==0 || $row->bk_status==0)
			{
				$hide = " disabled";
			}
			if($user_type != 7 && $user_type != 3)
			{
				$hide1 = " disabled";
			}

			if($row->bk_shift=="m") { $shift = "Morning"; }
			else if($row->bk_shift=="e") { $shift = "Evening"; }
			$output 	.=	"<tr><td>".$row->bk_no."</td>";
			$output 	.=	"<td>".date('d-m-Y',strtotime($row->bk_date))."</td>";
			$output 	.=	"<td>".$row->u_name."</td>";
			$output 	.=	"<td>".$shift."</td>";
			$output 	.=	"<td>".$row->bk_mrd."</td>";
			$output 	.=	"<td>".$row->bk_name."</td>";
			$output 	.=	"<td>".$row->bk_phone."</td>";

			$output 	.=	"<td class='btn-group  btn-group-xs' >
			<a href='".$this->config->item('admin_url')."opregister/book_add/".$row->bk_id."'class='btn btn-primary view-btn-edit' ".$hide." title='Add to OP'><i class='fa fa-stethoscope'></i></a>
			<a href='".$this->config->item('admin_url')."booking/edit/".$row->bk_id."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
			<a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."booking/delete/".$row->bk_id."' href='javascript:void(0)' ".$hide1." title='Delete'><i class='fa fa-times-circle-o'></i></a>
			</td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	} 

  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('booking');
		$this->db->where('bk_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}

	function get_token()
	{
		$bk_date = date("Y-m-d",strtotime('tomorrow'));
		$this->db->select('bk_no');
		$this->db->from('booking');
		$this->db->where('bk_date',$bk_date);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return '01';
		}
		else
		{
			$result 	= $query->row()->bk_no;
			$result++;
			return $result;
		}
	}

	function get_patients()
	{
		$this->db->select('*');
		$this->db->from('patient');
		$this->db->where('p_status','1');
		$query 			=	$this->db->get();
		return $query->result_array();
	}

	function today_book()
	{
		$output	=	"";
		$shift 	=	"";
		$bk_date = date("Y-m-d");

		$this->db->select('booking.*,users.u_name');
		$this->db->from('booking');
		$this->db->join('users','users.u_emp_id = booking.bk_doc', 'inner');
		$this->db->order_by('booking.bk_no','asc');
		$this->db->where('booking.bk_today',$bk_date);
		$query 			=	$this->db->get();
		if($query->num_rows()>0) {
		foreach($query->result() as $row)
		{
			if($row->bk_shift=="m") { $shift = "Morning"; }
			else if($row->bk_shift=="e") { $shift = "Evening"; }
			$output 	.=	"<tr><td>".$row->bk_no."</td>";
			$output 	.=	"<td>".date("d-m-Y",strtotime($row->bk_date))."</td>";
			$output 	.=	"<td>".date("d-m-Y",strtotime($row->bk_today))."</td>";
			$output 	.=	"<td>".$row->u_name."</td>";
			$output 	.=	"<td>".$shift."</td>";
			$output 	.=	"<td>".$row->bk_mrd."</td>";
			$output 	.=	"<td>".$row->bk_name."</td>";
			$output 	.=	"<td>".$row->bk_phone."</td>";


			$output 	.=	"<td class='btn-group  btn-group-xs' ><a href='".$this->config->item('admin_url')."booking/edit/".$row->bk_id."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
			<a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."booking/delete/".$row->bk_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	} 
	else { return false; }
		
	}

	function get_visit_type($op_mrd)
	{
		$result = "";
		$this->db->select('p_tot_visits');
		$this->db->from('patient');
		$this->db->where('p_mrd_no',$op_mrd);

		$query 			=	$this->db->get();
		
		if($query->row()->p_tot_visits > 1)
		{
			$result = "Follow Up";
		}
		else
		{
			$result = "New Visit";
		}
		return $result;
	}

	function get_opno($op_shift,$op_doctor)
	{
		$bk_date = date("Y-m-d");
		$this->db->select('op_opno');
		$this->db->from('op');
		$this->db->where('op_date',$bk_date);
		$this->db->where('op_shift',$op_shift);
		$this->db->where('op_doctor',$op_doctor);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return 'B_01';
		}
		else
		{
			$result 	= $query->row()->op_opno;
			$result++;
			return $result;
		}
	}

	function get_docfee($op_doctor)
	{
		$this->db->select('u_fees');
		$this->db->from('users');
		$this->db->where('u_emp_id',$op_doctor);
		$query 			=	$this->db->get();
		return $query->row()->u_fees;
	}

	function get_citys()
	{
		$this->db->select('cities.*');
		$this->db->from('cities');
		$this->db->join('states','states.id = cities.state_id','inner');
		$this->db->where('states.id','19');
		$query 			=	$this->db->get();
		return $query->result_array();
	}

}