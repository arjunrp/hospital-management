<?php

class Stock_model extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;
		$total_stock  	=   "";

		$this->db->select('sum(stock_qty) as total_stock,product.*');
		$this->db->from('stock');
		$this->db->join('product','product.pd_code = stock.stock_product_id','inner');
		$this->db->where('stock.stock_status','standby');
		$this->db->order_by("stock.stock_id", "asc");
		$this->db->group_by("stock.stock_product_id");
		// $this->db->group_by("stock.stock_dept");
		$result  		=    $this->db->get();
		foreach($result->result() as $row)
		{
			$sl_no++;

			if($row->pd_unit !="No.s") 
			{	$total_stock = $row->total_stock/$row->pd_qty;	}
			else if($row->pd_unit =="No.s") 
			{	$total_stock = $row->total_stock;	}

			$nfp  = "<font color='#FF0000'><b>".$total_stock." ".$row->pd_unit."</b></font>"; 
			$nfp1 = "<font color='#D2691E'><b>".$total_stock." ".$row->pd_unit."</b></font>"; 
			if($total_stock <= $row->pd_min) 
				{ $np=$nfp; }
			else if($total_stock > $row->pd_min && $total_stock < $row->pd_max) 
				{ $np=$total_stock." ".$row->pd_unit; }
			else if($total_stock >= $row->pd_max) 
				{ $np=$nfp1; }

			if($row->pd_type =="m") 
				{ $store_type = "Medical";}
			else
			{ $store_type = "General";}

			
			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->pd_code."</td>";
			$output 	.=	"<td>".$row->pd_product."</td>";
			$output 	.=	"<td>".$row->pd_min." ".$row->pd_unit."</td>";
			$output 	.=	"<td>".$row->pd_max." ".$row->pd_unit."</td>";
			$output 	.=	"<td>".$np."</td>";
			$output 	.=	"<td>".$store_type."</td>";
			$output 	.=	"<td><a class='btn btn-primary btn-xs' href='".$this->config->item('admin_url')."stock/view/".$row->pd_code."'><i class='fa fa-eye'></i></a>
			<a class='btn btn-info btn-xs' href='".$this->config->item('admin_url')."stock/edit/".$row->pd_code."'><i class='fa fa-pencil-square-o'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}

  
	function get_one_banner($pd_id)
	{
		$this->db->select('stock.*,product.pd_product as ved_item,department.dp_department');
		$this->db->from('stock');
		$this->db->join('product','product.pd_code = stock.stock_product_id','inner');
		$this->db->join('department','department.dp_id = stock.stock_dept','inner');
		$this->db->where('stock.stock_product_id',$pd_id);
		// $this->db->where('stock.stock_dept',"3");
		$this->db->where('stock.stock_status',"standby");
		$this->db->group_by('stock.stock_id');
		$query 			=	$this->db->get();
		return $query->result_array();
	}

	function get_stock_view($id)
	{
		$this->db->select('sum(stock_qty) as total_stock,product.pd_qty,product.pd_unit,department.dp_department');
		$this->db->from('stock');
		$this->db->join('department','department.dp_id = stock.stock_dept','left');
		$this->db->join('product','product.pd_code = stock.stock_product_id','left');
		$this->db->where('stock.stock_status','standby');
		$this->db->where('stock.stock_product_id',$id);
		$this->db->order_by("stock.stock_id", "desc");
		$this->db->group_by("stock.stock_product_id");
		$this->db->group_by("stock.stock_dept");
    	$query	=	$this->db->get();
		return $query->result_array();
	}
}