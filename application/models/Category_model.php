<?php

class Category_model extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;
		// $this->db->order_by('id','asc');
       	$sql="SELECT C.*,SC.br_id as bid,SC.br_brand FROM mw_category AS C INNER JOIN mw_brand AS SC WHERE SC.br_id=C.brandid AND C.status=1 AND SC.br_status=1 order by C.id ASC";
		// $this->db->select('*');
		// $this->db->from('subcategory');
		// $query 			=	$this->db->get();
		$query 			=	$this->db->query($sql);
		foreach($query->result() as $row)
		{
			$sl_no++;
			if($row->status == '1')
			{
				$image = "<i class='fa fa-check'></i>";
				$status = 0;
			}
			else
			{
				$image = "<i class='fa fa-ban'></i>";
				$status = 1;
			}
			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->category."</td>";
			$output 	.=	"<td>".$row->brand."</td>";
			 
			 
			 
			// $output 	.=	"<td><a href='".$this->config->item('admin_url')."pcategory/active/".$row->id."/".$status."'>".$image."</a></td>";
			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='".$this->config->item('admin_url')."pcategory/edit/".$row->id."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
			
<a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."pcategory/delete/".$row->id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>";
			$output		.=	"</tr>";
			 // <a href='".$this->config->item('admin_url')."pcategory/delete/".$row->id."'class='btn btn-danger' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>";
		}
		return $output;
	}

  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('category');
		$this->db->where('id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}


	function get_brand()
	{
		$this->db->select('*');
		$this->db->from('brand');
		$query 			=	$this->db->get();
		return $query->result_array();
	}




}