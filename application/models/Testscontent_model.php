<?php

class Testscontent_model extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;
    // $sql="SELECT E.*,EC.* FROM mw_test_name AS E INNER JOIN mw_test_content AS EC WHERE E.tn_id=EC.tc_tnameid AND E.tn_status=1 AND EC.tc_status=1 order by EC.tc_id asc";

		$this->db->select('test_name.*,test_content.*');
    	$this->db->from('test_content');
    	$this->db->join('test_name','test_name.tn_id = test_content.tc_tnameid', 'left');
    	$this->db->order_by('test_name.tn_name','asc');
    	$query 			=	$this->db->get();
		// $this->db->select('*');
		// $this->db->from('expence');
		// 

		foreach($query->result() as $row)
		{
			$sl_no++;
			if($row->tc_status == '1')
			{
				$image = "<i class='fa fa-check'></i>";
				$tc_status = 0;
			}
			else
			{
				$image = "<i class='fa fa-ban'></i>";
				$status = 1;
			}
			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->tn_name."</td>";
			$output 	.=	"<td>".$row->tc_content."</td>";
			$output 	.=	"<td>".$row->tc_men." ".$row->tc_unit."</td>";
			$output 	.=	"<td>".$row->tc_women." ".$row->tc_unit."</td>";
			$output 	.=	"<td>".$row->tc_child." ".$row->tc_unit."</td>";
			 
			 
			// $output 	.=	"<td><img src='".$this->config->item('image_url')."features/small/".$row->feature_image."'></td>";
			 
			// $output 	.=	"<td><a href='".$this->config->item('admin_url')."expence/active/".$row->id."/".$status."'>".$image."</a></td>";
			// $output 	.=	"<td class='btn-group  btn-group-xs'><a href='".$this->config->item('admin_url')."expence/edit/".$row->id."' class='btn btn-info view-btn-edit' title='Edit' ><i class='fa fa-pencil-square-o'></i></a> 
			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='".$this->config->item('admin_url')."testscontent/edit/".$row->tc_id."' class='btn btn-primary view-btn-edit' title='Edit' ><i class='fa fa-pencil-square-o'></i></a>
			 
		   <a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."testscontent/delete/".$row->tc_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>";
			// <a  href='".$this->config->item('admin_url')."expence/delete/".$row->id."'class='btn btn-danger' title='Delete'><i class='fa fa-times-circle-o'></i></a>";
			// $output		.=	"<td><a href='".$this->config->item('admin_url')."expence/delete/".$row->id."'><i class='fa fa-trash'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}


function get_category()
	{
		$this->db->select('*');
		$this->db->from('test_name');
		$query 			=	$this->db->get();
		return $query->result_array();
	}
  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('test_content');
		$this->db->where('tc_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}
}