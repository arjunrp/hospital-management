<?php

class Clientmodel extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('*');
		$this->db->from('customer');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{
			$sl_no++;
			if($row->status == '1')
			{
				$image = "<i class='fa fa-check'></i>";
				$status = 0;
			}
			else
			{
				$image = "<i class='fa fa-ban'></i>";
				$status = 1;
			}
			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->customer."</td>";
			// $output 	.=	"<td>".$row->service_description."</td>";
			// $output 	.=	"<td><img src='".$this->config->item('image_url')."client/small/".$row->client_logo."'></td>";
			// $output 	.=	"<td>".$row->service_type."</td>";
			 
			$output 	.=	"<td><a href=javascript:confirm_del('".$this->config->item('admin_url')."client/active/".$row->id."/".$status."')>".$image."</a></td>";
			$output 	.=	"<td><a href='".$this->config->item('admin_url')."client/edit/".$row->id."'><i class='fa fa-pencil'></i></a></td>";
			$output		.=	"<td><a href=javascript:confirm_del('".$this->config->item('admin_url')."client/delete/".$row->id."')><i class='fa fa-trash'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}

  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('customer');
		$this->db->where('id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}
}