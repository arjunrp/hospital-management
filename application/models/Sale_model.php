	<?php

class Sale_model extends CI_Model {

	// function __construct() {
	// 	parent::__construct();

	// 	$this->load->database();
	// }

	function get_current_page_records($limit, $start, $st = NULL, $st1 = NULL)
    {
        if ($st == "NIL") 	$st  = ""; 
        if ($st1 == "NIL") 	$st1 = ""; 

        $this->db->select('voucher_entry.*');
    	$this->db->from('voucher_entry');
    	// $this->db->order_by('voucher_entry.ve_vno','desc');
    	$where = '(ve_type="si" or ve_type = "so")';
    	$this->db->where($where);
    	$this->db->where("ve_vno LIKE '%$st%'");
		$this->db->where("ve_date LIKE '%$st1%'");
		$this->db->limit($limit,$start);
		$this->db->order_by('ve_id','desc');
    	$query=$this->db->get();

    	// $sql = "select * from voucher_entry where ve_vno like '%$st%' AND ve_date like '%$st1%' AND ve_type like '%$st2%' AND p_address like '%$st3%' AND p_guardian like '%$st4%' limit " . $start . ", " . $limit;
     //    $query = $this->db->query($sql);

        if($query->num_rows()!=0)
        {
        	return $query->result();	
        }
    }

    function get_total($st = NULL,$st1 = NULL)
    {
        if ($st == "NIL")   $st 	= ""; 
        if ($st1 == "NIL")  $st1 = ""; 

        $this->db->select('voucher_entry.*');
    	$this->db->from('voucher_entry');
    	// $this->db->order_by('voucher_entry.ve_vno','desc');
    	$where = '(ve_type="si" or ve_type = "so")';
    	$this->db->where($where);
    	$this->db->where("ve_vno LIKE '%$st%'");
		$this->db->where("ve_date LIKE '%$st1%'");
    	$query=$this->db->get();
        return $query->num_rows();
    }

    function get_current_page_records_name($limit, $start, $st = NULL)
    {
        if ($st == "NIL") { $st = ""; };

        $this->db->select('voucher_entry.*');
    	$this->db->from('voucher_entry');
    	// $this->db->order_by('voucher_entry.ve_vno','desc');
    	$where = '(ve_type="si" or ve_type = "so")';
    	$this->db->where($where);
    	$this->db->limit($limit,$start);
    	$this->db->order_by('ve_id','desc');
    	$query=$this->db->get();

        if($query->num_rows()!=0)
        {
        	return $query->result();	
        }
    }

    function get_total_name($st = NULL)
    {
        if ($st == "NIL") { $st = ""; }
        $this->db->select('voucher_entry.*');
    	$this->db->from('voucher_entry');
    	$this->db->order_by('voucher_entry.ve_vno','desc');
    	$where = '(ve_type="si" or ve_type = "so")';
    	$this->db->where($where);
    	$query=$this->db->get();
        return $query->num_rows();
    }

	
	function list_all()
	{

		// <a class=' btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."sales/delete/".$row->ve_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a>

		$today= date("Y-m-d");
		$yester = date("Y-m-d",strtotime('-2 day',strtotime($today)));
		$output 		=	"";
		$this->db->select('voucher_entry.*');
    	$this->db->from('voucher_entry');
    	$this->db->order_by('voucher_entry.ve_vno','desc');
    	$this->db->where('voucher_entry.ve_date >',$yester);
    	$where = '(ve_type="si" or ve_type = "so")';
    	$this->db->where($where);
    	$query=$this->db->get();
		foreach($query->result() as $row)
		{
			if($row->ve_pstaus=="NP") { $fp = "<font color='#b20000'>Not Completed</font>";  }
			elseif($row->ve_pstaus=="FP") { $fp = "<font color='#228B22'>Completed</font>";  }
			$output 	.=	"<tr><td>".$row->ve_vno."</td>";
			$output 	.=	"<td>".date("d-m-Y",strtotime($row->ve_date))."</td>";
			$output 	.=	"<td>".$fp."</td>";
			 

			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."sales/sale_View/".$row->ve_id."'class='btn btn-primary view-btn-edit' title='View'><i class='fa fa-eye'></i></a> </td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	}

	function list_all_pending()
	{
		// <a class=' btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."sales/pend_delete/".$row->ps_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a>
		$output 		=	"";
		$slno 			=	'1';
		$this->db->select('pending.*');
    	$this->db->from('pending');
    	$this->db->order_by('pending.ps_id','desc');
    	$query=$this->db->get();
		foreach($query->result() as $row)
		{

			$output 	.=	"<tr><td>".$slno++."</td>";
			$output 	.=	"<td>".date("d-m-Y",strtotime($row->ps_date))."</td>";
			$output 	.=	"<td>".$row->ps_patient."</td>";
			$output 	.=	"<td>".$row->ps_phone."</td>";
			 

			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."sales/sale_PView/".$row->ps_id."'class='btn btn-primary view-btn-edit' title='View'><i class='fa fa-eye'></i></a>
							<a href='". $this->config->item('admin_url')."sales/sale_PAdd/".$row->ps_id."'class='btn btn-default view-btn-edit' title='Sale'><i class='fa fa-download'></i></a>
							
			 </td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	}


	public function set_sale_stock($stockpid,$purchase_unit_qty,$sale_date,$ved_batch){
		
		$this->db->select('*');
		$this->db->from('stock');
		$this->db->where('stock_product_id', $stockpid);
		$this->db->where('stock_dept','3');
		$this->db->where('stock_batch',$ved_batch);
		$this->db->order_by("stock_id", "desc");
		$this->db->where('stock_status','standby');
		$this->db->limit(1);
		$result  		=    $this->db->get();
		$number_of_rows = $result->num_rows();

		if($number_of_rows != 0){
			$sid						=	$result->row()->stock_id;
			$sqty						=	$result->row()->stock_qty;
			$sdate 						=	$result->row()->stock_date;
			$data['stock_qty'] 			=	$sqty - $purchase_unit_qty;
			$data['stock_date']			=	$sale_date;	
			$data['stock_product_id']	=	$stockpid;
			$data['stock_batch']		=	$ved_batch;
			$data['stock_dept']			=	"3";
			$data['stock_status']		=	"standby";	
			$data['created_at']			=	$result->row()->created_at;
			if($sdate == $sale_date)
			{
				$this->db->update('stock',$data, array('stock_id' => $sid));
			}
			else
			{
				$this->db->insert('stock',$data);	
				$data1['stock_status']		=	"transfer";	
				$this->db->update('stock',$data1, array('stock_id' => $sid));
			}
		}
		else
		{
			$data['stock_qty '] 		=	0	-	$purchase_unit_qty;
			$data['stock_date ']		=	$sale_date;	
			$data['stock_product_id']	=	$stockpid;
			$data['stock_batch']		=	$ved_batch;
			$data['stock_dept']			=	"3";
			$data['stock_status']		=	"standby";
			$data['created_at']			=	date('Y-m-d H:i:s.') . gettimeofday()['usec'];	
			$this->db->insert('stock',$data);
		}
	}

	public function up_sale_balance($amount_paid,$sale_date){
		
		$this->db->select('*');
		$this->db->from('balance');
		$this->db->where('balance_date >=',$sale_date);
		$this->db->order_by("balance_id", "desc");
		$result  		=    $this->db->get();
		$query			=    $result->result_array();
 
			foreach ($query as $value) 
			{
			$bid						=	$value['balance_id'];
			$bamount					=	$value['balance_amount'];
			$data['balance_amount '] 	=	$bamount + $amount_paid;
			$this->db->update('balance',$data, array('balance_id' => $bid));
			}
	}


	public function set_sale_balance($amount_paid,$sale_date){
		
		$this->db->select('*');
		$this->db->from('balance');
		$this->db->order_by("balance_id", "desc");
		$this->db->limit(1);
		$result  		=    $this->db->get();
		$number_of_rows = $result->num_rows();

		if($number_of_rows != 0){
			$bid						=	$result->row()->balance_id;
			$bamount					=	$result->row()->balance_amount;
			$bdate 						=	$result->row()->balance_date;
			$data['balance_amount '] 	=	$bamount + $amount_paid;
			$data['balance_date ']		=	$sale_date;	
			if($bdate == $sale_date)
			{
				$this->db->update('balance',$data, array('balance_date' => $sale_date));
			}
			else
			{
				$this->db->insert('balance',$data);	
			}
		}
		else
		{
			$data['balance_amount '] 	=	0	+	$amount_paid;
			$data['balance_date ']		=	$sale_date;	
			$this->db->insert('balance',$data);
		}
	}

	public function getSaleId(){

		// $query = $this->db->query('SELECT ve_vno FROM mw_voucher_entry WHERE ((ve_vno % 2) != 0) AND (ve_type="si" || ve_type="so")  ORDER BY ve_vno DESC LIMIT 1');
	//$query 			=	$his->db->get();
		$user_id = $this->session->id;
		// echo $user_id;
		// $this->db->select('SUBSTRING(ve_vno, 4, length(ve_vno)-5) as ve_vno');
    // $where1 = '((CAST(SUBSTRING(ve_vno, 4, length(ve_vno)-3) as UNSIGNED) % 2) != 0)';

	$this->db->select('CAST(SUBSTRING(ve_vno, 6, length(ve_vno)-3) as SIGNED) as ve_vno');
    $this->db->from('voucher_entry'); 
    $where = '(ve_type="si" or ve_type = "so")';
    $this->db->where($where);
    $this->db->where('SUBSTRING(ve_vno, 3, 2) =',$user_id);
    $this->db->order_by('ve_vno','desc');
    $this->db->limit(1);
    $query=$this->db->get();

	if($query->num_rows()==0)
	{
		$data = 'PH'.$user_id.'_1';
	}
	else
	{
		$result 	= $query->row()->ve_vno+1;
		$data = 'PH'.$user_id.'_'.$result;
	}
		return $data;
	}

	public function getSaleId_even(){

		// $query = $this->db->query('SELECT ve_vno FROM mw_voucher_entry WHERE ((ve_vno % 2) != 0) AND (ve_type="si" || ve_type="so")  ORDER BY ve_vno DESC LIMIT 1');
	//$query 			=	$his->db->get();


	$this->db->select('CAST(SUBSTRING(ve_vno, 4, length(ve_vno)-3) as UNSIGNED) as ve_vno');
    $this->db->from('voucher_entry'); 
    $where1 = '((CAST(SUBSTRING(ve_vno, 4, length(ve_vno)-3) as UNSIGNED) % 2) = 0)';
    $where = '(ve_type="si" or ve_type = "so")';
    $this->db->where($where);
    $this->db->where($where1);
    $this->db->order_by('ve_vno','desc');
    $this->db->limit(1);
    $query=$this->db->get();

	if($query->num_rows()==0)
	{
		$data = 'PH_1';
	}
	else
	{
		$result 	= $query->row()->ve_vno+2;
		$data = 'PH_'.$result;
	}
		return $data;
	}

	public function getSaleId_odd(){

		// $query = $this->db->query('SELECT ve_vno FROM mw_voucher_entry WHERE ((ve_vno % 2) != 0) AND (ve_type="si" || ve_type="so")  ORDER BY ve_vno DESC LIMIT 1');
	//$query 			=	$his->db->get();


	$this->db->select('CAST(SUBSTRING(ve_vno, 4, length(ve_vno)-3) as UNSIGNED) as ve_vno');
    $this->db->from('voucher_entry'); 
    $where1 = '((CAST(SUBSTRING(ve_vno, 4, length(ve_vno)-3) as UNSIGNED) % 2) != 0)';
    $where = '(ve_type="si" or ve_type = "so")';
    $this->db->where($where);
    $this->db->where($where1);
    $this->db->order_by('ve_vno','desc');
    $this->db->limit(1);
    $query=$this->db->get();

	if($query->num_rows()==0)
	{
		$data = 'PH_1';
	}
	else
	{
		$result 	= $query->row()->ve_vno+2;
		$data = 'PH_'.$result;
	}
		return $data;
	}

	public function getCustomer(){
		$query=$this->db->get_where('patient',array('p_status'=>1));
		return $query->result_array();
		
	}

	public function getProduct(){
		$this->db->select('voucher_entry_detail.*,stock.stock_qty,stock.stock_id,product.pd_cgst,product.pd_sgst,product.pd_qty');
		$this->db->from('stock');
		$this->db->join('voucher_entry_detail','voucher_entry_detail.ved_itemid = stock.stock_product_id AND voucher_entry_detail.ved_batch = stock.stock_batch', 'inner');
		$this->db->join('voucher_entry','voucher_entry_detail.ved_veid = voucher_entry.ve_id', 'inner');
		$this->db->join('product','voucher_entry_detail.ved_itemid = product.pd_code', 'inner');
		$this->db->group_by('stock.stock_product_id');
		$this->db->group_by('stock.stock_batch');
		$this->db->order_by('stock.created_at','asc');
    	$this->db->where('stock.stock_qty > ','0');
    	$this->db->where('stock.stock_dept','3');
    	$this->db->where('stock.stock_status','standby');
    	$this->db->where('voucher_entry.ve_type','p');
    	
		$query=$this->db->get();
		return $query->result_array();
		
	}

	public function getProducts(){

		$this->db->select('voucher_entry_detail.*,stock.stock_qty,product.pd_unitprice,product.pd_price');
		$this->db->from('stock');
		$this->db->join('voucher_entry_detail','voucher_entry_detail.ved_itemid = stock.stock_product_id AND voucher_entry_detail.ved_batch = stock.stock_batch', 'inner');
		$this->db->join('product','voucher_entry_detail.ved_itemid = product.pd_code', 'inner');
		$this->db->group_by('stock.stock_product_id');
		$this->db->group_by('stock.stock_batch');
		$this->db->order_by('stock.stock_id','asc');
		$this->db->where('stock.stock_qty > ','0');
		$this->db->where('stock.stock_dept','3');

	// $this->db->select('c.stock_qty, e1.*');
 //    $this->db->from('stock c');
 //    $this->db->join('product e1','c.stock_product_id = e1.id', 'left');
 //    $this->db->join('stock e2','c.stock_product_id = e2.stock_product_id AND e2.stock_id > c.stock_id', 'left');
 //    $this->db->where('e2.stock_id',NULL);
    $query=$this->db->get();
    return $query->result_array();

	}


public function getSaleview($sid){
	$this->db->select('voucher_entry.*,voucher_entry_detail.*,u1.u_name');
    $this->db->from('voucher_entry');
    $this->db->join('voucher_entry_detail','voucher_entry_detail.ved_veid = voucher_entry.ve_id', 'inner');
    // $this->db->join('patient','patient.p_mrd_no = voucher_entry.ve_mrd', 'inner');
    // $this->db->join('users','users.u_emp_id = voucher_entry.ve_user', 'inner'); 
    $this->db->join('users u1','u1.u_emp_id = voucher_entry.ve_doctor', 'inner'); 
    $this->db->where('voucher_entry.ve_id',$sid);
    $query=$this->db->get();
    return $query->result_array();
}

public function getSalePview($sid){
	$this->db->select('pending.*,pending_detail.*');
    $this->db->from('pending');
    $this->db->join('pending_detail','pending_detail.psd_psid = pending.ps_id', 'inner');
    // $this->db->join('users','users.u_emp_id = pending.ps_user', 'inner'); 
    $this->db->where('pending.ps_id',$sid);
    $query=$this->db->get();
    return $query->result_array();
}

public function get_patientsip(){
		$this->db->select('ip.*,patient.p_title,patient.p_name,patient.p_phone,department.dp_department,users.u_name');
		$this->db->from('ip');
		$this->db->join('patient','patient.p_mrd_no = ip.ip_mrd', 'inner');
		$this->db->join('department','department.dp_id = ip.ip_department', 'inner');
		$this->db->join('users','users.u_emp_id = ip.ip_doctor', 'inner');
		$this->db->where('ip.ip_dis','1');
		$query=$this->db->get();
		return $query->result_array();
	}

	public function get_pending_details($ve_id){

	  $this->db->select('pending_detail.*,pending.ps_date');
      $this->db->from('pending_detail');
      $this->db->join('pending','pending.ps_id = pending_detail.psd_psid', 'inner');
      $this->db->where('pending_detail.psd_psid',$ve_id);
      $this->db->order_by('pending_detail.psd_id','asc');
      $query=$this->db->get();
      return $query->result_array();
	}

	public function get_voucher_details($ve_id){

	  $this->db->select('*');
      $this->db->from('pending_detail');
      $this->db->where('psd_psid',$ve_id);
      $this->db->order_by('psd_id','asc');
      $query=$this->db->get();
      return $query->result_array();
	}


	}

	?>
