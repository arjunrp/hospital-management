<?php

class Porder_model extends CI_Model {

	function __construct() {
		parent::__construct();

		$this->load->database();
	}

	function list_all()
	{
		$output 		=	"";
		$status 		=	"<font color='#b20000'><b>Not Delivered</b></font>";
		$this->db->select('porder.*,suppliers.sp_vendor');
    	$this->db->from('porder');
    	$this->db->join('suppliers','suppliers.sp_id = porder.po_supplier', 'inner');
    	$this->db->order_by('porder.po_pono','desc');
    	$query=$this->db->get();
		foreach($query->result() as $row)
		{
			if($row->po_avail=="fd") { $status1 = "disabled"; $status = "<font color='#228B22'><b>Delivered</b></font>"; }
			if($row->po_avail=="nfd") { $status1 = ""; $status = "<font color='#FF4500'><b>Partially Delivered</b></font>"; }
			if($row->po_avail=="nd") { $status1 = ""; $status = "<font color='#b20000'><b>Not Delivered</b></font>"; }
			$output 	.=	"<tr><td>".date("d-m-Y",strtotime($row->po_date))."</td>";
			$output 	.=	"<td>".$row->po_pono."</td>";
			$output 	.=	"<td>".$row->sp_vendor."</td>";
			$output 	.=	"<td>".date("d-m-Y",strtotime($row->po_dlvry_date))."</td>";
			$output 	.=	"<td>".$status."</td>";
			 

			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."porder/view/".$row->po_id."' class='btn btn-primary view-btn-edit' title='View'><i class='fa fa-eye'></i></a> 
			<a href='".$this->config->item('admin_url')."porder/edit/".$row->po_id."' class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
			<a href='".$this->config->item('admin_url')."porder/order_complete/".$row->po_id."' $status1 class='btn btn-default view-btn-edit' title='Complete Order'><i class='fa fa-check-circle-o'></i></a>
			 <a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."porder/delete/".$row->po_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	}

	public function stock_check($stockpid)
	{
		$this->db->select('typeid');
		$this->db->from('product');
		$this->db->where('id', $stockpid);
		$result  		=    $this->db->get();
		return $result->row()->typeid;
	}

	public function getPurchase($pid){
		// $this->db->order_by("purchase_id","desc");

		$query=$this->db->get_where('purchase',array('status'=>1,'purchase_id'=>$pid));
		return $query->row_array();
	}
	public function getPorderId()
     {

		$this->db->select('po_pono');
		$this->db->from('porder');
		$this->db->order_by('po_pono','desc');
		$this->db->where('po_status','1');
		$this->db->limit(1);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return '1';
		}
		else
		{
			$result 	= $query->row()->po_pono + 1;
			// $result++;
			return $result;
		}
	}

	public function getVendors($pid){
		   $this->db->select('vendor.*');
    $this->db->from('vendor');
    $this->db->join('purchase_detail','purchase_detail.vendor_id = vendor.id', 'inner'); 
     $this->db->where('purchase_detail.purchase_detail_id',$pid);
    $query=$this->db->get();
			return $query->row_array();
	}

	public function getProduct(){
		$this->db->select('*');
		$this->db->from('product');
		$this->db->where('pd_status','1');
		$query=$this->db->get();
		return $query->result_array();
	}

  public function getPorderview($pid){
		$this->db->select('porder.*,suppliers.*,porder_detail.*');
    $this->db->from('porder');
    $this->db->join('porder_detail','porder_detail.pod_poid = porder.po_id', 'inner'); 
    $this->db->join('suppliers','suppliers.sp_id = porder.po_supplier', 'inner');
    // $this->db->join('users','users.u_emp_id = porder.po_user', 'inner'); 
    $this->db->where('porder.po_id',$pid);
    $query=$this->db->get();
    return $query->result_array();
  }



	
	public function getCompany(){
		$this->db->select('*');
		$this->db->from('company');
		$query=$this->db->get();
		return $query->result_array();
	}

	}

	?>
