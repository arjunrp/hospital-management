<?php

class Room_model extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('*');
		$this->db->from('room');
		$this->db->order_by('rm_id','asc');
		// $this->db->where('rm_status','1');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{
			$sl_no++;
			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->rm_no."</td>";
			$output 	.=	"<td>"."Rs. ".$row->rm_fees."</td>";
			$output 	.=	"<td>"."Rs. ".$row->rm_nurse."</td>";
			$output 	.=	"<td>"."Rs. ".$row->rm_hc."</td>";

			$output 	.=	"<td class='btn-group  btn-group-xs' ><a href='".$this->config->item('admin_url')."room/edit/".$row->rm_id."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
			<a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."room/delete/".$row->rm_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	}

  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('room');
		$this->db->where('rm_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}

	function get_room_no()
	{
		$this->db->select('rm_no');
		$this->db->from('room');
		$this->db->where('rm_status',1);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return '0';
		}
		else
		{
			$result 	= $query->row()->rm_no;
			$result++;
			return $result;
		}
	}


}