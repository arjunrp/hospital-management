<?php

class Inventory_model extends CI_Model 
{

	function get_stock($fdate,$tdate,$product,$p_dept)
	{
		$output 		=	"";

			$temp  	= array();
    		$temp2 	= array();
    		$tep2 	= array();
    		$sum   	= array();

			$this->db->select('stock.*,product.pd_product,product.pd_qty,product.pd_unit');
			$this->db->from('stock');
			$this->db->join('product', 'product.pd_code = stock.stock_product_id', 'inner');
			$this->db->order_by('stock.stock_date','desc');
			$this->db->where('stock_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');

			if($product==0)
			{
    				$query                 =    $this->db->get();
    		}
    		else if($product>=0)
    		{
    			$this->db->where('stock.stock_product_id',$product);
				$query                 =    $this->db->get();
    		}

    $data                  =    $query->result_array();

    foreach($data as $key => $value)
    {   

        if(in_array($value['stock_date'], $temp) && array_key_exists($value['stock_product_id'], $temp)){

            $temp2[$value['stock_date'].$value['stock_product_id']]['sale_item_id']  	=  $value['stock_product_id'];
            $temp2[$value['stock_date'].$value['stock_product_id']]['sale_date'] 		=  $value['stock_date'];
            $temp2[$value['stock_date'].$value['stock_product_id']]['sale_item']    	=  $value['pd_product'];
            $temp2[$value['stock_date'].$value['stock_product_id']]['purchase_qty']  	=  ($sum[$value['stock_date'].$value['stock_product_id']]['purchase_qty'] + $this->getPurchase($value['stock_product_id'], $value['stock_date']))/ $value['pd_qty']." ".$value['pd_unit'];
            $temp2[$value['stock_date'].$value['stock_product_id']]['sales_qty']     	=  ($sum[$value['stock_date'].$value['stock_product_id']]['sales_qty']    + $this->getSales($value['stock_product_id'], $value['stock_date']))/ $value['pd_qty']." ".$value['pd_unit'];

            $temp2[$value['stock_date'].$value['stock_product_id']]['purchase_return']  = ($sum[$value['stock_date'].$value['stock_product_id']]['purchase_return'] + $this->purchaseReturn($value['stock_product_id'], $value['stock_date']))/ $value['pd_qty']." ".$value['pd_unit'];
            $temp2[$value['stock_date'].$value['stock_product_id']]['sales_return']     = ($sum[$value['stock_date'].$value['stock_product_id']]['sales_return']    + $this->salesReturn($value['stock_product_id'], $value['stock_date']))/ $value['pd_qty']." ".$value['pd_unit'];
            $temp2[$value['stock_date'].$value['stock_product_id']]['consume']     		= ($sum[$value['stock_date'].$value['stock_product_id']]['consume']    + $this->getConsume($value['stock_product_id'], $value['stock_date']))/ $value['pd_qty']." ".$value['pd_unit'];

            $temp2[$value['stock_date'].$value['stock_product_id']]['openingStock']  	= ($this->getOpeningStock($value['stock_product_id'], $value['stock_date']))/ $value['pd_qty']." ".$value['pd_unit'];

            $temp2[$value['stock_date'].$value['stock_product_id']]['closingstock']  	= ($this->getClosingStock($value['stock_product_id'], $value['stock_date']))/ $value['pd_qty']." ".$value['pd_unit'];


        } else {


            $temp2[$value['stock_date'].$value['stock_product_id']]['sale_item_id']  	=  $value['stock_product_id'];
            $temp2[$value['stock_date'].$value['stock_product_id']]['sale_date'] 		=  $value['stock_date'];
            $temp2[$value['stock_date'].$value['stock_product_id']]['sale_item']    	=  $value['pd_product'];
            $temp2[$value['stock_date'].$value['stock_product_id']]['purchase_qty']  	= ($this->getPurchase($value['stock_product_id'], $value['stock_date']))/ $value['pd_qty']." ".$value['pd_unit'];
            $temp2[$value['stock_date'].$value['stock_product_id']]['sales_qty']  		= ($this->getSales($value['stock_product_id'], $value['stock_date']))/ $value['pd_qty']." ".$value['pd_unit'];
             $temp2[$value['stock_date'].$value['stock_product_id']]['purchase_return'] = ($this->purchaseReturn($value['stock_product_id'], $value['stock_date']))/ $value['pd_qty']." ".$value['pd_unit'];
            $temp2[$value['stock_date'].$value['stock_product_id']]['sales_return']     = ($this->salesReturn($value['stock_product_id'], $value['stock_date']))/ $value['pd_qty']." ".$value['pd_unit'];
             $temp2[$value['stock_date'].$value['stock_product_id']]['consume']     	= ($this->getConsume($value['stock_product_id'], $value['stock_date']))/ $value['pd_qty']." ".$value['pd_unit'];

            $temp2[$value['stock_date'].$value['stock_product_id']]['openingStock']  	= ($this->getOpeningStock($value['stock_product_id'], $value['stock_date']))/ $value['pd_qty']." ".$value['pd_unit'];
			$temp2[$value['stock_date'].$value['stock_product_id']]['closingstock']  	= ($this->getClosingStock($value['stock_product_id'], $value['stock_date']))/ $value['pd_qty']." ".$value['pd_unit'];

        } 

    }
		 return $temp2;
			
		}

	

function getConsume($sale_item_id,$sale_date)
{    
    $this->db->select_sum('ved_qty');
    $this->db->from('voucher_entry_detail');
    $this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid','inner');
    $this->db->where('voucher_entry_detail.ved_itemid',$sale_item_id);
    $this->db->where('voucher_entry_detail.ved_date',$sale_date);
    $this->db->where('voucher_entry.ve_type','c');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->row()->ved_qty;
    	return $rowcount;
	}
}

function getPurchase($sale_item_id,$sale_date)
{    
    $this->db->select('sum(ved_qty) as ved_qty,sum(ved_free) as ved_free');
    $this->db->from('voucher_entry_detail');
    $this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid','inner');
    $this->db->where('voucher_entry_detail.ved_itemid',$sale_item_id);
    $this->db->where('voucher_entry_detail.ved_date',$sale_date);
    $this->db->where('voucher_entry.ve_type','p');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->row()->ved_qty + $query->row()->ved_free;
    	return $rowcount;
	}
}

function getClosingStock($sale_item_id,$sale_date)
{
    $this->db->select('sum(stock_qty) as stock_qty');
    $this->db->from('stock');
    $this->db->where('stock_product_id',$sale_item_id);
    $this->db->where('stock_date',$sale_date);
    $this->db->order_by("stock_id", "desc");
    $this->db->limit(1);
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	return  $query->row()->stock_qty; 
	}
}
function getOpeningStock($sale_item_id,$sale_date)
{
    $this->db->select('sum(stock_qty) as stock_qty');
    $this->db->from('stock');
    $this->db->where('stock_product_id',$sale_item_id);
    $this->db->where('stock_date <',$sale_date);
    $this->db->order_by("stock_id", "desc");
    $this->db->limit(1);
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	return  $query->row()->stock_qty; 
	}
}

function getSales($sale_item_id, $sale_date)
{    
    $this->db->select_sum('ved_qty');
    $this->db->from('voucher_entry_detail');
    $this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid','inner');
    $this->db->where('voucher_entry_detail.ved_itemid',$sale_item_id);
    $this->db->where('voucher_entry_detail.ved_date',$sale_date);
    $this->db->where('voucher_entry.ve_type','si');
    $this->db->or_where('voucher_entry.ve_type','so');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->row()->ved_qty;
    	return $rowcount;
	}
}

function purchaseReturn($sale_item_id,$sale_date)
{    
    $this->db->select_sum('ved_qty');
    $this->db->from('voucher_entry_detail');
    $this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid','inner');
    $this->db->where('voucher_entry_detail.ved_itemid',$sale_item_id);
    $this->db->where('voucher_entry_detail.ved_date',$sale_date);
    $this->db->where('voucher_entry.ve_type','pr');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->row()->ved_qty;
    	return $rowcount;
	}
}

function salesReturn($sale_item_id,$sale_date)
{    
    $this->db->select_sum('ved_qty');
    $this->db->from('voucher_entry_detail');
    $this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid','inner');
    $this->db->where('voucher_entry_detail.ved_itemid',$sale_item_id);
    $this->db->where('voucher_entry_detail.ved_date',$sale_date);
    $this->db->where('voucher_entry.ve_type','sr');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->row()->ved_qty;
    	return $rowcount;
	}
}



function list_small($product)
{
	$this->db->select('product');
	$this->db->from('product');
	$this->db->where('id',$product);
	$query 			=	$this->db->get();
	return $query->row()->product;		
}

function get_stock_detail($fdate,$tdate,$product,$r_type)
	{
		$output 		=	"";

		if($r_type==1)
		{
			$this->db->select('purchase_detail.*');
			$this->db->from('purchase_detail');
			$this->db->join('product', 'product.id = purchase_detail.purchase_item_id', 'inner');
			$this->db->order_by('purchase_detail.purchase_date','desc');
			$this->db->where('purchase_detail.purchase_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			if($product=='br')
			{
			$p_catg = $this->input->post('p_brand');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.brandid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='gr')
			{
			$p_tcatg = $this->input->post('p_type');
			$p_catg = $this->input->post('p_group');
			$p_scatg = $this->input->post('p_scategory');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_scatg>0)
			{
			$this->db->where('product.subcategoryid',$p_scatg);  
			}
			$this->db->where('product.groupid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='ty')
			{
			$p_gcatg = $this->input->post('p_group');
			$p_catg = $this->input->post('p_type');
			$p_scatg = $this->input->post('p_scategory');
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			if($p_scatg>0)
			{ 
			$this->db->where('product.subcategoryid',$p_scatg); 
			}
			$this->db->where('product.typeid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='sc')
			{
			$p_catg = $this->input->post('p_scategory');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.subcategoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='ca')
			{
			$p_catg = $this->input->post('p_category');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.categoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product==0)
			{
					$query 			=	$this->db->get();
			}

			else if($product>0)
			{
					$this->db->where('purchase_detail.purchase_item_id',$product);
					$query 			=	$this->db->get();
			}

					foreach($query->result() as $row)
			{
					$output 	.=	"<tr><td>".$row->purchase_date."</td>"; 
					$output 	.=	"<td>".$row->purchase_item."</td>";
					$output 	.=	"<td>".$row->purchase_product_qty."</td>";
					$output		.=	"</tr>";
			}
			return $output;
		}

		if($r_type==2)
		{
			$this->db->select('sale_details.*');
			$this->db->from('sale_details');
			$this->db->join('product', 'product.id = sale_details.sale_item_id', 'inner');
			$this->db->order_by('sale_details.sale_date','desc');
			$this->db->where('sale_details.sale_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');

			if($product=='br')
			{
			$p_catg = $this->input->post('p_brand');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.brandid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='gr')
			{
			$p_tcatg = $this->input->post('p_type');
			$p_catg = $this->input->post('p_group');
			$p_scatg = $this->input->post('p_scategory');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_scatg>0)
			{
			$this->db->where('product.subcategoryid',$p_scatg);  
			}
			$this->db->where('product.groupid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='ty')
			{
			$p_gcatg = $this->input->post('p_group');
			$p_catg = $this->input->post('p_type');
			$p_scatg = $this->input->post('p_scategory');
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			if($p_scatg>0)
			{ 
			$this->db->where('product.subcategoryid',$p_scatg); 
			}
			$this->db->where('product.typeid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='sc')
			{
			$p_catg = $this->input->post('p_scategory');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.subcategoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='ca')
			{
			$p_catg = $this->input->post('p_category');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.categoryid',$p_catg); 
			$query=$this->db->get();
			}

			else if($product==0)
			{
    				$query                 =    $this->db->get();
    		}
    		else if($product>=0)
    		{
    			$this->db->where('sale_details.sale_item_id',$product);
				$query                 =    $this->db->get();
    		}

			foreach($query->result() as $row)
			{
			$output 	.=	"<tr><td>".$row->sale_date."</td>"; 
			$output 	.=	"<td>".$row->sale_item."</td>";
			$output 	.=	"<td>".$row->sale_item_qty."</td>";
			$output		.=	"</tr>";
		}
		return $output;
			}

			if($r_type==5)
		{
			$this->db->select('consume.*');
			$this->db->from('consume');
			$this->db->join('product', 'product.id = consume.consume_item_id', 'inner');
			$this->db->order_by('consume.consume_date','desc');
			$this->db->where('consume.consume_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');
			if($product=='br')
			{
			$p_catg = $this->input->post('p_brand');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.brandid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='gr')
			{
			$p_tcatg = $this->input->post('p_type');
			$p_catg = $this->input->post('p_group');
			$p_scatg = $this->input->post('p_scategory');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_scatg>0)
			{
			$this->db->where('product.subcategoryid',$p_scatg);  
			}
			$this->db->where('product.groupid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='ty')
			{
			$p_gcatg = $this->input->post('p_group');
			$p_catg = $this->input->post('p_type');
			$p_scatg = $this->input->post('p_scategory');
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			if($p_scatg>0)
			{ 
			$this->db->where('product.subcategoryid',$p_scatg); 
			}
			$this->db->where('product.typeid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='sc')
			{
			$p_catg = $this->input->post('p_scategory');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.subcategoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='ca')
			{
			$p_catg = $this->input->post('p_category');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.categoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product==0)
			{
					$query 			=	$this->db->get();
			}

			else if($product>0)
			{
					$this->db->where('consume.consume_item_id',$product);
					$query 			=	$this->db->get();
			}

					foreach($query->result() as $row)
			{
					$output 	.=	"<tr><td>".$row->consume_date."</td>"; 
					$output 	.=	"<td>".$row->consume_item."</td>";
					$output 	.=	"<td>".$row->consume_item_qty."</td>";
					$output		.=	"</tr>";
			}
			return $output;
		}

			if($r_type==3)
		{
			$temp  	= array();
    		$temp2 	= array();
    		$tep2 	= array();
    		$sum   	= array();

			$this->db->select('preturn.preturn_date,preturn_details.*,product.product');
			$this->db->from('preturn');
			$this->db->join('preturn_details', 'preturn_details.preturn_id = preturn.preturn_id', 'inner');
			$this->db->join('product', 'product.id = preturn_details.preturn_details_item_id', 'inner');
			$this->db->order_by('preturn.preturn_date','desc');
			$this->db->where('preturn.preturn_date BETWEEN "'.$fdate. '" and "'. $tdate.'"');

			if($product=='br')
			{
			$p_catg = $this->input->post('p_brand');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.brandid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='gr')
			{
			$p_tcatg = $this->input->post('p_type');
			$p_catg = $this->input->post('p_group');
			$p_scatg = $this->input->post('p_scategory');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_scatg>0)
			{
			$this->db->where('product.subcategoryid',$p_scatg);  
			}
			$this->db->where('product.groupid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='ty')
			{
			$p_gcatg = $this->input->post('p_group');
			$p_catg = $this->input->post('p_type');
			$p_scatg = $this->input->post('p_scategory');
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			if($p_scatg>0)
			{ 
			$this->db->where('product.subcategoryid',$p_scatg); 
			}
			$this->db->where('product.typeid',$p_catg);
			$query=$this->db->get();
			}
			else if($product=='sc')
			{
			$p_catg = $this->input->post('p_scategory');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.subcategoryid',$p_catg); 
			$query=$this->db->get();
			}
			else if($product=='ca')
			{
			$p_catg = $this->input->post('p_category');
			$p_tcatg = $this->input->post('p_type');
			$p_gcatg = $this->input->post('p_group');
			if($p_tcatg>0)
			{
			$this->db->where('product.typeid',$p_tcatg);
			}
			if($p_gcatg>0)
			{
			$this->db->where('product.groupid',$p_gcatg);
			}
			$this->db->where('product.categoryid',$p_catg); 
			$query=$this->db->get();
			}

			else if($product==0)
			{
    				$query                 =    $this->db->get();
    		}
    		else if($product>=0)
    		{
    			$this->db->where('preturn_details.preturn_details_item_id',$product);
				$query                 =    $this->db->get();
    		}

    		$data                  =    $query->result_array();

    foreach($data as $key => $value)
    {   

        if(in_array($value['preturn_id'], $temp)){

            $temp2[$value['preturn_id']]['sale_item_id']  	=  $value['preturn_details_item_id'];
            $temp2[$value['preturn_id']]['sale_date'] 	  	=  $value['preturn_date'];
            $temp2[$value['preturn_id']]['sale_item']    	=  $value['product'];
            $temp2[$value['preturn_id']]['purchase_return'] = $this->purchaseReturndetail($value['preturn_id']);
            $temp2[$value['preturn_id']]['sales_return']    = $this->salesReturndetail($value['preturn_id']);



        } else {


            $temp2[$value['preturn_id']]['sale_item_id']  	=  $value['preturn_details_item_id'];
            $temp2[$value['preturn_id']]['sale_date'] 	  	=  $value['preturn_date'];
            $temp2[$value['preturn_id']]['sale_item']    	=  $value['product'];
            $temp2[$value['preturn_id']]['purchase_return'] = $this->purchaseReturndetail($value['preturn_id']);
            $temp2[$value['preturn_id']]['sales_return']    = $this->salesReturndetail($value['preturn_id']);

        } 

    }
		 return $temp2;
			
		}

}

// function getPurchasedetail($sale_item_id,$sale_date)
// {    
//     $this->db->select('purchase_product_qty');
//     $this->db->from('purchase_detail');
//     $this->db->where('purchase_item_id',$sale_item_id);
//     $this->db->where('purchase_date',$sale_date);
//     $query = $this->db->get();
//     if($query->num_rows()==0)
//     {
//     	return 0;
//     }
//     else
//     {
//     	$rowcount = $query->row()->purchase_product_qty;
//     	return $rowcount;
// 	}
// }

// function getSalesdetail($sale_item_id, $sale_date)
// {    
//     $this->db->select('sale_item_qty');
//     $this->db->from('sale_details');
//     $this->db->where('sale_item_id',$sale_item_id);
//     $this->db->where('sale_date', $sale_date);
//     $query = $this->db->get();
//     if($query->num_rows()==0)
//     {
//     	return 0;
//     }
//     else
//     {
//     	$rowcount = $query->row()->sale_item_qty;
//     	return $rowcount;
// 	}
// }

function purchaseReturndetail($preturn_id)
{    
    $this->db->select('preturn_details.preturn_details_qty');
    $this->db->from('preturn_details');
    $this->db->join('preturn', 'preturn.preturn_id = preturn_details.preturn_id', 'inner');
    $this->db->where('preturn_details.preturn_id',$preturn_id);
    $this->db->where('preturn.preturn_type','1');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->row()->preturn_details_qty;
    	return $rowcount;
	}
}

function salesReturndetail($preturn_id)
{    
    $this->db->select('preturn_details.preturn_details_qty');
    $this->db->from('preturn_details');
    $this->db->join('preturn', 'preturn.preturn_id = preturn_details.preturn_id', 'inner');
    $this->db->where('preturn_details.preturn_id',$preturn_id);
    $this->db->where('preturn.preturn_type','2');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->row()->preturn_details_qty;
    	return $rowcount;
	}
}

function purchaseReturndetails($sale_item_id,$sale_date)
{    
    $this->db->select('preturn_details.preturn_details_qty');
    $this->db->from('preturn_details');
    $this->db->join('preturn', 'preturn.preturn_id = preturn_details.preturn_id', 'inner');
    $this->db->where('preturn_details.preturn_details_item_id',$sale_item_id);
    $this->db->where('preturn.preturn_date',$sale_date);
    $this->db->where('preturn.preturn_type','1');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->row()->preturn_details_qty;
    	return $rowcount;
	}
}

function salesReturndetails($sale_item_id,$sale_date)
{    
    $this->db->select('preturn_details.preturn_details_qty');
    $this->db->from('preturn_details');
    $this->db->join('preturn', 'preturn.preturn_id = preturn_details.preturn_id', 'inner');
    $this->db->where('preturn_details.preturn_details_item_id',$sale_item_id);
    $this->db->where('preturn.preturn_date',$sale_date);
    $this->db->where('preturn.preturn_type','2');
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
    	return 0;
    }
    else
    {
    	$rowcount = $query->row()->preturn_details_qty;
    	return $rowcount;
	}
}



	public function getCompany(){
		$this->db->select('*');
		$this->db->from('company');
		$query=$this->db->get();
		return $query->result_array();
	}
	


}