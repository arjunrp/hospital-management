<?php

class Customermodel extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('*');
		$this->db->from('customer');
		$this->db->order_by('id','asc');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{
			$sl_no++;
			if($row->status == '1')
			{
				$image = "<i class='fa fa-check'></i>";
				$status = 0;
			}
			else
			{
				$image = "<i class='fa fa-ban'></i>";
				$status = 1;
			}
			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->customer."</td>"; 
			// $output 	.=	"<td>".$row->address."</td>";
			// $output 	.=	"<td>".$row->street."</td>";
			// $output 	.=	"<td>".$row->city."</td>";
			// $output 	.=	"<td>".$row->zip."</td>";
			// $output 	.=	"<td>".$row->country."</td>";
			// $output 	.=	"<td>".$row->state."</td>";
			$output 	.=	"<td>".$row->phone."</td>";
			$output 	.=	"<td>".$row->email."</td>";
			// $output 	.=	"<td>".$row->fax."</td>";
			// $output 	.=	"<td>".$row->website."</td>";
			// $output 	.=	"<td><img src='".$this->config->item('image_url')."features/small/".$row->feature_image."'></td>";
			 
			// $output 	.=	"<td><a href='".$this->config->item('admin_url')."customer/active/".$row->id."/".$status."'>".$image."</a></td>";
			 $output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."customer/view/".$row->id."'class='btn btn-primary view-btn-edit' title='View'><i class='fa fa-eye'></i></a>
			 <a href='". $this->config->item('admin_url')."customer/edit/".$row->id."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a> 
			  <a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."customer/delete/".$row->id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>";
			 // <a href= '".$this->config->item('admin_url')."customer/delete/".$row->id."' class='btn btn-danger' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}

	function list_small($id)
	{
		$this->db->select('*');
		$this->db->from('customer');
		$this->db->where('id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}

  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('customer');
		$this->db->where('id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}



		function get_country()
	{
			$this->db->select('*');
			$this->db->from('countries'); 
			$query=$this->db->get();
			return $query->result_array();
	}

function get_state()
	{
			$this->db->select('*');
			$this->db->from('states'); 
			$query=$this->db->get();
			return $query->result_array();
	}
function get_city()
	{
			$this->db->select('*');
			$this->db->from('cities'); 
			$query=$this->db->get();
			return $query->result_array();
	}

	function get_states($states)
	{
			$query=$this->db->get_where('states',array('country_id'=>$states));
			return $query->result_array();
	}
	function get_citys($citys)
	{
			$query=$this->db->get_where('cities',array('state_id'=>$citys));
			return $query->result_array();
	}

	function get_payment($id)
	{
		$output 		=	"";
		$sl_no 			=	0;

			$this->db->select('sales.sale_date,sales.amount_payable,sales.amount_paid,sales.amount_balance,customer.payment');
			$this->db->from('sales');
			$this->db->join('customer','customer.id = sales.customer_id', 'inner');
			$this->db->where('customer.id',$id);
			$query 			=	$this->db->get();

					foreach($query->result() as $row)
			{
					$sl_no++;
					$output 	.=	"<tr><td>".$sl_no."</td>";
					$output 	.=	"<td>".$row->sale_date."</td>"; 
					$output 	.=	"<td>".$row->amount_payable."</td>";
					$output 	.=	"<td>".$row->payment."</td>";
					$output 	.=	"<td>".$row->amount_paid."</td>";
					$output 	.=	"<td>".$row->amount_balance."</td>";
					$output		.=	"</tr>";
			}
			return $output;
}


function get_payment_main($id)
	{
		$output 		=	"";
		$sl_no 			=	0;

		

			$this->db->select('sales.amount_payable as samount1,sales.amount_paid as spdamount1,sales.amount_balance as sblamount1');
			$this->db->from('sales');
			$this->db->join('customer','customer.id = sales.customer_id', 'inner');
			$this->db->where('customer.id',$id);
			$query 			=	$this->db->get();
			return $query->result_array();
}

}