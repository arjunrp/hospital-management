<?php

class Purchase_model extends CI_Model {

	function __construct() {
		parent::__construct();

		$this->load->database();
	}

	function list_all()
	{
		$user_type = $this->session->u_type;
		$output 		=	"";
		$status 	 	=	"1";
		$acstatus 	 	=	"";
		$this->db->select('voucher_entry.*,suppliers.sp_vendor');
    	$this->db->from('voucher_entry');
    	$this->db->join('suppliers','suppliers.sp_id = voucher_entry.ve_supplier', 'inner');
    	$this->db->order_by('voucher_entry.ve_vno','desc');
    	$this->db->where('voucher_entry.ve_type','p');
    	$query=$this->db->get();
		foreach($query->result() as $row)
		{
			$status 	 	=	"";
			$acstatus 	 	=	"";



			if($row->ve_pstaus=="NP") { $fp = "<font color='#b20000'><b>Not Completed</b></font>";  }
			elseif($row->ve_pstaus=="FP") { $fp = "<font color='#228B22'><b>Completed</b></font>";  }



			$output 	.=	"<tr><td>".$row->ve_vno."</td>";
			$output 	.=	"<td>".date("d-m-Y",strtotime($row->ve_date))."</td>";
			$output 	.=	"<td>".$row->ve_bill_no."</td>";
			$output 	.=	"<td>".$row->sp_vendor."</td>";
			$output 	.=	"<td>".$row->ve_pono."</td>";
			$output 	.=	"<td>".$fp."</td>";

			 

			$output 	.=	"<td class='btn-group  btn-group-xs'><a href='". $this->config->item('admin_url')."purchase/purchase_View/".$row->ve_id."'class='btn btn-primary view-btn-edit' title='View'><i class='fa fa-eye'></i></a> 
			<a href='".$this->config->item('admin_url')."purchase/edit/".$row->ve_id."'class=' btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
			 <a class=' btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."purchase/delete/".$row->ve_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>"; 
			$output		.=	"</tr>";
		}
		return $output;
	}

	public function set_purchase_stock($stockpid,$purchase_unit_qty,$purchase_date,$ved_batch,$ve_customer){
		
		$this->db->select('*');
		$this->db->from('stock');
		$this->db->where('stock_product_id', $stockpid);
		$this->db->where('stock_dept',$ve_customer);
		$this->db->where('stock_batch',$ved_batch);
		$this->db->order_by("stock_id", "desc");
		$this->db->limit(1);
		$result  		=    $this->db->get();
		$number_of_rows = $result->num_rows();

		if($number_of_rows != 0){
			$sid						=	$result->row()->stock_id;
			$sqty						=	$result->row()->stock_qty;
			$sdate 						=	$result->row()->stock_date;
			$data['stock_qty'] 			=	$sqty + $purchase_unit_qty;
			$data['stock_date']			=	$purchase_date;	
			$data['stock_product_id']	=	$stockpid;
			$data['stock_batch']		=	$ved_batch;
			$data['stock_dept']			=	$ve_customer;
			$data['stock_status']		=	"standby";	
			$data['created_at']			=	$result->row()->created_at;
			if($sdate == $purchase_date)
			{
				$this->db->update('stock',$data, array('stock_id' => $sid));
			}
			else
			{
				$this->db->insert('stock',$data);	
				$data1['stock_status']		=	"transfer";	
				$this->db->update('stock',$data1, array('stock_id' => $sid));
			}
		}
		else
		{
			$data['stock_qty '] 		=	0	+	$purchase_unit_qty;
			$data['stock_date ']		=	$purchase_date;	
			$data['stock_product_id']	=	$stockpid;
			$data['stock_batch']		=	$ved_batch;
			$data['stock_dept']			=	$ve_customer;
			$data['stock_status']		=	"standby";
			$data['created_at']			=	date('Y-m-d H:i:s.') . gettimeofday()['usec'];	
			$this->db->insert('stock',$data);
		}
	}

	public function delete_purchase_stock($stockpid,$purchase_unit_qty,$purchase_date,$ved_batch,$ve_customer){
		
		$this->db->select('*');
		$this->db->from('stock');
		$this->db->where('stock_product_id', $stockpid);
		$this->db->where('stock_dept',$ve_customer);
		$this->db->where('stock_batch',$ved_batch);
		$this->db->where('stock_date >=',$purchase_date);
		$this->db->order_by("stock_id", "asc");
		$query  		=    $this->db->get();
		$result 		=    $query->result_array();
		foreach ($result as $value) {
		$sid						=	$value['stock_id'];
		$sqty						=	$value['stock_qty'];
		$data['stock_qty'] 			=	$sqty - $purchase_unit_qty;
		$this->db->update('stock',$data, array('stock_id' => $sid));
		}

	}

	public function update_purchase_stock($stockpid,$purchase_unit_qty,$purchase_date,$ved_batch,$ve_customer){
		
		$this->db->select('*');
		$this->db->from('stock');
		$this->db->where('stock_product_id',$stockpid);
		$this->db->where('stock_dept',$ve_customer);
		$this->db->where('stock_batch',$ved_batch);
		$this->db->where('stock_date >=',$purchase_date);
		$this->db->order_by("stock_id", "asc");
		$query  				= $this->db->get();
		$number_of_rows 		= $query->num_rows();
		if($number_of_rows != 0){
		$result 		=    $query->result_array();
		foreach ($result as $value) {
		$sid						=	$value['stock_id'];
		$sqty						=	$value['stock_qty'];
		$data['stock_qty'] 			=	$sqty + $purchase_unit_qty;
		$this->db->update('stock',$data, array('stock_id' => $sid));
		}
	}
	else
		{
			$data['stock_qty '] 		=	0	+	$purchase_unit_qty;
			$data['stock_date ']		=	$purchase_date;	
			$data['stock_product_id']	=	$stockpid;
			$data['stock_batch']		=	$ved_batch;
			$data['stock_dept']			=	$ve_customer;
			$data['stock_status']		=	"standby";

			$this->db->insert('stock',$data);
		}

	}


	public function update_PO($ve_pono,$stockpid,$purchase_item_qty,$ved_price){
			
			$this->db->select('porder_detail.pod_id,porder_detail.pod_qty,porder_detail.pod_purqty,porder_detail.pod_price,porder_detail.pod_purprice');
			$this->db->from('porder');
			$this->db->join('porder_detail','porder_detail.pod_poid = porder.po_id');
			$this->db->where('porder.po_pono',$ve_pono);
			$this->db->where('porder_detail.pod_item_id',$stockpid);
			$query = $this->db->get();
			$result = $query->result_array();

			foreach ($result as $key => $value) {

				$pod_id  		= $value['pod_id'];

				$pod_qty 		= $value['pod_qty'];
				$pod_purqty 	= $value['pod_purqty'];
				$total_purqty 	= $pod_purqty + $purchase_item_qty;

				$pod_purprice 	= $value['pod_purprice'];


			$data1['pod_purprice'] 	=	$ved_price;
			$data1['pod_purqty'] 	=	$total_purqty;

			if($total_purqty == $pod_qty)
			{
				$data1['pod_avail'] 	=	"1";
			}
			else
			{
				$data1['pod_avail'] 	=	"2";
			}

			$this->db->where('pod_id', $pod_id);
			$this->db->update('porder_detail', $data1);
		}

	}

	public function set_purchase_balance($amount_paid,$purchase_date){
		
		$this->db->select('*');
		$this->db->from('balance');
		$this->db->order_by("balance_id", "desc");
		$this->db->limit(1);
		$result  		=    $this->db->get();
		$number_of_rows = $result->num_rows();

		if($number_of_rows != 0){
			$bid						=	$result->row()->balance_id;
			$bamount					=	$result->row()->balance_amount;
			$bdate 						=	$result->row()->balance_date;
			$data['balance_amount '] 	=	$bamount - $amount_paid;
			$data['balance_date ']		=	$purchase_date;	
			if($bdate == $purchase_date)
			{
				$this->db->update('balance',$data, array('balance_date' => $purchase_date));
			}
			else
			{
				$this->db->insert('balance',$data);	
			}
		}
		else
		{
			$data['balance_amount '] 	=	0	-	$amount_paid;
			$data['balance_date ']		=	$purchase_date;	
			$this->db->insert('balance',$data);
		}
	}

	public function dlt_purchase_balance($amount_paid,$purchase_date){
		
		$this->db->select('*');
		$this->db->from('balance');
		$this->db->where('balance_date >=',$purchase_date);
		$this->db->order_by("balance_id", "desc");
		$result  		=    $this->db->get();
		$query			=    $result->result_array();
 
			foreach ($query as $value) 
			{
			$bid						=	$value['balance_id'];
			$bamount					=	$value['balance_amount'];
			$data['balance_amount '] 	=	$bamount + $amount_paid;
			$this->db->update('balance',$data, array('balance_id' => $bid));
			}
	}

	public function up_purchase_balance($amount_paid,$purchase_date){
		
		$this->db->select('*');
		$this->db->from('balance');
		$this->db->where('balance_date >=',$purchase_date);
		$this->db->order_by("balance_id", "desc");
		$result  		=    $this->db->get();
		$query			=    $result->result_array();
 
			foreach ($query as $value) 
			{
			$bid						=	$value['balance_id'];
			$bamount					=	$value['balance_amount'];
			$data['balance_amount '] 	=	$bamount - $amount_paid;
			$this->db->update('balance',$data, array('balance_id' => $bid));
			}
	}


	public function getPurchaseId()
     {
		$this->db->select('CAST(ve_vno as SIGNED INTEGER) as ve_vno');
		$this->db->from('voucher_entry');
		$this->db->order_by('ve_vno','desc');
		$this->db->where('ve_type','p');
		$this->db->limit(1);
		$query 			=	$this->db->get();
		if($query->num_rows()==0)
		{
			return '1';
		}
		else
		{
			$result 	= trim($query->row()->ve_vno) + 1;
			// $result++;
			return $result;
		}
	}


	public function getPorders(){

		$this->db->select('porder.*,suppliers.sp_id,suppliers.sp_vendor,suppliers.sp_phone,product.pd_sgst,product.pd_cgst');
    	$this->db->from('porder');
    	$this->db->join('porder_detail','porder_detail.pod_poid = porder.po_id', 'inner'); 
    	$this->db->join('suppliers','suppliers.sp_id = porder.po_supplier', 'inner');
    	$this->db->join('product','product.pd_code = porder_detail.pod_item_id', 'inner'); 
     	$this->db->order_by('porder.po_pono','desc');
     	$this->db->group_by('porder.po_pono');
     	$this->db->where('porder.po_avail !=','fd');
    	$query=$this->db->get();
		return $query->result_array();
	}

	public function getProduct(){
		$this->db->select('*');
		$this->db->from('product');
		$this->db->where('pd_status','1');
		$query=$this->db->get();
		return $query->result_array();
	}

	public function getPurchaseList(){
		$this->db->select('purchase.purchase_id,purchase.purchase_date,purchase.amount_payable,purchase.bill_number,purchase.paid,vendor.vendor,purchase.vendor_id');
    $this->db->from('purchase');
    $this->db->join('vendor','vendor.id = purchase.vendor_id', 'inner');
    $this->db->order_by('purchase.purchase_id','desc');
    $query=$this->db->get();
    return $query->result_array();
  }

  public function getPurchaseview($pid){
	$this->db->select('voucher_entry.*,suppliers.*,voucher_entry_detail.*');
    $this->db->from('voucher_entry');
    $this->db->join('voucher_entry_detail','voucher_entry_detail.ved_veid = voucher_entry.ve_id', 'inner');
    $this->db->join('suppliers','suppliers.sp_id = voucher_entry.ve_supplier', 'inner');
    // $this->db->join('users','users.u_emp_id = voucher_entry.ve_user', 'inner'); 
    $this->db->where('voucher_entry.ve_id',$pid);
    $this->db->where('voucher_entry.ve_type','p');
    $query=$this->db->get();
    return $query->result_array();
  }

  public function getPurchaseLists($pid){
		$this->db->select('purchase.purchase_id,purchase.purchase_date,purchase.vendor_id,purchase.amount,purchase.amount_payable,purchase.discount,purchase.bill_number,purchase_detail.purchase_item,purchase_detail.purchase_product_qty,purchase_detail.purchase_unit_price,purchase_detail.purchase_discount,purchase_detail.purchase_total,vendor.*');
    $this->db->from('purchase');
    $this->db->join('purchase_detail','purchase_detail.purchase_id = purchase.purchase_id', 'inner'); 
    $this->db->join('vendor','vendor.id = purchase.vendor_id', 'inner');
    $this->db->where('purchase.purchase_id',$pid);
    $query=$this->db->get();
    return $query->result_array();
  }
	public function updatePurchases($id){
		if(!empty($id)){
			// $query=$this->db->get_where('purchase_detail',array('status'=>1,'purchase_id'=>$id));
			// 
			 $this->db->select('purchase.purchase_id , purchase.purchase_date,purchase.amount,purchase.amount_payable,purchase.bill_number,purchase_detail.purchase_item,purchase_detail.purchase_product_qty,purchase_detail.purchase_unit_price,purchase_detail.purchase_discount,purchase_detail.purchase_total,purchase_detail.purchase_detail_id,purchase_detail.vendor_id');
    $this->db->from('purchase');
    $this->db->join('purchase_detail','purchase_detail.purchase_id = purchase.purchase_id', 'inner'); 
     $this->db->where('purchase_detail.purchase_id',$id);
    $query=$this->db->get();
			return $query->row_array();}
			else{
				$data['purchase_date']=$this->input->post('date');
				$data['purchase_item']=$this->input->post('item');
				$data['purchase_product_qty']=$this->input->post('qty');
				$data['purchase_unit_price']=$this->input->post('price');
				$data['purchase_discount']=$this->input->post('discount');
				$data['purchase_total']=$this->input->post('total');
				$data['purchase_billnumber']=$this->input->post('billnumber');
				$data['vendor_id']=$this->input->post('vendor');

				$id=$this->input->post('purchase_detail_id');
				$this->db->update('purchase_detail',$data,array('purchase_id'=>$id));
			}
		}
		public function deleteCustomer($id){
			$this->db->delete('customer',array('id'=>$id));
		}
public function delete_purchases($pid){
	$this->db->delete('purchase_detail',array('purchase_detail_id'=>$pid));

}
	
	public function getCompany(){
		$this->db->select('countries.name as coname,states.name as stname,cities.name as ctname,company.*');
		$this->db->from('company');
		$this->db->join('countries','countries.id = company.country','inner');
		$this->db->join('states','states.id = company.state','inner');
		$this->db->join('cities','cities.id = company.city','inner');
		$query=$this->db->get();
		return $query->result_array();
	}

	public function get_voucher_details($ve_id){

	  $this->db->select('*');
      $this->db->from('voucher_entry_detail');
      $this->db->where('ved_veid',$ve_id);
      $this->db->order_by('ved_id','asc');
      $query=$this->db->get();
      return $query->result_array();
	}

	public function get_voucher($ve_id){

	  $this->db->select('ve_apaid');
      $this->db->from('voucher_entry');
      $this->db->where('ve_id',$ve_id);
      $query=$this->db->get();
      return $query->row()->ve_apaid;
	}

	}

	?>
