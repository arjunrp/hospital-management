<?php

class User_type_model extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('*');
		$this->db->from('user_type');
		$this->db->order_by('ut_id','asc');
		$this->db->where('ut_status',1);
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{
			$sl_no++;
			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->ut_user_type."</td>";
			$output 	.=	"<td class='btn-group  btn-group-xs' ><a href='".$this->config->item('admin_url')."user_type/edit/".$row->ut_id."'class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>
			<a class='btn btn-danger delete_employee' data-emp-id='".$this->config->item('admin_url')."user_type/delete/".$row->ut_id."' href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a></td>"; 			 
			$output		.=	"</tr>";
		}
		return $output;
	}

  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('user_type');
		$this->db->where('ut_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}
}