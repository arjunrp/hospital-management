<div class="static-content">
	<div class="page-content">
		<ol class="breadcrumb">
			<li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
			<li><?php echo $page_title; ?></li>
		</ol>

		<div class="page-heading">
			<h1><?php echo $page_title; ?></h1>
		</div>

		<div class="container-fluid">
			<div class="row">
				 

				<div data-widget-group="group1">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="panel-ctrls"></div>
								</div>

								<div class="panel-body no-padding">
									<table id="example" class="table" cellspacing="0" width="100%">
										<thead>
											<tr> 
												<th width="15%"> Name</th>
												<th width="15%"> Email Id</th>
												<th width="15%"> Phone </th>  
												<th width="55%"> About</th>
											</tr>
										</thead>
										<tbody id="ajax_content">
											<?php foreach ($donations as $key => $value) { ?>
												<tr>
													<td><?php echo $value['DON_FNAME']." ".$value['DON_LNAME']; ?></td> 
													<td><?php echo $value['DON_EMAIL']; ?></td>
													<td><?php echo $value['DON_PHONE']; ?></td>
													<td><?php echo $value['DON_ABOUT']; ?></td> 
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>