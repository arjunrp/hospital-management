<link href="<?php echo $this->config->item('base_url');?>assets/datepicker/css/datepicker.css" rel="stylesheet">    
<link href="<?php echo $this->config->item('base_url');?>assets/datepicker/less/datepicker.less" property="stylesheet" rel="stylesheet" media="screen"> 

<div class="static-content">
	<div class="page-content">
		<ol class="breadcrumb">
			<li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
			<li><a href="<?php echo $this->config->item('admin_url')."events"; ?>">Events</a></li>
			<li><?php echo $page_title; ?></li>
		</ol>

		<div class="page-heading">
			<h1><?php echo $page_title; ?></h1>
		</div>

		<div class="container-fluid">
			<div data-widget-group="group1">
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default" data-widget='{"draggable": "false"}'>
							<?php echo form_open_multipart($action); ?>
								<div class="panel-body">

									<div class="row">
										<div class="col-xs-6">
											<div class="form-group">
												<label for="focusinput" class="control-label">Event Title</label>
												<input class="form-control" type="hidden" name="id" value="<?php echo $id;?>">
												<input type="text" name="title" class="form-control validate[required,minSize[2],maxSize[50]]" value="<?php echo $title;?>">
											</div>
										</div>
										<div class="col-xs-3">
											<div class="form-group">
												<label for="focusinput" class="control-label">Date</label>
												<input readonly="" type="text" class="form-control validate[required]" id="dpd1" name="evdate" value="<?php echo $evdate; ?>">
											</div>
										</div>
										<div class="col-xs-3">
											<div class="form-group"> 
												<label for="focusinput" class="control-label">PDF Upload <?php echo $evpdf; ?></label>
												<input type="file" name="pdf_up"> 
											</div>
										</div>
									</div>

									 

									<div class="row">										
										<div class="col-xs-12">
											<div class="form-group">
												<label for="focusinput" class="control-label">Description</label>
												<textarea rows="5" class="form-control validate[required,minSize[2]]" name="desc"><?php echo $desc; ?></textarea>
											</div>
										</div>
									</div>

									 

								</div>
								<div class="panel-footer">
									<div class="row">
										<div class="col-lg-8">
											<button type="submit" class="btn-success btn">Submit</button>
											<button class="btn-default btn" id="cancel_btn">Cancel</button>
										</div>
									</div>
								</div>
							<?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo $this->config->item('base_url');?>assets/datepicker/js/bootstrap-datepicker.js"></script>

<script>
 $('#dpd1').datepicker({
    format: 'yyyy-mm-dd'
 }); 
</script>

