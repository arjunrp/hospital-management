<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>assets/admin/plugins/ckeditor/ckeditor.js"></script>

<div class="static-content">
	<div class="page-content">
		<ol class="breadcrumb">
			<li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
			<li><a href="<?php echo $this->config->item('admin_url')."services"; ?>">Profile</a></li>
			<li><?php echo $page_title; ?></li>
		</ol>

		<div class="page-heading">
			<h1><?php echo $page_title; ?></h1>
		</div>

		<div class="container-fluid">
			<div data-widget-group="group1">
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default" data-widget='{"draggable": "false"}'>
							<?php 
							echo form_open_multipart($action); ?>
								<div class="panel-body">

									<div class="row">
										<?php
										    if($this->uri->segment(3)=="add")
										    $clas   =   'required="required"';
											else
											$clas	=	'';
										?>
										<div class="col-xs-3">
											<div class="form-group">
												<label for="focusinput" class="control-label">Image(200 X 165)</label>
												<input type="file" name="image" <?php echo $clas; ?>>
												<?php if($image != "") { echo "<img src='".$this->config->item("base_url")."uploads/profile/small/".$image."'>"; } ?>
											</div>
										</div>

										<div class="col-xs-4">
											<div class="form-group">
												<label for="focusinput" class="control-label">First Name</label>
												<input class="form-control" type="hidden" name="id" value="<?php echo $id;?>">
												<input type="text" name="ufname" class="form-control validate[required,minSize[2],maxSize[250]]" value="<?php echo $ufname;?>">
											</div>
										</div>
										<div class="col-xs-4">
											<div class="form-group">
												<label for="selector1" class="control-label">Last Name</label>
												<input type="text" name="ulname" class="form-control validate[required,minSize[2],maxSize[250]]" value="<?php echo $ulname;?>">
											</div>
										</div>
									</div>

									<!-- <div class="row">
										<div class="col-xs-3">
											<div class="form-group">
												<label for="selector1" class="control-label">Username</label>
												<input type="text" name="uemail" class="form-control validate[required,minSize[2],maxSize[250]]" value="<?php echo $uemail;?>">
											</div>
										</div>

										<div class="col-xs-3">
											<div class="form-group">
												<label for="selector1" class="control-label">Change Password</label>
												<input type="text" name="upswd" id="upswd" class="form-control validate[required,minSize[2],maxSize[250]]" value="<?php echo $upswd;?>">
											</div>
										</div>  

										<div class="col-xs-3">
											<div class="form-group">
												<label for="selector1" class="control-label">Confirm Password</label>
												<input type="text" id="cpswd" class="form-control validate[required,minSize[2],maxSize[250]]" onblur="Validate()">
											</div>
										</div>
									</div> -->

 									<script type="text/javascript">
//     function Validate() {
//         var password = document.getElementById("upswd").value;
//         var confirmPassword = document.getElementById("cpswd").value;
//         if (password != confirmPassword) {
//             alert("Passwords do not match.");
//             document.getElementById("upswd").value="";
//             document.getElementById("cpswd").value="";
//             return false;
//         }
//         return true;
//     }
// </script>

								</div>
								<div class="panel-footer">
									<div class="row">
										<div class="col-lg-8">
											<button type="submit" class="btn-success btn">Submit</button>
											<button class="btn-default btn" id="cancel_btn">Cancel</button>
										</div>
									</div>
								</div>
							<?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>