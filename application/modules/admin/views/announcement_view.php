<div class="static-content">
	<div class="page-content">
		<ol class="breadcrumb">
			<li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
			<li><a href="<?php echo $this->config->item('admin_url')."announcement"; ?>">Announcement</a></li>
			<li><?php echo $page_title; ?></li>
		</ol>

		<div class="page-heading">
			<h1><?php echo $page_title; ?></h1>
		</div>

		<div class="container-fluid">
			<div data-widget-group="group1">
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default" data-widget='{"draggable": "false"}'> 
							<div class="panel-body">
								<div class="row">
									<?php if(!$rows->ANCMT_PDF) {?>
									<div class="col-md-12">
										<a href="<?php echo $this->config->item('admin_url')."announcement/add_pdf/".$rows->ANCMT_ID; ?>"><button class="btn-success btn pull-right">Add PDF</button></a>
									</div>
									<?php } else { ?>
										<a target="_blank" class="btn btn-danger pull-right" href="<?php echo $this->config->item('base_url')."uploads/announcement_pdf/".$rows->ANCMT_PDF; ?>"><i class="fa fa-download fa-fw"></i>PDF</a>
									<?php } ?>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<table class="table m-n"> 
											<tbody>
												<tr>
													<td width="20%">Title</td>
													<td><?php echo $rows->ANCMT_TITLE; ?></td> 
												</tr> 
												<tr>
													<td>Posting Date</td>
													<td><?php echo $rows->ANCMT_DATE; ?></td> 
												</tr>  
												<tr>
													<td width="20%">Description</td>
													<td><?php echo $rows->ANCMT_DESC; ?></td> 
												</tr> 
												<tr>
													<td>Image</td>
													<td><img src="<?php echo $this->config->item('base_url')."uploads/announcement/small/".$rows->ANCMT_IMAGE; ?>">  </td> 
												</tr>  
											</tbody>
										</table>
									</div>
								</div>
							</div>  
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>