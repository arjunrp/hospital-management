<div class="static-sidebar-wrapper sidebar-bluegray">
  <div class="static-sidebar">
    <div class="sidebar">
      
    <div class="widget stay-on-collapse" id="widget-sidebar">
      <nav class="widget-body">
        <ul class="acc-menu">
            <li <?php if($this->uri->segment(2)=="category") { ?> class="active" <?php } ?>><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="ti ti-home"></i><span>Dashboard</span></a></li>
            <li <?php if($this->uri->segment(2)=="services") {?> class="active" <?php } ?> ><a href="<?php echo $this->config->item('admin_url')."services"; ?>"><i class="fa fa-file"></i> Services </a></li>
            <li <?php if($this->uri->segment(2)=="projects") {?> class="active" <?php } ?> ><a href="<?php echo $this->config->item('admin_url')."projects"; ?>"><i class="fa fa-picture-o"></i> Projects </a></li>
            <li <?php if($this->uri->segment(2)=="about") {?> class="active" <?php } ?> ><a href="<?php echo $this->config->item('admin_url')."about"; ?>"><i class="fa fa-gears"></i> About Us </a></li>
            <li <?php if($this->uri->segment(2)=="contact") {?> class="active" <?php } ?> ><a href="<?php echo $this->config->item('admin_url')."contact"; ?>"><i class="fa fa-newspaper-o"></i> Contact Us </a></li>          
            <li <?php if($this->uri->segment(2)=="testimonials") {?> class="active" <?php } ?> ><a href="<?php echo $this->config->item('admin_url')."testimonials"; ?>"><i class="fa fa-bullhorn"></i> Testimonials </a></li>          

            <li <?php if($this->uri->segment(2)=="news") {?> class="active" <?php } ?> ><a href="<?php echo $this->config->item('admin_url')."news"; ?>"><i class="fa fa-bullhorn"></i> News & Events </a></li>    

            <li><a href="<?php echo $this->config->item('admin_url');?>dashboard/dbBackup"><i class="fa fa-database"></i> Back Up </a></li>   

           
        </ul>
      </nav>
    </div>
  </div>
</div>
</div>