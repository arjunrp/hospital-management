<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>assets/admin/plugins/ckeditor/ckeditor.js"></script>

<div class="static-content">
	<div class="page-content">
		<ol class="breadcrumb">
			<li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
			<li><a href="<?php echo $this->config->item('admin_url')."services"; ?>">About Us</a></li>
			<li><?php echo $page_title; ?></li>
		</ol>

		<div class="page-heading">
			<h1><?php echo $page_title; ?></h1>
		</div>

		<div class="container-fluid">
			<div data-widget-group="group1">
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default" data-widget='{"draggable": "false"}'>
							<?php echo form_open_multipart($action); ?>
								<div class="panel-body">

									<div class="row">
										<div class="col-xs-12">
											<div class="form-group">
												<label for="selector1" class="control-label">Description</label>
												<textarea class="form-control validate[required] ckeditor" name="url"><?php echo $url; ?></textarea>
											</div>
										</div> 
									</div>

									<div class="row">
										<?php
										    if($this->uri->segment(3)=="add")
										    $clas   =   'required="required"';
											else
											$clas	=	'';
										?>
										<div class="col-xs-6">
											<div class="form-group">
												<label for="focusinput" class="control-label">Image(1600 X 400)</label>
												<input class="form-control" type="hidden" name="id" value="<?php echo $id;?>">
												<input type="file" name="image" <?php echo $clas; ?>>
												<?php if($image != "") { echo "<img src='".$this->config->item("base_url")."uploads/about/".$image."'>"; } ?>
											</div>
										</div>
										<div class="col-xs-6">
											<div class="form-group">
												<label for="selector1" class="control-label">Language Type</label>
												<select name="atype" class="form-control">
													<option value="<?php echo $type; ?>"><?php echo $type; ?></option>
													<option value="English"<?php if($type=="English") {?> style="display:none" <?php } ?>>English</option>
													<option value="Arabic"<?php if($type=="Arabic") {?> style="display:none" <?php } ?>>Arabic</option>
												</select>
											</div>
										</div>
									</div>

								</div>
								<div class="panel-footer">
									<div class="row">
										<div class="col-lg-8">
											<button type="submit" class="btn-success btn">Submit</button>
											<button class="btn-default btn" id="cancel_btn">Cancel</button>
										</div>
									</div>
								</div>
							<?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>