<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>assets/admin/plugins/ckeditor/ckeditor.js"></script>

<div class="static-content">
	<div class="page-content">
		<ol class="breadcrumb">
			<li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
			<li><a href="<?php echo $this->config->item('admin_url')."activity"; ?>">Activity</a></li>
			<li><?php echo $page_title; ?></li>
		</ol>

		<div class="page-heading">
			<h1><?php echo $page_title; ?></h1>
		</div>

		<div class="container-fluid">
			<div data-widget-group="group1">
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default" data-widget='{"draggable": "false"}'>
							<?php echo form_open_multipart($action); ?>
								<div class="panel-body">

									<div class="row">										
										<?php
										    if($this->uri->segment(3)=="add")
										    $clas   =   'required="required"';
											else
											$clas	=	'';
										?>
										<div class="col-xs-3">
											<div class="form-group">
												<label for="focusinput" class="control-label">Image (360 X 167)</label>
												<input type="file" name="ACT_DET_IMAGE" <?php echo $clas; ?>>
												<div style="width:50px; background-color:#9D743C;"><?php if($image != "") { echo "<img src='".$this->config->item("base_url")."uploads/activity_details/small/".$image."'>"; } ?></div>
											</div>
										</div> 
										<div class="col-xs-9">
											<div class="form-group">
												<label for="focusinput" class="control-label">Description</label>
												<input class="form-control" type="hidden" name="FK_ACT_HEAD_ID" value="<?php echo $cap_id;?>">
												<input class="form-control" type="hidden" name="ACT_DET_ID" value="<?php echo $id;?>">
												<textarea name="ACT_DET_DESC" class="form-control ckeditor validate['required']"><?php echo $desc; ?></textarea>
											</div>
										</div> 

									</div> 

								</div>
								<div class="panel-footer">
									<div class="row">
										<div class="col-lg-8">
											<button type="submit" class="btn-success btn">Submit</button>
											<button class="btn-default btn" id="cancel_btn">Cancel</button>
										</div>
									</div>
								</div>
							<?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>