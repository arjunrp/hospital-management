<div class="static-content">
	<div class="page-content">
		<ol class="breadcrumb">
			<li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
			<li><a href="<?php echo $this->config->item('admin_url')."activity"; ?>">Activity</a></li>
			<li><?php echo $page_title; ?></li>
		</ol>

		<div class="page-heading">
			<h1><?php echo $page_title; ?></h1>
		</div>

		<div class="container-fluid">
			<div data-widget-group="group1">
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default" data-widget='{"draggable": "false"}'> 
								<div class="panel-body">
								<div class="row">
								<?php if(!$rows->ACT_PDF) {?>
									<div class="col-md-12">
											<a href="<?php echo $this->config->item('admin_url')."activity/add_pdf/".$act_id; ?>"><button class="btn-success btn pull-right">Add PDF</button></a>
										</div>
										<?php } ?>
								</div>
									<div class="row">
										<div class="col-lg-12">
											<table class="table m-n"> 
												<tbody>
													<tr>
														<td width="20%">Description</td>
														<td><?php echo $rows->ACT_DESC; ?></td> 
													</tr> 
													<tr>
														<td>Meta Title</td>
														<td><?php echo $rows->ACT_META_TITLE; ?></td> 
													</tr> 
													<tr>
														<td>Meta Keywords</td>
														<td><?php echo $rows->ACT_META_KEY; ?></td> 
													</tr> 
													<tr>
														<td>Meta Description</td>
														<td><?php echo $rows->ACT_META_DESC; ?></td> 
													</tr>  
												</tbody>
											</table>
										</div>
									</div>
									<div class="row head_foo_cap">
										<div class="col-md-9">
											<h3>FOOTER CAPTIONS</h3>
										</div>
										<div class="col-md-3">
											<a href="<?php echo $this->config->item('admin_url')."activity/footercap/".$act_id; ?>"><button class="btn-success btn pull-right">Add Footer Caption</button></a>
										</div>
									</div>

									<?php foreach ($act_heads as $key => $value) { ?> 
									
									<div class="row">
										<div class="col-md-12">
											<div class="panel panel-info" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="" style="visibility: visible; opacity: 1; display: block; transform: translateY(0px);">
												<div class="panel-heading">
													<h2><?php echo $value['head_title']; ?></h2> 

													<div class="panel-ctrls" data-actions-container="" data-action-collapse="{&quot;target&quot;: &quot;.panel-body&quot;}"></div> 
													<div><a href="<?php echo $this->config->item('admin_url')."activity/delete_head/".$value['head_id']; ?>" class="btn btn-danger pull-right detail_add"><i class="fa fa-trash"></i>Delete Caption</a></div>
													<div><a href="<?php echo $this->config->item('admin_url')."activity/editcap/".$value['head_id']; ?>" class="btn btn-warning pull-right detail_add"><i class="fa fa-pencil"></i>Edit Caption</a></div>
													<div><a href="<?php echo $this->config->item('admin_url')."activity/add_details/".$value['head_id']; ?>" class="btn btn-info pull-right detail_add"><i class="fa fa-plus"></i>Add Details</a></div>
													<?php if($value['pdf1']<>"") {?><div><a href="<?php echo $this->config->item('base_url')."uploads/activity_cap_pdf/".$value['pdf1']; ?>" class="btn btn-success pull-right detail_add"><i class="fa fa-download"></i>PDF 1</a></div><?php } ?>
													<?php if($value['pdf2']<>"") {?><div><a href="<?php echo $this->config->item('base_url')."uploads/activity_cap_pdf/".$value['pdf2']; ?>" class="btn btn-success pull-right detail_add"><i class="fa fa-download"></i>PDF 2</a></div><?php } ?>


												</div>
												<div class="panel-body no-padding" style="display: block;">
													<table class="table table-striped">
														<thead>
															<tr class="info">
																<th>Image</th>
																<th>Description</th> 
																<th width="10%">Options</th>
															</tr>
														</thead>
														<tbody>
														<?php foreach ($value['head_details'] as $key1 => $value1) { ?>
															<tr>
																<td><img src="<?php echo $this->config->item('base_url').'uploads/activity_details/small/'.$value1->ACT_DET_IMAGE; ?>"></td>
																<td><?php echo $value1->ACT_DET_DESC; ?></td>
																<td><a href="<?php echo $this->config->item('admin_url')."activity/edit_details/".$value1->ACT_DET_ID; ?>" class="btn btn-success"><i class="fa fa-edit"></i></a>
																<a href="<?php echo $this->config->item('admin_url')."activity/activity_head_data_delete/".$value1->ACT_DET_ID; ?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
																</td>
															</tr> 
														<?php } ?>
															
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
									<?php } ?>


								</div>  
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>