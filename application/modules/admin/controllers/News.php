<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Newsmodel','News',TRUE);
		$this->template->set('title','News');
		$this->base_uri 			=	$this->config->item('admin_url')."news";
	}

	function index()
	{
		$data['page_title']			=	"News";
		$data['output']				=	$this->News->list_all();
		$this->template->load('template','news',$data);
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->News->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add News";
		$data['action']				=	$this->base_uri.'/insert';
		$data['id']					=	"";
		$data['ndate']				=	"";
		$data['url']				=	"";
		$data['news']				=	"";
		$data['image']				=	"";
		$data['type']				=	"";
		$this->template->load('template','news_add',$data);
	}

	function insert()
	{

		$title 									=	$this->input->post('news');
		$this->tab_groups['news']				=	$title;
		$news_date 								=	$this->input->post('ndate');
		$this->tab_groups['news_date']			=	date("Y-m-d",strtotime($news_date));
		$this->tab_groups['news_detail']		=	$this->input->post('url');
		$this->tab_groups['news_type']			=	$this->input->post('ntype');
		$this->db->insert('newsmaster',$this->tab_groups);

		$id 								=	$this->db->insert_id();
		$config['upload_path']				=	$this->config->item('image_path').'news/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'20000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ($this->upload->do_upload('image'))
		{		
			
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['news_image']	=	$data['upload_data']['file_name'];
			$this->db->update('newsmaster', $this->form_image,array('news_id'=>$id));
			
		}	
		else
		{
		
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
			
		}

		delete_files($this->config->item('image_path').'news/original/');
		redirect($this->base_uri);
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit News";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->News->get_one_banner($id);
		$data['id']					=	$row->news_id;
		$data['ndate']				=	$row->news_date;
		$data['url']				=	$row->news_detail;
		$data['news']				=	$row->news;
		$data['image']				=	$row->news_image;
		$data['type']				=	$row->news_type;
		$this->template->load('template','news_add',$data);
	}

	function update()
	{
		$id 								=	$this->input->post('id');
		$title 								=	$this->input->post('news');
		$this->tab_groups['news']			=	$title;
		$news_date 							=	$this->input->post('ndate');
		$this->tab_groups['news_date']		=	date("Y-m-d",strtotime($news_date));
		$this->tab_groups['news_detail']	=	$this->input->post('url');
		$this->tab_groups['news_type']		=	$this->input->post('ntype');
		$this->db->update('newsmaster', $this->tab_groups,array('news_id'=>$id));

		$config['upload_path']				=	$this->config->item('image_path').'news/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'20000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (  $this->upload->do_upload('image'))
		{		
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['news_image']=	$data['upload_data']['file_name'];
			$this->db->update('newsmaster', $this->form_image,array('news_id'=>$id));
		}	
		else
		{
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
		}
		delete_files($this->config->item('image_path').'news/original/');
		redirect($this->base_uri);
	}

	function process_uploaded_image($filename)
	{
		$large_path 	=	$this->config->item('image_path').'news/original/'.$filename;
		$medium_path 	=	$this->config->item('image_path').'news/'.$filename;
		$thumb_path 	=	$this->config->item('image_path').'news/small/'.$filename;
		
		$medium_config['image_library'] 	=	'gd2';
		$medium_config['source_image'] 		=	$large_path;
		$medium_config['new_image'] 		=	$medium_path;
		$medium_config['maintain_ratio'] 	=	TRUE;
		$medium_config['width'] 			=	1600;
		$medium_config['height'] 			=	400;
		$this->load->library('image_lib', $medium_config);
		$this->image_lib->initialize($medium_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		$thumb_config['image_library'] 		=	'gd2';
		$thumb_config['source_image'] 		=	$large_path;
		$thumb_config['new_image'] 			=	$thumb_path;
		$thumb_config['maintain_ratio'] 	=	TRUE;
		$thumb_config['width'] 				=	50;
		$thumb_config['height'] 			=	50;
		$this->load->library('image_lib', $thumb_config);
		$this->image_lib->initialize($thumb_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		return true;    
	}

	function active($id,$status)
	{
		$data 			=	array('news_status'=>$status);
		$this->db->update('newsmaster',$data,array('news_id'=>$id));
		$this->list_ajax();
	}

	function delete($id)
	{
		$row 			=	$this->News->get_one_banner($id);
		unlink($this->config->item('image_path').'news/small/'.$row->news_image);
		unlink($this->config->item('image_path').'news/'.$row->news_image);
		$this->db->delete('newsmaster',array('news_id'=>$id));
		$this->list_ajax();
	}
}