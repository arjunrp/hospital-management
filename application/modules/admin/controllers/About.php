<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Aboutmodel','About',TRUE);
		$this->template->set('title','About');
		$this->base_uri 			=	$this->config->item('admin_url')."about";
	}

	function index()
	{
		$data['page_title']			=	"About Us";
		$data['output']				=	$this->About->list_all();
		$this->template->load('template','about',$data);
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->About->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add About Us";
		$data['action']				=	$this->base_uri.'/insert';
		$data['id']					=	"";
		$data['url']				=	"";
		$data['image']				=	"";
		$data['type']				=	"";
		$this->template->load('template','about_add',$data);
	}

	function insert()
	{

		$this->tab_groups['about_description']		=	$this->input->post('url');
		$this->tab_groups['about_type']		=	$this->input->post('atype');
		$this->db->insert('activity_head',$this->tab_groups);

		$id 								=	$this->db->insert_id();
		$config['upload_path']				=	$this->config->item('image_path').'about/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'20000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ($this->upload->do_upload('image'))
		{		
			
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['about_image']	=	$data['upload_data']['file_name'];
			$this->db->update('activity_head', $this->form_image,array('about_id'=>$id));
			
		}	
		else
		{
		
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
			
		}

		delete_files($this->config->item('image_path').'about/original/');
		redirect($this->base_uri);
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit About Us";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->About->get_one_banner($id);
		$data['id']					=	$row->about_id;
		$data['url']				=	$row->about_description;
		$data['image']				=	$row->about_image;
		$data['type']				=	$row->about_type;
		$this->template->load('template','about_add',$data);
	}

	function update()
	{
		$id 								=	$this->input->post('id');
		$this->tab_groups['about_description']		=	$this->input->post('url');
		$this->tab_groups['about_type']		=	$this->input->post('atype');
		$this->db->update('activity_head', $this->tab_groups,array('about_id'=>$id));

		$config['upload_path']				=	$this->config->item('image_path').'about/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'20000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (  $this->upload->do_upload('image'))
		{		
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['about_image']=	$data['upload_data']['file_name'];
			$this->db->update('activity_head', $this->form_image,array('about_id'=>$id));
		}	
		else
		{
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
		}
		delete_files($this->config->item('image_path').'about/original/');
		redirect($this->base_uri);
	}

	function process_uploaded_image($filename)
	{
		$large_path 	=	$this->config->item('image_path').'about/original/'.$filename;
		$medium_path 	=	$this->config->item('image_path').'about/'.$filename;
		$thumb_path 	=	$this->config->item('image_path').'about/small/'.$filename;
		
		$medium_config['image_library'] 	=	'gd2';
		$medium_config['source_image'] 		=	$large_path;
		$medium_config['new_image'] 		=	$medium_path;
		$medium_config['maintain_ratio'] 	=	TRUE;
		$medium_config['width'] 			=	1600;
		$medium_config['height'] 			=	400;
		$this->load->library('image_lib', $medium_config);
		$this->image_lib->initialize($medium_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		$thumb_config['image_library'] 		=	'gd2';
		$thumb_config['source_image'] 		=	$large_path;
		$thumb_config['new_image'] 			=	$thumb_path;
		$thumb_config['maintain_ratio'] 	=	TRUE;
		$thumb_config['width'] 				=	50;
		$thumb_config['height'] 			=	50;
		$this->load->library('image_lib', $thumb_config);
		$this->image_lib->initialize($thumb_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		return true;    
	}

	function active($id,$status)
	{
		$data 			=	array('about_status'=>$status);
		$this->db->update('activity_head',$data,array('about_id'=>$id));
		$this->list_ajax();
	}

	function delete($id)
	{
		$row 			=	$this->About->get_one_banner($id);
		unlink($this->config->item('image_path').'about/small/'.$row->about_image);
		unlink($this->config->item('image_path').'about/'.$row->about_image);
		$this->db->delete('activity_head',array('about_id'=>$id));
		$this->list_ajax();
	}
}