<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		// if(!$this->session->userdata('logged_in'))
		// {
		// 	redirect($this->config->item('admin_url'));
		// }
		$this->load->model('Profilemodel','Profile',TRUE);
		$this->template->set('title','Profile');
		$this->base_uri 			=	$this->config->item('admin_url')."profile";
	}

	function index()
	{
		$data['page_title']			=	"Profile";
		$data['output']				=	$this->Profile->list_all();
		$this->template->load('template','profile_add',$data);
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Profile->list_all();
	}

	// function add()
	// {
	// 	$data['page_title']			=	"Add Services";
	// 	$data['action']				=	$this->base_uri.'/insert';
	// 	$data['id']					=	"";
	// 	$data['title']				=	"";
	// 	$data['url']				=	"";
	// 	$data['image']				=	"";
	// 	$data['type']				=	"";
	// 	$this->template->load('template','services_add',$data);
	// }

	// function insert()
	// {

	// 	$title 									=	$this->input->post('title');
	// 	$this->tab_groups['service_heading']			=	$title;
	// 	$this->tab_groups['service_description']		=	$this->input->post('url');
	// 	$this->tab_groups['service_type']		=	$this->input->post('stype');
	// 	$this->db->insert('usermaster',$this->tab_groups);

	// 	$id 								=	$this->db->insert_id();
	// 	$config['upload_path']				=	$this->config->item('image_path').'services/original/';
	// 	$config['allowed_types']			=	'gif|jpg|jpeg|png';
	// 	$config['max_size']					=	'20000';
	// 	@$config['file_name']				=	$id.$config['file_ext'];
	// 	$this->load->library('upload', $config);
	// 	$this->upload->initialize($config);

	// 	if ($this->upload->do_upload('image'))
	// 	{		
			
	// 		$data = array('upload_data' => $this->upload->data());
	// 		$this->process_uploaded_image($data['upload_data']['file_name']);
	// 		$this->form_image['service_image']	=	$data['upload_data']['file_name'];
	// 		$this->db->update('usermaster', $this->form_image,array('service_id'=>$id));
			
	// 	}	
	// 	else
	// 	{
		
	// 		$error = $this->upload->display_errors();	
	// 		$this->messages->add($error, 'error');
			
	// 	}

	// 	delete_files($this->config->item('image_path').'services/original/');
	// 	redirect($this->base_uri);
	// }

	function edit($id)
	{
		$data['page_title']			=	"Edit Profile";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Profile->get_one_banner($id);
		$data['id']					=	$row->USM_ID;
		$data['ufname']				=	$row->USM_FIRST_NAME;
		$data['ulname']				=	$row->USM_LAST_NAME;
		$data['uemail']				=	$row->USM_EMAIL;
		$data['upswd']				=	$row->USM_PASSWORD;
		$data['image']				=	$row->USM_AVATAR;
		$this->template->load('template','profile_add',$data);
	}

	function update()
	{
		$id 									=	$this->input->post('id');
		$title 									=	$this->input->post('ufname');
		$this->tab_groups['USM_FIRST_NAME']		=	$title;
		$this->tab_groups['USM_LAST_NAME']		=	$this->input->post('ulname');
		// $this->tab_groups['USM_EMAIL']			=	$this->input->post('uemail');
		// $this->tab_groups['USM_PASSWORD']		=	SHA1($this->input->post('upswd'));
		$this->db->update('usermaster', $this->tab_groups,array('USM_ID'=>$id));

		$config['upload_path']				=	$this->config->item('image_path').'profile/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'20000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (  $this->upload->do_upload('image'))
		{		
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['USM_AVATAR']=	$data['upload_data']['file_name'];
			$this->db->update('usermaster', $this->form_image,array('USM_ID'=>$id));
		}	
		else
		{
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
		}
		delete_files($this->config->item('image_path').'profile/original/');
		$this->edit($id);
	}

	function process_uploaded_image($filename)
	{
		$large_path 	=	$this->config->item('image_path').'profile/original/'.$filename;
		$medium_path 	=	$this->config->item('image_path').'profile/'.$filename;
		$thumb_path 	=	$this->config->item('image_path').'profile/small/'.$filename;
		
		$medium_config['image_library'] 	=	'gd2';
		$medium_config['source_image'] 		=	$large_path;
		$medium_config['new_image'] 		=	$medium_path;
		$medium_config['maintain_ratio'] 	=	TRUE;
		$medium_config['width'] 			=	1600;
		$medium_config['height'] 			=	400;
		$this->load->library('image_lib', $medium_config);
		$this->image_lib->initialize($medium_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		$thumb_config['image_library'] 		=	'gd2';
		$thumb_config['source_image'] 		=	$large_path;
		$thumb_config['new_image'] 			=	$thumb_path;
		$thumb_config['maintain_ratio'] 	=	TRUE;
		$thumb_config['width'] 				=	200;
		$thumb_config['height'] 			=	165;
		$this->load->library('image_lib', $thumb_config);
		$this->image_lib->initialize($thumb_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		return true;    
	}

	function active($id,$status)
	{
		$data 			=	array('USM_STATUS'=>$status);
		$this->db->update('usermaster',$data,array('USM_ID'=>$id));
		$this->list_ajax();
	}

	function delete($id)
	{
		$row 			=	$this->Profile->get_one_banner($id);
		unlink($this->config->item('image_path').'profile/small/'.$row->USM_AVATAR);
		unlink($this->config->item('image_path').'profile/'.$row->USM_AVATAR);
		$this->db->delete('usermaster',array('USM_ID'=>$id));
		$this->list_ajax();
	}
}