<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MX_Controller {
	
	function __construct()
	{
		parent::__construct();
// 		if(!$_SESSION['logged_in'] === true) 
// 		{
// 	    	redirect($this->config->item('admin_url'));
// 	    }
	    $this->template->set('title', 'Dashboard');
        $this->load->model("Dashboardmodel","Dashboard",TRUE);
	    $this->base_uri 	=	$this->config->item('admin_url')."dashboard";
	}
	
	public function index()
	{
         
		$data['page_title']   		  =	'Dashboard';
        $data['services_cnt']         =   $this->Dashboard->get_banner_count();
        $data['projects_cnt']         =   $this->Dashboard->get_page_count();
        $data['testimonials_cnt']     =   $this->Dashboard->get_activity_count();
        $data['news_cnt']             =   $this->Dashboard->get_news_count();
        // $data['events_cnt']         =   $this->Dashboard->get_events_count();
		$this->template->load('template','dashboard',$data);
	}

    function dbBackup( )
	{
		$user      			= $this->db->username; 
	    $pass      			= $this->db->password; 
	    $host      			= $this->db->hostname; 
	    $name             	= $this->db->database; 
	    $backup_name        = "ties_db_backup.sql";
	    $tables             = "*";

        $mysqli = new mysqli($host,$user,$pass,$name); 
        $mysqli->select_db($name); 
        $mysqli->query("SET NAMES 'utf8'");

        $queryTables    = $mysqli->query('SHOW TABLES'); 
        while($row = $queryTables->fetch_row()) 
        { 
            $target_tables[] = $row[0]; 
        }   
        if($tables == "*") 
        { 
        	$tables = array();
            $result = $mysqli->query('SHOW TABLES'); 
            while($row = mysqli_fetch_row($result))
            {
                $target_tables[] = $row[0];
            }
        } else {
        	$target_tables = array_intersect( $target_tables, $tables); 
        }
        foreach($target_tables as $table)
        {
            $result         =   $mysqli->query('SELECT * FROM '.$table);  
            $fields_amount  =   $result->field_count;  
            $rows_num=$mysqli->affected_rows;     
            $res            =   $mysqli->query('SHOW CREATE TABLE '.$table); 
            $TableMLine     =   $res->fetch_row();
            $content        = (!isset($content) ?  '' : $content) . "\n\n".$TableMLine[1].";\n\n";

            for ($i = 0, $st_counter = 0; $i < $fields_amount;   $i++, $st_counter=0) 
            {
                while($row = $result->fetch_row())  
                { //when started (and every after 100 command cycle):
                    if ($st_counter%100 == 0 || $st_counter == 0 )  
                    {
                            $content .= "\nINSERT INTO ".$table." VALUES";
                    }
                    $content .= "\n(";
                    for($j=0; $j<$fields_amount; $j++)  
                    { 
                        $row[$j] = str_replace("\n","\\n", addslashes($row[$j]) ); 
                        if (isset($row[$j]))
                        {
                            $content .= '"'.$row[$j].'"' ; 
                        }
                        else 
                        {   
                            $content .= '""';
                        }     
                        if ($j<($fields_amount-1))
                        {
                                $content.= ',';
                        }      
                    }
                    $content .=")";
                    //every after 100 command cycle [or at last line] ....p.s. but should be inserted 1 cycle eariler
                    if ( (($st_counter+1)%100==0 && $st_counter!=0) || $st_counter+1==$rows_num) 
                    {   
                        $content .= ";";
                    } 
                    else 
                    {
                        $content .= ",";
                    } 
                    $st_counter=$st_counter+1;
                }
            } $content .="\n\n\n";
        } 
        $backup_name = $backup_name ? $backup_name : $name.".sql";
        header('Content-Type: application/octet-stream');   
        header("Content-Transfer-Encoding: Binary"); 
        header("Content-disposition: attachment; filename=\"".$backup_name."\"");  
        echo $content; exit;
    }


}
