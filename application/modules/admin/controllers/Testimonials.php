<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testimonials extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Testimonialsmodel','Testimonials',TRUE);
		$this->template->set('title','Testimonials');
		$this->base_uri 			=	$this->config->item('admin_url')."testimonials";
	}

	function index()
	{
		$data['page_title']			=	"Testimonials";
		$data['output']				=	$this->Testimonials->list_all();
		$this->template->load('template','testimonials',$data);
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Testimonials->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Testimonials";
		$data['action']				=	$this->base_uri.'/insert';
		$data['id']					=	"";
		$data['name']				=	"";
		$data['comment']				=	"";
		$data['type']				=	"";
		$this->template->load('template','testimonials_add',$data);
	}

	function insert()
	{

		$title 										=	$this->input->post('name');
		$this->tab_groups['testimonial_name']		=	$title;
		$this->tab_groups['testimonial_comment']	=	$this->input->post('comment');
		$this->tab_groups['testimonial_type']		=	$this->input->post('ttype');
		$this->db->insert('eventsmaster',$this->tab_groups);

		$id 										=	$this->db->insert_id();
		redirect($this->base_uri);
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Testimonials";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Testimonials->get_one_banner($id);
		$data['id']					=	$row->testimonial_id;
		$data['name']				=	$row->testimonial_name;
		$data['comment']			=	$row->testimonial_comment;
		$data['type']				=	$row->testimonial_type;
		$this->template->load('template','testimonials_add',$data);
	}

	function update()
	{
		$id 										=	$this->input->post('id');
		$title 										=	$this->input->post('name');
		$this->tab_groups['testimonial_name']		=	$title;
		$this->tab_groups['testimonial_comment']	=	$this->input->post('comment');
		$this->tab_groups['testimonial_type']		=	$this->input->post('ttype');
		$this->db->update('eventsmaster', $this->tab_groups,array('testimonial_id'=>$id));
		redirect($this->base_uri);
	}

	function active($id,$status)
	{
		$data 			=	array('testimonial_status'=>$status);
		$this->db->update('eventsmaster',$data,array('testimonial_id'=>$id));
		$this->list_ajax();
	}

	function delete($id)
	{
		$row 			=	$this->Testimonials->get_one_banner($id);
		$this->db->delete('eventsmaster',array('testimonial_id'=>$id));
		$this->list_ajax();
	}
}