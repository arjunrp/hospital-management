<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Contactmodel','Contact',TRUE);
		$this->template->set('title','Contact');
		$this->base_uri 			=	$this->config->item('admin_url')."contact";
	}

	function index()
	{
		$data['page_title']			=	"Contact Us";
		$data['output']				=	$this->Contact->list_all();
		$this->template->load('template','contact',$data);
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Contact->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Contact Us";
		$data['action']				=	$this->base_uri.'/insert';
		$data['id']					=	"";
		$data['address']			=	"";
		$data['phone']				=	"";
		$data['phone2']				=	"";
		$data['email']				=	"";
		$data['time']				=	"";
		$data['md_name']			=	"";
		$data['md_qual']			=	"";
		$data['url']				=	"";
		$data['image']				=	"";
		$data['type']				=	"";
		$this->template->load('template','contact_add',$data);
	}

	function insert()
	{

		$title 									=	$this->input->post('address');
		$this->tab_groups['address']			=	$title;
		$this->tab_groups['phone']		=	$this->input->post('phone');
		$this->tab_groups['phone2']		=	$this->input->post('phone2');
		$this->tab_groups['email']		=	$this->input->post('email');
		$this->tab_groups['time']		=	$this->input->post('time');
		$this->tab_groups['md_name']		=	$this->input->post('md_name');
		$this->tab_groups['md_qual']		=	$this->input->post('md_qual');
		$this->tab_groups['md_msg']		=	$this->input->post('url');
		$this->tab_groups['contact_type']		=	$this->input->post('ctype');
		$this->db->insert('announcementmaster',$this->tab_groups);

		$id 								=	$this->db->insert_id();
		$config['upload_path']				=	$this->config->item('image_path').'contact/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'20000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ($this->upload->do_upload('image'))
		{		
			
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['md_foto']	=	$data['upload_data']['file_name'];
			$this->db->update('announcementmaster', $this->form_image,array('contact_id'=>$id));
			
		}	
		else
		{
		
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
			
		}

		delete_files($this->config->item('image_path').'contact/original/');
		redirect($this->base_uri);
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Contact Us";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Contact->get_one_banner($id);
		$data['id']					=	$row->contact_id;
		$data['address']			=	$row->address;
		$data['phone']				=	$row->phone;
		$data['phone2']				=	$row->phone2;
		$data['email']				=	$row->email;
		$data['time']				=	$row->time;
		$data['md_name']			=	$row->md_name;
		$data['md_qual']			=	$row->md_qual;
		$data['url']				=	$row->md_msg;
		$data['image']				=	$row->md_foto;
		$data['type']				=	$row->contact_type;
		$this->template->load('template','contact_add',$data);
	}

	function update()
	{
		$id 								=	$this->input->post('id');
		$title 								=	$this->input->post('address');
		$this->tab_groups['address']		=	$title;
		$this->tab_groups['phone']			=	$this->input->post('phone');
		$this->tab_groups['phone2']			=	$this->input->post('phone2');
		$this->tab_groups['email']			=	$this->input->post('email');
		$this->tab_groups['time']			=	$this->input->post('time');
		$this->tab_groups['md_name']		=	$this->input->post('md_name');
		$this->tab_groups['md_qual']		=	$this->input->post('md_qual');
		$this->tab_groups['md_msg']			=	$this->input->post('url');
		$this->tab_groups['contact_type']	=	$this->input->post('ctype');
		$this->db->update('announcementmaster', $this->tab_groups,array('contact_id'=>$id));

		$config['upload_path']				=	$this->config->item('image_path').'contact/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'20000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (  $this->upload->do_upload('image'))
		{		
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['md_foto']=	$data['upload_data']['file_name'];
			$this->db->update('announcementmaster', $this->form_image,array('contact_id'=>$id));
		}	
		else
		{
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
		}
		delete_files($this->config->item('image_path').'contact/original/');
		redirect($this->base_uri);
	}

	function process_uploaded_image($filename)
	{
		$large_path 	=	$this->config->item('image_path').'contact/original/'.$filename;
		$medium_path 	=	$this->config->item('image_path').'contact/'.$filename;
		$thumb_path 	=	$this->config->item('image_path').'contact/small/'.$filename;
		
		$medium_config['image_library'] 	=	'gd2';
		$medium_config['source_image'] 		=	$large_path;
		$medium_config['new_image'] 		=	$medium_path;
		$medium_config['maintain_ratio'] 	=	TRUE;
		$medium_config['width'] 			=	1600;
		$medium_config['height'] 			=	400;
		$this->load->library('image_lib', $medium_config);
		$this->image_lib->initialize($medium_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		$thumb_config['image_library'] 		=	'gd2';
		$thumb_config['source_image'] 		=	$large_path;
		$thumb_config['new_image'] 			=	$thumb_path;
		$thumb_config['maintain_ratio'] 	=	TRUE;
		$thumb_config['width'] 				=	50;
		$thumb_config['height'] 			=	50;
		$this->load->library('image_lib', $thumb_config);
		$this->image_lib->initialize($thumb_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		return true;    
	}

	function active($id,$status)
	{
		$data 			=	array('status'=>$status);
		$this->db->update('announcementmaster',$data,array('contact_id'=>$id));
		$this->list_ajax();
	}

	function delete($id)
	{
		$row 			=	$this->Contact->get_one_banner($id);
		unlink($this->config->item('image_path').'contact/small/'.$row->md_foto);
		unlink($this->config->item('image_path').'contact/'.$row->md_foto);
		$this->db->delete('announcementmaster',array('contact_id'=>$id));
		$this->list_ajax();
	}
}