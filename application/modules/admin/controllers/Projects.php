<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projects extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Projectsmodel','Projects',TRUE);
		$this->template->set('title','Projects');
		$this->base_uri 			=	$this->config->item('admin_url')."projects";
	}

	function index()
	{
		$data['page_title']			=	"Projects";
		$data['output']				=	$this->Projects->list_all();
		$this->template->load('template','projects',$data);
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Projects->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Projects";
		$data['action']				=	$this->base_uri.'/insert';
		$data['id']					=	"";
		$data['title']				=	"";
		$data['url']				=	"";
		$data['image']				=	"";
		$data['type']				=	"";
		$this->template->load('template','projects_add',$data);
	}

	function insert()
	{

		$title 									=	$this->input->post('title');
		$this->tab_groups['portfolio_project']			=	$title;
		$this->tab_groups['portfolio_description']		=	$this->input->post('url');
		$this->tab_groups['portfolio_type']		=	$this->input->post('ptype');
		$this->db->insert('activitymaster',$this->tab_groups);

		$id 								=	$this->db->insert_id();
		$config['upload_path']				=	$this->config->item('image_path').'projects/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'20000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ($this->upload->do_upload('image'))
		{		
			
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['portfolio_image']	=	$data['upload_data']['file_name'];
			$this->db->update('activitymaster', $this->form_image,array('portfolio_id'=>$id));
			
		}	
		else
		{
		
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
			
		}

		delete_files($this->config->item('image_path').'projects/original/');
		redirect($this->base_uri);
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Projects";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Projects->get_one_banner($id);
		$data['id']					=	$row->portfolio_id;
		$data['title']				=	$row->portfolio_project;
		$data['url']				=	$row->portfolio_description;
		$data['image']				=	$row->portfolio_image;
		$data['type']				=	$row->portfolio_type;
		$this->template->load('template','projects_add',$data);
	}

	function update()
	{
		$id 								=	$this->input->post('id');
		$title 								=	$this->input->post('title');
		$this->tab_groups['portfolio_project']		=	$title;
		$this->tab_groups['portfolio_description']		=	$this->input->post('url');
		$this->tab_groups['portfolio_type']		=	$this->input->post('ptype');
		$this->db->update('activitymaster', $this->tab_groups,array('portfolio_id'=>$id));

		$config['upload_path']				=	$this->config->item('image_path').'projects/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'20000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (  $this->upload->do_upload('image'))
		{		
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['portfolio_image']=	$data['upload_data']['file_name'];
			$this->db->update('activitymaster', $this->form_image,array('portfolio_id'=>$id));
		}	
		else
		{
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
		}
		delete_files($this->config->item('image_path').'projects/original/');
		redirect($this->base_uri);
	}

	function process_uploaded_image($filename)
	{
		$large_path 	=	$this->config->item('image_path').'projects/original/'.$filename;
		$medium_path 	=	$this->config->item('image_path').'projects/'.$filename;
		$thumb_path 	=	$this->config->item('image_path').'projects/small/'.$filename;
		
		$medium_config['image_library'] 	=	'gd2';
		$medium_config['source_image'] 		=	$large_path;
		$medium_config['new_image'] 		=	$medium_path;
		$medium_config['maintain_ratio'] 	=	TRUE;
		$medium_config['width'] 			=	1600;
		$medium_config['height'] 			=	400;
		$this->load->library('image_lib', $medium_config);
		$this->image_lib->initialize($medium_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		$thumb_config['image_library'] 		=	'gd2';
		$thumb_config['source_image'] 		=	$large_path;
		$thumb_config['new_image'] 			=	$thumb_path;
		$thumb_config['maintain_ratio'] 	=	TRUE;
		$thumb_config['width'] 				=	50;
		$thumb_config['height'] 			=	50;
		$this->load->library('image_lib', $thumb_config);
		$this->image_lib->initialize($thumb_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		return true;    
	}

	function active($id,$status)
	{
		$data 			=	array('portfolio_status'=>$status);
		$this->db->update('activitymaster',$data,array('portfolio_id'=>$id));
		$this->list_ajax();
	}

	function delete($id)
	{
		$row 			=	$this->Projects->get_one_banner($id);
		unlink($this->config->item('image_path').'projects/small/'.$row->portfolio_image);
		unlink($this->config->item('image_path').'projects/'.$row->portfolio_image);
		$this->db->delete('activitymaster',array('portfolio_id'=>$id));
		$this->list_ajax();
	}
}