<?php

class Testimonialsmodel extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('*');
		$this->db->from('eventsmaster');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{
			$sl_no++;
			if($row->testimonial_status == '1')
			{
				$image = "<i class='fa fa-check'></i>";
				$status = 0;
			}
			else
			{
				$image = "<i class='fa fa-ban'></i>";
				$status = 1;
			}
			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->testimonial_name."</td>";
			$output 	.=	"<td>".$row->testimonial_comment."</td>";
			$output 	.=	"<td>".$row->testimonial_type."</td>";
			$output 	.=	"<td><a href=javascript:confirm_del('".$this->config->item('admin_url')."testimonials/active/".$row->testimonial_id."/".$status."')>".$image."</a></td>";
			$output 	.=	"<td><a href='".$this->config->item('admin_url')."testimonials/edit/".$row->testimonial_id."'><i class='fa fa-pencil'></i></a></td>";
			$output		.=	"<td><a href=javascript:confirm_del('".$this->config->item('admin_url')."testimonials/delete/".$row->testimonial_id."')><i class='fa fa-trash'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}

  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('eventsmaster');
		$this->db->where('testimonial_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}
}