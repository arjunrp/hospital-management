<?php

class Contactmodel extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('*');
		$this->db->from('announcementmaster');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{
			$sl_no++;
			if($row->status == '1')
			{
				$image = "<i class='fa fa-check'></i>";
				$status = 0;
			}
			else
			{
				$image = "<i class='fa fa-ban'></i>";
				$status = 1;
			}
			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->address."</td>";
			$output 	.=	"<td>".$row->phone."</td>";
			$output 	.=	"<td>".$row->phone2."</td>";
			$output 	.=	"<td>".$row->email."</td>";
			$output 	.=	"<td>".$row->time."</td>";
			$output 	.=	"<td>".$row->md_name."</td>";
			$output 	.=	"<td>".$row->md_qual."</td>";
			$output 	.=	"<td>".$row->md_msg."</td>";
			$output 	.=	"<td><img src='".$this->config->item('image_url')."contact/small/".$row->md_foto."'></td>";
			$output 	.=	"<td>".$row->contact_type."</td>";
			$output 	.=	"<td><a href=javascript:confirm_del('".$this->config->item('admin_url')."contact/active/".$row->contact_id."/".$status."')>".$image."</a></td>";
			$output 	.=	"<td><a href='".$this->config->item('admin_url')."contact/edit/".$row->contact_id."'><i class='fa fa-pencil'></i></a></td>";
			$output		.=	"<td><a href=javascript:confirm_del('".$this->config->item('admin_url')."contact/delete/".$row->contact_id."')><i class='fa fa-trash'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}

  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('announcementmaster');
		$this->db->where('contact_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}
}