<?php

class Projectsmodel extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('*');
		$this->db->from('activitymaster');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{
			$sl_no++;
			if($row->portfolio_status == '1')
			{
				$image = "<i class='fa fa-check'></i>";
				$status = 0;
			}
			else
			{
				$image = "<i class='fa fa-ban'></i>";
				$status = 1;
			}
			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->portfolio_project."</td>";
			$output 	.=	"<td>".$row->portfolio_description."</td>";
			$output 	.=	"<td><img src='".$this->config->item('image_url')."projects/small/".$row->portfolio_image."'></td>";
			$output 	.=	"<td>".$row->portfolio_type."</td>";
			$output 	.=	"<td><a href=javascript:confirm_del('".$this->config->item('admin_url')."projects/active/".$row->portfolio_id."/".$status."')>".$image."</a></td>";
			$output 	.=	"<td><a href='".$this->config->item('admin_url')."projects/edit/".$row->portfolio_id."'><i class='fa fa-pencil'></i></a></td>";
			$output		.=	"<td><a href=javascript:confirm_del('".$this->config->item('admin_url')."projects/delete/".$row->portfolio_id."')><i class='fa fa-trash'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}

  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('activitymaster');
		$this->db->where('portfolio_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}
}