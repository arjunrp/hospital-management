<?php

class Profilemodel extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('*');
		$this->db->from('usermaster');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{
			$sl_no++;
			if($row->USM_STATUS == '1')
			{
				$image = "<i class='fa fa-check'></i>";
				$status = 0;
			}
			else
			{
				$image = "<i class='fa fa-ban'></i>";
				$status = 1;
			}
			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->USM_ID."</td>";
			$output 	.=	"<td>".$row->USM_FIRST_NAME."</td>";
			$output 	.=	"<td><img src='".$this->config->item('image_url')."profile/small/".$row->USM_AVATAR."'></td>";
			$output 	.=	"<td>".$row->USM_LAST_NAME."</td>";
			$output 	.=	"<td><a href=javascript:confirm_del('".$this->config->item('admin_url')."profile/active/".$row->USM_ID."/".$status."')>".$image."</a></td>";
			$output 	.=	"<td><a href='".$this->config->item('admin_url')."profile/edit/".$row->service_id."'><i class='fa fa-pencil'></i></a></td>";
			$output		.=	"<td><a href=javascript:confirm_del('".$this->config->item('admin_url')."profile/delete/".$row->USM_ID."')><i class='fa fa-trash'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}

  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('usermaster');
		$this->db->where('USM_ID',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}
}