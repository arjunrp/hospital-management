<?php

class Servicesmodel extends CI_Model 
{
	function list_all()
	{
		$output 		=	"";
		$sl_no 			=	0;

		$this->db->select('*');
		$this->db->from('bannermaster');
		$query 			=	$this->db->get();

		foreach($query->result() as $row)
		{
			$sl_no++;
			if($row->service_status == '1')
			{
				$image = "<i class='fa fa-check'></i>";
				$status = 0;
			}
			else
			{
				$image = "<i class='fa fa-ban'></i>";
				$status = 1;
			}
			$output 	.=	"<tr><td>".$sl_no."</td>";
			$output 	.=	"<td>".$row->service_heading."</td>";
			$output 	.=	"<td>".$row->service_description."</td>";
			$output 	.=	"<td><img src='".$this->config->item('image_url')."services/small/".$row->service_image."'></td>";
			$output 	.=	"<td>".$row->service_type."</td>";
			$output 	.=	"<td><a href=javascript:confirm_del('".$this->config->item('admin_url')."services/active/".$row->service_id."/".$status."')>".$image."</a></td>";
			$output 	.=	"<td><a href='".$this->config->item('admin_url')."services/edit/".$row->service_id."'><i class='fa fa-pencil'></i></a></td>";
			$output		.=	"<td><a href=javascript:confirm_del('".$this->config->item('admin_url')."services/delete/".$row->service_id."')><i class='fa fa-trash'></i></a></td>";
			$output		.=	"</tr>";
		}
		return $output;
	}

  
	function get_one_banner($id)
	{
		$this->db->select('*');
		$this->db->from('bannermaster');
		$this->db->where('service_id',$id);
		$query 			=	$this->db->get();
		$row 			=	$query->row();
		return $row;
	}
}