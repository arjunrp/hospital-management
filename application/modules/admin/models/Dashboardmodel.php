<?php
class Dashboardmodel extends CI_Model 
{
	 
	function get_banner_count()
	{
		$this->db->select("*");
		$this->db->from("bannermaster");
		$this->db->where("service_status",1);
		$query	=	$this->db->get();
		$cnt 	=	$query->num_rows();
		return $cnt;
	}

	function get_page_count()
	{
		$this->db->select("*");
		$this->db->from("activitymaster");
		$this->db->where("portfolio_status",1);
		$query	=	$this->db->get();
		$cnt 	=	$query->num_rows();
		return $cnt;
	}

 	function get_activity_count()
	{
		$this->db->select("*");
		$this->db->from("eventsmaster");
		$this->db->where("testimonial_status",1);
		$query	=	$this->db->get();
		$cnt 	=	$query->num_rows();
		return $cnt;
	}

 	function get_news_count()
	{
		$this->db->select("*");
		$this->db->from("newsmaster");
		$this->db->where("news_status",1);
		$query	=	$this->db->get();
		$cnt 	=	$query->num_rows();
		return $cnt;
	}

	// function get_events_count()
	// {
	// 	$this->db->select("*");
	// 	$this->db->from("EVENTSMASTER");
	// 	$this->db->where("EVT_STATUS",1);
	// 	$query	=	$this->db->get();
	// 	$cnt 	=	$query->num_rows();
	// 	return $cnt;
	// }

}