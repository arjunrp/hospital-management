<?php if( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Setcaptcha
{
	function __construct()
	{
		$this->ci 			=	& get_instance();

	}

	function get_captcha_data()
	{ 
		$this->ci->load->helper('captcha');
		$values = array(
                'word' => '',
                'word_length' => 5,
                'img_path' => './images/',
                'img_url' =>  base_url() .'images/',
                'font_path'  => base_url() . 'system/fonts/texb.ttf',
                'img_width' => '80',
                'img_height' => 40,
                'expiration' => 3600
               );

		$cap 							=	create_captcha($values); 
		$_SESSION['fcaptchaWord']     	=	$cap['word']; 
		return $cap;
	}
}