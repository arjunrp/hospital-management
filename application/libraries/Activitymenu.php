<?php if( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Activitymenu
{
	function __construct()
	{
		$this->ci 			=	& get_instance();
	}

	function activity_list()
	{
		$this->ci->db->select('*');
		$this->ci->db->from('ACTIVITYMASTER');
		$this->ci->db->order_by('ACT_ID');
		$this->ci->db->where('ACT_STATUS',1);
		$query 			=	$this->ci->db->get();
		$row 			=	$query->result();
		return $row;
	}
}