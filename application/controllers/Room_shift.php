<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Room_shift extends MX_Controller 
{
  public $tab_groups;
  public $form_image;

  function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('logged_in'))
    {
      redirect($this->config->item('admin_url'));
    }

    $this->load->model('Room_shift_model','Room_shift',TRUE);
    $this->load->model('Ipregister_model','Ipregister',TRUE);
    $this->load->model('Booking_model','Booking',TRUE);
    $this->load->model('Users_model','Users',TRUE);
    $this->template->set('title','IP Register');
    $this->base_uri       = $this->config->item('admin_url')."room_shift";
  }

  function index()
  {
    $data['page_head']      = "Room Occupancy List";
    $data['page_title']     = "Room Occupancy";
    $data['output']         = $this->Room_shift->list_all();
     
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('ip_list',$data);
      $this->load->view('templates/footer'); 
  }

  public function list_ajax()
  {
    echo $data['output']    = $this->Room_shift->list_all();
  }

  function add($id)
  {
    $data['page_title']           = "Room Shifting";
    $data['action']               = $this->base_uri.'/insert';
    $data['get_ips']              = $this->Room_shift->get_ip($id);
    $data['get_rooms']            = $this->Room_shift->get_rooms();
    $data['output']               = $this->Room_shift->get_patient_rooms($id);
    $data['bed_occup']            = $this->Room_shift->get_all_beds();


        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('room_shift_add',$data);
        $this->load->view('templates/footer');  
  }

  function insert()
  {

    $rs_ip                                =  $this->input->post('rs_ip');
    $room_shift_id                        =  $this->Room_shift->get_roomshift($rs_ip);

    $this->tab_groups['rs_ip']            =  $rs_ip;
    $this->tab_groups['rs_mrd']           =  $this->input->post('rs_mrd');
    $rs_rmno                              =  $this->input->post('rs_rmno');
    $this->tab_groups['rs_rmno']          =  $rs_rmno;
    $rs_bdno                              =  $this->input->post('rs_bdno');
    $this->tab_groups['rs_bdno']          =  $rs_bdno;
    $rs_date                              =  $this->input->post('rs_adm');
    $rs_time                              =  $this->input->post('rs_time');
    $rs_adm                               =  $rs_date ." ".$rs_time;
    $rs_adm                               =  date("Y-m-d H:i:s",strtotime($rs_adm));
    $this->tab_groups['rs_adm']           =  $rs_adm;
    $this->tab_groups['rs_dis']           =  "0000-00-00 00:00:00";

    $data['rs_dis']                       =  date("Y-m-d H:i:s",strtotime('-1 minutes',strtotime($rs_adm)));

    $this->db->insert('room_shift',$this->tab_groups);

    if($room_shift_id!=0)
    {

    $this->db->update('room_shift',$data,array('rs_id'=>$room_shift_id));

    $this->db->select('rs_bdno');
    $this->db->from('room_shift');
    $this->db->where('rs_id',$room_shift_id);
    $query  = $this->db->get();
    $bdno   = $query->row()->rs_bdno;
    $datasss['bd_avail']                 =  '1';

    $this->db->update('bed',$datasss,array('bd_id'=>$bdno));
    }
    $datass['bd_avail']                  =  '0';
    $this->db->update('bed',$datass,array('bd_id'=>$rs_bdno));

    $this->session->set_flashdata('Success','Room Shift Done');
    redirect('room_shift/add/'.$rs_ip);
  }

  function edit($id)
  {
    $data['page_title']     = "Edit Room Shifting";
    $data['action']         = $this->base_uri.'/update';
    $row                    = $this->Room_shift->get_one_banner($id);


    $data['rs_id']          = $id;
    $rs_ip                  = $row->rs_ip;
    $data['rs_mrd']         = $row->rs_mrd;
    $data['rs_ip']          = $rs_ip;
    $rs_rmno                = $row->rs_rmno;
    $data['rs_room']        = $this->Room_shift->get_rm_no($rs_rmno);
    $data['rs_rmno']        = $row->rs_rmno;
    $data['rs_bdno']        = $row->rs_bdno;
    $data['rs_beds']        = $this->Room_shift->get_bed_no($rs_rmno);
    $data['rs_adm']         = $row->rs_adm;
    $data['rs_dis']         = $row->rs_dis;

    $data['get_ips']        = $this->Room_shift->get_ip($rs_ip);
    $data['get_rooms']      = $this->Room_shift->get_rooms();
     
        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('room_shift_edit',$data);
        $this->load->view('templates/footer');  
  }

  function update()
  {

    $rs_id                                =  $this->input->post('rs_id');
    $rs_ip                                =  $this->input->post('rs_ip');
    $room_shift_id                        =  $this->Room_shift->get_roomshift($rs_ip);

    $this->tab_groups['rs_ip']            =  $rs_ip;
    $this->tab_groups['rs_mrd']           =  $this->input->post('rs_mrd');
    $this->tab_groups['rs_rmno']          =  $this->input->post('rs_rmno');
    $rs_bdno                              =  $this->input->post('rs_bdno');
    $this->tab_groups['rs_bdno']          =  $rs_bdno;

    $rs_date                              =  $this->input->post('rs_adm');
    $rs_time                              =  $this->input->post('rs_time');
    $rs_adm                               =  $rs_date ." ".$rs_time;
    $rs_adm                               =  date("Y-m-d H:i:s",strtotime($rs_adm));
    $this->tab_groups['rs_adm']           =  $rs_adm;

    $rs_ddate                             =  $this->input->post('rs_dis');
    $rs_dtime                             =  $this->input->post('rs_dtime');
    $rs_dis                               =  $rs_ddate ." ".$rs_dtime;
    $rs_dis                               =  date("Y-m-d H:i:s",strtotime($rs_dis));
    $this->tab_groups['rs_dis']           =  $rs_dis;

    // $data['rs_dis']                       =  date("Y-m-d",strtotime('-1 day',strtotime($rs_adm)));
    // $data['rs_dis']                       =  $rs_adm;
    $this->db->update('room_shift', $this->tab_groups,array('rs_id'=>$rs_id));
    // $this->db->update('room_shift',$data,array('rs_id'=>$room_shift_id));

    $datass['bd_avail']                   =  '0';
    $this->db->update('bed',$datass,array('bd_id'=>$rs_bdno));

    $this->session->set_flashdata('Success','Room Shift Updated');
    redirect('room_shift/add/'.$rs_ip);
  }

  function delete($id)
  {

    if($_REQUEST['empid']) {
      $id       = $_REQUEST['empid']; 

      $row      = $this->Room_shift->get_one_banner($id);

      $this->db->select('rs_bdno');
      $this->db->from('room_shift');
      $this->db->where('rs_id',$id);
      $query  = $this->db->get();
      $rs_ip  = $query->row()->rs_ip;
      $bdno   = $query->row()->rs_bdno;

      $data['bd_avail']                 =  '1';
      $this->db->update('bed',$data,array('bd_id'=>$bdno));
      $this->db->delete('room_shift',array('rs_id'=>$id));

      $this->db->select('rs_id,rs_bdno');
      $this->db->from('room_shift');
      $this->db->where('rs_ip',$rs_ip);
      $this->db->order_by('rs_id','desc');
      $this->db->limit(1);
      $query1  = $this->db->get();
      $rs_id  = $query1->row()->rs_id;
      $bdno1  = $query1->row()->rs_bdno;

      $datas['rs_dis']                 =  '0000-00-00 00:00:00';
      $this->db->update('room_shift',$datas,array('rs_id'=>$rs_id));

      $datass['bd_avail']              =  '0';
      $this->db->update('bed',$datass,array('bd_id'=>$bdno1));


   }
        echo "Record Deleted";
  }


  function get_ipno()
  {
    $ip_dept   = $this->input->post('ip_dept');

      $this->db->select('ip_ipno');
      $this->db->from('ip');
      $this->db->where('ip_department',$ip_dept);
      $query      = $this->db->get();
      if($query->num_rows()==0)
      {
        $this->db->select('dp_short');
        $this->db->from('department');
        $this->db->where('dp_id',$ip_dept);
        $result      = $this->db->get();
        $data = $result->row()->dp_short."/0001";
      }
      else
      {
        $data   = $query->row()->ip_ipno;
        $data++;
      }
    print_r($data);
  }

  function getBeds($c_id){

    $this->db->select('*');
    $this->db->from('bed');
    $this->db->where('bd_rmid',$c_id);
    $this->db->where('bd_avail','1');
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
  }

}