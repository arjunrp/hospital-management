<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testscontent extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Testscontent_model','Testscontent',TRUE);
		$this->template->set('title','testscontent');
		$this->base_uri 			=	$this->config->item('admin_url')."testscontent";
	}

	function index()
	{
		$data['page_title']			=	"Tests Content List";
		$data['output']				=	$this->Testscontent->list_all();
		// $this->template->load('template','customer',$data);
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('test_content_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Testscontent->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Test Content";
		$data['action']				=	$this->base_uri.'/insert';
		$data['tc_id']				=	"";
		$data['tc_content']			=	"";
		$data['tc_unit']			=	"";
        $data['tc_men']			    =	"";
        $data['tc_women']			=	"";
        $data['tc_child']			=	"";
		$data['catid'] 				=	"";
		$data['categories'] 		=	$this->Testscontent->get_category();
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('test_content_add',$data);
      	$this->load->view('templates/footer');  
	}

function View($id)
	{
		$data['page_title']			=	"Edit Testscontent";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Testscontent->get_one_banner($id);
		$data['tc_id']				=	$row->tc_id;
		$data['tc_content']			=	$row->tc_content;
		$data['tc_unit'] 			=	$row->tc_unit;
		$data['tc_men'] 			=	$row->tc_men;
		$data['tc_women'] 			=	$row->tc_women;
		$data['tc_child'] 			=	$row->tc_child;
		$data['catid'] 				=	$row->categoryid;
		$data['categories'] 		=	$this->Testscontent->get_category();
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('test_content_view',$data);
      	$this->load->view('templates/footer');  
	}
	
	function insert()
	{
		$this->tab_groups['tc_content']	=	$this->input->post('tc_content');
		$this->tab_groups['tc_unit']	=	$this->input->post('tc_unit');
		$this->tab_groups['tc_men']		=	$this->input->post('tc_men');
		$this->tab_groups['tc_women']	=	$this->input->post('tc_women');
		$this->tab_groups['tc_child']	=	$this->input->post('tc_child');
		$this->tab_groups['tc_tnameid']	=	$this->input->post('tc_tnameid');
		$this->db->insert('test_content',$this->tab_groups);
		$id 							=	$this->db->insert_id();
		$this->session->set_flashdata('Success','Testscontent Created');
		redirect($this->base_uri."/add");
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Testsubcategory";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Testscontent->get_one_banner($id);
		$data['tc_id']				=	$row->tc_id;
		$data['tc_content']			=	$row->tc_content;
		$data['tc_unit'] 			=	$row->tc_unit;
	    $data['tc_women'] 			=	$row->tc_women;
		$data['tc_men'] 			=	$row->tc_men;
		$data['tc_child'] 			=	$row->tc_child;
		$data['catid'] 				=	$row->tc_tnameid;
		$data['categories'] 		=	$this->Testscontent->get_category();
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('test_content_add',$data);
      	$this->load->view('templates/footer');  
	}

	 

	function update()
	{
		$id 							=	$this->input->post('tc_id');
		$this->tab_groups['tc_content']	=	$this->input->post('tc_content');
	    $this->tab_groups['tc_unit'] 	=	$this->input->post('tc_unit');
	    $this->tab_groups['tc_women'] 	=	$this->input->post('tc_women');
	    $this->tab_groups['tc_men'] 	=	$this->input->post('tc_men');
	    $this->tab_groups['tc_child'] 	=	$this->input->post('tc_child');
		$this->tab_groups['tc_tnameid'] =	$this->input->post('tc_tnameid');
		$this->db->update('test_content', $this->tab_groups,array('tc_id'=>$id));
		$this->session->set_flashdata('Success','Expense Updated');
		redirect($this->base_uri);
	}


	function delete($id)
	{


         if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$this->db->delete('test_content',array('tc_id'=>$id));
			$this->session->set_flashdata('Error','User Deleted');
		}
				echo "Record Deleted";
	}
}