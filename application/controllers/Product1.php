
<?php

class Product1 extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->load->database();

    $this->load->helper(array('form', 'url'));
    $this->load->model('Product_model1');
    $this->load->model('Subcategory_model');
  }

  public function index() {
           if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
            
    if($this->input->post('submit')=='Submit'){
      $this->Product_model->insertProduct();
      $this->session->set_flashdata('Success','Product Created');
      redirect('product/getProduct');
    }
    else{
      $data['categories']=$this->Subcategory_model->getCategory();
      $data['types']=$this->Product_model->getType();
      $data['groups']=$this->Product_model->getGroup();

      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('product_add',$data);
      $this->load->view('templates/footer');  
    }
  }
  }
  public function getSubcategory($c_id){
    if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    $query=$this->db->get_where('subcategory',array('categoryid'=>$c_id));
    $data=$query->result_array();
    echo  json_encode($data);
  }}
  public function getProduct(){
     if ($this->session->userdata('username') == '') {
            redirect('login/index/');
        }   else {
    $data['products']=$this->Product_model->getProduct();
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('product_list',$data);
    $this->load->view('templates/footer'); 
  }
  }
  public function updateProduct($id){
    if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    if(!empty($id)){
      $data['products']=$this->Product_model->updateProduct($id);
      $data['categories']=$this->Subcategory_model->getCategory();
      $data['types']=$this->Product_model->getType();
      $data['groups']=$this->Product_model->getGroup();
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('product_update',$data);
      $this->load->view('templates/footer');  
    }
    if($this->input->post('submit')=='Submit'){
      $this->Product_model->updateProduct();
      $this->session->set_flashdata('Success','Product Updated');
      redirect('product/getProduct');
    }
    }

  }
  public function deleteProduct($id){
    if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    $this->Product_model->deleteProduct($id);
    $this->session->set_flashdata('Success','Product Deleted');
    redirect('product/getProduct');
  }}



}

?>
