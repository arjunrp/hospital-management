<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Booking_token extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Booking_token_model','Booking_token',TRUE);
		$this->template->set('title','Booking Token');
		$this->base_uri 			=	$this->config->item('admin_url')."booking_token";
	}

	function index()
	{
		$data['page_title']			=	"Booking Token List";
		$data['output']				=	$this->Booking_token->list_all();
		 
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('booking_token_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Booking_token->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Booking Token";
		$data['action']				=	$this->base_uri.'/insert';
		$data['dc_docid']			=	"";
		$data['doctors']			=	$this->Booking_token->get_doctor();
		$data['dc_morntok']			=	"";
		$data['dc_evngtok']			=	"";
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('booking_token_add',$data);
      	$this->load->view('templates/footer');  
	}

	function insert()
	{

		 
		$this->tab_groups['dc_docid']	     =	$this->input->post('dc_docid');
		$this->tab_groups['dc_morntok']	     =	$this->input->post('dc_morntok');
		$this->tab_groups['dc_evngtok']	     =	$this->input->post('dc_evngtok');
		 
		$this->db->insert('doctor',$this->tab_groups);

		
		$this->session->set_flashdata('Success','Doctor Booking Created');
		redirect($this->base_uri."/add");
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Doctor Booking";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Booking_token->get_one_banner($id);
		$data['doctors']			=	$this->Booking_token->get_doctor();
		$data['dc_docid']			=	$row->dc_docid;
		$data['dc_morntok']			=	$row->dc_morntok;
		$data['dc_evngtok']			=	$row->dc_evngtok;
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('booking_token_add',$data);
      	$this->load->view('templates/footer');  
	}

	function update()
	{
		$id 							=	$this->input->post('dc_docid');
		$this->tab_groups['dc_morntok']	=	$this->input->post('dc_morntok');
		$this->tab_groups['dc_evngtok']	=	$this->input->post('dc_evngtok');
		 
		$this->db->update('doctor', $this->tab_groups,array('dc_docid'=>$id));
		$this->session->set_flashdata('Success','Doctor Booking Updated');
		redirect($this->base_uri);
	}
	function delete($id)
	{

		if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$row 			=	$this->Booking_token->get_one_banner($id);
			$this->db->delete('doctor',array('dc_docid'=>$id));

	 }
				echo "Record Deleted";
	}
}