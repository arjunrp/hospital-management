

<?php

class Customer extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->load->helper(array('form', 'url'));
    $this->load->model('Customer_model');

  }

  public function index() {
    if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    if($this->input->post('submit')=='Submit'){
      $this->Customer_model->insert_customer();
      $this->session->set_flashdata('Success','Customer Created');
      redirect('Customer/getCustomer');
    }
    else{
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('customer_add');
      $this->load->view('templates/footer');  
    }

  }}
  public  function getCustomer(){
    if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    $data['customers']=$this->Customer_model->getCustomer();
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('customer_list',$data);
    $this->load->view('templates/footer');  
  }}
  public function updateCustomer($id){
    if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    if(!empty($id)){
      $data['customer']=$this->Customer_model->updateCustomer($id);
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('customer_update',$data);
      $this->load->view('templates/footer'); 
    }
    if($this->input->post('submit')=='Submit')
    {
      $this->Customer_model->updateCustomer();
      $this->session->set_flashdata('Success','Customer Updated');
      redirect('Customer/getCustomer');
    }
  }}
  public function deleteCustomer($id){
    if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    $this->Customer_model->deleteCustomer($id);
    $this->session->set_flashdata('Success','Customer Deleted');
    redirect('Customer/getCustomer');
  }
  }








}

?>
