<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Discharge extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Discharge_model','Discharge',TRUE);
		$this->load->model('Investigation_model','Investigation',TRUE);
    $this->load->model('Sale_model','Sales',TRUE);
    $this->load->model('Purchase_model','Purchase',TRUE);
		$this->template->set('title','Discharge');
		$this->base_uri 			=	$this->config->item('admin_url')."discharge";
	}

	function index()
	{
		$data['page_title']			=	"List";
		$data['output']				=	$this->Discharge->list_all();
		 
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('discharge_list',$data);
    	$this->load->view('templates/footer'); 
	}

    function pending_list()
  {

    $data['page_title']     = "Discharge Pending List";
    $data['output']         = $this->Discharge->list_pending();

    // $data['sale_detail']=$this->Sales->getAbout();
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('sale_pending',$data);
    $this->load->view('templates/footer'); 
  }
  
  function discharge_Pending($sid){
    $data['ve_status'] = "cc";
    $this->db->update('voucher_entry',$data, array('ve_id' => $sid));
    redirect($this->base_uri."/add/");
  }

	public function list_ajax()
	{
		echo $data['output']		=	$this->Bed->list_all();
	}

	function add()
	{
		$data['page_title']			  =	"New Discharge";
		$data['action']				    =	$this->base_uri.'/insert';
		$data['bd_id']				    =	"";
		$data['patients']    	    = $this->Investigation->get_patientsip();
		$data['bill_no']			    =	$this->Discharge->get_discharge_no();
		$data['additional_bills']	=	$this->Discharge->get_additional_bill();
    $data['admin_pswd']       = $this->Discharge->get_admin_pswd();
    // print_r($data['admin_pswd']);
		// print_r($data['additional_bills']);
		 
		    $this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('discharge_add',$data);
      	$this->load->view('templates/footer');  
	}

  function d_return($ipno)
  {
    $data['page_title']       = "Discharge Return";
    $data['discharges']       = $this->Discharge->get_discharge($ipno);
    $data['rooms']            = $this->Discharge->get_room($ipno);
    $data['voucher_bills']    = $this->Discharge->get_voucher_bills($ipno);
    $data['scanning_bills']   = $this->Discharge->get_scanning_bills($ipno);
    $data['xray_bills']       = $this->Discharge->get_xray_bills($ipno);
    $data['pharmacy_bills']   = $this->Discharge->get_pharmacy_bills($ipno);
    $data['pharmacy_returns'] = $this->Discharge->get_pharmacy_returns($ipno);
    $data['advances']         = $this->Discharge->get_advances($ipno);
    $data['additionals']      = $this->Discharge->get_additional_items($ipno);
    $data['bill_no']          = $this->Discharge->get_discharge_rno();
    // print_r($data['admin_pswd']);
    // print_r($data['additional_bills']);
     
        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('discharge_return',$data);
        $this->load->view('templates/footer');  
  }

  function view($ipno)
  {
    $data['page_title']       = "Patient Discharged";

    $data['discharges']       = $this->Discharge->get_discharge($ipno);
    $data['rooms']            = $this->Discharge->get_room($ipno);
    $data['voucher_bills']    = $this->Discharge->get_voucher_bills($ipno);
    $data['scanning_bills']   = $this->Discharge->get_scanning_bills($ipno);
    $data['xray_bills']       = $this->Discharge->get_xray_bills($ipno);
    $data['pharmacy_bills']   = $this->Discharge->get_pharmacy_bills($ipno);
    $data['pharmacy_returns'] = $this->Discharge->get_pharmacy_returns($ipno);
    $data['advances']         = $this->Discharge->get_advances($ipno);
    $data['additionals']      = $this->Discharge->get_additional_items($ipno);

    // print_r($data['rooms']);
    // print_r($data['voucher_bills']);
    // print_r($data['scanning_bills']);
    // print_r($data['xray_bills']);
    // print_r($data['pharmacy_bills']);
    // print_r($data['pharmacy_returns']);
    // print_r($data['advances']);
    // print_r($data['additionals']);
     
        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('discharge_view',$data);
        $this->load->view('templates/footer');  
  }

  function getPrint(){
    $ipno                    = $_GET['Printid'];
    $data['company']          = $this->Purchase->getCompany();
    
    $data['discharges']       = $this->Discharge->get_discharge($ipno);
    // print_r($data['discharges']);
    $data['rooms']            = $this->Discharge->get_room($ipno);
    $data['voucher_bills']    = $this->Discharge->get_voucher_bills($ipno);
    $data['scanning_bills']   = $this->Discharge->get_scanning_bills($ipno);
    $data['xray_bills']       = $this->Discharge->get_xray_bills($ipno);
    $data['pharmacy_bills']   = $this->Discharge->get_pharmacy_bills($ipno);
    $data['pharmacy_returns'] = $this->Discharge->get_pharmacy_returns($ipno);
    $data['advances']         = $this->Discharge->get_advances($ipno);
    $data['additionals']       = $this->Discharge->get_additional_items($ipno);
    // print_r($data['voucher_bills']);
      
    $this->load->view('discharge_print',$data);
  }


	function get_room($c_id){

    $this->db->select('room_shift.*,room.*');
    $this->db->from('ip'); 
    $this->db->join('room_shift','room_shift.rs_ip = ip.ip_ipno', 'inner'); 
    $this->db->join('room','room.rm_id = room_shift.rs_rmno', 'inner'); 
    $this->db->where('ip.ip_ipno',$c_id);
    $this->db->group_by('room_shift.rs_id');
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
  }
  function get_voucher_bills($c_id){

    $this->db->select('*');
    $this->db->from('voucher_entry'); 
    $this->db->where('ve_customer',$c_id);
    $this->db->where('ve_type',"lbi");
    $this->db->where('ve_pstaus',"NP");
    $this->db->where('ve_status',"cr");
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
  }

  function get_scanning_bills($c_id){

    $this->db->select('*');
    $this->db->from('voucher_entry'); 
    $this->db->where('ve_customer',$c_id);
    $this->db->where('ve_type',"scani");
    $this->db->where('ve_pstaus',"NP");
    $this->db->where('ve_status',"cr");
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
  }

  function get_xray_bills($c_id){

    $this->db->select('*');
    $this->db->from('voucher_entry'); 
    $this->db->where('ve_customer',$c_id);
    $this->db->where('ve_type',"xrayi");
    $this->db->where('ve_pstaus',"NP");
    $this->db->where('ve_status',"cr");
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
  }

  function get_pharmacy_bills($c_id){

    $this->db->select('*');
    $this->db->from('voucher_entry'); 
    $this->db->where('ve_customer',$c_id);
    $this->db->where('ve_type',"si");
    $this->db->where('ve_pstaus',"NP");
    $this->db->where('ve_status',"cr");
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
  }

  function get_pharmacy_returns($c_id){

    $this->db->select('*');
    $this->db->from('voucher_entry'); 
    $this->db->where('ve_customer',$c_id);
    $this->db->where('ve_type',"sr");
    $this->db->where('ve_status',"cr");
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
  }

   function get_advances($c_id){

    $this->db->select('*');
    $this->db->from('voucher_entry'); 
    $this->db->where('ve_customer',$c_id);
    $this->db->where('ve_type',"ad");
    $this->db->where('ve_status',"acc");
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
  }

  function dischargeAdd(){

    $this->tab_groups['ve_vno']           = $this->input->post('ve_vno');
    $discharge_date                       = $this->input->post('ve_date');
    $discharge_time                       = $this->input->post('dis_time');
    $d_date                               = $discharge_date." ".$discharge_time;
    $d_date                               = date("Y-m-d H:i:s",strtotime($d_date));
    $this->tab_groups['ve_date']          = $d_date;
    $this->tab_groups['ve_supplier']      = "16";
    $ip_ipno      						            = $this->input->post('ve_customer');
    $this->tab_groups['ve_customer']      = $ip_ipno;
    $this->tab_groups['ve_mrd']        	  = $this->input->post('ve_mrd');
    $this->tab_groups['ve_amount']        = $this->input->post('apayable');
    $this->tab_groups['ve_discount']      = $this->input->post('discountp');
    $this->tab_groups['ve_discounta']     = $this->input->post('discounta');
    $this->tab_groups['ve_gtotal']        = $this->input->post('sum2');
    $this->tab_groups['ve_apayable']      = $this->input->post('sum');
    $amount_paid                          = $this->input->post('sum');
    $this->tab_groups['ve_apaid']         = "0";
    $this->tab_groups['ve_balance']       = $amount_paid;
    $this->tab_groups['ve_round']         = $this->input->post('roundoff');

    $this->tab_groups['ve_pstaus']        = "NP";
    $this->tab_groups['ve_user']          = $this->input->post('user_type');
    $this->tab_groups['ve_type']          = "dis";

    $pending                              = $this->input->post('pending');
    if($pending=="False")
    {
        $this->tab_groups['ve_status']        = "cc";
    }

    else
    {
      $this->tab_groups['ve_status']        = "pending";
    }

    // $this->tab_groups['ve_status']        = "cc";

    $this->db->insert('voucher_entry',$this->tab_groups);
    $dischargeid                          = $this->db->insert_id();
    // $this->Sales->set_sale_balance($amount_paid,$discharge_date);

    $data['ip_discharge'] 			= date("Y-m-d");
    $data['ip_dis'] 					  = 0;
    $data['ip_dtime']           = date("H:i:s");
    $this->db->update('ip',$data,array('ip_ipno'=>$ip_ipno));

     $today = date("Y-m-d H:i:s");
     $updateDis = array('rs_dis' => $today);

     $this->db->order_by('rs_id','desc');
     $this->db->limit(1);
     $this->db->where('rs_ip',$ip_ipno);
     $this->db->update('room_shift', $updateDis);

     foreach($this->input->post('rs_id') as $key => $val)
    {
      $inputss = array();
      $inputss['rs_days']    = $this->input->post('rs_days')[$key];
      $inputss['rs_rt']      = $this->input->post('rs_rt')[$key];
      $inputss['rs_nc']      = $this->input->post('rs_nc')[$key];
      $inputss['rs_hc']      = $this->input->post('rs_hc')[$key];
      $this->db->update('room_shift',$inputss,array('rs_id' => $val));
    } 

     $this->db->select('rs_bdno');
     $this->db->from('room_shift');
     $this->db->where('rs_ip',$ip_ipno);
     $this->db->order_by('rs_id','desc');
     $this->db->limit(1);
     $query   = $this->db->get();
     $rs_bdno = $query->row()->rs_bdno;

     $updateBed = array('bd_avail' =>'1');
     $this->db->where('bd_id',$rs_bdno);
     $this->db->update('bed', $updateBed);

     if($this->input->post('ved_item'))
     {
     foreach($this->input->post('ved_item') as $k => $v)
  {

    $inputs = array();
    $inputs['ved_veid']       = $dischargeid;
    $inputs['ved_date']       = $discharge_date;
    $inputs['ved_itemid']     = $this->input->post('ved_itemid')[$k];
    $inputs['ved_item']       = $v;
    $inputs['ved_qty']        = $this->input->post('ved_qty')[$k];
    $inputs['ved_price']      = $this->input->post('ved_price')[$k];
    $inputs['ved_gtotal']     = $this->input->post('ved_total')[$k];
    $this->db->insert('voucher_entry_detail',$inputs);
  } 
}
     print_r($ip_ipno);
  }

function dischargeReturn(){

    $this->tab_groups['ve_bill_no']       = $this->input->post('ve_bill_no');
    $this->tab_groups['ve_vno']           = $this->input->post('ve_vno');
    $discharge_date                       = $this->input->post('ve_date');
    $discharge_date                       = date("Y-m-d H:i:s",strtotime($discharge_date));
    $this->tab_groups['ve_date']          = $discharge_date;
    $this->tab_groups['ve_supplier']      = "16";
    $ip_ipno                              = $this->input->post('ve_customer');
    $this->tab_groups['ve_customer']      = $ip_ipno;
    $this->tab_groups['ve_mrd']           = $this->input->post('ve_mrd');
    $this->tab_groups['ve_amount']        = $this->input->post('apayable');
    $this->tab_groups['ve_discount']      = $this->input->post('discountp');
    $this->tab_groups['ve_discounta']     = $this->input->post('discounta');
    $this->tab_groups['ve_gtotal']        = 0-$this->input->post('sum2');
    $this->tab_groups['ve_apayable']      = 0-$this->input->post('sum');
    $amount_paid                          = 0-$this->input->post('sum');
    $this->tab_groups['ve_apaid']         = "0";
    $this->tab_groups['ve_balance']       = $amount_paid;
    $this->tab_groups['ve_round']         = $this->input->post('roundoff');

    $this->tab_groups['ve_pstaus']        = "NP";
    $this->tab_groups['ve_user']          = $this->input->post('user_type');
    $this->tab_groups['ve_type']          = "dsr";

    $this->tab_groups['ve_status']        = "cc";

    $this->db->insert('voucher_entry',$this->tab_groups);
    $dischargeid                          = $this->db->insert_id();
    // $this->Sales->set_sale_balance($amount_paid,$discharge_date);

    $data['ip_discharge']       = date("Y-m-d");
    $data['ip_dis']             = 1;
    $data['ip_dtime']           = date("H:i:s");
    $this->db->update('ip',$data,array('ip_ipno'=>$ip_ipno));

     $this->db->select('rs_bdno');
     $this->db->from('room_shift');
     $this->db->where('rs_ip',$ip_ipno);
     $this->db->order_by('rs_id','desc');
     $this->db->limit(1);
     $query   = $this->db->get();
     $rs_bdno = $query->row()->rs_bdno;

     $updateBed = array('bd_avail' =>'0');
     $this->db->where('bd_id',$rs_bdno);
     $this->db->update('bed', $updateBed);

     if($this->input->post('ved_item'))
     {
     foreach($this->input->post('ved_item') as $k => $v)
  {

    $inputs = array();
    $inputs['ved_veid']       = $dischargeid;
    $inputs['ved_date']       = $discharge_date;
    $inputs['ved_itemid']     = $this->input->post('ved_itemid')[$k];
    $inputs['ved_item']       = $v;
    $inputs['ved_qty']        = $this->input->post('ved_qty')[$k];
    $inputs['ved_price']      = $this->input->post('ved_price')[$k];
    $inputs['ved_gtotal']     = $this->input->post('ved_total')[$k];
    $this->db->insert('voucher_entry_detail',$inputs);
  } 
}
     print_r($ip_ipno);
  }

}