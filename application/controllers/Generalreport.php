<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Generalreport extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Generalmodel','General',TRUE);
		$this->load->model('Product_model','Product',TRUE);
		$this->load->model('Subcategory_model','Subcategory',TRUE);
		$this->load->model('Inventory_model','Inventory',TRUE);
		$this->load->model('Purchase_model','Purchase',TRUE);
		$this->template->set('title','Account Report');
		$this->base_uri 			=	$this->config->item('admin_url')."generalreport";
	}





function Report()
	{
		$data['page_title']			=	"Dashboard";
		 
		// $this->template->load('template','customer',$data);
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('ac_report',$data);
    	$this->load->view('templates/footer'); 
	}

	function index()
	{
		$data['page_title']			=	"Medicne Category & GST Report";
		$data['action']				=	$this->base_uri.'/account_filter';
		$data['action1']			=	"";
		$data['r_type']				=	"5";
		$data['fdate']				=	"";
		$data['tdate']				=	"";
		$data['p_type']				=	"";
		$data['output']				=	"";
		$data['output1']			=	"";
		$data['cgsts1']				=	"";
		$data['sgsts1']				=	"";
		$data['catg1']				=	"";
		$data['cgsts']				=	$this->General->getCgst();
		$data['sgsts']				=	$this->General->getSgst();
		$data['catgs']				=	$this->General->getCategory();
		// $data['stocks']				=	"";
		
		
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('general_report',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Customer->list_all();
	}

	function account_filter()
	{
		$data['page_title']			=	"Medicne Category & GST Report";
		$data['action']				=	$this->base_uri.'/account_filter';
		$data['action1']			=	$this->base_uri.'/account_summary_print';
		$fdate						=	$this->input->post('fdate');
		$tdate						=	$this->input->post('tdate');
		$p_type						=	$this->input->post('p_type');
		$r_type						=	$this->input->post('r_type');

		$p_cgst						=	$this->input->post('p_cgst');
		$p_sgst						=	$this->input->post('p_sgst');
		$p_catg						=	$this->input->post('p_catg');
		
		$data['r_type']				=	$r_type;
		$data['fdate']				=	$fdate;
		$data['tdate']				=	$tdate;
		$data['p_type']				=	$p_type;
		$data['cgsts1']				=	$p_cgst;
		$data['sgsts1']				=	$p_sgst;	
		$data['catg1']				=	$p_catg;
		// print_r($p_type); echo "<br>"; print_r($p_cgst); echo "<br>"; print_r($p_sgst); echo "<br>"; print_r($p_catg); 
		// exit();
		$data['output']				=	$this->General->get_account($fdate,$tdate,$r_type,$p_type,$p_cgst,$p_sgst,$p_catg);

		$data['output1']			=	$this->General->get_account_main($fdate,$tdate,$r_type,$p_type,$p_cgst,$p_sgst,$p_catg);
		// print_r($data['output1']);
		$data['cgsts']				=	$this->General->getCgst();
		$data['sgsts']				=	$this->General->getSgst();
		$data['catgs']				=	$this->General->getCategory();
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('general_report',$data);
      	$this->load->view('templates/footer');  
	}

function account_summary_print()
	{
		$fdate						=	$this->input->post('fdate1');
		$tdate						=	$this->input->post('tdate1');
		$p_type						=	$this->input->post('p_type1');
		$r_type						=	$this->input->post('r_type1');
		
		$p_cgst						=	$this->input->post('p_cgst1');
		$p_sgst						=	$this->input->post('p_sgst1');
		$p_catg						=	$this->input->post('p_catg1');

		$data['r_type']				=	$r_type;
		$data['fdate']				=	$fdate;
		$data['tdate']				=	$tdate;
		$data['p_type']				=	$p_type;
		$data['cgsts1']				=	$p_cgst;
		$data['sgsts1']				=	$p_sgst;	
		$data['catg1']				=	$p_catg;

		$data['output']				=	$this->General->get_account($fdate,$tdate,$r_type,$p_type,$p_cgst,$p_sgst,$p_catg);

		$data['output1']			=	$this->General->get_account_main($fdate,$tdate,$r_type,$p_type,$p_cgst,$p_sgst,$p_catg);
		// print_r($data['output1']);
		$data['cgsts']				=	$this->General->getCgst();
		$data['sgsts']				=	$this->General->getSgst();
		$data['catgs']				=	$this->General->getCategory();
		$data['company']  			=   $this->Purchase->getCompany();

		         
      	$this->load->view('general_report_print',$data);
      	 
	}



	function acdetail()
	{

		$data['page_title']			=	"Account Detail Report";
		$data['action']				=	$this->base_uri.'/account_filter_detail';
		$data['action1']			=	"";
		$data['r_type']				=	"5";
		$data['fdate']				=	"";
		$data['tdate']				=	"";
		$data['product']			=	"";
		$data['products']			=	"";
		$data['output']				=	"";
		$data['output1']			=	"";
		$data['departments']		=	$this->Account->getDepartment();
		$data['brands']		    	=	$this->Product->getBrand();

		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('ac_detailreport',$data);
    	$this->load->view('templates/footer'); 
	}

	function account_filter_detail()
	{
		$data['page_title']			=	"Account Detail Report";
		$data['action']				=	$this->base_uri.'/account_filter_detail';
		$data['action1']			=	$this->base_uri.'/account_detail_print';
		$fdate						=	$this->input->post('fdate');
		$tdate						=	$this->input->post('tdate');
		$product					=	$this->input->post('product');
		$r_type						=	$this->input->post('r_type');
		$data['r_type']				=	$r_type;
		$data['fdate']				=	$fdate;
		$data['tdate']				=	$tdate;
		$data['product']			=	$product;
		if($product==0)
		{
			$data['products']		= "All Products";
		}
		else
		{
		$data['products']			=	$this->Account->list_small($product);
		}
		

		$data['output']				=	$this->Account->get_account_detail($fdate,$tdate,$product,$r_type);
		$data['output1']			=	$this->Account->get_account_main($fdate,$tdate,$product,$r_type);
		$data['departments']		=	$this->Account->getDepartment();
		$data['brands']		    	=	$this->Product->getBrand();

		// print_r($data['output']);

		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('ac_detailreport',$data);
      	$this->load->view('templates/footer');    
	}

	function account_detail_print()
	{
		$data['page_title']			=	"Account Detail Report";
		$fdate						=	$this->input->post('fdate1');
		$tdate						=	$this->input->post('tdate1');
		$product					=	$this->input->post('product1');
		$r_type						=	$this->input->post('r_type1');
		$data['r_type']				=	$r_type;
		$data['fdate']				=	$fdate;
		$data['tdate']				=	$tdate;
		$data['product']			=	$product;
		if($product==0)
		{
			$data['products']		= "All Products";
		}
		else
		{
		$data['products']			=	$this->Account->list_small($product);
		}

		$data['output']				=	$this->Account->get_account_detail($fdate,$tdate,$product,$r_type);
		$data['output1']			=	$this->Account->get_account_main($fdate,$tdate,$product,$r_type);
		$data['company']  			= 	$this->Inventory->getCompany();       
      	$this->load->view('ac_detailreport_print',$data);  
	}

	function getBproduct($c_id){

    $query=$this->db->get_where('category',array('brandid'=>$c_id));
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
 	}
 	function getGTproduct($c_id,$sc_id,$t_id){
    $query=$this->db->get_where('product',array('groupid'=>$c_id,'subcategoryid'=>$sc_id,'typeid'=>$t_id));
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
 	}
 	function getGproduct($c_id,$sc_id){
    $query=$this->db->get_where('product',array('groupid'=>$c_id,'subcategoryid'=>$sc_id));
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
 	}

 	function getTproduct($c_id,$sc_id){

    $query=$this->db->get_where('product',array('typeid'=>$c_id,'subcategoryid'=>$sc_id));
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
 	}
 	function getTGproduct($c_id,$sc_id,$g_id){

    $query=$this->db->get_where('product',array('typeid'=>$c_id,'subcategoryid'=>$sc_id,'groupid'=>$g_id));
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
 	}

 	function getSproduct($c_id){

    $query=$this->db->get_where('product',array('subcategoryid'=>$c_id));
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
 	}
 }