<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Excategory extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Excategory_model','Excategory',TRUE);
		$this->template->set('title','Excategory');
		$this->base_uri 			=	$this->config->item('admin_url')."excategory";
	}

	function index()
	{
		$data['page_title']			=	"Expense Category";
		$data['output']				=	$this->Excategory->list_all();
		// $this->template->load('template','customer',$data);
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('ecategory_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Excategory->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Expense Category";
		$data['action']				=	$this->base_uri.'/insert';
		$data['id']					=	"";
		$data['category']			=	"";
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('ecategory_add',$data);
      	$this->load->view('templates/footer');  
	}

	function insert()
	{

		$title 							=	$this->input->post('category');
		$this->tab_groups['category']	=	$title;
		 
		$this->db->insert('excategory',$this->tab_groups);

		$id 								=	$this->db->insert_id();
		// $config['upload_path']				=	$this->config->item('image_path').'features/original/';
		// $config['allowed_types']			=	'gif|jpg|jpeg|png';
		// $config['max_size']					=	'20000';
		// @$config['file_name']				=	$id.$config['file_ext'];
		// $this->load->library('upload', $config);
		// $this->upload->initialize($config);

		// if ($this->upload->do_upload('image'))
		// {		
			
		// 	$data = array('upload_data' => $this->upload->data());
		// 	$this->process_uploaded_image($data['upload_data']['file_name']);
		// 	$this->form_image['feature_image']	=	$data['upload_data']['file_name'];
		// 	$this->db->update('features', $this->form_image,array('feature_id'=>$id));
			
		// }	
		// else
		// {
		
		// 	$error = $this->upload->display_errors();	
		// 	$this->messages->add($error, 'error');
			
		// }

		// delete_files($this->config->item('image_path').'features/original/');
		$this->session->set_flashdata('Success','Expense Category Created');
		redirect($this->base_uri."/add");
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Expense Category";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Excategory->get_one_banner($id);
		$data['id']					=	$row->id;
		$data['category']			=	$row->category;
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('ecategory_add',$data);
      	$this->load->view('templates/footer');  
		// $this->template->load('template','features_add',$data);
	}

	function update()
	{
		$id 							=	$this->input->post('id');
		$title 							=	$this->input->post('category');
		$this->tab_groups['category']	=	$title;
		 
		$this->db->update('excategory', $this->tab_groups,array('id'=>$id));

		// $config['upload_path']				=	$this->config->item('image_path').'features/original/';
		// $config['allowed_types']			=	'gif|jpg|jpeg|png';
		// $config['max_size']					=	'20000';
		// @$config['file_name']				=	$id.$config['file_ext'];
		// $this->load->library('upload', $config);
		// $this->upload->initialize($config);
		// if (  $this->upload->do_upload('image'))
		// {		
		// 	$data = array('upload_data' => $this->upload->data());
		// 	$this->process_uploaded_image($data['upload_data']['file_name']);
		// 	$this->form_image['feature_image']=	$data['upload_data']['file_name'];
		// 	$this->db->update('features', $this->form_image,array('feature_id'=>$id));
		// }	
		// else
		// {
		// 	$error = $this->upload->display_errors();	
		// 	$this->messages->add($error, 'error');
		// }
		// delete_files($this->config->item('image_path').'features/original/');
		$this->session->set_flashdata('Success','Expense Category Updated');
		redirect($this->base_uri);
	}

	function process_uploaded_image($filename)
	{
		$large_path 	=	$this->config->item('image_path').'features/original/'.$filename;
		$medium_path 	=	$this->config->item('image_path').'features/'.$filename;
		$thumb_path 	=	$this->config->item('image_path').'features/small/'.$filename;
		
		$medium_config['image_library'] 	=	'gd2';
		$medium_config['source_image'] 		=	$large_path;
		$medium_config['new_image'] 		=	$medium_path;
		$medium_config['maintain_ratio'] 	=	TRUE;
		$medium_config['width'] 			=	1600;
		$medium_config['height'] 			=	400;
		$this->load->library('image_lib', $medium_config);
		$this->image_lib->initialize($medium_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		$thumb_config['image_library'] 		=	'gd2';
		$thumb_config['source_image'] 		=	$large_path;
		$thumb_config['new_image'] 			=	$thumb_path;
		$thumb_config['maintain_ratio'] 	=	TRUE;
		$thumb_config['width'] 				=	50;
		$thumb_config['height'] 			=	50;
		$this->load->library('image_lib', $thumb_config);
		$this->image_lib->initialize($thumb_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		return true;    
	}

	function active($id,$status)
	{
		$data 			=	array('status'=>$status);
		$this->db->update('excategory',$data,array('id'=>$id));
		// $this->list_ajax();
		redirect($this->base_uri);
	}

	function delete($id)
	{
		if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$this->db->delete('excategory',array('id'=>$id));
			// $this->session->set_flashdata('Error','User Deleted');
		}
				echo "Record Deleted";
		// $row 			=	$this->Excategory->get_one_banner($id);
		// // unlink($this->config->item('image_path').'features/small/'.$row->feature_image);
		// // unlink($this->config->item('image_path').'features/'.$row->feature_image);
		// $this->db->delete('excategory',array('id'=>$id));
		// // $this->list_ajax();
		// $this->session->set_flashdata('Error','Expense Category Deleted');
		// redirect($this->base_uri);
	}
}