<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Brand extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Brand_model','Brand',TRUE);
		$this->template->set('title','Manufacturers');
		$this->base_uri 			=	$this->config->item('admin_url')."brand";
	}

	function index()
	{
		$data['page_title']			=	"Manufacturers List";
		$data['output']				=	$this->Brand->list_all();
		 
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('brand_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Brand->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Manufacturers";
		$data['action']				=	$this->base_uri.'/insert';
		$data['br_id']				=	"";
		$data['br_brand']			=	"";
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('brand_add',$data);
      	$this->load->view('templates/footer');  
	}

	function insert()
	{

		 
		$this->tab_groups['br_brand']	=	$this->input->post('br_brand');
		 
		$this->db->insert('brand',$this->tab_groups);
		$this->session->set_flashdata('Success','Manufacturers Created');
		redirect($this->base_uri."/add");
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Manufacturers";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Brand->get_one_banner($id);
		$data['br_id']				=	$id;
		$data['br_brand']			=	$row->br_brand;
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('brand_add',$data);
      	$this->load->view('templates/footer');  
	}

	function update()
	{
		$id 								=	$this->input->post('br_id');
		$this->tab_groups['br_brand']	    =	$this->input->post('br_brand');
		 
		$this->db->update('brand', $this->tab_groups,array('br_id'=>$id));

		$this->session->set_flashdata('Success','Manufacturers Updated');
		redirect($this->base_uri);
	}


	function delete($id)
	{

		if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$row 	=	$this->Brand->get_one_banner($id);
			$this->db->delete('brand',array('br_id'=>$id));

	 }
				echo "Record Deleted";
	}
}