<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Preturn extends MX_Controller 
{
  public $tab_groups;
  public $form_image;

  function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('logged_in'))
    {
      redirect($this->config->item('admin_url'));
    }
    $this->load->model('Return_model','Return',TRUE);
    $this->load->model('Consume_model','Consume',TRUE);
    $this->load->model('Sale_model','Sales',TRUE);
    $this->load->model('Purchase_model','Purchase',TRUE);
    $this->load->model('Stock_model','Stock',TRUE);
    $this->load->model('Product_model','Product',TRUE);
    $this->template->set('title','Product Return');
    $this->base_uri       = $this->config->item('admin_url')."return";
  }

    function return_Slist(){
    $data['page_title']="Sales Return List";
    $data['output']=$this->Return->getSReturn();
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('return_slist',$data);
    $this->load->view('templates/footer'); 
  }

  function return_Plist(){
    $data['page_title']="Purchase Return List";
    $data['output']=$this->Return->getPReturn();
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('return_plist',$data);
    $this->load->view('templates/footer'); 
  }

  function return_Sadd()
  {
      $data['page_title'] = "Add Sales Return";
      $data['products']   = $this->Return->getSProducts();
      // print_r($data['products']);
      // $data['customer']   = $this->Sales->getCustomer();

      $data['returnid']   = $this->Return->getSReturnId();
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('return_sadd',$data);
      $this->load->view('templates/footer'); 
  }

  function return_Padd()
  {
      $data['page_title'] = "Add Purchase Return";
      $data['products']   = $this->Return->getPProducts();
      // $data['vendors']    = $this->Product->getSupplier();
      $data['returnid']   = $this->Return->getPReturnId();
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('return_padd',$data);
      $this->load->view('templates/footer'); 
  }

   function returnPBillno($bRNo)
  {
    $this->db->select('voucher_entry_detail.*,suppliers.sp_vendor,suppliers.sp_phone,suppliers.sp_id,voucher_entry.ve_customer');
    $this->db->from('voucher_entry_detail');
    $this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid', 'inner');
    $this->db->join('suppliers','voucher_entry.ve_supplier = suppliers.sp_id', 'inner');
    $this->db->where('voucher_entry.ve_bill_no',$bRNo);
    $this->db->where('voucher_entry.ve_type','p');
    $query  = $this->db->get();
    $result = $query->result_array();
    echo json_encode($result);
  }

  function returnSBillno($bRNo)
  {
    // print_r($bRNo);
    $this->db->select('voucher_entry_detail.*,voucher_entry.*');
    $this->db->from('voucher_entry_detail');
    $this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid', 'inner');
    // $this->db->join('patient','voucher_entry.ve_mrd = patient.p_mrd_no', 'inner');
    $this->db->where('voucher_entry.ve_vno',$bRNo);
    $where = '(ve_type="si" or ve_type = "so")';
    $this->db->where($where);
    $query=$this->db->get();
    if($query->num_rows()==0)
    {
      $result = "0";
    }
    else 
    {
      $result=$query->result_array();
    }
    echo json_encode($result);
  }

  function returnPPrice($bRNo,$r_item)
  {
    $this->db->select('voucher_entry.ve_date,voucher_entry_detail.*,product.pd_qty');
    $this->db->from('voucher_entry_detail');
    $this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid', 'inner');
    $this->db->join('product','product.pd_code = voucher_entry_detail.ved_itemid', 'inner');
    $this->db->where('voucher_entry.ve_bill_no',$bRNo);
    $this->db->where('voucher_entry_detail.ved_id',$r_item);
    $query=$this->db->get();
    $result=$query->result_array();
    echo json_encode($result);
  }

  function returnSPrice($bRNo,$r_item)
  {
    $this->db->select('voucher_entry.ve_date,voucher_entry_detail.*,product.pd_qty');
    $this->db->from('voucher_entry_detail');
    $this->db->join('voucher_entry','voucher_entry.ve_id = voucher_entry_detail.ved_veid', 'inner');
    $this->db->join('product','product.pd_code = voucher_entry_detail.ved_itemid', 'inner');
    $this->db->where('voucher_entry.ve_vno',$bRNo);
    $this->db->where('voucher_entry_detail.ved_id',$r_item);
    $query=$this->db->get();
    $result=$query->result_array();
    echo json_encode($result);
  }

  function Pedit($sid)
  {
      $data['page_title']   = "Edit Purchase Return";
      $data['preturns']     = $this->Return->getPReturnview($sid);
      $data['products']     = $this->Return->getPProducts();
      $data['vendors']      = $this->Product->getSupplier();

      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('return_pedit',$data);
      $this->load->view('templates/footer'); 
  }


  function return_SView($sid){
    $data['page_title'] = "Sale Return View";
    $data['preturns']   = $this->Return->getSReturnview($sid);
    // print_r($data['preturn']);
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('return_sview', $data);
    $this->load->view('templates/footer');  
  }

  function return_PView($sid){

    $data['page_title'] = "Purchase Return View";
    $data['preturns']    = $this->Return->getPReturnview($sid);
    // print_r($data['preturns']);
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('return_pview', $data);
    $this->load->view('templates/footer');  
  }


  function returnPAdd(){
    $ve_vno                         = $this->input->post('ve_vno');
    $ve_bill_no                     = $this->input->post('ve_bill_no');
    $ve_date                        = $this->input->post('ve_date');
    $ve_date                        = date("Y-m-d",strtotime($ve_date));
    $amount_paid                    = $this->input->post('apaid');
    $supp_id                        = $this->input->post('ve_supplier');

    $tab_groups['ve_bill_no']       = $ve_bill_no;
    $tab_groups['ve_vno']           = $ve_vno;
    $tab_groups['ve_date']          = $ve_date;
    $tab_groups['ve_supplier']      = $supp_id;
    $ve_customer                    = $this->input->post('ve_customer');
    $tab_groups['ve_customer']      = $ve_customer;
    $tab_groups['ve_amount']        = $this->input->post('sum'); 
    $tab_groups['ve_sgst']          = $this->input->post('sgsta');    
    $tab_groups['ve_cgst']          = $this->input->post('cgsta');  
    $tab_groups['ve_gtotal']        = $this->input->post('apayable');  
    $tab_groups['ve_apayable']      = $this->input->post('gtotal');
    $tab_groups['ve_apaid']         = $amount_paid;
    $amount_balance                 = $this->input->post('balance');
    $tab_groups['ve_balance']       = $amount_balance;
    $tab_groups['ve_round']         = $this->input->post('roundoff');
    $tab_groups['ve_type']          = "pr";
    $tab_groups['ve_user']          = $this->input->post('user_type');

    if($amount_balance==0)
    {
      $tab_groups['ve_pstaus']      =   "FP";
    }
    else
    {
      $tab_groups['ve_pstaus']      =   "NP";
    }


    $this->db->insert('voucher_entry',$tab_groups);
    $ve_id                           = $this->db->insert_id();
    
    $this->Sales->set_sale_balance($amount_paid,$ve_date);

  foreach($this->input->post('ved_item') as $k => $v)
  {
    $inputs = array();
    $inputs['ved_veid']       = $ve_id;
    $inputs['ved_date']       = $ve_date;
    $stockpid                 = $this->input->post('ved_itemid')[$k];
    $inputs['ved_itemid']     = $stockpid;
    $inputs['ved_item']       = $v;
    $ved_price                = $this->input->post('ved_price')[$k];
    $ved_batch                = $this->input->post('ved_batch')[$k];
    $inputs['ved_batch']      = $ved_batch;
    $inputs['ved_expiry']     = $this->input->post('ved_expiry')[$k];

    $purchase_item_qty        = $this->input->post('ved_qty')[$k];
    $unit_item_qty            = $this->input->post('ved_uqty')[$k];
    $purchase_unit_qty        = $purchase_item_qty * $unit_item_qty;
    $inputs['ved_qty']        = $purchase_unit_qty;
    $inputs['ved_uqty']       = $unit_item_qty;
    $inputs['ved_price']      = $ved_price;
    $ved_unit                 = $this->input->post('ved_unit')[$k];
    $inputs['ved_unit']       = $ved_unit;
    $inputs['ved_total']      = $this->input->post('ved_total')[$k];
    $inputs['ved_sgstp']      = $this->input->post('ved_sgstp')[$k];
    $inputs['ved_sgsta']      = $this->input->post('ved_sgsta')[$k];
    $inputs['ved_cgstp']      = $this->input->post('ved_cgstp')[$k];
    $inputs['ved_cgsta']      = $this->input->post('ved_cgsta')[$k];
    $inputs['ved_gtotal']     = $this->input->post('ved_gtotal')[$k];

    $this->Return->set_preturn_stock($stockpid,$purchase_unit_qty,$ve_date,$ved_batch,$ve_customer);
    $this->db->insert('voucher_entry_detail',$inputs);
  }
     print_r($ve_id);
  }

  function returnSAdd(){
    $ve_vno                         = $this->input->post('ve_vno');
    $ve_bill_no                     = $this->input->post('ve_bill_no');
    $ve_date                        = $this->input->post('ve_date');
    $ve_date                        = date("Y-m-d",strtotime($ve_date));
    $amount_paid                    = $this->input->post('gtotal');
    $ve_mrd                         = $this->input->post('ve_mrd');

    $tab_groups['ve_bill_no']       = $ve_bill_no;
    $tab_groups['ve_vno']           = $ve_vno;
    $tab_groups['ve_date']          = $ve_date;
    $tab_groups['ve_mrd']           = $ve_mrd;
    $tab_groups['ve_customer']      = $this->input->post('ve_customer');
    $tab_groups['ve_supplier']      = $this->input->post('ve_supplier');
    $tab_groups['ve_patient']       = $this->input->post('ve_patient');
    $tab_groups['ve_phone']         = $this->input->post('ve_phone');
    $tab_groups['ve_amount']        = $this->input->post('sum'); 
    $tab_groups['ve_sgst']          = $this->input->post('sgsta');    
    $tab_groups['ve_cgst']          = $this->input->post('cgsta');  
    $tab_groups['ve_gtotal']        = $this->input->post('apayable');  
    $tab_groups['ve_apayable']      = 0-$amount_paid;
    $tab_groups['ve_apaid']         = "0";
    $tab_groups['ve_balance']       = 0-$amount_paid;
    $tab_groups['ve_round']         = $this->input->post('roundoff');
    $tab_groups['ve_pstaus']        = "NP";
    $tab_groups['ve_type']          = "sr";
    $tab_groups['ve_user']          = $this->input->post('user_type');
    $payment_type                   = $this->input->post('payment_type');
    if($payment_type == "True")
    {
      $tab_groups['ve_status']        = "cr";
    }
    else
    {
      $tab_groups['ve_status']        = "cc";
    }
    
    $this->db->insert('voucher_entry',$tab_groups);
    $ve_id                           = $this->db->insert_id();

  foreach($this->input->post('ved_item') as $k => $v)
  {
    $inputs = array();
    $inputs['ved_veid']       = $ve_id;
    $inputs['ved_date']       = $ve_date;
    $stockpid                 = $this->input->post('ved_itemid')[$k];
    $inputs['ved_itemid']     = $stockpid;
    $inputs['ved_item']       = $v;
    $ved_price                = $this->input->post('ved_price')[$k];
    $ved_batch                = $this->input->post('ved_batch')[$k];
    $inputs['ved_batch']      = $ved_batch;
    $inputs['ved_expiry']     = $this->input->post('ved_expiry')[$k];

    $purchase_item_qty        = $this->input->post('ved_qty')[$k];
    $unit_item_qty            = $this->input->post('ved_uqty')[$k];
    $inputs['ved_price']      = $ved_price;
    $ved_unit                 = $this->input->post('ved_unit')[$k];
    if($ved_unit != "No.s") {
    $purchase_unit_qty        = $purchase_item_qty * $unit_item_qty;
    }
    else
    {
      $purchase_unit_qty      = $purchase_item_qty;
    }

    $inputs['ved_qty']        = $purchase_unit_qty;
    $inputs['ved_uqty']       = $unit_item_qty;

    $inputs['ved_unit']       = $ved_unit;
    $inputs['ved_total']      = $this->input->post('ved_total')[$k];
    $inputs['ved_sgstp']      = $this->input->post('ved_sgstp')[$k];
    $inputs['ved_sgsta']      = $this->input->post('ved_sgsta')[$k];
    $inputs['ved_cgstp']      = $this->input->post('ved_cgstp')[$k];
    $inputs['ved_cgsta']      = $this->input->post('ved_cgsta')[$k];
    $inputs['ved_gtotal']     = $this->input->post('ved_gtotal')[$k];

    $this->Return->set_sreturn_stock($stockpid,$purchase_unit_qty,$ve_date,$ved_batch);
    $this->db->insert('voucher_entry_detail',$inputs);
  }
     print_r($ve_id);
  }

  function preturnVUpdate(){

    $upid                               = $_GET['pUpid'];
    $purchase_date                      = $this->input->post('purchase_date');
    $purchase_date                      = date("Y-m-d",strtotime($purchase_date));
    $tab_groups['ve_apaid']             = $this->input->post('ve_apaid');
    $amount_paid                        = $this->input->post('ve_apaid');
    $amount_balance                     = $this->input->post('amount_balance');
    $tab_groups['ve_balance']           = $amount_balance;
    if($amount_balance==0)
    {
      $tab_groups['ve_pstaus']  =   "FP";
    }
    else
    {
      $tab_groups['ve_pstaus']  =   "NP";
    }
      $previuos_apaid = $this->Purchase->get_voucher($upid);
      $this->Return->dlt_purchase_balance($previuos_apaid,$purchase_date);

      $this->db->update('voucher_entry',$tab_groups,array('ve_id'=>$upid));
      
      $this->Return->up_purchase_balance($amount_paid,$purchase_date);
    }

    function sreturnVUpdate(){

    $upid                               = $_GET['pUpid'];
    $purchase_date                      = $this->input->post('purchase_date');
    $purchase_date                      = date("Y-m-d",strtotime($purchase_date));
    $tab_groups['ve_apaid']             = $this->input->post('ve_apaid');
    $amount_paid                        = $this->input->post('ve_apaid');
    $amount_balance                     = $this->input->post('amount_balance');
    $tab_groups['ve_balance']           = $amount_balance;
    if($amount_balance==0)
    {
      $tab_groups['ve_pstaus']  =   "FP";
    }
    else
    {
      $tab_groups['ve_pstaus']  =   "NP";
    }
      $previuos_apaid = $this->Purchase->get_voucher($upid);
      $this->Return->dlt_sale_balance($previuos_apaid,$purchase_date);

      $this->db->update('voucher_entry',$tab_groups,array('ve_id'=>$upid));
      
      $this->Return->up_sale_balance($amount_paid,$purchase_date);
    }

  function pdelete()
  {

    if($_REQUEST['empid']) {
      $id   = $_REQUEST['empid']; 
      $idd  = $id;

      $result = $this->Purchase->get_voucher_details($idd);

      foreach ($result as $key => $value) {

        $stockpid             = $value['ved_itemid'];
        $purchase_item_qty    = $value['ved_qty'];
        $purchase_date        = $value['ved_date'];
        $ved_batch            = $value['ved_batch'];

        $this->Return->delete_purchase_stock($stockpid,$purchase_item_qty,$purchase_date,$ved_batch,$ve_customer);
        
      }

      $previuos_apaid = $this->Purchase->get_voucher($idd);
      $this->Return->dlt_purchase_balance($previuos_apaid,$purchase_date);

      $this->db->delete('voucher_entry',array('ve_id'=>$id));
      $this->db->delete('voucher_entry_detail',array('ved_veid'=>$id));

   }
        echo "Record Deleted";
  }

  function sdelete()
  {

    if($_REQUEST['empid']) {
      $id   = $_REQUEST['empid']; 
      $idd  = $id;

      $result = $this->Purchase->get_voucher_details($idd);

      foreach ($result as $key => $value) {

        $stockpid             = $value['ved_itemid'];
        $purchase_item_qty    = $value['ved_qty'];
        $purchase_date        = $value['ved_date'];
        $ved_batch            = $value['ved_batch'];

        $this->Return->delete_sale_stock($stockpid,$purchase_item_qty,$purchase_date,$ved_batch);
        
      }
      
      $previuos_apaid = $this->Purchase->get_voucher($idd);
      $this->Purchase->dlt_purchase_balance($previuos_apaid,$purchase_date);

      $this->db->delete('voucher_entry',array('ve_id'=>$id));
      $this->db->delete('voucher_entry_detail',array('ved_veid'=>$id));

   }
        echo "Record Deleted";
  }

    function getPprint(){
  $sid              = $_GET['sPrintid'];
  $data['company']  = $this->Purchase->getCompany();
  $data['preturns']    = $this->Return->getPReturnview($sid);
  // $this->load->view('templates/header');  
          
  $this->load->view('pr_print',$data);
  }

  function getSprint(){
  $sid                = $_GET['sPrintid'];
  $data['company']    = $this->Purchase->getCompany();
  $data['preturns']   = $this->Return->getSReturnview($sid);
  // $this->load->view('templates/header');  
          
  $this->load->view('sr_print',$data);
  }

}