<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Opregister extends MX_Controller 
{
  public $tab_groups;
  public $form_image;
  public $tab_datas;
  public $patient_datas;
  public $renew_groups;
  public $renew_data;

  function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('logged_in'))
    {
      redirect($this->config->item('admin_url'));
    }
    $this->load->model('Opregister_model','Opregister',TRUE);
    $this->load->model('Booking_model','Booking',TRUE);
    $this->load->model('Booking_token_model','Booking_token',TRUE);
    $this->load->model('Users_model','Users',TRUE);
    $this->load->model('Purchase_model','Purchase',TRUE);
    $this->load->model('Patientmodel','Patient',TRUE);
    $this->load->model('Sale_model','Sales',TRUE);
    $this->template->set('title','OP Register');
    $this->base_uri       = $this->config->item('admin_url')."opregister";
  }

  function index()
  {
    $data['page_head']      = "OP List";
    $data['page_title']     = "Today's OP";
    $data['output']         = $this->Opregister->list_all();
     
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('op_list',$data);
      $this->load->view('templates/footer'); 
  }

  public function list_ajax()
  {
    echo $data['output']    = $this->Opregister->list_all();
  }

  function add()
  {
    $data['page_title']   = "Add OP";
    $data['action']       = $this->base_uri.'/insert';
    $data['doctors']      = $this->Booking_token->get_doctor();
    // $data['patient']      = $this->Booking->get_patients();
    // $data['booking']      = $this->Opregister->today_book();
    // $data['output']       = "$this->Opregister->list_all()";

     
        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('op_add',$data);
        $this->load->view('templates/footer');  
  }

  function book_add($id)
  {
    $data['page_title']   = "Add OP";
    $data['action']       = $this->base_uri.'/insert';
    $data['booking']      = $this->Opregister->today_books($id);
    // print_r($data['booking']);
    // exit();
    // $data['output']       = $this->Opregister->list_all();

     
        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('op_badd',$data);
        $this->load->view('templates/footer');  
  }

  function get_op($id)
  {
    $data['page_title']   = "Add OP";
    $data['action']       = $this->base_uri.'/insert';
    $data['doctors']      = $this->Booking_token->get_doctor();
    $data['patients']     = $this->Opregister->today_nbooks($id);
    // print_r($data['patients']);
    // $data['output']       = $this->Opregister->list_all();

     
        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('op_nbadd',$data);
        $this->load->view('templates/footer');  
  }

  function insert()
  {
    $renew_chk                          =  $this->input->post('renew_chk');

    $bk_id                              =  $this->input->post('bk_id');
    $op_mrd                             =  $this->input->post('op_mrd');
    $op_department                      =  $this->input->post('op_department');
    $op_visit_type                      =  $this->Booking->get_visit_type($op_mrd);
    // print_r(($op_visit_type));
    // exit();
    $this->tab_groups['op_mrd']         =  $op_mrd;
    $this->tab_groups['op_opno']        =  $this->input->post('op_opno');
    $op_type                            =  $this->input->post('op_type');
    $this->tab_groups['op_type']        =  $op_type;

    // $this->tab_groups['op_name']        =  $this->input->post('op_name');
    // $this->tab_groups['op_phone']       =  $this->input->post('p_phone');
    // $this->tab_groups['op_doc_name']    =  $this->input->post('op_doc_name');

    $this->tab_groups['op_date']        =  $this->input->post('op_date');
    $this->tab_groups['op_department']  =  $op_department;
    $this->tab_groups['op_doctor']      =  $this->input->post('op_doctor');
    $this->tab_groups['op_shift']       =  $this->input->post('op_shift');
    $this->tab_groups['op_docfee']      =  $this->input->post('op_docfee');
    $this->tab_groups['op_visit_type']  =  $op_visit_type;
    $row                                =  $this->Opregister->get_patient($op_mrd);
    $data['p_renew_visits']             =  $row->p_renew_visits+1;
    $data['p_tot_visits']               =  $row->p_tot_visits+1;

    $this->db->insert('op',$this->tab_groups);

    $this->db->update('patient',$data,array('p_mrd_no'=>$op_mrd));

    $datas['bk_status']  = 0;
    $this->db->update('booking', $datas,array('bk_id'=>$bk_id));

    if($op_visit_type == "Follow Up")
    {
      $this->tab_datas['mr_mrd']          =  $op_mrd;
      $this->tab_datas['mr_doc']          =  $this->input->post('op_doctor');
      $this->tab_datas['mr_tno']          =  $this->input->post('op_opno');
      $this->tab_datas['mr_shift']        =  $this->input->post('op_shift');
      $this->tab_datas['mr_date']         =  $this->input->post('op_date');
      $this->tab_datas['mr_remark ']      =  "Ordered to OP";
      $this->tab_datas['mr_status']       =  "op";
      $this->db->insert('mrd_room', $this->tab_datas);
    }
    $this->patient_datas['p_phone']          =  $this->input->post('p_phone');
    $this->patient_datas['p_address']        =  $this->input->post('p_address');
    $this->patient_datas['p_street']         =  $this->input->post('p_street');
    $this->patient_datas['p_age']            =  $this->input->post('p_age');
    $this->patient_datas['p_sex']            =  $this->input->post('p_sex');

    $this->db->update('patient',$this->patient_datas,array('p_mrd_no'=>$op_mrd));

// Renew Starts
    if($renew_chk=="r") {
    $row                                    = $this->Patient->get_one_banner($op_mrd);
    $p_renewfee                             = $this->Patient->get_renewfee();
    $p_renew_date                           = $row->p_renew_date;
    $p_renew_fee                            = $row->p_renew_fee;
    $today_date                             = date("Y-m-d");
    $renew_groups['p_renew_date']           = $today_date;
    $renew_groups['p_renew_visits']         = 0;
    $renew_groups['p_renew_fee']            = $p_renew_fee + $p_renewfee;
    $this->db->update('patient', $renew_groups,array('p_mrd_no'=>$op_mrd));

    $this->Sales->set_sale_balance($p_renewfee,$today_date);

    $ve_vno               = $this->Patient->getPatientRenewno();

    $this->renew_data['ve_vno']             = $ve_vno;
    $this->renew_data['ve_date']            = $today_date;
    $this->renew_data['ve_cash_date']       = $today_date;
    $this->renew_data['ve_supplier']        = "17";
    $this->renew_data['ve_mrd']             = $op_mrd;
    $this->renew_data['ve_apayable']        = $p_renewfee;
    $this->renew_data['ve_apaid']           = $p_renewfee;
    $this->renew_data['ve_balance']         = "0";
    $this->renew_data['ve_pstaus']          = "FP";
    $this->renew_data['ve_type']            = "prenew";

    $this->db->insert('voucher_entry',$this->renew_data);
  }
    // Renew Ends

    $this->session->set_flashdata('Success','OP Created');
    redirect($this->base_uri."/add");

    
  }

  function token_print($id){

  $row                  = $this->Opregister->get_token_op($id);

  $data['op_opno']      = $row->op_opno;
  $data['op_shift']     = $row->op_shift;
  $data['op_date']      = $row->op_date;
  $data['op_doctor']    = $row->u_name;
  $data['op_dept']      = $row->dp_department;
  $data['p_mrd_no']     = $row->p_mrd_no;
  $data['p_name']       = $row->p_title." ".$row->p_name;
  $data['p_age']        = $row->p_age;
  $data['p_sex']        = $row->p_sex;

  $data['company']      = $this->Purchase->getCompany();
    // $this->load->view ('templates/header');       
    $this->load->view ('op_print',$data);
  }

  function verify_tokens()
  {
    $sft_id   = $this->input->post('sft_id');
    $doc_id   = $this->input->post('doc_id');
    $bk_date  = $this->input->post('op_date');

    if($sft_id=="m")
    {
      $this->db->select('dc_morntok');
      $this->db->from('doctor');
      $this->db->where('dc_docid',$doc_id);
      $query      = $this->db->get();
      $data = $query->row()->dc_morntok;

      $this->db->select('count(*) as token_cnt');
      $this->db->from('op');
      $this->db->where('op_doctor',$doc_id);
      $this->db->where('op_shift','m');
      $this->db->where('op_date',$bk_date);
      $query1      = $this->db->get();
      if($query1->num_rows()==0)
      {
        $data1 = 0;
      }
      else
      {
        $data1   = $query1->row()->token_cnt;
      }

    }
    else if($sft_id=="e")
    {
      $this->db->select('dc_evngtok');
      $this->db->from('doctor');
      $this->db->where('dc_docid',$doc_id);
      $query      = $this->db->get();
      $data = $query->row()->dc_evngtok;

      $this->db->select('count(*) as token_cnt');
      $this->db->from('op');
      $this->db->where('op_doctor',$doc_id);
      $this->db->where('op_shift','e');
      $this->db->where('op_date',$bk_date);
      $query1      = $this->db->get();
      if($query1->num_rows()==0)
      {
        $data1 = 0;
      }
      else
      {
        $data1   = $query1->row()->token_cnt;
      }
    }
    print_r($data-$data1);
  }

  function get_token()
  {
    $sft_id   = $this->input->post('sft_id');
    $doc_id   = $this->input->post('doc_id');
    $bk_date  = $this->input->post('op_date');

    // $sql = "SELECT MAX(CAST(SUBSTRING(op_opno, 4, length(op_opno)-3) AS UNSIGNED)) FROM op where (op_doctor='.$doc_id.') AND (op_shift='.$sft_id.') AND (op_date='.$bk_date.') AND (op_type='nb') LIMIT 1";

      $this->db->select('CAST(SUBSTRING(op_opno, 4, length(op_opno)-2) as SIGNED) as op_opno');
      $this->db->from('op');
      $this->db->where('op_doctor',$doc_id);
      $this->db->where('op_shift',$sft_id);
      $this->db->where('op_date',$bk_date);
      $this->db->where('op_type',"nb");
      $this->db->order_by('op_opno','desc');
      $this->db->limit(1);
      $query      = $this->db->get();
      if($query->num_rows()==0)
      {
        $data = 'NB_1';
      }
      else
      {
        // $result = explode('_', $query->row()->op_opno);
        $end2   =  $query->row()->op_opno;
        $end2++;
        $data = 'NB_'.$end2;
      }
    print_r($data);
  }

   function get_total_tokens()
  {
    $sft_id   = $this->input->post('sft_id');
    $doc_id   = $this->input->post('doc_id');
    $bk_date  = $this->input->post('op_date');

      $this->db->select('count(*) as token_cnt');
      $this->db->from('op');
      $this->db->where('op_doctor',$doc_id);
      $this->db->where('op_shift',$sft_id);
      $this->db->where('op_date',$bk_date);
      $query      = $this->db->get();
      if($query->num_rows()==0)
      {
        $data = 0;
      }
      else
      {
        $data   = $query->row()->token_cnt;
      }
    print_r($data);
  }

  function get_doctor_fee()
  {
    $doc_id = $this->input->post('doc_id');

      $this->db->select('u_fees');
      $this->db->from('users');
      $this->db->where('u_emp_id',$doc_id);
      $query  = $this->db->get();
      $data   = $query->row()->u_fees;

    print_r($data);
  }


  function get_bk_doctor($bk_no,$bk_dt){

    $this->db->select('users.u_emp_id,users.u_name,users.u_department,users.u_fees,booking.bk_shift');
    $this->db->from('users');
    $this->db->join('booking','booking.bk_doc = users.u_emp_id');
    $this->db->where('booking.bk_no',$bk_no);
    $this->db->where('booking.bk_date',$bk_dt);
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
  }

  function get_doctor_dept()
  {
    $doc_id = $this->input->post('doc_id');

      $this->db->select('u_department');
      $this->db->from('users');
      $this->db->where('u_emp_id',$doc_id);
      $query  = $this->db->get();
      $data   = $query->row()->u_department;
    print_r($data);
  }

  function edit($id)
  {
    $data['page_title']     = "Edit OP";
    $data['action']         = $this->base_uri.'/update';
    // $row                    = $this->Opregister->get_one_banner($id);
    $data['doctors']        = $this->Booking_token->get_doctor();
    $data['patients']       = $this->Opregister->get_one_banner($id);
    // $data['op_id']          = $id;
    // $op_mrd                 = $row->op_mrd;
    // $data['op_mrd']         = $op_mrd;
    // $data['op_opno']        = $row->op_opno;
    // $data['op_type']        = $row->op_type;
    // $data['op_date']        = $row->op_date;
    // $data['op_department']  = $row->op_department;
    // $data['op_doctor']      = $row->op_doctor;
    // $data['op_shift']       = $row->op_shift;
    // $data['op_docfee']      = $row->op_docfee;
    // $data['op_visit_type']  = $row->op_visit_type;

    // $row1                   = $this->Opregister->get_patient($op_mrd);
    // $data['bk_name']        = $row1->p_name;
    // $data['bk_phone']       = $row1->p_phone;

    // $data['booking']      = $this->Opregister->today_book();
     
        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('op_edit',$data);
        $this->load->view('templates/footer');  
  }

  function update()
  {
    $id                                =  $this->input->post('op_id');
    $op_mrd                            =  $this->input->post('op_mrd');
    $this->tab_groups['op_mrd']        =  $op_mrd;
    $this->tab_groups['op_opno']       =  $this->input->post('op_opno');
    $this->tab_groups['op_type']       =  $this->input->post('op_type');
    $this->tab_groups['op_date']       =  $this->input->post('op_date');
    $this->tab_groups['op_doctor']     =  $this->input->post('op_doctor');
    $this->tab_groups['op_shift']      =  $this->input->post('op_shift');
    $op_docfee                         =  $this->input->post('op_docfee');
    $op_department                     =  $this->input->post('op_department');

    // $this->tab_groups['op_name']        =  $this->input->post('op_name');
    // $this->tab_groups['op_phone']       =  $this->input->post('p_phone');
    // $this->tab_groups['op_doc_name']    =  $this->input->post('op_doc_name');

    if($op_docfee != "" && $op_department != "")
    {
      $this->tab_groups['op_department'] =  $op_department;
      $this->tab_groups['op_docfee']     =  $op_docfee;
    }
    $this->tab_groups['op_visit_type'] =  $this->input->post('op_visit_type');
     
    $this->db->update('op', $this->tab_groups,array('op_id'=>$id));

    $this->patient_datas['p_phone']          =  $this->input->post('p_phone');
    $this->patient_datas['p_address']        =  $this->input->post('p_address');
    $this->patient_datas['p_street']         =  $this->input->post('p_street');
    $this->patient_datas['p_age']            =  $this->input->post('p_age');
    $this->patient_datas['p_sex']            =  $this->input->post('p_sex');

    $this->db->update('patient',$this->patient_datas,array('p_mrd_no'=>$op_mrd));

    $this->session->set_flashdata('Success','OP Updated');
    redirect('opregister/add');
  }

  function delete()
  {

    if($_REQUEST['empid']) {
      $id   = $_REQUEST['empid']; 
      $row1                     =  $this->Opregister->get_one_banners($id);
      $mr_mrd                   =  $row1->op_mrd;
      $p_renew_visits           =  $row1->p_renew_visits;
      $data['p_renew_visits']   =  $p_renew_visits-1;
      $p_tot_visits             =  $row1->p_tot_visits;
      $data['p_tot_visits']     =  $p_tot_visits-1;

      $this->db->delete('op',array('op_id'=>$id));
      $this->db->update('patient',$data,array('p_mrd_no'=>$mr_mrd));
      $this->db->delete('mrd_room',array('mr_mrd'=>$mr_mrd));
   }
        echo "Record Deleted";
  }

  function get_mrd($mrd)
  {

      $this->db->select('patient.p_mrd_no,patient.p_title,patient.p_name,patient.p_phone,patient.p_renew_days,patient.p_renew_count,patient.p_renew_visits,patient.p_renew_date,patient.p_address,patient.p_street,patient.p_age,patient.p_sex');
      $this->db->from('patient');
      $this->db->where('patient.p_mrd_no',$mrd);
      $query1  = $this->db->get();
      $data  = $query1->result_array();

      if($query1->num_rows()==0)
      { $data ="0"; }
       echo  json_encode($data);
  }
  function get_phone($phone)
  {
     
        $this->db->select('patient.p_mrd_no,patient.p_title,patient.p_name,patient.p_phone,patient.p_renew_days,patient.p_renew_count,patient.p_renew_visits,patient.p_renew_date,patient.p_address,patient.p_street,patient.p_age');
      $this->db->from('patient');
      $this->db->where('patient.p_phone',$phone);
      $query1  = $this->db->get();
      $data  = $query1->result_array();

      if($query1->num_rows()==0)
      { $data ="0"; }
 

    echo  json_encode($data);
  }

  function get_previuos_visit()
  {
      $op_mrd = $this->input->post('op_mrd');
      $this->db->select('op_date');
      $this->db->from('op');
      $this->db->where('op_mrd',$op_mrd);
      $this->db->order_by('op_date','desc');
      $this->db->limit(1);
      $query  = $this->db->get();
      if($query->num_rows() == 0)
      {
        $data  = "Fresh Visit";
      }
      else
      {
         $data  = $query->row()->op_date;
      }
     
      print_r($data);
  }

}