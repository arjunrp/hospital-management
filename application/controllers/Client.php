<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Client extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Clientmodel','Client',TRUE);
		$this->template->set('title','Client');
		$this->base_uri 			=	$this->config->item('admin_url')."customer";
	}

	function index()
	{
		$data['page_title']			=	"Customer";
		$data['output']				=	$this->Customer->list_all();
		$this->template->load('template','Customer',$data);
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Customer->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Customer";
		$data['action']				=	$this->base_uri.'/insert';
		$data['id']					=	"";
		$data['title']				=	"";
		$data['url']				=	"";
		$data['image']				=	"";
		$data['type']				=	"";
		$this->template->load('template','client_add',$data);
	}

	function insert()
	{

		$title 									=	$this->input->post('title');
		$this->tab_groups['customer']			=	$title;
		// $this->tab_groups['service_description']		=	$this->input->post('url');
		// $this->tab_groups['service_type']		=	$this->input->post('stype');
		$this->db->insert('customer',$this->tab_groups);

		$id 								=	$this->db->insert_id();
		$config['upload_path']				=	$this->config->item('image_path').'client/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'20000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ($this->upload->do_upload('image'))
		{		
			
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			// $this->form_image['client_logo']	=	$data['upload_data']['file_name'];
			$this->db->update('customer', $this->form_image,array('id'=>$id));
			
		}	
		else
		{
		
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
			
		}

		delete_files($this->config->item('image_path').'client/original/');
		redirect($this->base_uri);
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Client";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Client->get_one_banner($id);
		$data['id']					=	$row->id;
		$data['title']				=	$row->customer;
		// $data['url']				=	$row->service_description;
		// $data['image']				=	$row->client_image;
		// $data['type']				=	$row->service_type;
		$this->template->load('template','client_add',$data);
	}

	function update()
	{
		$id 								=	$this->input->post('id');
		$title 								=	$this->input->post('title');
		$this->tab_groups['customer']		=	$title;
		// $this->tab_groups['service_description']		=	$this->input->post('url');
		// $this->tab_groups['service_type']		=	$this->input->post('stype');
		$this->db->update('customer', $this->tab_groups,array('id'=>$id));

		$config['upload_path']				=	$this->config->item('image_path').'client/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'20000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (  $this->upload->do_upload('image'))
		{		
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['client_logo']=	$data['upload_data']['file_name'];
			$this->db->update('customer', $this->form_image,array('id'=>$id));
		}	
		else
		{
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
		}
		delete_files($this->config->item('image_path').'client/original/');
		redirect($this->base_uri);
	}

	function process_uploaded_image($filename)
	{
		$large_path 	=	$this->config->item('image_path').'client/original/'.$filename;
		$medium_path 	=	$this->config->item('image_path').'client/'.$filename;
		$thumb_path 	=	$this->config->item('image_path').'client/small/'.$filename;
		
		$medium_config['image_library'] 	=	'gd2';
		$medium_config['source_image'] 		=	$large_path;
		$medium_config['new_image'] 		=	$medium_path;
		$medium_config['maintain_ratio'] 	=	TRUE;
		$medium_config['width'] 			=	1600;
		$medium_config['height'] 			=	400;
		$this->load->library('image_lib', $medium_config);
		$this->image_lib->initialize($medium_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		$thumb_config['image_library'] 		=	'gd2';
		$thumb_config['source_image'] 		=	$large_path;
		$thumb_config['new_image'] 			=	$thumb_path;
		$thumb_config['maintain_ratio'] 	=	TRUE;
		$thumb_config['width'] 				=	50;
		$thumb_config['height'] 			=	50;
		$this->load->library('image_lib', $thumb_config);
		$this->image_lib->initialize($thumb_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		return true;    
	}

	function active($id,$status)
	{
		$data 			=	array('status'=>$status);
		$this->db->update('customer',$data,array('id'=>$id));
		$this->list_ajax();
	}

	function delete($id)
	{
		$row 			=	$this->Client->get_one_banner($id);
		// unlink($this->config->item('image_path').'client/small/'.$row->client_logo);
		// unlink($this->config->item('image_path').'client/'.$row->client_logo);
		$this->db->delete('customer',array('id'=>$id));
		$this->list_ajax();
	}
}