<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Opbilling extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Opbilling_model','Opbilling',TRUE);
    $this->load->model('Discharge_model','Discharge',TRUE);
		$this->load->model('Investigation_model','Investigation',TRUE);
    $this->load->model('Sale_model','Sales',TRUE);
    $this->load->model('Purchase_model','Purchase',TRUE);
    $this->load->model('Booking_token_model','Booking_token',TRUE);
		$this->template->set('title','OP Billing');
		$this->base_uri 			=	$this->config->item('admin_url')."opbilling";
	}

	function index()
	{
		$data['page_title']			=	"List";
		$data['output']				=	$this->Opbilling->list_all();
		 
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('opbilling_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Bed->list_all();
	}

	function add()
	{
		$data['page_title']			  =	"New OP Bill";
		$data['action']				    =	$this->base_uri.'/insert';
		$data['bd_id']				    =	"";
		$data['patients']    	    = $this->Investigation->get_patientsop();
		$data['bill_no']			    =	$this->Opbilling->get_op_bill_no();
		$data['additional_bills']	=	$this->Discharge->get_additional_bill();
    $data['admin_pswd']       = $this->Discharge->get_admin_pswd();
    $data['doctors']          = $this->Booking_token->get_doctor();
    // print_r($data['admin_pswd']);
		// print_r($data['additional_bills']);
		 
		    $this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('opbilling_add',$data);
      	$this->load->view('templates/footer');  
	}

  function view($ve_id)
  {
    $data['page_title']       = "OP Bill View";
    $data['additionals']      = $this->Opbilling->get_additional_items($ve_id);
     // print_r($data['additionals']);
      // exit();
     
        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('opbilling_view',$data);
        $this->load->view('templates/footer');  
  }

  function getPrint(){
    $ipno                    = $_GET['Printid'];
    $data['company']          = $this->Purchase->getCompany();
    
    $data['additionals']      = $this->Opbilling->get_additional_items($ipno);
    
    $this->load->view('opbilling_print',$data);
  }


  function opbillingAdd(){

    $this->tab_groups['ve_vno']           = $this->input->post('ve_vno');
    $opbill_date                          = $this->input->post('ve_date');
    $opbill_date                          = date("Y-m-d",strtotime($opbill_date));
    $this->tab_groups['ve_date']          = $opbill_date;
    $this->tab_groups['ve_supplier']      = "3";
    $op_opno      						            = $this->input->post('ve_customer');
    $this->tab_groups['ve_customer']      = $op_opno;
    $this->tab_groups['ve_mrd']        	  = $this->input->post('ve_mrd');
    $this->tab_groups['ve_patient']       = $this->input->post('ve_patient');
    $this->tab_groups['ve_phone']         = $this->input->post('ve_phone');
    $this->tab_groups['ve_doctor']        = $this->input->post('ve_doctor');
    $this->tab_groups['ve_amount']        = $this->input->post('lab_mrp');
    $this->tab_groups['ve_discount']      = $this->input->post('discountp');
    $this->tab_groups['ve_discounta']     = $this->input->post('discounta');
    $this->tab_groups['ve_apayable']      = $this->input->post('sum');
    $amount_paid                          = $this->input->post('sum');
    $this->tab_groups['ve_apaid']         = "0";
    $this->tab_groups['ve_balance']       = $amount_paid;
    $this->tab_groups['ve_round']         = $this->input->post('roundoff');

    $this->tab_groups['ve_pstaus']        = "NP";
    $this->tab_groups['ve_user']          = $this->input->post('user_type');
    $this->tab_groups['ve_type']          = "opbl";
    $this->tab_groups['ve_status']        = "cc";
    $this->db->insert('voucher_entry',$this->tab_groups);
    $opbillid                             = $this->db->insert_id();

     foreach($this->input->post('ved_item') as $k => $v)
  {

    $inputs = array();
    $inputs['ved_veid']       = $opbillid;
    $inputs['ved_date']       = $opbill_date;
    $inputs['ved_itemid']     = $this->input->post('ved_itemid')[$k];
    $inputs['ved_item']       = $v;
    $inputs['ved_price']      = $this->input->post('ved_price')[$k];
    $inputs['ved_qty']        = $this->input->post('ved_qty')[$k];

    $inputs['ved_gtotal']     = $this->input->post('ved_total')[$k];
    $this->db->insert('voucher_entry_detail',$inputs);
  } 
       print_r($opbillid);
}

}