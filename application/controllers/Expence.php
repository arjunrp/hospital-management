<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Expence extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Expence_model','Expence',TRUE);
		$this->load->model('Purchase_model','Purchase',TRUE);
		$this->template->set('title','Expence');
		$this->base_uri 			=	$this->config->item('admin_url')."expence";
	}

	function index()
	{
		$data['page_title']			=	"Expense List";
		$data['output']				=	$this->Expence->list_all();
		// $this->template->load('template','customer',$data);
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('expence_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Expence->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Expense";
		$data['action']				=	$this->base_uri.'/insert';
		$data['ve_id']				=	"";
		$data['ve_bill_no']			=	"";
		$data['ve_vno']			   	=	$this->Expence->getExpenseId();
		$data['ve_date'] 			=	"";
		$data['ve_customer']		=	"";
		$data['ve_apayable']		=	"";
		$data['categories'] 		=	$this->Expence->get_category();
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('expence_add',$data);
      	$this->load->view('templates/footer');  
	}

	function insert()
	{
		 
		$this->tab_groups['ve_bill_no']	    =	$this->input->post('ve_bill_no');
		$this->tab_groups['ve_vno']			=	$this->input->post('ve_vno');
		$expense_date                  		=   $this->input->post('ve_date');
		$expense_date 						=	date("Y-m-d",strtotime($expense_date));
		$this->tab_groups['ve_date']		=	$expense_date;
		$this->tab_groups['ve_customer']	=	$this->input->post('ve_customer');
		$amount_paid                    	=   $this->input->post('ve_apayable');
		$this->tab_groups['ve_apayable']	=	$amount_paid;
		$this->tab_groups['ve_apaid']		=	$amount_paid;
		$this->tab_groups['ve_balance']		=	"0";
		$this->tab_groups['ve_type']		=	"exp";

		$this->Purchase->set_purchase_balance($amount_paid,$expense_date);

		$this->db->insert('voucher_entry',$this->tab_groups);

		$id 								=	$this->db->insert_id();
		$this->session->set_flashdata('Success','Expense Created');
		redirect($this->base_uri);
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Expense";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Expence->get_one_banner($id);
		$data['ve_id']				=	$row->ve_id;
		$data['ve_bill_no']			=	$row->ve_bill_no ;
		$data['ve_vno']			    =	$row->ve_vno;
		$data['ve_date'] 			=	$row->ve_date;
		$data['ve_customer']		=	$row->ve_customer;
		$data['ve_apayable']		=	$row->ve_apayable ;
		$data['categories'] 		=	$this->Expence->get_category();
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('expence_add',$data);
      	$this->load->view('templates/footer');  

	}

	function view($id)
	{
		$data['page_title']			=	"View Expense";
		$row 						=	$this->Expence->get_one_banner($id);
		$data['ve_id']				=	$row->ve_id;
		$data['ve_bill_no']			=	$row->ve_bill_no ;
		$data['ve_vno']			    =	$row->ve_vno;
		$data['ve_date'] 			=	$row->ve_date;
		$data['ve_customer']		=	$row->ve_customer;
		$data['ve_apayable']		=	$row->ve_apayable ;
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('expence_view',$data);
      	$this->load->view('templates/footer');  
	}

	function update()
	{
		$ve_id 							    =	$this->input->post('ve_id');
		  
		$this->tab_groups['ve_bill_no']	    =	$this->input->post('ve_bill_no');
		$this->tab_groups['ve_vno']			=	$this->input->post('ve_vno');
		$expense_date                  		=   $this->input->post('ve_date');
		$expense_date 						=	date("Y-m-d",strtotime($expense_date));
		$this->tab_groups['ve_date']		=	$expense_date;
		$this->tab_groups['ve_customer']	=	$this->input->post('ve_customer');
		$amount_paid                    	=   $this->input->post('ve_apayable');
		$this->tab_groups['ve_apayable']	=	$amount_paid;
		$this->tab_groups['ve_apaid']		=	$amount_paid;
		$this->tab_groups['ve_balance']		=	"0";

		$previuos_apaid = $this->Purchase->get_voucher($ve_id);
		$this->Purchase->dlt_purchase_balance($previuos_apaid,$expense_date);
		 
		$this->db->update('voucher_entry', $this->tab_groups,array('ve_id'=>$ve_id));

		$this->Purchase->up_purchase_balance($amount_paid,$expense_date);

		$this->session->set_flashdata('Success','Expense Updated');
		redirect($this->base_uri);
	}


	function delete($id)
	{


         if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$row 	=	$this->Expence->get_one_banner($id);

			$expense_date = $row->ve_date;
			$previuos_apaid = $this->Purchase->get_voucher($id);

			$this->Purchase->dlt_purchase_balance($previuos_apaid,$expense_date);

			$this->db->delete('voucher_entry',array('ve_id'=>$id));

			$this->session->set_flashdata('Error','User Deleted');
		}
				echo "Record Deleted";

	}
}