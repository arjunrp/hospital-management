

<?php

class Vendors extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->load->helper(array('form', 'url'));
    $this->load->model('Vendor_model');

  }

  public function index() {
    if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    if($this->input->post('submit')=='Submit'){
      $this->Vendor_model->insert_vendor();
      $this->session->set_flashdata('Success','vendor Created');
      redirect('vendors/getVendors');
      
    }
    else{
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('vendor_add');
      $this->load->view('templates/footer');  
    }

  }}
  public  function getVendors(){
    if ($this->session->userdata('username') == '') {
            redirect('login/index/');
        }   else {
    $data['vendors']=$this->Vendor_model->getVendorslist();
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('vendor_list',$data);
    $this->load->view('templates/footer');  
  }}
  public function updateVendor($id){
    if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    if(!empty($id)){
      $data['vendor']=$this->Vendor_model->updateVendor($id);
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('vendor_update',$data);
      $this->load->view('templates/footer'); 
    }
    if($this->input->post('submit')=='Submit')
    {
      $this->Vendor_model->updateVendor();
      $this->session->set_flashdata('Success','Vendor Updated');
      redirect('vendors/getVendors');
    }
  }}
  public function deleteVendor($id){
    if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    $this->Vendor_model->deleteVendor($id);
    $this->session->set_flashdata('Success','Vendor Deleted');
    redirect('vendors/getVendors');
  }}








}

?>
