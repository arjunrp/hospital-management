<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Room extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Room_model','Room',TRUE);
		$this->template->set('title','Rooms');
		$this->base_uri 			=	$this->config->item('admin_url')."room";
	}

	function index()
	{
		$data['page_title']			=	"Rooms List";
		$data['output']				=	$this->Room->list_all();
		 
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('room_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Room->list_all();
	}

	function add()
	{
		$data['page_title']		=	"Add Rooms";
		$data['action']			=	$this->base_uri.'/insert';
		$data['rm_id']			=	"";
		$data['rm_no']			=	$this->Room->get_room_no();
		$data['rm_fees']		=	0;
		$data['rm_nurse']		=	0;
		$data['rm_hc']			=	0;
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('room_add',$data);
      	$this->load->view('templates/footer');  
	}

	function insert()
	{

		$this->tab_groups['rm_no']	     =	$this->input->post('rm_no');
		$this->tab_groups['rm_fees']	 =	$this->input->post('rm_fees');
		$this->tab_groups['rm_nurse']	 =	$this->input->post('rm_nurse');
		$this->tab_groups['rm_hc']	     =	$this->input->post('rm_hc');
		$this->db->insert('room',$this->tab_groups);

		$this->session->set_flashdata('Success','Rooms Created');
		redirect($this->base_uri."/add");
	}

	function edit($id)
	{
		$data['page_title']		=	"Edit Rooms";
		$data['action']			=	$this->base_uri.'/update';
		$row 					=	$this->Room->get_one_banner($id);
		$data['rm_id']			=	$row->rm_id;
		$data['rm_no']			=	$row->rm_no;
		$data['rm_fees']		=	$row->rm_fees;
		$data['rm_nurse']		=	$row->rm_nurse;
		$data['rm_hc']			=	$row->rm_hc;
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('room_add',$data);
      	$this->load->view('templates/footer');  
	}

	function update()
	{
		$id 							=	$this->input->post('rm_id');
		$this->tab_groups['rm_no']	     =	$this->input->post('rm_no');
		$this->tab_groups['rm_fees']	 =	$this->input->post('rm_fees');
		$this->tab_groups['rm_nurse']	 =	$this->input->post('rm_nurse');
		$this->tab_groups['rm_hc']	     =	$this->input->post('rm_hc');
		 
		$this->db->update('room', $this->tab_groups,array('rm_id'=>$id));
		$this->session->set_flashdata('Success','Rooms Updated');
		redirect($this->base_uri);
	}
	function delete($id)
	{

		if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$row 			=	$this->Room->get_one_banner($id);
			$this->db->delete('room',array('rm_id'=>$id));

	 }
				echo "Record Deleted";
	}
}