<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stocktransfer extends MX_Controller 
{
  public $tab_groups;
  public $form_image;

  function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('logged_in'))
    {
      redirect($this->config->item('admin_url'));
    }
    $this->load->model('Stocktransfer_model','Stocktransfer',TRUE);
    $this->load->model('Purchase_model','Purchase',TRUE);
    $this->load->model('Product_model','Product',TRUE);
    $this->template->set('title','Stock Transfer List');
    $this->base_uri       = $this->config->item('admin_url')."stocktransfer";
  }

  function index(){

      $data['page_title']     = "Stock Transfers";
      $data['output']         = $this->Stocktransfer->list_all();
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('stocktransfer_list',$data);
      $this->load->view('templates/footer'); 
  }

  function add(){

    $data['page_title']     = "New Stock Transfer";
    $data['departments']    = $this->Stocktransfer->getDepartment();
    // $data['products']       = $this->Stocktransfer->getProduct();
    $data['stransfer_ID']   = $this->Stocktransfer->getPurchaseId();

     $this->load->view('templates/header');  
     $this->load->view('templates/sidebar');        
     $this->load->view('stocktransfer_add',$data);
     $this->load->view('templates/footer');  
  }

  function edit($id){

    $data['page_title']     = "Edit Stock Transfer";
    $data['stransfers']     = $this->Stocktransfer->getSTransferview($id);
    $data['departments']    = $this->Stocktransfer->getDepartment();
    $data['products']       = $this->Stocktransfer->getProduct();

     $this->load->view('templates/header');  
     $this->load->view('templates/sidebar');        
     $this->load->view('stocktransfer_edit',$data);
     $this->load->view('templates/footer');  
    }

  function getPrint(){
    $pid              = $_GET['pPrintid'];
    $data['company']  = $this->Purchase->getCompany();
    $data['purchase'] = $this->Purchase->getPurchaseview($pid);
    $this->load->view('templates/header');       
    $this->load->view('p_print',$data);
  }

  function  purchase_Report(){
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('purchase_report');
    $this->load->view('templates/footer');
  }

  function stransfer_View($pid){

    $data['page_title']     = "View Stock Transfer";
    $data['stransfers']      = $this->Stocktransfer->getSTransferview($pid);
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('stocktransfer_view',$data);
    $this->load->view('templates/footer');
  }

  function stockAdd(){
    $this->tab_groups['ve_vno']           = $this->input->post('ve_vno');
    $ve_date                              = $this->input->post('ve_date');
    $ve_date                              = date("Y-m-d",strtotime($ve_date));
    $this->tab_groups['ve_date']          = $ve_date;

    $ve_supplier                          = $this->input->post('ve_supplier');
    $this->tab_groups['ve_supplier']      = $ve_supplier;
    $ve_customer                          = $this->input->post('ve_customer');
    $this->tab_groups['ve_customer']      = $ve_customer;
    $this->tab_groups['ve_user']          = $this->input->post('user_type');
    $this->tab_groups['ve_type']          = "st";
    $this->db->insert('voucher_entry',$this->tab_groups);
    $stransfer                            = $this->db->insert_id();

  foreach($this->input->post('st_item') as $k => $v)
  {

    $inputs = array();
    $inputs['ved_veid']       = $stransfer;
    $inputs['ved_date']       = $ve_date;
    $stockpid                 = $this->input->post('st_product_id')[$k];
    $inputs['ved_itemid']     = $stockpid;
    $inputs['ved_item']       = $v;
    $ved_batch                = $this->input->post('st_batch')[$k];
    $inputs['ved_batch']      = $ved_batch;

    $purchase_item_qty        = $this->input->post('st_qty')[$k];
    $unit_item_qty            = $this->input->post('ved_uqty')[$k];

    $purchase_unit_qty        = $purchase_item_qty * $unit_item_qty;
    $inputs['ved_qty']        = $purchase_unit_qty;
    $inputs['ved_uqty']       = $unit_item_qty;

    $inputs['ved_expiry']     = $this->input->post('st_expiry')[$k];
    
    $ved_unit                 = $this->input->post('st_unt')[$k];

    if($ved_unit == "Strip") { $ved_unit = "Str"; }
    else if($ved_unit == "Bottle") { $ved_unit = "Bot"; }
    else if($ved_unit == "Packet") { $ved_unit = "Pkt"; }
    else if($ved_unit == "No.s") { $ved_unit = "No.s"; }
    $inputs['ved_unit']       = $ved_unit;

    $this->db->insert('voucher_entry_detail',$inputs);
    $this->Stocktransfer->set_purchase_stock($stockpid,$purchase_unit_qty,$ve_customer,$ved_batch,$ve_supplier,$ve_date);
  } 

  print_r($stransfer);
  }


    function stransferUpdate(){
    $ve_id                                = $this->input->post('ve_id');
    $ve_date                              = $this->input->post('ve_date');
    
    $this->db->delete('voucher_entry_detail',array('ved_veid'=>$ve_id));

  foreach($this->input->post('st_item') as $k => $v)
  {

    $inputs = array();
    $inputs['ved_veid']       = $ve_id;
    $inputs['ved_date']       = $ve_date;
    $stockpid                 = $this->input->post('st_product_id')[$k];
    $inputs['ved_itemid']     = $stockpid;
    $inputs['ved_item']       = $v;
    $ved_batch                = $this->input->post('st_batch')[$k];
    $inputs['ved_batch']      = $ved_batch;
    $purchase_item_qty        = $this->input->post('st_qty')[$k];
    $unit_item_qty            = $this->input->post('ved_uqty')[$k];
    $inputs['ved_qty']        = $purchase_item_qty;
    $inputs['ved_uqty']       = $unit_item_qty;
    $purchase_unit_qty        = $purchase_item_qty * $unit_item_qty;
    $inputs['ved_expiry']     = $this->input->post('st_expiry')[$k];
    $inputs['ved_unit']       = $this->input->post('st_unt')[$k];

    // $this->Purchase->update_PO($ve_pono,$stockpid,$purchase_item_qty,$ved_price);
    // $this->Purchase->set_purchase_stock($stockpid,$purchase_item_qty,$purchase_date,$ved_batch);

    $this->db->insert('voucher_entry_detail',$inputs);
  } 

  print_r($ve_id);
  }

   function delete($id)
  {

    if($_REQUEST['empid']) {
      $id   = $_REQUEST['empid'];

      $idd = $id;
      $result = $this->Stocktransfer->get_voucher_details($idd);

      foreach ($result as $key => $value) {

        $stockpid             = $value['ved_itemid'];
        $purchase_item_qty    = $value['ved_qty'];
        $ve_date              = $value['ved_date'];
        $ved_batch            = $value['ved_batch'];
        $ve_supplier          = $value['ve_supplier'];
        $ve_customer          = $value['ve_customer'];

        $this->Stocktransfer->delete_stock($stockpid,$purchase_item_qty,$ve_customer,$ved_batch,$ve_supplier,$ve_date);
        
      }
      $this->db->delete('voucher_entry',array('ve_id'=>$id));
      $this->db->delete('voucher_entry_detail',array('ved_veid'=>$id));

   }
        echo "Record Deleted";
  }

  function productGetbydept()
  {
    $department = $this->input->post('ve_supplier');

    $this->db->select('voucher_entry_detail.ved_itemid,voucher_entry_detail.ved_item,voucher_entry_detail.ved_batch,voucher_entry_detail.ved_expiry,product.pd_unit,product.pd_qty,stock.stock_qty,stock.created_at');
    $this->db->from('stock');
    $this->db->join('voucher_entry_detail','voucher_entry_detail.ved_itemid = stock.stock_product_id AND voucher_entry_detail.ved_batch = stock.stock_batch', 'inner');
    $this->db->join('product','voucher_entry_detail.ved_itemid = product.pd_code', 'inner');
    $this->db->group_by('stock.stock_product_id');
    $this->db->group_by('stock.stock_batch');
    $this->db->order_by('stock.created_at','asc');
    $this->db->where('stock.stock_qty > ','0');
    $this->db->where('stock.stock_dept',$department);
    $this->db->where('stock.stock_status','standby');
    // $this->db->limit(1);
    $query=$this->db->get();
    $data['products'] = $query->result_array();
    // print_r($data['products']);
    $this->load->view('autocomplete', $data);
  }


  
}