

<?php

class Subcategory1 extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->load->helper(array('form', 'url'));
    $this->load->model('Subcategory_model');

  }

  function index() {
    if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    if($this->input->post('submit')=='Submit'){
      $this->Subcategory_model->insertSubCategory();
      $this->session->set_flashdata('Success','SubCategory Created');
      redirect('subcategory1/getSubcategory');
    }
    else{
     $data['categories']=$this->Subcategory_model->getCategory();
     $this->load->view('templates/header');  
     $this->load->view('templates/sidebar');        
     $this->load->view('subcategory_add',$data);
     $this->load->view('templates/footer');
     
   }   
 }}
 public function getSubcategory(){
  if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
   $data['subcategories']=$this->Subcategory_model->getSubcategory();
   $this->load->view('templates/header');  
   $this->load->view('templates/sidebar');        
   $this->load->view('subcategory_list',$data);
   $this->load->view('templates/footer');
 }}
 public function updateSubcategory($id){
  if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
  if(!empty($id)){
   $data['categories']=$this->Subcategory_model->getCategory();

   $data['subcategories']=$this->Subcategory_model->updateSubcategory($id);
   $this->load->view('templates/header');  
   $this->load->view('templates/sidebar');        
   $this->load->view('subcategory_update',$data);
   $this->load->view('templates/footer');
 }
 if($this->input->post('submit')=='Submit'){
      $this->Subcategory_model->updateSubcategory();
      $this->session->set_flashdata('Success','Subcategory Updated');
      redirect('subcategory/getSubcategory');
    }

}}
 public function deleteSubcategory($id){
  if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    $this->Subcategory_model->deleteSubcategory($id);
    $this->session->set_flashdata('Success','Subcategory Deleted');
    redirect('subcategory/getSubcategory');
  }}

}

?>
