<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xraytest extends MX_Controller 
{
  public $tab_groups;
  public $form_image;
  public $patient_datas;

  function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('logged_in'))
    {
      redirect($this->config->item('admin_url'));
    }

    $this->load->model('Xraytest_model','Xraytest',TRUE);
    $this->load->model('Scanning_model','Scanning',TRUE);
    $this->load->model('Xrayname_model','Xrayname',TRUE);
    $this->load->model('Purchase_model','Purchase',TRUE);
    $this->load->model('Return_model','Return',TRUE);
    $this->load->model('Booking_token_model','Booking_token',TRUE);
    $this->template->set('title','Investigations');
    $this->base_uri       = $this->config->item('admin_url')."xraytest";
  }

  function index()
  {
    $data['page_head']      = "Xray Test List";
    // $data['page_title']  = "Active IPs";
    $data['output']         = $this->Xraytest->list_all();
     
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('xraytest_list',$data);
      $this->load->view('templates/footer'); 
  }

  function add()
  {
    $data['page_title']   = "New Xray";

    $data['xray_tests']   = $this->Xrayname->get_category();
    $data['patient']      = $this->Scanning->get_patientsop();
    $data['doctors']      = $this->Booking_token->get_doctor();
    $data['ippatient']    = $this->Scanning->get_patientsip();
    $data['xray_no']      = $this->Xraytest->getXrayId();
    // print_r($data['scan_tests']);
     
        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('xraytest_add',$data);
        $this->load->view('templates/footer');  
  }

  function edit($id)
  {
    $data['page_title']   = "Edit Xray Test";
    $data['xray_tests']   = $this->Xrayname->get_category();
    $data['xray_bills']   = $this->Xraytest->getXrayview($id);
    // print_r($data['lab_bills'] );
     
        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('xraytest_edit',$data);
        $this->load->view('templates/footer');  
  }


  function view($id)
  {

    $data['page_title']     = "Xray Bill View";
    $data['xray_bills']     = $this->Xraytest->getXrayview($id);
    // print_r($data['lab_bills']);
     
        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('xraytest_view',$data);
        $this->load->view('templates/footer');
  }

    function getBillPrint(){

    $pid                    = $_GET['Printid'];
    $data['company']        = $this->Purchase->getCompany();
    $data['xray_bills']     = $this->Xraytest->getXrayview($pid);
    // print_r($data['lab_bills'] );
    // $this->load->view('templates/header');       
    $this->load->view('xray_bill_print',$data);
  }

  function xray_price()
  {
    $xname   = $this->input->post('xname');

      $this->db->select('xt_price');
      $this->db->from('xray_test_name');
      $this->db->where('xt_id',$xname);
      $query      = $this->db->get();
      $data   = $query->row()->xt_price;
    print_r($data);
  }

  function get_discharge()
  {
    $ip_mrd   = $this->input->post('ip_mrd');

      $this->db->select('ip_discharge');
      $this->db->from('ip');
      $this->db->where('ip_mrd',$ip_mrd);
      $this->db->order_by('ip_ipno','desc');
      $this->db->limit(1);
      $query      = $this->db->get();
      if($query->num_rows()==0)
      {
        $data = "0";
      }
      else
      {
        $data   = $query->row()->ip_discharge;
      }
    print_r($data);
  }

  function getXraynames($c_id){

    $this->db->select('*');
    $this->db->from('xray_test_name'); 
    $this->db->where('xt_xr_id',$c_id);
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
  }



  function xraytestAdd(){

    $this->tab_groups['ve_vno']           = $this->input->post('ve_vno');
    $ve_date                              = $this->input->post('ve_date');
    $ve_time                              = $this->input->post('ve_time');
    $ve_date                              = date("Y-m-d",strtotime($ve_date));
    $ve_time                              = date("H:i:s",strtotime($ve_time));
    $ve_date                              = $ve_date. " ".$ve_time;
    $this->tab_groups['ve_date']          = date("Y-m-d H:i:s",strtotime($ve_date));
    // exit();
    $this->tab_groups['ve_supplier']      = $this->input->post('ve_supplier');
    $this->tab_groups['ve_customer']      = $this->input->post('ve_customer');
    $ve_mrd                               = $this->input->post('ve_mrd');
    $this->tab_groups['ve_mrd']           = $ve_mrd;
    $this->tab_groups['ve_patient']       = $this->input->post('ve_patient');
    $this->tab_groups['ve_phone']         = $this->input->post('ve_phone');
    $this->tab_groups['ve_doctor']        = $this->input->post('ve_doctor');

    $this->tab_groups['ve_amount']        = $this->input->post('ve_age');
    $this->tab_groups['ve_pono']          = $this->input->post('ve_gender');

    $amount_paid                          = $this->input->post('lab_mrp');
    $this->tab_groups['ve_apayable']      = $amount_paid;
    $ip_op                                = $this->input->post('ip_op');
    $pay_type                             = $this->input->post('payment_type');
    
    if($pay_type=="True")
    {
      $this->tab_groups['ve_status']        = "cr";
    }
    else
    {
      $this->tab_groups['ve_status']        = "cc";
    }

    $this->tab_groups['ve_apaid']         = "0";
    $this->tab_groups['ve_balance']       = $amount_paid;
    $this->tab_groups['ve_pstaus']        = "NP";

    $this->tab_groups['ve_user']          = $this->input->post('user_type');

    $ve_type = "xrayi"; 

    $this->tab_groups['ve_type']          = $ve_type;
    $this->db->insert('voucher_entry',$this->tab_groups);
    $testid                           = $this->db->insert_id();

    if($ve_mrd != "" || $ve_mrd != " ")
    {
      $this->patient_datas['p_phone']          =  $this->input->post('p_phone');
      $this->patient_datas['p_age']            =  $this->input->post('ve_age');
      $this->patient_datas['p_sex']            =  $this->input->post('ve_gender');
      $this->db->update('patient',$this->patient_datas,array('p_mrd_no'=>$ve_mrd));
    }
    

  foreach($this->input->post('ved_item') as $k => $v)
  {

    $inputs  = array();
    $inputs['ved_veid']       = $testid;
    $inputs['ved_date']       = $ve_date;
    $stockpid                 = $this->input->post('ved_itemid')[$k];
    $inputs['ved_itemid']     = $stockpid;
    $inputs['ved_item']       = $v;

    $inputs['ved_total']      = $this->input->post('ved_total')[$k];
    $inputs['ved_gtotal']     = $this->input->post('ved_total')[$k];

    $this->db->insert('voucher_entry_detail',$inputs);


  } 

  print_r($testid);
  }

  function delete($id)
  {

    if($_REQUEST['empid']) {
      $id   = $_REQUEST['empid']; 
      $row           = $this->Scanning->get_one_banner($id);
      $ve_apayable   = $row->ve_apayable;
      $ve_date       = $row->ve_date;
      $ve_status     = $row->ve_status;
      if($ve_status=="acc" || $ve_status=="acr")
      {
        $this->Return->dlt_purchase_balance($ve_apayable,$ve_date);
      }

      $this->db->delete('voucher_entry',array('ve_id'=>$id));
      $this->db->delete('voucher_entry_detail',array('ved_veid'=>$id));

   }
        echo "Record Deleted";
  }

  function get_gender($mrd)
  {

      $this->db->select('p_sex');
      $this->db->from('patient');
      $this->db->where('p_mrd_no',$mrd);
      $query1  = $this->db->get();
      $data  = $query1->result_array();

      if($query1->num_rows()==0)
      { $data ="0"; }
       echo  json_encode($data);
  }

}