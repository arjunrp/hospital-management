<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mrd_room extends MX_Controller 
{
	public $tab_groups;
	public $tab_data;
	public $tab_datas;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->library('pagination');
		$this->load->model('Mrd_room_model','Mrd_room',TRUE);
		$this->load->model('Patientmodel','Patient',TRUE);
		$this->load->model('Users_model','Users',TRUE);
		$this->load->model('Booking_token_model','Booking_token',TRUE);
		$this->load->model('Return_model','Return',TRUE);
		$this->load->model('Purchase_model','Purchase',TRUE);
		$this->template->set('title','Patient');
		$this->base_uri 			=	$this->config->item('admin_url')."mrd_room";
	}

	public function index()
    {

		$data['page_head']	  =	"MRD Transfered List";
		$data['output']      = $this->Mrd_room->list_all();

        $this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('mrd_transfer_list',$data);
    	$this->load->view('templates/footer');
    }

	 public function adjustment()
    {

		$data['page_title']	  =	"MRD Adjustment";
		$data['action']       = $this->base_uri.'/insert';
		$data['doctors']      = $this->Booking_token->get_doctor();
    $this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('mrd_adjustment',$data);
    	$this->load->view('templates/footer');
    }

     public function edit($mr_id)
    {

		$data['page_title']	  =	"MRD Adjustment";
		$data['action']       = $this->base_uri.'/update';
		$data['doctors']      = $this->Booking_token->get_doctor();
		$row   				         = $this->Mrd_room->mrd_adjust($mr_id);
		$data['mr_id']        = $row->mr_id;
		$data['mr_mrd']       = $row->mr_mrd;
		$data['mr_patient']   = $row->p_title." ".$row->p_name;
		$data['mr_phone']     = $row->p_phone;
		$data['mr_doc']       = $row->mr_doc;
		$data['mr_tno']       = $row->mr_tno;
		$data['mr_date']      = $row->mr_date;
		$data['mr_remark']    = $row->mr_remark;
        $this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('mrd_adjust_edit',$data);
    	$this->load->view('templates/footer');
    }

    function insert()
  {
  	$this->tab_groups['mr_tno']         =  $this->input->post('mr_tno');
    $this->tab_groups['mr_mrd']         =  $this->input->post('mr_mrd');
    $this->tab_groups['mr_doc']         =  $this->input->post('mr_doc');
    $this->tab_groups['mr_date']        =  $this->input->post('mr_date');
    $this->tab_groups['mr_remark']      =  $this->input->post('mr_remark');
    $this->tab_groups['mr_status']      =  "2";

    $this->db->insert('mrd_room',$this->tab_groups);


    $this->session->set_flashdata('Success','MRD Transfered');
    redirect('mrd_room');
  }

   function update()
  {
  	$mr_id         						=  $this->input->post('mr_id');
    $this->tab_groups['mr_doc']         =  $this->input->post('mr_doc');
    $this->tab_groups['mr_remark']      =  $this->input->post('mr_remark');
    $this->tab_groups['mr_status']      =  "2";

    $this->db->update('mrd_room',$this->tab_groups,array('mr_id'=>$mr_id));

    $this->session->set_flashdata('Success','MRD Transfered');
    redirect('mrd_room');
  }
    

	public function mrd_request()
	{
		$data['page']				=	"send";
		$data['page_title']			=	"MRD Requests";

		$data["output_m"]  = $this->Mrd_room->list_to_give_m();
    $data["output_e"]  = $this->Mrd_room->list_to_give_e();

		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('mrd_requests',$data);
    	$this->load->view('templates/footer');
	}

	function mrd_take($mr_id)
	{
		$datas['page']				=	"send";
		$datas['page_title']			=	"MRD Requests";

		$row 								     =   $this->Mrd_room->get_one_banner_mrd($mr_id);	


    $data['mr_remark']        =   "Moved to OP";
		$data['mr_status']				=   "2";
		$this->db->update('mrd_room', $data,array('mr_id'=>$mr_id));

		$datas["output_m"] = $this->Mrd_room->list_to_give_m();
    $datas["output_e"] = $this->Mrd_room->list_to_give_e();

		  $this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('mrd_requests',$datas);
    	$this->load->view('templates/footer');

	}

	public function mrd_given()
	{
		$data['page']				=	"receive";
		$data['page_title']			=	"MRD Returns";
		$data["output_m"] = $this->Mrd_room->list_to_get_m();
    $data["output_e"] = $this->Mrd_room->list_to_get_e();

		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('mrd_requests',$data);
    	$this->load->view('templates/footer');
	}

	function mrd_return($mr_id)
	{
		$datas['page']						=	"receive";
		$datas['page_title']				=	"MRD Returns";
		$row 								=   $this->Mrd_room->get_one_banner_mrd($mr_id);	

		$this->db->delete('mrd_room',array('mr_id'=>$mr_id));

		$datas["output_m"] = $this->Mrd_room->list_to_get_m();
    $datas["output_e"] = $this->Mrd_room->list_to_get_e();

		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('mrd_requests',$datas);
    	$this->load->view('templates/footer');

	}

	function get_availability()
  {
      $op_mrd = $this->input->post('op_mrd');

      $this->db->select('mr_mrd');
      $this->db->from('mrd_room');
      $this->db->where('mr_mrd',$op_mrd);
      $this->db->where('mr_status','2');
      $query  = $this->db->get();
      if($query->num_rows()==0)
      {
        $data  = 0;
      }
      else
      {
         $data  = 1;
      }
     
      print_r($data);
  }

  function get_op_no()
  {
      $op_mrd = $this->input->post('op_mrd');

      $this->db->select('op_opno');
      $this->db->from('op');
      $this->db->where('op_mrd',$op_mrd);
      $this->db->order_by('op_date','desc');
      $this->db->limit(1);
      $query  = $this->db->get();
      if($query->num_rows()==0)
      {
        $data  = 0;
      }
      else
      {
         $data  = $query->row()->op_opno;
      }
     
      print_r($data);
  }


}