<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Users_model','Users',TRUE);
		$this->load->model('Patientmodel','Patient',TRUE);
		$this->template->set('title','Users');
		$this->base_uri 			=	$this->config->item('admin_url')."users";
		$this->home_uri 			=	$this->config->item('admin_url')."dashboard";
	}

	function index()
	{
		$user_type = $this->session->type;
		$user_type = $this->Users->getUsertype($user_type);
		if($user_type=="Super User" || $user_type=="Administrator")
		{
		$data['page_title']			=	"Users' List";
		$data['output']				=	$this->Users->list_all();
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('users',$data);
    	$this->load->view('templates/footer'); 
    	}
    	else
    	{
    		redirect($this->home_uri);
    	}
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Users->list_all();
	}



function view($id)
	{
		$data['page_title']			=	"View Users";
		$data['action']				=	$this->base_uri.'/report_filter/'.$id;
		$row 						=	$this->Users->get_one_banner($id);
		$data['u_emp_id']			=	$row->u_emp_id;
		$data['u_name']			    =	$row->u_name;
		$data['u_email']			=	$row->u_email;
		$data['u_password']			=	$row->u_password;
		$data['u_date']				=	$row->u_date;
		$data['u_type'] 			=	$row->u_type;
		$data['u_designation'] 		=	$row->u_designation; 
		$data['u_department']		=	$row->u_department;
		$data['u_fees']			    =	$row->u_fees;
		$data['u_phone']			=	$row->u_phone;
		$data['u_address']			=	$row->u_address;
		$data['u_street']			=	$row->u_street;
		$data['u_city']				=	$row->u_city;
		$data['u_state']			=	$row->u_state;
		$data['u_country']			=	$row->u_country;
		$data['image']				=	$row->u_image;
		$data['u_basic_sal']		=	$row->u_basic_sal;
		$data['u_sal_day']		    =	$row->u_sal_day;
		$data['u_cas_month']		=	$row->u_cas_month;
		


		$citys 						=	$row->u_state;
		$states 					=	$row->u_country;
		$data['cities'] 			=	$this->Patient->get_citys($citys);
		$data['countries'] 		    =	$this->Patient->get_country();
		$data['states'] 		    =	$this->Patient->get_states($states);  
		$data['type']				=	$this->Users->getU_type();
		$data['designation']		=	$this->Users->getDesignation();
		$data['department']			=	$this->Users->getDepartment();
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('users_view',$data);
      	$this->load->view('templates/footer');  
	}


	function add()
	{
		$data['page_title']			=	"Add User";
		$data['action']				=	$this->base_uri.'/insert';
		$data['u_emp_id']			=	$this->Users->get_emp_id();
		$data['u_name']			    =	"";
		$data['u_email']			=	"";
		$data['u_password']			=	"00000000";
		$data['u_date']				=	"";
		$data['u_type'] 			=	"";
		$data['u_designation'] 		=	""; 
		$data['u_department']		=	"";
		$data['u_fees']			    =	"0";
		$data['u_phone']			=	"";
		$data['u_address']			=	"";
		$data['u_street']			=	"";
		$data['u_city']				=	"";
		$data['u_state']			=	"";
		$data['u_country']			=	"";
		$data['u_basic_sal']		=	"";
		$data['u_sal_day']			=	"";
		$data['u_cas_month']		=	"";

		$data['countries']			=	$this->Patient->get_country();
		$data['cities'] 			=	"";
		$data['states'] 		    =	""; 
		$data['type']				=	$this->Users->getU_type();
		$data['designation']		=	$this->Users->getDesignation();
		$data['department']			=	$this->Users->getDepartment();
		
		$data['image']				=	"";
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('users_add',$data);
      	$this->load->view('templates/footer');  
	}

	function insert()
	{
		$this->tab_groups['u_emp_id']	    =	$this->input->post('u_emp_id');
		$this->tab_groups['u_name']	    	=	$this->input->post('u_name');
		$this->tab_groups['u_email']	   	=	$this->input->post('u_email');
		$this->tab_groups['u_password']		=	$this->input->post('u_password');
		$this->tab_groups['u_date']			=	$this->input->post('u_date');
		$this->tab_groups['u_type']	    	=	$this->input->post('u_type');
		$this->tab_groups['u_designation']	=	$this->input->post('u_designation');
		$this->tab_groups['u_department']	=	$this->input->post('u_department');
		$this->tab_groups['u_fees']			=	$this->input->post('u_fees');
		$this->tab_groups['u_phone']		=	$this->input->post('u_phone');
		$this->tab_groups['u_address']	    =	$this->input->post('u_address');
		$this->tab_groups['u_street']	   	=	$this->input->post('u_street');
		$this->tab_groups['u_city']			=	$this->input->post('u_city');
		$this->tab_groups['u_state']		=	$this->input->post('u_state');
		$this->tab_groups['u_country']		=	$this->input->post('u_country');
		$this->tab_groups['u_basic_sal']	=	$this->input->post('u_basic_sal');
		$this->tab_groups['u_sal_day']	   =	$this->input->post('u_sal_day');
		$this->tab_groups['u_cas_month']	=	$this->input->post('u_cas_month');

		
		$this->db->insert('users',$this->tab_groups);

		$id 								=	$this->input->post('u_emp_id');
		$config['upload_path']				=	$this->config->item('image_path').'users/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'200000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ($this->upload->do_upload('image'))
		{		
			
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['u_image']	=	$data['upload_data']['file_name'];
			$this->db->update('users', $this->form_image,array('u_emp_id'=>$id));
			
		}	
		else
		{
		
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
			
		}

		delete_files($this->config->item('image_path').'users/original/');
		$this->session->set_flashdata('Success','User Created');
		redirect($this->base_uri."/add");
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit User";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Users->get_one_banner($id);
		$data['u_emp_id']			=	$row->u_emp_id;
		$data['u_name']			    =	$row->u_name;
		$data['u_email']			=	$row->u_email;
		$data['u_password']			=	$row->u_password;
		$data['u_date']				=	$row->u_date;
		$data['u_type'] 			=	$row->u_type;
		$data['u_designation'] 		=	$row->u_designation; 
		$data['u_department']		=	$row->u_department;
		$data['u_fees']			    =	$row->u_fees;
		$data['u_phone']			=	$row->u_phone;
		$data['u_address']			=	$row->u_address;
		$data['u_street']			=	$row->u_street;
		$data['u_city']				=	$row->u_city;
		$data['u_state']			=	$row->u_state;
		$data['u_country']			=	$row->u_country;
		$data['u_basic_sal']		=	$row->u_basic_sal;
		$data['u_sal_day']		    =	$row->u_sal_day;
		$data['u_cas_month']		=	$row->u_cas_month;




		$citys 						=	$row->u_state;
		$states 					=	$row->u_country;
		$data['cities'] 			=	$this->Patient->get_citys($citys);
		$data['countries'] 		    =	$this->Patient->get_country();
		$data['states'] 		    =	$this->Patient->get_states($states);  
		$data['type']				=	$this->Users->getU_type();
		$data['designation']		=	$this->Users->getDesignation();
		$data['department']			=	$this->Users->getDepartment();
		$data['image']				=	$row->u_image;
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('users_add',$data);
      	$this->load->view('templates/footer');  
		// $this->template->load('template','features_add',$data);
	}

	function update()
	{
		$id 							    =	$this->input->post('u_emp_id');
		$this->tab_groups['u_emp_id']	    =	$id;
		$this->tab_groups['u_name']	    	=	$this->input->post('u_name');
		$this->tab_groups['u_email']	   	=	$this->input->post('u_email');
		$this->tab_groups['u_password']		=	$this->input->post('u_password');
		$this->tab_groups['u_date']			=	$this->input->post('u_date');
		$this->tab_groups['u_type']	    	=	$this->input->post('u_type');
		$this->tab_groups['u_designation']	=	$this->input->post('u_designation');
		$this->tab_groups['u_department']	=	$this->input->post('u_department');
		$this->tab_groups['u_fees']			=	$this->input->post('u_fees');
		$this->tab_groups['u_phone']		=	$this->input->post('u_phone');
		$this->tab_groups['u_address']	    =	$this->input->post('u_address');
		$this->tab_groups['u_street']	   	=	$this->input->post('u_street');
		$this->tab_groups['u_city']			=	$this->input->post('u_city');
		$this->tab_groups['u_state']		=	$this->input->post('u_state');
		$this->tab_groups['u_country']		=	$this->input->post('u_country');
		$this->tab_groups['u_basic_sal']	=	$this->input->post('u_basic_sal');
		$this->tab_groups['u_sal_day']		=	$this->input->post('u_sal_day');
		$this->tab_groups['u_cas_month']	=	$this->input->post('u_cas_month');
		 
		$this->db->update('users', $this->tab_groups,array('u_emp_id'=>$id));

		$config['upload_path']				=	$this->config->item('image_path').'users/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'200000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ($this->upload->do_upload('image'))
		{		
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['u_image']=	$data['upload_data']['file_name'];
			$this->db->update('users', $this->form_image,array('u_emp_id'=>$id));
		}	
		else
		{
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
		}
		delete_files($this->config->item('image_path').'users/original/');
		$this->session->set_flashdata('Success','User Updated');
		redirect($this->base_uri);
	}

	function process_uploaded_image($filename)
	{
		$large_path 	=	$this->config->item('image_path').'users/original/'.$filename;
		$medium_path 	=	$this->config->item('image_path').'users/'.$filename;
		$thumb_path 	=	$this->config->item('image_path').'users/small/'.$filename;
		
		$medium_config['image_library'] 	=	'gd2';
		$medium_config['source_image'] 		=	$large_path;
		$medium_config['new_image'] 		=	$medium_path;
		$medium_config['maintain_ratio'] 	=	TRUE;
		$medium_config['width'] 			=	1600;
		$medium_config['height'] 			=	400;
		$this->load->library('image_lib', $medium_config);
		$this->image_lib->initialize($medium_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		$thumb_config['image_library'] 		=	'gd2';
		$thumb_config['source_image'] 		=	$large_path;
		$thumb_config['new_image'] 			=	$thumb_path;
		$thumb_config['maintain_ratio'] 	=	TRUE;
		$thumb_config['width'] 				=	50;
		$thumb_config['height'] 			=	50;
		$this->load->library('image_lib', $thumb_config);
		$this->image_lib->initialize($thumb_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		return true;    
	}

	function active($id,$status)
	{
		$data 			=	array('status'=>$status);
		$this->db->update('login',$data,array('user_id'=>$id));
		// $this->list_ajax();
		redirect($this->base_uri);
	}

	function delete()
	{
		if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$row 			=	$this->Users->get_one_banner($id);
		  	unlink($this->config->item('image_path').'users/small/'.$row->u_image);
		  	unlink($this->config->item('image_path').'users/'.$row->u_image);
			$this->db->delete('users',array('u_emp_id'=>$id));
		}
				echo "Record Deleted";
	}
}