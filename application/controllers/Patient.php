<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Patient extends MX_Controller 
{
	public $tab_groups;
	public $tab_data;
	public $tab_datas;
	public $form_image;
	public $t_groups;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->library('pagination');
		$this->load->model('Patientmodel','Patient',TRUE);
		$this->load->model('Users_model','Users',TRUE);
		$this->load->model('Sale_model','Sales',TRUE);
		$this->load->model('Return_model','Return',TRUE);
		$this->load->model('Purchase_model','Purchase',TRUE);
		$this->template->set('title','Patient');
		$this->base_uri 			=	$this->config->item('admin_url')."patient";
	}

	 public function index()
    {
        //pagination settings
        
        $params = array();
		$params['page_title']			=	"Patients List";
        $limit_per_page = 50;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->Patient->get_total_name();
 
        if ($total_records > 0) 
        {
            // get current page records
            $params["results"] = $this->Patient->get_current_page_records_name($limit_per_page, $start_index,NULL);
             
            $config['base_url'] = $this->config->item('admin_url')."patient/index";
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;
            $config['attributes'] = array('class' => 'codpage');
             
            $this->pagination->initialize($config);
             
            // build paging links
            $params["links"] = $this->pagination->create_links();
        }

        $this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('patient_list',$params);
    	$this->load->view('templates/footer');
    }
    
    function search()
    {	
    	$pmrd 		=  ($this->input->post("pmrd"))? $this->input->post("pmrd") : "NIL";	
    	$pname 		=  ($this->input->post("pname"))? $this->input->post("pname") : "NIL";	
    	$pphone 	=  ($this->input->post("pphone"))? $this->input->post("pphone") : "NIL";	
    	$paddress 	=  ($this->input->post("paddress"))? $this->input->post("paddress") : "NIL";	
    	$pcareof 	=  ($this->input->post("pcareof"))? $this->input->post("pcareof") : "NIL";
        
        // $search = $pmrd."/".$pname."/".$pphone."/".$paddress;

        // $search = ($this->uri->segment(3)) ? $this->uri->segment(3) : $search;

        $params = array();
		$params['page_title']			=	"Patients List";
        $limit_per_page = 50;

        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $total_records = $this->Patient->get_total($pmrd,$pname,$pphone,$paddress,$pcareof);
 
        if ($total_records > 0) 
        {
        	$params["results"] = $this->Patient->get_current_page_records($limit_per_page, $start_index,$pmrd,$pname,$pphone,$paddress,$pcareof);
             
            $config['base_url']   	= $this->config->item('admin_url')."patient/search/$pmrd/$pname/$pphone/$paddress/$pcareof";
            $config['total_rows'] 	= $total_records;
            $config['per_page']   	= $limit_per_page;
            $config["uri_segment"] 	= 3;
            $config['attributes'] 	= array('class' => 'codpage');
             
            $this->pagination->initialize($config);
             
            // build paging links
            $params["links"] = $this->pagination->create_links();
        }

        $this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('patient_list',$params);
    	$this->load->view('templates/footer');
    }


	

	 function index_copy()
	{
		ini_set('MAX_EXECUTION_TIME', -1);

		$this->load->library('pagination');

		$config['base_url'] = $this->config->item('admin_url');
		$config['total_rows'] = 200;
		$config['per_page'] = 20;

		$this->pagination->initialize($config);

		echo $this->pagination->create_links();


		$data['page_title']			=	"Patients List";
		$data['output']				=	$this->Patient->list_all();
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('patient_list',$data);
    	$this->load->view('templates/footer'); 
	}


	function reg_print($id){
    $row 						=	$this->Patient->get_one_banner($id);

	$data['p_mrd_no']			=	$row->p_mrd_no;
	$data['p_name']				=	$row->p_title." ".$row->p_name;
	$data['p_age'] 				=	$row->p_age;
	$data['p_sex']				=	$row->p_sex;
	$data['p_blood'] 			=	$row->p_blood;
	$data['p_phone']			=	$row->p_phone;
	$data['p_address']			=	$row->p_address;

    // $this->load->view ('templates/header');       
    $this->load->view ('patient_reg_print',$data);
  }

	function view($id)
	{
		$data['page_title']			=	"Patient View";
		$row 						=	$this->Patient->get_one_banner($id);
		$p_renew_date 				= 	date_create($row->p_renew_date);
		$today_date 				= 	date("Y-m-d");
		$today_date 				=	date_create($today_date);
		$diff           			=	date_diff($p_renew_date,$today_date);
		$data['date_count'] 		=	$diff->format("%a");

		$data['p_mrd_no']			=	$row->p_mrd_no;
		$data['p_old_mrd']			=	$row->p_old_mrd;
		$data['p_date']				=	date("d-m-Y",strtotime($row->p_date));
		$data['p_new_date']			=	date("d-m-Y",strtotime($row->p_new_date));
		$data['p_renew_date']		=	date("d-m-Y",strtotime($row->p_renew_date));
		$data['p_title']			=	$row->p_title;
		$data['p_name']				=	$row->p_name;
		$data['p_age'] 				=	$row->p_age;
		// $data['p_dob']				=	date("d-m-Y",strtotime($row->p_dob));
		$data['p_sex']				=	$row->p_sex;
		$data['p_blood'] 			=	$row->p_blood;
		$data['p_phone']			=	$row->p_phone;
		$data['p_email'] 			=	$row->p_email;
		$data['p_address']			=	$row->p_address;
		$data['p_street']			=	$row->p_street;
		$data['p_country']			=	$row->p_country;
		$data['p_city']				=	$row->p_city;
		$data['p_state']			=	$row->p_state;
		$data['p_zip']				=	$row->p_zip;
		$data['p_guardian']			=	$row->p_guardian;
		$data['p_id_type']			=	$row->p_id_type;
		$data['p_id_no']			=	$row->p_id_no;
		$data['p_renew_count']		=	$row->p_renew_count;
		$data['p_renew_days']		=	$row->p_renew_days;
		$data['p_renew_visits']		=	$row->p_renew_visits;
		$data['image']				=	$row->p_image;

		$citys 						=	$row->p_state;
		$states 					=	$row->p_country;
		$data['cities'] 			=	$this->Patient->get_citys($citys);
		$data['countries'] 		    =	$this->Patient->get_country();
		$data['states'] 		    =	$this->Patient->get_states($states); 

		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('patient_view',$data);
      	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Customer->list_all();
	}

	function add()
	    {
// $this->Patient->list_all_merge()
		$data['page_title']			=	"Add Patient";
		$data['output']				=	"0";
		$data['output1']			=	$this->Patient->cancelled_mrd();
		$data['action']				=	$this->base_uri.'/insert';
		$data['action1']			=	$this->base_uri.'/merge';
		$get_ip_add 				=	$_SERVER['REMOTE_ADDR'];
		if($get_ip_add == "192.168.1.204")
		{
			$data['p_mrd_no']			=	$this->Patient->get_mrd_even();
		}

		else
		{
			$data['p_mrd_no']			=	$this->Patient->get_mrd_odd();
		}
		
		$data['p_date']				=	date("Y-m-d");
		$data['p_title']			=	"";
		$data['p_name']				=	"";
		$data['p_age'] 				=	"";
		// $data['p_dob']				=	"";
		$data['p_sex']				=	"";
		$data['p_blood'] 			=	"";
		$data['p_phone']			=	"";
		$data['p_email'] 			=	"";
		$data['p_address']			=	"";
		$data['p_street']			=	"";
		$data['countries']			=	$this->Patient->get_country();
		$data['p_country']			=	"101";
		$data['p_city']				=	"1997";
		$data['p_state']			=	"19";
		$data['p_zip']				=	"";
		$data['p_guardian']			=	"";
		$data['p_id_type']			=	"";
		$data['p_id_no']			=	"";
		$data['p_renew_days']		=	"30";
		$data['p_renew_count']		=	"5";
		$data['p_renew_date']		=	date("Y-m-d");
		$data['p_renew_visits']		=	"0";
		$data['image']				=	"";
		$data['image1']				=	"";	
		$data['p_regfee']			=	$this->Patient->get_reg_fee();

		$citys 						=	"19";
		$states 					=	"101";
		$data['cities'] 			=	$this->Patient->get_citys($citys);
		$data['states'] 		    =	$this->Patient->get_states($states); 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('add_patient',$data);
      	$this->load->view('templates/footer');  
	}

	function insert()
	{
		
		// print_r($ve_vno);
		// exit();
		$title 								=	$this->input->post('p_mrd_no');

		$this->tab_groups['p_mrd_no']		=	$title;
		$this->tab_groups['p_old_mrd']		=	$title;

		$padte								=	$this->input->post('p_date');
		$padte								=	date("Y-m-d",strtotime($padte));
		$this->tab_groups['p_date']			=	$padte;
		$this->tab_groups['p_new_date']		=	$padte;
		$this->tab_groups['p_renew_date']	=	$padte;
		$this->tab_groups['p_title']		=	$this->input->post('p_title');
		$this->tab_groups['p_name']			=	strtoupper($this->input->post('p_name'));
		$this->tab_groups['p_age']			=	$this->input->post('p_age');
		// $p_dob								=	$this->input->post('p_dob');
		// $p_dob								=	date("Y-m-d",strtotime($p_dob));
		// $this->tab_groups['p_dob']			=	$p_dob;
		$this->tab_groups['p_sex']			=	$this->input->post('p_sex');
		$this->tab_groups['p_blood']		=	$this->input->post('p_blood');
		$this->tab_groups['p_phone']		=	$this->input->post('p_phone');
		$this->tab_groups['p_email']		=	$this->input->post('p_email');
		$this->tab_groups['p_address']		=	$this->input->post('p_address');
		$this->tab_groups['p_street']		=	$this->input->post('p_street');
		$this->tab_groups['p_country']		=	$this->input->post('p_country');
		$this->tab_groups['p_city']	    	=	$this->input->post('p_city');
		$this->tab_groups['p_state']		=	$this->input->post('p_state');
		$this->tab_groups['p_zip']	    	=	$this->input->post('p_zip');
		$this->tab_groups['p_guardian']		=	strtoupper($this->input->post('p_guardian'));
		$this->tab_groups['p_id_type']		=	$this->input->post('p_id_type');
		$this->tab_groups['p_id_no']		=	$this->input->post('p_id_no');
		$this->tab_groups['p_renew_days']	=	$this->input->post('p_renew_days');
		$this->tab_groups['p_renew_count']	=	$this->input->post('p_renew_count');
		$this->tab_groups['p_status']		=	"1";

		$p_regfee							=	$this->input->post('p_regfee');
		$this->tab_groups['p_regfee']		=	$p_regfee;

		$this->db->insert('patient',$this->tab_groups);


		$this->Sales->set_sale_balance($p_regfee,$padte);

		$ve_vno 							=	$this->Patient->getPatientRegno();

		$this->tab_data['ve_vno']			=	$ve_vno;
		$this->tab_data['ve_date']			=	$padte;
		$this->tab_data['ve_cash_date']		=	$padte;
		$this->tab_data['ve_supplier']		=	"17";
		$this->tab_data['ve_mrd']			=	$title;
		$this->tab_data['ve_apayable']		=	$p_regfee;
		$this->tab_data['ve_apaid']			=	$p_regfee;
		$this->tab_data['ve_balance']		=	"0";
		$this->tab_data['ve_pstaus']		=	"FP";
		$this->tab_data['ve_user']			=	$this->input->post('user_type');
		$this->tab_data['ve_type']			=	"preg";

		$this->db->insert('voucher_entry',$this->tab_data);

		$id 								=	$title;
		$config['upload_path']				=	$this->config->item('image_path').'patient/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'20000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ($this->upload->do_upload('image'))
		{		
			
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['p_image']	=	$data['upload_data']['file_name'];
			$this->db->update('patient', $this->form_image,array('p_mrd_no'=>$title));
			
		}	
		else
		{
		
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
			
		}

		delete_files($this->config->item('image_path').'patient/original/');
		$this->session->set_flashdata('Success','Patient Created');
		redirect($this->base_uri."/reg_print/".$title);
	}

	function edit($p_mrd_no)
	{
		// $this->Patient->list_all_merge()
		$data['page_title']			=	"Edit Patient";
		$data['output']				=	"1";
		$data['output1']			=	$this->Patient->cancelled_mrd();
		$data['action']				=	$this->base_uri.'/update';
		$data['action1']			=	$this->base_uri.'/merge';
		$row 						=	$this->Patient->get_one_banner($p_mrd_no);
		$data['p_mrd_no']			=	$row->p_mrd_no;
		$data['p_date']				=	$row->p_date;
		$data['p_title']			=	$row->p_title;
		$data['p_name']				=	$row->p_name;
		$data['p_age'] 				=	$row->p_age;
		// $data['p_dob']				=	date("d-m-Y",strtotime($row->p_dob));
		$data['p_sex']				=	$row->p_sex;
		$data['p_blood'] 			=	$row->p_blood;
		$data['p_phone']			=	$row->p_phone;
		$data['p_email'] 			=	$row->p_email;
		$data['p_address']			=	$row->p_address;
		$data['p_street']			=	$row->p_street;
		$data['p_country']			=	$row->p_country;
		$data['p_city']				=	$row->p_city;
		$data['p_state']			=	$row->p_state;
		$data['p_zip']				=	$row->p_zip;
		$data['p_guardian']			=	$row->p_guardian;
		$data['p_id_type']			=	$row->p_id_type;
		$data['p_id_no']			=	$row->p_id_no;
		$data['image']				=	$row->p_image;
		$data['image1']				=	$row->p_image;
		$data['p_regfee']			=	$row->p_regfee;
		$data['p_renew_count']		=	$row->p_renew_count;
		$data['p_renew_days']		=	$row->p_renew_days;
		$data['p_renew_date']		=	$row->p_renew_date;
		$data['p_renew_visits']		=	$row->p_renew_visits;
		$citys 						=	$row->p_state;
		$states 					=	$row->p_country;
		$data['cities'] 			=	$this->Patient->get_citys($citys);
		$data['countries'] 		    =	$this->Patient->get_country();
		$data['states'] 		    =	$this->Patient->get_states($states); 

		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('add_patient',$data);
      	$this->load->view('templates/footer');  
	}

	function update()
	{
		$title 							=	$this->input->post('p_mrd_no');
		$this->tab_groups['p_mrd_no']	=	$title;
		$padte							=	$this->input->post('p_date');
		$padte							=	date("Y-m-d",strtotime($padte));
		$this->tab_groups['p_date']		=	$padte;
		$this->tab_groups['p_title']	=	$this->input->post('p_title');
		$this->tab_groups['p_name']		=	strtoupper($this->input->post('p_name'));
		$this->tab_groups['p_age']		=	$this->input->post('p_age');
		// $p_dob							=	$this->input->post('p_dob');
		// $p_dob							=	date("Y-m-d",strtotime($p_dob));
		// $this->tab_groups['p_dob']		=	$p_dob;
		$this->tab_groups['p_sex']		=	$this->input->post('p_sex');
		$this->tab_groups['p_blood']	=	$this->input->post('p_blood');
		$this->tab_groups['p_phone']	=	$this->input->post('p_phone');
		$p_phone						=	$this->input->post('p_phone');
		$this->tab_groups['p_email']	=	$this->input->post('p_email');
		$this->tab_groups['p_address']	=	$this->input->post('p_address');
		$this->tab_groups['p_street']	=	$this->input->post('p_street');
		$this->tab_groups['p_country']	=	$this->input->post('p_country');
		$this->tab_groups['p_city']	    =	$this->input->post('p_city');
		$this->tab_groups['p_state']	=	$this->input->post('p_state');
		$this->tab_groups['p_zip']	    =	$this->input->post('p_zip');
		$this->tab_groups['p_guardian']	=	strtoupper($this->input->post('p_guardian'));
		$this->tab_groups['p_id_type']	=	$this->input->post('p_id_type');
		$this->tab_groups['p_id_no']	=	$this->input->post('p_id_no');
		$this->tab_groups['p_renew_days']	=	$this->input->post('p_renew_days');
		$this->tab_groups['p_renew_count']	=	$this->input->post('p_renew_count');
		$this->tab_groups['p_renew_date']	=	$this->input->post('p_renew_date');
		$this->tab_groups['p_renew_visits']	=	$this->input->post('p_renew_visits');

		$image1								=	$this->input->post('image1');
		$this->db->update('patient', $this->tab_groups,array('p_mrd_no'=>$title));

		$id 								=	$title;
		$config['upload_path']				=	$this->config->item('image_path').'patient/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'20000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ($this->upload->do_upload('image'))
		{		
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['p_image']	=	$data['upload_data']['file_name'];
			$this->db->update('patient', $this->form_image,array('p_mrd_no'=>$title));
			// unlink($this->config->item('image_path').'patient/small/'.$image1);
		  	// unlink($this->config->item('image_path').'patient/'.$image1);
		}	
		else
		{
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
		}
		delete_files($this->config->item('image_path').'patient/original/');
		$this->session->set_flashdata('Success','Patient Updated');
		redirect($this->base_uri);
	}

	function process_uploaded_image($filename)
	{
		$large_path 	=	$this->config->item('image_path').'patient/original/'.$filename;
		$medium_path 	=	$this->config->item('image_path').'patient/'.$filename;
		$thumb_path 	=	$this->config->item('image_path').'patient/small/'.$filename;
		
		$medium_config['image_library'] 	=	'gd2';
		$medium_config['source_image'] 		=	$large_path;
		$medium_config['new_image'] 		=	$medium_path;
		$medium_config['maintain_ratio'] 	=	TRUE;
		$medium_config['width'] 			=	1600;
		$medium_config['height'] 			=	400;
		$this->load->library('image_lib', $medium_config);
		$this->image_lib->initialize($medium_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		$thumb_config['image_library'] 		=	'gd2';
		$thumb_config['source_image'] 		=	$large_path;
		$thumb_config['new_image'] 			=	$thumb_path;
		$thumb_config['maintain_ratio'] 	=	TRUE;
		$thumb_config['width'] 				=	50;
		$thumb_config['height'] 			=	50;
		$this->load->library('image_lib', $thumb_config);
		$this->image_lib->initialize($thumb_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		return true;    
	}


	function delete($id)
	{

		// if($_REQUEST['empid']) {
			// $id 	=	$_REQUEST['empid'];	
			$row 			=	$this->Patient->get_one_banner($id);
		  	unlink($this->config->item('image_path').'patient/small/'.$row->p_image);
		  	unlink($this->config->item('image_path').'patient/'.$row->p_image);

		  	$amount 	= $row->p_regfee + $row->p_renew_fee;
		  	$reg_date 	= $row->p_date;
		  	$p_id 		= $row->p_id;
      		$this->Return->dlt_purchase_balance($amount,$reg_date);

			$this->db->delete('patient',array('p_id'=>$p_id));
			$this->db->delete('voucher_entry',array('ve_mrd'=>$id,'ve_type'=>'preg'));
			$this->session->set_flashdata('Error','Patient Deleted');
			redirect($this->base_uri);
		// }
	}

	function cancelled($mrd)
	{
		$data['page_title']			=	"Add Patient";
		$data['output']				=	"";
		$data['output1']			=	$this->Patient->cancelled_mrd();
		$data['action']				=	$this->base_uri.'/insert';
		$data['action1']			=	$this->base_uri.'/merge';
		$data['p_mrd_no']			=	$mrd;
		$data['p_date']				=	date("Y-m-d");
		$data['p_title']			=	"";
		$data['p_name']				=	"";
		$data['p_age'] 				=	"";
		// $data['p_dob']				=	"";
		$data['p_sex']				=	"";
		$data['p_blood'] 			=	"";
		$data['p_phone']			=	"";
		$data['p_email'] 			=	"";
		$data['p_address']			=	"";
		$data['p_street']			=	"";
		$data['countries']			=	$this->Patient->get_country();
		$data['p_country']			=	"";
		$data['p_city']				=	"";
		$data['p_state']			=	"";
		$data['p_zip']				=	"";
		$data['p_guardian']			=	"";
		$data['p_id_type']			=	"";
		$data['p_id_no']			=	"";
		$data['p_renew_days']		=	"";
		$data['p_renew_count']		=	"";
		$data['image']				=	"";
		$data['image1']				=	"";	
		$data['p_regfee']			=	$this->Patient->get_reg_fee();

		$data['cities'] 			=	"";
		$data['states'] 		    =	""; 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('add_patient',$data);
      	$this->load->view('templates/footer');  
	}

	function merge()
	{
		$new 						=	$this->input->post('p_mrd_new');
		$old 						=	$this->input->post('p_mrd_old');
		if($new >= $old)
		{
			$p_mrd_new = $new;
			$p_mrd_old = $old;
		}
		else
		{
			$p_mrd_new = $old;
			$p_mrd_old = $new;
		}

		$row 							=	$this->Patient->get_one_banner($p_mrd_old);
		$this->tab_groups['p_new_date']	=	date("Y-m-d");
		$this->tab_groups['p_old_mrd']	=	$p_mrd_old;
		$this->db->update('patient',$this->tab_groups,array('p_mrd_no'=>$p_mrd_new));

		$status = array('p_status' => 0);    
		$this->db->where('p_mrd_no', $p_mrd_old);
		$this->db->update('patient', $status); 

		$this->db->delete('mrd_room',array('mr_mrd'=>$p_mrd_old));

		unlink($this->config->item('image_path').'patient/small/'.$row->p_image);
		unlink($this->config->item('image_path').'patient/'.$row->p_image);
		redirect($this->base_uri);
	}
	function renew($id)
	{

		$row 									=	$this->Patient->get_one_banner($id);
		$p_renewfee								=	$this->Patient->get_renewfee();
		$p_renew_date 							= 	$row->p_renew_date;
		$p_renew_fee 							=	$row->p_renew_fee;
		$today_date 							= 	date("Y-m-d");
			$tab_groups['p_renew_date']			=	$today_date;
			$tab_groups['p_renew_visits']		=	0;
			$tab_groups['p_renew_fee']			=	$p_renew_fee + $p_renewfee;
			$this->db->update('patient', $tab_groups,array('p_mrd_no'=>$id));

			$this->Sales->set_sale_balance($p_renewfee,$today_date);

			$ve_vno 							=	$this->Patient->getPatientRenewno();

		$this->tab_data['ve_vno']			=	$ve_vno;
		$this->tab_data['ve_date']			=	$today_date;
		$this->tab_data['ve_cash_date']		=	$today_date;
		$this->tab_data['ve_supplier']		=	"17";
		$this->tab_data['ve_mrd']			=	$id;
		$this->tab_data['ve_apayable']		=	$p_renewfee;
		$this->tab_data['ve_apaid']			=	$p_renewfee;
		$this->tab_data['ve_balance']		=	"0";
		$this->tab_data['ve_pstaus']		=	"FP";
		// $this->tab_data['ve_user']			=	$this->input->post('user_type');
		$this->tab_data['ve_type']			=	"prenew";

		$this->db->insert('voucher_entry',$this->tab_data);
		redirect($this->config->item('admin_url')."opregister/add");
	}


	function get_state($ctid){
		$query=$this->db->get_where('states',array('country_id'=>$ctid));
		$data=$query->result_array();
    	if($query->num_rows()==0)
    	{ $data ="0"; }
    	echo  json_encode($data);
 		}


	function get_city($ctid)
	{
		$query=$this->db->get_where('cities',array('state_id'=>$ctid));
		$data=$query->result_array();
    	if($query->num_rows()==0)
    	{ $data ="0"; }
    	echo  json_encode($data);
 		}

 		function duplication($ctid)
	{
		$query=$this->db->get_where('patient',array('p_phone'=>$ctid));
		$data=$query->result_array();
    	if($query->num_rows()==0)
    	{ $data = "0"; }
    	echo json_encode($data);
 		}

 		function getPrint($pid){

    	$data['company']      		=   $this->Purchase->getCompany();
    	$data['patients']            = 	$this->Patient->getPatientRgview($pid);
    	// print_r($data['patients']);
    	$this->load->view('patient_print',$data);
  }

  		function getRPrint($pid){

    	$data['company']      		=   $this->Purchase->getCompany();
    	$data['patients']           = 	$this->Patient->getPatientRwview($pid);
    	// print_r($data['patients']);
    	$this->load->view('patientr_print',$data);
  }

  function search_patient()
	    {

		$data['page_title']			=	"Search Patient";

		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('search_patient',$data);
      	$this->load->view('templates/footer');  
	}

	public function mrd_request()
	{
		$data['page']				=	"send";
		$data['page_title']			=	"MRD Requests";

		$data["output"] = $this->Patient->list_to_give();

		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('mrd_requests',$data);
    	$this->load->view('templates/footer');
	}

	function mrd_take($mr_id)
	{
		$datas['page']				=	"send";
		$datas['page_title']			=	"MRD Requests";

		$row 								=   $this->Patient->get_one_banner_mrd($mr_id);	

		$data['mr_department']				=   $row->mr_tdepartment;
		$data['mr_date']					=   $row->mr_tdate;
		$data['mr_status']					=   "2";
		$this->db->update('mrd_room', $data,array('mr_id'=>$mr_id));

		$datas["output"] = $this->Patient->list_to_give();

		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('mrd_requests',$datas);
    	$this->load->view('templates/footer');

	}

	public function mrd_given()
	{
		$data['page']				=	"receive";
		$data['page_title']			=	"MRD Returns";

		$data["output"] = $this->Patient->list_to_get();

		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('mrd_requests',$data);
    	$this->load->view('templates/footer');
	}

	function mrd_return($mr_id)
	{
		$datas['page']				=	"receive";
		$datas['page_title']			=	"MRD Returns";
		$row 								=   $this->Patient->get_one_banner_mrd($mr_id);	

		$data['mr_department']				=   "18";
		$data['mr_tdepartment']				=   "18";
		$data['mr_date']					=   date("Y-m-d");
		$data['mr_tdate']					=   date("Y-m-d");
		$data['mr_status']					=   "1";
		$this->db->update('mrd_room', $data,array('mr_id'=>$mr_id));

		$datas["output"] = $this->Patient->list_to_get();

		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('mrd_requests',$datas);
    	$this->load->view('templates/footer');

	}


}