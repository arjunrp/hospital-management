<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accountreport extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Accountmodel','Account',TRUE);
		$this->load->model('Product_model','Product',TRUE);
		$this->load->model('Inventory_model','Inventory',TRUE);
		$this->load->model('Users_model','Users',TRUE);
		$this->template->set('title','Account Report');
		$this->base_uri 			=	$this->config->item('admin_url')."accountreport";
	}

	function Report()
	{
		$data['page_title']			=	"Dashboard";
		 
		// $this->template->load('template','customer',$data);
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('ac_report',$data);
    	$this->load->view('templates/footer'); 
	}

	function index()
	{
		$data['page_title']			=	"Account Summary Report";
		$data['action']				=	$this->base_uri.'/account_filter';
		$data['action1']			=	"";
		$data['r_type']				=	"5";
		$data['fdate']				=	"";
		$data['tdate']				=	"";
		$data['product']			=	"";
		$data['products']			=	"";
		$data['output']				=	"";
		$data['output1']			=	"";
		$data['brands']		    	=	$this->Product->getBrand();
		// $data['stocks']				=	"";
		
		
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('ac_summaryreport',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Customer->list_all();
	}

	function account_filter()
	{
		$data['page_title']			=	"Account Summary Report";
		$data['action']				=	$this->base_uri.'/account_filter';
		$data['action1']			=	$this->base_uri.'/account_summary_print';
		$fdate						=	$this->input->post('fdate');
		$tdate						=	$this->input->post('tdate');
		$product					=	$this->input->post('product');
		$r_type						=	$this->input->post('r_type');
		
		$data['r_type']				=	$r_type;
		$data['fdate']				=	$fdate;
		$data['tdate']				=	$tdate;
		$data['product']			=	$product;
		if($product == "0" || $product == "br"  || $product == "ty"  || $product == "brty" )
		{
			$data['products']		= "All Products";
		}
		else
		{
		$data['products']			=	$this->Account->list_small($product);
		}

		$data['output']				=	$this->Account->get_account($fdate,$tdate,$product,$r_type);

		$data['output1']			=	$this->Account->get_account_main($fdate,$tdate,$product,$r_type);
		// print_r($data['output1']);
		$data['brands']		    	=	$this->Product->getBrand();
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('ac_summaryreport',$data);
      	$this->load->view('templates/footer');  
	}

function account_summary_print()
	{
		$fdate						=	$this->input->post('fdate1');
		$tdate						=	$this->input->post('tdate1');
		$product					=	$this->input->post('product1');
		$r_type						=	$this->input->post('r_type1');

		$data['r_type']				=	$r_type;
		$data['fdate']				=	$fdate;
		$data['tdate']				=	$tdate;
		$data['product']			=	$product;
		if($product==0)
		{
			$data['products']		= "All Products";
		}
		else
		{
		$data['products']			=	$this->Account->list_small($product);
		}
		$data['output']				=	$this->Account->get_account($fdate,$tdate,$product,$r_type);
		$data['company']  			= 	$this->Account->getCompany();
		$data['output1']			=	$this->Account->get_account_main($fdate,$tdate,$product,$r_type);

		$data['brands']		    	=	$this->Product->getBrand();

		         
      	$this->load->view('ac_sum_print',$data);
      	 
	}



	function acdetail()
	{

		$data['page_title']			=	"Account Detail Report";
		$data['action']				=	$this->base_uri.'/account_filter_detail';
		$data['action1']			=	"";
		$data['r_type']				=	"5";
		$data['fdate']				=	"";
		$data['tdate']				=	"";
		$data['product']			=	"";
		$data['products']			=	"";
		$data['output']				=	"";
		$data['output1']			=	"";
		$data['departments']		=	$this->Account->getDepartment();
		$data['brands']		    	=	$this->Product->getBrand();

		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('ac_detailreport',$data);
    	$this->load->view('templates/footer'); 
	}

	function account_filter_detail()
	{
		$data['page_title']			=	"Account Detail Report";
		$data['action']				=	$this->base_uri.'/account_filter_detail';
		$data['action1']			=	$this->base_uri.'/account_detail_print';
		$fdate						=	$this->input->post('fdate');
		$tdate						=	$this->input->post('tdate');
		$product					=	$this->input->post('product');
		$r_type						=	$this->input->post('r_type');
		$data['r_type']				=	$r_type;
		$data['fdate']				=	$fdate;
		$data['tdate']				=	$tdate;
		$data['product']			=	$product;
		if($product==0)
		{
			$data['products']		= "All Products";
		}
		else
		{
		$data['products']			=	$this->Account->list_small($product);
		}
		

		$data['output']				=	$this->Account->get_account_detail($fdate,$tdate,$product,$r_type);
		$data['output1']			=	$this->Account->get_account_main($fdate,$tdate,$product,$r_type);
		$data['departments']		=	$this->Account->getDepartment();
		$data['brands']		    	=	$this->Product->getBrand();

		// print_r($data['output']);

		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('ac_detailreport',$data);
      	$this->load->view('templates/footer');    
	}

	function account_detail_print()
	{
		$data['page_title']			=	"Account Detail Report";
		$fdate						=	$this->input->post('fdate1');
		$tdate						=	$this->input->post('tdate1');
		$product					=	$this->input->post('product1');
		$r_type						=	$this->input->post('r_type1');
		$data['r_type']				=	$r_type;
		$data['fdate']				=	$fdate;
		$data['tdate']				=	$tdate;
		$data['product']			=	$product;
		if($product==0)
		{
			$data['products']		= "All Products";
		}
		else
		{
		$data['products']			=	$this->Account->list_small($product);
		}

		$data['output']				=	$this->Account->get_account_detail($fdate,$tdate,$product,$r_type);
		$data['output1']			=	$this->Account->get_account_main($fdate,$tdate,$product,$r_type);
		$data['company']  			= 	$this->Inventory->getCompany();       
      	$this->load->view('ac_detailreport_print',$data);  
	}

	function getBproduct($c_id){

    $query=$this->db->get_where('category',array('brandid'=>$c_id));
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
 	}
 	function getGTproduct($c_id,$sc_id,$t_id){
    $query=$this->db->get_where('product',array('groupid'=>$c_id,'subcategoryid'=>$sc_id,'typeid'=>$t_id));
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
 	}
 	function getGproduct($c_id,$sc_id){
    $query=$this->db->get_where('product',array('groupid'=>$c_id,'subcategoryid'=>$sc_id));
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
 	}

 	function getTproduct($c_id,$sc_id){

    $query=$this->db->get_where('product',array('typeid'=>$c_id,'subcategoryid'=>$sc_id));
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
 	}
 	function getTGproduct($c_id,$sc_id,$g_id){

    $query=$this->db->get_where('product',array('typeid'=>$c_id,'subcategoryid'=>$sc_id,'groupid'=>$g_id));
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
 	}

 	function getSproduct($c_id){

    $query=$this->db->get_where('product',array('subcategoryid'=>$c_id));
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
 	}
}