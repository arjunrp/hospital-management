<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Purchase extends MX_Controller 
{
  public $tab_groups;
  public $form_image;

  function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('logged_in'))
    {
      redirect($this->config->item('admin_url'));
    }
    $this->load->model('Purchase_model','Purchase',TRUE);
    $this->load->model('Product_model','Product',TRUE);
    $this->template->set('title','Purchase List');
    $this->base_uri       = $this->config->item('admin_url')."purchase";
  }

  function index(){

      $data['page_title']     = "Purchase List";
      $data['output']   = $this->Purchase->list_all();
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('purchase_list',$data);
      $this->load->view('templates/footer'); 
  }

  function add(){
    $data['page_title']     = "Create Purchase";
    $data['vendors']        =  $this->Product->getSupplier();
    $data['products']       =  $this->Purchase->getProduct();
    $data['purchase_ID']    =  $this->Purchase->getPurchaseId();
    $data['porders']        =  $this->Purchase->getPorders();
    // print_r($data['porders']);

     $this->load->view('templates/header');  
     $this->load->view('templates/sidebar');        
     $this->load->view('purchase_add',$data);
     $this->load->view('templates/footer');  
  }

  function edit($id){

    $data['page_title']       = "Edit Purchase";
    $data['purchases']        = $this->Purchase->getPurchaseview($id);
    $data['suppliers']        = $this->Product->getSupplier();
    $data['products']         = $this->Purchase->getProduct();

     $this->load->view('templates/header');  
     $this->load->view('templates/sidebar');        
     $this->load->view('purchase_edit',$data);
     $this->load->view('templates/footer');  
    }

  function getPrint(){
    $pid              = $_GET['pPrintid'];
    $data['company']  = $this->Purchase->getCompany();
    $data['purchases']     = $this->Purchase->getPurchaseview($pid);
    // $this->load->view('templates/header');       
    $this->load->view('p_print',$data);
  }

  function  purchase_Report(){
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('purchase_report');
    $this->load->view('templates/footer');
  }

  function  purchase_View($pid){

    $data['page_title']     = "View Purchase";
    $data['purchases']     = $this->Purchase->getPurchaseview($pid);
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('purchase_view',$data);
    $this->load->view('templates/footer');
  }

  function purchaseAdd(){

    $this->tab_groups['ve_bill_no']       = $this->input->post('ve_bill_no');
    $this->tab_groups['ve_vno']           = $this->input->post('ve_vno');
    $ve_pono                              = $this->input->post('po_pono');
    $this->tab_groups['ve_pono']          = $ve_pono;
    $purchase_date                        = $this->input->post('ve_date');
    $purchase_date                        = date("Y-m-d",strtotime($purchase_date));
    $this->tab_groups['ve_date']          = $purchase_date;
    $this->tab_groups['ve_supplier']      = $this->input->post('ve_supplier');
    $ve_customer                          = $this->input->post('ve_customer');
    $this->tab_groups['ve_customer']      = $ve_customer;
    $this->tab_groups['ve_amount']        = $this->input->post('sum');
    // $this->tab_groups['ve_discount']      = $this->input->post('discountp');
    $this->tab_groups['ve_discounta']     = $this->input->post('discounta');
    $this->tab_groups['ve_sgst']          = $this->input->post('ve_sgst');
    $this->tab_groups['ve_cgst']          = $this->input->post('ve_cgst');
    $this->tab_groups['ve_gtotal']        = $this->input->post('gtotal');
    $this->tab_groups['ve_apayable']      = $this->input->post('apayable');
    $amount_paid                          = $this->input->post('apaid');
    $amount_balance                       = $this->input->post('balance');
    $this->tab_groups['ve_apaid']         = $amount_paid;
    $this->tab_groups['ve_balance']       = $amount_balance;
    $this->tab_groups['ve_round']         = $this->input->post('roundoff');
    if($amount_balance==0)
    {
      $this->tab_groups['ve_pstaus']      =   "FP";
    }
    else
    {
      $this->tab_groups['ve_pstaus']      =   "NP";
    }
    $this->tab_groups['ve_user']          = $this->input->post('user_type');
    $this->tab_groups['ve_type']          = "p";
    $this->db->insert('voucher_entry',$this->tab_groups);
    $purchaseid                           = $this->db->insert_id();
    $this->Purchase->set_purchase_balance($amount_paid,$purchase_date);


    $delivery_type                        = $this->input->post('delivery_type');
  if($ve_customer=="15") 
  {
    if($delivery_type == "Full")
    {
      $data['po_avail']     = "fd";
      $this->db->update('porder',$data, array('po_pono' => $ve_pono));
    }
    else
    {
      $data['po_avail']     = "nfd";
      $this->db->update('porder',$data, array('po_pono' => $ve_pono));
    }
  }

  foreach($this->input->post('ved_item') as $k => $v)
  {

    $inputs = array();
    $inputs['ved_veid']       = $purchaseid;
    $inputs['ved_date']       = $purchase_date;
    $stockpid                 = $this->input->post('ved_itemid')[$k];
    $inputs['ved_itemid']     = $stockpid;
    $inputs['ved_item']       = $v;
    $ved_batch                = $this->input->post('ved_batch')[$k];
    $inputs['ved_batch']      = $ved_batch;

    $expiry_date              = $this->input->post('ved_expiry')[$k];
    $expiry_date              = date("Y-m-d",strtotime($expiry_date));
    $inputs['ved_expiry']     = $expiry_date;

    $purchase_item_qty        = $this->input->post('ved_qty')[$k];
    $free_item_qty            = $this->input->post('ved_free')[$k];
    $unit_item_qty            = $this->input->post('ved_uqty')[$k];

    $purchase_unit_qty        = $purchase_item_qty * $unit_item_qty;
    $free_unit_qty            = $free_item_qty * $unit_item_qty;
    $inputs['ved_free']       = $free_unit_qty;
    $inputs['ved_qty']        = $purchase_unit_qty;
    $purchased_unit_qty       = $purchase_unit_qty + $free_unit_qty;
    $inputs['ved_uqty']       = $unit_item_qty;

    $ved_price                = $this->input->post('ved_price')[$k];
    $inputs['ved_price']      = $ved_price;
    $inputs['ved_slprice']    = $this->input->post('ved_slprice')[$k];

    $ved_unit                 = $this->input->post('ved_unit')[$k];
    $inputs['ved_unit']       = $ved_unit;
    $inputs['ved_total']      = $this->input->post('ved_total')[$k];
    $inputs['ved_sgstp']      = $this->input->post('ved_sgstp')[$k];
    $inputs['ved_sgsta']      = $this->input->post('ved_sgsta')[$k];
    $inputs['ved_cgstp']      = $this->input->post('ved_cgstp')[$k];
    $inputs['ved_cgsta']      = $this->input->post('ved_cgsta')[$k];
    $inputs['ved_gtotal']     = $this->input->post('ved_gtotal')[$k];

  if($ve_customer=="15") 
  {
    $this->Purchase->update_PO($ve_pono,$stockpid,$purchase_item_qty,$ved_price);
  }
    $this->Purchase->set_purchase_stock($stockpid,$purchased_unit_qty,$purchase_date,$ved_batch,$ve_customer);

    $this->db->insert('voucher_entry_detail',$inputs);

  } 

  print_r($purchaseid);
  }

    function purchaseVUpdate(){
    $upid                               = $_GET['pUpid'];
    $purchase_date                      = $this->input->post('purchase_date');
    $purchase_date                      = date("Y-m-d",strtotime($purchase_date));
    $tab_groups['ve_apaid']             = $this->input->post('ve_apaid');
    $amount_paid                        = $this->input->post('ve_apaid');
    $amount_balance                     = $this->input->post('amount_balance');
    $tab_groups['ve_balance']           = $amount_balance;
    if($amount_balance==0)
    {
      $tab_groups['ve_pstaus']  =   "FP";
    }
    else
    {
      $tab_groups['ve_pstaus']  =   "NP";
    }
      $previuos_apaid = $this->Purchase->get_voucher($upid);
      $this->Purchase->dlt_purchase_balance($previuos_apaid,$purchase_date);

      $this->db->update('voucher_entry',$tab_groups,array('ve_id'=>$upid));
      
      $this->Purchase->up_purchase_balance($amount_paid,$purchase_date);
    }


    function purchaseUpdate($ve_id1){
    $this->tab_groups['ve_bill_no']       = $this->input->post('ve_bill_no');
    $ve_id                                = $this->input->post('ve_id');
    $ve_date                              = $this->input->post('ve_date');
    $ve_pono                              = $this->input->post('po_pono');
    $ve_customer                          = $this->input->post('ve_customer');
    $this->tab_groups['ve_amount']        = $this->input->post('sum');
    $this->tab_groups['ve_discount']      = $this->input->post('discountp');
    $this->tab_groups['ve_discounta']     = $this->input->post('discounta');
    $this->tab_groups['ve_sgst']          = $this->input->post('ve_sgst');
    $this->tab_groups['ve_cgst']          = $this->input->post('ve_cgst');
    $this->tab_groups['ve_gtotal']        = $this->input->post('gtotal');
    $this->tab_groups['ve_apayable']      = $this->input->post('apayable');
    $amount_paid                          = $this->input->post('apaid');
    $amount_balance                       = $this->input->post('balance');
    $this->tab_groups['ve_apaid']         = $amount_paid;
    $this->tab_groups['ve_balance']       = $amount_balance;
    $this->tab_groups['ve_round']         = $this->input->post('roundoff');

    if($amount_balance==0)
    {
      $this->tab_groups['ve_pstaus']      =   "FP";
    }
    else
    {
      $this->tab_groups['ve_pstaus']      =   "NP";
    }
    $this->tab_groups['ve_user']          = $this->input->post('user_type');

    $previuos_apaid = $this->Purchase->get_voucher($ve_id1);
    $this->Purchase->dlt_purchase_balance($previuos_apaid,$ve_date);
        
    $this->db->update('voucher_entry',$this->tab_groups,array('ve_id'=>$ve_id));

    $this->Purchase->up_purchase_balance($amount_paid,$ve_date);

    $result = $this->Purchase->get_voucher_details($ve_id1);

      foreach ($result as $key => $value) {

        $stockpid             = $value['ved_itemid'];
        $purchase_item_qty    = $value['ved_qty'];
        $free_item_qty        = $value['ved_free'];
        $purchase_date        = $value['ved_date'];
        $ved_batch            = $value['ved_batch'];
        $purchased_unit_qty   = $purchase_item_qty + $free_item_qty;

        $this->Purchase->delete_purchase_stock($stockpid,$purchased_unit_qty,$purchase_date,$ved_batch,$ve_customer);
        
      }

  $this->db->delete('voucher_entry_detail',array('ved_veid'=>$ve_id));

  foreach($this->input->post('ved_item') as $k => $v)
  {

    $inputs = array();
    $inputs['ved_veid']       = $ve_id;
    $inputs['ved_date']       = $ve_date;
    $stockpid                 = $this->input->post('ved_itemid')[$k];
    $inputs['ved_itemid']     = $stockpid;
    $inputs['ved_item']       = $v;
    $ved_batch                = $this->input->post('ved_batch')[$k];
    $inputs['ved_batch']      = $ved_batch;
    $expiry_date              = $this->input->post('ved_expiry')[$k];
    $expiry_date              = date("Y-m-d",strtotime($expiry_date));
    $inputs['ved_expiry']     = $expiry_date;

    $purchase_item_qty        = $this->input->post('ved_qty')[$k];
    $free_item_qty            = $this->input->post('ved_free')[$k];
    $purchase_item_qty1       = $purchase_item_qty + $free_item_qty;
    $unit_item_qty            = $this->input->post('ved_uqty')[$k];
    $purchase_unit_qty        = $purchase_item_qty * $unit_item_qty;
    $free_unit_qty            = $free_item_qty * $unit_item_qty;
    $inputs['ved_free']       = $free_unit_qty;
    $inputs['ved_qty']        = $purchase_unit_qty;
    $purchased_unit_qty       = $purchase_unit_qty + $free_unit_qty;
    $inputs['ved_uqty']       = $unit_item_qty;
    
    $ved_price                = $this->input->post('ved_price')[$k];
    $inputs['ved_price']      = $ved_price;
    $inputs['ved_slprice']    = $this->input->post('ved_slprice')[$k];

    $inputs['ved_unit']       = $this->input->post('ved_unit')[$k];
    $inputs['ved_total']      = $this->input->post('ved_total')[$k];
    $inputs['ved_sgstp']      = $this->input->post('ved_sgstp')[$k];
    $inputs['ved_sgsta']      = $this->input->post('ved_sgsta')[$k];
    $inputs['ved_cgstp']      = $this->input->post('ved_cgstp')[$k];
    $inputs['ved_cgsta']      = $this->input->post('ved_cgsta')[$k];
    $inputs['ved_gtotal']     = $this->input->post('ved_gtotal')[$k];

    $this->Purchase->update_PO($ve_pono,$stockpid,$purchase_item_qty1,$ved_price);
    $this->Purchase->update_purchase_stock($stockpid,$purchased_unit_qty,$ve_date,$ved_batch,$ve_customer);

    $this->db->insert('voucher_entry_detail',$inputs);
  } 
  print_r($ve_id);

  }



    function get_porders($sp_id){

    $this->db->select('porder_detail.*,porder.*,product.pd_qty');
    $this->db->from('porder');
    $this->db->join('porder_detail','porder_detail.pod_poid = porder.po_id', 'inner'); 
    $this->db->join('product','porder_detail.pod_item_id = product.pd_code', 'inner'); 
    $this->db->where('porder.po_pono',$sp_id);
    $this->db->where('porder_detail.pod_avail !=',"1");
    $query=$this->db->get();
    $data=$query->result_array();
      if($query->num_rows()==0)
      { $data ="0"; }
      echo  json_encode($data);
    }

    function delete($id)
  {

    if($_REQUEST['empid']) {
      $id   = $_REQUEST['empid']; 

      $result = $this->Purchase->get_voucher_details($id);

      foreach ($result as $key => $value) {

        $stockpid             = $value['ved_itemid'];
        $purchase_item_qty    = $value['ved_qty'];
        $free_item_qty        = $value['ved_free'];
        $purchase_date        = $value['ved_date'];
        $ved_batch            = $value['ved_batch'];
        $purchased_unit_qty   = $purchase_item_qty + $free_item_qty;

        $this->Purchase->delete_purchase_stock($stockpid,$purchased_unit_qty,$purchase_date,$ved_batch,$ve_customer);
        
      }


      $previuos_apaid = $this->Purchase->get_voucher($id);
      $this->Purchase->dlt_purchase_balance($previuos_apaid,$purchase_date);

      $this->db->delete('voucher_entry',array('ve_id'=>$id));

      $this->db->delete('voucher_entry_detail',array('ved_veid'=>$id));

   }
        echo "Record Deleted";
  }

  
}