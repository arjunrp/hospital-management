<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bed extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Bed_model','Bed',TRUE);
		$this->template->set('title','Beds');
		$this->base_uri 			=	$this->config->item('admin_url')."bed";
	}

	function index()
	{
		$data['page_title']			=	"Beds List";
		$data['output']				=	$this->Bed->list_all();
		 
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('bed_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Bed->list_all();
	}

	function add()
	{
		$data['page_title']		=	"Add Beds";
		$data['action']			=	$this->base_uri.'/insert';
		$data['bd_id']			=	"";
		$data['bd_no']			=	"";
		$data['rm_no']			=	$this->Bed->get_room_no();
		$data['bd_rmid']		=	"";
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('bed_add',$data);
      	$this->load->view('templates/footer');  
	}

	function add1()
	{
		$data['page_title']		=	"Add Beds";
		$data['action']			=	$this->base_uri.'/insert';
		$data['bd_id']			=	"";
		$data['bd_no']			=	$this->Bed->get_bed_no();
		$data['rm_no']			=	$this->Bed->get_room_no();
		$data['company']  		= $this->Bed->getCompany();
		$data['bd_rmid']		=	"";
		 
		$this->load->view('templates/header');  
      	// $this->load->view('templates/sidebar');        
      	$this->load->view('discharge_print',$data);
      	// $this->load->view('templates/footer');  
	}

	function insert()
	{

		$this->tab_groups['bd_no']	 =	$this->input->post('bd_no');
		$this->tab_groups['bd_rmid'] =	$this->input->post('bd_rmid');
		$this->db->insert('bed',$this->tab_groups);

		$this->session->set_flashdata('Success','Beds Created');
		redirect($this->base_uri."/add");
	}

	function edit($id)
	{
		$data['page_title']		=	"Edit Beds";
		$data['action']			=	$this->base_uri.'/update';
		$row 					=	$this->Bed->get_one_banner($id);
		$data['bd_id']			=	$row->bd_id;
		$data['bd_no']			=	$row->bd_no;
		$data['bd_rmid']		=	$row->bd_rmid;
		$data['rm_no']			=	$this->Bed->get_room_no();
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('bed_add',$data);
      	$this->load->view('templates/footer');  
	}

	function update()
	{
		$id 							=	$this->input->post('bd_id');
		$this->tab_groups['bd_no']	     =	$this->input->post('bd_no');
		$this->tab_groups['bd_rmid']	 =	$this->input->post('bd_rmid');
		 
		$this->db->update('bed', $this->tab_groups,array('bd_id'=>$id));
		$this->session->set_flashdata('Success','Beds Updated');
		redirect($this->base_uri);
	}
	function delete($id)
	{

		if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$row 			=	$this->Bed->get_one_banner($id);
			$this->db->delete('bed',array('bd_id'=>$id));

	 }
				echo "Record Deleted";
	}

	function get_beds()
  {
      $room = $this->input->post('room');
      $this->db->select('*');
      $this->db->from('bed');
      $this->db->where('bd_rmid',$room);
      $query1  = $this->db->get();
      $data  = $query1->result_array();

      if($query1->num_rows()==0)
      { $data ="0"; }
 

    echo  json_encode($data);
  }
}