<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sales extends MX_Controller 
{
  public $tab_groups;
  public $form_image;

  function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('logged_in'))
    {
      redirect($this->config->item('admin_url'));
    }
    $this->load->library('pagination');
    $this->load->model('Sale_model','Sales',TRUE);
    $this->load->model('Purchase_model','Purchase',TRUE);
    $this->load->model('Return_model','Return',TRUE);
    $this->load->model('Stock_model','Stock',TRUE);
    $this->load->model('Investigation_model','Investigation',TRUE);
    $this->load->model('Booking_token_model','Booking_token',TRUE);
    $this->template->set('title','Sales');
    $this->base_uri       = $this->config->item('admin_url')."sales";
  }

   public function index()
    {
        //pagination settings
        $params = array();
        $params['page_title']     = "Sales";
        $limit_per_page           = 15;
        $start_index              = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records            = $this->Sales->get_total_name();
 
        if ($total_records > 0) 
        {
            // get current page records
            $params["results"]      = $this->Sales->get_current_page_records_name($limit_per_page, $start_index,NULL);
             
            $config['base_url']     = $this->config->item('admin_url')."sales/index";
            $config['total_rows']   = $total_records;
            $config['per_page']     = $limit_per_page;
            $config["uri_segment"]  = 3;
            $config['attributes']   = array('class' => 'codpage');
             
            $this->pagination->initialize($config);
             
            // build paging links
            $params["links"] = $this->pagination->create_links();
        }

        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('sale_list',$params);
        $this->load->view('templates/footer');
    }
    
    function search()
    { 
      $bill_no         =  ($this->input->post("bno"))? $this->input->post("bno") : "NIL";  
      $bill_date       =  ($this->input->post("bdate"))? $this->input->post("bdate") : "NIL";  
        
        // $search = $pmrd."/".$pname."/".$pphone."/".$paddress;

        // $search = ($this->uri->segment(3)) ? $this->uri->segment(3) : $search;

        $params = array();
        $params['page_title']     = "Sales";
        $limit_per_page           = 15;

        $start_index              = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $total_records            = $this->Sales->get_total($bill_no,$bill_date);
 
        if ($total_records > 0) 
        {
          $params["results"] = $this->Sales->get_current_page_records($limit_per_page, $start_index,$bill_no,$bill_date);
             
            $config['base_url']     = $this->config->item('admin_url')."sales/search/$bill_no/$bill_date";
            $config['total_rows']   = $total_records;
            $config['per_page']     = $limit_per_page;
            $config["uri_segment"]  = 3;
            $config['attributes']   = array('class' => 'codpage');
             
            $this->pagination->initialize($config);
             
            // build paging links
            $params["links"] = $this->pagination->create_links();
        }

        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('sale_list',$params);
        $this->load->view('templates/footer');
    }

  function index_copy()
  {

    $data['page_title']     = "Sales List";
    $data['output']   = $this->Sales->list_all();

    // $data['sale_detail']=$this->Sales->getAbout();
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('sale_list',$data);
    $this->load->view('templates/footer'); 
  }

  function pendinglist()
  {

    $data['page_title']     = "Sales Pending List";
    $data['output']         = $this->Sales->list_all_pending();

    // $data['sale_detail']=$this->Sales->getAbout();
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('sale_list_pending',$data);
    $this->load->view('templates/footer'); 
  }


  function add(){

      $data['page_title']   = "Create Sale";
      $get_ip_add           = $_SERVER['REMOTE_ADDR'];
      $data['patient']      = $this->Investigation->get_patientsop();
      $data['ippatient']    = $this->Sales->get_patientsip();
      $data['products']     = $this->Sales->getProduct();
      $data['sale_ID']      = $this->Sales->getSaleId();
      // if($get_ip_add != "192.168.1.207")
      // {
      //   $data['sale_ID']     = $this->Sales->getSaleId_even();
      // }
      // else
      // {
      //   $data['sale_ID']     = $this->Sales->getSaleId_odd();
      // }
      $data['doctors']      = $this->Booking_token->get_doctor();


      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('sale_add',$data);
      $this->load->view('templates/footer'); 
  }

  function sale_Report(){

    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('sale_report');
    $this->load->view('templates/footer');
  }

  function sale_View($sid){

    $data['sales']     = $this->Sales->getSaleview($sid);
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('sale_view', $data);
    $this->load->view('templates/footer');  
  }

    function sale_PAdd($sid){
    $data['page_title']   = "Create Sale";
    $get_ip_add           = $_SERVER['REMOTE_ADDR'];  
    $data['sale_ID']      = $this->Sales->getSaleId();
      // if($get_ip_add != "192.168.1.207")
      // {
      //   $data['sale_ID']     = $this->Sales->getSaleId_even();
      // }
      // else
      // {
      //   $data['sale_ID']     = $this->Sales->getSaleId_odd();
      // }

    $data['patient']      = $this->Investigation->get_patientsop();
    $data['ippatient']    = $this->Sales->get_patientsip();
    $data['products']     = $this->Sales->getProduct();
    $data['doctors']      = $this->Booking_token->get_doctor();
    $data['sales']        = $this->Sales->getSalePview($sid);

    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('sale_add_pending', $data);
    $this->load->view('templates/footer');  
  }


  function sale_PView($sid){

    $data['sales']     = $this->Sales->getSalePview($sid);
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('sale_view_pending', $data);
    $this->load->view('templates/footer');  
  }


  function getPrint(){
  $sid              = $_GET['sPrintid'];
  $data['company']  = $this->Purchase->getCompany();
  $data['sales']    = $this->Sales->getSaleview($sid);
  // print_r($data['sales']);
  // $this->load->view('templates/header');  
          
  $this->load->view('s_print',$data);
  }

  function saleAdd(){

    $ve_vno                               = $this->input->post('ve_vno');
    $this->tab_groups['ve_vno']           = $ve_vno;
    $sale_time                            = date("H:i:s");
    $sale_date1                           = $this->input->post('ve_date');
    $sale_date                            = $sale_date1." ".$sale_time;
    $sale_date                            = date("Y-m-d H:i:s",strtotime($sale_date));
    $this->tab_groups['ve_date']          = $sale_date;
    $this->tab_groups['ve_supplier']      = $this->input->post('ve_supplier');
    $this->tab_groups['ve_mrd']           = $this->input->post('ve_mrd');
    $ve_customer                          = "3";
    $this->tab_groups['ve_customer']      = $this->input->post('ve_customer');
    $this->tab_groups['ve_patient']       = $this->input->post('ve_patient');
    $this->tab_groups['ve_phone']         = $this->input->post('ve_phone');
    $this->tab_groups['ve_doctor']        = $this->input->post('ve_doctor');
    $this->tab_groups['ve_amount']        = $this->input->post('sum');
    $this->tab_groups['ve_discount']      = $this->input->post('discountp');
    $this->tab_groups['ve_discounta']     = $this->input->post('discounta');
    $this->tab_groups['ve_sgst']          = $this->input->post('ve_sgst');
    $this->tab_groups['ve_cgst']          = $this->input->post('ve_cgst');
    $this->tab_groups['ve_gtotal']        = $this->input->post('gtotal');
    $this->tab_groups['ve_apayable']      = $this->input->post('apayable');
    $this->tab_groups['ve_apaid']         = "0";
    $this->tab_groups['ve_balance']       = $this->input->post('apayable');
    $this->tab_groups['ve_round']         = $this->input->post('roundoff');

    $pay_type                             = $this->input->post('payment_type');
    $ved_stype                            = $this->input->post('ved_stype');
    if($ved_stype == "pen")
    {
      $ve_id          = $this->input->post('ve_id');
      $result         = $this->Sales->get_pending_details($ve_id);
      foreach ($result as $key => $value) {

        $stockpid             = $value['psd_itemid'];
        $purchase_item_qty    = $value['psd_qty'];
        $purchase_date        = $value['ps_date'];
        $ved_batch            = $value['psd_batch'];
        $this->Return->delete_purchase_stock($stockpid,$purchase_item_qty,$purchase_date,$ved_batch,$ve_customer);
      }

      $this->db->delete('pending',array('ps_id'=>$ve_id));
      $this->db->delete('pending_detail',array('psd_psid'=>$ve_id));
      }

      if($pay_type=="True")
      {
        $this->tab_groups['ve_status']        = "cr";
      }
      else
      {
        $this->tab_groups['ve_status']        = "cc";
      }

    $this->tab_groups['ve_pstaus']        =   "NP";

    $this->tab_groups['ve_user']          = $this->input->post('user_type');
    $ip_op                                = $this->input->post('ip_op');
    $this->tab_groups['ve_type']          = "si";
    $this->db->insert('voucher_entry',$this->tab_groups);
    $saleid                           = $this->db->insert_id();
    

  foreach($this->input->post('ved_item') as $k => $v)
  {

    $inputs = array();
    $inputs['ved_veid']       = $saleid;
    $inputs['ved_date']       = $sale_date1;
    $stockpid                 = $this->input->post('ved_itemid')[$k];
    $inputs['ved_itemid']     = $stockpid;
    $inputs['ved_item']       = $v;
    $ved_batch                = $this->input->post('ved_batch')[$k];
    $inputs['ved_batch']      = $ved_batch;
    $inputs['ved_expiry']     = $this->input->post('ved_expiry')[$k];
    $purchase_item_qty        = $this->input->post('ved_qty')[$k];
    $ved_price                = $this->input->post('ved_price')[$k];
    $unit_item_qty            = $this->input->post('ved_uqty')[$k];

    $ved_unit                 = $this->input->post('ved_unit')[$k];
    $inputs['ved_unit']       = $ved_unit;
    if($ved_unit != "No.s")
    {
      $purchase_unit_qty      = $purchase_item_qty * $unit_item_qty;
    }
    else
    {
      $purchase_unit_qty      = $purchase_item_qty;
    }
    $inputs['ved_qty']        = $purchase_unit_qty;
    $inputs['ved_uqty']       = $unit_item_qty;
    $inputs['ved_price']      = $ved_price;
    $inputs['ved_total']      = $this->input->post('ved_total')[$k];
    $inputs['ved_sgstp']      = $this->input->post('ved_sgstp')[$k];
    $inputs['ved_sgsta']      = $this->input->post('ved_sgsta')[$k];
    $inputs['ved_cgstp']      = $this->input->post('ved_cgstp')[$k];
    $inputs['ved_cgsta']      = $this->input->post('ved_cgsta')[$k];
    $inputs['ved_gtotal']     = $this->input->post('ved_gtotal')[$k];

    $this->Sales->set_sale_stock($stockpid,$purchase_unit_qty,$sale_date1,$ved_batch);

    $this->db->insert('voucher_entry_detail',$inputs);
  } 

  print_r($saleid);
  }

  function salePending(){

    $ve_id                            = $this->input->post('ve_id');
    if($ve_id != "0")
    {
      $sale_date                            = $this->input->post('ve_date');
      $sale_date                            = date("Y-m-d",strtotime($sale_date));
      $this->tab_groups['ps_date']          = $sale_date;
      $this->tab_groups['ps_mrd']           = $this->input->post('ve_mrd');
      $ve_customer                          = "3";
      $this->tab_groups['ps_customer']      = $this->input->post('ve_customer');
      $this->tab_groups['ps_patient']       = $this->input->post('ve_patient');
      $this->tab_groups['ps_phone']         = $this->input->post('ve_phone');
      $this->tab_groups['ps_amount']        = $this->input->post('sum');
      $this->tab_groups['ps_discount']      = $this->input->post('discountp');
      $this->tab_groups['ps_discounta']     = $this->input->post('discounta');
      $this->tab_groups['ps_sgst']          = $this->input->post('ve_sgst');
      $this->tab_groups['ps_cgst']          = $this->input->post('ve_cgst');
      $this->tab_groups['ps_gtotal']        = $this->input->post('gtotal');
      $this->tab_groups['ps_apayable']      = $this->input->post('apayable');
      $this->tab_groups['ps_user']          = $this->input->post('user_type');

      $this->db->update('pending',$this->tab_groups,array('ps_id'=>$ve_id));

      $result = $this->Sales->get_pending_details($ve_id);

      foreach ($result as $key => $value) {
        $stockpid             = $value['psd_itemid'];
        $purchase_item_qty    = $value['psd_qty'];
        $purchase_date        = $value['ps_date'];
        $ved_batch            = $value['psd_batch'];

        $this->Return->delete_purchase_stock($stockpid,$purchase_item_qty,$purchase_date,$ved_batch,$ve_customer);
      }

    $this->db->delete('pending_detail',array('psd_psid'=>$ve_id));

  foreach($this->input->post('ved_item') as $k => $v)
  {

    $inputs = array();
    $inputs['psd_psid']       = $ve_id;
    $stockpid                 = $this->input->post('ved_itemid')[$k];
    $inputs['psd_itemid']     = $stockpid;
    $inputs['psd_item']       = $v;
    $ved_batch                = $this->input->post('ved_batch')[$k];
    $inputs['psd_batch']      = $ved_batch;
    $inputs['psd_expiry']     = $this->input->post('ved_expiry')[$k];
    $purchase_item_qty        = $this->input->post('ved_qty')[$k];
    $ved_price                = $this->input->post('ved_price')[$k];
    $unit_item_qty            = $this->input->post('ved_uqty')[$k];

    $ved_unit                 = $this->input->post('ved_unit')[$k];
    $inputs['psd_unit']       = $ved_unit;
    if($ved_unit != "No.s")
    {
      $purchase_unit_qty      = $purchase_item_qty * $unit_item_qty;
    }
    else
    {
      $purchase_unit_qty      = $purchase_item_qty;
    }
    $inputs['psd_qty']        = $purchase_unit_qty;
    $inputs['psd_uqty']       = $unit_item_qty;
    $inputs['psd_price']      = $ved_price;
    $inputs['psd_total']      = $this->input->post('ved_total')[$k];
    $inputs['psd_sgstp']      = $this->input->post('ved_sgstp')[$k];
    $inputs['psd_sgsta']      = $this->input->post('ved_sgsta')[$k];
    $inputs['psd_cgstp']      = $this->input->post('ved_cgstp')[$k];
    $inputs['psd_cgsta']      = $this->input->post('ved_cgsta')[$k];
    $inputs['psd_gtotal']     = $this->input->post('ved_gtotal')[$k];

    $this->Sales->set_sale_stock($stockpid,$purchase_unit_qty,$sale_date,$ved_batch);

    $this->db->insert('pending_detail',$inputs);
    }
    }

    else
    {
    // $ve_vno                            = $this->input->post('ve_vno');
    // $this->tab_groups['ps_vno']        = $ve_vno;
    $sale_date                            = $this->input->post('ve_date');
    $sale_date                            = date("Y-m-d",strtotime($sale_date));
    $this->tab_groups['ps_date']          = $sale_date;
    $this->tab_groups['ps_mrd']           = $this->input->post('ve_mrd');
    $this->tab_groups['ps_customer']      = $this->input->post('ve_customer');
    $this->tab_groups['ps_patient']       = $this->input->post('ve_patient');
    $this->tab_groups['ps_phone']         = $this->input->post('ve_phone');
    $this->tab_groups['ps_amount']        = $this->input->post('sum');
    $this->tab_groups['ps_discount']      = $this->input->post('discountp');
    $this->tab_groups['ps_discounta']     = $this->input->post('discounta');
    $this->tab_groups['ps_sgst']          = $this->input->post('ve_sgst');
    $this->tab_groups['ps_cgst']          = $this->input->post('ve_cgst');
    $this->tab_groups['ps_gtotal']        = $this->input->post('gtotal');
    $this->tab_groups['ps_apayable']      = $this->input->post('apayable');

    $this->tab_groups['ps_user']          = $this->input->post('user_type');
    $this->db->insert('pending',$this->tab_groups);
    $saleid                           = $this->db->insert_id();
    

  foreach($this->input->post('ved_item') as $k => $v)
  {

    $inputs = array();
    $inputs['psd_psid']       = $saleid;
    $stockpid                 = $this->input->post('ved_itemid')[$k];
    $inputs['psd_itemid']     = $stockpid;
    $inputs['psd_item']       = $v;
    $ved_batch                = $this->input->post('ved_batch')[$k];
    $inputs['psd_batch']      = $ved_batch;
    $inputs['psd_expiry']     = $this->input->post('ved_expiry')[$k];
    $purchase_item_qty        = $this->input->post('ved_qty')[$k];
    $ved_price                = $this->input->post('ved_price')[$k];
    $unit_item_qty            = $this->input->post('ved_uqty')[$k];

    $ved_unit                 = $this->input->post('ved_unit')[$k];
    $inputs['psd_unit']       = $ved_unit;
    if($ved_unit != "No.s")
    {
      $purchase_unit_qty      = $purchase_item_qty * $unit_item_qty;
    }
    else
    {
      $purchase_unit_qty      = $purchase_item_qty;
    }
    $inputs['psd_qty']        = $purchase_unit_qty;
    $inputs['psd_uqty']       = $unit_item_qty;
    $inputs['psd_price']      = $ved_price;
    $inputs['psd_total']      = $this->input->post('ved_total')[$k];
    $inputs['psd_sgstp']      = $this->input->post('ved_sgstp')[$k];
    $inputs['psd_sgsta']      = $this->input->post('ved_sgsta')[$k];
    $inputs['psd_cgstp']      = $this->input->post('ved_cgstp')[$k];
    $inputs['psd_cgsta']      = $this->input->post('ved_cgsta')[$k];
    $inputs['psd_gtotal']     = $this->input->post('ved_gtotal')[$k];

    $this->Sales->set_sale_stock($stockpid,$purchase_unit_qty,$sale_date,$ved_batch);

    $this->db->insert('pending_detail',$inputs);
  } 

}

  print_r($saleid);
  }

  function getProduct_unit($pd_id){

    $this->db->select('*');
    $this->db->from('product'); 
    $this->db->where('pd_code',$pd_id);
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
  }

  function getDoctorop(){

    $patient = $this->input->post('patient');

    $this->db->select('users.u_name');
    $this->db->from('op'); 
    $this->db->join('users','users.u_emp_id = op.op_doctor'); 
    $this->db->where('op.op_id',$patient);
    $query=$this->db->get();
    $data=$query->row()->u_name;
    
    if($query->num_rows()==0)
    { $data ="0"; }
    print_r($data);
  }

  function getDoctorip(){

    $patient = $this->input->post('patient');

    $this->db->select('users.u_name');
    $this->db->from('ip'); 
    $this->db->join('users','users.u_emp_id = ip.ip_doctor'); 
    $this->db->where('ip.ip_ipno',$patient);
    $query=$this->db->get();
    $data=$query->row()->u_name;
    if($query->num_rows()==0)
    { $data ="0"; }
    print_r($data);
  }

  function pend_delete($id)
  {

    if($_REQUEST['empid']) {

      $id   = $_REQUEST['empid']; 

      $result = $this->Sales->get_pending_details($id);

      foreach ($result as $key => $value) {

        $stockpid             = $value['psd_itemid'];
        $purchase_item_qty    = $value['psd_qty'];
        $purchase_date        = $value['ps_date'];
        $ved_batch            = $value['psd_batch'];

        $this->Return->delete_purchase_stock($stockpid,$purchase_item_qty,$purchase_date,$ved_batch,$ve_customer);
      }

      $this->db->delete('pending',array('ps_id'=>$id));

      $this->db->delete('pending_detail',array('psd_psid'=>$id));

   }
        echo "Record Deleted";
  }

  function delete($id)
  {

    if($_REQUEST['empid']) {
      
      $id   = $_REQUEST['empid']; 

      $result = $this->Purchase->get_voucher_details($id);

      foreach ($result as $key => $value) {

        $stockpid             = $value['ved_itemid'];
        $purchase_item_qty    = $value['ved_qty'];
        $purchase_date        = $value['ved_date'];
        $ved_batch            = $value['ved_batch'];

        $this->Return->delete_purchase_stock($stockpid,$purchase_item_qty,$purchase_date,$ved_batch,$ve_customer);
      }

      $this->db->delete('voucher_entry',array('ve_id'=>$id));

      $this->db->delete('voucher_entry_detail',array('ved_veid'=>$id));

   }
        echo "Record Deleted";
  }

}