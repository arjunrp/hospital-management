<?php

class Users extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('Users_model');
	}

	function index() {
		if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
		
		$this->load->view('templates/header');  
		$this->load->view('templates/sidebar');        
		$this->load->view('users_add');
		$this->load->view('templates/footer');        

	} }
	function add_user(){
		$id=$this->Users_model->add_user();
		$this->session->set_flashdata('Success','user Created');
		redirect('/users/getUser');
	}

	function getprofile() {
		$data['login']=$this->Users_model->getUser();
		$this->load->view('templates/header');  
		$this->load->view('templates/sidebar');        
		$this->load->view('profile_list',$data);
		$this->load->view('templates/footer');        

	} 


	function getprofile_view($uid) {
		$data['update_users']=$this->Users_model->update_user($uid);
		$this->load->view('templates/header');  
		$this->load->view('templates/sidebar');        
		$this->load->view('profile_view',$data);
		$this->load->view('templates/footer');        

	} 


	function getprivilige() {
		$data['login']=$this->Users_model->getUser();

		$this->load->view('templates/header');  
		$this->load->view('templates/sidebar');        
		$this->load->view('users_list',$data);
		$this->load->view('templates/footer');        
	}




	function getUser() {
		if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
		$data['login']=$this->Users_model->getUser();

		$this->load->view('templates/header');  
		$this->load->view('templates/sidebar');        
		$this->load->view('users_list',$data);
		$this->load->view('templates/footer');        
	}}
	function updateUser($uid){
		
		$data['update_users']=$this->Users_model->update_user($uid);
		$this->load->view('templates/header');  
		$this->load->view('templates/sidebar');        
		$this->load->view('users_update',$data);
		$this->load->view('templates/footer');   
	}
	function update_User(){
		if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {

		$this->Users_model->userUpdate();
		$this->session->set_flashdata('Success','user Updated');
		redirect('/users/getUser');
	} }

	function deleteUser($uid){
		if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
		$this->Users_model->deleteUser($uid);
		$this->session->set_flashdata('Success','user Deleted');
		redirect('/users/getUser');

	}}

}

?>
