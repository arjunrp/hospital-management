<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventory extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Inventory_model','Inventory',TRUE);
		$this->load->model('Product_model','Product',TRUE);
		$this->load->model('Users_model','Users',TRUE);
		$this->template->set('title','Inventory Report');
		$this->base_uri 			=	$this->config->item('admin_url')."inventory";
	}

	function index()
	{
		$data['page_title']			=	"Inventory Summary Report";
		$data['action']				=	$this->base_uri.'/stock_filter';
		$data['action1']			=	"";
		$data['p_dept']				=	"4";
		$data['fdate']				=	"";
		$data['tdate']				=	"";
		$data['product']			=	"";
		$data['products']			=	"";
		$data['output']				=	"";
		$data['brands']		    	=	$this->Product->getBrand();
		$data['departments']		=	$this->Users->getDepartment();
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('in_summary',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Customer->list_all();
	}

	function stock_filter()
	{
		$data['page_title']			=	"Inventory Summary Report";
		$data['action']				=	$this->base_uri.'/stock_filter';
		$data['action1']			=	$this->base_uri.'/stock_print';
		$fdate						=	$this->input->post('fdate');
		$tdate						=	$this->input->post('tdate');
		$product					=	$this->input->post('product');
		$p_dept						=	$this->input->post('p_dept');
		$data['p_dept']				=	$p_dept;
		$data['fdate']				=	$fdate;
		$data['tdate']				=	$tdate;
		$data['product']			=	$product;
		if($product==0)
		{
			$data['products']		= "All Products";
		}
		else
		{
		$data['products']			=	$this->Inventory->list_small($product);
		}
		$data['output']				=	$this->Inventory->get_stock($fdate,$tdate,$product,$p_dept);
		$data['brands']		    	=	$this->Product->getBrand();
		$data['departments']		=	$this->Users->getDepartment();
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('in_summary',$data);
      	$this->load->view('templates/footer');  
	}

	function stock_print()
	{
		$fdate						=	$this->input->post('fdate1');
		$tdate						=	$this->input->post('tdate1');
		$product					=	$this->input->post('product1');
		$r_type						=	$this->input->post('r_type1');
		$data['r_type']				=	$r_type;
		$data['fdate']				=	$fdate;
		$data['tdate']				=	$tdate;
		$data['product']			=	$product;
		if($product==0)
		{
			$data['products']		= "All Products";
		}
		else
		{
		$data['products']			=	$this->Inventory->list_small($product);
		}
		$data['output']				=	$this->Inventory->get_stock($fdate,$tdate,$product,$r_type);
		$data['company']  			= 	$this->Inventory->getCompany();
		$data['categorys']			=	$this->Subcategory->get_category();
		$data['groups']				=	$this->Product->getGroup();
		$data['brands']		    	=	$this->Product->getBrand();
		$data['types']				=	$this->Product->getType();
      
      	$this->load->view('in_sum_print',$data); 
	}

	function indetail()
	{

		$data['page_title']			=	"Inventory Detail Report";
		$data['action']				=	$this->base_uri.'/stock_filter_detail';
		$data['action1']			=	"";
		$data['r_type']				=	"4";
		$data['stocks']				=	"";
		$data['fdate']				=	"";
		$data['tdate']				=	"";
		$data['product']			=	"";
		$data['products']			=	"";
		$data['output']				=	"";
		$data['categorys']			=	$this->Subcategory->get_category();
		$data['groups']				=	$this->Product->getGroup();
		$data['brands']		    	=	$this->Product->getBrand();
		$data['types']				=	$this->Product->getType();
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('in_detail',$data);
    	$this->load->view('templates/footer'); 
	}

	function stock_filter_detail()
	{
		$data['page_title']			=	"Inventory Detail Report";
		$data['action']				=	$this->base_uri.'/stock_filter_detail';
		$data['action1']			=	$this->base_uri.'/stock_print_detail';
		$fdate						=	$this->input->post('fdate');
		$tdate						=	$this->input->post('tdate');
		$product					=	$this->input->post('product');
		$r_type						=	$this->input->post('r_type');
		$data['r_type']				=	$r_type;
		$data['fdate']				=	$fdate;
		$data['tdate']				=	$tdate;
		$data['product']			=	$product;
		if($product==0)
		{
			$data['products']		= "All Products";
		}
		else
		{
		$data['products']			=	$this->Inventory->list_small($product);
		}
		$data['output']				=	$this->Inventory->get_stock_detail($fdate,$tdate,$product,$r_type);
		$data['categorys']			=	$this->Subcategory->get_category();
		$data['groups']				=	$this->Product->getGroup();
		$data['brands']		    	=	$this->Product->getBrand();
		$data['types']				=	$this->Product->getType();
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('in_detail',$data);
      	$this->load->view('templates/footer');  
	}

	function stock_print_detail()
	{
		$data['action']				=	$this->base_uri.'/stock_filter_detail';
		$fdate						=	$this->input->post('fdate1');
		$tdate						=	$this->input->post('tdate1');
		$product					=	$this->input->post('product1');
		$r_type						=	$this->input->post('r_type1');
		$data['r_type']				=	$r_type;
		$data['fdate']				=	$fdate;
		$data['tdate']				=	$tdate;
		$data['product']			=	$product;
		if($product==0)
		{
			$data['products']		= "All Products";
		}
		else
		{
		$data['products']			=	$this->Inventory->list_small($product);
		}
		$data['output']				=	$this->Inventory->get_stock_detail($fdate,$tdate,$product,$r_type);
		$data['categorys']			=	$this->Subcategory->get_category();
		$data['groups']				=	$this->Product->getGroup();
		$data['brands']		    	=	$this->Product->getBrand();
		$data['types']				=	$this->Product->getType();
		 $data['company']  = $this->Inventory->getCompany();
         
      	$this->load->view('in_det_print',$data);
	}

	function getBproduct($c_id){

	$query=$this->db->get_where('product',array('pd_brandid'=>$c_id));
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
 	}

 	function getBTproduct($c_id,$c_id1){

		$this->db->select('*');
		$this->db->from('product');
		$this->db->where('pd_brandid',$c_id);
		$this->db->where('pd_type',$c_id1);
		$query=$this->db->get();
		$data=$query->result_array();

    	if($query->num_rows()==0)
    	{ $data ="0"; }
   		echo  json_encode($data);
   	}


 	function getTproduct($c_id){
    $query=$this->db->get_where('product',array('pd_type'=>$c_id));
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
 	}
 	

}