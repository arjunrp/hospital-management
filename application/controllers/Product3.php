<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends MX_Controller 
{
  public $tab_groups;
  public $form_image;

  function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('logged_in'))
    {
      redirect($this->config->item('admin_url'));
    }
    $this->load->model('Product_model','Product',TRUE);
    $this->template->set('title','Product');
    $this->base_uri       = $this->config->item('admin_url')."product";
  }

  function index()
  {
    $data['page_title']     = "Products List";
    $data['output']       = $this->Product->list_all();
    // $this->template->load('template','customer',$data);
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('product_list',$data);
    $this->load->view('templates/footer');  
  }

  public function list_ajax()
  {
    echo $data['output']    = $this->Product->list_all();
  }

  function add()
  {
    $data['page_title']     = "Add Products";
    $data['action']         = $this->base_uri.'/insert';
    $data['id']             = "";
    $data['pname']          = "";
    $data['pbarcode']       = "";
    $data['psubcategory']   = "";
    $data['pgroup']         = "";
    $data['pmxqty']         = "";
    $data['pcode']          = "";
    $data['pcategory']      = "";
    $data['ptype']          = "";
    $data['pmnqty']         = "";
    $data['price']          = "";
     
    // $this->template->load('template','features_add',$data);
    $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('product_add',$data);
        $this->load->view('templates/footer');  
  }

  function insert()
  {

     
    $this->tab_groups['pname']  = $this->input->post('pname');
    $this->tab_groups['pbarcode']     = $this->input->post('pbarcode');
    $this->tab_groups['psubcategory']      = $this->input->post('psubcategory');
    $this->tab_groups['pgroup']  = $this->input->post('pgroup');
    $this->tab_groups['pmxqty']    = $this->input->post('pmxqty');
    $this->tab_groups['pcode']    = $this->input->post('pcode');
    $this->tab_groups['pcategory']    = $this->input->post('pcategory');
    $this->tab_groups['pmnqty']      = $this->input->post('pmnqty');
    $this->tab_groups['price']  = $this->input->post('price');
    // $this->tab_groups['service_type']    = $this->input->post('stype');
    $this->db->insert('product',$this->tab_groups);

    $id                 = $this->db->insert_id();
    // $config['upload_path']       = $this->config->item('image_path').'features/original/';
    // $config['allowed_types']     = 'gif|jpg|jpeg|png';
    // $config['max_size']          = '20000';
    // @$config['file_name']        = $id.$config['file_ext'];
    // $this->load->library('upload', $config);
    // $this->upload->initialize($config);

    // if ($this->upload->do_upload('image'))
    // {    
      
    //  $data = array('upload_data' => $this->upload->data());
    //  $this->process_uploaded_image($data['upload_data']['file_name']);
    //  $this->form_image['feature_image']  = $data['upload_data']['file_name'];
    //  $this->db->update('features', $this->form_image,array('feature_id'=>$id));
      
    // }  
    // else
    // {
    
    //  $error = $this->upload->display_errors(); 
    //  $this->messages->add($error, 'error');
      
    // }

    // delete_files($this->config->item('image_path').'features/original/');
    redirect($this->base_uri);
  }

  function edit($id)
  {s
    $data['page_title']     = "Edit product";
    $data['action']         = $this->base_uri.'/update';
    $row                    = $this->Product->get_one_banner($id);
    $data['id']             = $row->id;
    $data['pname']          = $row->product;
    $data['pbarcode']       = $row->barcode;
    $data['psubcategory']   = $row->subcategoryid;
    $data['pgroup']         = $row->groupid;
    $data['pmxqty']         = $row->maxquantity;
    $data['pcategory']      = $row->categoryid;
    $data['ptype']          = $row->typeid;
    $data['pmnqty']         = $row->minquantity;
    $data['price']          = $row->price;
    
    $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('product_add',$data);
        $this->load->view('templates/footer');  
    // $this->template->load('template','features_add',$data);
  }

  function update()
  {
    $id                           = $this->input->post('id');
    // $title                        = $this->input->post('customer');
    $this->tab_groups['product']  = $title;
    $this->tab_groups['pname']  = $this->input->post('pname');
    $this->tab_groups['pbarcode']     = $this->input->post('pbarcode');
    $this->tab_groups['psubcategory']      = $this->input->post('psubcategory');
    $this->tab_groups['country']  = $this->input->post('country');
    $this->tab_groups['pgroup']    = $this->input->post('pgroup');
    $this->tab_groups['pmxqty']    = $this->input->post('pmxqty');
    $this->tab_groups['pcategory']    = $this->input->post('pcategory');
    $this->tab_groups['ptype']      = $this->input->post('ptype');
    $this->tab_groups['pmnqty']  = $this->input->post('pmnqty');
    $this->db->update('product', $this->tab_groups,array('id'=>$id));

    // $config['upload_path']       = $this->config->item('image_path').'features/original/';
    // $config['allowed_types']     = 'gif|jpg|jpeg|png';
    // $config['max_size']          = '20000';
    // @$config['file_name']        = $id.$config['file_ext'];
    // $this->load->library('upload', $config);
    // $this->upload->initialize($config);
    // if (  $this->upload->do_upload('image'))
    // {    
    //  $data = array('upload_data' => $this->upload->data());
    //  $this->process_uploaded_image($data['upload_data']['file_name']);
    //  $this->form_image['feature_image']= $data['upload_data']['file_name'];
    //  $this->db->update('features', $this->form_image,array('feature_id'=>$id));
    // }  
    // else
    // {
    //  $error = $this->upload->display_errors(); 
    //  $this->messages->add($error, 'error');
    // }
    // delete_files($this->config->item('image_path').'features/original/');
    redirect($this->base_uri);
  }

  function process_uploaded_image($filename)
  {
    $large_path   = $this->config->item('image_path').'features/original/'.$filename;
    $medium_path  = $this->config->item('image_path').'features/'.$filename;
    $thumb_path   = $this->config->item('image_path').'features/small/'.$filename;
    
    $medium_config['image_library']   = 'gd2';
    $medium_config['source_image']    = $large_path;
    $medium_config['new_image']     = $medium_path;
    $medium_config['maintain_ratio']  = TRUE;
    $medium_config['width']       = 1600;
    $medium_config['height']      = 400;
    $this->load->library('image_lib', $medium_config);
    $this->image_lib->initialize($medium_config);
    if (!$this->image_lib->resize()) {
      echo $this->image_lib->display_errors();
      return false;
    }

    $thumb_config['image_library']    = 'gd2';
    $thumb_config['source_image']     = $large_path;
    $thumb_config['new_image']      = $thumb_path;
    $thumb_config['maintain_ratio']   = TRUE;
    $thumb_config['width']        = 50;
    $thumb_config['height']       = 50;
    $this->load->library('image_lib', $thumb_config);
    $this->image_lib->initialize($thumb_config);
    if (!$this->image_lib->resize()) {
      echo $this->image_lib->display_errors();
      return false;
    }

    return true;    
  }

  function active($id,$status)
  {
    $data       = array('status'=>$status);
    $this->db->update('product',$data,array('id'=>$id));
    $this->list_ajax();
  }

  function delete($id)
  {
    $row      = $this->Product->get_one_banner($id);
    // unlink($this->config->item('image_path').'features/small/'.$row->feature_image);
    // unlink($this->config->item('image_path').'features/'.$row->feature_image);
    $this->db->delete('product',array('id'=>$id));
    $this->list_ajax();
  }
}