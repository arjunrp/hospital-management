<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}	
		$this->load->model('Company_model','Company',TRUE);
		$this->template->set('title','Company');
		$this->base_uri 			=	$this->config->item('admin_url')."company";
	}

	function index()
	{
		$data['page_title']			=	"Company List";
		$data['output']				=	$this->Company->list_all();
		// $this->template->load('template','customer',$data);
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('company_list',$data);
    	$this->load->view('templates/footer'); 
	}

function view()
	{
		$data['page_title']			=	"Company View";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Company->get_one_banners();
		$data['id']					=	$row->id;
		$data['name']			    =	$row->company;
		$data['address']			=	$row->address;
		$data['city']				=	$row->city;
		$data['cities']				=	$this->Company->get_city();
		$data['zip']				=	$row->zip;
		$data['phone']			    =	$row->phone;
		$data['street']			    =	$row->street;
		$data['state']			    =	$row->state;
		$data['states']				=	$this->Company->get_state();
		$data['country']			=	$row->country;
		$data['countries']			=	$this->Company->get_country();
		$data['email']			    =	$row->email;
		$data['gst_no']			    =	$row->gst_no;
		$data['drug_lic']			=	$row->drug_lic;
		$data['website']			=	$row->website;
		$data['tags']			    =	$row->tags;
		$data['image']				=	$row->company_logo;
		$data['reg_fee']			=	$row->reg_fee;
		$data['renew_fee']			=	$row->renew_fee;

		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('company_view',$data);
      	$this->load->view('templates/footer'); 
	}




	public function list_ajax()
	{
		echo $data['output']		=	$this->Company->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Company";
		$data['action']				=	$this->base_uri.'/insert';
		$data['id']					=	"";
		$data['name']			    =	"";
		$data['address']			=	"";
		$data['street']			    =	"";
		$data['cid'] 				=	"";
		$data['city']				=	"";
		$data['zip']			    =	"";
		$data['ctid'] 				=	"";
		$data['countries']			=	$this->Company->get_country();
		$data['stid'] 				=	"";
		$data['state']			    =	"";
		$data['email']			    =	"";
		$data['website']			=	"";
		$data['phone']			    =	"";
		$data['tags']			    =	"";
		$data['image']				=	"";	
		$data['gst_no']			    =	"";
		$data['drug_lic']			=	"";
		$data['reg_fee']			=	"";
		$data['renew_fee']			=	"";


		// $data['type']			=	"";
		// $this->template->load('template','features_add',$data);
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('company_add',$data);
      	$this->load->view('templates/footer');
      	redirect($this->base_uri);  
	}

	function insert()
	{

		 
		$this->tab_groups['company']	=	$this->input->post('vendor');
		$this->tab_groups['address']	=	$this->input->post('address');
		$this->tab_groups['city']		=	$this->input->post('city');
		$this->tab_groups['zip']	    =	$this->input->post('zip');
		$this->tab_groups['phone']		=	$this->input->post('phone');
		$this->tab_groups['website']	=	$this->input->post('website');
		$this->tab_groups['street']		=	$this->input->post('street');
		$this->tab_groups['state']	    =	$this->input->post('state');
		$this->tab_groups['country']	=	$this->input->post('country');
		$this->tab_groups['email']	    =	$this->input->post('email');
		$this->tab_groups['gst_no']	    =	$this->input->post('gst_no');
		$this->tab_groups['drug_lic']	=	$this->input->post('drug_lic');
		$this->tab_groups['tags']	    =	$this->input->post('tags');
		$this->tab_groups['reg_fee']	=	$this->input->post('reg_fee');
		$this->tab_groups['renew_fee']	=	$this->input->post('renew_fee');


		// $this->tab_groups['service_type']		=	$this->input->post('stype');
		$this->db->insert('company',$this->tab_groups);

		$id 								=	$this->db->insert_id();
		$config['upload_path']				=	$this->config->item('image_path').'company/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'20000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ($this->upload->do_upload('image'))
		{		
			
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['company_logo']	=	$data['upload_data']['file_name'];
			$this->db->update('company', $this->form_image,array('id'=>$id));
			
		}	
		else
		{
		
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
			
		}

		delete_files($this->config->item('image_path').'company/original/');
		$this->session->set_flashdata('Success','company Created');
		redirect($this->base_uri."/add");
	}

	function edit()
	{
		$data['page_title']			=	"Edit Company";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Company->get_one_banners();
		$data['id']					=	$row->id;
		$data['name']			    =	$row->company;
		$data['address']			=	$row->address;
		$data['city']				=	$row->city;
		$data['zip']				=	$row->zip;
		$data['phone']			    =	$row->phone;
		$data['street']			    =	$row->street;
		$data['state']			    =	$row->state;
		$data['country']			=	$row->country;
		$data['email']			    =	$row->email;
		$data['website']			=	$row->website;
		$data['image']				=	$row->company_logo;
		$data['gst_no']			    =	$row->gst_no;
		$data['drug_lic']			=	$row->drug_lic;
		$data['tags']			    =	$row->tags;
		$data['reg_fee']			=	$row->reg_fee;
		$data['renew_fee']			=	$row->renew_fee;

        $citys 						=	$row->state;
		$states 					=	$row->country;
		$data['cities'] 			=	$this->Company->get_citys($citys);
		$data['countries'] 		    =	$this->Company->get_country();
		$data['states'] 		    =	$this->Company->get_states($states); 


		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('company_add',$data);
      	$this->load->view('templates/footer'); 

      	 
		// $this->template->load('template','features_add',$data);


	}

	function update()
	{
		$id 							=	$this->input->post('id');
		$this->tab_groups['company']	=	$this->input->post('vendor');
		$this->tab_groups['address']	=	$this->input->post('address');
		$this->tab_groups['city']		=	$this->input->post('city');
		$this->tab_groups['zip']		=	$this->input->post('zip');
		$this->tab_groups['country']	=	$this->input->post('country');
		$this->tab_groups['state']		=	$this->input->post('state');
		$this->tab_groups['phone']		=	$this->input->post('phone');
		$this->tab_groups['email']		=	$this->input->post('email');
		$this->tab_groups['website']	=	$this->input->post('website');
		$this->tab_groups['gst_no']  	=	$this->input->post('gst_no');
		$this->tab_groups['drug_lic']	=	$this->input->post('drug_lic');
		$this->tab_groups['tags']	    =	$this->input->post('tags');
		$this->tab_groups['reg_fee']	=	$this->input->post('reg_fee');
		$this->tab_groups['renew_fee']	=	$this->input->post('renew_fee');
		$this->db->update('company', $this->tab_groups,array('id'=>$id));

		$config['upload_path']				=	$this->config->item('image_path').'company/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'20000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (  $this->upload->do_upload('image'))
		{		
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['company_logo']=	$data['upload_data']['file_name'];
			$this->db->update('company', $this->form_image,array('id'=>$id));
		}	
		else
		{
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
		}
		delete_files($this->config->item('image_path').'company/original/');
		$this->session->set_flashdata('Success','company Updated');
		redirect('company/view');
	}

	function process_uploaded_image($filename)
	{
		$large_path 	=	$this->config->item('image_path').'company/original/'.$filename;
		$medium_path 	=	$this->config->item('image_path').'company/'.$filename;
		$thumb_path 	=	$this->config->item('image_path').'company/small/'.$filename;
		
		$medium_config['image_library'] 	=	'gd2';
		$medium_config['source_image'] 		=	$large_path;
		$medium_config['new_image'] 		=	$medium_path;
		$medium_config['maintain_ratio'] 	=	TRUE;
		$medium_config['width'] 			=	1600;
		$medium_config['height'] 			=	400;
		$this->load->library('image_lib', $medium_config);
		$this->image_lib->initialize($medium_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		$thumb_config['image_library'] 		=	'gd2';
		$thumb_config['source_image'] 		=	$large_path;
		$thumb_config['new_image'] 			=	$thumb_path;
		$thumb_config['maintain_ratio'] 	=	TRUE;
		$thumb_config['width'] 				=	50;
		$thumb_config['height'] 			=	50;
		$this->load->library('image_lib', $thumb_config);
		$this->image_lib->initialize($thumb_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		return true;    
	}

	function active($id,$status)
	{
		$data 			=	array('status'=>$status);
		$this->db->update('company',$data,array('id'=>$id));
		// $this->list_ajax();
		redirect($this->base_uri);
	}

	function delete($id)
	{
		$row 			=	$this->Company->get_one_banners();
		unlink($this->config->item('image_path').'company/small/'.$row->company_logo);
		unlink($this->config->item('image_path').'company/'.$row->company_logo);
		$this->db->delete('company',array('id'=>$id));
		// $this->list_ajax();
		$this->session->set_flashdata('Error','Company Deleted');
		redirect($this->base_uri);
	}

	function get_state($ctid){
		$query=$this->db->get_where('states',array('country_id'=>$ctid));
		$data=$query->result_array();
    	if($query->num_rows()==0)
    	{ $data ="0"; }
    	echo  json_encode($data);
 		}


	function get_city($ctid)
	{
		$query=$this->db->get_where('cities',array('state_id'=>$ctid));
		$data=$query->result_array();
    	if($query->num_rows()==0)
    	{ $data ="0"; }
    	echo  json_encode($data);
 		}
}