<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ipregister extends MX_Controller 
{
  public $tab_groups;
  public $form_image;
  public $patient_datas;

  function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('logged_in'))
    {
      redirect($this->config->item('admin_url'));
    }
    $this->load->model('Ipregister_model','Ipregister',TRUE);
    $this->load->model('Booking_model','Booking',TRUE);
    $this->load->model('Opregister_model','Opregister',TRUE);
    $this->load->model('Users_model','Users',TRUE);
    $this->template->set('title','IP Register');
    $this->base_uri       = $this->config->item('admin_url')."ipregister";
  }

  function index()
  {
    $data['page_head']      = "IP List";
    $data['page_title']     = "Active IPs";
    $data['output']         = $this->Ipregister->list_all();
     
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('ip_list',$data);
      $this->load->view('templates/footer'); 
  }

  public function list_ajax()
  {
    echo $data['output']    = $this->Ipregister->list_all();
  }

  function add()
  {
    $data['page_title']   = "Add IP";
    $data['action']       = $this->base_uri.'/insert';
    $data['departments']  = $this->Ipregister->getDepartment();
    // $data['patient']      = $this->Booking->get_patients();
    $data['cities']       = $this->Booking->get_citys();
    $data['output']       = $this->Ipregister->today_ip();

     
        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('ip_add',$data);
        $this->load->view('templates/footer');  
  }

  function insert()
  {

    $ip_mrd                               =  $this->input->post('ip_mrd');
    $this->tab_groups['ip_mrd']           =  $ip_mrd;
    $this->tab_groups['ip_ipno']          =  $this->input->post('ip_ipno');
    $this->tab_groups['ip_date']          =  $this->input->post('ip_date');
    $ip_time                              =  $this->input->post('ip_time');
    $this->tab_groups['ip_time']          =  date("H:i:s",strtotime($ip_time));
    $this->tab_groups['ip_department  ']  =  $this->input->post('ip_department');
    $this->tab_groups['ip_doctor']        =  $this->input->post('ip_doctor');
    $this->tab_groups['ip_discharge']     =  "0000-00-00";

    $this->db->insert('ip',$this->tab_groups);

    $this->patient_datas['p_phone']          =  $this->input->post('p_phone');
    $this->patient_datas['p_address']        =  $this->input->post('p_address');
    $this->patient_datas['p_street']         =  $this->input->post('p_street');
    $this->patient_datas['p_age']            =  $this->input->post('p_age');

    $this->db->update('patient',$this->patient_datas,array('p_mrd_no'=>$ip_mrd));

    $this->session->set_flashdata('Success','IP Created');
    redirect('ipregister/add');
  }

  function get_ipno()
  {
    $ip_dept   = $this->input->post('ip_dept');

      $this->db->select('ip_ipno');
      $this->db->from('ip');
      $this->db->where('ip_department',$ip_dept);
      $query      = $this->db->get();
      if($query->num_rows()==0)
      {
        $this->db->select('dp_short');
        $this->db->from('department');
        $this->db->where('dp_id',$ip_dept);
        $result      = $this->db->get();
        $data = $result->row()->dp_short."_0001";
      }
      else
      {
        $data   = $query->row()->ip_ipno;
        $data++;
      }
    print_r($data);
  }

  function get_discharge()
  {
    $ip_mrd   = $this->input->post('ip_mrd');

      $this->db->select('ip_discharge');
      $this->db->from('ip');
      $this->db->where('ip_mrd',$ip_mrd);
      $this->db->order_by('ip_ipno','desc');
      $this->db->limit(1);
      $query      = $this->db->get();
      if($query->num_rows()==0)
      {
        $data = "0";
      }
      else
      {
        $data   = $query->row()->ip_discharge;
      }
    print_r($data);
  }

  function getDoctors($c_id){

    $this->db->select('users.*');
    $this->db->from('users');
    $this->db->join('user_type','user_type.ut_id = users.u_type', 'inner'); 
    $this->db->where('users.u_department',$c_id);
    $this->db->where('user_type.ut_user_type','Doctor');
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
  }

  function edit($id)
  {
    $data['page_title']     = "Edit OP";
    $data['action']         = $this->base_uri.'/update';
    $row                    = $this->Ipregister->get_one_banner($id);

    $data['ip_id']          = $id;
    $ip_mrd                 = $row->ip_mrd;
    $data['ip_mrd']         = $ip_mrd;
    $data['ip_ipno']        = $row->ip_ipno;
    $data['ip_date']        = $row->ip_date;
    $ip_department          = $row->ip_department;
    $data['ip_department']  = $row->ip_department;
    $data['ip_doctor']      = $row->ip_doctor;

    $row1                   = $this->Opregister->get_patient($ip_mrd);
    $data['bk_name']        = $row1->p_name;
    $data['bk_phone']       = $row1->p_phone;
    $data['image']          = $row1->p_image;


    $data['departments']    = $this->Ipregister->getDepartment();
    $data['patient']        = $this->Booking->get_patients();
    $data['doctors']        = $this->Ipregister->getDoctors($ip_department);
     
        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('ip_edit',$data);
        $this->load->view('templates/footer');  
  }

  function update()
  {

    $id                                   =  $this->input->post('ip_id');
    $this->tab_groups['ip_mrd']           =  $this->input->post('ip_mrd');
    $this->tab_groups['ip_ipno']          =  $this->input->post('ip_ipno');
    $this->tab_groups['ip_date']          =  $this->input->post('ip_date');
    $this->tab_groups['ip_department  ']  =  $this->input->post('ip_department');
    $this->tab_groups['ip_doctor']        =  $this->input->post('ip_doctor');

    $old_ipno                             =  $this->input->post('old_ipno'); 
    $data['rs_ip']                        =  $this->input->post('ip_ipno');
     
    $this->db->update('ip', $this->tab_groups,array('ip_id'=>$id));
    $this->db->update('room_shift',$data,array('rs_ip'=>$old_ipno));
    $this->session->set_flashdata('Success','IP Updated');
    redirect('ipregister/add');
  }

  function delete($id)
  {

    if($_REQUEST['empid']) {
      $id       = $_REQUEST['empid']; 
      $row      = $this->Ipregister->get_one_banner($id);
      $ip_ipno                  =  $row->ip_ipno;
      $this->db->delete('ip',array('ip_id'=>$id));
      $this->db->delete('room_shift',array('rs_ip'=>$ip_ipno));

   }
        echo "Record Deleted";
  }

  function get_mrd($mrd)
  {



       $this->db->select('patient.p_mrd_no,patient.p_title,patient.p_name,patient.p_phone,patient.p_address,patient.p_street,patient.p_city,users.u_emp_id,users.u_name,department.dp_id,department.dp_department,patient.p_age');
      $this->db->from('op');
      $this->db->join('patient','patient.p_mrd_no = op.op_mrd','inner');
      $this->db->join('users','users.u_emp_id = op.op_doctor','inner');
      $this->db->join('department','department.dp_id = op.op_department','inner');
      $this->db->where('patient.p_mrd_no',$mrd);
      $this->db->order_by('op.op_id','desc');
      $this->db->limit(1);
      $query  = $this->db->get();
      $data  = $query->result_array();

      if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
  }
  function get_phone($phone)
  {

      $this->db->select('patient.p_mrd_no,patient.p_title,patient.p_name,patient.p_phone,patient.p_address,patient.p_street,patient.p_city,users.u_emp_id,users.u_name,department.dp_id,department.dp_department,patient.p_age');
      $this->db->from('op');
      $this->db->join('patient','patient.p_mrd_no = op.op_mrd','inner');
      $this->db->join('users','users.u_emp_id = op.op_doctor','inner');
      $this->db->join('department','department.dp_id = op.op_department','inner');
      $this->db->where('patient.p_phone',$phone);
      $this->db->order_by('op.op_id','desc');
      $this->db->limit(1);
      $query  = $this->db->get();
      $data  = $query->result_array();

      if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
  }

}