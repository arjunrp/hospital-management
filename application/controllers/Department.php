<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Department extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Department_model','Department',TRUE);
		$this->template->set('title','Department');
		$this->base_uri 			=	$this->config->item('admin_url')."department";
	}

	function index()
	{
		$data['page_title']			=	"Departments List";
		$data['output']				=	$this->Department->list_all();
		 
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('department_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Department->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Department";
		$data['action']				=	$this->base_uri.'/insert';
		$data['id']					=	"";
		$data['department']			=	"";
		$data['dp_short']			=	"";
		$data['dp_type']			=	"";
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('department_add',$data);
      	$this->load->view('templates/footer');  
	}

	function insert()
	{
		$this->tab_groups['dp_department']	     =	$this->input->post('department');
		$this->tab_groups['dp_short']	     	 =	strtoupper($this->input->post('dp_short'));
		$this->tab_groups['dp_type']	     	 =	$this->input->post('dp_type');
		$this->db->insert('department',$this->tab_groups);
		$this->session->set_flashdata('Success','Department Created');
		redirect($this->base_uri."/add");
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Department";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Department->get_one_banner($id);
		$data['id']					=	$row->dp_id;
		$data['department']			=	$row->dp_department;
		$data['dp_short']			=	$row->dp_short;
		$data['dp_type']			=	$row->dp_type;
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('department_add',$data);
      	$this->load->view('templates/footer');  
	}

	function update()
	{
		$id 									=	$this->input->post('id');
		$this->tab_groups['dp_department']	    =	$this->input->post('department');
		$this->tab_groups['dp_short']	     	=	strtoupper($this->input->post('dp_short'));
		$this->tab_groups['dp_type']	     	 =	$this->input->post('dp_type');
		 
		$this->db->update('department', $this->tab_groups,array('dp_id'=>$id));
		$this->session->set_flashdata('Success','Department Updated');
		redirect($this->base_uri);
	}

	
	function active($id,$status)
	{
		$data 			=	array('status'=>$status);
		$this->db->update('brand',$data,array('id'=>$id));
		redirect($this->base_uri);
	}

	function delete($id)
	{

		if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$row 			=	$this->Department->get_one_banner($id);
			$this->db->delete('department',array('dp_id'=>$id));

	 }
				echo "Record Deleted";
	}
}