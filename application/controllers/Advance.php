<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Advance extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Advance_model','Advance',TRUE);
		$this->load->model('Sale_model','Sales',TRUE);
		$this->load->model('Investigation_model','Investigation',TRUE);
		$this->load->model('Return_model','Return',TRUE);
		$this->load->model('Purchase_model','Purchase',TRUE);
		$this->template->set('title','Advance');
		$this->base_uri 			=	$this->config->item('admin_url')."advance";
	}

	function index()
	{
		$data['page_title']			=	"Payment List";
		$data['output']				=	$this->Advance->list_all();

		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('advance_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Advance->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Payment";
		$data['action']				=	$this->base_uri.'/insert';
		$data['advance_ID']    		=  	$this->Advance->getAdvanceId();
		$data['patients']    		= 	$this->Investigation->get_patientsip();

		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('advance_add',$data);
      	$this->load->view('templates/footer');  
	}





	function View($id)
	{
		$data['page_title']			=	"View Payment";
		$row 						=	$this->Advance->get_one_banner($id);
		$data['ve_id']				=	$row->ve_id;
		$data['ve_vno'] 			=	$row->ve_vno;
		$data['ve_date'] 			=	$row->ve_date;
		$data['ve_customer'] 		=	$row->ve_customer;
		$data['ve_mrd'] 			=	$row->ve_mrd;
		$data['p_name'] 			=	$row->p_title." ".$row->p_name;
		$data['p_phone'] 			=	$row->p_phone;
		$data['ve_apayable'] 		=	$row->ve_apayable;
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('advance_view',$data);
      	$this->load->view('templates/footer');  
	}

	function insert()
	{
 
		$this->tab_groups['ve_vno']	 		=	$this->input->post('ve_vno');
		$ve_date					 		=	$this->input->post('ve_date');
		$ve_date 							=	date("Y-m-d",strtotime($ve_date));
		$this->tab_groups['ve_date'] 		=	$ve_date;
		$this->tab_groups['ve_customer']	=	$this->input->post('ve_customer');
		$this->tab_groups['ve_supplier']	=	$this->input->post('ve_supplier');
		$this->tab_groups['ve_mrd']	 		=	$this->input->post('ve_mrd');
		$amount_paid						=	$this->input->post('ve_apayable');
		$this->tab_groups['ve_apayable']	=	$amount_paid;
		$this->tab_groups['ve_apaid']		=	"0";
		$this->tab_groups['ve_balance']		=	$amount_paid;
		$this->tab_groups['ve_pstaus']		=	"NP";
		$this->tab_groups['ve_type'] 		=	"ad";
		$this->tab_groups['ve_user']		=	$this->input->post('ve_user');
		$this->tab_groups['ve_status']      =   "cc";
	
		$this->db->insert('voucher_entry',$this->tab_groups);
		$ve_id                              = $this->db->insert_id();
		$this->session->set_flashdata('Success','Advance Payment Added');

		redirect($this->base_uri."/getPrint/".$ve_id);
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Payment";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Advance->get_one_banner($id);
		$data['ve_id']				=	$row->ve_id;
		$data['ve_vno'] 			=	$row->ve_vno;
		$data['ve_date'] 			=	$row->ve_date;
		$data['ve_customer'] 		=	$row->ve_customer;
		$data['ve_mrd'] 			=	$row->ve_mrd;
		$data['p_name'] 			=	$row->p_title." ".$row->p_name;
		$data['p_phone'] 			=	$row->p_phone;
		$data['ve_apayable'] 		=	$row->ve_apayable;
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('advance_edit',$data);
      	$this->load->view('templates/footer');  
	}

	 

	function update()
	{
	    $id 								=	$this->input->post('ve_id');
	    $ve_date 							=	$this->input->post('ve_date');
	    $amount_paid						=	$this->input->post('ve_apayable');
		$this->tab_groups['ve_apayable']	=	$amount_paid;
		$this->tab_groups['ve_user']		=	$this->input->post('ve_user');

		$this->db->update('voucher_entry', $this->tab_groups,array('ve_id'=>$id));

		$this->session->set_flashdata('Success','Advance Payment Updated');
		redirect($this->base_uri);
	}


	function delete($id)
	{


         if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$this->db->delete('voucher_entry',array('ve_id'=>$id));
			$this->session->set_flashdata('Error','Advance Payment Deleted');
		}
				echo "Record Deleted";

	}

function getPrint($sid){
  $data['company']  		=   $this->Purchase->getCompany();
  $row 						=	$this->Advance->get_one_banner($sid);
  $data['ve_id']			=	$row->ve_id;
  $data['ve_vno'] 			=	$row->ve_vno;
  $data['ve_date'] 			=	$row->ve_date;
  $data['ve_customer'] 		=	$row->ve_customer;
  $data['ve_mrd'] 			=	$row->ve_mrd;
  $data['p_name'] 			=	$row->p_title." ".$row->p_name;
  $data['p_phone'] 			=	$row->p_phone;
  $data['ve_apayable'] 		=	$row->ve_apayable;
          
  $this->load->view('adv_print',$data);
  }
}