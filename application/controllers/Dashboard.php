<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Dashboard_model','Dashboard',TRUE);
		$this->load->model('Users_model','Users',TRUE);
		$this->template->set('title','Dashboard');
		$this->base_uri 			=	$this->config->item('admin_url')."dashboard";
	}

	function index()
	{
		$permission=1;
		$output="";
		$user_type = $this->session->type;
		$user_type  = $this->Users->getUsertype($user_type);
        if($user_type=="Super User" || $user_type=="Administrator")
        {
            $permission=0;
        }
		$data['permission1']		=	$permission;

		$data['page_title']			=	"Dashboard";
		// $data['sale_cnt']			= $this->Dashboard->count_all();
		// $data['expence']			= $this->Dashboard->count_expence();
		// $data['sale']			    = $this->Dashboard->count_sale();
		// $data['purchase_cnt']		= $this->Dashboard->count_pur();
		// $data['debit_sum']			= $this->Dashboard->debit_sum();
		// // print_r($data['debit_sum']);
		// $data['credit_sum']			= $this->Dashboard->credit_sum();
		// // print_r($data['credit_sum']);
		// $data['graph_report']		= $this->Dashboard->graph_report();
		// $data['graph_report_r']		= $this->Dashboard->graph_report();
		// $data['graph_report_c']		= $this->Dashboard->graph_report();
		// $data['graph_report_p']		= $this->Dashboard->graph_report();
		// print_r($data['graph_report']);


		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('dashbord_list',$data);
    	$this->load->view('templates/footer'); 
	}

	 
}