<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xrayname extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Xrayname_model','Xrayname',TRUE);
		$this->template->set('title','xrayname');
		$this->base_uri 			=	$this->config->item('admin_url')."xrayname";
	}

	function index()
	{
		$data['page_title']			=	"Test Name List";
		$data['output']				=	$this->Xrayname->list_all();
		// $this->template->load('template','customer',$data);
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('xray_name_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Xrayname->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Test Name";
		$data['action']				=	$this->base_uri.'/insert';
		$data['xt_id']				=	"";
		$data['xt_name']			=	"";
		$data['xt_price']			=	"";
		$data['catid'] 				=	"";
		$data['categories'] 		=	$this->Xrayname->get_category();
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('xray_name_add',$data);
      	$this->load->view('templates/footer');  
	}





function View($id)
	{
		$data['page_title']			=	"Edit Test Name";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Xrayname->get_one_banner($id);
		$data['xt_id']				=	$row->xt_id;
		$data['xt_name'] 			=	$row->xt_name;
		$data['xt_price'] 			=	$row->xt_price;
		$data['catid'] 				=	$row->xt_xr_id;
		$data['categories'] 		=	$this->Xrayname->get_category();
		 
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('xray_name_list',$data);
      	$this->load->view('templates/footer');  
	}
	function insert()
	{
 
		$this->tab_groups['xt_name']	=	strtoupper($this->input->post('xt_name'));
		$this->tab_groups['xt_price']	=	$this->input->post('xt_price');
		$this->tab_groups['xt_xr_id']	=	$this->input->post('xt_xr_id');
		$this->db->insert('xray_test_name',$this->tab_groups);

		$id 								=	$this->db->insert_id();
		$this->session->set_flashdata('Success','Test Name Created');
		redirect($this->base_uri."/");
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Testsubcategory";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Xrayname->get_one_banner($id);
		$data['xt_id']				=	$row->xt_id;
		$data['xt_name'] 			=	$row->xt_name;
		$data['xt_price'] 			=	$row->xt_price;
		$data['catid'] 				=	$row->xt_xr_id;
		$data['categories'] 		=	$this->Xrayname->get_category();
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('xray_name_add',$data);
      	$this->load->view('templates/footer');  
	}

	 

	function update()
	{
		$id 						     =	$this->input->post('xt_id');
	    $this->tab_groups['xt_name']     =	strtoupper($this->input->post('xt_name'));
		$this->tab_groups['xt_price']    =	$this->input->post('xt_price');
		$this->tab_groups['xt_xr_id']   =	$this->input->post('xt_xr_id');
		$this->db->update('xray_test_name', $this->tab_groups,array('xt_id'=>$id));
		$this->session->set_flashdata('Success','Expense Updated');
		redirect($this->base_uri);
	}


	function delete($id)
	{

         if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$this->db->delete('xray_test_name',array('xt_id'=>$id));
			$this->session->set_flashdata('Error','User Deleted');
		}
				echo "Record Deleted";
	}
}