<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scan extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Scan_model','Scan',TRUE);
		$this->template->set('title','Scan');
		$this->base_uri 			=	$this->config->item('admin_url')."scan";
	}

	function index()
	{
		$data['page_title']			=	"Scan Category";
		$data['output']				=	$this->Scan->list_all();
		// $this->template->load('template','customer',$data);
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('scan_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Scan->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Scan";
		$data['action']				=	$this->base_uri.'/insert';
		$data['sc_id']				=	"";
		$data['sc_test']			=	"";
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('scan_add',$data);
      	$this->load->view('templates/footer');  
	}

	function insert()
	{

		$title 							=	strtoupper($this->input->post('sc_test'));
		$this->tab_groups['sc_test']	=	$title;
		$this->db->insert('scan',$this->tab_groups);

		$id 								=	$this->db->insert_id();
		$this->session->set_flashdata('Success','Scan Category Created');
		redirect($this->base_uri."/add");
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Scan Category";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Scan->get_one_banner($id);
		$data['sc_id']				=	$row->sc_id;
		$data['sc_test']			=	$row->sc_test;
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('scan_add',$data);
      	$this->load->view('templates/footer');  
		// $this->template->load('template','features_add',$data);
	}

	function update()
	{
		$id 							=	$this->input->post('sc_id');
		$title 							=	strtoupper($this->input->post('sc_test'));
		$this->tab_groups['sc_test']	=	$title;
		 
		$this->db->update('scan', $this->tab_groups,array('sc_id'=>$id));

		$this->session->set_flashdata('Success','Scan Updated');
		redirect($this->base_uri);
	}

	function delete($id)
	{
		if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$this->db->delete('scan',array('sc_id'=>$id));
			$this->session->set_flashdata('Error','Scan Deleted');
		}
				echo "Record Deleted";
	}
}