<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Booking extends MX_Controller 
{
  public $tab_groups;
  public $form_image;

  function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('logged_in'))
    {
      redirect($this->config->item('admin_url'));
    }
    $this->load->model('Booking_model','Booking',TRUE);
    $this->load->model('Booking_token_model','Booking_token',TRUE);
    $this->template->set('title','Booking');
    $this->base_uri       = $this->config->item('admin_url')."booking";
  }

  function index()
  {
    $data['page_head']      = "Booking List";
    $data['page_title']     = "Active Bookings";
    $data['output']         = $this->Booking->list_all();
     
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('booking_list',$data);
      $this->load->view('templates/footer'); 
  }

  public function list_ajax()
  {
    echo $data['output']    = $this->Booking->list_all();
  }

  function add()
  {
    $data['page_title']   = "Add Booking";
    $data['action']       = $this->base_uri.'/insert';
    // $data['bk_no']        = $this->Booking->get_token();
    $data['doctors']      = $this->Booking_token->get_doctor();
    // $data['patient']      = $this->Booking->get_patients();
    $data['output']       = $this->Booking->today_book();

     
        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('booking_add',$data);
        $this->load->view('templates/footer');  
  }

  function verify_tokens()
  {
    $sft_id = $this->input->post('sft_id');
    $doc_id = $this->input->post('doc_id');
    $bk_date  = $this->input->post('bk_date');

    if($sft_id=="m")
    {
      $this->db->select('dc_morntok');
      $this->db->from('doctor');
      $this->db->where('dc_docid',$doc_id);
      $query      = $this->db->get();
      $data = $query->row()->dc_morntok;
    }
    else if($sft_id=="e")
    {
      $this->db->select('dc_evngtok');
      $this->db->from('doctor');
      $this->db->where('dc_docid',$doc_id);
      $query      = $this->db->get();
      $data = $query->row()->dc_evngtok;
    }
    print_r($data);
  }

  function get_token()
  {
    $sft_id   = $this->input->post('sft_id');
    $doc_id   = $this->input->post('doc_id');
    $bk_date  = $this->input->post('bk_date');

      $this->db->select('u_name');
      $this->db->from('users');
      $this->db->where('u_emp_id',$doc_id);
      $query1   = $this->db->get();
      $doc_name = $query1->row()->u_name;

      $book = substr($doc_name, 3, 4);

      $this->db->select('SUBSTRING(bk_no, 6, 3) as bk_nos', FALSE);
      // $this->db->select('SUBSTRING(bk_no, 4, length(bk_no)-1) as SIGNED) as bk_nos');
      $this->db->from('booking');
      $this->db->where('bk_doc',$doc_id);
      $this->db->where('bk_shift',$sft_id);
      $this->db->where('bk_date',$bk_date);
      $this->db->order_by('bk_id','desc');
      $this->db->limit(1);
      $query      = $this->db->get();
      if($query->num_rows()==0)
      {
        $data = $book.'_1';
      }
      else
      {
         // print_r($query->row()->bk_nos);
        $end2   = $query->row()->bk_nos + 1;
        // print_r($end2);
        // $end2++;
        $data = $book."_".$end2;
      }

    print_r($data);
  }

  function get_doctor()
  {
    $doc_id = $this->input->post('doc_id');

      $this->db->select('u_department');
      $this->db->from('users');
      $this->db->where('u_emp_id',$doc_id);
      $query  = $this->db->get();
      $data   = $query->row()->u_department;

    print_r($data);
  }

  function insert()
  {
    $this->tab_groups['bk_no']        =  $this->input->post('bk_no');
    $this->tab_groups['bk_date']      =  $this->input->post('bk_date');
    $this->tab_groups['bk_today']     =  date("Y-m-d");
    $this->tab_groups['bk_shift']     =  $this->input->post('bk_shift');
    $this->tab_groups['bk_doc']       =  $this->input->post('bk_doc');
    $this->tab_groups['bk_dept']      =  $this->input->post('bk_dept');
    $this->tab_groups['bk_mrd']       =  $this->input->post('bk_mrd');
    $this->tab_groups['bk_name']      =  $this->input->post('bk_name');
    $this->tab_groups['bk_phone']     =  $this->input->post('bk_phone');
    $this->tab_groups['bk_address']   =  $this->input->post('bk_address');
    $this->tab_groups['bk_street']    =  $this->input->post('bk_street');
    $this->tab_groups['bk_age']       =  $this->input->post('bk_age');
    $this->tab_groups['bk_user']      =  $this->input->post('user_type');
     
    $this->db->insert('booking',$this->tab_groups);
    $this->session->set_flashdata('Success','Booking Created');
    redirect('booking/add');
  }

  function edit($id)
  {
    $data['page_title']   = "Edit Booking";
    $data['action']       = $this->base_uri.'/update';
    $row                  = $this->Booking->get_one_banner($id);
    $data['doctors']      = $this->Booking_token->get_doctor();
    $data['bk_id']        = $id;
    $data['bk_no']        = $row->bk_no;
    $data['bk_date']      = $row->bk_date;
    $data['bk_shift']     = $row->bk_shift;
    $data['bk_doc']       = $row->bk_doc;
    $data['bk_dept']      = $row->bk_dept;
    $data['bk_mrd']       = $row->bk_mrd;
    $data['bk_name']      = $row->bk_name;
    $data['bk_phone']     = $row->bk_phone;
    $data['output']       = $this->Booking->today_book();
    $data['bk_address']   = $row->bk_address;
    $data['bk_street']    = $row->bk_street;
    $data['bk_age']       = $row->bk_age;
     
        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('booking_edit',$data);
        $this->load->view('templates/footer');  
  }

  function update()
  {
    $id               = $this->input->post('bk_id');
    $this->tab_groups['bk_shift']     =  $this->input->post('bk_shift');
    $this->tab_groups['bk_doc']       =  $this->input->post('bk_doc');
    $this->tab_groups['bk_dept']      =  $this->input->post('bk_dept');
    $this->tab_groups['bk_mrd']       =  $this->input->post('bk_mrd');
    $this->tab_groups['bk_name']      =  $this->input->post('bk_name');
    $this->tab_groups['bk_phone']     =  $this->input->post('bk_phone');
    $this->tab_groups['bk_address']   =  $this->input->post('bk_address');
    $this->tab_groups['bk_street']    =  $this->input->post('bk_street');
    $this->tab_groups['bk_age']       =  $this->input->post('bk_age');
     
    $this->db->update('booking', $this->tab_groups,array('bk_id'=>$id));
    $this->session->set_flashdata('Success','Booking Updated');
    redirect('booking/add');
  }

  function delete($id)
  {

    if($_REQUEST['empid']) {
      $id   = $_REQUEST['empid']; 
      $row      = $this->Booking->get_one_banner($id);
      $this->db->delete('booking',array('bk_id'=>$id));

   }
        echo "Record Deleted";
  }

  function opadd($id)
  {

    $row                                =  $this->Booking->get_one_banner($id);
    $op_mrd                             =  $row->bk_mrd;
    $op_opno                            =  "B_".$row->bk_no;
    $op_department                      =  $row->bk_dept;
    $op_date                            =  date("Y-m-d");
    $op_shift                           =  $row->bk_shift;
    $op_doctor                          =  $row->bk_doc;
    $op_visit_type                      =  $this->Booking->get_visit_type($op_mrd,$op_department);
    // $op_opno                            =  $this->Booking->get_opno($op_shift,$op_doctor);
    $op_docfee                          =  $this->Booking->get_docfee($op_doctor);
    $this->tab_groups['op_mrd']         =  $op_mrd;
    $this->tab_groups['op_opno']        =  $op_opno;
    $this->tab_groups['op_type']        =  "b";
    $this->tab_groups['op_date']        =  $op_date;
    $this->tab_groups['op_department']  =  $op_department;
    $this->tab_groups['op_doctor']      =  $op_doctor;
    $this->tab_groups['op_shift']       =  $op_shift;
    $this->tab_groups['op_docfee']      =  $op_docfee;
    $this->tab_groups['op_visit_type']  =  $op_visit_type;
     
    $this->db->insert('op', $this->tab_groups);
    $data['bk_status']                  = 0;
    $this->db->update('booking', $data,array('bk_id'=>$id));
    $this->session->set_flashdata('Success','OP Created');
    redirect('booking/add');
  }
  function get_mrd($mrd)
  {

      $this->db->select('patient.p_mrd_no,patient.p_title,patient.p_name,patient.p_phone,patient.p_address,patient.p_street,patient.p_city');
      $this->db->from('patient');
      $this->db->where('patient.p_mrd_no',$mrd);
      $query  = $this->db->get();
      $data  = $query->result_array();

      if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
  }
  function get_phone($phone)
  {

      $this->db->select('patient.p_mrd_no,patient.p_title,patient.p_name,patient.p_phone,patient.p_address,patient.p_street,patient.p_city');
      $this->db->from('patient');
      $this->db->where('patient.p_phone',$phone);
      $query  = $this->db->get();
      $data  = $query->result_array();

      if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
  }
}