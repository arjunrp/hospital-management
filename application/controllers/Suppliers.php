<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Suppliers extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}	
		$this->load->model('Suppliers_model','Suppliers',TRUE);
		$this->load->model('Patientmodel','Patient',TRUE);
		$this->template->set('title','Suppliers');
		$this->base_uri 			=	$this->config->item('admin_url')."suppliers";
	}

	function index()
	{
		$data['page_title']			=	"Suppliers List";
		$data['output']				=	$this->Suppliers->list_all();
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('supplier_list',$data);
    	$this->load->view('templates/footer'); 
	}

function view($id)
	{
		$data['page_title']			=	"Vendor View";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Suppliers->get_one_banner($id);
		$data['sp_id']			    =	$id;
		$data['sp_lic']			    =	$row->sp_lic;
		$data['sp_vendor']			=	$row->sp_vendor;
		$data['sp_address']			=	$row->sp_address;
		$data['sp_gst_no']			=	$row->sp_gst_no;
		$data['sp_street']			=	$row->sp_street;
		$data['sp_city']			=	$row->sp_city;
		$data['sp_zip']				=	$row->sp_zip;
		$data['sp_country']			=	$row->sp_country;
		$data['sp_state']			=	$row->sp_state;
		$data['sp_phone']			=	$row->sp_phone;
		$data['sp_email']			=	$row->sp_email;
		$data['sp_fax']				=	$row->sp_fax;
		$data['sp_website']			=	$row->sp_website;
		$data['sp_payment']			=	$row->sp_payment;
        $data['image']				=	$row->sp_image;
		
		$citys 						=	$row->sp_state;
		$states 					=	$row->sp_country;
		$data['cities'] 			=	$this->Patient->get_citys($citys);
		$data['countries'] 		    =	$this->Patient->get_country();
		$data['states'] 		    =	$this->Patient->get_states($states);
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('supplier_view',$data);
      	$this->load->view('templates/footer'); 
	}




	public function list_ajax()
	{
		echo $data['output']		=	$this->Suppliers->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Supplier";
		$data['action']				=	$this->base_uri.'/insert';
		$data['sp_id']				=	"";
		$data['sp_lic']			    =	"";
		$data['sp_vendor']			=	"";
		$data['sp_address']			=	"";
		$data['sp_gst_no'] 			=	"";
		$data['sp_street']			=	"";
		$data['sp_city']			=	"";
		$data['sp_zip'] 			=	"";
		$data['sp_country']			=	"";
		$data['sp_state'] 			=	"";
		$data['sp_phone']			=	"";
		$data['sp_email']			=	"";
		$data['sp_fax']			    =	"";
		$data['sp_website']			=	"";
		$data['sp_payment']			=	"";
		$data['image']				=	"";

		$data['cities']				=	"";
		$data['states']				=	"";
		$data['countries'] 			=	$this->Patient->get_country();

		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('supplier_add',$data);
      	$this->load->view('templates/footer');  
	}

	function insert()
	{

		$this->tab_groups['sp_lic']	    =	$this->input->post('sp_lic');
		$this->tab_groups['sp_vendor']	=	$this->input->post('sp_vendor');
		$this->tab_groups['sp_address']	=	$this->input->post('sp_address');
		$this->tab_groups['sp_gst_no']	=	$this->input->post('sp_gst_no');
		$this->tab_groups['sp_street']	=	$this->input->post('sp_street');
		$this->tab_groups['sp_city']	=	$this->input->post('sp_city');
		$this->tab_groups['sp_zip']		=	$this->input->post('sp_zip');
		$this->tab_groups['sp_country']	=	$this->input->post('sp_country');
		$this->tab_groups['sp_state']	=	$this->input->post('sp_state');
		$this->tab_groups['sp_phone']	=	$this->input->post('sp_phone');
		$this->tab_groups['sp_email']	=	$this->input->post('sp_email');
		$this->tab_groups['sp_fax']		=	$this->input->post('sp_fax');
		$this->tab_groups['sp_website']	=	$this->input->post('sp_website');
		$this->tab_groups['sp_payment']	=	$this->input->post('sp_payment');

		$this->db->insert('suppliers',$this->tab_groups);

		$id 						=	$this->db->insert_id();
		$config['upload_path']		=	$this->config->item('image_path').'supplier/original/';
		$config['allowed_types']	=	'gif|jpg|jpeg|png';
		$config['max_size']			=	'20000';
		@$config['file_name']		=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ($this->upload->do_upload('image'))
		{		
			
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['sp_image']	=	$data['upload_data']['file_name'];
			$this->db->update('suppliers', $this->form_image,array('sp_id'=>$id));
			
		}	
		else
		{
		
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
			
		}

		delete_files($this->config->item('image_path').'supplier/original/');
		$this->session->set_flashdata('Success','Supplier Created');
		redirect($this->base_uri."/add");

		 
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Supplier";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Suppliers->get_one_banner($id);
		$data['sp_id']				=	$row->sp_id;
		$data['sp_lic']			    =	$row->sp_lic;
		$data['sp_vendor']			=	$row->sp_vendor;
		$data['sp_address']			=	$row->sp_address;
		$data['sp_gst_no']			=	$row->sp_gst_no;
		$data['sp_street']			=	$row->sp_street;
		$data['sp_city']			=	$row->sp_city;
		$data['sp_zip']				=	$row->sp_zip;
		$data['sp_country']			=	$row->sp_country;
		$data['sp_state']			=	$row->sp_state;
		$data['sp_phone']			=	$row->sp_phone;
		$data['sp_email']			=	$row->sp_email;
		$data['sp_fax']				=	$row->sp_fax;
		$data['sp_website']			=	$row->sp_website;
		$data['sp_payment']			=	$row->sp_payment;
        $data['image']				=	$row->sp_image;
		
		$citys 						=	$row->sp_state;
		$states 					=	$row->sp_country;
		$data['cities'] 			=	$this->Patient->get_citys($citys);
		$data['countries'] 		    =	$this->Patient->get_country();
		$data['states'] 		    =	$this->Patient->get_states($states); 

		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('supplier_add',$data);
      	$this->load->view('templates/footer');  
	}
 
	function update()
	{
		$id 							=	$this->input->post('sp_id');
		$this->tab_groups['sp_lic']	    =	$this->input->post('sp_lic');
		$this->tab_groups['sp_vendor']	=	$this->input->post('sp_vendor');
		$this->tab_groups['sp_address']	=	$this->input->post('sp_address');
		$this->tab_groups['sp_gst_no']	=	$this->input->post('sp_gst_no');
		$this->tab_groups['sp_street']	=	$this->input->post('sp_street');
		$this->tab_groups['sp_city']	=	$this->input->post('sp_city');
		$this->tab_groups['sp_zip']		=	$this->input->post('sp_zip');
		$this->tab_groups['sp_country']	=	$this->input->post('sp_country');
		$this->tab_groups['sp_state']	=	$this->input->post('sp_state');
		$this->tab_groups['sp_phone']	=	$this->input->post('sp_phone');
		$this->tab_groups['sp_email']	=	$this->input->post('sp_email');
		$this->tab_groups['sp_fax']		=	$this->input->post('sp_fax');
		$this->tab_groups['sp_website']	=	$this->input->post('sp_website');
		$this->tab_groups['sp_payment']	=	$this->input->post('sp_payment');

		$this->db->update('suppliers', $this->tab_groups,array('sp_id'=>$id));

		$config['upload_path']		=	$this->config->item('image_path').'supplier/original/';
		$config['allowed_types']	=	'gif|jpg|jpeg|png';
		$config['max_size']			=	'20000';
		@$config['file_name']		=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ($this->upload->do_upload('image'))
		{		
			
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['sp_image']	=	$data['upload_data']['file_name'];
			$this->db->update('suppliers', $this->form_image,array('sp_id'=>$id));
			
		}	
		else
		{
		
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
			
		}

		delete_files($this->config->item('image_path').'supplier/original/');
		$this->session->set_flashdata('Success','Supplier Updated');
		redirect($this->base_uri);
	}

	function process_uploaded_image($filename)
	{
		$large_path 	=	$this->config->item('image_path').'supplier/original/'.$filename;
		$medium_path 	=	$this->config->item('image_path').'supplier/'.$filename;
		$thumb_path 	=	$this->config->item('image_path').'supplier/small/'.$filename;
		
		$medium_config['image_library'] 	=	'gd2';
		$medium_config['source_image'] 		=	$large_path;
		$medium_config['new_image'] 		=	$medium_path;
		$medium_config['maintain_ratio'] 	=	TRUE;
		$medium_config['width'] 			=	1600;
		$medium_config['height'] 			=	400;
		$this->load->library('image_lib', $medium_config);
		$this->image_lib->initialize($medium_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		$thumb_config['image_library'] 		=	'gd2';
		$thumb_config['source_image'] 		=	$large_path;
		$thumb_config['new_image'] 			=	$thumb_path;
		$thumb_config['maintain_ratio'] 	=	TRUE;
		$thumb_config['width'] 				=	50;
		$thumb_config['height'] 			=	50;
		$this->load->library('image_lib', $thumb_config);
		$this->image_lib->initialize($thumb_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		return true;    
	}

	function delete($id)
	{
		if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$row 	=	$this->Suppliers->get_one_banner($id);
			 unlink($this->config->item('image_path').'supplier/small/'.$row->sp_image);
			 unlink($this->config->item('image_path').'supplier/'.$row->sp_image);
			$this->db->delete('suppliers',array('sp_id'=>$id));
		}
				echo "Record Deleted";
	}



}