<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stock extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('stock_model','Stock',TRUE);
		$this->load->model('Product_model','Product',TRUE);
		$this->template->set('title','Stock');
		$this->base_uri 			=	$this->config->item('admin_url')."stock";
	}

	function index()
	{
		$data['page_title']			=	"Stock List";
		$data['output']				=	$this->Stock->list_all();
		// $this->template->load('template','customer',$data);
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('stock_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Stock->list_all();
	}




	function view($id)
	{
		$data['page_title']			=	"View Stock";

		$row 						=	$this->Product->get_one_banner($id);
		$data['pd_id']				=	$id;
		$data['pd_code']			=	$row->pd_code;
		$data['pd_product']			=	$row->pd_product;
		$data['pd_contentid']		=	$row->pd_contentid;
		$data['pd_brandid']			=	$row->pd_brandid;
		$data['pd_sgst']			=	$row->pd_sgst;
		$data['pd_cgst']			=	$row->pd_cgst;
		$data['pd_min']			    =	$row->pd_min;
		$data['pd_max']				=	$row->pd_max;
		$data['pd_supplier']		=	$row->pd_supplier;
		$data['pd_unit']	    	=	$row->pd_unit;
		$data['pd_qty']			    =	$row->pd_qty;
		$data['pd_type']			=	$row->pd_type;
		$data['image']				=	$row->pd_image;	

		$data['contents']			=	$this->Product->getContent();
		$data['suppliers']			=	$this->Product->getSupplier();
		$data['brands']	    		=	$this->Product->getBrand();

		$data['stock_details'] 		=	$this->Stock->get_stock_view($id);
		 
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('stock_view',$data);
      	$this->load->view('templates/footer');  
		// $this->template->load('template','features_add',$data);
	}


	function add()
	{
		$data['page_title']			=	"Add Stock";
		$data['action']				=	$this->base_uri.'/insert';
		$data['id']					=	"";
		$data['customer']			=	"";
		$data['address']			=	"";
		$data['city']				=	"";
		$data['zip']				=	"";
		$data['country']			=	"";
		 
		// $data['image']				=	"";
		// $data['type']				=	"";
		// $this->template->load('template','features_add',$data);
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('customer_add',$data);
      	$this->load->view('templates/footer');  
	}

	function insert()
	{

		$title 							=	$this->input->post('customer');
		$this->tab_groups['customer']	=	$title;
		$this->tab_groups['stock_date']	=	$this->input->post('s_date');
		$this->tab_groups['stock_product_id']		=	$this->input->post('s_pid');
		$this->tab_groups['stock_product']		=	$this->input->post('s_product');
		$this->tab_groups['stock_qty']	=	$this->input->post('s_qty');
		 
		// $this->tab_groups['service_type']		=	$this->input->post('stype');
		$this->db->insert('customer',$this->tab_groups);

		$id 								=	$this->db->insert_id();
		// $config['upload_path']				=	$this->config->item('image_path').'features/original/';
		// $config['allowed_types']			=	'gif|jpg|jpeg|png';
		// $config['max_size']					=	'20000';
		// @$config['file_name']				=	$id.$config['file_ext'];
		// $this->load->library('upload', $config);
		// $this->upload->initialize($config);

		// if ($this->upload->do_upload('image'))
		// {		
			
		// 	$data = array('upload_data' => $this->upload->data());
		// 	$this->process_uploaded_image($data['upload_data']['file_name']);
		// 	$this->form_image['feature_image']	=	$data['upload_data']['file_name'];
		// 	$this->db->update('features', $this->form_image,array('feature_id'=>$id));
			
		// }	
		// else
		// {
		
		// 	$error = $this->upload->display_errors();	
		// 	$this->messages->add($error, 'error');
			
		// }

		// delete_files($this->config->item('image_path').'features/original/');
		redirect($this->base_uri);
	}

	function edit($pd_id)
	{
		$data['page_title']			=	"Edit Stock";
		$data['action']				=	$this->base_uri.'/update';
		$data['stock_products']		=	$this->Stock->get_one_banner($pd_id);

		$row 						=	$this->Product->get_one_banner($pd_id);
		$data['pd_id']				=	$pd_id;
		$data['pd_code']			=	$row->pd_code;
		$data['pd_product']			=	$row->pd_product;
		$data['pd_contentid']		=	$row->pd_contentid;
		$data['pd_brandid']			=	$row->pd_brandid;
		$data['pd_sgst']			=	$row->pd_sgst;
		$data['pd_cgst']			=	$row->pd_cgst;
		$data['pd_min']			    =	$row->pd_min;
		$data['pd_max']				=	$row->pd_max;
		$data['pd_supplier']		=	$row->pd_supplier;
		$data['pd_unit']	    	=	$row->pd_unit;
		$data['pd_qty']			    =	$row->pd_qty;
		$data['pd_type']			=	$row->pd_type;
		$data['image']				=	$row->pd_image;	

		$data['contents']			=	$this->Product->getContent();
		$data['suppliers']			=	$this->Product->getSupplier();
		$data['brands']	    		=	$this->Product->getBrand();
 		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('stock_edit',$data);
      	$this->load->view('templates/footer');  
		// $this->template->load('template','features_add',$data);
	}

	function update()
	{

		foreach($this->input->post('stock_id') as $k => $v)
  		{

    		$inputs = array();
    		$inputs['stock_batch']  = $this->input->post('stock_batch')[$k];
    		$inputs['stock_qty']    = $this->input->post('stock_qty')[$k];
    		$this->db->update('stock', $inputs,array('stock_id'=>$v));
  		} 
		print_r("Success");
	}


	function delete($id)
	{
		$row 			=	$this->Stock->get_one_banner($id);
		// unlink($this->config->item('image_path').'features/small/'.$row->feature_image);
		// unlink($this->config->item('image_path').'features/'.$row->feature_image);
		$this->db->delete('stock',array('stock_id'=>$id));
		$this->list_ajax();
	}
}