<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Allreport extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Allreportmodel','Allreport',TRUE);
		$this->load->model('Product_model','Product',TRUE);
		$this->load->model('Subcategory_model','Subcategory',TRUE);
		$this->load->model('Inventory_model','Inventory',TRUE);
		$this->template->set('title','Report');
		$this->base_uri 			=	$this->config->item('admin_url')."allreport";
	}


	public function list_ajax()
	{
		echo $data['output']		=	$this->Customer->list_all();
	}


	function index()
	{

		$data['page_title']			=	"Account Report";
		$data['action']				=	$this->base_uri.'/account_filter_detail';
		$data['action1']			=	"";
		$data['r_type']				=	"4";
		$data['fdate']				=	"";
		$data['tdate']				=	"";
		$data['product']			=	"";
		$data['products']			=	"";
		$data['output']				=	"";
		$data['output1']			=	"";
		$data['categorys']			=	$this->Subcategory->get_category();
		$data['groups']				=	$this->Product->getGroup();
		$data['brands']		    	=	$this->Product->getBrand();
		$data['types']				=	$this->Product->getType();
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('ac_report',$data);
    	$this->load->view('templates/footer'); 
	}

	function account_filter_detail()
	{
		$data['page_title']			=	"Account Report";
		$data['action']				=	$this->base_uri.'/account_filter_detail';
		$data['action1']			=	$this->base_uri.'/account_detail_print';
		$fdate						=	$this->input->post('fdate');
		$tdate						=	$this->input->post('tdate');
		$product					=	$this->input->post('product');
		$r_type						=	$this->input->post('r_type');
		$data['r_type']				=	$r_type;
		$data['fdate']				=	$fdate;
		$data['tdate']				=	$tdate;
		$data['product']			=	$product;
		if($product==0)
		{
			$data['products']		= "All Products";
		}
		else
		{
		$data['products']			=	$this->Allreport->list_small($product);
		}
		
		if($r_type==3)
		{
			$data['sroutput']		=	$this->Allreport->sreturn_blnce_details($fdate,$tdate,$product);
			$data['proutput']		=	$this->Allreport->preturn_blnce_details($fdate,$tdate,$product);
		}
		$data['output']				=	$this->Allreport->get_account_detail($fdate,$tdate,$product,$r_type);
		$data['output1']			=	$this->Allreport->get_account_main($fdate,$tdate,$product,$r_type);
		$data['categorys']			=	$this->Subcategory->get_category();
		$data['groups']				=	$this->Product->getGroup();
		$data['brands']		    	=	$this->Product->getBrand();
		$data['types']				=	$this->Product->getType();
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('ac_report',$data);
      	$this->load->view('templates/footer');    
	}

	function account_detail_print()
	{
		$data['page_title']			=	"Account Report";
		$fdate						=	$this->input->post('fdate1');
		$tdate						=	$this->input->post('tdate1');
		$product					=	$this->input->post('product1');
		$r_type						=	$this->input->post('r_type1');
		$data['r_type']				=	$r_type;
		$data['fdate']				=	$fdate;
		$data['tdate']				=	$tdate;
		$data['product']			=	$product;
		if($product==0)
		{
			$data['products']		= "All Products";
		}
		else
		{
		$data['products']			=	$this->Allreport->list_small($product);
		}
		if($r_type==3)
		{
			$data['sroutput']		=	$this->Allreport->sreturn_blnce_details($fdate,$tdate,$product);
			$data['proutput']		=	$this->Allreport->preturn_blnce_details($fdate,$tdate,$product);
		}
		$data['output']				=	$this->Allreport->get_account_detail($fdate,$tdate,$product,$r_type);
		$data['output1']			=	$this->Allreport->get_account_main($fdate,$tdate,$product,$r_type);
		$data['company']  = $this->Inventory->getCompany();      
      	$this->load->view('ac_report_print',$data);  
	}

	function getBproduct($c_id){

    $query=$this->db->get_where('category',array('brandid'=>$c_id));
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
 	}
 	function getGTproduct($c_id,$sc_id,$t_id){
    $query=$this->db->get_where('product',array('groupid'=>$c_id,'subcategoryid'=>$sc_id,'typeid'=>$t_id));
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
 	}
 	function getGproduct($c_id,$sc_id){
    $query=$this->db->get_where('product',array('groupid'=>$c_id,'subcategoryid'=>$sc_id));
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
 	}

 	function getTproduct($c_id,$sc_id){

    $query=$this->db->get_where('product',array('typeid'=>$c_id,'subcategoryid'=>$sc_id));
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
 	}
 	function getTGproduct($c_id,$sc_id,$g_id){

    $query=$this->db->get_where('product',array('typeid'=>$c_id,'subcategoryid'=>$sc_id,'groupid'=>$g_id));
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
 	}

 	function getSproduct($c_id){

    $query=$this->db->get_where('product',array('subcategoryid'=>$c_id));
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
 	}
}