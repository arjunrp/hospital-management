<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stock_adjust extends MX_Controller 
{
  public $tab_groups;
  public $form_image;

  function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('logged_in'))
    {
      redirect($this->config->item('admin_url'));
    }
    $this->load->model('Stock_adjust_model','Stock_adjust',TRUE);
    $this->load->model('Stocktransfer_model','Stocktransfer',TRUE);
    $this->load->model('Sale_model','Sales',TRUE);
    $this->load->model('Purchase_model','Purchase',TRUE);
    $this->load->model('Stock_model','Stock',TRUE);
    $this->load->model('Consume_model','Consume',TRUE);
    $this->template->set('title','Consumption');
    $this->base_uri       = $this->config->item('admin_url')."stock_adjust";
  }

  function index()
  {
    $data['page_title']     =   "Stock Adjustment List";
    $data['output'] =   $this->Stock_adjust->list_all();

    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('stock_adjust_list',$data);
    $this->load->view('templates/footer'); 
      
  }

  function add(){

      $data['page_title']         = "New Stock Adjustment";
      $data['products']           = $this->Stock_adjust->productGetbydept();
      $data['stock_adjust_id']    = $this->Stock_adjust->getSAId();
      $data['departments']        = $this->Stocktransfer->getDepartment();
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('stock_adjust_add',$data);
      $this->load->view('templates/footer'); 
  }

  function edit($id){

      $data['page_title']   = "Edit Stock Adjustment";
      $data['stadjusts']    =  $this->Stock_adjust->getStadjustview($id);
      $data['products']     =  $this->Stock_adjust->productGetbydept();

      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('stock_adjust_edit',$data);
      $this->load->view('templates/footer'); 
  }

  function stock_adjust_View($sid){
    $data['page_title']    =  "Stock Transaction View";
    $data['stadjusts']     =  $this->Stock_adjust->getStadjustview($sid);

    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('stock_adjust_view', $data);
    $this->load->view('templates/footer');  
  }

  function getPrint(){
  $sid              = $_GET['cPrintid'];
  $data['company']  = $this->Purchase->getCompany();
  $data['consume']  = $this->Consume->getConsumeview($sid);
  $data['consumes']  = $this->Consume->getConsumeview($sid);
  $this->load->view('templates/header');    
  $this->load->view('consume_print',$data);
  }

  function stadjustAdd(){

    $this->tab_groups['ve_vno']           = $this->input->post('ve_vno');
    $ve_date                              = $this->input->post('ve_date');
    $ve_date                              = date("Y-m-d",strtotime($ve_date));
    $this->tab_groups['ve_date']          = $ve_date;

    $ve_supplier                          = $this->input->post('ve_supplier');
    $this->tab_groups['ve_supplier']      = $ve_supplier;
    $ve_customer                          = $this->input->post('ve_customer');
    $this->tab_groups['ve_customer']      = $ve_customer;
    $this->tab_groups['ve_user']          = $this->input->post('user_type');
    $this->tab_groups['ve_type']          = "sta";
    $this->db->insert('voucher_entry',$this->tab_groups);
    $sadjustid                            = $this->db->insert_id();

  foreach($this->input->post('ved_item') as $k => $v)
  {

    $inputs = array();
    $inputs['ved_veid']       = $sadjustid;
    $inputs['ved_date']       = $ve_date;
    $stockpid                 = $this->input->post('ved_itemid')[$k];
    $inputs['ved_itemid']     = $stockpid;
    $inputs['ved_item']       = $v;
    $ved_batch                = $this->input->post('ved_batch')[$k];
    $inputs['ved_batch']      = $ved_batch;
    $purchase_item_qty        = $this->input->post('ved_qty')[$k];
    $unit_item_qty            = $this->input->post('ved_uqty')[$k];
    $inputs['ved_expiry']     = $this->input->post('ved_expiry')[$k];
    
    $ved_unit                 = $this->input->post('ved_unit')[$k];

    $inputs['ved_unit']       = $ved_unit;
    $purchase_unit_qty        = $purchase_item_qty * $unit_item_qty;


    $inputs['ved_qty']        = $purchase_unit_qty;
    $inputs['ved_uqty']       = $unit_item_qty;

    $this->db->insert('voucher_entry_detail',$inputs);
    $this->Stock_adjust->set_sadjust_stock($stockpid,$purchase_unit_qty,$ved_batch,$ve_supplier,$ve_date);
  } 
  print_r($sadjustid);
  }

  function stadjustUpdate($ve_id1){
    $ve_id                                = $this->input->post('ve_id');
    $ve_date                              = $this->input->post('ve_date');
    $result = $this->Consume->get_voucher_details($ve_id1);
      foreach ($result as $key => $value) {

        $stockpid             = $value['ved_itemid'];
        $purchase_item_qty    = $value['ved_qty'];
        $ve_supplier          = $value['ve_supplier'];
        $purchase_date        = $value['ved_date'];
        $ved_batch            = $value['ved_batch'];

        $this->Stock_adjust->delete_sadjust_stock($stockpid,$purchase_item_qty,$purchase_date,$ved_batch,$ve_supplier);
        
      }

    
    $this->db->delete('voucher_entry_detail',array('ved_veid'=>$ve_id));

  foreach($this->input->post('ved_item') as $k => $v)
  {

    $inputs = array();
    $inputs['ved_veid']       = $ve_id;
    $inputs['ved_date']       = $ve_date;
    $stockpid                 = $this->input->post('ved_itemid')[$k];
    $inputs['ved_itemid']     = $stockpid;
    $inputs['ved_item']       = $v;
    $ved_batch                = $this->input->post('ved_batch')[$k];
    $inputs['ved_batch']      = $ved_batch;
    $purchase_item_qty        = $this->input->post('ved_qty')[$k];
    $unit_item_qty            = $this->input->post('ved_uqty')[$k];
    $inputs['ved_expiry']     = $this->input->post('ved_expiry')[$k];
    
    $ved_unit                 = $this->input->post('ved_unit')[$k];

    $inputs['ved_unit']       = $ved_unit;
    $purchase_unit_qty        = $purchase_item_qty * $unit_item_qty;

    $inputs['ved_qty']        = $purchase_unit_qty;
    $inputs['ved_uqty']       = $unit_item_qty;

    $this->Stock_adjust->update_sadjust_stock($stockpid,$purchase_unit_qty,$ve_date,$ved_batch,$ve_supplier);

    $this->db->insert('voucher_entry_detail',$inputs);
  } 
}

function delete($id)
  {

    if($_REQUEST['empid']) {
      $id   = $_REQUEST['empid']; 

      $idd = $id;
      $result = $this->Consume->get_voucher_details($idd);

      foreach ($result as $key => $value) {

        $stockpid             = $value['ved_itemid'];
        $purchase_item_qty    = $value['ved_qty'];
        $ve_supplier          = $value['ve_supplier'];
        $purchase_date        = $value['ved_date'];
        $ved_batch            = $value['ved_batch'];

        $this->Stock_adjust->delete_sadjust_stock($stockpid,$purchase_item_qty,$purchase_date,$ved_batch,$ve_supplier);
        
      }
      $this->db->delete('voucher_entry',array('ve_id'=>$id));
      $this->db->delete('voucher_entry_detail',array('ved_veid'=>$id));

   }
        echo "Record Deleted";
  }

}