<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Additionalbill extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Additionalbill_model','Additionalbill',TRUE);
		$this->template->set('title','Addtional Billing');
		$this->base_uri 			=	$this->config->item('admin_url')."additionalbill";
	}

	function index()
	{
		$data['page_title']			=	"Billing Items List";
		$data['output']				=	$this->Additionalbill->list_all();
		 
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('additionalbill_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Additionalbill->list_all();
	}

	function add()
	{
		$data['page_title']		=	"Add Billing Items";
		$data['action']			=	$this->base_uri.'/insert';
		$data['ab_id']			=	"";
		$data['ab_name']		=	"";
		$data['ab_amount']		=	"";
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('additionalbill_add',$data);
      	$this->load->view('templates/footer');  
	}

	function insert()
	{

		$this->tab_groups['ab_name']	 =	strtoupper($this->input->post('ab_name'));
		$this->tab_groups['ab_amount']	 =	$this->input->post('ab_amount');
		$this->db->insert('additional_bill',$this->tab_groups);

		$this->session->set_flashdata('Success','Additional Billing Items Created');
		redirect($this->base_uri."/add");
	}

	function edit($id)
	{
		$data['page_title']		=	"Edit Billing Items";
		$data['action']			=	$this->base_uri.'/update';
		$row 					=	$this->Additionalbill->get_one_banner($id);
		$data['ab_id']			=	$row->ab_id;
		$data['ab_name']		=	$row->ab_name;
		$data['ab_amount']		=	$row->ab_amount;
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('additionalbill_add',$data);
      	$this->load->view('templates/footer');  
	}

	function update()
	{
		$id 							=	$this->input->post('ab_id');
		$this->tab_groups['ab_name']	=	strtoupper($this->input->post('ab_name'));
		$this->tab_groups['ab_amount']	=	$this->input->post('ab_amount');
		 
		$this->db->update('additional_bill', $this->tab_groups,array('ab_id'=>$id));
		$this->session->set_flashdata('Success','Additional Billing Items Updated');
		redirect($this->base_uri);
	}
	function delete($id)
	{

		if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$row 			=	$this->Room->get_one_banner($id);
			$this->db->delete('additional_bill',array('ab_id'=>$id));

	 }
				echo "Record Deleted";
	}
}