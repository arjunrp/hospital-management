<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mcontents extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Mcontents_model','Mcontents',TRUE);
		$this->template->set('title','Medicine Content');
		$this->base_uri 			=	$this->config->item('admin_url')."mcontents";
	}

	function index()
	{
		$data['page_title']			=	"Medicine Content List";
		$data['output']				=	$this->Mcontents->list_all();
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('mcontents_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Mcontents->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Medicine Content";
		$data['action']				=	$this->base_uri.'/insert';
		$data['mc_id']				=	"";
		$data['mc_content']			=	"";
		 
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('mcontents_add',$data);
      	$this->load->view('templates/footer');  
	}

	function insert()
	{

		$this->tab_groups['mc_content']	=	$this->input->post('mc_content');
		 
		$this->db->insert('mcontents',$this->tab_groups);

		$this->session->set_flashdata('Success','Medicine Content Created');
		redirect($this->base_uri);
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Medicine Content";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Mcontents->get_one_banner($id);
		$data['mc_id']				=	$id;
		$data['mc_content']			=	$row->mc_content;
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('mcontents_add',$data);
      	$this->load->view('templates/footer');  
	}

	function update()
	{
		$id 							=	$this->input->post('mc_id');
		$this->tab_groups['mc_content']	=	$this->input->post('mc_content');
		 
		$this->db->update('mcontents', $this->tab_groups,array('mc_id'=>$id));

		$this->session->set_flashdata('Success','Medicine Content Updated');
		redirect($this->base_uri);
	}

	function delete($id)
	{
		if($_REQUEST['empid']) {
		$id 	=	$_REQUEST['empid'];	
		$this->db->delete('mcontents',array('mc_id'=>$id));
		}
				echo "Record Deleted";
	}
}