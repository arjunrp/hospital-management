<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scanname extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Scanname_model','Scanname',TRUE);
		$this->template->set('title','Scanname');
		$this->base_uri 			=	$this->config->item('admin_url')."Scanname";
	}

	function index()
	{
		$data['page_title']			=	"Test Name List";
		$data['output']				=	$this->Scanname->list_all();
		// $this->template->load('template','customer',$data);
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('scan_name_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Scanname->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Test Name";
		$data['action']				=	$this->base_uri.'/insert';
		$data['sn_id']			    =	"";
		$data['sn_name']			=	"";
		$data['sn_price']			=	"";
		$data['catid'] 				=	"";
		$data['categories'] 		=	$this->Scanname->get_category();
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('scan_name_add',$data);
      	$this->load->view('templates/footer');  
	}





function View($id)
	{
		$data['page_title']			=	"Edit Test Name";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Scanname->get_one_banner($id);
		$data['sn_id']				=	$row->sn_id;
		$data['sn_name'] 			=	$row->sn_name;
		$data['sn_price'] 			=	$row->sn_price;
		$data['catid'] 				=	$row->sn_scanid;
		$data['categories'] 		=	$this->Scanname->get_category();
		 
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('scan_name_list',$data);
      	$this->load->view('templates/footer');  
	}
	function insert()
	{
 
		$this->tab_groups['sn_name']	=	strtoupper($this->input->post('sc_test'));
		$this->tab_groups['sn_price']	=	$this->input->post('sn_price');
		$this->tab_groups['sn_scanid']	=	$this->input->post('sn_scanid');
		$this->db->insert('sc_name',$this->tab_groups);

		$id 								=	$this->db->insert_id();
		$this->session->set_flashdata('Success','Test Name Created');
		redirect($this->base_uri."/add");
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Testsubcategory";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Scanname->get_one_banner($id);
		$data['sn_id']				=	$row->sn_id;
		$data['sn_name'] 			=	$row->sn_name;
		$data['sn_price'] 			=	$row->sn_price;
		$data['catid'] 				=	$row->sn_scanid;
		$data['categories'] 		=	$this->Scanname->get_category();
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('scan_name_add',$data);
      	$this->load->view('templates/footer');  
	}

	 

	function update()
	{
		$id 						=	$this->input->post('sn_id');
	    $this->tab_groups['sn_name'] =	strtoupper($this->input->post('sn_name'));
		$this->tab_groups['sn_price'] =	$this->input->post('sn_price');
		$this->tab_groups['sn_scanid'] =	$this->input->post('sn_scanid');
		$this->db->update('sc_name', $this->tab_groups,array('sn_id'=>$id));
		$this->session->set_flashdata('Success','Expense Updated');
		redirect($this->base_uri);
	}


	function delete($id)
	{

         if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$this->db->delete('sc_name',array('sn_id'=>$id));
			$this->session->set_flashdata('Error','User Deleted');
		}
				echo "Record Deleted";
	}
}