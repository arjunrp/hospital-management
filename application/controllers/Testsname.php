<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testsname extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Testsname_model','Testsname',TRUE);
		$this->template->set('title','testsname');
		$this->base_uri 			=	$this->config->item('admin_url')."testsname";
	}

	function index()
	{
		$data['page_title']			=	"Test Name List";
		$data['output']				=	$this->Testsname->list_all();
		// $this->template->load('template','customer',$data);
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('test_name_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Testsname->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Test Name";
		$data['action']				=	$this->base_uri.'/insert';
		$data['tn_id']					=	"";
		$data['tn_name']			     =	"";
		$data['tn_price']			     =	"";
		$data['catid'] 				=	"";
		$data['categories'] 		=	$this->Testsname->get_category();
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('test_name_add',$data);
      	$this->load->view('templates/footer');  
	}





function View($id)
	{
		$data['page_title']			=	"Edit Test Name";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Testsname->get_one_banner($id);
		$data['tn_id']				=	$row->tn_id;
		$data['tn_name'] 			=	$row->tn_name;
		$data['tn_price'] 			=	$row->tn_price;
		$data['catid'] 				=	$row->tn_testid;
		$data['categories'] 		=	$this->Testsname->get_category();
		 
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('test_name_list',$data);
      	$this->load->view('templates/footer');  
	}
	function insert()
	{
 
		$this->tab_groups['tn_name']	=	$this->input->post('tn_name');
		$this->tab_groups['tn_price']	=	$this->input->post('tn_price');
		$this->tab_groups['tn_testid']	=	$this->input->post('tn_testid');
		$this->db->insert('test_name',$this->tab_groups);

		$id 								=	$this->db->insert_id();
		$this->session->set_flashdata('Success','Test Name Created');
		redirect($this->base_uri."/add");
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Testsubcategory";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Testsname->get_one_banner($id);
		$data['tn_id']				=	$row->tn_id;
		$data['tn_name'] 			=	$row->tn_name;
		$data['tn_price'] 			=	$row->tn_price;
		$data['catid'] 				=	$row->tn_testid;
		$data['categories'] 		=	$this->Testsname->get_category();
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('test_name_add',$data);
      	$this->load->view('templates/footer');  
	}

	 

	function update()
	{
		$id 						=	$this->input->post('tn_id');
	    $this->tab_groups['tn_name'] =	$this->input->post('tn_name');
		$this->tab_groups['tn_price'] =	$this->input->post('tn_price');
		$this->tab_groups['tn_testid'] =	$this->input->post('tn_testid');
		$this->db->update('test_name', $this->tab_groups,array('tn_id'=>$id));
		$this->session->set_flashdata('Success','Expense Updated');
		redirect($this->base_uri);
	}


	function delete($id)
	{

         if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$this->db->delete('test_name',array('tn_id'=>$id));
			$this->session->set_flashdata('Error','User Deleted');
		}
				echo "Record Deleted";
	}
}