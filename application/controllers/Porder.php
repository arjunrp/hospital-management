<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Porder extends MX_Controller 
{
  public $tab_groups;
  public $form_image;

  function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('logged_in'))
    {
      redirect($this->config->item('admin_url'));
    }
    $this->load->model('Porder_model','Porder',TRUE);
    $this->load->model('Product_model','Product',TRUE);
    $this->load->model('Purchase_model','Purchase',TRUE);
    $this->template->set('title','Purchase Order List');
    $this->base_uri       = $this->config->item('admin_url')."porder";
  }

  function index(){

          $data['page_title']     = "Purchase Orders";
          $data['output']         = $this->Porder->list_all();
          $this->load->view('templates/header');  
          $this->load->view('templates/sidebar');        
          $this->load->view('porder_list',$data);
          $this->load->view('templates/footer'); 

  }

  function add(){

    $data['page_title']     = "Add Purchase Order";
    $data['suppliers']    =   $this->Product->getSupplier();
    $data['products']     =   $this->Porder->getProduct();
    $data['po_pono']      =   $this->Porder->getPorderId();

     $this->load->view('templates/header');  
     $this->load->view('templates/sidebar');        
     $this->load->view('porder_add',$data);
     $this->load->view('templates/footer');  

  }

  function view($pid){

    $data['page_title']     = "View Purchase Order";
    $data['porders']        = $this->Porder->getPorderview($pid);
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('porder_view',$data);
    $this->load->view('templates/footer');
  }

  function getPrint(){
    $pid              = $_GET['pPrintid'];
    $data['company']  = $this->Purchase->getCompany();
    $data['porders']  = $this->Porder->getPorderview($pid);
    // $this->load->view('templates/header');       
    $this->load->view('po_print',$data);
  }

  function  purchase_Report(){
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('purchase_report');
    $this->load->view('templates/footer');
  }


  function porderAdd(){
    $this->tab_groups['po_pono']        = $this->input->post('po_pono');
    $po_date                            = $this->input->post('po_date');
    $po_date                            = date("Y-m-d",strtotime($po_date));
    $this->tab_groups['po_date']        = $po_date;
    $sp_id                              = $this->input->post('sp_id');
    if($sp_id==0 || $sp_id=='')
    {
      $datas['sp_vendor']               = $this->input->post('sp_vendor');
      $datas['sp_phone']                = $this->input->post('sp_phone');
      $this->db->insert('suppliers',$datas);
      $sp_id                            = $this->db->insert_id();
    }

    $this->tab_groups['po_supplier']    = $sp_id;
    $po_dlvry_date                      = $this->input->post('po_dlvry_date');
    $po_dlvry_date                      = date("Y-m-d",strtotime($po_dlvry_date));
    $this->tab_groups['po_dlvry_date']  = $po_dlvry_date;
    $this->tab_groups['po_dlvry_type']  = $this->input->post('po_dlvry_type');

    $this->tab_groups['po_total']       = $this->input->post('sum1');
    $this->tab_groups['po_sgst']        = $this->input->post('sgstot1');
    $this->tab_groups['po_cgst']        = $this->input->post('cgstot1');

    $this->tab_groups['po_amount']      = $this->input->post('po_amount');
    $this->tab_groups['po_user']        = $this->input->post('user_type');
    $this->db->insert('porder',$this->tab_groups);
    $porderid                           = $this->db->insert_id();

  foreach($this->input->post('pod_item') as $k => $v)
  {
    $inputs = array();
    $inputs['pod_poid']                 = $porderid;
    $inputs['pod_date']                 = $po_date;
    $inputs['pod_item']                 = $v;
    $inputs['pod_qty']                  = $this->input->post('pod_qty')[$k];
    $inputs['pod_offer_qty']            = $this->input->post('pod_offer_qty')[$k];
    $inputs['pod_unit']                 = $this->input->post('pod_unit')[$k];
    $inputs['pod_price']                = $this->input->post('pod_price')[$k];
    $inputs['pod_sgsta']                = $this->input->post('pod_sgsta')[$k];
    $inputs['pod_sgstp']                = $this->input->post('pod_sgstp')[$k];
    $inputs['pod_csgta']                = $this->input->post('pod_cgsta')[$k];
    $inputs['pod_cgstp']                = $this->input->post('pod_cgstp')[$k];
    $inputs['pod_total']                = $this->input->post('pod_total')[$k];
    $inputs['pod_item_id']              = $this->input->post('pod_item_id')[$k];

    $this->db->insert('porder_detail',$inputs);
  }
  print_r($porderid);
  
  }

  function edit($id){

    $data['page_title']       = "Edit Purchase Order";
    $data['porders']          = $this->Porder->getPorderview($id);
    $data['suppliers']        = $this->Product->getSupplier();
    $data['products']         = $this->Porder->getProduct();

     $this->load->view('templates/header');  
     $this->load->view('templates/sidebar');        
     $this->load->view('porder_edit',$data);
     $this->load->view('templates/footer');  
    }

    function porderUpdate(){

    $po_id                              = $this->input->post('po_id');
    $po_date                            = $this->input->post('po_date');
    $po_dlvry_date                      = $this->input->post('po_dlvry_date');
    $po_dlvry_date                      = date("Y-m-d",strtotime($po_dlvry_date));
    $this->tab_groups['po_dlvry_date']  = $po_dlvry_date;
    $this->tab_groups['po_dlvry_type']  = $this->input->post('po_dlvry_type');
    $this->tab_groups['po_total']       = $this->input->post('sum1');
    $this->tab_groups['po_sgst']        = $this->input->post('sgstot1');
    $this->tab_groups['po_cgst']        = $this->input->post('cgstot1');
    $this->tab_groups['po_amount']      = $this->input->post('po_amount');
    $this->tab_groups['po_user']        = $this->input->post('user_type');
    $this->db->update('porder',$this->tab_groups,array('po_id'=>$po_id));
    $this->db->delete('porder_detail',array('pod_poid'=>$po_id));

  foreach($this->input->post('pod_item') as $k => $v)
  {
    $inputs = array();
    $inputs['pod_poid']                 = $po_id;
    $inputs['pod_date']                 = $po_date;
    $inputs['pod_item']                 = $v;
    $inputs['pod_qty']                  = $this->input->post('pod_qty')[$k];
    $inputs['pod_offer_qty']            = $this->input->post('pod_offer_qty')[$k];
    $inputs['pod_unit']                 = $this->input->post('pod_unit')[$k];
    $inputs['pod_price']                = $this->input->post('pod_price')[$k];
    $inputs['pod_sgsta']                = $this->input->post('pod_sgsta')[$k];
    $inputs['pod_sgstp']                = $this->input->post('pod_sgstp')[$k];
    $inputs['pod_csgta']                = $this->input->post('pod_cgsta')[$k];
    $inputs['pod_cgstp']                = $this->input->post('pod_cgstp')[$k];
    $inputs['pod_total']                = $this->input->post('pod_total')[$k];
    $inputs['pod_item_id']              = $this->input->post('pod_item_id')[$k];

    $this->db->insert('porder_detail',$inputs);
  }
  print_r($po_id);
  
  }

    function delete($id)
  {

    if($_REQUEST['empid']) {
      $id   = $_REQUEST['empid']; 
      $row      = $this->Porder->getPorderview($id);
      $this->db->delete('porder',array('po_id'=>$id));
      $this->db->delete('porder_detail',array('pod_poid'=>$id));
   }
        echo "Record Deleted";
  }

  function get_package($sp_id){
    $query=$this->db->get_where('medtp',array('mt_supplier'=>$sp_id));
    $data=$query->result_array();
      if($query->num_rows()==0)
      { $data ="0"; }
      echo  json_encode($data);
    }
    function get_packages($sp_id){

    $this->db->select('medtp_detail.*,product.pd_sgst,product.pd_cgst');
    $this->db->from('medtp');
    $this->db->join('medtp_detail','medtp_detail.mtd_mtid = medtp.mt_id', 'inner'); 
    $this->db->join('product','medtp_detail.mtd_item_id = product.pd_code', 'inner'); 
    $this->db->where('medtp.mt_id',$sp_id);
    $query=$this->db->get();
    $data=$query->result_array();
      if($query->num_rows()==0)
      { $data ="0"; }
      echo  json_encode($data);
    }

    function order_complete($id)
    {
        $data['po_avail'] = "fd";
        $this->db->update('porder',$data,array('po_id'=>$id));
        redirect($this->base_uri);
    }

  
}