<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Investigation extends MX_Controller 
{
  public $tab_groups;
  public $form_image;
  public $patient_datas;

  function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('logged_in'))
    {
      redirect($this->config->item('admin_url'));
    }
    $this->load->model('Investigation_model','Investigation',TRUE);
    $this->load->model('Ipregister_model','Ipregister',TRUE);
    $this->load->model('Booking_model','Booking',TRUE);
    $this->load->model('Opregister_model','Opregister',TRUE);
    $this->load->model('Users_model','Users',TRUE);
    $this->load->model('Testsname_model','Testsname',TRUE);
    $this->load->model('Purchase_model','Purchase',TRUE);
    $this->load->model('Sale_model','Sales',TRUE);
    $this->load->model('Return_model','Return',TRUE);
    $this->template->set('title','Investigations');
    $this->base_uri       = $this->config->item('admin_url')."investigation";
  }

  function index()
  {
    $data['page_head']      = "Lab List";
    // $data['page_title']     = "Active IPs";
    $data['output']         = $this->Investigation->list_all();
     
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('investigation_list',$data);
      $this->load->view('templates/footer'); 
  }

  public function list_ajax()
  {
    echo $data['output']    = $this->Ipregister->list_all();
  }

  function add()
  {
    $data['page_title']   = "New Lab Test";

    $data['lab_tests']    = $this->Testsname->get_category();
    $data['patient']      = $this->Investigation->get_patientsop();

    $data['ippatient']    = $this->Investigation->get_patientsip();
    $data['lab_no']       = $this->Investigation->getLabId();

     
        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('investigation_add',$data);
        $this->load->view('templates/footer');  
  }

  function field_value($inv_id)
  {
    $data['page_title']   = "Investigation Result";
    $data['action']       = $this->base_uri.'/insert';
    $data['lab_contents'] = $this->Investigation->get_category($inv_id);
    $data['lab_bills']    = $this->Investigation->get_lab_bill($inv_id);
    // print_r($data['lab_contents']);
     
        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('inv_result_add',$data);
        $this->load->view('templates/footer');  
  }

  function edit($id)
  {
    $data['page_title']   = "Edit Lab Test";
    $data['lab_tests']    = $this->Testsname->get_category();
    $data['lab_bills']    = $this->Investigation->getInvestigationview($id);
    // print_r($data['lab_bills'] );
     
        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('investigation_edit',$data);
        $this->load->view('templates/footer');  
  }


  function view($id)
  {

    $data['page_title']   = "Lab Test View";
    $data['lab_contents'] = $this->Investigation->get_categorys($id);
    $data['lab_bills']    = $this->Investigation->getInvestigationview($id);
    // print_r($data['lab_bills']);
     
        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('investigation_view',$data);
        $this->load->view('templates/footer');
  }

    function getBillPrint(){

    $pid                  = $_GET['Printid'];
    $data['company']      = $this->Purchase->getCompany();
    $data['lab_bills']    = $this->Investigation->getInvestigationview($pid);
    // print_r($data['lab_bills'] );
    // $this->load->view('templates/header');       
    $this->load->view('lab_bill_print',$data);
  }

  function getResultPrint(){

    $pid                  = $_GET['Printid'];
    $data['company']      = $this->Purchase->getCompany();
    $data['lab_contents'] = $this->Investigation->get_categorys($pid);
    $data['lab_bills']    = $this->Investigation->getInvestigationview($pid);
    // print_r($data['lab_contents'] );
    // $this->load->view('templates/header');       
    $this->load->view('lab_result_print',$data);
  }

  function test_price()
  {
    $tname   = $this->input->post('tname');

      $this->db->select('tn_price');
      $this->db->from('test_name');
      $this->db->where('tn_id',$tname);
      $query      = $this->db->get();
      $data   = $query->row()->tn_price;
    print_r($data);
  }

  function get_discharge()
  {
    $ip_mrd   = $this->input->post('ip_mrd');

      $this->db->select('ip_discharge');
      $this->db->from('ip');
      $this->db->where('ip_mrd',$ip_mrd);
      $this->db->order_by('ip_ipno','desc');
      $this->db->limit(1);
      $query      = $this->db->get();
      if($query->num_rows()==0)
      {
        $data = "0";
      }
      else
      {
        $data   = $query->row()->ip_discharge;
      }
    print_r($data);
  }

  function getTestnames($c_id){

    $this->db->select('*');
    $this->db->from('test_name'); 
    $this->db->where('tn_testid',$c_id);
    $query=$this->db->get();
    $data=$query->result_array();
    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
  }



  function investigationAdd(){

    $this->tab_groups['ve_vno']           = $this->input->post('ve_vno');
    $ve_date                              = $this->input->post('ve_date');
    $ve_time                              = $this->input->post('ve_time');
    $ve_date                              = date("Y-m-d",strtotime($ve_date));
    $ve_time                              = date("H:i:s",strtotime($ve_time));
    $ve_date                              = $ve_date. " ".$ve_time;
    $this->tab_groups['ve_date']          = date("Y-m-d H:i:s",strtotime($ve_date));
    // exit();
    $this->tab_groups['ve_supplier']      = $this->input->post('ve_supplier');
    $this->tab_groups['ve_customer']      = $this->input->post('ve_customer');
    $ve_mrd                               = $this->input->post('ve_mrd');
    $this->tab_groups['ve_mrd']           = $ve_mrd;
    $this->tab_groups['ve_patient']       = $this->input->post('ve_patient');
    $this->tab_groups['ve_phone']         = $this->input->post('ve_phone');
    $this->tab_groups['ve_doctor']        = $this->input->post('ve_doctor');

    $this->tab_groups['ve_amount']        = $this->input->post('ve_age');
    $this->tab_groups['ve_pono']          = $this->input->post('ve_gender');

    $amount_paid                          = $this->input->post('lab_mrp');
    $this->tab_groups['ve_apayable']      = $amount_paid;
    $ip_op                                = $this->input->post('ip_op');
    $pay_type                             = $this->input->post('payment_type');
    
    if($pay_type=="True")
    {
      $this->tab_groups['ve_status']        = "cr";
    }
    else
    {
      $this->tab_groups['ve_status']        = "cc";
    }

    $this->tab_groups['ve_apaid']         = "0";
    $this->tab_groups['ve_balance']       = $amount_paid;
    $this->tab_groups['ve_pstaus']        = "NP";

    $this->tab_groups['ve_user']          = $this->input->post('user_type');

    $ve_type = "lbi";

    $this->tab_groups['ve_type']          = $ve_type;
    $this->db->insert('voucher_entry',$this->tab_groups);
    $testid                           = $this->db->insert_id();

    if($ve_mrd != "" || $ve_mrd != " ")
    {
      $this->patient_datas['p_phone']          =  $this->input->post('p_phone');
      $this->patient_datas['p_age']            =  $this->input->post('ve_age');
      $this->patient_datas['p_sex']            =  $this->input->post('ve_gender');
      $this->db->update('patient',$this->patient_datas,array('p_mrd_no'=>$ve_mrd));
    }
    

  foreach($this->input->post('ved_item') as $k => $v)
  {

    $inputs  = array();
    $inputs1 = array();
    $inputs['ved_veid']       = $testid;
    $inputs['ved_date']       = $ve_date;
    $stockpid                 = $this->input->post('ved_itemid')[$k];
    $inputs['ved_itemid']     = $stockpid;
    $inputs['ved_item']       = $v;

    $inputs['ved_total']      = $this->input->post('ved_total')[$k];
    $inputs['ved_gtotal']     = $this->input->post('ved_total')[$k];

    $this->db->insert('voucher_entry_detail',$inputs);
    $tnameid                           = $this->db->insert_id();

    // $this->db->select('*');
    // $this->db->from('test_content');
    // $this->db->where('tc_tnameid',$stockpid);
    // $query    = $this->db->get();
    // $rowcount = $query->result_array();

    //   foreach ($rowcount as $value) {

    //     $inputs1['ind_inid']        = $tnameid;
    //     $inputs1['ind_contentid']   = $value['tc_id'];
    //     $inputs1['ind_content']     = $value['tc_content'];
    //     $this->db->insert('investigation_detail',$inputs1);
    //   }

  } 

  print_r($testid);
  }

  function investigationUpdate($ve_idd){

    $ve_id                                = $this->input->post('ve_id');
    $ve_date                              = $this->input->post('ve_date');

    $this->tab_groups['ve_bill_no']       = "";

    $amount_paid                          = $this->input->post('lab_mrp');
    $this->tab_groups['ve_apayable']      = $amount_paid;
    $pay_type                             = $this->input->post('payment_type');

    $this->tab_groups['ve_amount']        = $this->input->post('ve_age');
    $this->tab_groups['ve_pono']          = $this->input->post('ve_gender');

    if($pay_type=="True")
    {
      $this->tab_groups['ve_status']        = "cr";
    }
    else
    {
      $this->tab_groups['ve_status']        = "cc";
    }

    $this->tab_groups['ve_apaid']         = "0";
    $this->tab_groups['ve_balance']       = $amount_paid;
    $this->tab_groups['ve_pstaus']        = "NP";

    $this->tab_groups['ve_user']          = $this->input->post('user_type');

    $this->db->update('voucher_entry',$this->tab_groups,array('ve_id'=>$ve_id));
    $sql = "DELETE t1 FROM `mw_investigation_detail` t1 JOIN `mw_voucher_entry_detail` t2 ON t1.ind_inid  = t2.ved_id WHERE t2.ved_veid = ".$ve_id;
    $this->db->query($sql);
    $this->db->delete('voucher_entry_detail',array('ved_veid'=>$ve_id));

  foreach($this->input->post('ved_item') as $k => $v)
  {

    $inputs  = array();
    $inputs1 = array();
    $inputs['ved_veid']       = $ve_id;
    $inputs['ved_date']       = $ve_date;
    $stockpid                 = $this->input->post('ved_itemid')[$k];
    $inputs['ved_itemid']     = $stockpid;
    $inputs['ved_item']       = $v;

    $inputs['ved_total']      = $this->input->post('ved_total')[$k];
    $inputs['ved_gtotal']     = $this->input->post('ved_total')[$k];

    $this->db->insert('voucher_entry_detail',$inputs);
    $tnameid                           = $this->db->insert_id();

  } 
  print_r($ve_id);
  }

  function resultAdd(){

    foreach($this->input->post('ind_inid') as $k => $v)
    {
      $inputs = array();
      $inputs['ind_inid']         = $v;
      $inputs['ind_contentid']    = $this->input->post('ind_contentid')[$k];
      $inputs['ind_content']      = $this->input->post('ind_content')[$k];
      $inputs['ind_result']       = $this->input->post('ind_result')[$k];
      $inputs['ind_remarks']      = $this->input->post('ind_remarks')[$k];
      $this->db->insert('investigation_detail',$inputs);
    }

      $ve_id                        = $this->input->post('ve_id');
      $data['ve_bill_no']           = "updated";
      $this->db->update('voucher_entry',$data,array('ve_id'=>$ve_id));

      // print_r($testid);
  }

  function resultUpdate(){

    foreach($this->input->post('ind_id') as $k => $v)
    {
      $inputs = array();
      $inputs['ind_result']         = $this->input->post('ind_result')[$k];
      $inputs['ind_remarks']        = $this->input->post('ind_remarks')[$k];
      $this->db->update('investigation_detail',$inputs,array('ind_id'=>$v));
    }
      $ve_id                        = $this->input->post('ve_id');
      print_r($ve_id);
  }

  function delete($id)
  {

    if($_REQUEST['empid']) {
      $id   = $_REQUEST['empid']; 
      $row           = $this->Investigation->get_one_banner($id);
      $ve_apayable   = $row->ve_apayable;
      $ve_date       = $row->ve_date;
      $ve_status     = $row->ve_status;
      if($ve_status=="acc" || $ve_status=="acr")
      {
        $this->Return->dlt_purchase_balance($ve_apayable,$ve_date);
      }

      $this->db->delete('voucher_entry',array('ve_id'=>$id));
      $sql = "DELETE t1 FROM `mw_investigation_detail` t1 JOIN `mw_voucher_entry_detail` t2 ON t1.ind_inid  = t2.ved_id WHERE t2.ved_veid = ".$id;
    $this->db->query($sql);
    $this->db->delete('voucher_entry_detail',array('ved_veid'=>$id));

   }
        echo "Record Deleted";
  }

  function get_gender($mrd)
  {

      $this->db->select('p_sex');
      $this->db->from('patient');
      $this->db->where('p_mrd_no',$mrd);
      $query1  = $this->db->get();
      $data  = $query1->result_array();

      if($query1->num_rows()==0)
      { $data ="0"; }
       echo  json_encode($data);
  }

}