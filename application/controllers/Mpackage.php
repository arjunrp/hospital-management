<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpackage extends MX_Controller 
{
  public $tab_groups;
  public $form_image;

  function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('logged_in'))
    {
      redirect($this->config->item('admin_url'));
    }
    $this->load->model('Mpackage_model','Mpackage',TRUE);
    $this->load->model('Porder_model','Porder',TRUE);
    $this->load->model('Product_model','Product',TRUE);
    $this->template->set('title','Purchase Order List');
    $this->base_uri       = $this->config->item('admin_url')."mpackage";
  }

  function index(){

          $data['page_title']     = "Product Packages";
          $data['output']         = $this->Mpackage->list_all();
          $this->load->view('templates/header');  
          $this->load->view('templates/sidebar');        
          $this->load->view('mpackage_list',$data);
          $this->load->view('templates/footer'); 

  }

  function add(){

    $data['page_title']     = "Add Product Package";
    $data['suppliers']      =  $this->Product->getSupplier();
    $data['products']       =  $this->Porder->getProduct();
    // print_r($data['products']);
    $data['mt_pkgno']       =  $this->Mpackage->getPackageId();

     $this->load->view('templates/header');  
     $this->load->view('templates/sidebar');        
     $this->load->view('mpackage_add',$data);
     $this->load->view('templates/footer');  

  }

  function view($pid){

    $data['page_title']     = "View Product Package";
    $data['mpackages']      = $this->Mpackage->getPackageview($pid);
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('mpackage_view',$data);
    $this->load->view('templates/footer');
  }

  function getPrint(){
    $pid              = $_GET['pPrintid'];
    $data['company']  = $this->Purchase->getCompany();
    $data['purchase'] = $this->Purchase->getPurchaseview($pid);
    $this->load->view('templates/header');       
    $this->load->view('p_print',$data);
  }

  function  purchase_Report(){
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('purchase_report');
    $this->load->view('templates/footer');
  }


  function mpackageAdd(){
    $this->tab_groups['mt_pkgno']        = $this->input->post('mt_pkgno');
    $this->tab_groups['mt_pakcage']      = $this->input->post('mt_pakcage');
    $this->tab_groups['mt_supplier']     = $this->input->post('mt_supplier');
    $this->tab_groups['mt_user']         = $this->input->post('user_type');
    $this->db->insert('medtp',$this->tab_groups);
    $mt_id                               = $this->db->insert_id();

  foreach($this->input->post('pod_item') as $k => $v)
  {
    $inputs = array();
    $inputs['mtd_mtid']                 = $mt_id;
    $inputs['mtd_item']                 = $v;
    $inputs['mtd_item_id']              = $this->input->post('pod_item_id')[$k];
    $inputs['mtd_qty']                  = $this->input->post('pod_qty')[$k];
    $inputs['mtd_unit']                 = $this->input->post('pod_unit')[$k];
    $inputs['mtd_price']                = $this->input->post('pod_price')[$k];
    $inputs['mtd_cgstp']                = $this->input->post('mtd_cgstp')[$k];
    $inputs['mtd_cgsta']                = $this->input->post('mtd_cgsta')[$k];
    $inputs['mtd_sgstp']                = $this->input->post('mtd_sgstp')[$k];
    $inputs['mtd_sgsta']                = $this->input->post('mtd_sgsta')[$k];
    $inputs['mtd_total']                = $this->input->post('pod_total')[$k];

    $this->db->insert('medtp_detail',$inputs);
  }
  print_r($mt_id);
  
  }

  function edit($id){

    $data['page_title']       = "Edit Product Package";
    $data['action']           = $this->base_uri.'/update';
    $data['mpackages']        = $this->Mpackage->getPackageview($id);
    // print_r($data['mpackages']);
    $data['suppliers']        = $this->Product->getSupplier();
    $data['products']         = $this->Porder->getProduct();
    $data['mp_id']            = $id;

     $this->load->view('templates/header');  
     $this->load->view('templates/sidebar');        
     $this->load->view('mpackage_edit',$data);
     $this->load->view('templates/footer');  
    }

    function mpackageUpdate(){
    $mt_id                              = $this->input->post('mt_id');
    $this->tab_groups['mt_pkgno']       = $this->input->post('mt_pkgno');
    $this->tab_groups['mt_pakcage']     = $this->input->post('mt_pakcage');
    $this->tab_groups['mt_supplier']    = $this->input->post('mt_supplier');
    $this->tab_groups['mt_user']        = $this->input->post('user_type');
    $this->db->update('medtp',$this->tab_groups,array('mt_id'=>$mt_id));
    $this->db->delete('medtp_detail',array('mtd_mtid'=>$mt_id));

  foreach($this->input->post('pod_item') as $k => $v)
  {
    $inputs = array();
    $inputs['mtd_mtid']                 = $mt_id;
    $inputs['mtd_item_id']              = $this->input->post('pod_item_id')[$k];
    $inputs['mtd_item']                 = $v;
    $inputs['mtd_qty']                  = $this->input->post('pod_qty')[$k];
    $inputs['mtd_unit']                 = $this->input->post('pod_unit')[$k];
    $inputs['mtd_price']                = $this->input->post('pod_price')[$k];
    $inputs['mtd_cgstp']                = $this->input->post('mtd_cgstp')[$k];
    $inputs['mtd_cgsta']                = $this->input->post('mtd_cgsta')[$k];
    $inputs['mtd_sgstp']                = $this->input->post('mtd_sgstp')[$k];
    $inputs['mtd_sgsta']                = $this->input->post('mtd_sgsta')[$k];
    $inputs['mtd_total']                = $this->input->post('pod_total')[$k];
    $this->db->insert('medtp_detail',$inputs);
  }
  print_r($mt_id);
  
  }

    function delete($id)
  {

    if($_REQUEST['empid']) {
      $id   = $_REQUEST['empid']; 
      $row      = $this->Mpackage->getPackageview($id);
      $this->db->delete('medtp',array('mt_id'=>$id));
      $this->db->delete('medtp_detail',array('mtd_mtid'=>$id));
   }
        echo "Record Deleted";
  }


  
}