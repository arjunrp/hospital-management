<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ccounter extends MX_Controller 
{
  public $tab_groups;
  public $form_image;
  public $tab_data;

  function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('logged_in'))
    {
      redirect($this->config->item('admin_url'));
    }
    $this->load->model('Ccounter_model','Ccounter',TRUE);
    $this->load->model('Sale_model','Sales',TRUE);
    $this->load->model('Purchase_model','Purchase',TRUE);
    $this->load->model('Stock_model','Stock',TRUE);
    $this->load->model('Investigation_model','Investigation',TRUE);
    $this->load->model('Discharge_model','Discharge',TRUE);
    $this->load->model('Advance_model','Advance',TRUE);
    $this->load->model('Opbilling_model','Opbilling',TRUE);
    $this->load->model('Addbilling_model','Addbilling',TRUE);
    $this->load->model('Scanning_model','Scanning',TRUE);
    $this->load->model('Xraytest_model','Xraytest',TRUE);
    $this->template->set('title','Cash Payment');
    $this->base_uri       = $this->config->item('admin_url')."ccounter";
  }

  function index()
  {

    $data['output']         =  $this->Ccounter->list_all();
    // print_r($data['output']);

    // $data['sale_detail']=$this->Sales->getAbout();
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('ccounter_list',$data);
    $this->load->view('templates/footer'); 
  }


  function todays_report(){

    $data['treports']       =  $this->Ccounter->getTodayreport();
    //  print_r($data['treports'] );
    // exit();
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('tcash_report',$data);
    $this->load->view('templates/footer');
  }

   function reg_report(){

    $data['reg_reports']       =  $this->Ccounter->getToday_reg_report();
     // print_r($data['treports'] );
    // exit();
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('reg_cash_report',$data);
    $this->load->view('templates/footer');
  }

function ccounterreport(){

    $data['action']        = $this->base_uri.'/total_cash_report';
    $data['action1']       = "";
    $data['treports']      =  0;
    $data['departments']   =  $this->Ccounter->getDepartment();

    $data['from_date1']    =  "";
    $data['to_date1']      =  "";
    $data['dept1']         =  "";
    $data['type']          =  "";

    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('totcash_report',$data);
    $this->load->view('templates/footer');
  }

  function total_cash_report(){

    $data['action']       = $this->base_uri.'/total_cash_report';

    $data['action1']      = $this->base_uri.'/totreport_print';

    $from_date            = date("Y-m-d",strtotime($this->input->post('from_date')));
    $to_date              = date("Y-m-d",strtotime($this->input->post('to_date')));
    $dept                 = $this->input->post('dept');
    $f_type               = $this->input->post('f_type');
    $b_type               = $this->input->post('b_type');

    if($f_type=="d")
    {
    
      if($dept=="17")
      {
        $data['treports']     =  $this->Ccounter->getTotalCreport_OP($from_date,$to_date,$dept);
      }
      else if($dept=="16")
      {
        $data['treports']     =  $this->Ccounter->getTotalCreport_IP($from_date,$to_date,$dept);
      }
      else
      {
         $data['treports']     =  $this->Ccounter->getTotalCreport($from_date,$to_date,$dept);
      }
    }
    else
    {

      if($dept=="17")
      {
        $data['sreports']     =  $this->Ccounter->getSummaryCreport_OP($from_date,$to_date,$dept);
      }
      else if($dept=="3")
      {
        $data['sreports']     =  $this->Ccounter->getSummaryCreport_Pharma($from_date,$to_date,$b_type);
        // print_r($data['sreports']);
      }
      else
      {
        $data['sreports']     =  $this->Ccounter->getSummaryCreport($from_date,$to_date,$dept);
      }
    }

    $data['departments']  =  $this->Ccounter->getDepartment();

    $data['from_date1']   =  $from_date ;
    $data['to_date1']     =  $to_date;
    $data['dept1']        =  $dept;
    $data['type']         =  $f_type;
    $data['b_type1']      =  $b_type;
    // print_r($data['treports']);

    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('totcash_report',$data);
    $this->load->view('templates/footer');
  }

  function totreport_print(){

    $from_date            = date("Y-m-d",strtotime($this->input->post('from_date1')));
    $to_date              = date("Y-m-d",strtotime($this->input->post('to_date1')));
    $dept                 = $this->input->post('dept1');
    $f_type               = $this->input->post('f_type1');
    $b_type               = $this->input->post('b_type1');

    if($dept == 0 || $dept == "")
    {
      $data['dept']         = "All";
    }
    else
    {
      $data['dept']         = $this->Ccounter->get_department($dept);
    }

     if($f_type=="d")
    {
    
      if($dept=="17")
      {
        $data['treports']     =  $this->Ccounter->getTotalCreport_OP($from_date,$to_date,$dept);
      }
      else if($dept=="16")
      {
        $data['treports']     =  $this->Ccounter->getTotalCreport_IP($from_date,$to_date,$dept);
      }
      else
      {
         $data['treports']     =  $this->Ccounter->getTotalCreport($from_date,$to_date,$dept);
      }
    }
    else
    {

      if($dept=="17")
      {
        $data['sreports']     =  $this->Ccounter->getSummaryCreport_OP($from_date,$to_date,$dept);
      }
      else if($dept=="3")
      {
        $data['sreports']     =  $this->Ccounter->getSummaryCreport_Pharma($from_date,$to_date,$b_type);
        // print_r($data['sreports']);
      }
      else
      {
        $data['sreports']     =  $this->Ccounter->getSummaryCreport($from_date,$to_date,$dept);
      }
    }

    $data['from_date']   =  $from_date;
    $data['to_date']     =  $to_date;
    $data['type']        =  $f_type;
    // print_r($from_date);
    // print_r($to_date);
    // print_r($dept);
    // print_r($data['treports']);
    // exit();
    $this->load->view('totcash_print',$data);
  }

  function treport_print(){

    $data['treports']       =  $this->Ccounter->getTodayreport();
    // print_r($data['treports'] );
    // exit();       
    $this->load->view('tcash_print',$data);
  }

  function reg_report_print(){

    $data['reg_reports']       =  $this->Ccounter->getToday_reg_report();
   
    $this->load->view('reg_cash_print',$data);
  }

  function cash_PView($sid){

    $data['page_title']    = "Cash Payment";
    $data['action']        = $this->base_uri.'/insert';
    $data['sales']         = $this->Ccounter->getCashview($sid);
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('ccounter_add', $data);
    $this->load->view('templates/footer');  
  }

  function cash_OView($sid){

    $data['page_title']    = "Cash Payment";
    $data['action']        = $this->base_uri.'/insert';
    $data['additionals']   = $this->Opbilling->get_additional_items($sid);
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('ccounter_oadd', $data);
    $this->load->view('templates/footer');  
  }

  function cash_ABView($sid){

    $data['page_title']    = "Cash Payment";
    $data['action']        = $this->base_uri.'/insert1';
    $data['additionals']   = $this->Addbilling->get_additional_items($sid);
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('ccounter_abadd', $data);
    $this->load->view('templates/footer');  
  }

  function cash_DView($ipno){

    $data['page_title']    = "Cash Payment";
    $data['action']        = $this->base_uri.'/update';
    $data['discharges']       = $this->Discharge->get_discharge($ipno);
    // print_r($data['discharges']);
    $data['rooms']            = $this->Discharge->get_room($ipno);
    $data['voucher_bills']    = $this->Discharge->get_voucher_bills($ipno);
    $data['scanning_bills']   = $this->Discharge->get_scanning_bills($ipno);
    $data['xray_bills']       = $this->Discharge->get_xray_bills($ipno);
    $data['pharmacy_bills']   = $this->Discharge->get_pharmacy_bills($ipno);
    $data['pharmacy_returns'] = $this->Discharge->get_pharmacy_returns($ipno);
    $data['advances']         = $this->Discharge->get_advances($ipno);
    $data['additionals']      = $this->Discharge->get_additional_items($ipno);
    // print_r($data['sales']);
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('ccounter_dadd', $data);
    $this->load->view('templates/footer');  
  }

  function cash_LView($sid){

    $data['page_title']    = "Cash Payment";
    $data['action']    = $this->base_uri.'/insert';
    $data['lab_bills']    = $this->Investigation->getInvestigationview($sid);
    // print_r($data['sales']);
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('ccounter_ladd', $data);
    $this->load->view('templates/footer');  
  }

  function cash_ScView($sid){

    $data['page_title']    = "Cash Payment";
    $data['action']    = $this->base_uri.'/insert';
    $data['scan_bills']     = $this->Scanning->getScanview($sid);
    // print_r($data['sales']);
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('ccounter_scadd', $data);
    $this->load->view('templates/footer');  
  }

  function cash_XrView($sid){

    $data['page_title']    = "Cash Payment";
    $data['action']        = $this->base_uri.'/insert';
    $data['xray_bills']    = $this->Xraytest->getXrayview($sid);
    // print_r($data['sales']);
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('ccounter_xradd', $data);
    $this->load->view('templates/footer');  
  }

  function cash_AView($sid){

    $data['page_title']    = "Cash Payment";
    $data['action']        = $this->base_uri.'/insert';
    $row                   = $this->Advance->get_one_banner($sid);
    $data['ve_id']         = $row->ve_id;
    $data['ve_vno']        = $row->ve_vno;
    $data['ve_date']       = $row->ve_date;
    $data['ve_customer']   = $row->ve_customer;
    $data['ve_mrd']        = $row->ve_mrd;
    $data['p_name']        = $row->p_title." ".$row->p_name;
    $data['p_phone']       = $row->p_phone;
    $data['ve_apayable']   = $row->ve_apayable;


    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('ccounter_aadd', $data);
    $this->load->view('templates/footer');  
  }

   function insert($ve_id,$sale_date,$amount_paid)
  {
    // $ve_id                               =  $this->input->post('ve_id');
    // $sale_date                           =  $this->input->post('ve_date');
    // $amount_paid                         =  $this->input->post('grandtotal');
    $this->tab_groups['ve_apaid']        =  $amount_paid;
    $this->tab_groups['ve_balance']      =  "0";
    $this->tab_groups['ve_pstaus']       =  "FP";
    $this->tab_groups['ve_status']       =  "acc";
    $this->tab_groups['ve_cash_date']    =  date("Y-m-d");

    $this->db->update('voucher_entry',$this->tab_groups,array('ve_id'=>$ve_id));

    $this->Sales->set_sale_balance($amount_paid,$sale_date);

    $this->session->set_flashdata('Success','Cash Received');
    redirect($this->base_uri);
  }

  function insert1($ve_id,$sale_date,$amount_paid)
  {
    // $ve_id                               =  $this->input->post('ve_id');
    // $sale_date                           =  $this->input->post('ve_date');
    // $amount_paid                         =  $this->input->post('grandtotal');
    $this->tab_groups['ve_apaid']        =  $amount_paid;
    $this->tab_groups['ve_balance']      =  "0";
    $this->tab_groups['ve_pstaus']       =  "FP";
    $this->tab_groups['ve_status']       =  "acc";
    $this->tab_groups['ve_cash_date']    =  date("Y-m-d");

    $this->db->update('voucher_entry',$this->tab_groups,array('ve_id'=>$ve_id));

    $this->session->set_flashdata('Success','Cash Received');
    redirect($this->base_uri);
  }

  function insert2($ve_id,$sale_date,$amount_paid)
  {
    // $ve_id                               =  $this->input->post('ve_id');
    // $sale_date                           =  $this->input->post('ve_date');
    // $amount_paid                         =  $this->input->post('grandtotal');
    $this->tab_groups['ve_apaid']        =  $amount_paid;
    $this->tab_groups['ve_balance']      =  "0";
    $this->tab_groups['ve_pstaus']       =  "FP";
    $this->tab_groups['ve_status']       =  "acc";
    $this->tab_groups['ve_cash_date']    =  date("Y-m-d");

    $this->db->update('voucher_entry',$this->tab_groups,array('ve_id'=>$ve_id));

    $this->Purchase->set_purchase_balance($amount_paid,$sale_date);

    $this->session->set_flashdata('Success','Cash Received');
    redirect($this->base_uri);
  }

  function update($ve_id,$sale_date,$ve_customer,$amount_paid)
  {

    $this->tab_groups['ve_apaid']        =  $amount_paid;
    $this->tab_groups['ve_balance']      =  "0";
    $this->tab_groups['ve_pstaus']       =  "FP";
    $this->tab_groups['ve_status']       =  "acc";
    $this->tab_groups['ve_cash_date']    =  date("Y-m-d");

    $this->Sales->set_sale_balance($amount_paid,$sale_date);
    $this->db->update('voucher_entry',$this->tab_groups,array('ve_id'=>$ve_id));

    $this->db->select('*');
    $this->db->from('voucher_entry');
    $this->db->where('ve_customer',$ve_customer);
    $this->db->where('ve_status','cr');
    $query = $this->db->get();
    $result= $query->result_array();

    foreach ($result as $value) {

      $ve_apaid = $value['ve_apayable'];
      $ve_id    = $value['ve_id'];

      $this->tab_data['ve_apaid']        = $ve_apaid;
      $this->tab_data['ve_balance']      = "0";
      $this->tab_data['ve_pstaus']       = "FP";
      $this->tab_data['ve_status']       = "acr";
      $this->tab_data['ve_cash_date']    =  date("Y-m-d");
      $this->db->update('voucher_entry',$this->tab_data,array('ve_id'=>$ve_id));
    }

    $this->db->select('*');
    $this->db->from('voucher_entry');
    $this->db->where('ve_customer',$ve_customer);
    $this->db->where('ve_type','ad');
    $query1  = $this->db->get();
    if($query1->num_rows() > 0)
    {
      $result1 = $query1->result_array();
      foreach ($result1 as $value1) {

     $this->tab_data['ve_bill_no']      =  $value1['ve_vno'];
     $this->tab_data['ve_vno']          =  $this->Ccounter->getAdvReturnId();
     $this->tab_data['ve_date']         =  date("Y-m-d");
     $this->tab_data['ve_cash_date']    =  date("Y-m-d");
     $this->tab_data['ve_supplier']     =  $value1['ve_supplier'];
     $this->tab_data['ve_customer']     =  $value1['ve_customer'];
     $this->tab_data['ve_mrd']          =  $value1['ve_mrd'];
     $this->tab_data['ve_apayable']     =  -$value1['ve_apayable'];
     $this->tab_data['ve_apaid']        =  -$value1['ve_apayable'];
     $this->tab_data['ve_balance']      = "0";
     $this->tab_data['ve_pstaus']       = "FP";
     $this->tab_data['ve_status']       = "acc";
     $this->tab_data['ve_type']         = "adr";
     $this->db->insert('voucher_entry',$this->tab_data);
    }
  }

    $this->session->set_flashdata('Success','Cash Received');
    redirect($this->base_uri);
  }

   function update1($ve_id,$sale_date,$ve_customer,$amount_paid)
  {

    $this->tab_groups['ve_apaid']        =  $amount_paid;
    $this->tab_groups['ve_balance']      =  "0";
    $this->tab_groups['ve_pstaus']       =  "FP";
    $this->tab_groups['ve_status']       =  "acc";
    $this->tab_groups['ve_cash_date']    =  date("Y-m-d");

    $this->Sales->set_sale_balance($amount_paid,$sale_date);
    $this->db->update('voucher_entry',$this->tab_groups,array('ve_id'=>$ve_id));

    $this->db->select('*');
    $this->db->from('voucher_entry');
    $this->db->where('ve_customer',$ve_customer);
    $this->db->where('ve_status','acr');
    $query = $this->db->get();
    $result= $query->result_array();

    foreach ($result as $value) {

      $ve_apaid = $value['ve_apayable'];
      $ve_id    = $value['ve_id'];

      $this->tab_data['ve_apaid']        = "0";
      $this->tab_data['ve_balance']      = $ve_apaid;
      $this->tab_data['ve_pstaus']       = "NP";
      $this->tab_data['ve_status']       = "cr";
      $this->tab_data['ve_cash_date']    = "0000-00-00";
      $this->db->update('voucher_entry',$this->tab_data,array('ve_id'=>$ve_id));
    }

    $this->session->set_flashdata('Success','Cash Received');
    redirect($this->base_uri);
  }

  function delete($ve_id)
  {
    $this->db->delete('voucher_entry',array('ve_id'=>$ve_id));
    $this->db->delete('voucher_entry_detail',array('ved_veid'=>$ve_id));
    redirect($this->base_uri);
  }




}