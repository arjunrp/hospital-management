<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}	
		$this->load->model('Product_model','Product',TRUE);
		$this->template->set('title','Product');
		$this->base_uri 			=	$this->config->item('admin_url')."product";
	}

	function index()
	{
		$data['page_title']			=	"Product List";
		$data['output']				=	$this->Product->list_all();
		// print_r($data['output']);
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('product_list',$data);
    	$this->load->view('templates/footer'); 
	}


	function view($id)
	{
		
		$data['page_title']			=	"Edit Product";
		$row 						=	$this->Product->get_one_banner($id);
		$data['pd_id']				=	$id;
		$data['pd_code']			=	$row->pd_code;
		$data['pd_product']			=	$row->pd_product;
		$data['pd_contentid']		=	$row->pd_contentid;
		$data['pd_brandid']			=	$row->pd_brandid;
		$data['pd_sgst']			=	$row->pd_sgst;
		$data['pd_cgst']			=	$row->pd_cgst;
		$data['pd_min']			    =	$row->pd_min;
		$data['pd_max']				=	$row->pd_max;
		$data['pd_supplier']		=	$row->pd_supplier;
		$data['pd_unit']	    	=	$row->pd_unit;
		$data['pd_qty']			    =	$row->pd_qty;
		$data['pd_type']			=	$row->pd_type;
		$data['pd_catg']			=	$row->pd_catg;
		$data['image']				=	$row->pd_image;	

		$data['contents']			=	$this->Product->getContent();
		$data['suppliers']			=	$this->Product->getSupplier();
		$data['brands']	    		=	$this->Product->getBrand();
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('product_view',$data);
      	$this->load->view('templates/footer'); 
	}




	public function list_ajax()
	{
		echo $data['output']		=	$this->Product->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add product";
		$data['action']				=	$this->base_uri.'/insert';
		$data['pd_id']				=	"";
		$data['pd_code']			=	$this->Product->get_product_code();
		$data['pd_product']			=	"";
		$data['pd_contentid']		=	"";
		$data['pd_brandid']			=	"";
		$data['pd_sgst']			=	"";
		$data['pd_cgst']	    	=	"";
		$data['pd_min']			    =	"";
		$data['pd_max']			    =	"";
		$data['pd_supplier']		=	"";
		$data['pd_unit']			=	"";
		$data['pd_qty']				=	"";
		$data['pd_type']			=	"";
		$data['pd_catg']			=	"";
		$data['contents']			=	$this->Product->getContent();
		$data['suppliers']			=	$this->Product->getSupplier();
		$data['brands']	    		=	$this->Product->getBrand();
		$data['image']			    =	"";
		 

		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('product_add',$data);
      	$this->load->view('templates/footer');  
	}

	function insert()
	{

		 
		$this->tab_groups['pd_code']	    =	$this->input->post('pd_code');
		$this->tab_groups['pd_product']		=	$this->input->post('pd_product');
		$this->tab_groups['pd_contentid']	=	implode('+',$this->input->post('pd_contentid'));
		$this->tab_groups['pd_brandid']	    =	$this->input->post('pd_brandid');
		$this->tab_groups['pd_sgst']		=	$this->input->post('pd_sgst');
		$this->tab_groups['pd_cgst']	    =	$this->input->post('pd_cgst');
		$this->tab_groups['pd_min']	        =	$this->input->post('pd_min');
		$this->tab_groups['pd_max']			=	$this->input->post('pd_max');
		$this->tab_groups['pd_supplier']	=	implode(',',$this->input->post('pd_supplier'));
		$this->tab_groups['pd_unit']		=	$this->input->post('pd_unit');
		$this->tab_groups['pd_qty']			=	$this->input->post('pd_qty');
		$this->tab_groups['pd_type']		=	$this->input->post('pd_type');
		$this->tab_groups['pd_catg']		=	strtoupper($this->input->post('pd_catg'));
		// print_r($this->tab_groups['pd_contentid']);
		$this->db->insert('product',$this->tab_groups);
		$id 								=	$this->input->post('pd_code');
		$config['upload_path']				=	$this->config->item('image_path').'product/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'20000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ($this->upload->do_upload('image'))
		{		
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['pd_image']=	$data['upload_data']['file_name'];
			$this->db->update('product', $this->form_image,array('pd_code'=>$id));
		}	
		else
		{
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
		}
		delete_files($this->config->item('image_path').'product/original/');
		$this->session->set_flashdata('Success','Product Updated');
		redirect($this->base_uri."/add");
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Product";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Product->get_one_banner($id);
		$data['pd_id']				=	$row->pd_id;
		$data['pd_code']			=	$id;
		$data['pd_product']			=	$row->pd_product;
		$data['pd_contentid']		=	$row->pd_contentid;
		$data['pd_brandid']			=	$row->pd_brandid;
		$data['pd_sgst']			=	$row->pd_sgst;
		$data['pd_cgst']			=	$row->pd_cgst;
		$data['pd_min']			    =	$row->pd_min;
		$data['pd_max']				=	$row->pd_max;
		$data['pd_supplier']		=	$row->pd_supplier;
		$data['pd_unit']	    	=	$row->pd_unit;
		$data['pd_qty']			    =	$row->pd_qty;
		$data['image']				=	$row->pd_image;	
		$data['pd_type']			=	$row->pd_type;
		$data['pd_catg']			=	$row->pd_catg;

		$data['contents']			=	$this->Product->getContent();
		$data['suppliers']			=	$this->Product->getSupplier();
		$data['brands']	    		=	$this->Product->getBrand();
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('product_add',$data);
      	$this->load->view('templates/footer');  
	}

	function update()
	{
		$id 								=	$this->input->post('pd_code');
		 
		$this->tab_groups['pd_code']	    =	$this->input->post('pd_code');
		$this->tab_groups['pd_product']		=	$this->input->post('pd_product');
		$this->tab_groups['pd_contentid']	=	implode('+',$this->input->post('pd_contentid'));
		$this->tab_groups['pd_brandid']	    =	$this->input->post('pd_brandid');
		$this->tab_groups['pd_sgst']		=	$this->input->post('pd_sgst');
		$this->tab_groups['pd_cgst']	    =	$this->input->post('pd_cgst');
		$this->tab_groups['pd_min']	        =	$this->input->post('pd_min');
		$this->tab_groups['pd_max']			=	$this->input->post('pd_max');
		$this->tab_groups['pd_supplier']	=	implode(',',$this->input->post('pd_supplier'));
		$this->tab_groups['pd_unit']		=	$this->input->post('pd_unit');
		$this->tab_groups['pd_qty']			=	$this->input->post('pd_qty');
		$this->tab_groups['pd_type']		=	$this->input->post('pd_type');
		$this->tab_groups['pd_catg']		=	strtoupper($this->input->post('pd_catg'));
		$this->db->update('product', $this->tab_groups,array('pd_code'=>$id));

		$pd_code 							=	$id;

		$config['upload_path']				=	$this->config->item('image_path').'product/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'20000';
		@$config['file_name']				=	$pd_code.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (  $this->upload->do_upload('image'))
		{		
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['pd_image']=	$data['upload_data']['file_name'];
			$this->db->update('product', $this->form_image,array('pd_code'=>$pd_code));
		}	
		else
		{
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
		}
		delete_files($this->config->item('image_path').'product/original/');
		$this->session->set_flashdata('Success','Product Updated');
		redirect($this->base_uri);
	}

	function process_uploaded_image($filename)
	{
		$large_path 	=	$this->config->item('image_path').'product/original/'.$filename;
		$medium_path 	=	$this->config->item('image_path').'product/'.$filename;
		$thumb_path 	=	$this->config->item('image_path').'product/small/'.$filename;
		
		$medium_config['image_library'] 	=	'gd2';
		$medium_config['source_image'] 		=	$large_path;
		$medium_config['new_image'] 		=	$medium_path;
		$medium_config['maintain_ratio'] 	=	TRUE;
		$medium_config['width'] 			=	1600;
		$medium_config['height'] 			=	400;
		$this->load->library('image_lib', $medium_config);
		$this->image_lib->initialize($medium_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		$thumb_config['image_library'] 		=	'gd2';
		$thumb_config['source_image'] 		=	$large_path;
		$thumb_config['new_image'] 			=	$thumb_path;
		$thumb_config['maintain_ratio'] 	=	TRUE;
		$thumb_config['width'] 				=	50;
		$thumb_config['height'] 			=	50;
		$this->load->library('image_lib', $thumb_config);
		$this->image_lib->initialize($thumb_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		return true;    
	}

	function delete($id)
	{

		if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$row 			=	$this->Product->get_one_banner($id);
			unlink($this->config->item('image_path').'product/small/'.$row->pd_image);
		 	unlink($this->config->item('image_path').'product/'.$row->pd_image);
			$this->db->delete('product',array('pd_code'=>$id));
		}
				echo "Record Deleted";
	}


}