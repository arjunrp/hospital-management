<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Customermodel','Customer',TRUE);
		$this->template->set('title','Customer');
		$this->base_uri 			=	$this->config->item('admin_url')."customer";
	}

	function index()
	{
		$data['page_title']			=	"Customers List";
		$data['output']				=	$this->Customer->list_all();
		// $this->template->load('template','customer',$data);
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('customer',$data);
    	$this->load->view('templates/footer'); 
	}

	function view($id)
	{
		$data['page_title']			=	"Customers View";
		$row 						=	$this->Customer->get_one_banner($id);
	    $data['id']					=	$row->id;
		$data['customer']			=	$row->customer;
		$data['address']			=	$row->address;
		$data['city']				=	$row->city;
		$data['cities']				=	$this->Customer->get_city();
		$data['zip']				=	$row->zip;
		$data['countries']			=	$this->Customer->get_country();
		$data['country']			=	$row->country;
		$data['states']				=	$this->Customer->get_state();
		$data['state']				=	$row->state;
		$data['phone']				=	$row->phone;
		$data['email']				=	$row->email;
		$data['fax']				=	$row->fax;
		$data['website']			=	$row->website;
		$data['street']			    =	$row->street;
		$data['payment']			=	$row->payment;
		$data['image']				=	$row->customer_image;
		$data['output']				=	$this->Customer->get_payment($id);
		$data['output1']			=	$this->Customer->get_payment_main($id);
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('customer_view',$data);
      	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Customer->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Customers";
		$data['action']				=	$this->base_uri.'/insert';
		$data['id']					=	"";
		$data['customer']			=	"";
		$data['address']			=	"";
		$data['cid'] 				=	"";
		$data['city']				=	"";
		$data['zip']				=	"";
		$data['ctid'] 				=	"";
		$data['countries']			=	$this->Customer->get_country();
		$data['country']			=	"";
		$data['stid'] 				=	"";
		$data['state']				=	"";
		$data['phone']				=	"";
		$data['email']				=	"";
		$data['fax']				=	"";
		$data['website']			=	"";
		$data['street']			    =	"";
		$data['payment']			=	"";
		$data['image']				=	"";
		// $data['image']				=	"";
		// $data['type']				=	"";
		// $this->template->load('template','features_add',$data);
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('customer_add',$data);
      	$this->load->view('templates/footer');  
	}

	function insert()
	{

		$title 							=	$this->input->post('customer');
		$this->tab_groups['customer']	=	$title;
		$this->tab_groups['address']	=	$this->input->post('address');
		$this->tab_groups['city']		=	$this->input->post('city');
		$this->tab_groups['zip']		=	$this->input->post('zip');
		$this->tab_groups['country']	=	$this->input->post('country');
		$this->tab_groups['state']		=	$this->input->post('state');
		$this->tab_groups['phone']		=	$this->input->post('phone');
		$this->tab_groups['email']		=	$this->input->post('email');
		$this->tab_groups['fax']		=	$this->input->post('fax');
		$this->tab_groups['website']	=	$this->input->post('website');
		$this->tab_groups['payment']	=	$this->input->post('payment');
		$this->tab_groups['street']	    =	$this->input->post('street');
		// $this->tab_groups['service_type']		=	$this->input->post('stype');
		$this->db->insert('customer',$this->tab_groups);

		$id 								=	$this->db->insert_id();
		$config['upload_path']				=	$this->config->item('image_path').'customer/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'20000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ($this->upload->do_upload('image'))
		{		
			
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['customer_image']	=	$data['upload_data']['file_name'];
			$this->db->update('customer', $this->form_image,array('id'=>$id));
			
		}	
		else
		{
		
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
			
		}

		delete_files($this->config->item('image_path').'customer/original/');
		$this->session->set_flashdata('Success','Customer Created');
		redirect($this->base_uri);
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Customers";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Customer->get_one_banner($id);
		$data['id']					=	$row->id;
		$data['customer']			=	$row->customer;
		$data['address']			=	$row->address;
		$data['city']				=	$row->city;
		$data['zip']				=	$row->zip;
		$data['country']			=	$row->country;
		$data['state']				=	$row->state;
		$data['phone']				=	$row->phone;
		$data['email']				=	$row->email;
		$data['payment']			=	$row->payment;
		$data['fax']				=	$row->fax;
		$data['website']			=	$row->website;
		$data['street']			    =	$row->street;
		$data['image']				=	$row->customer_image;
		$citys 						=	$row->state;
		$states 					=	$row->country;
		$data['cities'] 			=	$this->Customer->get_citys($citys);
		$data['countries'] 		    =	$this->Customer->get_country();
		$data['states'] 		    =	$this->Customer->get_states($states); 

		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('customer_add',$data);
      	$this->load->view('templates/footer');  
		// $this->template->load('template','features_add',$data);
	}

	function update()
	{
		$id 							=	$this->input->post('id');
		$title 							=	$this->input->post('customer');
		$this->tab_groups['customer']	=	$title;
		$this->tab_groups['address']	=	$this->input->post('address');
		$this->tab_groups['city']		=	$this->input->post('city');
		$this->tab_groups['zip']		=	$this->input->post('zip');
		$this->tab_groups['country']	=	$this->input->post('country');
		$this->tab_groups['state']		=	$this->input->post('state');
		$this->tab_groups['phone']		=	$this->input->post('phone');
		$this->tab_groups['email']		=	$this->input->post('email');
		$this->tab_groups['fax']		=	$this->input->post('fax');
		$this->tab_groups['website']	=	$this->input->post('website');
		$this->tab_groups['payment']	=	$this->input->post('payment');
		$this->tab_groups['street']	=	$this->input->post('street');
		$this->db->update('customer', $this->tab_groups,array('id'=>$id));

		$config['upload_path']				=	$this->config->item('image_path').'customer/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'20000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (  $this->upload->do_upload('image'))
		{		
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['customer_image']=	$data['upload_data']['file_name'];
			$this->db->update('customer', $this->form_image,array('id'=>$id));
		}	
		else
		{
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
		}
		delete_files($this->config->item('image_path').'customer/original/');
		$this->session->set_flashdata('Success','Customer Updated');
		redirect($this->base_uri);
	}

	function process_uploaded_image($filename)
	{
		$large_path 	=	$this->config->item('image_path').'customer/original/'.$filename;
		$medium_path 	=	$this->config->item('image_path').'customer/'.$filename;
		$thumb_path 	=	$this->config->item('image_path').'customer/small/'.$filename;
		
		$medium_config['image_library'] 	=	'gd2';
		$medium_config['source_image'] 		=	$large_path;
		$medium_config['new_image'] 		=	$medium_path;
		$medium_config['maintain_ratio'] 	=	TRUE;
		$medium_config['width'] 			=	1600;
		$medium_config['height'] 			=	400;
		$this->load->library('image_lib', $medium_config);
		$this->image_lib->initialize($medium_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		$thumb_config['image_library'] 		=	'gd2';
		$thumb_config['source_image'] 		=	$large_path;
		$thumb_config['new_image'] 			=	$thumb_path;
		$thumb_config['maintain_ratio'] 	=	TRUE;
		$thumb_config['width'] 				=	50;
		$thumb_config['height'] 			=	50;
		$this->load->library('image_lib', $thumb_config);
		$this->image_lib->initialize($thumb_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		return true;    
	}

	function active($id,$status)
	{
		$data 			=	array('status'=>$status);
		$this->db->update('customer',$data,array('id'=>$id));
		// $this->list_ajax();
		redirect($this->base_uri);
	}

	function delete($id)
	{

		if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$row 			=	$this->Customer->get_one_banner($id);
		  unlink($this->config->item('image_path').'customer/small/'.$row->customer_image);
		 unlink($this->config->item('image_path').'customer/'.$row->customer_image);
			$this->db->delete('customer',array('id'=>$id));
			// $this->session->set_flashdata('Error','User Deleted');
		}
				echo "Record Deleted";
		// $row 			=	$this->Customer->get_one_banner($id);
		// unlink($this->config->item('image_path').'customer/small/'.$row->customer_image);
		// unlink($this->config->item('image_path').'customer/'.$row->customer_image);
		// $this->db->delete('customer',array('id'=>$id));
		// // $this->list_ajax();
		// $this->session->set_flashdata('Error','Customer Deleted');
		// redirect($this->base_uri);
	}




	function get_state($ctid){
		$query=$this->db->get_where('states',array('country_id'=>$ctid));
		$data=$query->result_array();
    	if($query->num_rows()==0)
    	{ $data ="0"; }
    	echo  json_encode($data);
 		}


	function get_city($ctid)
	{
		$query=$this->db->get_where('cities',array('state_id'=>$ctid));
		$data=$query->result_array();
    	if($query->num_rows()==0)
    	{ $data ="0"; }
    	echo  json_encode($data);
 		}
}