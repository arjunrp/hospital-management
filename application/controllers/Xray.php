<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xray extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Xray_model','Xray',TRUE);
		$this->template->set('title','Xray');
		$this->base_uri 			=	$this->config->item('admin_url')."xray";
	}

	function index()
	{
		$data['page_title']			=	"Xray Category";
		$data['output']				=	$this->Xray->list_all();
		// $this->template->load('template','customer',$data);
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('xray_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Xray->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Xray";
		$data['action']				=	$this->base_uri.'/insert';
		$data['xr_id']				=	"";
		$data['xr_test']			=	"";
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('xray_add',$data);
      	$this->load->view('templates/footer');  
	}

	function insert()
	{

		$title 							=	strtoupper($this->input->post('xr_test'));
		$this->tab_groups['xr_test']	=	$title;
		$this->db->insert('xray',$this->tab_groups);

		$id 								=	$this->db->insert_id();
		$this->session->set_flashdata('Success','Test Xray Created');
		redirect($this->base_uri."/");
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Test Xray";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Xray->get_one_banner($id);
		$data['xr_id']				=	$row->xr_id;
		$data['xr_test']			=	$row->xr_test;
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('xray_add',$data);
      	$this->load->view('templates/footer');  
		// $this->template->load('template','features_add',$data);
	}

	function update()
	{
		$id 							=	$this->input->post('xr_id');
		$title 							=	strtoupper($this->input->post('xr_test'));
		$this->tab_groups['xr_test']	=	$title;
		 
		$this->db->update('xray', $this->tab_groups,array('xr_id'=>$id));

		$this->session->set_flashdata('Success','Xray Updated');
		redirect($this->base_uri);
	}

	function delete($id)
	{
		if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$this->db->delete('Xray',array('xr_id'=>$id));
			$this->session->set_flashdata('Error','User Deleted');
		}
				echo "Record Deleted";
	}
}