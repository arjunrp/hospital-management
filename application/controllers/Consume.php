<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Consume extends MX_Controller 
{
  public $tab_groups;
  public $form_image;

  function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('logged_in'))
    {
      redirect($this->config->item('admin_url'));
    }
    $this->load->model('Consume_model','Consume',TRUE);
    $this->load->model('Stocktransfer_model','Stocktransfer',TRUE);
    $this->load->model('Sale_model','Sales',TRUE);
    $this->load->model('Purchase_model','Purchase',TRUE);
    $this->load->model('Stock_model','Stock',TRUE);
    $this->template->set('title','Consumption');
    $this->base_uri       = $this->config->item('admin_url')."consume";
  }

  function index()
  {
    $data['page_title']     =   "Consumption List";
    $data['output'] =   $this->Consume->list_all();

    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('consume_list',$data);
    $this->load->view('templates/footer'); 
      
  }

  function add(){

      $data['page_title']   = "New Consumption";
      // $data['products']     = $this->Consume->getCProducts();
      $data['consumeid']    = $this->Consume->getConsumeId();
      $data['departments']  = $this->Stocktransfer->getDepartment();
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('consume_add',$data);
      $this->load->view('templates/footer'); 
  }

  function edit($id){

      $data['page_title']   = "Edit Consumption";
      $data['consumes']     =  $this->Consume->getConsumeview($id);

      foreach ($data['consumes'] as $key => $value) {
        $department = $value['ve_supplier'];
      }

      $data['products']     = $this->Consume->getProducts($department);
      // print_r($data['products']  );

      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('consume_edit',$data);
      $this->load->view('templates/footer'); 
  }

  function consume_View($sid){
    $data['page_title']  =  "Consumption View";
    $data['consumes']     =  $this->Consume->getConsumeview($sid);

    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('consume_view', $data);
    $this->load->view('templates/footer');  
  }

  function getPrint(){
  $sid              = $_GET['cPrintid'];
  $data['company']  = $this->Purchase->getCompany();
  $data['consume']  = $this->Consume->getConsumeview($sid);
  $data['consumes']  = $this->Consume->getConsumeview($sid);
  $this->load->view('templates/header');    
  $this->load->view('consume_print',$data);
  }

  function consumeAdd(){

    $this->tab_groups['ve_vno']           = $this->input->post('ve_vno');
    $ve_date                              = $this->input->post('ve_date');
    $ve_date                              = date("Y-m-d",strtotime($ve_date));
    $this->tab_groups['ve_date']          = $ve_date;

    $ve_supplier                          = $this->input->post('ve_supplier');
    $this->tab_groups['ve_supplier']      = $ve_supplier;
    $ve_customer                          = $this->input->post('ve_customer');
    $this->tab_groups['ve_customer']      = $ve_customer;
    $this->tab_groups['ve_user']          = $this->input->post('user_type');
    $this->tab_groups['ve_type']          = "c";
    $this->db->insert('voucher_entry',$this->tab_groups);
    $consumeid                            = $this->db->insert_id();

  foreach($this->input->post('ved_item') as $k => $v)
  {

    $inputs = array();
    $inputs['ved_veid']       = $consumeid;
    $inputs['ved_date']       = $ve_date;
    $stockpid                 = $this->input->post('ved_itemid')[$k];
    $inputs['ved_itemid']     = $stockpid;
    $inputs['ved_item']       = $v;
    $ved_batch                = $this->input->post('ved_batch')[$k];
    $inputs['ved_batch']      = $ved_batch;
    $purchase_item_qty        = $this->input->post('ved_qty')[$k];
    $unit_item_qty            = $this->input->post('ved_uqty')[$k];
    $inputs['ved_expiry']     = $this->input->post('ved_expiry')[$k];
    
    $ved_unit                 = $this->input->post('ved_unit')[$k];

    $inputs['ved_unit']       = $ved_unit;

    if($ved_unit != "No.s")
    {
      $purchase_unit_qty      = $purchase_item_qty * $unit_item_qty;
    }
    else
    {
      $purchase_unit_qty      = $purchase_item_qty;
    }
    $inputs['ved_qty']        = $purchase_unit_qty;
    $inputs['ved_uqty']       = $unit_item_qty;

    $this->db->insert('voucher_entry_detail',$inputs);
    $this->Consume->set_consume_stock($stockpid,$purchase_unit_qty,$ved_batch,$ve_supplier,$ve_date);
  } 
  print_r($consumeid);
  }

  function consumeUpdate($ve_id1){
    $ve_id                                = $this->input->post('ve_id');
    $ve_date                              = $this->input->post('ve_date');
    $result = $this->Consume->get_voucher_details($ve_id1);
      foreach ($result as $key => $value) {

        $stockpid             = $value['ved_itemid'];
        $purchase_item_qty    = $value['ved_qty'];
        $ve_supplier          = $value['ve_supplier'];
        $purchase_date        = $value['ved_date'];
        $ved_batch            = $value['ved_batch'];

        $this->Consume->delete_consume_stock($stockpid,$purchase_item_qty,$purchase_date,$ved_batch,$ve_supplier);
        
      }

    
    $this->db->delete('voucher_entry_detail',array('ved_veid'=>$ve_id));

  foreach($this->input->post('ved_item') as $k => $v)
  {

    $inputs = array();
    $inputs['ved_veid']       = $ve_id;
    $inputs['ved_date']       = $ve_date;
    $stockpid                 = $this->input->post('ved_itemid')[$k];
    $inputs['ved_itemid']     = $stockpid;
    $inputs['ved_item']       = $v;
    $ved_batch                = $this->input->post('ved_batch')[$k];
    $inputs['ved_batch']      = $ved_batch;
    $purchase_item_qty        = $this->input->post('ved_qty')[$k];
    $unit_item_qty            = $this->input->post('ved_uqty')[$k];
    $inputs['ved_expiry']     = $this->input->post('ved_expiry')[$k];
    
    $ved_unit                 = $this->input->post('ved_unit')[$k];

    $inputs['ved_unit']       = $ved_unit;

    if($ved_unit != "No.s")
    {
      $purchase_unit_qty      = $purchase_item_qty * $unit_item_qty;
    }
    else
    {
      $purchase_unit_qty      = $purchase_item_qty;
    }
    $inputs['ved_qty']        = $purchase_unit_qty;
    $inputs['ved_uqty']       = $unit_item_qty;

    $this->Consume->update_consume_stock($stockpid,$purchase_unit_qty,$ve_date,$ved_batch,$ve_supplier);

    $this->db->insert('voucher_entry_detail',$inputs);
  } 
}

function delete($id)
  {

    if($_REQUEST['empid']) {
      $id   = $_REQUEST['empid']; 

      $idd = $id;
      $result = $this->Consume->get_voucher_details($idd);

      foreach ($result as $key => $value) {

        $stockpid             = $value['ved_itemid'];
        $purchase_item_qty    = $value['ved_qty'];
        $ve_supplier          = $value['ve_supplier'];
        $purchase_date        = $value['ved_date'];
        $ved_batch            = $value['ved_batch'];

        $this->Consume->delete_consume_stock($stockpid,$purchase_item_qty,$purchase_date,$ved_batch,$ve_supplier);
        
      }
      $this->db->delete('voucher_entry',array('ve_id'=>$id));
      $this->db->delete('voucher_entry_detail',array('ved_veid'=>$id));

   }
        echo "Record Deleted";
  }

  function productGetbydept($department)
  {
    $this->db->select('voucher_entry_detail.ved_itemid,voucher_entry_detail.ved_item,voucher_entry_detail.ved_batch,voucher_entry_detail.ved_expiry,voucher_entry_detail.ved_unit,voucher_entry_detail.ved_uqty,stock.stock_qty,stock.created_at');
    $this->db->from('stock');
    $this->db->join('voucher_entry_detail','voucher_entry_detail.ved_itemid = stock.stock_product_id AND voucher_entry_detail.ved_batch = stock.stock_batch', 'inner');
    $this->db->group_by('stock.stock_product_id');
    $this->db->group_by('stock.stock_batch');
    $this->db->order_by('stock.created_at','asc');
    $this->db->where('stock.stock_qty > ','0');
    $this->db->where('stock.stock_dept',$department);
    $this->db->where('stock.stock_status','standby');

    $query=$this->db->get();
    $data['products'] = $query->result_array();
    // print_r($query->result_array());
    $this->load->view('autocomplete_consume', $data);
  }

}