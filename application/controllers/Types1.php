

<?php

class Types extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->load->helper(array('form', 'url'));
    $this->load->model('Type_model');

  }

  public function index() {
    if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    if($this->input->post('submit')=='Submit'){
      $this->Type_model->insert_type();
      $this->session->set_flashdata('Success','Type Created');
      redirect('types/getTypes');
      
    }
    else{
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('type_add');
      $this->load->view('templates/footer');  
    }

  }}
  public  function getTypes(){
    if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    $data['types']=$this->Type_model->getTypelist();
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('type_list',$data);
    $this->load->view('templates/footer');  
  }}
  public function updateType($id){
    if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    if(!empty($id)){
      $data['type']=$this->Type_model->updateType($id);
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('type_update',$data);
      $this->load->view('templates/footer'); 
    }
    if($this->input->post('submit')=='Submit')
    {
      $this->Type_model->updateType();
      $this->session->set_flashdata('Success','Type Updated');
      redirect('types/getTypes');
    }
  }}
  public function deleteType($id){
    if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    $this->Type_model->deleteType($id);
    $this->session->set_flashdata('Success','Type Deleted');
    redirect('types/getTypes');
  }}








}

?>
