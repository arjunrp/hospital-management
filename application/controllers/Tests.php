<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tests extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Tests_model','Tests',TRUE);
		$this->template->set('title','Tests');
		$this->base_uri 			=	$this->config->item('admin_url')."tests";
	}

	function index()
	{
		$data['page_title']			=	"Test Category";
		$data['output']				=	$this->Tests->list_all();
		// $this->template->load('template','customer',$data);
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('tests_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Tests->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Test";
		$data['action']				=	$this->base_uri.'/insert';
		$data['ts_id']				=	"";
		$data['ts_test']			=	"";
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('tests_add',$data);
      	$this->load->view('templates/footer');  
	}

	function insert()
	{

		$title 							=	$this->input->post('ts_test');
		$this->tab_groups['ts_test']	=	$title;
		$this->db->insert('tests',$this->tab_groups);

		$id 								=	$this->db->insert_id();
		$this->session->set_flashdata('Success','Test Category Created');
		redirect($this->base_uri."/add");
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Test Category";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Tests->get_one_banner($id);
		$data['ts_id']				=	$row->ts_id;
		$data['ts_test']			=	$row->ts_test;
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('tests_add',$data);
      	$this->load->view('templates/footer');  
		// $this->template->load('template','features_add',$data);
	}

	function update()
	{
		$id 							=	$this->input->post('ts_id');
		$title 							=	$this->input->post('ts_test');
		$this->tab_groups['ts_test']	=	$title;
		 
		$this->db->update('tests', $this->tab_groups,array('ts_id'=>$id));

		$this->session->set_flashdata('Success','Test Updated');
		redirect($this->base_uri);
	}

	function delete($id)
	{
		if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$this->db->delete('tests',array('ts_id'=>$id));
			$this->session->set_flashdata('Error','User Deleted');
		}
				echo "Record Deleted";
	}
}