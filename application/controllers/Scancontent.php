<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scancontent extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Scancontent_model','Scancontent',TRUE);
		$this->template->set('title','scancontent');
		$this->base_uri 			=	$this->config->item('admin_url')."Scancontent";
	}

	function index()
	{
		$data['page_title']			=	"Scan content List";
		$data['output']				=	$this->Scancontent->list_all();
		// $this->template->load('template','customer',$data);
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('scan_content_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Scancontent->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Scan content";
		$data['action']				=	$this->base_uri.'/insert';
		$data['scnt_id']			=	"";
		$data['scnt_content']		=	"";
        $data['scnt_men']			=	"";
        $data['scnt_women']			=	"";
        $data['scnt_chid']			=	"";
		$data['catid'] 				=	"";
		$data['categories'] 		=	$this->Scancontent->get_category();
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('scan_content_add',$data);
      	$this->load->view('templates/footer');  
	}

function View($id)
	{
		$data['page_title']			=	"Edit Scan content";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Scancontent->get_one_banner($id);
		$data['scnt_id']			=	$row->scnt_id;
		$data['scnt_content']		=	$row->scnt_content;
		$data['scnt_men'] 			=	$row->scnt_men;
		$data['scnt_women'] 		=	$row->scnt_women;
		$data['scnt_chid'] 			=	$row->scnt_chid;
		$data['catid'] 				=	$row->scnt_scname;
		$data['categories'] 		=	$this->Scancontent->get_category();
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('scan_content_view',$data);
      	$this->load->view('templates/footer');  
	}
	
	function insert()
	{
		$this->tab_groups['scnt_content']	=	strtoupper($this->input->post('scnt_content'));
		$this->tab_groups['scnt_men']		=	$this->input->post('scnt_men');
		$this->tab_groups['scnt_women']	    =	$this->input->post('scnt_women');
		$this->tab_groups['scnt_chid']	    =	$this->input->post('scnt_chid');
		$this->tab_groups['scnt_scname']	    =	$this->input->post('scnt_scname');
		$this->db->insert('scan_content',$this->tab_groups);
		$id 							=	$this->db->insert_id();
		$this->session->set_flashdata('Success','Testscontent Created');
		redirect($this->base_uri."/add");
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Scan content";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Scancontent->get_one_banner($id);
		$data['scnt_id']			=   $row->scnt_id;
		$data['scnt_content']		=	$row->scnt_content;
	    $data['scnt_women'] 		=	$row->scnt_women;
		$data['scnt_chid'] 			=	$row->scnt_chid;
		$data['scnt_men'] 			=	$row->scnt_men;
		$data['catid'] 				=	$row->scnt_scname;
		$data['categories'] 		=	$this->Scancontent->get_category();
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('scan_content_add',$data);
      	$this->load->view('templates/footer');  
	}

	 

	function update()
	{
		$id 							=	$this->input->post('scnt_id');
		$this->tab_groups['scnt_content']	=	strtoupper($this->input->post('scnt_content'));
	    $this->tab_groups['scnt_women'] 	    =	$this->input->post('scnt_women');
	    $this->tab_groups['scnt_men'] 	    =	$this->input->post('scnt_men');
	    $this->tab_groups['scnt_chid'] 	    =	$this->input->post('scnt_chid');
		$this->tab_groups['scnt_scname']     =	$this->input->post('scnt_scname');
		$this->db->update('scan_content', $this->tab_groups,array('scnt_id'=>$id));
		$this->session->set_flashdata('Success','Expense Updated');
		redirect($this->base_uri);
	}


	function delete($id)
	{


         if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$this->db->delete('scan_content',array('scnt_id'=>$id));
			$this->session->set_flashdata('Error','Scan content');
		}
				echo "Record Deleted";
	}
}