

<?php

class Stock extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->load->helper(array('form', 'url'));
    $this->load->model('Stock_model');

  }

  public function index() {
    if($this->input->post('submit')=='Submit'){
      $this->customer_model->insert_customer();
      $this->session->set_flashdata('Success','Customer Created');
      redirect('customer/getCustomer');
    }
    else{
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('customer_add');
      $this->load->view('templates/footer');  
    }

  }
  public  function getStock(){
    if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    $data['stocks']=$this->Stock_model->getStock();
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('stock_list',$data);
    $this->load->view('templates/footer');  
  }}
  public function updateCustomer($id){
    if(!empty($id)){
      $data['customer']=$this->customer_model->updateCustomer($id);
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('customer_update',$data);
      $this->load->view('templates/footer'); 
    }
    if($this->input->post('submit')=='Submit')
    {
      $this->customer_model->updateCustomer();
      $this->session->set_flashdata('Success','Customer Updated');
      redirect('customer/getCustomer');
    }
  }

 public function stock_Report(){
       $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('stocks_report');
      $this->load->view('templates/footer'); 


 }


  public function deleteCustomer($id){
    if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    $this->customer_model->deleteCustomer($id);
    $this->session->set_flashdata('Success','Customer Deleted');
    redirect('customer/getCustomer');
  }}








}

?>
