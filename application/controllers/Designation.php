<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Designation extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Designation_model','Designation',TRUE);
		$this->template->set('title','Designation');
		$this->base_uri 			=	$this->config->item('admin_url')."designation";
	}

	function index()
	{
		$data['page_title']			=	"Designations List";
		$data['output']				=	$this->Designation->list_all();
		 
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('designation_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Designation->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Designation";
		$data['action']				=	$this->base_uri.'/insert';
		$data['id']					=	"";
		$data['designation']		=	"";
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('designation_add',$data);
      	$this->load->view('templates/footer');  
	}

	function insert()
	{
		$this->tab_groups['dg_designation']	     =	$this->input->post('designation');
		$this->db->insert('designation',$this->tab_groups);
		$this->session->set_flashdata('Success','Designation Created');
		redirect($this->base_uri."/add");
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit Designation";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->Designation->get_one_banner($id);
		$data['id']					=	$row->dg_id;
		$data['designation']		=	$row->dg_designation;
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('designation_add',$data);
      	$this->load->view('templates/footer');  
	}

	function update()
	{
		$id 									=	$this->input->post('id');
		$this->tab_groups['dg_designation']	    =	 $this->input->post('designation');
		 
		$this->db->update('designation', $this->tab_groups,array('dg_id'=>$id));
		$this->session->set_flashdata('Success','Designation Updated');
		redirect($this->base_uri);
	}

	
	function active($id,$status)
	{
		$data 			=	array('status'=>$status);
		$this->db->update('brand',$data,array('id'=>$id));
		redirect($this->base_uri);
	}

	function delete($id)
	{

		if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$row 			=	$this->Designation->get_one_banner($id);
			$this->db->delete('designation',array('dg_id'=>$id));

	 }
				echo "Record Deleted";
	}
}