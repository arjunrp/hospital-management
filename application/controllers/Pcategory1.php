

<?php

class Pcategory extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->load->helper(array('form', 'url'));
    $this->load->model('Category_model');

  }

  public function index() {
    if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    if($this->input->post('submit')=='Submit'){
      $this->Category_model->insert_category();
      $this->session->set_flashdata('Success','Category Created');
      redirect('pcategory/getCategorylist');
    }
    else{
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('category_add');
      $this->load->view('templates/footer');  
    }

  }}

  function getCategorylist(){
    if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    $data['category']=$this->Category_model->getCategorylist();
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('category_list',$data);
    $this->load->view('templates/footer');  
  }}


  public function updateCategory($id){
    if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    if(!empty($id)){
      $data['category']=$this->Category_model->updateCategory($id);
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('category_update',$data);
      $this->load->view('templates/footer'); 
    }
    if($this->input->post('submit')=='Submit')
    {
      $this->Category_model->updateCategory();
      $this->session->set_flashdata('Success','Category Updated');
      redirect('pcategory/getCategorylist');
    }

  }}

public function deleteCategory($id){
  if ($this->session->userdata('username') == '') {
            redirect('Login/index/');
        }   else {
    $this->Category_model->deleteCategory($id);
    $this->session->set_flashdata('Success','Category Deleted');
    redirect('pcategory/getCategorylist');
  }}


}

?>
