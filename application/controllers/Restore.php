<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Restore extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('Restore_model','Restore',TRUE);
		$this->template->set('title','Restore');
		$this->base_uri 			=	$this->config->item('admin_url')."restore";
	}

	function index()
	{
		$data['page_title']			=	"Restore";
		$data['output']				=	$this->Restore->list_all();
		 
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('restore_edit',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->Restore->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add Brand";
		$data['action']				=	$this->base_uri.'/insert';
		$data['id']					=	"";
		$data['brand']			    =	"";
		$data['image']				=	"";
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('brand_add',$data);
      	$this->load->view('templates/footer');  
	}

	function insert()
	{

		 
		$this->tab_groups['brand']	     =	$this->input->post('brand');
		 
		$this->db->insert('brand',$this->tab_groups);

		$id 								=	$this->db->insert_id();
		$config['upload_path']				=	$this->config->item('image_path').'brand/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'20000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ($this->upload->do_upload('image'))
		{		
			
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['brand_image']	=	$data['upload_data']['file_name'];
			$this->db->update('brand', $this->form_image,array('id'=>$id));
			
		}	
		else
		{
		
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
			
		}

		delete_files($this->config->item('image_path').'brand/original/');
		$this->session->set_flashdata('Success','Brand Created');
		redirect($this->base_uri);
	}

	 

	function update()
	{
		$id 							=	$this->input->post('id');
		 
		$this->tab_groups['brand']	    =	 $this->input->post('brand');
		 
		$this->db->update('brand', $this->tab_groups,array('id'=>$id));

		$config['upload_path']				=	$this->config->item('image_path').'brand/original/';
		$config['allowed_types']			=	'gif|jpg|jpeg|png';
		$config['max_size']					=	'20000';
		@$config['file_name']				=	$id.$config['file_ext'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (  $this->upload->do_upload('image'))
		{		
			$data = array('upload_data' => $this->upload->data());
			$this->process_uploaded_image($data['upload_data']['file_name']);
			$this->form_image['brand_image']=	$data['upload_data']['file_name'];
			$this->db->update('brand', $this->form_image,array('id'=>$id));
		}	
		else
		{
			$error = $this->upload->display_errors();	
			$this->messages->add($error, 'error');
		}
		delete_files($this->config->item('image_path').'features/original/');
		$this->session->set_flashdata('Success','Brand Updated');
		redirect($this->base_uri);
	}

	function process_uploaded_image($filename)
	{
		$large_path 	=	$this->config->item('image_path').'brand/original/'.$filename;
		$medium_path 	=	$this->config->item('image_path').'brand/'.$filename;
		$thumb_path 	=	$this->config->item('image_path').'brand/small/'.$filename;
		
		$medium_config['image_library'] 	=	'gd2';
		$medium_config['source_image'] 		=	$large_path;
		$medium_config['new_image'] 		=	$medium_path;
		$medium_config['maintain_ratio'] 	=	TRUE;
		$medium_config['width'] 			=	1600;
		$medium_config['height'] 			=	400;
		$this->load->library('image_lib', $medium_config);
		$this->image_lib->initialize($medium_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		$thumb_config['image_library'] 		=	'gd2';
		$thumb_config['source_image'] 		=	$large_path;
		$thumb_config['new_image'] 			=	$thumb_path;
		$thumb_config['maintain_ratio'] 	=	TRUE;
		$thumb_config['width'] 				=	50;
		$thumb_config['height'] 			=	50;
		$this->load->library('image_lib', $thumb_config);
		$this->image_lib->initialize($thumb_config);
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
			return false;
		}

		return true;    
	}

	function active($id,$status)
	{
		$data 			=	array('status'=>$status);
		$this->db->update('brand',$data,array('id'=>$id));
		redirect($this->base_uri);
		// $this->list_ajax();
	}

	function delete($id)
	{
		$row 			=	$this->Brand->get_one_banner($id);
		unlink($this->config->item('image_path').'brand/small/'.$row->brand_image);
		unlink($this->config->item('image_path').'brand/'.$row->brands_image);
		$this->db->delete('brand',array('id'=>$id));
		
		$this->session->set_flashdata('Error','Brand Deleted');
		redirect($this->base_uri);
		// $this->list_ajax();
	}
}