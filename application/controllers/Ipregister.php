<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ipregister extends MX_Controller 
{
  public $tab_groups;
  public $form_image;
  public $patient_datas;

  function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('logged_in'))
    {
      redirect($this->config->item('admin_url'));
    }
    $this->load->model('Ipregister_model','Ipregister',TRUE);
    $this->load->model('Booking_model','Booking',TRUE);
    $this->load->model('Opregister_model','Opregister',TRUE);
    $this->load->model('Users_model','Users',TRUE);
    $this->load->model('Booking_token_model','Booking_token',TRUE);
    $this->template->set('title','IP Register');
    $this->base_uri       = $this->config->item('admin_url')."ipregister";
  }

  function index()
  {
    $data['page_head']      = "IP List";
    $data['page_title']     = "Active IPs";
    $data['output']         = $this->Ipregister->list_all();
     
      $this->load->view('templates/header');  
      $this->load->view('templates/sidebar');        
      $this->load->view('ip_list',$data);
      $this->load->view('templates/footer'); 
  }

  public function list_ajax()
  {
    echo $data['output']    = $this->Ipregister->list_all();
  }

  function add()
  {
    $data['page_title']   = "Add IP";
    $data['action']       = $this->base_uri.'/insert';
    $data['doctors']      = $this->Booking_token->get_doctor();
    $data['ip_no']        = $this->Ipregister->get_ip_no();
    $data['output']       = $this->Ipregister->today_ip();

     
        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('ip_add',$data);
        $this->load->view('templates/footer');  
  }

  function insert()
  {

    $ip_mrd                               =  $this->input->post('ip_mrd');
    $this->tab_groups['ip_mrd']           =  $ip_mrd;
    $this->tab_groups['ip_ipno']          =  $this->input->post('ip_ipno');
    $this->tab_groups['ip_date']          =  $this->input->post('ip_date');
    $ip_time                              =  $this->input->post('ip_time');
    $this->tab_groups['ip_time']          =  date("H:i:s",strtotime($ip_time));
    $this->tab_groups['ip_department  ']  =  $this->input->post('ip_department');
    $this->tab_groups['ip_doctor']        =  $this->input->post('ip_doctor');
    $ip_dis                               =  $this->input->post('ip_dis1'); 

    // $this->tab_groups['ip_phone']         =  $this->input->post('p_phone');
    // $this->tab_groups['ip_name']          =  $this->input->post('ip_name');
    // $this->tab_groups['ip_dept_name']     =  $this->input->post('ip_dept_name');
    // $this->tab_groups['ip_doc_name']      =  $this->input->post('ip_doc_name');

    if($ip_dis==0)
    {
      $this->tab_groups['ip_discharge']   =  $this->input->post('ip_discharge');
      $ip_dtime                           =  $this->input->post('ip_dtime');
      $this->tab_groups['ip_dtime']       =  date("H:i:s",strtotime($ip_dtime));
      
    }

    else
    {
      $this->tab_groups['ip_discharge']   =  "0000-00-00";
      $this->tab_groups['ip_dtime']       =  "00:00:00";
    }

    $this->tab_groups['ip_dis']           =  $ip_dis;

    $this->db->insert('ip',$this->tab_groups);

    $this->patient_datas['p_phone']          =  $this->input->post('p_phone');
    $this->patient_datas['p_address']        =  $this->input->post('p_address');
    $this->patient_datas['p_street']         =  $this->input->post('p_street');
    $this->patient_datas['p_age']            =  $this->input->post('p_age');
    $this->patient_datas['p_sex']            =  $this->input->post('p_sex');
    $this->patient_datas['p_guardian']       =  $this->input->post('p_guardian');

    $this->db->update('patient',$this->patient_datas,array('p_mrd_no'=>$ip_mrd));

    $this->session->set_flashdata('Success','IP Created');
    redirect('ipregister/add');
  }

  function get_ipno()
  {
    $ip_dept   = $this->input->post('ip_dept');

      $this->db->select('ip_ipno');
      $this->db->from('ip');
      $this->db->where('ip_department',$ip_dept);
      $query      = $this->db->get();
      if($query->num_rows()==0)
      {
        $this->db->select('dp_short');
        $this->db->from('department');
        $this->db->where('dp_id',$ip_dept);
        $result      = $this->db->get();
        $data = $result->row()->dp_short."_0001";
      }
      else
      {
        $data   = $query->row()->ip_ipno;
        $data++;
      }
    print_r($data);
  }

  function get_discharge()
  {
    $ip_mrd   = $this->input->post('ip_mrd');

      $this->db->select('ip_discharge');
      $this->db->from('ip');
      $this->db->where('ip_mrd',$ip_mrd);
      $this->db->order_by('ip_ipno','desc');
      $this->db->limit(1);
      $query      = $this->db->get();
      if($query->num_rows()==0)
      {
        $data = "0";
      }
      else
      {
        $data   = $query->row()->ip_discharge;
      }
    print_r($data);
  }

  function getDepartment($c_id){

    $this->db->select('department.*');
    $this->db->from('department');
    $this->db->join('users','department.dp_id = users.u_department', 'inner'); 
    $this->db->where('users.u_emp_id',$c_id);
    $query=$this->db->get();
    $data=$query->result_array();

    if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
  }

  function edit($id)
  {
    $data['page_title']     = "Edit OP";
    $data['action']         = $this->base_uri.'/update';
    $row                    = $this->Ipregister->get_one_banner($id);

    $data['ip_id']          = $id;
    $ip_mrd                 = $row->ip_mrd;
    $data['ip_mrd']         = $ip_mrd;
    $data['ip_ipno']        = $row->ip_ipno;
    $data['ip_date']        = $row->ip_date;
    $data['ip_time']        = $row->ip_time;
    $data['p_name']         = $row->p_name;
    $data['p_phone']        = $row->p_phone;
    $data['p_age']          = $row->p_age;
    $data['p_sex']          = $row->p_sex;
    $data['p_address']      = $row->p_address;
    $data['p_street']       = $row->p_street;
    $data['p_guardian']     = $row->p_guardian;
    $data['ip_department']  = $row->ip_department;
    $data['department']     = $row->dp_department;
    $data['ip_doctor']      = $row->ip_doctor;
    $data['ip_discharge']   = $row->ip_discharge;
    $data['ip_dtime']       = $row->ip_dtime;
    $data['ip_dis']         = $row->ip_dis;

    $data['doctors']        = $this->Booking_token->get_doctor();
     
        $this->load->view('templates/header');  
        $this->load->view('templates/sidebar');        
        $this->load->view('ip_edit',$data);
        $this->load->view('templates/footer');  
  }

  function update()
  {

    $id                                   =  $this->input->post('ip_id');
    $ip_mrd                               =  $this->input->post('ip_mrd');
    $this->tab_groups['ip_mrd']           =  $ip_mrd;
    $this->tab_groups['ip_ipno']          =  $this->input->post('ip_ipno');
    $this->tab_groups['ip_date']          =  $this->input->post('ip_date');
    $ip_time                              =  $this->input->post('ip_time');
    $this->tab_groups['ip_time']          =  date("H:i:s",strtotime($ip_time));
    $this->tab_groups['ip_department  ']  =  $this->input->post('ip_department');
    $this->tab_groups['ip_doctor']        =  $this->input->post('ip_doctor');

    // $this->tab_groups['ip_phone']         =  $this->input->post('p_phone');
    // $this->tab_groups['ip_name']          =  $this->input->post('ip_name');
    // $this->tab_groups['ip_dept_name']     =  $this->input->post('ip_dept_name');
    // $this->tab_groups['ip_doc_name']      =  $this->input->post('ip_doc_name');

    $ip_dis                               =  $this->input->post('ip_dis1');

    if($ip_dis == "0")
    {
      $this->tab_groups['ip_discharge']   =  $this->input->post('ip_discharge');
      $ip_dtime                           =  $this->input->post('ip_dtime');
      $this->tab_groups['ip_dtime']       =  date("H:i:s",strtotime($ip_dtime));
    }

    else
    {
      $this->tab_groups['ip_discharge']   =  "0000-00-00";
      $this->tab_groups['ip_dtime']       =  "00:00:00";
    }
    // print_r($ip_dis);
    // exit();

    $this->tab_groups['ip_dis']           =  $ip_dis;

    $this->db->update('ip', $this->tab_groups,array('ip_id'=>$id));

    $old_ipno                             =  $this->input->post('old_ipno'); 
    $data['rs_ip']                        =  $this->input->post('ip_ipno');

    $this->db->update('room_shift',$data,array('rs_ip'=>$old_ipno));

    $this->patient_datas['p_phone']          =  $this->input->post('p_phone');
    $this->patient_datas['p_address']        =  $this->input->post('p_address');
    $this->patient_datas['p_street']         =  $this->input->post('p_street');
    $this->patient_datas['p_age']            =  $this->input->post('p_age');
    $this->patient_datas['p_sex']            =  $this->input->post('p_sex');
    $this->patient_datas['p_guardian']       =  $this->input->post('p_guardian');

    $this->db->update('patient',$this->patient_datas,array('p_mrd_no'=>$ip_mrd));
    $this->session->set_flashdata('Success','IP Updated');
    redirect('ipregister/add');
  }

  function delete($id)
  {

    if($_REQUEST['empid']) {
      $id       = $_REQUEST['empid']; 
      $row      = $this->Ipregister->get_one_banner($id);
      $ip_ipno                  =  $row->ip_ipno;
      $this->db->delete('ip',array('ip_id'=>$id));
      $this->db->delete('room_shift',array('rs_ip'=>$ip_ipno));

   }
        echo "Record Deleted";
  }

  function get_mrd($mrd)
  {
      $this->db->select('p_title,p_name,p_phone,p_guardian,p_age,p_sex,p_address,p_street,p_sex');
      $this->db->from('patient');
      $this->db->where('p_mrd_no',$mrd);
      $query  = $this->db->get();
      $data  = $query->result_array();

      if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
  }
  function get_phone($phone)
  {
      $this->db->select('p_mrd_no,p_title,p_name,p_guardian,p_age,p_sex,p_address,p_street,p_sex');
      $this->db->from('patient');
      $this->db->where('p_phone',$phone);
      $query  = $this->db->get();
      $data  = $query->result_array();

      if($query->num_rows()==0)
    { $data ="0"; }
    echo  json_encode($data);
  }

}