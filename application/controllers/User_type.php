<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_type extends MX_Controller 
{
	public $tab_groups;
	public $form_image;

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			redirect($this->config->item('admin_url'));
		}
		$this->load->model('User_type_model','User_type',TRUE);
		$this->template->set('title','User_type');
		$this->base_uri 			=	$this->config->item('admin_url')."user_type";
	}

	function index()
	{
		$data['page_title']			=	"User_type List";
		$data['output']				=	$this->User_type->list_all();
		 
		$this->load->view('templates/header');  
    	$this->load->view('templates/sidebar');        
    	$this->load->view('user_type_list',$data);
    	$this->load->view('templates/footer'); 
	}

	public function list_ajax()
	{
		echo $data['output']		=	$this->User_type->list_all();
	}

	function add()
	{
		$data['page_title']			=	"Add User_type";
		$data['action']				=	$this->base_uri.'/insert';
		$data['id']					=	"";
		$data['user_type']			=	"";
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('user_type_add',$data);
      	$this->load->view('templates/footer');  
	}

	function insert()
	{
		$this->tab_groups['ut_user_type']	     =	$this->input->post('user_type');
		$this->db->insert('user_type',$this->tab_groups);
		$this->session->set_flashdata('Success','User_type Created');
		redirect($this->base_uri."/add");
	}

	function edit($id)
	{
		$data['page_title']			=	"Edit User_type";
		$data['action']				=	$this->base_uri.'/update';
		$row 						=	$this->User_type->get_one_banner($id);
		$data['id']					=	$row->ut_id;
		$data['user_type']			=	$row->ut_user_type;
		 
		$this->load->view('templates/header');  
      	$this->load->view('templates/sidebar');        
      	$this->load->view('user_type_add',$data);
      	$this->load->view('templates/footer');  
	}

	function update()
	{
		$id 									=	$this->input->post('id');
		$this->tab_groups['ut_user_type']	    =	 $this->input->post('user_type');
		 
		$this->db->update('user_type', $this->tab_groups,array('ut_id'=>$id));
		$this->session->set_flashdata('Success','User_type Updated');
		redirect($this->base_uri);
	}

	
	function active($id,$status)
	{
		$data 			=	array('status'=>$status);
		$this->db->update('brand',$data,array('id'=>$id));
		redirect($this->base_uri);
	}

	function delete($id)
	{

		if($_REQUEST['empid']) {
			$id 	=	$_REQUEST['empid'];	
			$row 	=	$this->User_type->get_one_banner($id);
			$this->db->delete('user_type',array('ut_id'=>$id));

	 }
				echo "Record Deleted";
	}
}