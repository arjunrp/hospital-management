<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
<link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script>
  $(document).ready(function() {
    var amount1        = $('#amount').val();
  $('#amount').change(function() {
    var amount        = $('#amount').val();
    var blnce         = $('#blnce').val();
    var blnce         = parseFloat(amount)-parseFloat(amount1);
    $('#blnce').val(blnce);
  });
});
    </script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});

 $(function()
  {
    $("#ve_mrd").autocomplete({
      source: [<?php
      $i=0;
      foreach ($patients as $patient){
        if ($i>0) {echo ",";}
        echo '{value:"' . $patient['ip_mrd'] . '",patient:"' .$patient['p_title'] ." " . $patient['p_name'] . '",patientph:"' . $patient['p_phone'] . '",patientop:"' . $patient['ip_ipno'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          // $("#customerid").val(ui.item ? ui.item.vid : '');
          $("#bk_name").val(ui.item ? ui.item.patient : '');
          $("#bk_phone").val(ui.item ? ui.item.patientph : '');
          $("#ve_customer").val(ui.item ? ui.item.patientop : '');
        }
      });   
  });   

  $(function() {
    $("#bk_phone").autocomplete({
      source: [<?php
      $i=0;
      foreach ($patients as $patient){
        if ($i>0) {echo ",";}
        echo '{value:"' . $patient['p_phone'] . '",patient:"' .$patient['p_title'] ." " . $patient['p_name'] . '",patientmrd:"' . $patient['ip_mrd'] . '",patientop:"' . $patient['ip_ipno'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          // $("#customerid").val(ui.item ? ui.item.vid : '');
          $("#bk_name").val(ui.item ? ui.item.patient : '');
          $("#ve_mrd").val(ui.item ? ui.item.patientmrd : '');
          $("#ve_customer").val(ui.item ? ui.item.patientop : '');
        }
      });   
  }); 

  $(function() {
    $("#ve_customer").autocomplete({
      source: [<?php
      $i=0;
      foreach ($patients as $patient){
        if ($i>0) {echo ",";}
        echo '{value:"' . $patient['ip_ipno'] . '",mrd:"' . $patient['ip_mrd'] . '",patient:"'.$patient['p_title'] ." " . $patient['p_name'] . '",patientph:"' . $patient['p_phone'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          $("#ve_mrd").val(ui.item ? ui.item.mrd : '');
          $("#bk_name").val(ui.item ? ui.item.patient : '');
          $("#bk_phone").val(ui.item ? ui.item.patientph : '');
        }
      });   
  });  
</script>

<?php
    $user_type = $this->session->id;
    ?>

<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
      Advance Payment
      <small>
       <?php echo $page_title; ?></small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."advance"; ?>">Advance Payment</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div align="right">
         <a title="short key-ALT+B" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."advance"; ?>"accesskey="b" ><i class="fa fa-mail-reply-all"></i> Back</a>
       </div>

     </div>

     <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row">
       <div class="col-md-12">
        <div class="col-md-2">
        <div class="form-group required">
          <label  class="control-label">Voucher No<sup></sup></label>
          <input  class="form-control" readonly  type="text" name="ve_vno" value="<?=$advance_ID ?>">   
        <input type="hidden" name="ve_user" value="<?=$user_type?>">               
        </div> </div>
        <!-- <div class="col-md-8"></div> -->
        <div class="col-md-2">
        <div class="form-group required">
         <label  class="control-label">Date<sup></sup></label>
          <input class="form-control validate[required]"  tabindex="2" data-prompt-position="topRight:150" type="text" name="ve_date" id="datepicker" value="<?=date("Y-m-d") ?>">                
        </div></div> 
        <div class="col-md-4"></div>
        <div class="col-md-2">
         <div class="form-group required">
          <label  class="control-label">IP No.<sup></sup></label>
          <input  class="form-control"  type="text" tabindex="3" data-prompt-position="bottomRight:130" name="ve_customer" id="ve_customer">
          <input type="hidden" name="ve_supplier" value="16">
        </div> </div>

        <div class="col-md-2">
         <div class="form-group required">
          <label  class="control-label">MRD No.<sup></sup></label>
          <input  class="form-control validate[required]" tabindex="4" data-prompt-position="bottomRight:130"  type="text" name="ve_mrd" id="ve_mrd">
        </div> </div>
        </div>
        <div class="col-md-12"><br>
        <div class="col-md-3">
       <div class="form-group required">
          <label  class="control-label">Name<sup></sup></label>
          <input  class="form-control" tabindex="5" data-prompt-position="topRight:90"  type="text" id="bk_name">               
        </div></div>
      <div class="col-md-2">
        <div class="form-group required">
          <label  class="control-label">Phone<sup></sup></label>
          <input  class="form-control" tabindex="6" data-prompt-position="bottomRight:130" type="text" id="bk_phone">               
        </div>
        </div>
         <div class="col-md-2">
         <div class="form-group required">
          <label  class="control-label">Amount<sup></sup></label>
          <input  class="form-control validate[required,custom[number]]" tabindex="7" data-prompt-position="bottomRight:130"  type="text" name="ve_apayable">               
        </div>
       </div>
     </div>
      
     <div class="col-md-12"><br>
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            <div>
              <input class="btn-large btn-primary btn" type="submit" value="Submit" name="submit" accesskey="s" title="short key-ALT+S"> 
              <input class="btn-large btn-default btn" type="reset" value="Reset">
            </div>
          </div>

        </div>
      </div>
    </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</section>
  
</section><!-- /.right-side -->

 <script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });</script>
<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>