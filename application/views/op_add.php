
  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script type="text/javascript" charset="utf-8">
  $(function()
  {
    $("#bk_no").autocomplete({
      source: [<?php
      $i=0;
      foreach ($booking as $bookings){
        if ($i>0) {echo ",";}
        echo '{value:"' . $bookings['bk_no'] . '",bk_id:"' . $bookings['bk_id'] . '",patient:"' . $bookings['bk_name'] . '",patientph:"' . $bookings['bk_phone'] . '",mrd:"' . $bookings['bk_mrd'] . '",renewd:"' . $bookings['p_renew_days'] . '",renewc:"' . $bookings['p_renew_count'] . '",renewv:"' . $bookings['p_renew_visits'] . '",renewdate:"' . $bookings['p_renew_date'] . '",patientaddr:"' . $bookings['p_address'] . '",patientstr:"' . $bookings['p_street'] . '",patientage:"' . $bookings['bk_age'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          $("#bk_id").val(ui.item ? ui.item.bk_id : '');
          $("#bk_name").val(ui.item ? ui.item.patient : '');
          $("#bk_phone").val(ui.item ? ui.item.patientph : '');
          $("#op_mrd").val(ui.item ? ui.item.mrd : '');
          $("#rw_days").val(ui.item ? ui.item.renewd : '');
          $("#rw_count").val(ui.item ? ui.item.renewc : '');  
          $("#p_renew_visits").val(ui.item ? ui.item.renewv : '');
          $("#p_renew_date").val(ui.item ? ui.item.renewdate : '');
          $("#bk_address").val(ui.item ? ui.item.patientaddr : '');
          $("#bk_street").val(ui.item ? ui.item.patientstr : '');
          $("#bk_age").val(ui.item ? ui.item.patientage : '');

        }
      });   
  });
  </script>
  <script> 
$(document).ready(function() {
  $('.bkno').hide();
  var op_mrd = "";

  $('#op_mrd').change(function(){

    var uRL1   = "<?= base_url() ?>opregister/get_mrd/"+$(this).val();
      $.ajax({

        url : uRL1,
        dataType: "json",
        success:function(data3)
        {
           $.each(data3, function(index) {
        $("#bk_name").val(data3[index].p_name);
        $("#bk_phone").val(data3[index].p_phone);
        $("#rw_days").val(data3[index].p_renew_days);
        $("#rw_count").val(data3[index].p_renew_count);
        $("#p_renew_visits").val(data3[index].p_renew_visits);
        $("#p_renew_date").val(data3[index].p_renew_date);
        $("#bk_address").val(data3[index].p_address);
        $("#bk_street").val(data3[index].p_street);
        $("#bk_age").val(data3[index].p_age);
        var selected = "selected";
        if(data3[index].p_sex=="F") { var selected1 = selected; }
        else if(data3[index].p_sex=="M") { var selected2 = selected; }
        else { var selected3 = selected; }
              $("#p_sex").empty();
              $("#p_sex").append('<option value="F" '+selected1+'>Female</option>');
              $("#p_sex").append('<option value="M" '+selected2+'>Male</option>');
              $("#p_sex").append('<option value="C" '+selected3+'>Child</option>');

      });
       var op_mrd = $("#op_mrd").val();
       $.ajax({

        url : "<?= base_url() ?>opregister/get_previuos_visit",
        type: "POST",
        data : {op_mrd:op_mrd},
        success: function(data5){
      $('#pre_date').val(data5.trim());
     }
   });
    var rw_days         = $('#rw_days').val();
    var rw_count        = $('#rw_count').val();
    var p_renew_date    = $('#p_renew_date').val();
    var p_renew_visits  = $('#p_renew_visits').val();
    var uRL    = "<?= base_url() ?>patient/view/"+op_mrd;

    var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
    var firstDate   = new Date(p_renew_date);
    var secondDate  = new Date();

    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
    if(diffDays>rw_days || rw_count<=p_renew_visits)  
    {
      alert("Registration Card is Expired");
      $('#op_mrd').val("");
      window.location = "<?= base_url() ?>patient/view/"+op_mrd;
    }

  }

      });

  });

$('#bk_phone').change(function(){

    var uRL1   = "<?= base_url() ?>opregister/get_phone/"+$(this).val();
      $.ajax({

        url : uRL1,
        dataType: "json",
        success:function(data3)
        {
           $.each(data3, function(index) {
        $("#bk_name").val(data3[index].p_name);
        $("#op_mrd").val(data3[index].p_mrd_no);
        $("#rw_days").val(data3[index].p_renew_days);
        $("#rw_count").val(data3[index].p_renew_count);
        $("#p_renew_visits").val(data3[index].p_renew_visits);
        $("#p_renew_date").val(data3[index].p_renew_date);
        $("#bk_address").val(data3[index].p_address);
        $("#bk_street").val(data3[index].p_street);
        $("#bk_age").val(data3[index].p_age);
        var selected = "selected";
        if(data3[index].p_sex=="F") { var selected1 = selected; }
        else if(data3[index].p_sex=="M") { var selected2 = selected; }
        else { var selected3 = selected; }
              $("#p_sex").empty();
              $("#p_sex").append('<option value="F" '+selected1+'>Female</option>');
              $("#p_sex").append('<option value="M" '+selected2+'>Male</option>');
              $("#p_sex").append('<option value="C" '+selected3+'>Child</option>');

      });
    var op_mrd = $("#op_mrd").val();
    $.ajax({

        url : "<?= base_url() ?>opregister/get_previuos_visit",
        type: "POST",
        data : {op_mrd:op_mrd},
        success: function(data5){
      $('#pre_date').val(data5);
     }
   });

    var rw_days         = $('#rw_days').val();
    var rw_count        = $('#rw_count').val();
    var p_renew_date    = $('#p_renew_date').val();
    var p_renew_visits  = $('#p_renew_visits').val();
    var uRL    = "<?= base_url() ?>patient/view/"+op_mrd;

    var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
    var firstDate   = new Date(p_renew_date);
    var secondDate  = new Date();

    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

    if(diffDays>rw_days || rw_count<=p_renew_visits)  
    {
      alert("Registration Card is Expired");
      $('#op_mrd').val("");
      window.location = "<?= base_url() ?>patient/view/"+op_mrd;
    }

  }
      });
       

  });

  $('#op_type').change(function(){
    $('#bk_no').val("");
    $('#op_mrd').val("");
    $('#bk_phone').val("");
    $('#bk_name').val("");
    var op_type = $('#op_type').val();
    if(op_type=="b")
    {
      $('.bkno').show();
    }
    else if(op_type=="nb" || op_type=="0")
    {
      $('.bkno').hide();
          <?php
            $js_array = json_encode($doctors);
            echo "var doctors = ". $js_array . ";\n";
          ?>
          $('#op_doctor').empty();
          $("#op_doctor").append('<option value="0">--Select--</option>');
          $.each(doctors, function(index) {
          $("#op_doctor").append('<option value=' + doctors[index].u_emp_id +'>'+doctors[index].u_name+'</option>');
          });
    } 
    $('#op_shift').empty();
    $("#op_shift").append('<option value="0">--Select--</option>');
    $("#op_shift").append('<option value="m">--Morning--</option>');
    $("#op_shift").append('<option value="e">--Evening--</option>');

  });

  $('#bk_no').blur(function(){
    var type_id = $('#op_type').val();
    if(type_id=="b")
    {
      var op_no = $('#bk_no').val();
      $('#op_opno').val(+op_no);
    }
      var op_mrd =  $('#op_mrd').val();
       $.ajax({

        url : "<?= base_url() ?>opregister/get_previuos_visit",
        type: "POST",
        data : {op_mrd:op_mrd},
        success: function(data5){
      $('#pre_date').val(data5);
     }
   });

      $.ajax({
        url : "<?= base_url() ?>opregister/get_mrd/"+op_mrd,
        dataType: "json",
        success:function(data3)
        {
           $.each(data3, function(index) {
          var selected = "selected";
          if(data3[index].p_sex=="F") { var selected1 = selected; }
          else if(data3[index].p_sex=="M") { var selected2 = selected; }
          else { var selected3 = selected; }
              $("#p_sex").empty();
              $("#p_sex").append('<option value="F" '+selected1+'>Female</option>');
              $("#p_sex").append('<option value="M" '+selected2+'>Male</option>');
              $("#p_sex").append('<option value="C" '+selected3+'>Child</option>');
        });
      }

      });

  });

  $('#bk_no').change(function(){
    var bk_no  = $('#bk_no').val();
    var bk_dt  = $('#datepicker').val();
     $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>opregister/get_bk_doctor/"+bk_no+"/"+bk_dt,
     success: function(data5){
      $('#op_doctor').empty();
      $.each(data5, function(index) {
        $("#op_doctor").append('<option value=' + data5[index].u_emp_id +'>'+data5[index].u_name+'</option>');
        $('#op_department').val( data5[index].u_department);
        $('#op_docfee').val(data5[index].u_fees);
        $('#op_shift').empty();
        if(data5[index].bk_shift == "m")
        {
          $("#op_shift").append('<option value="m">--Morning--</option>');
        }
        else
        {
          $("#op_shift").append('<option value="e">--Evening--</option>');
        }
        
      });
    }
  })
  });

  $('#op_doctor').change(function(){
    var doc_id = $('#op_doctor').val();
    var uRL1   = "<?= base_url() ?>opregister/get_doctor_fee";
    $.ajax({
        url : uRL1,
        type: "POST",
        data : {doc_id:doc_id},
        success:function(data2, textStatus, jqXHR)
        {
          $('#op_docfee').val(data2);
        }
  })
  });

    $('#op_doctor').change(function(){
    var op_doc_name = $('#op_doctor option:selected').text();
    $('#op_doc_name').val(op_doc_name);
    var doc_id = $('#op_doctor').val();
    var uRL1   = "<?= base_url() ?>opregister/get_doctor_dept";
    $.ajax({
        url : uRL1,
        type: "POST",
        data : {doc_id:doc_id},
        success:function(data3, textStatus, jqXHR)
        {
          $('#op_department').val(data3);
        }
  })
  });

  $('#op_shift').change(function(){
    var sft_id  = $('#op_shift').val();
    var doc_id  = $('#op_doctor').val();
    var type_id = $('#op_type').val();
    var op_date = $('#datepicker').val();
    // alert(sft_id)
    // alert(op_date)
    if(doc_id==0)
    {
      alert("Select Doctor");
       $('#op_shift').empty();
       $("#op_shift").append('<option value="0">--Select--</option>');
       $("#op_shift").append('<option value="m">--Morning--</option>');
       $("#op_shift").append('<option value="e">--Evening--</option>');
    }
    else {
    var uRL1   = "<?= base_url() ?>opregister/get_token";
    $.ajax({
        url : uRL1,
        type: "POST",
        data : {sft_id:sft_id,doc_id:doc_id,op_date:op_date},
        success:function(data1, textStatus, jqXHR)
        {
          $('#op_opno').val(data1.trim());
        }
  }) 
  } 
  });

  $('#op_shift').blur(function(){
    var sft_id  = $('#op_shift').val();
    var doc_id  = $('#op_doctor').val();
    var op_date = $('#datepicker').val();
    var type_id = $('#op_type').val();

    var uRL    = "<?= base_url() ?>opregister/verify_tokens";
    $.ajax({
        url : uRL,
        type: "POST",
        data : {sft_id:sft_id,doc_id:doc_id,op_date:op_date},
        success:function(data, textStatus, jqXHR)
        {
          $('#token').val(data);
          // alert(data)
          if(data<0)
          {
            alert("Maximum Token Exceeded");
          }
        }
  })
  });

  });
  </script>


  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#customer_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       OP
       <small>Create OP </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."opregister"; ?>">OP</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/opregister">
        <i class="fa fa-plus-circle"></i> Today's OP List
      </a>
    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>

      <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row"> 
       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">OP No.<sup>*</sup></label>
          <input class="form-control" type="text" id="op_opno" name="op_opno">
          <input class="form-control validate[required]" type="hidden" id="token" value="0"> 
          <input class="form-control" type="hidden" name="user_type" value="<?=$user_type?>"> 
          <input class="form-control" type="hidden" name="bk_id" id="bk_id">              
        </div>
        <div class="col-md-2">
          <label  class="control-label">Date<sup>*</sup></label>
          <input class="form-control validate[required]"  data-prompt-position="topRight:150" type="text" name="op_date" id="datepicker" value="<?=date("Y-m-d") ?>">                
        </div>
         
         <div class="col-md-2">
         <label  class="control-label">OP Type<sup>*</sup></label>
         <select class="form-control validate[required] input-sm" tabindex="1" id="op_type" tabindex="1" name="op_type"> 
          <!-- <option value="0">--Select--</option> -->
          <!-- <option value="b">--Booking--</option> -->
          <option value="nb">--Not-Booking--</option>
          </select>  
       </div>
       <div class="col-md-1 bkno">
        <label  class="control-label">Bk No.<sup>*</sup></label>
          <input class="form-control input-sm"  type="text" id="bk_no" name="bk_no">
       </div>
        <div class="col-md-3">
         <label  class="control-label">Doctor<sup>*</sup></label>
         <select class="form-control validate[required] input-sm" id="op_doctor" tabindex="2" tabindex="3" name="op_doctor">
          <option value="0">--Select--</option>
              <?php foreach ($doctors as $key => $doctors) {
                ?>
              <option value="<?php echo $doctors['u_emp_id']?>"><?php echo $doctors['u_name']?></option>
                <?php
              }?>
            </select> 
            <input class="form-control" type="hidden" name="op_doc_name" id="op_doc_name">   
            <input class="form-control" type="hidden" name="op_department" id="op_department">   
            <input class="form-control" type="hidden" name="op_docfee" id="op_docfee">    
       </div>
       <div class="col-md-2">
         <label  class="control-label">Shift<sup>*</sup></label>
         <select class="form-control validate[required] input-sm" tabindex="3" id="op_shift" name="op_shift"> 
              <option value="0">--Select--</option>
              <option value="m">--Morning--</option>
              <option value="e">--Evening--</option>
            </select> 
       </div>
      </div>
      <div class="col-md-12">
       <div class="col-md-2">
         <label  class="control-label">MRD<sup>*</sup></label>
         <input class="form-control validate[required] input-sm"  tabindex="4" data-prompt-position="bottomRight:100" tabindex="5" type="text" name="op_mrd" id="op_mrd"><input class="form-control" type="hidden" id="rw_days"> 
         <input class="form-control" type="hidden" id="rw_count">    
       </div>
       <div class="col-md-2">
         <label  class="control-label">Patient's Name<sup></sup></label>
         <input class="form-control input-sm" type="text" tabindex="5" id="bk_name" name="op_name">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Phone No.<sup></sup></label>
         <input class="form-control input-sm" tabindex="6" type="text" id="bk_phone" name="p_phone">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Address<sup></sup></label>
         <input class="form-control input-sm"  type="text" id="bk_address" name="p_address">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Street<sup></sup></label>
         <input class="form-control input-sm"  type="text" id="bk_street" name="p_street">
       </div>
       <div class="col-md-1">
         <label  class="control-label">Age<sup></sup></label>
         <input class="form-control input-sm" type="text" name="p_age" id="bk_age">
       </div>
       <div class="col-md-2">
         <label class="control-label">Sex<sup></sup></label>
         <select class="form-control input-sm" name="p_sex" id="p_sex">
          <option value="Female">Female</option>
          <option value="Male">Male</option>
          <option value="Others">Others</option>
         </select>
       </div>
       <div class="col-md-2">
         <label  class="control-label">Prevoius Visit Date<sup></sup></label>
         <input class="form-control input-sm" readonly type="text" id="pre_date">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Last Renewal<sup></sup></label>
         <input readonly class="form-control " type="text" id="p_renew_date">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Visits after renewal<sup></sup></label>
         <input readonly class="form-control input-sm" type="text" id="p_renew_visits">
        
       </div>
     </div>
          </div>
          <br><br>
          <div class="box-footer">
            <div class="row">
              <div class="col-md-12">
                <div>
                  <!-- <button class="button btn btn-warning" tabindex="8" type="button" id="chk">Check Validity</button> -->
                  <button class="button btn btn-primary" tabindex="7" type="submit">Add to OP Register</button>
                  <input class="btn-large btn-default btn" type="reset" tabindex="utf-8" value="Reset">
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php echo form_close(); ?>

        <!-- <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover gridView" id="category_table">
                    <thead>
                      <tr>
                        <th>OP No.</th>
                        <th>Date</th>
                        <th>Doctor</th>
                        <th>Shift</th>
                        <th>MRD</th>
                        <th>Patient's Name</th>
                        <th>Phone No.</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                  <tbody> 
                    <?php if($output) { echo $output; } ?>
                  </tbody>
                  </table> 
                

          </div>
        </div>
      </div>
    </div>
  </div>
</div> -->
</div>
</section>

</section><!-- /.right-side -->


<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });</script>
<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>