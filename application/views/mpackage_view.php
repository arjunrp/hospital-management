


<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
 
<?php
foreach($mpackages as $mpackage)
{
  $mt_id          = $mpackage['mt_id'];
  $mt_pkgno       = $mpackage['mt_pkgno'];
  $mt_pakcage     = $mpackage['mt_pakcage'];
  $mt_supplier    = $mpackage['mt_supplier'];
  $sp_vendor      = $mpackage['sp_vendor'];
  $sp_phone       = $mpackage['sp_phone'];
  $u_emp_id       = $mpackage['u_emp_id'];

}

?>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>

<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
     Medicine Packages
      <small>View Medicine Package</small>
    </h1>

    <ol class="breadcrumb">
      <li><a href=" ">Dashboard</a></li>
      <li><a href=" ">Medicine Package</a></li>
      <li class="active"></li> <?=$page_title?>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i>  <?=$page_title?>
       </h3>
        <div class="box-tools">
              <a title="Back" class="btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."mpackage"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>
     </div>

     <form id="post_form">
     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">
           
            <div class="form-group col-md-2">
            <label  class="control-label">Pkg No. :</label> <?= $mt_pkgno  ?><br>
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Package Name :</label> <?=$mt_pakcage  ?><br>
          </div>

          <div class="form-group col-md-2">
            <label  class="control-label">Supplier ID :</label> <?= $mt_supplier  ?> <br>         
          </div>

          <div class="form-group col-md-3">
            <label  class="control-label">Supplier :</label> <?= $sp_vendor  ?><br>
          </div>

          <div class="form-group col-md-3">
            <label  class="control-label">Supplier Ph. :</label> <?= $sp_phone  ?><br>
          </div>

          <div class="form-group col-md-2">    
          <label class="control-label">User ID : </label> <?= $u_emp_id ?>
          </div>

 <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">

               
                   <table class="table table-condensed dataTable no-footer gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Sl. No </th>
                        <th>Product</th>
                        <th>Unit Price</th>
                        <th>Quantity</th>
                      </tr>
                    </thead>
                  <tbody> 
                    <?php
                    $slno=1;
                    foreach($mpackages as $mpackage)
                      { ?> 
                    <tr>
                      <td><?= $slno++  ?></td>
                      <td><a href="<?= $this->config->item('admin_url')."product/view/".$mpackage['mtd_item_id'] ?>"><?=$mpackage['mtd_item'] ?></a></td>
                      <td><?=$mpackage['mtd_price'] ?></td>
                      <td><?=$mpackage['mtd_qty']." ".$mpackage['mtd_unit']  ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                  </table> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
           

        </div>
      </div>
      
      
    </div>
  </form><br>
  </div>
</section>

</section><!-- /.right-side -->


