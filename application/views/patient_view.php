
<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
     Patient
      <small>View  Patient </small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."patient"; ?>">Patient</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i><b> MRD No. <?=$p_mrd_no  ?></b>
       </h3>

       <div align="right">
        <a title="Print" class="btn btn-sm btn-primary" href="<?php echo $this->config->item('admin_url')."patient/reg_print/$p_mrd_no"; ?>"><i class="fa fa-print"></i> Registration Card</a>
        <a title="Print" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."patient/getPrint/$p_mrd_no"; ?>"><i class="fa fa-print"></i> Bill Receipt</a>
        <?php if($date_count>$p_renew_days || $p_renew_count<=$p_renew_visits) { ?> 
        <a title="Renew" class="btn btn-sm btn-warning" href="<?php echo $this->config->item('admin_url')."patient/renew/$p_mrd_no"; ?>"><i class="fa fa-print"></i> Renew Patient</a>
        <?php } ?>
        <a title="Edit" class="btn btn-sm btn btn-info" href="<?php echo $this->config->item('admin_url')."patient/edit/$p_mrd_no"; ?>"><i class="fa fa-pencil-square-o"></i> Edit</a>
        <a title="Back" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."patient"; ?>"><i class="fa fa-mail-reply-all"></i> Back</a>
        <!--  <a title="Print" class="btn-large btn-warning btn" href="<?php echo $this->config->item('admin_url')."patient/getPrint/$p_mrd_no"; ?>"><i class="fa fa-print"></i>Print</a> -->
        </div>
     </div>


     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">
          <div class="form-group col-md-2">
          <label for="focusinput" class="control-label">Image</label><br>
         <?php if($image != "") { echo "<img width='150px' height='150px' src='".$this->config->item("base_url")."uploads/patient/".$image."'>"; } else { echo "<img width='150px' height='150px' src='".$this->config->item("base_url")."uploads/Default.png"."'>"; } ?>
          </div>

          <div class="form-group col-md-2">
            <label  class="control-label">Patient Name<sup>*</sup></label><br>
            <?= $p_title." ".$p_name ?>
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Registered Date<sup>*</sup></label><br>
            <?= $p_date ?>             
          </div>
          <?php 
          if($p_mrd_no!=$p_old_mrd) { ?>
          <div class="form-group col-md-2">
            <label  class="control-label">Old MRD<sup>*</sup></label><br>
            <?= $p_old_mrd ?>             
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Merged Date<sup>*</sup></label><br>
            <?= $p_new_date ?>             
          </div>
          <?php } ?>
          <div class="form-group col-md-2">
            <label  class="control-label">Renewed Date<sup>*</sup></label><br>
            <?= $p_renew_date ?>             
          </div>
          <div class="form-group col-md-1">
            <label  class="control-label">Age<sup>*</sup></label><br>
            <?= $p_age ?>             
          </div>
          <div class="form-group col-md-1">
            <label  class="control-label">Gender<sup>*</sup></label><br>
            <?= $p_sex ?>             
          </div>          
          <!-- <div class="form-group col-md-2">
            <label  class="control-label">DOB<sup>*</sup></label><br>
            <?= $p_dob ?>             
          </div> -->
          <div class="form-group col-md-2">
            <label  class="control-label">Blood Group<sup>*</sup></label><br>
            <?= $p_blood ?>             
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Phone<sup>*</sup></label><br>
            <?= $p_phone ?>             
          </div>
          <div class="form-group col-md-2">
             <label  class="control-label">Care Of<sup>*</sup></label><br>
            <?= $p_guardian  ?>                 
          </div> 
          <div class="form-group col-md-2">
            <label  class="control-label">Address<sup>*</sup></label><br>
            <?= $p_address ?>             
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Street<sup>*</sup></label><br>
            <?= $p_street ?>             
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">City<sup>*</sup></label><br>
            <?php foreach ($cities as $key => $cities) {
              if($cities['id']==$p_city) { echo $cities['name']; } 
              } ?>
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">State<sup>*</sup></label><br>
           <?php foreach ($states as $key => $states) {
              if($states['id']==$p_state) { echo $states['name']; } 
              } ?>                 
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Country<sup>*</sup></label><br>
            <?php foreach ($countries as $key => $countries) {
              if($countries['id']==$p_country) { echo $countries['name']; } 
              } ?>
            </div>
           <div class="form-group col-md-2">
            <label  class="control-label">Zip<sup>*</sup></label><br>
            <?= $p_zip ?>
          </div>
           <div class="form-group col-md-2">
            <label  class="control-label">ID Type<sup>*</sup></label><br>
            <?= $p_id_type  ?>          
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">ID No.<sup>*</sup></label><br>
            <?= $p_id_no  ?>             
          </div> 
          <div class="form-group col-md-2">
            <label  class="control-label">Email<sup>*</sup></label><br>
            <?= $p_email ?>             
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Renewal Visits<sup>*</sup></label><br>
            <?= $p_renew_count ?>             
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Renewal Days<sup>*</sup></label><br>
            <?= $p_renew_days ?>             
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

</section><!-- /.right-side -->


