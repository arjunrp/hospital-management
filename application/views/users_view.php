<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
      Users
      <small>View   User </small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."users"; ?>"> Users</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">

      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> User
       </h3>
       <div align="right">
          <a title="Edit" class="btn btn-sm btn btn-info" href="<?php echo $this->config->item('admin_url')."users/edit/$u_emp_id"; ?>"><i class="fa fa-pencil-square-o"></i> Edit</a>
        <a title="Back" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."users"; ?>"><i class="fa fa-mail-reply-all"></i> Back</a>
        </div>
     </div>

     <div class="box-body">
      <div class="row">
         
        <div class="col-md-12 ">
                   <div class="form-group col-md-2">
          <label for="focusinput" class="control-label">Image</label><br>
         <?php if($image != "") { echo "<img width='150px' height='150px' src='".$this->config->item("base_url")."uploads/users/".$image."'>"; } else { echo "<img width='150px' height='150px' src='".$this->config->item("base_url")."uploads/Default.png"."'>"; } ?>
          </div>
          
          <div class="form-group col-md-3">
            <label  class="control-label">Employee Id<sup>*</sup></label><br>
            <?=$u_emp_id ?>             
          </div>
           <div class="form-group col-md-3">
            <label  class="control-label">Name<sup>*</sup></label><br>
            <?= $u_name ?>             
          </div>
           <div class="form-group col-md-3">
            <label  class="control-label">User Type<sup>*</sup></label><br>
            <?php foreach ($type as $key => $type) {
              if($type['ut_id']==$u_type) { echo $type['ut_user_type']; } 
              } ?>             
          </div>
           <div class="form-group col-md-3">
            <label  class="control-label">Department<sup>*</sup></label><br> 
            <?php foreach ($department as $key => $department) {
              if($department['dp_id']==$u_department) { echo $department['dp_department']; } 
              } ?>             
          </div>

           <div class="form-group col-md-3">
            <label  class="control-label">Designation<sup>*</sup></label><br>
            <?php foreach ($designation as $key => $designation) {
              if($designation['dg_id']==$u_designation) { echo $designation['dg_designation']; } 
              } ?>              
          </div>
          <?php if($u_fees!=0) { ?>
           <div class="form-group col-md-3">
            <label  class="control-label">Consulting Fees<sup>*</sup></label><br>
            <?= $u_fees ?>             
          </div> <?php } ?>
          <div class="form-group col-md-3">
            <label  class="control-label">Phone<sup>*</sup></label><br>
            <?= $u_phone  ?>             
          </div>
          <div class="form-group col-md-3">
            <label  class="control-label">Address<sup>*</sup></label><br>
            <?= $u_address  ?>             
          </div>
          <div class="form-group col-md-3">
            <label  class="control-label">Street<sup>*</sup></label><br>
            <?= $u_street  ?>             
          </div>
          <div class="form-group col-md-3">
            <label  class="control-label">City<sup>*</sup></label><br>
            <?php foreach ($cities as $key => $cities) {
              if($cities['id']==$u_city) { echo $cities['name']; } 
              } ?>            
          </div>
          <div class="form-group col-md-3">
            <label  class="control-label">State<sup>*</sup></label><br>
            <?php foreach ($states as $key => $states) {
              if($states['id']==$u_state) { echo $states['name']; } 
              } ?>              
          </div>
          <div class="form-group col-md-3">
            <label  class="control-label">Country<sup>*</sup></label><br>
            <?php foreach ($countries as $key => $countries) {
              if($countries['id']==$u_country) { echo $countries['name']; } 
              } ?>          
          </div>
            <div class="form-group col-md-2">
            </div>
          <div class="form-group col-md-3">
            <label  class="control-label">Basic Salary<sup>*</sup></label><br>
            <?= $u_basic_sal  ?>             
          </div>
           <div class="form-group col-md-3">
            <label  class="control-label">Salary/day<sup>*</sup></label><br>
            <?= $u_sal_day  ?>             
          </div>
           <div class="form-group col-md-3">
            <label  class="control-label">Casual/Month<sup>*</sup></label><br>
            <?= $u_cas_month  ?>             
          </div>
        </div>
      </div></div>



    </div>

</section>

</section><!-- /.right-side -->
