
<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
     Stock
      <small><?=$page_title ?></small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."stock"; ?>">Stock</a></li>
      <li class="active"><?= $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">

      
     <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?= $page_title; ?>
       </h3>
       <div align="right">
        <a title="short key-ALT+B" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."stock"; ?>" accesskey="b"><i class="fa fa-mail-reply-all"></i> Back</a>
        </div>
     </div>


     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">
          <div class="form-group col-md-2">
          <label for="focusinput" class="control-label">Image</label><br>
          <?php if($image != "") { echo "<img width='150px' height='150px' src='".$this->config->item("base_url")."uploads/product/".$image."'>"; } else { echo "<img width='150px' height='150px' src='".$this->config->item("base_url")."uploads/product.png"."'>"; } ?>
           </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Product Code <sup>*</sup></label><br>
            <?= $pd_code ?>
          </div>

          <div class="form-group col-md-3">
            <label  class="control-label">Brand<sup>*</sup></label><br>
           <?php foreach ($brands as $key => $brand) {
              if($brand['br_id']==$pd_brandid) { echo $brand['br_brand']; } 
              } ?>             
          </div>
          <div class="form-group col-md-3">
            <label  class="control-label">Suppliers<sup>*</sup></label><br>
            <?php $pdsupplier1=""; $pdsupplier=""; 
            foreach ($suppliers as $key => $supplier) {
              $e = explode(",", $pd_supplier);
              if(in_array($supplier['sp_id'],$e)) { 
                $pdsupplier[]=$supplier['sp_vendor'];
                $pdsupplier1= implode(',',$pdsupplier); } 
              } echo $pdsupplier1;  ?>               
          </div>

          <div class="form-group col-md-2">
            <label  class="control-label">Main Content <sup>*</sup></label><br>
             <?php $pdcontentid1=""; $pdcontentid=""; 
            foreach ($contents as $key => $content) {
              $e1 = explode("+", $pd_contentid);
              if(in_array($content['mc_id'],$e1)) { 
                $pdcontentid[]=$content['mc_content'];
                $pdcontentid1= implode('+',$pdcontentid); } 
              } echo $pdcontentid1;  ?> 
          </div>

          <div class="form-group col-md-3">
             <label  class="control-label">GST-State(%) <sup>*</sup></label><br>
            <?= $pd_sgst  ?>                 
          </div> 
           
           <div class="form-group col-md-3">
            <label  class="control-label">GST-Central(%) <sup>*</sup></label><br>
            <?= $pd_cgst  ?>          
          </div>

          <div class="form-group col-md-2">
            <label  class="control-label">Min Qty<sup>*</sup></label><br>
            <?=$pd_min  ?>
            </div>

           <div class="form-group col-md-2">
            <label  class="control-label">Max Qty <sup>*</sup></label><br>
           <?=$pd_max  ?>
          </div>

          <div class="form-group col-md-3">        
          <label  class="control-label">Product Type<sup>*</sup></label><br>
          <?php if($pd_type=="g") { echo "General"; }
              else if($pd_type=="m") { echo "Medical"; } ?>                
          </div>

          <div class="form-group col-md-3">
            <label  class="control-label">Unit<sup>*</sup></label><br>
             <?=$pd_unit ?>

          </div> 

           <div class="form-group col-md-3">        
          <label  class="control-label">Per No.s<sup>*</sup></label><br>
          <?= $pd_qty  ?>                
          </div>


        </div>
      </div>
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            <br>
            <table class="table table-striped table-bordered table-hover gridView" id="category_table">
                    <thead>
                      <tr>
                        <th>Sl No.</th>
                        <th>Department</th>
                        <th>Stock</th>
                      </tr>
                    </thead>
                  <tbody> 
                    <?php $slno =1; foreach ($stock_details as $stock) {
                      ?>
                    <tr> 
                      <td> <?= $slno?></td>
                      <td> <?= $stock['dp_department']?></td>
                      <td> <?= $stock['total_stock']/$stock['pd_qty']. " ".$stock['pd_unit'] ?></td>
                    </tr>
                    <?php $slno++; } ?>
                  </tbody>
                  </table> 
            
          </div>

        </div>
      </div>
      
    </div>
  </div>
</section>
<br><br><br><br><br><br><br><br><br><br><br>  
</section>
<!-- /.right-side -->


