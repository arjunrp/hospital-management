
<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
      Customer
      <small>Update  Customer </small>
    </h1>

    <ol class="breadcrumb">
      <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="">Customer</a></li>
      <li class="active">Update Customer</li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
        <h3 class="box-title">
         <i class="fa fa-th"></i>Update Customer
       </h3>

     </div>

     <?php echo form_open_multipart('customer/updateCustomer') ?>
     <div class="box-body">
      <div class="row">
        <div class="col-md-6 ">

          <div class="form-group required">
            <label  class="control-label">Customer<sup></sup></label>
            <input class="form-control" required="required"  type="text" name="customer" value="<?php echo $customer['customer'];?>">                
          </div>
            <input class="form-control" required="required"  type="hidden" name="customerid" value="<?php echo $customer['id'];?>">                

          <div class="form-group required">
            <label  class="control-label">Address<sup></sup></label>
            <textarea name="address" class="form-control"><?php echo $customer['address'];?></textarea>
          </div>

          <div class="form-group required">
            <label  class="control-label">City<sup></sup></label>
            <input class="form-control" required="required"  type="text" name="city" value="<?php echo $customer['city'];?>">                
          </div> 

          <div class="form-group required">
            <label  class="control-label">Zip<sup></sup></label>
            <input class="form-control" required="required"  type="text" name="zip" value="<?php echo $customer['zip'];?>">                
          </div>

          <div class="form-group required">
            <label  class="control-label">Country<sup></sup></label>
            <input class="form-control" required="required"  type="text" name="country" value="<?php echo $customer['country'];?>">                
          </div>

        </div>
        <div class="col-md-6 ">

         <div class="form-group required">
          <label  class="control-label">State<sup></sup></label>
          <input class="form-control" required="required"  type="text" name="state" value="<?php echo $customer['state'];?>">                
        </div>

        <div class="form-group required">
          <label  class="control-label">Phone<sup></sup></label>
          <input class="form-control" required="required"  type="text" name="phone" value="<?php echo $customer['phone'];?>">                
        </div>

        <div class="form-group required">
          <label  class="control-label">Email<sup></sup></label>
          <input class="form-control" required="required"  type="text" name="email" value="<?php echo $customer['email'];?>">                
        </div>        

        <div class="form-group required">
          <label  class="control-label">Fax<sup></sup></label>
          <input class="form-control" required="required"  type="text" name="fax" value="<?php echo $customer['fax'];?>">                
        </div>

        <div class="form-group required">
          <label  class="control-label">Website<sup></sup></label>
          <input class="form-control" required="required"  type="text" name="website" value="<?php echo $customer['website'];?>">                
        </div>

      </div>
    </div>
    <div class="box-footer">
      <div class="row">
        <div class="col-md-12">
          <div>
            <input class="btn-large btn-primary btn" type="submit" value="Submit" name="submit"> 
            <input class="btn-large btn-default btn" type="reset" value="Reset">
          </div>
        </div>

      </div>
    </div>
    <?php echo form_close(); ?>
  </div>
</div>
</section>

</section><!-- /.right-side -->


