
<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
 
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>

<div class="content-wrapper">
  <section class="content-header">
    <h1>
     Cash Counter
      <small>Today's Report</small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."ccounter"; ?>">Cash Counter</a></li>
      <li class="active">Today's Report</li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> Today's Cash Report
       </h3>
        <div class="box-tools">
              <a title="Back" class="btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."ccounter"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>
     </div>


      <form id="post_form">
     <div class="box-body">

          <div class="row">
          <div class="col-md-12">
             
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">

                     <table class="table table-condensed dataTable no-footer gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Sl. No </th>
                        <th>Date</th>
                        <th>Department</th>
                        <th>Bill No From</th>
                        <th>Bill No To</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                  <tbody>
                  <?php
                  $slno=1;
                  $total_amount = 0;
                  $sect = 1;
                  $sumpharm  = 0;
                  $sumpharmr = 0;
                  $sumpharmb = 0;
                  $count = 1;
                  $count1 = 1;
                  $count2 = 1;
                  foreach($treports as $treport)
                  { 
                    $total_amount = $total_amount + $treport['apaid'];
                    ?> 
                    <tr>
                      <td><?= $slno++  ?></td>
                      <td><?=date("d-m-Y",strtotime($treport['ve_cash_date'])) ?></td>
                      <td><?php if($treport['ve_type'] != "opbl")
                          { echo $treport['dp_department']; }
                          if($treport['ve_type'] == "si")
                          {
                            echo $sect++;
                          }
                          else if($treport['ve_type'] == "so")
                          {
                            echo $sect++;
                          }
                          else if($treport['ve_type'] == "sr")
                          {
                            echo " - Sales Return";
                          }
                          else if($treport['ve_type'] == "opbl")
                          {
                            echo "OP Billing";
                          }
                          else if($treport['ve_type'] == "ad")
                          {
                            echo " - Advance Payment";
                          }
                          else if($treport['ve_type'] == "adr")
                          {
                            echo " - Advance Return";
                          }
                          else if($treport['ve_type'] == "dis")
                          {
                            echo " - Discharge";
                          }
                          else if($treport['ve_type'] == "dsr")
                          {
                            echo " - Discharge Return";
                          }

                      ?></td>
                      <td><?=$treport['mi_veno'] ?></td>
                      <td><?=$treport['mx_veno'] ?></td>
                      <td><?="Rs. ".$treport['apaid'] ?></td>
                      <?php if($treport['ve_type'] == "si") { $count++;  ?><td><b><?php $sumpharm = $sumpharm + $treport['apaid']; if($count>2) { echo"Rs. ".$sumpharm; } ?></b></td> <?php } ?>
                      <?php if($treport['ve_type'] == "sr") { $count1++; ?><td><b><?php $sumpharmr = $sumpharmr + $treport['apaid']; if($count1>2) { echo "Rs. ".$sumpharmr; } ?></b></td> <?php } ?>
                      <?php if($treport['ve_type'] == "opbl") { $count2++; ?><td><b><?php $sumpharmb = $sumpharmb + $treport['apaid']; if($count2>2) { echo "Rs. ".$sumpharmb; } ?></b></td> <?php } ?>
                    </tr>
                    <?php }  ?>

                  </tbody>
                  </table> 
                
                  <table class="table table-bordered">
                    <tr><th width="60%"></th><th>Total</th>
                    <th><?="Rs. ".$total_amount?></th></tr>

                    </table>
   
<!-- <br><?php if($ve_balance!=0) { ?><a href="#" class="btn btn-success" onclick="save();"><i class="fa fa-floppy-o"></i> Save</a> <?php } ?> -->
<!-- <input type="input" id="Printid"> -->
 <a href="#" class="btn btn-warning" onclick="print();"><i class="fa fa-print"></i> Print</a>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
           

        </div>
      </div>
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            
          </div>

        </div>
      </div>
     
    </form>
   
</section>

</div><!-- /.right-side -->

<script>
function print(){

          window.location = "<?php echo base_url();?>index.php/ccounter/treport_print";

}
</script>


