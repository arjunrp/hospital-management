<!-- <meta http-equiv="refresh" content="25" /> -->
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('.purchase_table').dataTable({
    "aaSorting": [[ 0, "asc" ]],
    "bJQueryUI": true,
    "sPaginationType": "full_numbers",
    "iDisplayLength": 25,
    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]

});
});
</script>
<?php
$nfp = "<font color='#FF0000'><b>Not Completed</b></font>"; 
$fp  = "<font color='#006600'><b>Completed</b></font>" ;
?>
<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
     MRD Room
     <small>MRD Transfer</small>
   </h1>
   <ol class="breadcrumb">
    <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."patient/mrd_request"; ?>">MRD Transfer</a></li>
      <li class="active"><?php echo $page_title; ?></li>
  </ol>&nbsp;&nbsp; 
   <?php if($this->session->flashdata('Success')){ ?>
           <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?> 
    
</section>
<section class="content">    <!-- Success-Messages -->
  <div class="box box-info">
    <div class="box-header">
     
    <h3 class="box-title"><i class="fa fa-th"></i> <?=$page_title?>
    </h3>
    <div class="box-tools">

      <?php
      if($page == "send")
      {
        $pg_redirect = "mrd_given";
        $pg_disp = "MRD Return";
      }
      else
      {
        $pg_redirect = "mrd_request";
        $pg_disp = "MRD Request";
      }

       ?>
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/mrd_room/<?=$pg_redirect?>" 
       >
      <i class="fa fa-plus-circle"></i> <?=$pg_disp ?>
      </a>
    </div>

  </div><!-- /.box-header -->

  <div class="box-body">
    <div id="example_wrapper" class="table-responsive">
      <div class="row">
        <div class="col-md-12">
             <div class="col-md-12">
            <table>
              <tr>
                <th style="text-align:center"><u>Morning Shift</u></th>
                <th style="text-align:center"><u>Evening Shift</u></th>
                </tr>
                <tr>
                <td width="50%" style="border-right:thin dotted #000; padding-right:20px">
                         
                  <table class="table table-condensed dataTable no-footer purchase_table">
              <thead>
                <tr>

                  <th>OP No</th>
                  <th>Date</th>
                  <th>MRD</th>
                  <th>Doctor</th>
                  <th>Patient</th>
                  <th>Remark</th>
                  <?php if($page == "send")    { ?> <th>Take</th> <?php } ?>
                  <?php if($page == "receive") { ?> <th>Return</th> <?php } ?>
                </tr>
              </thead>
              <tbody>
               <?= $output_m ?>
             </tbody>
          </table></td>

                <td width="50%" style="padding-left:10px">
                  <table class="table table-condensed dataTable no-footer purchase_table">
              <thead>
                <tr>

                  <th>OP No</th>
                  <th>Date</th>
                  <th>MRD</th>
                  <th>Doctor</th>
                  <th>Patient</th>
                  <th>Remark</th>
                  <?php if($page == "send")    { ?> <th>Take</th> <?php } ?>
                  <?php if($page == "receive") { ?> <th>Return</th> <?php } ?>
                </tr>
              </thead>
              <tbody>
               <?= $output_e?>
                                    

            </tbody>
          </table></td>
              </tr>


            </table>
            
        </div> </div> 
      </div> </div>
    </div><!-- /.box-body -->
  </div>
</section>
</section><!-- /.right-side -->

