

<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>


  <!-- Amount Paid -->
  <script>
  $(document).ready(function() {
  $('#balance_paid').change(function() {

    var amount_paid     =  $('#amount_paid').val();
    var amountpaid      =  $('#amountpaid').val();
    var amount_balance  =  $('#amount_balance').val();
    var amountbalance   =  $('#amountbalance').val();
    var balancepaid     =  $('#balance_paid').val();

    if(balancepaid>=0) {
      amount_paid     =  parseFloat(amountpaid);
      amount_balance  =  parseFloat(amountbalance);
    }

    var balance_pay       = parseFloat(amount_balance) - parseFloat(balancepaid);
    var amount_paid       = parseFloat(amount_paid) + parseFloat(balancepaid);


    $('#amount_paid').val(amount_paid);
    $('#amount_balance').val(balance_pay);
    $('#amount_paid1').html(amount_paid);
    $('#amount_balance1').html(balance_pay);


    if(balance_pay<0)
    {
      var Printid   =  $('#Printid').val();
      alert("Invalid Amount");
      window.location="<?= base_url() ?>purchase/purchase_View/"+Printid;
    }


  });
  });
  </script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>


  <?php
foreach($purchases as $purchase)
{
  $ve_id          = $purchase['ve_id'];
  $ve_bill_no     = $purchase['ve_bill_no'];
  $ve_vno         = $purchase['ve_vno'];
  $ve_pono        = $purchase['ve_pono'];
  $ve_date        = $purchase['ve_date'];
  $sp_id          = $purchase['sp_id'];
  $sp_vendor      = $purchase['sp_vendor'];
  $sp_phone       = $purchase['sp_phone'];
  $ve_amount      = $purchase['ve_amount'];
  $ve_discount    = $purchase['ve_discount'];
  $ve_discounta   = $purchase['ve_discounta'];
  $ve_sgst        = $purchase['ve_sgst'];
  $ve_cgst        = $purchase['ve_cgst'];
  $ve_gtotal      = $purchase['ve_gtotal'];
  $ve_apayable    = $purchase['ve_apayable'];
  $ve_apaid       = $purchase['ve_apaid'];
  $ve_balance     = $purchase['ve_balance'];
  $ve_round       = $purchase['ve_round'];
  $ve_pstaus      = $purchase['ve_pstaus'];
  // $user_name      = $purchase['u_emp_id'];
  $ve_status      = $purchase['ve_status'];
}

?>

<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
     Purchase
      <small>View  Purchase</small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."purchase"; ?>">Purchase</a></li>
      <li class="active"></li> <?=$page_title?>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> Purchase
       </h3>
        <div class="box-tools">
              <a title="Back" class="btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."purchase"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>
     </div>

     <form id="post_form">
     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">
           
            <div class="form-group col-md-2">
            <label  class="control-label">Voucher No. :</label> <?= $ve_vno  ?><br>
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Date :</label> <?= date("d-m-Y",strtotime($ve_date))  ?><br>
            <input type ="hidden" name="ve_date" value="<?=$ve_date?>">
          </div>
          <div class="form-group col-md-3"></div>
          <div class="form-group col-md-2">
            <label  class="control-label">PO No. :</label> <?= $ve_pono  ?><br>
            <input type ="hidden" name="po_pono" value="<?=$ve_pono?>">
          </div>

          <!-- <div class="form-group col-md-3">    
          <label class="control-label">User Name : </label> <?= $user_name ?>
          </div> -->

          <div class="form-group col-md-2">
            <label  class="control-label">Supplier ID :</label> <?= $sp_id  ?> <br>         
          </div>

          <div class="form-group col-md-3">
            <label  class="control-label">Supplier :</label> <?= $sp_vendor  ?><br>
          </div>
          <div class="form-group col-md-1"></div>

          <div class="form-group col-md-3">
            <label  class="control-label">Supplier Ph. :</label> <?= $sp_phone  ?><br>
          </div>
          <div class="form-group col-md-3">    
          <label class="control-label">Supplier Bill No.  : </label> <?= $ve_bill_no ?>
          </div>

 <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">

               
                   <table class="table table-condensed dataTable no-footer gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Sl. No </th>
                        <th>Product</th>
                        <th>Batch</th>
                        <th>Expiry</th>
                        <th>Unit Price</th>
                        <th>Sell Price</th>
                        <th>Qty</th>
                        <th>Free</th>
                        <th>CGST</th>
                        <th>SGST</th>
                        <th>Sub Total</th>
                      </tr>
                    </thead>
                  <tbody> 
                    <?php
                    $slno=1;
                    foreach($purchases as $purchase)
                      { ?> 
                    <tr>
                      <td><?= $slno++  ?></td>
                      <td><a href="<?= $this->config->item('admin_url')."product/view/".$purchase['ved_itemid'] ?>"><?=$purchase['ved_item'] ?></a></td>
                      <td><?=$purchase['ved_batch'] ?></td>
                      <td><?=$purchase['ved_expiry'] ?></td>
                      <td><?="Rs. ".$purchase['ved_price'] ?></td>
                      <td><?="Rs. ".$purchase['ved_slprice'] ?></td>
                      <td><?=$purchase['ved_qty']/$purchase['ved_uqty']." ".$purchase['ved_unit']  ?></td>
                      <td><?=$purchase['ved_free']/$purchase['ved_uqty']." ".$purchase['ved_unit']  ?></td>
                      <td><?="Rs. ".$purchase['ved_cgsta']." (".$purchase['ved_cgstp']."%)"  ?></td>
                      <td><?="Rs. ".$purchase['ved_sgsta']." (".$purchase['ved_sgstp']."%)"  ?></td>
                      <td><?="Rs. ".$purchase['ved_gtotal'] ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                  </table> 
                
                  <table class="table table-bordered">
                    <tr><th width="80%">Total</th>
                      <th>Rs. <label><?= $ve_amount ?></label></th></tr>
                      <tr><th width="80%">SGST Total</th>
                      <th>Rs. <label><?= $ve_sgst ?></label></th></tr>
                      <tr><th width="80%">CGST Total</th>
                      <th>Rs. <label><?= $ve_cgst ?></label></th></tr>
                      <tr><th width="80%">Discount</th>
                    <th>Rs.<label><?= $ve_discounta ?></label>( <?= $ve_discount ?>%)</th></tr>

                    <tr><th width="80%">Grand Total</th>
                    <th>Rs. <label><?= $ve_gtotal ?></label></th></tr>
                    <tr><th width="80%">Amount Payable</th>
                      <th><input type="hidden" name="grandtotal" readonly id="grand_total" value="<?= $ve_apayable ?>">Rs. <label id="grandtotal1"><?= $ve_apayable ?></label></th></tr>

                      <tr><th width="80%">Round off Value</th>
                      <th>Rs. <label><?= $ve_round ?></label></th></tr>
                    <input type="hidden" class="form-control" id="Printid" value="<?= $ve_id ?>">

                     <tr><th width="80%">Amount Paid</th>
                      <input type="hidden" class="form-control" name="ve_apaid" readonly id="amount_paid" value="<?= $ve_apaid ?>">
                      <input type="hidden" class="form-control" readonly id="amountpaid" value="<?= $ve_apaid ?>">
                      <th>Rs.  <label><?= $ve_apaid ?></label></th></tr>
                      <?php if($ve_balance>0) { ?>
                    <tr><th width="80%">Balance to Pay</th><th>
                      <input type="hidden" class="form-control" name="amount_balance" readonly id="amount_balance" value="<?= $ve_balance ?>">
                      <input type="hidden" class="form-control" readonly id="amountbalance" value="<?= $ve_balance ?>">

                      Rs. <label id="amount_balance1"><?= $ve_balance ?></label></th></tr>
                      
                  <tr><th width="80%">Balance Payment</th>
                      <th>
                          <input <?php if($ve_balance==0) { ?> type="hidden" <?php } else { ?>type="input" <?php } ?> class="form-control" name="balance_paid" id="balance_paid" autofocus="autofocus"/>
                        <label><?php if($ve_balance==0) { echo $ve_apaid; } ?></label></th></tr>
                      <?php } ?>
                    </table>
   
<br><?php if($ve_balance!=0) { ?><a href="#" class="btn btn-success" onclick="save();"><i class="fa fa-floppy-o"></i> Save</a> <?php } ?>
<!-- <input type="input" id="Printid"> -->
 <a href="#" class="btn btn-warning" onclick="print();"><i class="fa fa-print"></i> Print</a>


          </div>
        </div>
      </div>
    </div>
  </div>
</div>
           

        </div>
      </div>
      
      
    </div>
  </form><br>
  </div>
</section>

</section><!-- /.right-side -->

<script>
function save(){

      var postData = $("#post_form").serializeArray();
      var amount_paid    =  $('#balance_paid').val();
      if(amount_paid=="")
          {
            alert("Enter the amount paid");
          }
          else
          {
      var Printid         =  $('#Printid').val();
      var formURL  = "<?= base_url() ?>purchase/purchaseVUpdate/?pUpid="+Printid;
      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          alert("Success");
           window.location = "<?php echo base_url();?>index.php/purchase/purchase_view/"+Printid;

        }

      });
    }
}


function print(){

          var Printid         =  $('#Printid').val();
          window.location = "<?php echo base_url();?>index.php/purchase/getPrint?pPrintid="+Printid;
}
</script>

