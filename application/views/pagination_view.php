<link rel="stylesheet" href='https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css' type="text/css"/>
<link rel="stylesheet" href='https://cdn.datatables.net/scroller/1.4.3/css/scroller.dataTables.min.css' type="text/css"/>
<script src="//code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/scroller/1.4.3/js/dataTables.scroller.min.js"></script>

<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>



<section class="right-side" style="min-height:700px;">
    <section class="content-header">
        <h1>
           <?php echo $page_title; ?>
       </h1>
       <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?php echo $page_title; ?></li>
    </ol>&nbsp;&nbsp;
     <?php if($this->session->flashdata('Success')){ ?>
           <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
  
</section>

<section class="content">    <!-- Success-Messages -->
    <div class="box box-info">
        <div class="box-header">
           <div class="row">
        <div class="col-md-8 col-md-offset-2 well">
        <?php 
        $attr = array("class" => "form-horizontal", "role" => "form", "id" => "form1", "name" => "form1");
        echo form_open("pagination/search", $attr);?>
            <div class="form-group">
                <div class="col-md-6">
                    <input class="form-control" id="book_name" name="book_name" placeholder="Search for Book Name..." type="text" value="<?php echo set_value('book_name'); ?>" />
                </div>
                <div class="col-md-6">
                    <input id="btn_search" name="btn_search" type="submit" class="btn btn-danger" value="Search" />
                    <a href="<?php echo base_url(). "index.php/pagination/index"; ?>" class="btn btn-primary">Show All</a>
                </div>
            </div>
        <?php echo form_close(); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2 bg-border">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Book Name</th>
                    <th>Author Name</th>
                    <th>ISBN</th>
                    </tr>
                </thead>
                <tbody>
                <?php for ($i = 0; $i < count($booklist); ++$i) { ?>
                <tr>
                    <td><?php echo ($page+$i+1); ?></td>
                    <td><?php echo $booklist[$i]->name; ?></td>
                    <td><?php echo $booklist[$i]->author; ?></td>
                    <td><?php echo $booklist[$i]->isbn; ?></td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <?php echo $pagination; ?>
        </div>
        
        </div><!-- /.box-body -->
</div>
</section>
</section><!-- /.right-side -->


<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>

<style type="text/css">
.codpage{
    box-sizing: border-box;
  display: inline-block;
  min-width: 1.5em;
  padding: 0.5em 1em;
  margin-left: 2px;
  text-align: center;
  text-decoration: none !important;
  cursor: pointer;
  *cursor: hand;
  color: #333 !important;
  border: 1px solid transparent;

}

.codpage:hover {
  color: #333 !important;
  border: 1px solid #cacaca;
  background-color: white;
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, white), color-stop(100%, #dcdcdc));
  /* Chrome,Safari4+ */
  background: -webkit-linear-gradient(top, white 0%, #dcdcdc 100%);
  /* Chrome10+,Safari5.1+ */
  background: -moz-linear-gradient(top, white 0%, #dcdcdc 100%);
  /* FF3.6+ */
  background: -ms-linear-gradient(top, white 0%, #dcdcdc 100%);
  /* IE10+ */
  background: -o-linear-gradient(top, white 0%, #dcdcdc 100%);
  /* Opera 11.10+ */
  background: linear-gradient(to bottom, white 0%, #dcdcdc 100%);
  /* W3C */
}
.codpage:active {
outline: none;
  background-color: #48a0e2;
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #48a0e2), color-stop(100%, #48a0e2));
  /* Chrome,Safari4+ */
  background: -webkit-linear-gradient(top, #48a0e2 0%, #48a0e2 100%);
  /* Chrome10+,Safari5.1+ */
  background: -moz-linear-gradient(top, #72d3f9 0%, #48a0e2 100%);
  /* FF3.6+ */
  background: -ms-linear-gradient(top, #72d3f9 0%, #48a0e2 100%);
  /* IE10+ */
  background: -o-linear-gradient(top, #72d3f9 0%, #48a0e2 100%);
  /* Opera 11.10+ */
  background: linear-gradient(to bottom, #72d3f9 0%, #48a0e2 100%);
  /* W3C */
  box-shadow: inset 0 0 3px #111;
}

</style>

<!-- box-sizing: border-box;
  display: inline-block;
  min-width: 1.5em;
  padding: 0.5em 1em;
  margin-left: 2px;
  text-align: center;
  text-decoration: none !important;
  cursor: pointer;
  *cursor: hand;
  color: #333 !important;
  border: 1px solid transparent; -->
 
