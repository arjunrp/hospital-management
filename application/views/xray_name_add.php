<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?> 
<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
       <?php echo $page_title; ?>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."xrayname"; ?>">Xray Name</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div align="right">
         <a title="short key-ALT+B" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."xrayname"; ?>"accesskey="b" ><i class="fa fa-mail-reply-all"></i> Back</a>
       </div>

     </div>

     <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row">
          <div class="col-md-12">
        <div class="col-md-6">
          <div class="form-group required">
            <label  class="control-label" >Test<sup></sup></label>
            <select class="form-control input-sm" tabindex="1"  name="xt_xr_id"> 
              <option value="Select">--Select--</option>
              <?php foreach ($categories as $key => $mw_xray) {
                ?>
              <option value="<?php echo $mw_xray['xr_id']?>"<?php if($mw_xray['xr_id']==$catid) { ?> selected="selected" <?php } ?>><?php echo $mw_xray['xr_test']?></option>
                <?php
              }?>
            </select>               
          </div>

<div class="form-group required">
          <label  class="control-label">X-ray Name<sup></sup></label>
          <input  class="form-control validate[required] input-sm"  tabindex="2" type="text" name="xt_name" value="<?=$xt_name?>">
           <input class="form-control"  type="hidden" name="xt_id" value="<?=$xt_id?>">                
        </div>
<div class="form-group required">
          <label  class="control-label">Price<sup></sup></label>
          <input  class="form-control validate[required,customs[number]] input-sm"  tabindex="3"  type="text" name="xt_price" value="<?=$xt_price?>">               
        </div>


 
          
        </div>



         
      <!--   <div class="col-md-6">
      <div class="form-group required">
          <label  class="control-label">Men<sup></sup></label>
          <input  class="form-control validate[required]"  type="text" name="type" value="<?=$type  ?>">
                     
        </div>

      <div class="form-group required">
          <label  class="control-label"> Woman<sup></sup></label>
          <input  class="form-control validate[required]"  type="text" name="type" value="<?=$type  ?>">
                        
        </div>

          <div class="form-group required">
          <label  class="control-label">Child<sup></sup></label>
          <input  class="form-control validate[required]"  type="text" name="price" value="<?=$price  ?>">             
        </div>
  
 -->
        
      </div></div>
      <br>
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            <div>
              <input class="btn-large btn-primary btn" type="submit" value="Submit" tabindex="4" name="submit" accesskey="s" title="short key-ALT+S"> 
              <input class="btn-large btn-default btn" type="reset"  tabindex="5" value="Reset">
            </div>
          </div>

        </div>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</section>
  
</section><!-- /.right-side -->

 