<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#customer_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>

 <?php
foreach ($scan_bills as $key => $scan_bill) {

    $ve_id            = $scan_bill['ve_id'];
    $ve_vno           = $scan_bill['ve_vno'];
    $ve_date          = $scan_bill['ve_date'];
    $ve_customer      = $scan_bill['ve_customer'];
    $ve_mrd           = $scan_bill['ve_mrd'];
    $p_name           = $scan_bill['ve_patient'];
    $p_phone          = $scan_bill['ve_phone'];
    $p_age            = $scan_bill['ve_amount'];
    $p_gender         = $scan_bill['ve_pono'];
    $ve_user          = $scan_bill['ve_user'];
    $ve_type          = $scan_bill['ve_type']; 
    $doctor           = $scan_bill['ve_doctor']; 
  }

  ?>

  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       Scanning
       <small>View</small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."scanning"; ?>">Scanning</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-success pull-right  view-btn-create" href="<?php echo base_url();?>index.php/investigation">
        <i class="fa fa-mail-reply-all"></i> Back
      </a>
    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>

      <form id="post_form">
     <div class="box-body">
      <div class="row"> 
       <div class="col-md-12">
        <div class="col-md-4">
          <label  class="control-label">Bill No. :<sup></sup></label> <?=$ve_vno?>
          <input class="form-control" type="hidden" name="user_type" value="<?=$user_type?>">
          <input class="form-control" type="hidden" name="ve_date" value="<?=$ve_date?>">
          <input class="form-control" type="hidden" id="ve_id" name="ve_id" value="<?=$ve_id?>">              
        </div>
        <div class="col-md-4">
          <label  class="control-label">Date : <sup></sup></label> <?=date("d-m-Y",strtotime($ve_date)) ?>                
        </div>

        <div class="col-md-4">
         <label  class="control-label">Doctor  : <sup></sup></label> <?=$doctor?>
       </div>
        <div class="col-md-12"><br></div>
        <?php if($ve_type=="lbi") { ?>
         <div class="col-md-3">
         <label  class="control-label">IP No. : <sup></sup></label> <?=$ve_customer?>
       </div>
       <?php } ?>

          <div class="col-md-3 ipno1">
         <label  class="control-label">MRD : <sup></sup></label> <?=$ve_mrd?>
       </div>
       <div class="col-md-4">
         <label  class="control-label">Patient : <sup></sup></label> <?=$p_name?>
       </div>
       <div class="col-md-2">
         <label  class="control-label">Phone No. : <sup></sup></label> <?=$p_phone?>
       </div>
       <div class="col-md-2">
         <label  class="control-label">Gender. : <sup></sup></label> <?php
          if($p_gender=="M") { echo "Male"; } 
          if($p_gender=="F") { echo "Female"; }
          if($p_gender=="C") { echo "Child"; }
           ?>
       </div>
       <div class="col-md-2">
         <label  class="control-label">Age. : <sup></sup></label> <?=$p_age?>
       </div>
      </div>

          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover gridView" id="item_table">
                  <tbody> 
                    <?php foreach ($scan_contents as $key => $scan_content) {
                      ?> <th style="text-align:center" colspan="6">Test Name :
                      <?=$scan_content['ved_item'] ?> </th> 
                      <tr><th>Test Content</th><th>Result</th><th>Remarks</th><th>Normal Value</th></tr>
                      <?php  if(!empty($scan_content['scnt_content'])) { 
                         foreach ($scan_content['scnt_content'] as $output2) {  ?>
                      <tr><td width="15%"><input type="hidden" class="form-control input-sm" name="ind_id[]" value="<?=$output2['ind_id'] ?>"><?=$output2['ind_content'] ?> </td>
                      <td width="15%"><input type="input" class="form-control input-sm" name="ind_result[]" value="<?=$output2['ind_result'] ?>"></td>
                      <td width="19%"><input type="input" class="form-control input-sm" name="ind_remarks[]" value="<?=$output2['ind_remarks'] ?>"></td>
                      <?php if($p_gender=="M") { ?><td width="15%"><?=$output2['scnt_men'] ?> </td> <?php } ?>
                      <?php if($p_gender=="F") { ?><td width="15%"><?=$output2['scnt_women'] ?> </td> <?php } ?>
                      <?php if($p_gender=="C") { ?><td width="15%"><?=$output2['scnt_chid'] ?> </td> <?php } ?>
                    </tr><tr><td colspan="5"></td></tr>
                      <?php 
                    } } } ?>
                    

                  </tbody>
                  </table> 

                    
                  <input type="hidden" id="Printid"> 
                  <a href="#" class="btn btn-success" onclick="save();" accesskey="p" title="short key-ALT+P" ><i class="fa fa-floppy-o"></i> Update</a> 
                    <a href="#" class="btn btn-warning" onclick="print();" accesskey="p" title="short key-ALT+P" ><i class="fa fa-print"></i> Print</a> 
                
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</form>
</div>
</section>

</section><!-- /.right-side -->

<script>
function save(){

      var postData = $("#post_form").serializeArray();
      var formURL  = "<?= base_url() ?>scanning/resultUpdate";
      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
           window.location = "<?php echo base_url();?>index.php/scanning/view/"+data;
        }
      });
}
function print(){

          var Printid         =  $('#ve_id').val();
          if(Printid=="")
          {
            alert("Click Save Before Print");
          }
          else
          {
          window.location = "<?php echo base_url();?>index.php/scanning/getResultPrint?Printid="+Printid;
          }

}
</script>

<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });</script>
<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>