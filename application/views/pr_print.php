<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<html>
<head>
<link rel="stylesheet" href='<?= base_url() ?>asset/plugins/jvectormap/jquery-jvectormap-1.2.2.css' type="text/css"/>
  <link media="all" type="text/css" rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.css">
   <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.min.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.min.css' type="text/css"/>
   <link rel="stylesheet" href="<?= base_url() ?>asset/css/skins/_all-skins.min.css"> 
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
 
  <!-- <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datepicker/datepicker3.css"> -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<!-- <link rel="stylesheet" href="<?= base_url() ?>asset/css/AdminLTE.min.css" type="text/css"/>  -->
   <link rel="stylesheet" href="<?= base_url() ?>asset/bootstrap/css/AdminLTE.min.css" type="text/css"/>
   <script  src="<?= base_url() ?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
   <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
   <style type="text/css">
   .font_reduce
   {
     font-size:small;
   }
   .font_reduce1
   {
     font-size:x-small;
   }
   .u {
    text-decoration: underline;
}
   </style>
</head>
 <body style="margin-top:0%;display:block;height:100%;width:80%;margin-left:9%;margin-right:6% " onload="window.print();">

<!-- onload="window.print();"  -->
    <?php foreach($company as $company) {
    $company_name     = $company['company'];
    $company_address  = $company['address'];
    $company_street   = $company['street'];
    $company_city     = $company['ctname'];
    $company_state    = $company['stname'];
    $company_country  = $company['coname'];
    $company_zip      = $company['zip'];
    $company_phone    = $company['phone'];
    $company_email    = $company['email'];
    $company_website  = $company['website'];
    $company_gst_no   = $company['gst_no'];
    $company_drug_lic = $company['drug_lic'];
    $company_tags     = $company['tags'];
}


foreach($preturns as $preturn)
{

  $ve_bill_no     = $preturn['ve_bill_no'];
  $ve_vno         = $preturn['ve_vno'];
  $ve_date        = $preturn['ve_date'];
  $sp_id          = $preturn['sp_id'];
  $sp_vendor      = $preturn['sp_vendor'];
  $sp_phone       = $preturn['sp_phone'];
  $ve_amount      = $preturn['ve_amount'];
  $ve_sgst        = $preturn['ve_sgst'];
  $ve_cgst        = $preturn['ve_cgst'];
  $ve_gtotal      = $preturn['ve_gtotal']; 
  $ve_apayable    = $preturn['ve_apayable']; 
  $ve_round       = $preturn['ve_round']; 
  $ve_apaid       = $preturn['ve_apaid']; 
  $ve_balance     = $preturn['ve_balance']; 
  $ve_type        = $preturn['ve_user']; 
}
?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {

 oTable = $('#category_table').dataTable({
});
});
</script>


<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
         <h4 style="border-bottom: thin dotted #000; font-family: Arial Black, Georgia, Serif; padding-bottom:5px">
          <center><b><?= strtoupper($company_name) ?></b></center>
          <small class="font_reduce"><center><b><?= "[ ".strtoupper($company_tags)." ]" ?></b></center></small>
           <center class="pull-center font_reduce1"><?= strtoupper($company_address).", ".strtoupper($company_street)." ,".strtoupper($company_city)." - ".$company_zip.", PH. : ".$company_phone  ?></center>
           <center class="pull-center font_reduce1"><?= "GST : ".strtoupper($company_gst_no).", Drug Lic No : ".strtoupper($company_drug_lic) ?> </center> </b>
                    </h4>
         <center> <small class="pull-center"><b><u>PURCHASE RETURN VOUCHER</u></b></small></center>

  <div class="box-tools">
              <a title="Back" class="dontprint btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."preturn/return_Padd"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>
    </div> </div>
    <!-- info row -->

     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">
          <br>
          <table class="font_reduce" width="100%">
            <tr>
              <th width="30%">Voucher No.         : <?= $ve_vno?></th>
              <th width="30%"></th>
              <th width="30%">Date                : <?= date("d-m-Y",strtotime($ve_date))?></th>
          </tr>
            <tr>
            <th width="30%">Purchase Bill No    : <?= $ve_bill_no?> </th>
            <th width="30%">Supplier            : <?= $sp_vendor?> </th>
            <th width="30%">Supplier Ph. No     : <?= $sp_phone?> </th>
          </tr>
           </table>
    </div>
      <!-- /.col -->

    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive" id="example-wrapper">
         <br>
       <table cellspacing="10" width="100%" class="font_reduce" style="border-top: thin dashed #000;border-bottom: thin solid #000; margin-bottom:10px">
          <thead>
                      <tr style="border-bottom: thin dashed #000">
                        <th height="35px">Sl. No </th>
                        <th>Product</th>
                        <th>Unit Price</th>
                        <th>Quantity</th>
                        <th>Batch</th>
                        <th>Expiry</th>
                        <th>Sub Total</th>
                        <th>SGST</th>
                        <th>CGST</th>
                        <th>Total(Inc. GST)</th>
                      </tr>
                    </thead>
                   <tbody> 
                    <?php
                  $slno=1;
                  foreach($preturns as $preturn)
                  { 
                    $sgstp = ($preturn['ved_sgsta']*100)/$preturn['ved_total'];
                    $cgstp = ($preturn['ved_cgsta']*100)/$preturn['ved_total'];
                    ?> 
                    <tr>
                      <td height="30px"><?= $slno++  ?></td>
                      <td><?=$preturn['ved_item'] ?></td>
                      <td><?=$preturn['ved_price'] ?></td>
                      <td><?=$preturn['ved_qty']." ".$preturn['ved_unit'] ?></td>
                      <td><?=$preturn['ved_batch'] ?></td>
                      <td><?=$preturn['ved_expiry'] ?></td>
                      <td><?=$preturn['ved_total'] ?></td>
                      <td><?=$preturn['ved_sgsta']." (".$preturn['ved_sgstp']." %)" ?></td>
                      <td><?=$preturn['ved_cgsta']." (".$preturn['ved_cgstp']." %)" ?></td>
                      <td><?=$preturn['ved_gtotal'] ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                  </table> 
                
                    <table width="100%" class="font_reduce" style="border-bottom: thin dashed #000; margin-bottom:10px">
                    <tr><th class="u">Total</th><th class="u">SGST Total</th><th class="u">CGST Total</th><th class="u">Grand Total</th><th class="u">Amount Payable</th><th class="u">Round off</th><th class="u">Amound Paid</th><th class="u">Balance</th></tr>
                    <tr>
                      <th>Rs. <label><?= $ve_amount ?></label></th>
                      <th>Rs. <label><?= $ve_sgst ?></label></th>
                      <th>Rs. <label><?= $ve_cgst ?></label></th>
                      <th>Rs. <label><?= $ve_gtotal ?></label></th>
                      <th>Rs. <label><?= $ve_apayable ?></label></th>
                      <th>Rs. <label><?= $ve_round ?></label></th>
                      <th>Rs. <label><?= $ve_apaid ?></label></th>
                      <th>Rs. <label><?= $ve_balance ?></label></th>
                    </tr>
                    </table>
        </div>
        <div class="col-xs-12" style="text-align:right">
          Authorized Signature  with Seal<br><br><br><br><br><br><br>
      </div>
      <!-- /.col -->
    </div>

     </div>
       
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
 
 <style type="text/css" media="print">
.dontprint
{ display: none; }
</style>
</body>
</html>