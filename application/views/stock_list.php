<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
  "aaSorting": [[ 2, "asc" ]],
  "bJQueryUI": true,
  "sPaginationType": "full_numbers",
  "iDisplayLength": 10,
  "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
});
});
</script>
<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
      <?php echo $page_title; ?>
   </h1>
   <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
     
    <li class="active"><?php echo $page_title; ?></li>
  </ol>
</section>

<section class="content">    <!-- Success-Messages -->
  <div class="box box-info">
    <div class="box-header">
    <h3 class="box-title"><i class="fa fa-th"></i> Stock
    </h3>
    <!-- <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/purchase">
        <i class="fa fa-plus-circle"></i> Create New
      </a>

    </div> -->
  </div><!-- /.box-header -->

  <div class="box-body">
    <div id="example_wrapper" class="table-responsive">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-12">
            <table id="category_table" class="table table-condensed dataTable no-footer">
              <thead>
                <tr>
                  <th>Sl.no</th>
                  <th>Product Code</th>
                  <th>Product</th>
                  <th>Min Quantity</th>
                  <th>Max Quantity</th>
                  <th>Stock Quantity</th>
                  <th>Product Type</th>
                  <th>Options</th> 
                </tr>
              </thead>
              <tbody>
                  <?php echo $output; ?>                          

            </tbody>
          </table>
        </div> </div> </div> 
      </div>
    </div><!-- /.box-body -->
  </div>
</section>
</section><!-- /.right-side -->

