<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

   <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
   <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->



  
  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#purchase_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>

<!-- Product Delete -->
<script>
  $(document).ready(function() {
    setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);

    <?php
$js_array = json_encode($stock_products);
echo "var st_prod = ". $js_array . ";\n";
?>


$.each(st_prod, function(index) {
        // alert(sum+","+vesgst+","+vecgst+","+apayable);

         var newrow      = '<tr><td>'+st_prod[index].dp_department+'</td><td><input type="hidden" value="'+st_prod[index].stock_id+'" name="stock_id[]"><input type="input" value="'+st_prod[index].stock_batch+'" name="stock_batch[]"></td><td><input class="form-control input-sm" type="input" name="stock_qty[]" value="'+st_prod[index].stock_qty+'"></td></tr>';
        $('#item_table tr:last').after(newrow);
      });
  });
  </script>



  <section class="right-side" style="min-height:700px;"> <?php 
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       Stock
       <small>Edit Stock </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."stock"; ?>">Stock</a></li>
      <li class="active"> <?=$page_title ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?= $page_title?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/stock">
        <i class="fa fa-plus-circle"></i> View All Stock
      </a>
    </div>
     </div><br>
      <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success"> 
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>

      <form id="post_form">
     <div class="box-body">
        
      <div class="row">                

       <div class="col-md-12">
         <div class="form-group col-md-2">
            <label  class="control-label">Product Code <sup>*</sup></label><br>
            <?= $pd_code ?>
          </div>

          <div class="form-group col-md-3">
            <label  class="control-label">Brand<sup>*</sup></label><br>
           <?php foreach ($brands as $key => $brand) {
              if($brand['br_id']==$pd_brandid) { echo $brand['br_brand']; } 
              } ?>             
          </div>
          <div class="form-group col-md-4">
            <label  class="control-label">Suppliers<sup>*</sup></label><br>
            <?php $pdsupplier1=""; $pdsupplier=""; 
            foreach ($suppliers as $key => $supplier) {
              $e = explode(",", $pd_supplier);
              if(in_array($supplier['sp_id'],$e)) { 
                $pdsupplier[]=$supplier['sp_vendor'];
                $pdsupplier1= implode(',',$pdsupplier); } 
              } echo $pdsupplier1;  ?>               
          </div>

          <div class="form-group col-md-3">
            <label  class="control-label">Main Content <sup>*</sup></label><br>
             <?php $pdcontentid1=""; $pdcontentid=""; 
            foreach ($contents as $key => $content) {
              $e1 = explode("+", $pd_contentid);
              if(in_array($content['mc_id'],$e1)) { 
                $pdcontentid[]=$content['mc_content'];
                $pdcontentid1= implode('+',$pdcontentid); } 
              } echo $pdcontentid1;  ?> 
          </div>

          <div class="form-group col-md-3">
             <label  class="control-label">GST-State(%) <sup>*</sup></label><br>
            <?= $pd_sgst  ?>                 
          </div> 
           
           <div class="form-group col-md-3">
            <label  class="control-label">GST-Central(%) <sup>*</sup></label><br>
            <?= $pd_cgst  ?>          
          </div>

          <div class="form-group col-md-2">
            <label  class="control-label">Min Qty<sup>*</sup></label><br>
            <?=$pd_min  ?>
            </div>

           <div class="form-group col-md-2">
            <label  class="control-label">Max Qty <sup>*</sup></label><br>
           <?=$pd_max  ?>
          </div>

          <div class="form-group col-md-3">        
          <label  class="control-label">Product Type<sup>*</sup></label><br>
          <?php if($pd_type=="g") { echo "General"; }
              else if($pd_type=="m") { echo "Medical"; } ?>                
          </div>

          <div class="form-group col-md-3">
            <label  class="control-label">Unit<sup>*</sup></label><br>
             <?=$pd_unit ?>

          </div> 

           <div class="form-group col-md-3">        
          <label  class="control-label">Per No.s<sup>*</sup></label><br>
          <?= $pd_qty  ?>                
          </div>



            </div>
          </div>
          <div class="box-footer">
            <div class="row">
 

            </div>
          </div>
          <?php //echo form_close(); ?>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table width="50%" class="table table-striped table-bordered table-hover gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Department</th>
                        <th>Batch</th>
                        <th>Qty</th>
                      </tr>
                    </thead>
                  <tbody> 
                  </tbody>
                  </table> 
   <button class="btn-large btn-success btn" type="submit" onclick="save();" name="submit1" accesskey="s" title="short key-ALT+S"><i class='fa fa-floppy-o'></i> Update</button>
   <input type="hidden" id="Printid">

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
</div>
</section>

</section><!-- /.right-side -->

<script>

function save(){

      var postData = $("#post_form").serializeArray();

      var formURL  = "<?= base_url() ?>stock/update";
      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          window.location = "<?php echo base_url();?>index.php/stock";
          <?php 
          $this->session->set_flashdata('Success','Stock Updated'); ?>
        }
      });

}

    </script>

<script>
 $('.datepicker').datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true
    });</script>