<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
<?php
foreach($stadjusts as $stadjust)
{
  $ve_id          = $stadjust['ve_id'];
  $ve_vno         = $stadjust['ve_vno'];
  $ve_date        = $stadjust['ve_date'];
  $dp_department  = "Central Store";
  $ve_customer    = $stadjust['ve_customer'];
  $u_emp_id       = $stadjust['u_emp_id'];
}

?>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
  
 oTable = $('#category_table').dataTable({
});
});
</script>

<div class="content-wrapper">
  <section class="content-header">
    <h1>
     Consumption
      <small><?= $page_title ?></small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."stock_adjust/"; ?>">Stock Adjust</a></li>
      <li class="active"><?= $page_title ?></li>
    </ol>
    <?php if($this->session->flashdata('Success')){ ?>
           <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
  </section>
  <section class="content">
    <div class="box box-primary">
      
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?= $page_title ?>
       </h3>
        <div class="box-tools">
              <a title="Back" class="btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."stock_adjust"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>
     </div>


      <form id="post_form">
     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">

        <div class="form-group col-md-2">
            <label  class="control-label">Consume No. : </label> <?=  $ve_vno ?><br>
          </div>
          <div class="form-group col-md-4">
            <label  class="control-label">Consume Date : </label> <?= date("d-m-Y",strtotime($ve_date)) ?><br>
          </div>
          <div class="form-group col-md-3" style="text-align:right">    
          <label class="control-label">Department : </label> <?= $dp_department ?>
          </div>
          <div class="form-group col-md-3" style="text-align:right">    
          <label class="control-label">User Name : </label> <?= $u_emp_id ?>
          </div>
          <div class="form-group col-md-3">    
          <label class="control-label">Remarks : </label> <?= $ve_customer ?>
          </div>
            </div>
          </div>

          <div class="row">
          <div class="col-md-12">
             
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
               
                                    <table class="table table-condensed dataTable no-footer gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Sl. No </th>
                        <th>Product</th>
                        <th>Batch</th>
                        <th>Expiry</th>
                        <th>Quantity</th>
                      </tr>
                    </thead>
                  <tbody>
                  <?php
                  $slno=1;
                  foreach($stadjusts as $stadjust)
                  { ?> 
                    <tr>
                      <td align="center"><?= $slno++  ?></td>
                      <td><a href="<?= $this->config->item('admin_url')."product/view/".$stadjust['ved_itemid'] ?>"><?=$stadjust['ved_item'] ?></a></td>
                      <td><?=$stadjust['ved_batch'] ?></td>
                      <td><?=$stadjust['ved_expiry'] ?></td>
                      <td><?=$stadjust['ved_qty']/$stadjust['ved_uqty']." ".$stadjust['ved_unit'] ?></td>
                    </tr>
                    <?php } ?>

                  </tbody>
                  </table> 
              
<input type="hidden" id="Printid" value="<?=$ve_id ?>">

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
           

        </div>
      </div>
     <!--  <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            
          </div>

        </div>
      </div> -->
     
    </form>
   
</section>
</div>
<!-- /.right-side -->

<script>

function print(){

          var Printid         =  $('#Printid').val();
          window.location = "<?php echo base_url();?>index.php/consume/getPrint?cPrintid="+Printid;

}
</script>


