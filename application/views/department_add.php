<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>

<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?php echo $page_title; ?>
    </h1>

    <ol class="breadcrumb">
      <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
       
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div align="right">
         <a title="short key-ALT+B" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."department"; ?>" accesskey="b" ><i class="fa fa-mail-reply-all"></i> Back</a>
                </div>

     </div>

     <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row">
        <div class="col-md-3">
          <div class="form-group required">
            <label  class="control-label">Department<sup>*</sup></label>
            <input class="form-control validate[required] input-sm" tabindex="1"  data-prompt-position="topRight:80" type="text" name="department" value="<?=$department  ?>" >
             <input class="form-control"  type="hidden" name="id" value="<?=$id ?>">                 
          </div>
      </div>
          <div class="col-md-2">
          <div class="form-group required">
            <label  class="control-label">Short Name<sup>*</sup></label>
            <input class="form-control validate[required,custom[onlyLetterSp]] input-sm" data-prompt-position="bottomLeft:180"  tabindex="2" type="text" name="dp_short" value="<?=$dp_short  ?>" >               
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group required">
            <label  class="control-label">Type<sup>*</sup></label>
             <select class="form-control validate[required] input-sm" tabindex="3" id="dp_type" name="dp_type"> 
            <option value="0">--Select--</option>
              <option value="t" <?php if($dp_type=="t") { ?> selected="selected" <?php } ?> >--Treatment--</option>  
              <option value="nt" <?php if($dp_type=="nt") { ?> selected="selected" <?php } ?> >--Others--</option>   
              </select>        
          </div>
        </div>
      </div>

      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            <div>
              <input class="btn-large btn-primary btn" type="submit" value="Submit" tabindex="4" name="submit"accesskey="s" title="short key-ALT+S"> 
              <input class="btn-large btn-default btn" type="reset"  tabindex="5"  value="Reset">
            </div>
          </div>
        </div>
      </div>
      </div>
      <?php echo form_close(); ?>
    </div>
</section>

</div><!-- /.right-side -->
