
<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
  
  $('#u_fees').hide();
  
$('#u_type').change(function(){
 var u_type = $('#u_type option:selected').text();
 if(u_type=="Doctor")
 {
    $('#u_fees').show();
 }
 else
 {
   $('#u_fees').hide();
 }
});

$('#country').change(function(){
    var ctid = $('#country').val();
    $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>customer/get_state/"+ctid,
     success: function(data){
      $("#state").empty();
      $.each(data, function(index) {
        $("#state").append('<option value=' + data[index].id +'>'+data[index].name+'</option>');
      });
    }
  })
  });

$('#state').change(function(){
    var ctid = $('#state').val();
    $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>customer/get_city/"+ctid,
     success: function(data){
      $("#city").empty();
      $.each(data, function(index) {
        $("#city").append('<option value=' + data[index].id +'>'+data[index].name+'</option>');
      });
    }
  })
  });
 oTable = $('#category_table').dataTable({
});
});
</script>

 
<section class="right-side" style="min-height:700px;">
   <section class="content-header">
        <h1>
            Users
            <small>Create  User </small>
        </h1>

        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."users"; ?>">Users</a></li>
      <li class="active"><?php echo $page_title; ?></li>
        </ol>
    </section>
  <section class="content">
    <div class="box box-primary">
      
      <div class="box-header">
        <h3 class="box-title">
                       <i class="fa fa-th"></i> <?= $page_title ?>
                   </h3>
      <div align="right">
         <a title="short key-ALT+B" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."users"; ?>" accesskey="b"><i class="fa fa-mail-reply-all"></i> Back</a>
       </div>

     </div>

<?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          
           <?php
                        if($this->uri->segment(1)=="add")
                        $clas   =   'required="required"';
                      else
                      $clas = '';
                    ?> 

           
            <div align="form-group">
               <div class="col-md-3">  
                         
                        
                        <?php if($image != "") { echo "<img id='blah' width='139px' height='150px' src='".$this->config->item("base_url")."uploads/users/".$image."'>"; } else { echo "<img id='blah' width='150px' height='150px' src='".$this->config->item("base_url")."uploads/Default2.png"."'>"; } ?>
                        <input type="file" name="image" onchange="readURL(this);" <?php echo $clas; ?>>
                      </div></div>

                <div class="form-group col-md-2">
                <div class="form-group required">
                     <label for="category" class="control-label">Employee Id<sup>*</sup></label>
                     <input class="form-control input-sm validate[required]" tabindex="1" name="u_emp_id" type="text" value="<?= $u_emp_id ?>" style="height:29px";> 
                   </div>            
                </div>
                <div class="form-group col-md-2">
                <div class="form-group required">
                     <label for="category" class="control-label">Name<sup>*</sup></label>
                      <input class="form-control input-sm validate[required]"  data-prompt-position="bottomLeft:190" tabindex="2" placeholder="User Name" type="text" name="u_name" value="<?= $u_name ?>"  style="height:29px"; >
                      <input type="hidden" name="u_date" value="<?= date('Y-m-d') ?>">     
                   </div>            
                </div>

          <div class="form-group col-md-2">
           <div class="form-group required">
                 <label for="category" class="control-label">User Email<sup>*</sup></label>
                  <input class="form-control input-sm" tabindex="3" placeholder="User Email" type="text" name="u_email" value="<?= $u_email ?>"  style="height:29px"; >        
              </div>
          </div>
          <div class="form-group col-md-2">
            <div class="form-group required">
              <!-- , validate[required]minSize[6],maxSize[12] -->
                  <label for="category" class="control-label">Password<sup>*</sup></label>
                    <input class="form-control input-sm" data-prompt-position="bottomRight:50"  type="password" tabindex="4" placeholder="User Password" id="password" name="u_password" value="<?= $u_password ?>"  style="height:29px"; >                                
              </div>
            </div>

           <div class="form-group col-md-2">
           <div class="form-group required">
                  <label for="category" class="control-label">Repeat Password<sup>*</sup></label>
                        <input class="form-control input-sm validate[equals[password]]" placeholder="Repeat Password" tabindex="5" data-prompt-position="bottomRight:50" value="<?= $u_password ?>" type="password"  style="height:29px"; > 
              </div>
          </div>

          <div class="form-group col-md-2">
             <div class="form-group required">
                  <label for="category" class="control-label">User Type<sup>*</sup></label>
                  <select class="form-control input-sm validate[required]" tabindex="6" style="height:29px"; name="u_type" id="u_type"> 
                  <option value="Select">--Select--</option>
                 <?php foreach ($type as $key => $type) {
                  ?>
                <option value="<?php echo $type['ut_id']?>"<?php if($type['ut_id']==$u_type) { ?> selected="selected" <?php } ?>><?php echo $type['ut_user_type']?></option>
                <?php
              }?>
            </select>
              </div>                
          </div> 
          <div class="form-group col-md-2">
             <div class="form-group required">
                  <label for="category" class="control-label">Department<sup>*</sup></label>
                  <select class="form-control input-sm validate[required]" tabindex="7" style="height:29px"; name="u_department" id="u_department"> 
                  <option value="Select">--Select--</option>
                 <?php foreach ($department as $key => $department) {
                  ?>
                <option value="<?php echo $department['dp_id']?>"<?php if($department['dp_id']==$u_department) { ?> selected="selected" <?php } ?>><?php echo $department['dp_department']?></option>
                <?php
              }?>
            </select>
              </div>                
          </div>
          <div class="form-group col-md-2">
             <div class="form-group required">
                  <label for="category" class="control-label">Designation<sup>*</sup></label>
                  <select class="form-control input-sm validate[required]" tabindex="8" style="height:29px"; name="u_designation" id="u_designation"> 
                  <option value="Select">--Select--</option>
                 <?php foreach ($designation as $key => $designation) {
                  ?>
                <option value="<?php echo $designation['dg_id']?>"<?php if($designation['dg_id']==$u_designation) { ?> selected="selected" <?php } ?>><?php echo $designation['dg_designation']?></option>
                <?php
              }?>
            </select>
              </div>                
          </div>
        </div>

          <div class="col-md-12">
            <br>
          <div class="form-group col-md-2">
           <div class="form-group required">
                 <label for="category" class="control-label">Phone<sup>*</sup></label>
                  <input class="form-control input-sm validate[required]" tabindex="9" style="height:29px";  data-prompt-position="bottomLeft:190" placeholder="Phone" type="text" name="u_phone" value="<?= $u_phone ?>">        
              </div>
          </div>
          <div class="form-group col-md-2">
           <div class="form-group required">
                 <label for="category" class="control-label">Address<sup> </sup></label>
                  <input class="form-control input-sm" type="text" tabindex="10" style="height:29px";  name="u_address" placeholder="Address" value="<?= $u_address ?>">      
              </div>
          </div>
          <div class="form-group col-md-2">
           <div class="form-group required">
            <label for="category" class="control-label">&emsp;<sup> </sup></label>
                  <input class="form-control input-sm"  style="height:29px"; tabindex="11" type="text" placeholder="Street" name="u_street" value="<?= $u_street ?>">      
              </div>
          </div>
          
          <div class="form-group col-md-2">
            <label for="category" class="control-label">&emsp;<sup> </sup></label>
             <select class="form-control input-sm validate[required]" tabindex="12" style="height:29px"; name="u_country" id="country"> 
              <option value="Select">--Country--</option>
              <?php foreach ($countries as $key => $countries) {
                ?>
              <option value="<?php echo $countries['id']?>"<?php if($countries['id']==$u_country) { ?> selected="selected" <?php } ?>><?php echo $countries['name']?></option>
                <?php
              }?>
            </select>
          </div>
          <div class="form-group col-md-2">
            <label for="category" class="control-label">&emsp;<sup> </sup></label>
          <select class="form-control input-sm validate[required]"  style="height:29px";  tabindex="13" name="u_state" id="state"> 
              <option value="Select">--State--</option>
              <?php foreach ($states as $key => $states) {
                ?>
              <option value="<?php echo $states['id']?>"<?php if($states['id']==$u_state) { ?> selected="selected" <?php } ?>><?php echo $states['name']?></option>
                <?php
              }?>
            </select>
          </div> 
          <div class="form-group col-md-2">
            <label for="category" class="control-label">&emsp;<sup> </sup></label>
          <select class="form-control input-sm"  style="height:29px";  tabindex="14" required="required"   name="u_city"  id="city"> 
              <option value="Select">--City--</option>
              <?php foreach ($cities as $key => $cities) {
                ?>
              <option value="<?php echo $cities['id']?>"<?php if($cities['id']==$u_city) { ?> selected="selected" <?php } ?>><?php echo $cities['name']?></option>
                <?php
              }?>
            </select>
          </div>
        </div>
           <div class="col-md-12">
          <div class="form-group col-md-2"  id="u_fees">
           <div class="form-group">
                 <label for="category" class="control-label">Consulting Fees<sup> </sup></label>
                  <input class="form-control input-sm" type="text" tabindex="15" style="height:29px";  placeholder="Consulting Fees" name="u_fees" value="<?= $u_fees ?>">        
              </div>
          </div>
          <div class="col-md-2">
           <div class="form-group">
                  <label for="category" class="control-label">Basic Salary<sup> </sup></label>
                  <input class="form-control input-sm" type="text" tabindex="16" style="height:29px";  placeholder="" name="u_basic_sal" value="<?= $u_basic_sal ?>">        
              </div>
          </div>
          <div class="form-group col-md-2">
           <div class="form-group required">
               <label for="category" class="control-label">Salary/Day<sup> </sup></label>
                  <input class="form-control input-sm" tabindex="17" style="height:29px";  placeholder=" " type="text" name="u_sal_day" value="<?= $u_sal_day?>">        
              </div>
          </div>
          <div class="form-group col-md-2">
           <div class="form-group required">
                <label for="category" class="control-label">Casual/Month<sup> </sup></label>
                  <input class="form-control input-sm" type="text" tabindex="18" style="height:29px";  name="u_cas_month" placeholder=" " value="<?= $u_cas_month ?>">      
              </div>
          </div>
          
        </div>
  
           </div></div>
      </div>

      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            <div>
                        <input class="btn-large btn-primary btn" tabindex="19" type="submit" value="Submit" accesskey="s" title="short key-ALT+S"> 
                        <input class="btn-large btn-default btn" type="reset" value="Reset">
                    </div>
          </div>

        </div>
      </div>
       
  <?php echo form_close(); ?>

</section>

</section><!-- /.right-side -->


<script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
            .attr('src', e.target.result)
            .width(139)
            .height(150);
        };

        reader.readAsDataURL(input.files[0]);
        
    }
}

</script>
    
        <script>
 $('#datepicker').datepicker({
      autoclose: true
    });</script>

 

 