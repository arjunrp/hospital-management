<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {

$('#country').change(function(){
    var ctid = $('#country').val();
    $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>customer/get_state/"+ctid,
     success: function(data){
      $("#state").empty();
      $.each(data, function(index) {
        $("#state").append('<option value=' + data[index].id +'>'+data[index].name+'</option>');
      });
    }
  })
  });

$('#state').change(function(){
    var ctid = $('#state').val();
    $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>customer/get_city/"+ctid,
     success: function(data){
      $("#city").empty();
      $.each(data, function(index) {
        $("#city").append('<option value=' + data[index].id +'>'+data[index].name+'</option>');
      });
    }
  })
  });

 oTable = $('#category_table').dataTable({
});
 });
</script>



<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
     Customer
      <small>Create  Customer </small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."customer"; ?>">Customers</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">Customer
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div align="right">
         <a title="short key-ALT+B" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."customer"; ?>" accesskey="b" ><i class="fa fa-mail-reply-all"></i> Back</a>
       </div>

     </div>

     <?php echo form_open_multipart($action); ?>
     <div class="box-body">
      <div class="row">
        <div class="col-md-12">


  <?php
                        if($this->uri->segment(1)=="add")
                        $clas   =   'required="required"';
                      else
                      $clas = '';
                    ?> 
           <div class="col-md-3">
          <div class="form-group">
                        <label for="focusinput" class="control-label">Image(1600 X 400)</label>
                        
                        <?php if($image != "") { echo "<img id='blah' width='139px' height='150px' src='".$this->config->item("base_url")."uploads/customer/".$image."'>"; } else { echo "<img id='blah' width='150px' height='150px' src='".$this->config->item("base_url")."uploads/Default2.png"."'>"; } ?>
                      <input class="form-control" type="hidden" name="id" value="<?php echo $id;?>">
                        <input type="file" name="image" onchange="readURL(this);" <?php echo $clas; ?>>
                      </div>
                    </div>
           
 
 
          
            <div class="form-group col-md-3">
            <label  class="control-label">Customer<sup>*</sup></label>
            <input class="form-control validate[required,custom[onlyLetterSp]]" tabindex="1" style="width:190px;height:29px";    type="text" name="customer" value="<?=$customer  ?>">  
            <input class="form-control"   type="hidden" name="id" value="<?=$id ?>">                
          </div>    
             <div class="form-group col-md-3">
             <label  class="control-label">Phone <sup>*</sup></label>
            <input class="form-control validate[required,custom[phone]]"   tabindex="2" placeholder="Phone" style="width:190px;height:29px";    type="text" name="phone" value="<?= $phone  ?>">                               
          </div> 
           <div class="form-group col-md-3">
            <label  class="control-label">Fax</label>
            <input class="form-control"  tabindex="9" placeholder="Fax"style="width:190px;height:29px";  type="text" name="fax" value="<?= $fax  ?>">                                
          </div>


           <div class="form-group col-md-3">
            <label  class="control-label">Address</label>
            <input    type="text" name="address" tabindex="3" placeholder="Address" style="width:190px;height:29px"; class="form-control" value="<?= $address ?>">
          </div>
           <div class="form-group col-md-3">
            <label  class="control-label"> </label>
            <input    type="text"  tabindex="4" placeholder="Street 2" style="width:190px;height:29px"; name="street" class="form-control" value="<?= $street ?>">
          </div>
          <div class="form-group col-md-3">
            <label  class="control-label">Email </label>
            <input class="form-control validate[required,custom[email]]" tabindex="10" placeholder="Email" style="width:190px;height:29px";  type="text" name="email" value="<?= $email  ?>">                
          </div>




          <div class="form-group col-md-3">
             <label  class="control-label">  </label>
           <!--  <input class="form-control" tabindex="5"  placeholder="City" style="width:190px;height:29px";  type="text" name="city" value="<?= $city  ?>">                 -->
          <select class="form-control"  style="width:190px;height:29px";  tabindex="5" required="required"   name="city"  id="city"> 
              <option value="Select">--City--</option>
              <?php foreach ($cities as $key => $company) {
                ?>
              <option value="<?php echo $company['id']?>"<?php if($company['id']==$city) { ?> selected="selected" <?php } ?>><?php echo $company['name']?></option>
                <?php
              }?>
            </select>
          </div>
           <div class="form-group col-md-3">
            <label  class="control-label">  </label>
            <input class="form-control"  tabindex="6"  placeholder="Zip" style="width:190px;height:29px";  type="text" name="zip" value="<?= $zip ?>">                
          </div>
           <div class="form-group col-md-3">
            <label  class="control-label">Website </label>
            <input class="form-control" tabindex="11" placeholder="Website" style="width:190px;height:29px";  type="text" name="website" value="<?= $website  ?>">                
          </div> 

<div class="col-md-12">
 <div class="col-md-3">
  </div> 
          <div class="form-group col-md-3">
            <label  class="control-label">  </label>
            <!-- <input class="form-control" placeholder="State" tabindex="7"  style="width:190px;height:29px";  type="text" name="state" value="<?= $state  ?>">                 -->
          <select class="form-control validate[required,minSize[2],maxSize[250]]"  style="width:190px;height:29px;margin-left:-9px";  tabindex="7"     name="state" id="state"> 
              <option value="Select">--State--</option>
              <?php foreach ($states as $key => $company) {
                ?>
              <option value="<?php echo $company['id']?>"<?php if($company['id']==$state) { ?> selected="selected" <?php } ?>><?php echo $company['name']?></option>
                <?php
              }?>
            </select>
          </div> 
          <div class="form-group col-md-3">
             <label  class="control-label">  </label>
             <select class="form-control validate[required,minSize[2],maxSize[250]]"  style="width:190px;height:29px";   tabindex="8" name="country" id="country"> 
              <option value="Select">--Country--</option>
              <?php foreach ($countries as $key => $company) {
                ?>
              <option value="<?php echo $company['id']?>"<?php if($company['id']==$country) { ?> selected="selected" <?php } ?>><?php echo $company['name']?></option>
                <?php
              }?>
            </select>
          </div>
          <div class="form-group col-md-2">
           <label  class="control-label"> &emsp;Payment Type</label>
          <select class="form-control" tabindex="14"  style="width:190px;height:29px;margin-left:10px;" name="payment"> 
          <option value="" >Select Payment </option>
          <option value="Cash">Cash</option>
          <option value="Debit">Debit</option>
                         
         </select>
          </div>
</div>
    


           
            </div> 
        </div>
      </div>
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            <div>
              <input class="btn-large btn-primary btn" tabindex="12" type="submit" value="Submit" name="submit" accesskey="s" title="short key-ALT+S"> 
              <input class="btn-large btn-default btn" type="reset" value="Reset">
            </div>
          </div>

        </div>
      </div>

      <?php echo form_close(); ?>
 
      <script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
            .attr('src', e.target.result)
            .width(139)
            .height(150);
        };

        reader.readAsDataURL(input.files[0]);
        
    }
}

</script>

    </div>
   <br> <br> <br> <br>  <br> 
</section>

</div><!-- /.right-side -->


