<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

   <script src="<?= base_url() ?>asset/plugins/timepicker/bootstrap-timepicker.min.js"></script>
     <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/timepicker/bootstrap-timepicker.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
$('#bk_phone').change(function(){
      var uRL1   = "<?= base_url() ?>ipregister/get_phone/"+$(this).val();

      $.ajax({

        url : uRL1,
        dataType: "json",
        success:function(data3)
        {
           $.each(data3, function(index) {
        $("#bk_name").val(data3[index].p_title+" "+data3[index].p_name);
        $("#ip_mrd").val(data3[index].p_mrd_no);
        $("#ip_department_id").val(data3[index].dp_id);
        $("#ip_department").val(data3[index].dp_department);
        $("#ip_doctor").val(data3[index].u_name);
        $("#ip_doctor_id").val(data3[index].u_emp_id);
        $("#bk_address").val(data3[index].p_address);
        $("#bk_street").val(data3[index].p_street);
        $("#bk_age").val(data3[index].p_age);
      });
        var ip_department = $('#ip_department_id').val();
        var uRL2   = "<?= base_url() ?>ipregister/get_ipno";
        $.ajax({
        url : uRL2,
        type: "POST",
        data : {ip_dept:ip_department},
        success:function(data2, textStatus, jqXHR)
        {
          $('#ip_ipno').val(data2);
        }
        })
        }

      });

  });

  $('#ip_mrd').change(function(){
      var uRL1   = "<?= base_url() ?>ipregister/get_mrd/"+$(this).val();
      $.ajax({

        url : uRL1,
        dataType: "json",
        success:function(data3)
        {
           $.each(data3, function(index) {
        $("#bk_name").val(data3[index].p_title+" "+data3[index].p_name);
        $("#bk_phone").val(data3[index].p_phone);
        $("#ip_department_id").val(data3[index].dp_id);
        $("#ip_department").val(data3[index].dp_department);
        $("#ip_doctor").val(data3[index].u_name);
        $("#ip_doctor_id").val(data3[index].u_emp_id);
        $("#bk_address").val(data3[index].p_address);
        $("#bk_street").val(data3[index].p_street);
        $("#bk_age").val(data3[index].p_age);
      });  

        
        var ip_department = $('#ip_department_id').val();
        var uRL2   = "<?= base_url() ?>ipregister/get_ipno";
        $.ajax({
        url : uRL2,
        type: "POST",
        data : {ip_dept:ip_department},
        success:function(data2, textStatus, jqXHR)
        {
          $('#ip_ipno').val(data2);
        }
        });

        }

       

      });


  });

});
  </script>
  <script> 
$(document).ready(function() {

   $('.mrd').change(function(){
    var ip_mrd = $('#ip_mrd').val();
    var uRL1   = "<?= base_url() ?>ipregister/get_discharge";
    $.ajax({
        url : uRL1,
        type: "POST",
        data : {ip_mrd:ip_mrd},
        success:function(data3, textStatus, jqXHR)
        {
          if(data3=="0000-00-00")
          {
            alert("Patient not discharged yet");
            $('#ip_mrd').val("");
            $('#bk_name').val("");
            $('#bk_phone').val("");
          }
        }
  })

   });

  });
  </script>


  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#customer_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       IP
       <small>Create IP </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."ipregister"; ?>">IP</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/ipregister">
        <i class="fa fa-plus-circle"></i> Active IP List
      </a>
    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>

      <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row"> 
       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">IP No.<sup></sup></label>
          <input class="form-control input-sm"  type="text" id="ip_ipno" name="ip_ipno"> 
          <input class="form-control" type="hidden" name="user_type" tabindex="1" value="<?=$user_type?>"> 

        </div>
        <div class="col-md-2">
          <label  class="control-label">Date<sup></sup></label>
          <input class="form-control validate[required] input-sm"  tabindex="2" data-prompt-position="topRight:150" type="text" name="ip_date" id="datepicker" value="<?=date("Y-m-d") ?>">                
        </div>
        <div class="bootstrap-timepicker">
                <div class="form-group col-md-2">
                  <label>Time:</label>
                  <div class="input-group">
                    <input type="text" name="ip_time" class="form-control timepicker">
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              </div>
          <div class="col-md-2">
         <label  class="control-label">MRD<sup></sup></label>
         <input class="mrd form-control validate[required] input-sm" data-prompt-position="bottomLeft:190" tabindex="5" type="text" name="ip_mrd" id="ip_mrd"></div>
       <div class="col-md-2">
         <label  class="control-label">Patient's Name<sup></sup></label>
         <input class="mrd form-control input-sm"  data-prompt-position="bottomLeft:190"  tabindex="6" type="text" id="bk_name">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Phone No.<sup></sup></label>
         <input class="mrd form-control text-input input-sm" data-prompt-position="bottomLeft:190" tabindex="7" type="text" id="bk_phone" name="p_phone">
       </div>

      </div>
      <div class="col-md-12">
       <div class="col-md-2">
         <label  class="control-label">Address<sup></sup></label>
         <input class="form-control input-sm" type="text" id="bk_address" name="p_address">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Street<sup></sup></label>
         <input class="form-control input-sm" type="text" id="bk_street" name="p_street">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Age<sup></sup></label>
         <input class="form-control input-sm" type="text" name="p_age" id="bk_age">
       </div>
         <div class="col-md-2">
         <label  class="control-label">Department<sup></sup></label>
         <input class="form-control input-sm" readonly   data-prompt-position="bottomLeft:190" tabindex="7" type="text" id="ip_department">
         <input type="hidden" name="ip_department" id="ip_department_id"> 
       </div>
        <div class="col-md-2">
         <label  class="control-label">Doctor<sup></sup></label>
         <input class="form-control input-sm" readonly   data-prompt-position="bottomLeft:190" tabindex="7" type="text" id="ip_doctor">
         <input type="hidden" name="ip_doctor" id="ip_doctor_id"> 
       </div>
     </div>
          </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-md-12">
                <div>
                  <button class="button btn btn-primary" tabindex="8" type="submit">Add to IP Register</button>
                  <input class="btn-large btn-default btn"  tabindex="9" type="reset" value="Reset">
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php echo form_close(); ?>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover gridView" id="category_table">
                    <thead>
                      <tr>
                        <th>IP No.</th>
                        <th>Doctor</th>
                        <th>Department</th>
                        <th>MRD</th>
                        <th>Patient's Name</th>
                        <th>Phone No.</th>
                        <th>Room</th>
                        <th>Options</th>
                      </tr>
                    </thead>
                  <tbody> 
                    <?php if($output) { echo $output; } ?>
                  </tbody>
                  </table> 
                

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>

</section><!-- /.right-side -->


<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });

$('.timepicker').timepicker({
      showInputs: false,
      minuteStep: 1,
    });

 </script>
<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>