<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

   <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
   <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->


   <script type="text/javascript" charset="utf-8">
  $(function()
  {
    $("#product").autocomplete({
      source: [<?php
      $i=0;
      $unit="";
      foreach ($products as $product){
        if ($i>0) {echo ",";}
        echo '{value:"' .$product['ved_item']." - ". $product['ved_batch']  . '",productname:"' . $product['ved_item'] . '",pid:"' . $product['ved_itemid'] . '",productunit:"' .$product['ved_unit']. '",productbatch:"' . $product['ved_batch'] . '",productexpiry:"' . $product['ved_expiry'] . '",productstock:"' . $product['stock_qty'] / $product['ved_uqty'] . '",productuqty:"' . $product['stock_qty'] . '",ved_uqty:"' . $product['ved_uqty'] . '"}';
                  $i++;
                }
                ?>
                ],
                minLength: 1,//search after one characters
                select: function(event,ui){

                  $("#productid").val(ui.item ? ui.item.pid : '');
                  $("#product_name").val(ui.item ? ui.item.productname : '');
                  $("#product_uqty").val(ui.item ? ui.item.productuqty : '');
                  $("#product_unit").val(ui.item ? ui.item.productunit : '');
                  $("#batch").val(ui.item ? ui.item.productbatch : '');
                  $("#expiry").val(ui.item ? ui.item.productexpiry : '');
                  $("#product_stock").val(ui.item ? ui.item.productstock : '');
                  $("#product_ustock").val(ui.item ? ui.item.productstock : '');
                  $("#ved_uqty").val(ui.item ? ui.item.ved_uqty : '');
        }
      });   
  });  
  </script>
  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#purchase_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>

<!-- Product Delete -->
<script>
  $(document).ready(function() {
    setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);

    <?php
$js_array = json_encode($consumes);
echo "var consume = ". $js_array . ";\n";
?>

$.each(consume, function(index) {


         var newrow      = '<tr><td><input type="hidden" value="'+consume[index].ved_itemid+'" name="ved_itemid[]"><input class="form-control input-sm" type="hidden" value="'+consume[index].ved_item+'" name="ved_item[]">' + consume[index].ved_item + '</td><td><input class="form-control input-sm" type="hidden" name="ved_batch[]" value="'+consume[index].ved_batch+'">'+consume[index].ved_batch+'</td><td><input class="form-control input-sm" type="hidden" placeholder="mm/yy" name="ved_expiry[]" value="'+consume[index].ved_expiry+'">'+consume[index].ved_expiry+'</td><td width="5%"><input class="form-control input-sm" type="input" value="'+consume[index].ved_qty/consume[index].ved_uqty+'" name="ved_qty[]"><input class="form-control input-sm" type="hidden" value="'+consume[index].ved_uqty+'" name="ved_uqty[]"></td><td width="20%"><input class="form-control input-sm" type="hidden" value="'+consume[index].ved_unit+'" name="ved_unit[]">'+consume[index].ved_unit+'</td><td width="5%"><button class="btn btn-xs btn-danger dlte"><i class="fa fa-times-circle-o"></i></button></td></tr>';
        $('#item_table tr:last').after(newrow);
      });


  $(document).on('click','button.dlte', function() {
    var $row    = $(this).closest("tr");    // Find the row

    var r = confirm("Are you sure want to delete!");
    if (r == true) {
    $(this).closest('tr').remove();
    } 
    });
  });
  </script>
<!-- Product Add -->
  <script>
  $(document).ready(function() {
  $('.button').click(function() {
    var item        = $('#product_name').val();
    var itemid      = $('#productid').val();
    var qty         = $('#product_quantity').val();
    var pd_qty      = $('#ved_uqty').val();
    var unt         = $('#ved_unit').val();
    var batch       = $('#batch').val();
    var expiry      = $('#expiry').val();

    var newrow      = '<tr><td><input type="hidden" value="'+itemid+'" name="ved_itemid[]"><input class="form-control input-sm" type="hidden" value="'+item+'" name="ved_item[]">' + item + '</td><td><input class="form-control input-sm" type="hidden" value="'+batch+'" name="ved_batch[]">'+batch+'</td><td><input type="hidden" value="'+expiry+'" name="ved_expiry[]">'+expiry+'</td><td><input type="hidden" value="'+qty+'" name="ved_qty[]">'+qty+'<input type="hidden" value="'+pd_qty+'" name="ved_uqty[]"></td><td><input type="hidden" value="'+unt+'" name="ved_unit[]">'+unt+'</td><td><button class="btn btn-xs btn-danger dlte"><i class="fa fa-times-circle-o"></i></button></td></tr>';

    $('#item_table tr:last').after(newrow);

    document.getElementById('product').value = "";
    document.getElementById('productid').value = "";
    document.getElementById('product_quantity').value = "";
    document.getElementById('product_stock').value = "";
    document.getElementById('product_unit').value = "";
    document.getElementById('batch').value = "";
    document.getElementById('expiry').value = "";
});
  });
  </script>
  <script>
  $(document).ready(function() {

    $('#ved_unit').focus(function(){
    var product_id = $('#productid').val();
    var unit_name = "";
     $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>sales/getProduct_unit/"+product_id,
     success: function(data){
      $('#ved_unit').empty();
      $("#ved_unit").append('<option value="0">--Select--</option>');
      $.each(data, function(index) {
        if(data[index].pd_unit=="Str")       { unit_name = "Strip"; }
        else if(data[index].pd_unit=="Bot")  { unit_name = "Bottle"; }
        else if(data[index].pd_unit=="Pkt")  { unit_name = "Packet"; }
        else if(data[index].pd_unit=="No.s") { unit_name = "No.s"; }
        $("#ved_unit").append('<option value=' + data[index].pd_unit +'>--'+unit_name+'--</option>');
          });
          if(unit_name != "No.s") {
            $("#ved_unit").append('<option value="No.s">--No.s--</option>');
          }
        }
      })
     });


  $('#ved_unit').change(function(){
      var ved_unit        = $('#ved_unit').val();
      $('#product_quantity').val("");
      if(ved_unit=="No.s")
      {
        var pd_stock  = $('#product_uqty').val();
      }
      else
      {
        var pd_stock   = $('#product_ustock').val();
      }
      $('#product_stock').val(pd_stock);
    });

    $('#product_quantity').change(function() {
    var quantity   =  $('#product_quantity').val();
    var stock      =  $('#product_stock').val();
    if(parseFloat(quantity)>parseFloat(stock))
    {
      alert("Insufficent Stock");
      $('#product_quantity').val("");
      $('#product_quantity').focus();
    }

  });
  });
  </script>

  <?php
foreach($consumes as $consume)
{
  $ve_id          = $consume['ve_id'];
  $ve_vno         = $consume['ve_vno'];
  $ve_date        = $consume['ve_date'];
  $dp_department  = $consume['dp_department'];
  $u_emp_id       = $consume['u_emp_id'];
}

?>


  <section class="right-side" style="min-height:700px;"> <?php 
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       Consumption
       <small>Edit </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."consume"; ?>">Consumption</a></li>
      <li class="active"> <?=$page_title ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?= $page_title?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/consume/add">
        <i class="fa fa-plus-circle"></i> Create New
      </a>
    </div>
     </div><br>
      <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success"> 
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>

      <form id="post_form">
     <div class="box-body">
        
      <div class="row">                

       <div class="col-md-12">
        <div class="form-group col-md-2">
            <label  class="control-label">Consume No. :</label> <?= $ve_vno  ?><br>
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Date :</label> <?= date("d-m-Y",strtotime($ve_date))  ?><br>
            <input type ="hidden" name="ve_date" value="<?=$ve_date?>">
          </div>
          <div class="form-group col-md-3"></div>

          <div class="form-group col-md-3">
            <label  class="control-label">Department :</label> <?= $dp_department  ?> <br>         
          </div>
          
          <div class="form-group col-md-2">    
          <label class="control-label">User Name : </label> <?= $u_emp_id ?>
          <input class="form-control"  type="hidden" name="user_type" value="<?= $user_type;?>">
          </div>


     </div>

     <div class="col-md-12">
      <br>
      <div class="col-md-4">
        <div class="form-group required">
          <label  class="control-label">Item<sup></sup></label>
          <input class="form-control input-sm" type="text" id="product">
          <input class="form-control" type="hidden" id="product_name">
          <input class="form-control" type="hidden" name="productid" id="productid">
          <input class="form-control" type="hidden" id="product_uqty">
          <input class="form-control" type="hidden" id="ved_uqty">
        </div> </div>

          <div class="col-md-1">
          <div class="form-group required">
            <label  class="control-label">Stock<sup></sup></label>
          <input class="form-control input-sm" readonly  type="text" id="product_stock">   
          <input class="form-control" readonly  type="hidden" id="product_ustock"> 
             </div> </div>

            <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Batch<sup></sup></label>
              <input class="form-control input-sm" type="text" id="batch">                
            </div> </div>
            <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Expiry<sup></sup></label>
              <input class="form-control input-sm" type="text" id="expiry">                
            </div> </div>

          <div class="col-md-2">
          <div class="form-group required">
            <label  class="control-label">Unit<sup></sup></label>
              <select class="form-control input-sm" id="ved_unit" name="ved_unit">
                <option value="0">--Select--</option>
              </select>
          </div> </div>

        
        <div class="col-md-1">
          <div class="form-group required">
            <label  class="control-label">Quantity<sup></sup></label>
            <input class="form-control input-sm validate[required]"  data-prompt-position="topRight:150" id="product_quantity" >                
          </div> </div>

            </div>
          </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-md-12">
                <div>
                  <a class="button btn btn-primary" >Add to Table</a>
                  <!-- <input class="btn-large btn-primary btn" type="submit" value="Submit" name="submit">  -->
                  <input class="btn-large btn-default btn" id="reset" type="reset" value="Reset">
                </div>
              </div>

            </div>
          </div>
          <?php //echo form_close(); ?>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">

                  <table class="table table-striped table-bordered table-hover gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Product</th>
                        <th>Batch</th>
                        <th>Expiry</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                  <tbody> 
                  </tbody>
                  </table> 

   <button class="btn-large btn-success btn" type="submit" onclick="save();" name="submit1" accesskey="s" title="short key-ALT+S"><i class='fa fa-floppy-o'></i> Save</button>
   <input type="hidden" id="Printid" name="ve_id" value="<?=$ve_id?>">

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
</div>
</section>

</section><!-- /.right-side -->

<script>

function save(){

      var postData = $("#post_form").serializeArray();
      var ve_id    =  $('#Printid').val();
      var formURL  = "<?= base_url() ?>consume/consumeUpdate/"+ve_id ;

      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          $('#Printid').val(data);
          window.location = "<?php echo base_url();?>index.php/consume";
          <?php 
          $this->session->set_flashdata('Success','Consumption Updated'); ?>
        }
      });
}

    </script>

<script>
 $('.datepicker').datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true
    });</script>