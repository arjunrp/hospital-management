<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
	</main><!-- #site-content -->

	<footer id="site-footer" role="contentinfo" class="footer-data">
		Designed and developed by <a href="http://www.rannsinfo.com/" target="_blank"> Ranns Info Services</a>
	</footer><!-- #site-footer -->

	<!-- js -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="<?php echo $this->config->item('base_url');?>assets/admin/login/js/bootstrap.min.js"></script>
	<script src="<?php echo $this->config->item('base_url');?>assets/admin/login/js/script.js"></script>

</body>
</html>