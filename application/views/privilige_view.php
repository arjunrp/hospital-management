
<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
     Customer
      <small>View  Customer </small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."priviliges"; ?>">Users</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?=$customer  ?>
       </h3>

       <div align="right">
        <a title="Back" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."priviliges"; ?>"><i class="fa fa-mail-reply-all"></i> Back</a>
        </div>
     </div>


     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">
          <div class="form-group col-md-2">
          <label for="focusinput" class="control-label">Image</label><br>
         <?php if($image != "") { echo "<img width='150px' height='150px' src='".$this->config->item("base_url")."uploads/customer/".$image."'>"; } else { echo "<img width='150px' height='150px' src='".$this->config->item("base_url")."uploads/Default.png"."'>"; } ?>
          </div>

          <div class="form-group col-md-3">
            <label  class="control-label">Address(street1)<sup>*</sup></label><br>
            <?= $address ?>
          </div>


          <div class="form-group col-md-3">
            <label  class="control-label">Street<sup>*</sup></label><br>
            <?= $street ?>             
          </div>

          <div class="form-group col-md-2">
            <label  class="control-label">City<sup>*</sup></label><br>
            <?= $city  ?>
          </div>


          <div class="form-group col-md-2">
            <label  class="control-label">State<sup>*</sup></label><br>
            <?= $state  ?>               
          </div>


          <div class="form-group col-md-3">
            <label  class="control-label">Country<sup>*</sup></label><br>
            <?= $country ?> 
            </div>

           <div class="form-group col-md-3">
            <label  class="control-label">Zip<sup>*</sup></label><br>
            <?= $zip ?>
          </div>

          <div class="form-group col-md-3">
             <label  class="control-label">Phone<sup>*</sup></label><br>
            <?= $phone  ?>                 
          </div> 
           
           <div class="form-group col-md-3">
            <label  class="control-label">Fax<sup>*</sup></label><br>
            <?= $fax  ?>          
          </div>
          <div class="form-group col-md-3">
            <label  class="control-label">Email<sup>*</sup></label><br>
            <?= $email  ?>             
          </div> 

           <div class="form-group col-md-3">        
          <label  class="control-label">Website<sup>*</sup></label><br>
          <?= $website  ?>                
          </div>

        </div>
      </div>
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            <!-- <div class="box-tools">
              <a title="Back" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."customer"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div> -->
          </div>

        </div>
      </div>
      
    </div>
  </div>
</section>

</section><!-- /.right-side -->


