<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>


  <?php
foreach($stransfers as $stransfer)
{
  $ve_id          = $stransfer['ve_id'];
  $ve_vno         = $stransfer['ve_vno'];
  $ve_pono        = $stransfer['ve_pono'];
  $ve_date        = $stransfer['ve_date'];
  $ve_supplier    = $stransfer['dp_1'];
  $ve_customer    = $stransfer['dp_2'];
  // $user_name      = $stransfer['u_emp_id'];

}

?>

<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
     Stock Transfer
      <small>View </small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."stocktransfer"; ?>">Stock Transfer</a></li>
      <li class="active"></li> <?=$page_title?>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?=$page_title?>
       </h3>
        <div class="box-tools">
              <a title="Back" class="btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."stocktransfer"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>
     </div>

     <form id="post_form">
     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">
           
            <div class="form-group col-md-2">
            <label  class="control-label">Voucher No. :</label> <?= $ve_vno  ?><br>
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Date :</label> <?= date("d-m-Y",strtotime($ve_date))  ?><br>
            <input type ="hidden" name="ve_date" value="<?=$ve_date?>">
          </div>

          <div class="form-group col-md-3">
            <label  class="control-label">Transfered From :</label> <?= $ve_supplier  ?> <br>         
          </div>

          <div class="form-group col-md-3">
            <label  class="control-label">Transfered To :</label> <?= $ve_customer  ?><br>
          </div>
          
          <!-- <div class="form-group col-md-2">    
          <label class="control-label">User Name : </label> <?= $user_name ?>
          </div> -->


 <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">

               
                   <table class="table table-condensed dataTable no-footer gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Sl. No </th>
                        <th>Product</th>
                        <th>Batch</th>
                        <th>Expiry</th>
                        <th>Quantity</th>
                      </tr>
                    </thead>
                  <tbody> 
                    <?php
                    $slno=1;
                    foreach($stransfers as $stransfer)
                      { ?> 
                    <tr>
                      <td><?= $slno++  ?></td>
                      <td><a href="<?= $this->config->item('admin_url')."product/view/".$stransfer['ved_itemid'] ?>"><?=$stransfer['ved_item'] ?></a></td>
                      <td><?=$stransfer['ved_batch'] ?></td>
                      <td><?=$stransfer['ved_expiry'] ?></td>
                      <td><?=$stransfer['ved_qty']/$stransfer['ved_uqty']." ".$stransfer['ved_unit']  ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                  </table> 

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
           

        </div>
      </div>
      
      
    </div>
  </form><br>
  </div>
</section>

</section><!-- /.right-side -->


