
<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
 
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {

$('#bk_phone').change(function(){
      var uRL1   = "<?= base_url() ?>booking/get_phone/"+$(this).val();
      $.ajax({

        url : uRL1,
        dataType: "json",
        success:function(data3)
        {
          $('#bk_name').empty();
          $("#bk_name").append('<option value="0">---Select Patient---</option>');
           $.each(data3, function(index) {
            $("#bk_name").append('<option value=' + data3[index].p_mrd_no +'>'+data3[index].p_title+" "+data3[index].p_name+'</option>');
      });
        }

      });

  });

$('#bk_name').change(function(){
      var uRL1   = "<?= base_url() ?>booking/get_mrd/"+$(this).val();
      $.ajax({

        url : uRL1,
        dataType: "json",
        success:function(data3)
        {

           $.each(data3, function(index) {
            $("#mrd").val(data3[index].p_mrd_no);
      });
        }

      });

  });

 oTable = $('#category_table').dataTable({
});
});
</script>

<div class="content-wrapper">
  <section class="content-header">
    <h1>
     Patient
      <small>Search Patient</small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."patient/add"; ?>">Patient</a></li>
      <li class="active">Search Patient</li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> Patient Search
       </h3>
        <div class="box-tools">
              <a title="Back" class="btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."patient/add"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>
     </div>


      
     <div class="box-body">
          <div class="row">

          <div class="col-md-12">
             <div class="col-md-2">
              <label  class="control-label">Patient Ph.<sup></sup></label>
              <input  class="form-control validate[required] input-sm" data-prompt-position="bottomRight:100" type="text" id="bk_phone">             
             </div>
             <div class="col-md-3">
              <label  class="control-label">Patient Name<sup></sup></label>
             <select id="bk_name" class="form-control input-sm">
              <option value="0">---Select Patient---</option>
             </select>          
             </div>
             
            <div class="col-md-2">
              <label  class="control-label">MRD<sup></sup></label>
              <input  class="form-control validate[required] input-sm" data-prompt-position="bottomRight:100" type="text" id="mrd">  
             </div>
             <div class="col-md-2">
              <label  class="control-label">&emsp;<sup></sup></label><br>
              <button class="btn btn-primary btn-sm" onclick="view();">View</button>
             </div>
           </div>

            
            
          
  </div>
</div>
           
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            
          </div>

        </div>
      </div>
             </div>
   
</section>

</div><!-- /.right-side -->

<script>
function view(){
  var mrd = $('#mrd').val();
  
  if(mrd != 0)
  {
    window.location = "<?php echo base_url();?>index.php/patient/view/"+mrd;
  }

}
</script>



