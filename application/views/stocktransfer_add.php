<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

   <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
   <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->


  <script type="text/javascript" charset="utf-8">
  $(function()
  {

    $('#ve_supplier').change(function(){
     var ve_supplier =  $('#ve_supplier').val()
      var formURLs  = "<?= base_url() ?>stocktransfer/productGetbydept";
      $.ajax(
      {
        url : formURLs,
        type: "POST",
        data: {ve_supplier:ve_supplier},
        success:function(valdata, textStatus, jqXHR)
        {   

            $('#autoh').html(valdata);
        } 

      });

    });
   
  });  
  </script>
  <script type="text/javascript" charset="utf-8">

  $(function() {
    $( "#purchase_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>

<!-- Product Delete -->
<script>
  $(document).ready(function() {
    setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);

  $(document).on('click','button.dlte', function() {
    var $row    = $(this).closest("tr");    // Find the row


    var r = confirm("Are you sure want to delete!");
    if (r == true) {
    $(this).closest('tr').remove();
    } 
    });
  });
  </script>
<!-- Product Add -->
  <script>
  $(document).ready(function() {
  $('.button').click(function() {
    var item        = $('#product_name').val();
    var itemid      = $('#productid').val();
    var qty         = $('#product_quantity').val();
    var uqty        = $('#product_uqty').val();
    var unt         = $('#product_unit').val();
    var batch       = $('#batch').val();
    var expiry      = $('#expiry').val();
    stock           = $('#product_stock').val();

        if(parseInt(qty)>parseInt(stock))
        {
          alert("Insufficent Stock"); 
         $('#product_quantity').val("");
        }
        else{

    var newrow      = '<tr><td><input type="hidden" value="'+itemid+'" name="st_product_id[]"><input class="form-control input-sm" type="hidden" value="'+item+'" name="st_item[]">' + item + '</td><td><input class="form-control input-sm" type="hidden" value="'+batch+'" name="st_batch[]">'+batch+'</td><td><input type="hidden" value="'+expiry+'" name="st_expiry[]">'+expiry+'</td><td><input type="hidden" value="'+qty+'" name="st_qty[]">'+qty+'<input type="hidden" value="'+uqty+'"name="ved_uqty[]"></td><td><input type="hidden" value="'+unt+'" name="st_unt[]">'+unt+'</td><td><button class="btn btn-xs btn-danger dlte"><i class="fa fa-times-circle-o"></i></button></td></tr>';

    $('#item_table tr:last').after(newrow);

    document.getElementById('product').value = "";
    document.getElementById('productid').value = "";
    document.getElementById('product_quantity').value = "";
    document.getElementById('product_uqty').value = "";
    document.getElementById('product_unit').value = "";
    document.getElementById('batch').value = "";
    document.getElementById('expiry').value = "";
    document.getElementById('product_stock').value = "";
  }
});
  });
  </script>
<!-- Discount Add -->
  <script>
  $(document).ready(function() { 
    $('#product_quantity').change(function() {
        quantity    = $('#product_quantity').val();
        stock       = $('#product_stock').val();

        if(parseInt(quantity)>parseInt(stock))
        {
          alert("Insufficent Stock"); 
          $('#product_quantity').val("");
          $('#product_quantity').focus();
        }

    });

  });
  </script>

    <!-- Amount Paid -->
  <script>
  $(document).ready(function() {
    // $('#reset').click(function() {
    //   window.location="<?= base_url() ?>purchase/add"
    // });

  });
  </script>



  <section class="right-side" style="min-height:700px;"> <?php 
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       Stock Transfer
       <small>New Stock Transfer </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."stocktransfer"; ?>">Stock Transfer</a></li>
      <li class="active"> <?=$page_title ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?= $page_title?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/stocktransfer/add">
        <i class="fa fa-plus-circle"></i> Create New
      </a>
    </div>
     </div><br>
      <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success"> 
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>

     <?php //echo form_open_multipart('purchase') ?>
      <form id="post_form">
     <div class="box-body">
        
      <div class="row">                

       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">Voucher No.<sup></sup></label>
          <input class="form-control input-sm" readonly  type="text"  tabindex="1" name="ve_vno" value="<?php echo $stransfer_ID;?>">             
          <input class="form-control"  type="hidden" name="user_type" value="<?= $user_type;?>">
        </div>
        <div class="col-md-2">
          <label  class="control-label">Date<sup></sup></label>
          <input  class="form-control input-sm"  tabindex="2" readonly data-prompt-position="topRight:150"  type="text" name="ve_date" value="<?=date("Y-m-d") ?>">                
        </div>

        <div class="col-md-2"></div>

        
       <div class="col-md-3">
         <label  class="control-label">Transfer From<sup></sup></label>
         <select class="form-control input-sm"  tabindex="3" id="ve_supplier" name="ve_supplier">
          <option value="">--Select--</option>
          <?php foreach($departments as $department) { ?>
          <option value="<?=$department['dp_id']?>">--<?=$department['dp_department']?>--</option>
          <?php } ?>
         </select>       
       </div>

       <div class="col-md-3">
         <label  class="control-label">Transfer To<sup></sup></label>
         <select class="form-control input-sm" tabindex="4" name="ve_customer">
          <option value="">--Select--</option>
          <?php foreach($departments as $department) { ?>
          <option value="<?=$department['dp_id']?>">--<?=$department['dp_department']?>--</option>
          <?php } ?>
         </select>   
       </div>



     </div>

     <div class="col-md-12">
      <br>
      <div class="col-md-4">
        <div class="form-group required">
          <label  class="control-label">Item<sup></sup></label>
         <span id="autoh"></span>
        </div> </div>
        
        <div class="col-md-1">
          <div class="form-group required">
            <label  class="control-label">Quantity<sup></sup></label>
            <input class="form-control input-sm validate[required]" tabindex="5" data-prompt-position="topRight:150" id="product_quantity" >                
          </div> </div>

          <div class="col-md-2">
          <div class="form-group required">
            <label  class="control-label">Unit<sup></sup></label>
            <input class="form-control input-sm" readonly  type="text" tabindex="6" id="product_unit"  name="product_unit">                
          </div> </div>

          <div class="col-md-1">
          <div class="form-group required">
            <label  class="control-label">Stock<sup></sup></label>
            <input class="form-control input-sm" readonly  data-prompt-position="topRight:150"  id="product_stock">                
          </div> </div>

            <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Batch<sup></sup></label>
              <input class="form-control input-sm" readonly type="text" id="batch">                
            </div> </div>
            <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Expiry<sup></sup></label>
              <input class="form-control input-sm" readonly type="text" id="expiry">                
            </div> </div>



            </div>
          </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-md-12">
                <div>
                  <a class="button btn btn-primary" tabindex="7" >Add to Table</a>
                  <!-- <input class="btn-large btn-primary btn" type="submit" value="Submit" name="submit">  -->
                  <input class="btn-large btn-default btn" id="reset" tabindex="8" type="reset" value="Reset">
                </div>
              </div>

            </div>
          </div>
          <?php //echo form_close(); ?>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Product</th>
                        <th>Batch</th>
                        <th>Expiry</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                  <tbody> 
                  </tbody>
                  </table> 
                
                  
   <button class="btn-large btn-success btn" type="submit" onclick="save();" name="submit1" accesskey="s" title="short key-ALT+S"><i class='fa fa-floppy-o'></i> Save</button>

   <input type="hidden" id="Printid">

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
</div>
</section>

</section><!-- /.right-side -->

<script>

function save(){

      var postData = $("#post_form").serializeArray();
      var formURL  = "<?= base_url() ?>stocktransfer/stockAdd";
      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          $('#Printid').val(data);
          window.location = "<?php echo base_url();?>index.php/stocktransfer/add";
          <?php 
          $this->session->set_flashdata('Success','Stock Transfered'); ?>
        }
      });
}

    </script>

<script>
 $('.datepicker').datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true
    });</script>