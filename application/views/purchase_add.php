<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="<?= base_url() ?>/confirms/bootpopup.js"></script>
   <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
   <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
  <script type="text/javascript" charset="utf-8">
  $(function()
  {
    $("#po_pono").autocomplete({
      source: [<?php
      $i=0;
      foreach ($porders as $porder){
        if ($i>0) {echo ",";}
        echo '{value:"' . $porder['po_pono'] . '",vid:"' . $porder['sp_id'] . '",vendorr:"' . $porder['sp_vendor'] . '",vendorph:"' . $porder['sp_phone'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          $("#vendorid").val(ui.item ? ui.item.vid : '');
          $("#vendor").val(ui.item ? ui.item.vendorr : '');
          $("#vendorph").val(ui.item ? ui.item.vendorph : '');
        }
      });   
  });  

  

  </script>


  <script type="text/javascript" charset="utf-8">
  $(function()
  {
    $("#product").autocomplete({
      source: [<?php
      $i=0;
      $unit="";
      foreach ($products as $product){


        if ($i>0) {echo ",";}
        echo '{value:"' .$product['pd_product'] . '",pid:"' . $product['pd_code'] . '",productname:"' . $product['pd_product'] . '",productunit:"' .  $product['pd_unit'] . '",productsgst:"' . $product['pd_sgst'] . '",productcgst:"' . $product['pd_cgst'] . '",productuqty:"' . $product['pd_qty'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){

          $("#productid").val(ui.item ? ui.item.pid : '');
          $("#product_name").val(ui.item ? ui.item.productname : '');
          $("#product_unit").val(ui.item ? ui.item.productunit : '');
          $("#product_uqty").val(ui.item ? ui.item.productuqty : '');
          $("#pod_sgst").val(ui.item ? ui.item.productsgst : '');
          $("#pod_cgst").val(ui.item ? ui.item.productcgst : '');
        }
      });   
  });  
  </script>
  <script type="text/javascript" charset="utf-8">
  function calcu(p)
  {
    var grand = 0;
    var sgrand = 0;
    var sgsta1 = 0;
    var cgsta1 = 0;
    var pp = parseFloat($('#pod_price'+p).val());
    var qty = parseFloat($('#pod_qty'+p).val());
    var sgstp = parseFloat($('#pod_sgstp'+p).val());
    var cgstp = parseFloat($('#pod_cgstp'+p).val());
    var total = pp * qty;

    var sgsta       = (parseFloat(total) * parseFloat(sgstp))/100;
    var cgsta       = (parseFloat(total) * parseFloat(cgstp))/100;
    var pod_total   = parseFloat(total) + parseFloat(sgsta) + parseFloat(cgsta);
    // alert(sgstp+","+cgstp+","+sgsta+","+cgsta+","+pod_total);

    $('#pod_sgsta'+p).val((Math.round(sgsta * 100) / 100).toFixed(2));
    $('#pod_1sgsta'+p).html((Math.round(sgsta * 100) / 100).toFixed(2));
    $('#pod_cgsta'+p).val((Math.round(cgsta * 100) / 100).toFixed(2));
    $('#pod_1cgsta'+p).html((Math.round(cgsta * 100) / 100).toFixed(2));
    $('#pod_total'+p).val((Math.round(pod_total * 100) / 100).toFixed(2));
    $('#ftotal1'+p).html((Math.round(pod_total * 100) / 100).toFixed(2));
    $('#pod_stotal'+p).val((Math.round(total * 100) / 100).toFixed(2));

    $('.ftotal').each(function(){
        grand += parseFloat(this.value);
    });
    $('.stotal').each(function(){
        sgrand += parseFloat(this.value);
    });
    $('.cgsta').each(function(){
        cgsta1 += parseFloat(this.value);
    });
    $('.sgsta').each(function(){
        sgsta1 += parseFloat(this.value);
    });


    grand1   = (Math.round(grand * 100) / 100).toFixed(2);
    sgrand  = (Math.round(sgrand * 100) / 100).toFixed(2);
    cgsta1  = (Math.round(cgsta1 * 100) / 100).toFixed(2);
    sgsta1  = (Math.round(sgsta1 * 100) / 100).toFixed(2);

    var pDiscount   =  $('#pDiscount').val();
    var grand       =  parseFloat(grand1) -  parseFloat(pDiscount); 

    $('#discounta').val(pDiscount);
    $('#discounta1').html(pDiscount);

    gtotal = Math.round(grand);

    $('#gtotal').val(grand);
    $('#gtotal1').html(grand);
    $('#apayable').val(gtotal);
    $('#apayable1').html(gtotal);
    $('#sum1').html(sgrand);
    $('#sum').val(sgrand);
    $('#vecgst1').html(cgsta1);
    $('#vecgst').val(cgsta1);
    $('#vesgst1').html(sgsta1);
    $('#vesgst').val(sgsta1);

    var roundoff    =   Math.round(grand) - parseFloat(grand);
    roundoff        =   (Math.round(roundoff * 100) / 100).toFixed(2);
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);

  }

  $(function() {
    $( "#purchase_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>

<!-- Product Delete -->
<script>
  $(document).ready(function() {
    setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);

     $('input[type="checkbox"]'). click(function(){
     if($(this). prop("checked") == true){
     $("#delivery_type").val("Full");
      }
      else if($(this). prop("checked") == false){
      $("#delivery_type").val("Part");
      } 
      });

    $('#po_pono').change(function() {
    var po_pono  = $('#po_pono').val();
    $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>purchase/get_porders/"+po_pono,
     success: function(data){
      $('#item_table').find("tr:gt(0)").remove();
      var count = 1;
      $.each(data, function(index) {
        if(data[index].po_avail == "nfd")
        {
          pod_offer_qty = "0";
        }
        else
        {
          pod_offer_qty = data[index].pod_offer_qty;
        }

        qty = data[index].pod_qty - data[index].pod_purqty;
        sum       = parseFloat(data[index].po_total);
        vesgst    = parseFloat(data[index].po_sgst);
        vecgst    = parseFloat(data[index].po_cgst);
        apayable  = parseFloat(data[index].po_amount);
        price     = parseFloat(data[index].pod_price) * parseFloat(data[index].pod_qty);

         var newrow      = '<tr><td width="20%"><input type="hidden" value="'+data[index].pod_item_id+'" name="ved_itemid[]"><input class="form-control input-sm" type="hidden" value="'+data[index].pod_item+'" name="ved_item[]">' + data[index].pod_item + '</td><td width="8%"><input class="form-control input-sm" type="input" name="ved_batch[]"></td><td width="10%"><input class="form-control input-sm" type="input" placeholder="yyyy-mm-dd" name="ved_expiry[]"></td><td width="8%"><input class="form-control input-sm" type="input" value="'+data[index].pod_price+'" id="pod_price'+count+'" onkeyup="calcu('+count+')" name="ved_price[]"></td><td width="8%"><input class="form-control input-sm" type="input" name="ved_slprice[]"></td><td width="6%"><input class="form-control input-sm" type="input" value="'+qty+'" id="pod_qty'+count+'" name="ved_qty[]" onkeyup="calcu('+count+')"><input type="hidden" value="'+data[index].pd_qty+'"name="ved_uqty[]"></td><td width="5%"><input class="form-control input-sm" type="hidden" value="'+pod_offer_qty+'" name="ved_free[]">'+pod_offer_qty+'</td><td width="8%"><input class="form-control input-sm" type="hidden" value="'+data[index].pod_unit+'" id="pod_unit'+count+'" name="ved_unit[]">'+data[index].pod_unit+'</td><td width="8%"><input type="hidden" value="'+data[index].pod_cgstp+'" id="pod_cgstp'+count+'" name="ved_cgstp[]"><input class="cgsta" type="hidden" value="'+data[index].pod_csgta+'" id="pod_cgsta'+count+'" name="ved_cgsta[]"><b>Rs. </b><label id="pod_1cgsta'+count+'">'+data[index].pod_csgta+'</label> ('+data[index].pod_cgstp+'%)</td><td width="8%"><input type="hidden" value="'+data[index].pod_sgstp+'" id="pod_sgstp'+count+'" name="ved_sgstp[]"><input class="sgsta" type="hidden" value="'+data[index].pod_sgsta+'" id="pod_sgsta'+count+'" name="ved_sgsta[]"><b>Rs. </b><label id="pod_1sgsta'+count+'">'+data[index].pod_sgsta+'</label> ('+data[index].pod_sgstp+'%)</td><td width="8%"><input type="hidden" value="'+data[index].pod_total+'" class="ftotal" id="pod_total'+count+'" name="ved_gtotal[]"><b>Rs. </b><label id="ftotal1'+count+'">'+data[index].pod_total+'</label><input class="stotal" type="hidden" value="'+price+'" id="pod_stotal'+count+'" name="ved_total[]"></td><td width="3%"><button class="btn btn-xs btn-danger dlte"><i class="fa fa-times-circle-o"></i></button></td></tr>';
        $('#item_table tr:last').after(newrow);
        count++;
      });
      gtotal = Math.round(apayable);
      $('#sum').val(sum);
      $('#sum1').html(sum);
      $('#vesgst').val(vesgst);
      $('#vesgst1').html(vesgst);
      $('#vecgst').val(vecgst);
      $('#vecgst1').html(vecgst);
      $('#gtotal1').html(apayable);
      $('#gtotal').val(apayable);
      $('#apayable1').html(gtotal);
      $('#apayable').val(gtotal);
      var roundoff    =   Math.round(apayable) - parseFloat(apayable);
      roundoff        =   (Math.round(roundoff * 100) / 100).toFixed(2);
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);
    }
  })

    });

  $(document).on('click','button.dlte', function() {
    var $row    = $(this).closest("tr");    // Find the row


    var r = confirm("Are you sure want to delete!");
    if (r == true) {
    $(this).closest('tr').remove();

    var grand = 0;
    var sgrand = 0;
    var sgsta1 = 0;
    var cgsta1 = 0;
    var gtotal = 0;
    $('.ftotal').each(function(){
        grand += parseFloat(this.value);
    });
    $('.stotal').each(function(){
        sgrand += parseFloat(this.value);
    });
    $('.cgsta').each(function(){
        cgsta1 += parseFloat(this.value);
    });
    $('.sgsta').each(function(){
        sgsta1 += parseFloat(this.value);
    });
    grand           =   (Math.round(grand * 100) / 100).toFixed(2);
    sgrand          =   (Math.round(sgrand * 100) / 100).toFixed(2);
    cgsta1          =   (Math.round(cgsta1 * 100) / 100).toFixed(2);
    sgsta1          =   (Math.round(sgsta1 * 100) / 100).toFixed(2);

    var pDiscount   =  $('#pDiscount').val(); 
    var grand       =  parseFloat(grand) -  parseFloat(pDiscount);
    grand           =  (Math.round(grand * 100) / 100).toFixed(2);

    $('#discounta').val(pDiscount);
    $('#discounta1').html(pDiscount);

    gtotal = Math.round(grand);

    $('#gtotal').val(grand);
    $('#gtotal1').html(grand);

    $('#apayable').val(gtotal);
    $('#apayable1').html(gtotal);
    $('#sum1').html(sgrand);
    $('#sum').val(sgrand);
    $('#vecgst1').html(cgsta1);
    $('#vecgst').val(cgsta1);
    $('#vesgst1').html(sgsta1);
    $('#vesgst').val(sgsta1);

    var roundoff    =   Math.round(grand) - parseFloat(grand);
    roundoff        =   (Math.round(roundoff * 100) / 100).toFixed(2);
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);
    } 
    });
  });
  </script>
<!-- Product Add -->
  <script>
  $(document).ready(function() {
  $('.button').click(function() {
    var item        = $('#product_name').val();
    var itemid      = $('#productid').val();
    var qty         = $('#product_quantity').val();
    var fqty        = $('#free_quantity').val();
    var unt         = $('#product_unit').val();
    var batch       = $('#batch').val();
    var expiry      = $('#expiry').val();
    var price       = $('#product_price').val();
    var sprice      = $('#product_sprice').val();
    var total       = $('#product_quantity').val() * $('#product_price').val(); 
    var uqty        = $('#product_uqty').val();

    var sgstp       = $('#pod_sgst').val();
    var cgstp       = $('#pod_cgst').val();

    var sgsta       = (parseFloat(total) * parseFloat(sgstp))/100;
    var cgsta       = (parseFloat(total) * parseFloat(cgstp))/100;
    var pod_total   = parseFloat(total) + parseFloat(sgsta) + parseFloat(cgsta);
    
    var sgsta       = (Math.round(sgsta * 100) / 100).toFixed(2);
    var cgsta       = (Math.round(cgsta * 100) / 100).toFixed(2);
    var pod_total   = (Math.round(pod_total * 100) / 100).toFixed(2);

    // alert(unt);

    var newrow      = '<tr><td width="20%"><input type="hidden" value="'+itemid+'" name="ved_itemid[]"><input class="form-control input-sm" type="hidden" value="'+item+'" name="ved_item[]">' + item + '</td><td width="8%"><input class="form-control input-sm" type="hidden" value="'+batch+'" name="ved_batch[]">'+batch+'</td><td width="9%"><input class="form-control" value="'+expiry+'" type="hidden" name="ved_expiry[]">'+expiry+'</td><td width="7%"><input type="hidden" value="'+price+'" name="ved_price[]">'+price+'</td><td width="7%"><input type="hidden" value="'+sprice+'" name="ved_slprice[]">'+sprice+'</td><td width="4%"><input type="hidden" value="'+qty+'" name="ved_qty[]">'+qty+'<input type="hidden" value="'+uqty+'" name="ved_uqty[]"></td><td width="4%"><input class="form-control input-sm" type="hidden" value="'+fqty+'" name="ved_free[]">'+fqty+'</td><td width="7%"><input type="hidden" value="'+unt+'" name="ved_unit[]">'+unt+'</td><td width="10%"><input type="hidden" value="'+cgstp+'" name="ved_cgstp[]"><input class="cgsta" type="hidden" value="'+cgsta+'" name="ved_cgsta[]"><b>Rs. </b><label>'+cgsta+'</label> ('+cgstp+'%)</td><td width="10%"><input type="hidden" value="'+sgstp+'" name="ved_sgstp[]"><input class="sgsta" type="hidden" value="'+sgsta+'" name="ved_sgsta[]"><b>Rs. </b><label>'+sgsta+'</label> ('+sgstp+'%)</td><td width="10%"><input type="hidden" value="'+pod_total+'" class="ftotal" name="ved_gtotal[]"><b>Rs. </b><label>'+pod_total+'</label><input class="stotal" type="hidden" value="'+total+'" name="ved_total[]"></td><td width="4%"><button class="btn btn-xs btn-danger dlte"><i class="fa fa-times-circle-o"></i></button></td></tr>';

    var sum         = $('#sum').val();
    var vesgst      = $('#vesgst').val();
    var vecgst      = $('#vecgst').val();
    var pDiscount   = $('#pDiscount').val();

    sum       = parseFloat(sum)     + parseFloat(total);
    vesgst    = parseFloat(vesgst)  + parseFloat(sgsta);
    vecgst    = parseFloat(vecgst)  + parseFloat(cgsta);
    apayable  = parseFloat(sum)     + parseFloat(vesgst) + parseFloat(vecgst);

    var apayable    =   parseFloat(apayable) -  parseFloat(pDiscount);

    apayable        =   (Math.round(apayable * 100) / 100).toFixed(2);

    $('#discounta').val(pDiscount);
    $('#discounta1').html(pDiscount);

    gtotal = Math.round(apayable);

    $('#gtotal').val(apayable);
    $('#gtotal1').html(apayable);

    $('#apayable').val(gtotal);
    $('#apayable1').html(gtotal);
    $('#sum1').html(sum);
    $('#sum').val(sum);
    $('#vecgst1').html(vecgst);
    $('#vecgst').val(vecgst);
    $('#vesgst1').html(vesgst);
    $('#vesgst').val(vesgst);

    var roundoff    =   Math.round(apayable) - parseFloat(apayable);
    roundoff        =   (Math.round(roundoff * 100) / 100).toFixed(2);
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);

    $('#item_table tr:last').after(newrow);

    document.getElementById('product').value = "";
    document.getElementById('productid').value = "";
    document.getElementById('product_quantity').value = "";
    document.getElementById('product_unit').value = "";
    document.getElementById('product_price').value = "";
    document.getElementById('batch').value = "";
    document.getElementById('expiry').value = "";
    document.getElementById('pod_sgst').value = "";
    document.getElementById('pod_cgst').value = "";
});
  });
  </script>
<!-- Discount Add -->
  <script>
  $(document).ready(function() {
    // $( "#dialog" ).dialog({
    //     modal: true,
    //     bgiframe: true,
    //     autoOpen: false,
    //   show: {
    //     effect: "blind",
    //     duration: 100
    //   },
    //   hide: {
    //     effect: "explode",
    //     duration: 500
    //   }
    // });
  $('.alldisc').click(function() {
      // $( "#dialog" ).dialog( "open" );
      // var user_pswd         = $('#user_pswd').val();
      // var user_pswd         = ""+user_pswd+"";
      // bootpopup.prompt("Password","password",
      // function(data) { if(JSON.stringify(data)==JSON.stringify(user_pswd)){
      
    var grand             =  0;
    var pDiscount         =  $('#pDiscount').val();
    var sum               =  $('#sum').val();
    var vesgst            =  $('#vesgst').val();
    var vecgst            =  $('#vecgst').val();
    sum1                  =  parseFloat(sum) + parseFloat(vesgst) + parseFloat(vecgst);

    var grand             =  parseFloat(sum1) - parseFloat(pDiscount);
    grand                 =  (Math.round(grand * 100) / 100).toFixed(2);

    gtotal = Math.round(grand);

    $('#discounta').val(pDiscount);
    $('#discounta1').html(pDiscount);

    $('#gtotal').val(grand);
    $('#gtotal1').html(grand);

    $('#apayable').val(gtotal);
    $('#apayable1').html(gtotal);

    var roundoff    =   Math.round(grand) - parseFloat(grand);
    roundoff        =   (Math.round(roundoff * 100) / 100).toFixed(2);
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);

//     } 
// else
// {
//   alert("Invalid Password!");
// }
  });
  });
  </script>

    <!-- Amount Paid -->
  <script>
  $(document).ready(function() {
    // $('#reset').click(function() {
    //   window.location="<?= base_url() ?>purchase/add"
    // });
  $('#apaid').change(function() {
    var payamount   =  $('#apayable').val();
    var payamount   =  Math.round(payamount);
    var paid        =  $('#apaid').val();
    var topay       =  parseFloat(payamount) - parseFloat(paid);
    
    if(topay<0)
    {
      alert("Invalid Amount");
      // window.location="<?= base_url() ?>purchase/add";
      $('#apaid').val("");
      document.getElementById("apaid").focus();
    }
    $('#balance').val(topay);
    $('#balance1').html(topay);
  });
  });
  </script>

  <section class="right-side" style="min-height:700px;"> <?php 
    $user_type  = $this->session->id;
    $password   = $this->session->password;
    $user_dept  = $this->session->u_dept;
    ?>
    <section class="content-header">
      <h1>
       Purchase
       <small>Create Purchase </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."purchase"; ?>">Purchase</a></li>
      <li class="active"> <?=$page_title ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?= $page_title?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/purchase/add">
        <i class="fa fa-plus-circle"></i> Create New
      </a>
    </div>
     </div><br>
      <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success"> 
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>

     <?php //echo form_open_multipart('purchase') ?>
      <form id="post_form">
     <div class="box-body">
        
      <div class="row">                

       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">Voucher No.<sup></sup></label>
          <input class="form-control input-sm" readonly  type="text"  tabindex="1" name="ve_vno" value="<?php echo $purchase_ID;?>">             
          <input class="form-control"  type="hidden" name="user_type" value="<?= $user_type;?>">
        </div>
        <div class="col-md-2">
          <label  class="control-label">Date<sup></sup></label>
          <input  class="form-control datepicker input-sm" tabindex="2" data-prompt-position="topRight:150"  type="text" name="ve_date" value="<?=date("Y-m-d") ?>">                
        </div>

        <div class="col-md-2">
         <label  class="control-label">PO<sup></sup></label>
         <input class="form-control validate[required] input-sm" tabindex="3" name="po_pono" type="text" id="po_pono" value="0"> 
       </div>

       <div class="col-md-2">
         <label  class="control-label">Supplier ID<sup></sup></label>
         <input class="form-control input-sm"  tabindex="4" data-prompt-position="bottomRight:100"  type="text" name="ve_supplier" id="vendorid">     
       </div>
       <div class="col-md-2">
         <label  class="control-label">Supplier<sup></sup></label>
         <input  class="form-control" type="text" id="vendor">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Supplier Ph. No<sup></sup></label>
         <input class="form-control" type="text" id="vendorph">
       </div>


     </div>

     <div class="col-md-12">
      <br>
      <div class="col-md-3">
        <div class="form-group required">
          <label  class="control-label">Item<sup></sup></label>
          <input class="form-control input-sm"   type="text" id="product">
          <input class="form-control"   type="hidden" id="product_name">
          <input class="form-control"   required="required"  type="hidden" name="productid" id="productid"><input class="form-control"  type="hidden" id="pod_sgst">
          <input class="form-control"  type="hidden" id="pod_cgst">
          <input class="form-control"  type="hidden" id="product_uqty">
        </div> </div>
        
        <div class="col-md-1">
          <div class="form-group required">
            <label  class="control-label">Quantity<sup></sup></label>
            <input class="form-control input-sm"  data-prompt-position="topRight:150"  id="product_quantity"  name="product_quantity">                
          </div> </div>

          <div class="col-md-1">
          <div class="form-group required">
            <label  class="control-label">Free<sup></sup></label>
            <input class="form-control input-sm"  data-prompt-position="topRight:150"  id="free_quantity"  name="free_quantity" value="0">                
          </div> </div>

          <div class="col-md-1">
          <div class="form-group required">
            <label  class="control-label">Unit<sup></sup></label>
            <input class="form-control input-sm" readonly  type="text" id="product_unit"  name="product_unit">                
          </div> </div>


          <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Unit Price<sup></sup></label>
              <input class="form-control input-sm"   id="product_price"  type="text" name="product_price">                
            </div> </div>
            <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Sell Price<sup></sup></label>
              <input class="form-control input-sm" id="product_sprice"  type="text" name="product_sprice">                
            </div> </div>
            <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Batch<sup></sup></label>
              <input class="form-control validate[required] input-sm" type="text" id="batch">                
            </div> </div>
            <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Expiry<sup></sup></label>
              <input class="form-control validate[required] input-sm datepicker" placeholder="yyyy-mm-dd" type="text" id="expiry">                
            </div> </div>

            <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Supplier Bill No.<sup></sup></label>
              <input class="form-control validate[required] input-sm" tabindex="5" data-prompt-position="bottomRight:100" type="text" name="ve_bill_no" id="ve_bill_no">                
            </div> </div>

            <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Store Type<sup></sup></label>
              <select class="form-control input-sm" tabindex="6" name="ve_customer">
                <?php if($user_dept==5 || $user_dept==15) { ?><option value="15">Central Store</option> <?php } ?>
                <?php if($user_dept==5 || $user_dept==23) { ?><option value="23">General Store</option> <?php } ?>
              </select>                
            </div> </div>

            <div class="col-md-2">
            <label  class="control-label">Fully Delivered<sup></sup></label><br>
            <input id="delivery_type" type="checkbox" name="delivery_type">
            </div>



            </div>
          </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-md-12">
                <div>
                  <a class="button btn btn-primary" >Add to Table</a>
                  <input class="btn-large btn-default btn" id="reset" type="reset" value="Reset">
                </div>
              </div>

            </div>
          </div>
          <?php //echo form_close(); ?>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Product</th>
                        <th>Batch</th>
                        <th>Expiry</th>
                        <th>U.Price</th>
                        <th>S.Price</th>
                        <th>Qty</th>
                        <th>Free</th>
                        <th>Unit</th>
                        <th>CGST</th>
                        <th>SGST</th>
                        <th>Total</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                  <tbody> 
                  </tbody>
                  </table> 
                
                  <table class="table table-bordered">
                    <tr><th width="67.5%">Total</th>
                      <th><input type="hidden" name="sum" readonly id="sum" value="0">Rs. <label id="sum1">0</label></th></tr>

                      <tr><th width="80%">SGST Total</th>
                      <th><input type="hidden" name="ve_sgst" readonly id="vesgst" value="0">Rs. <label id="vesgst1">0</label></th></tr>
                      <tr><th width="80%">CGST Total</th>
                      <th><input type="hidden" name="ve_cgst" readonly id="vecgst" value="0">Rs. <label id="vecgst1">0</label></th></tr>

                    <tr><th width="67.5%">Discount</th><th>
                     <input type="hidden" readonly name="discounta" id="discounta" value="0">Rs. <label id="discounta1"></label></th></tr>
                    <tr><th width="80%">Grand Total</th><th><input type="hidden" name="gtotal" readonly id="gtotal" value="0">Rs. <label id="gtotal1"></label></th></tr>
                    <tr><th width="80%">Amount Payable</th><th><input type="hidden" name="apayable" readonly id="apayable" value="0">Rs. <label id="apayable1">0</label></th></tr>
                    <tr><th width="80%">Round off Value</th><th><input type="hidden" name="roundoff" readonly id="roundoff" value="0">Rs. <label id="roundoff1"></label></th></tr>
                     <tr><th width="80%">Amount Paid</th><th>Rs. <input  class="form-control validate[required] text-input" data-prompt-position="topRight:150" name="apaid" id="apaid"></th></tr>
                     <tr><th width="80%">Balance to Pay</th><th><input type="hidden" class="form-control" name="balance" readonly id="balance">Rs. <label id="balance1"></label></th></tr>
                    </table>
                    <table class="table table-striped table-bordered table-hover">   
                     <tr>
                        <td width="15%"><b>DISCOUNT (Rs.) </b></td>
                        <td width="10%">
                          <input class="form-control input-sm" type="text" required="required" name="pDiscount" id="pDiscount" value="0" />
                        </td>          
                        <td width="5%"><button class="btn btn-info btn-sm alldisc" type="button">Add</button></td>
                        <td width="70%"></td>
                      </tr>
                  </table>
   <button class="btn-large btn-success btn" type="submit" onclick="save();" name="submit1" accesskey="s" title="short key-ALT+S"><i class='fa fa-floppy-o'></i> Save</button>
   <button class="btn-large btn-warning btn" type="submit" name="submit1" onclick="save_print();" accesskey="p" title="short key-ALT+P"><i class="fa fa-print"></i> Save & Print</button>
   <input type="hidden" id="Printid">
<!-- <a href="#" class="btn btn-success" onclick="save();"   accesskey="s" title="short key-ALT+S"><i class="fa fa-floppy-o"></i> Save</a>

<a href="#" class="btn btn-warning" onclick="save_print();" accesskey="p" title="short key-ALT+P"><i class="fa fa-print"></i> Save & Print</a> -->

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
</div>
</section>

</section><!-- /.right-side -->

<script>

function save(){

      var postData      = $("#post_form").serializeArray();
      var po_pono       =  $('#po_pono').val();
      var ve_bill_no    =  $('#ve_bill_no').val();
      var vendorid      =  $('#vendorid').val();
      var vendor        =  $('#vendor').val();
      var vendorph      =  $('#vendorph').val();
      var apaid         =  $('#apaid').val();
      var datepicker    =  $('#datepicker').val();

      if(po_pono != "" ||  ve_bill_no != "" ||  vendorid != "" || vendor != "" || apaid != "" || datepicker != "")
          {
      var formURL  = "<?= base_url() ?>purchase/purchaseAdd";
      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          $('#Printid').val(data);
          window.location = "<?php echo base_url();?>index.php/purchase/add";
          <?php 
          $this->session->set_flashdata('Success','purchased'); ?>
        }
      });
      }
}

function save_print(){

      var postData      = $("#post_form").serializeArray();
      var po_pono       =  $('#po_pono').val();
      var ve_bill_no    =  $('#ve_bill_no').val();
      var vendorid      =  $('#vendorid').val();
      var vendor        =  $('#vendor').val();
      var vendorph      =  $('#vendorph').val();
      var apaid         =  $('#apaid').val();
      var datepicker    =  $('#datepicker').val();

       if(po_pono != "" ||  ve_bill_no != "" ||  vendorid != "" || vendor != "" || apaid != "" || datepicker != "")
          {
      var formURL  = "<?= base_url() ?>purchase/purchaseAdd";
      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          // alert(data)
          window.location     = "<?php echo base_url();?>index.php/purchase/getPrint?pPrintid="+data;

        }
      });
      }
} 
    </script>

<script>
 $('.datepicker').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true
    });</script>