


<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

 
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {

$('#sp_country').change(function(){
    var ctid = $('#sp_country').val();
    $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>patient/get_state/"+ctid,
     success: function(data){
      $("#sp_state").empty();
      $.each(data, function(index) {
        $("#sp_state").append('<option value=' + data[index].id +'>'+data[index].name+'</option>');
      });
    }
  })
  });

$('#sp_state').change(function(){
    var ctid = $('#sp_state').val();
    $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>patient/get_city/"+ctid,
     success: function(data){
      $("#sp_city").empty();
      $.each(data, function(index) {
        $("#sp_city").append('<option value=' + data[index].id +'>'+data[index].name+'</option>');
      });
    }
  })
  });



 oTable = $('#category_table').dataTable({
});
 });
</script>




<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
  <div class="content-wrapper">
  <section class="content-header">
    <h1>
      Suppliers
      <small>Create  Suppliers </small>
    </h1>

    <ol class="breadcrumb">
        <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."suppliers"; ?>">Suppliers</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div align="right">
         <a title="short key-ALT+B" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."suppliers"; ?>" accesskey="b"><i class="fa fa-mail-reply-all"></i> Back</a>
       </div>

     </div>
               <?php echo form_open_multipart($action); ?>
     <div class="box-body">
      <div class="row">
        <div class="col-md-12">


  <?php
                        if($this->uri->segment(1)=="add")
                        $clas   =   'required="required"';
                      else
                      $clas = '';
                    ?> 
           <div class="col-md-3">
          <div class="form-group">
                        <label for="focusinput" class="control-label">Image(1600 X 400)</label>
                        
                        <?php if($image != "") { echo "<img id='blah' width='139px' height='150px' src='".$this->config->item("base_url")."uploads/supplier/".$image."'>"; } else { echo "<img id='blah' width='150px' height='150px' src='".$this->config->item("base_url")."uploads/Default2.png"."'>"; } ?>
                        <input type="file" name="image" onchange="readURL(this);" <?php echo $clas; ?>>
                      </div>
                    </div>
          <div class="form-group col-md-3">
            <label  class="control-label">Supplier<sup>*</sup></label>
            <input class="form-control validate[required]  input-sm" tabindex="1"  data-prompt-position="topRight:110" style="width:190px;height:29px;" type="text" name="sp_vendor" value="<?=$sp_vendor ?>">
            <input type="hidden" name="sp_id" value="<?=$sp_id ?>">                
          </div>    
             <div class="form-group col-md-3">
             <label  class="control-label">LIC No. <sup>*</sup></label>
             <input class="form-control  input-sm" tabindex="2" style="width:190px;height:29px;" type="text" name="sp_lic" value="<?=$sp_lic ?>">
          </div> 
           <div class="form-group col-md-3">
            <label  class="control-label">GST No. <sup>*</sup></label>
            <input class="form-control input-sm" tabindex="3" style="width:190px;height:29px;" type="text" name="sp_gst_no" value="<?=$sp_gst_no ?>">
          </div>
          <div class="form-group col-md-3">
            <label  class="control-label">Phone <sup>*</sup></label>
            <input class="form-control validate[required] input-sm"  tabindex="4" data-prompt-position="bottomRight:110" tabindex="4" style="width:190px;height:29px;" type="text" name="sp_phone" value="<?=$sp_phone ?>">
          </div>
          <div class="form-group col-md-3">
            <label  class="control-label">Fax <sup></sup></label>
            <input class="form-control input-sm" tabindex="5" style="width:190px;height:29px;" type="text" name="sp_fax" value="<?=$sp_fax ?>">  
          </div>
          <div class="form-group col-md-3">
            <label  class="control-label">Website  <sup></sup></label>
             <input class="form-control input-sm" tabindex="6" style="width:190px;height:29px;" type="text" name="sp_website" value="<?=$sp_website ?>">  
          </div>
          <div class="form-group col-md-3">
            <label  class="control-label">Address <sup>*</sup></label>
            <input class="form-control validate[required] input-sm" data-prompt-position="bottomRight:110" tabindex="7" placeholder="Supplier Address" style="width:190px;height:29px";  type="text" name="sp_address" value="<?= $sp_address ?>">
          </div>
          <div class="form-group col-md-3">
            <label  class="control-label">Street</label>
            <input type="text" name="sp_street" tabindex="8" placeholder="Street" style="width:190px;height:29px"; class="form-control input-sm" value="<?= $sp_street ?>">
          </div>
          <div class="form-group col-md-3">
           <label  class="control-label"> Zip Code <sup></sup></label>
          <input class="form-control input-sm"  tabindex="9"   placeholder="Zip Code" style="width:190px;height:29px";  type="text" name="sp_zip" value="<?= $sp_zip ?>">
          </div>
          <div class="form-group col-md-3">
             <label  class="control-label">Country <sup>*</sup></label>
             <select class="form-control validate[required] input-sm"  style="width:190px;height:29px";   tabindex="9" name="sp_country" id="sp_country"> 
              <option value="Select">--Country--</option>
              <?php foreach ($countries as $key => $countries) {
                ?>
              <option value="<?php echo $countries['id']?>"<?php if($countries['id']==$sp_country) { ?> selected="selected" <?php } ?>><?php echo $countries['name']?></option>
                <?php
              }?>
            </select>
          </div>
        <div class="form-group col-md-3">
          <label  class="control-label">State <sup>*</sup></label>
          <select class="form-control validate[required] input-sm"  style="width:190px;height:29px";  tabindex="10" name="sp_state" id="sp_state"> 
              <option value="Select">--State--</option>
              <?php foreach ($states as $key => $states) {
                ?>
              <option value="<?php echo $states['id']?>"<?php if($states['id']==$sp_state) { ?> selected="selected" <?php } ?>><?php echo $states['name']?></option>
                <?php
              }?>
            </select>
          </div> 
          <div class="form-group col-md-3">
            <label  class="control-label">City <sup>*</sup></label>
          <select class="form-control validate[required] input-sm"  style="width:190px;height:29px";  tabindex="11" name="sp_city"  id="sp_city"> 
              <option value="Select">--City--</option>
              <?php foreach ($cities as $key => $cities) {
                ?>
              <option value="<?php echo $cities['id']?>"<?php if($cities['id']==$sp_city) { ?> selected="selected" <?php } ?>><?php echo $cities['name']?></option>
                <?php
              }?>
            </select>
          </div>
          <div class="form-group col-md-3"></div>
           <div class="form-group col-md-3">
            <label  class="control-label">Email</label>
            <input type="text" name="sp_email" tabindex="13" placeholder="Supplier Email" style="width:190px;height:29px"; class="form-control input-sm" value="<?= $sp_email ?>">
          </div>
           <div class="form-group col-md-3">
           <label  class="control-label"> Payement Type <sup>*</sup></label>
          <select class="form-control input-sm"  style="width:190px;height:30px";  tabindex="14" name="sp_payment">
              <option value="Select">--Payement Type--</option>
              <option value="c"<?php if($sp_payment=='c') { ?> selected="selected" <?php } ?>>Cash</option>
              <option value="nc"<?php if($sp_payment=='nc') { ?> selected="selected" <?php } ?>>Credit</option>
            </select>
          </div>
          
            </div> 
        </div>
      </div>

      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
              <input class="btn-large btn-primary btn" tabindex="19" type="submit" value="Submit" name="submit" accesskey="s" title="short key-ALT+S"> 
              <input class="btn-large btn-default btn" type="reset" value="Reset">
          </div>
        </div>
      </div>

      <?php echo form_close(); ?>


</div>
</section>

</div>
 
<script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
            .attr('src', e.target.result)
            .width(139)
            .height(150);
        };

        reader.readAsDataURL(input.files[0]);
        
    }
}

</script>

<!-- vendor image change url -->



<!-- companydetails image change url -->

<script type="text/javascript">
function readURL2(input) {
    if (input.files && input.files[0]) {
        
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah2')
            .attr('src', e.target.result)
            .width(180)
            .height(65);
        };

        reader.readAsDataURL(input.files[0]);
        
    }
}

</script>

<!-- companydetails  change url -->

    