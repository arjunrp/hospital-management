<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


  <script> 
$(document).ready(function() {

   $('#bk_phone').change(function(){
      var uRL1   = "<?= base_url() ?>booking/get_phone/"+$(this).val();
      $.ajax({

        url : uRL1,
        dataType: "json",
        success:function(data3)
        {
           $.each(data3, function(index) {
        $("#bk_name").val(data3[index].p_title+" "+data3[index].p_name);
        $("#bk_mrd").val(data3[index].p_mrd_no);
      });
        }

      });

  });

  $('#bk_mrd').change(function(){
      var uRL1   = "<?= base_url() ?>booking/get_mrd/"+$(this).val();
      $.ajax({

        url : uRL1,
        dataType: "json",
        success:function(data3)
        {
           $.each(data3, function(index) {
        $("#bk_name").val(data3[index].p_title+" "+data3[index].p_name);
        $("#bk_phone").val(data3[index].p_phone);
      });
        }

      });

  });

  $('#bk_doc').change(function(){
    var doc_id = $('#bk_doc').val();
    var uRL1   = "<?= base_url() ?>booking/get_doctor";
    $.ajax({
        url : uRL1,
        type: "POST",
        data : {doc_id:doc_id},
        success:function(data2, textStatus, jqXHR)
        {
          $('#bk_dept').val(data2);
        }
  })
  });

  $('#bk_shift').change(function(){
    var sft_id = $('#bk_shift').val();
    var doc_id = $('#bk_doc').val();
    if(doc_id==0)
    {
      alert("Select Doctor");
       $('#bk_shift').empty();
       $("#bk_shift").append('<option value="0">--Select--</option>');
       $("#bk_shift").append('<option value="m">--Morning--</option>');
       $("#bk_shift").append('<option value="e">--Evening--</option>');
    }
    else {
    var uRL    = "<?= base_url() ?>booking/verify_tokens";
    $.ajax({
        url : uRL,
        type: "POST",
        data : {sft_id:sft_id,doc_id:doc_id},
        success:function(data, textStatus, jqXHR)
        {
          $('#token').val(data);
          var bk_no = $('#bk_no').val();
          if(data<bk_no)
          {
            alert("Max Token "+data+" Exceeded");
          }
        }
  })
  }
  });
  });
  </script>


  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#customer_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       Booking
       <small>Edit Booking </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."booking"; ?>">Booking</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/booking/add">
        <i class="fa fa-plus-circle"></i> Create New
      </a>
    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>

      <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row"> 
       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">Token No.<sup>*</sup></label>
          <input class="form-control" tabindex="1" type="text" id="bk_no" name="bk_no" value="<?= $bk_no;?>">
          <input class="form-control" type="hidden" name="bk_id" value="<?=$bk_id?>"> 
          <input class="form-control" type="hidden" id="token" value="0"> 
          <input class="form-control" type="hidden" name="user_type" value="<?=$user_type?>">                 
        </div>
        <div class="col-md-2">
          <label  class="control-label">Booking Date<sup>*</sup></label>
          <input class="form-control validate[required]" tabindex="2" readonly data-prompt-position="topRight:150" type="text" name="bk_date" id="datepicker" value="<?=$bk_date?>">                
        </div>
         <div class="col-md-3"></div>
        <div class="col-md-3">
         <label  class="control-label">Doctor<sup>*</sup></label>
         <select class="form-control validate[required]" id="bk_doc" tabindex="3" name="bk_doc"> <option>--Select--</option>
              <?php foreach ($doctors as $key => $doctors) {
                ?>
              <option value="<?php echo $doctors['u_emp_id']?>"<?php if($doctors['u_emp_id']==$bk_doc) { ?> selected="selected" <?php } ?> ><?php echo $doctors['u_name']?></option>
                <?php
              }?>
            </select> 
            <input class="form-control" type="hidden" name="bk_dept" id="bk_dept" value="<?=$bk_dept?>"> 
       </div>
       <div class="col-md-2">
         <label  class="control-label">Shift<sup>*</sup></label>
         <select class="form-control validate[required]" tabindex="3" id="bk_shift" name="bk_shift"> 
              <option>--Select--</option>
              <option value="m" <?php if($bk_shift=="m") { ?> selected="selected" <?php } ?>>--Morning--</option>
              <option value="e" <?php if($bk_shift=="e") { ?> selected="selected" <?php } ?>>--Evening--</option>
            </select> 
       </div>
      </div>
      <div class="col-md-12">
       <div class="col-md-2">
         <label  class="control-label">MRD<sup>*</sup></label>
         <input class="form-control" type="text" name="bk_mrd" id="bk_mrd" value="<?= $bk_mrd?>">      
       </div>
       <div class="col-md-2">
         <label  class="control-label">Patient's Name<sup></sup></label>
         <input class="form-control" type="text" name="bk_name" id="bk_name" value="<?= $bk_name?>">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Phone No.<sup></sup></label>
         <input class="form-control" type="text" name="bk_phone" id="bk_phone" value="<?= $bk_phone?>">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Address<sup></sup></label>
         <input class="form-control input-sm" type="text" name="bk_address" id="bk_address" value="<?= $bk_address?>">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Street<sup></sup></label>
         <input class="form-control input-sm" type="text" name="bk_street" id="bk_street" value="<?= $bk_street?>">
       </div>

       <div class="col-md-1">
         <label  class="control-label">Age<sup></sup></label>
         <input class="form-control input-sm" type="text" name="bk_age" id="bk_age" value="<?= $bk_age?>">
       </div>
     </div>
          </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-md-12">
                <div>
                  <button class="button btn btn-primary" type="submit">Update</button>
                  <input class="btn-large btn-default btn" type="reset" value="Reset">
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php echo form_close(); ?>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Token No.</th>
                        <th>Doctor</th>
                        <th>Shift</th>
                        <th>MRD</th>
                        <th>Patient's Name</th>
                        <th>Phone No.</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                  <tbody> 
                    <?php if($output) { echo $output; } ?>
                  </tbody>
                  </table> 
                

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>

</section><!-- /.right-side -->


<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });</script>

    <script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>
