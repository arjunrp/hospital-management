<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
<html>
<head>
<link rel="stylesheet" href='<?= base_url() ?>asset/plugins/jvectormap/jquery-jvectormap-1.2.2.css' type="text/css"/>
  <link media="all" type="text/css" rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.css">
   <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.min.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.min.css' type="text/css"/>
   <link rel="stylesheet" href="<?= base_url() ?>asset/css/skins/_all-skins.min.css"> 
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
 
  <!-- <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datepicker/datepicker3.css"> -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<!-- <link rel="stylesheet" href="<?= base_url() ?>asset/css/AdminLTE.min.css" type="text/css"/>  -->
   <link rel="stylesheet" href="<?= base_url() ?>asset/bootstrap/css/AdminLTE.min.css" type="text/css"/>
   <script  src="<?= base_url() ?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
   <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
   <style type="text/css">
   .font_reduce
   {
     font-size:small;
   }
   </style>
</head>

 <body onload="window.print();" style="margin-top:0%;display:block;height:100%;width:80%;margin-left:9%;margin-right:6% ">

<!-- onload="window.print();"  -->

<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
            
        <hr>
         <center> <small class="pull-center"><b><u>CASH REPORT - <?=date("d-m-Y") ?></u></b></small></center>

  <div class="box-tools">
              <a title="Back" class="dontprint btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."ccounter/todays_report"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>
    </div> </div>
    <!-- info row -->

     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">
            <br>

             <table class="table table-condensed dataTable no-footer gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Sl. No </th>
                        <th>Date</th>
                        <th>Department</th>
                        <th>Bill No From</th>
                        <th>Bill No To</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                  <tbody>
                  <?php
                  $slno=1;
                  $total_amount = 0;
                  $sect = 1;
                  $sumpharm  = 0;
                  $sumpharmr = 0;
                  $sumpharmb = 0;
                  $count = 1;
                  $count1 = 1;
                  $count2 = 1;
                  foreach($treports as $treport)
                  { 
                    $total_amount = $total_amount + $treport['apaid'];
                    ?> 
                    <tr>
                      <td><?= $slno++  ?></td>
                      <td><?=date("d-m-Y",strtotime($treport['ve_cash_date'])) ?></td>
                      <td><?php if($treport['ve_type'] != "opbl")
                          { echo $treport['dp_department']; }
                          if($treport['ve_type'] == "si")
                          {
                            echo $sect++;
                          }
                          else if($treport['ve_type'] == "so")
                          {
                            echo $sect++;
                          }
                          else if($treport['ve_type'] == "sr")
                          {
                            echo " - Sales Return";
                          }
                          else if($treport['ve_type'] == "opbl")
                          {
                            echo "OP Billing";
                          }
                          else if($treport['ve_type'] == "ad")
                          {
                            echo " - Advance Payment";
                          }
                          else if($treport['ve_type'] == "adr")
                          {
                            echo " - Advance Return";
                          }
                          else if($treport['ve_type'] == "dis")
                          {
                            echo " - Discharge";
                          }
                          else if($treport['ve_type'] == "dsr")
                          {
                            echo " - Discharge Return";
                          }

                      ?></td>
                      <td><?=$treport['mi_veno'] ?></td>
                      <td><?=$treport['mx_veno'] ?></td>
                      <td><?="Rs. ".$treport['apaid'] ?></td>
                      <?php if($treport['ve_type'] == "si") { $count++;  ?><td><b><?php $sumpharm = $sumpharm + $treport['apaid']; if($count>2) { echo"Rs. ".$sumpharm; } ?></b></td> <?php } ?>
                      <?php if($treport['ve_type'] == "sr") { $count1++; ?><td><b><?php $sumpharmr = $sumpharmr + $treport['apaid']; if($count1>2) { echo "Rs. ".$sumpharmr; } ?></b></td> <?php } ?>
                      <?php if($treport['ve_type'] == "opbl") { $count2++; ?><td><b><?php $sumpharmb = $sumpharmb + $treport['apaid']; if($count2>2) { echo "Rs. ".$sumpharmb; } ?></b></td> <?php } ?>
                    </tr>
                    <?php }  ?>

                  </tbody>
                  </table> 
                
                  <table class="table">
                    <tr><th width="50%"></th><th>Total</th>
                    <th><?="Rs. ".$total_amount?></th></tr>

                    </table>

   </div>
      <!-- /.col -->

    <div class="row">

        <div class="col-xs-8">
          Authorized Signature  with Seal<br><br><br><br><br><br><br>
      </div>
      <!-- /.col -->
    </div>

     </div>
       
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
 
 <style type="text/css" media="print">
.dontprint
{ display: none; }
</style>
</body>
</html>