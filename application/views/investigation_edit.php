<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script> 
$(document).ready(function() {

 <?php
$js_array = json_encode($lab_bills);
echo "var lab_bill = ". $js_array . ";\n";
?>

$.each(lab_bill, function(index) {
        // alert(sum+","+vesgst+","+vecgst+","+apayable);

         var newrow      = '<tr><td><input type="hidden" value="'+lab_bill[index].ved_itemid+'" name="ved_itemid[]"><input class="form-control input-sm" type="hidden" value="'+lab_bill[index].ved_item+'" name="ved_item[]">' + lab_bill[index].ved_item + '</td><td><input class="labmrp" type="hidden" value="'+lab_bill[index].ved_total+'" name="ved_total[]"><b>Rs. </b><label>'+lab_bill[index].ved_total+'</label></td><td><button class="btn btn-xs btn-danger dlte"><i class="fa fa-times-circle-o"></i></button></td></tr>';
        $('#item_table tr:last').after(newrow);
      });


  var checkbox = $('#payment_type');
  var ve_status = $('#payment_type').val();

  if(ve_status=="cr")
  { checkbox.attr('checked','checked'); }
  else
  { checkbox.removeAttr('checked'); }

  $('input[type="checkbox"]'). click(function(){
  if($(this). prop("checked") == true){
  $("#payment_type").val("True");
  }
  else if($(this). prop("checked") == false){
  $("#payment_type").val("False");
  } 
  });



  $('#lab_tests').change(function(){
    var lab_tests = $('#lab_tests').val();
     $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>investigation/getTestnames/"+lab_tests,
     success: function(data){
      $('#lab_tname').empty();
      $("#lab_tname").append('<option value="0">--Select--</option>');
      $.each(data, function(index) {
        $("#lab_tname").append('<option value=' + data[index].tn_id +'>'+data[index].tn_name+'</option>');
      });
    }
  })
  });

   $('#lab_tname').change(function(){
    var lab_tname = $('#lab_tname').val();
    var uRL1   = "<?= base_url() ?>investigation/test_price";
    $.ajax({
        url : uRL1,
        type: "POST",
        data : {tname:lab_tname},
        success:function(data2, textStatus, jqXHR)
        {
          $('#lab_tprice').val(data2);
        }
  })
  });



  });
  </script>

  <script>
  $(document).ready(function() {

  $('.button').click(function() {
    var sel = document.getElementById("lab_tname");
    var value = sel.options[sel.selectedIndex].value; // or sel.value
    var text = sel.options[sel.selectedIndex].text; 

    var lab_tid        = value;
    var lab_tname      = text;
    var lab_tprice     = $('#lab_tprice').val();
    

    var newrow      = '<tr><td><input type="hidden" value="'+lab_tid+'" name="ved_itemid[]"><input class="form-control input-sm" type="hidden" value="'+lab_tname+'" name="ved_item[]">' + lab_tname + '</td><td><input class="labmrp" type="hidden" value="'+lab_tprice+'" name="ved_total[]"><b>Rs. </b><label>'+lab_tprice+'</label></td><td><button class="btn btn-xs btn-danger dlte"><i class="fa fa-times-circle-o"></i></button></td></tr>';

    var lab_mrp     = $('#lab_mrp').val();

    lab_mrp         = parseFloat(lab_mrp) + parseFloat(lab_tprice);
    lab_mrp         = (Math.round(lab_mrp * 100) / 100).toFixed(2);

    $('#lab_mrp').val(lab_mrp);
    $('#lab_mrp1').html(lab_mrp);

    $('#item_table tr:last').after(newrow);

    // document.getElementById('product').value = "";
    // document.getElementById('productid').value = "";
    // document.getElementById('product_quantity').value = "";
    // document.getElementById('product_unit').value = "";
    // document.getElementById('product_price').value = "";
    // document.getElementById('batch').value = "";
    // document.getElementById('expiry').value = "";
    // document.getElementById('pod_sgst').value = "";
    // document.getElementById('pod_cgst').value = "";
});

$(document).on('click','button.dlte', function() {
    var $row    = $(this).closest("tr");    // Find the row

    var r = confirm("Are you sure want to delete!");
    if (r == true) {
    $(this).closest('tr').remove();

    var grand = 0;

    $('.labmrp').each(function(){
        grand += parseFloat(this.value);
    });

    grand           =   (Math.round(grand * 100) / 100).toFixed(2);

    $('#lab_mrp').val(grand);
    $('#lab_mrp1').html(grand);
    } 
    });

  });
  </script>


  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#customer_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>

 <?php
foreach ($lab_bills as $key => $lab_bill) {

    $ve_id            = $lab_bill['ve_id'];
    $ve_vno           = $lab_bill['ve_vno'];
    $ve_date          = $lab_bill['ve_date'];
    $ve_customer      = $lab_bill['ve_customer'];
    $ve_mrd           = $lab_bill['ve_mrd'];
    $p_name           = $lab_bill['ve_patient'];
    $p_phone          = $lab_bill['ve_phone'];
    $p_age            = $lab_bill['ve_amount'];
    $p_gender         = $lab_bill['ve_gtotal'];
    $ve_apayable      = $lab_bill['ve_apayable'];
    $ve_apaid         = $lab_bill['ve_apaid'];
    $ve_user          = $lab_bill['ve_user'];
    $ve_type          = $lab_bill['ve_type']; 
    $doctor           = $lab_bill['ve_doctor']; 
    $ve_status        = $lab_bill['ve_status'];
  }

  ?>
  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       Lab
       <small>Edit Lab Test </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."investigation"; ?>">Lab</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div class="box-tools">
      
    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>

      <form id="post_form">
     <div class="box-body">
      <div class="row"> 
       <div class="col-md-12">

        <div class="col-md-4">
          <label  class="control-label">Bill No. :<sup></sup></label><?=$ve_vno?>
          <input class="form-control" type="hidden" name="user_type" value="<?=$user_type?>">
          <input class="form-control" type="hidden" name="ve_date" value="<?=$ve_date?>">
          <input class="form-control" type="hidden" id="ve_id" name="ve_id" value="<?=$ve_id?>">
          <input class="form-control" type="hidden" name="ve_type" value="<?=$ve_type?>">            
        </div>
        <div class="col-md-4">
          <label  class="control-label">Date : <sup></sup></label> <?=date("d-m-Y",strtotime($ve_date)) ?>                
        </div>

       <div class="col-md-4">
         <label  class="control-label">Doctor  : <sup></sup></label> <?=$doctor?>
       </div>
        <div class="col-md-12"><br></div>
        <?php if($ve_type=="lbi") { ?>
         <div class="col-md-3">
         <label  class="control-label">IP No. : <sup></sup></label> <?=$ve_customer?>
       </div>
       <?php } ?>

          <div class="col-md-3 ipno1">
         <label  class="control-label">MRD : <sup></sup></label> <?=$ve_mrd?>
       </div>
       <div class="col-md-4">
         <label  class="control-label">Patient's : <sup></sup></label> <?=$p_name?>
       </div>
       <div class="col-md-2">
         <label  class="control-label">Phone No. : <sup></sup></label> <?=$p_phone?>
       </div>
     </div>
     <div class="col-md-12"> <br>
       <div class="col-md-2">
         <label  class="control-label">Age<sup></sup></label>
         <input class="form-control input-sm validate[required]" data-prompt-position="topLeft:190" type="text" value="<?=$p_age?>" name="ve_age">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Gender<sup></sup></label>
         <select class="form-control validate[required] input-sm" id="bk_gender" tabindex="8" name="ve_gender">
          <option value="Female" <?php if($p_gender=="Female") { ?> selected <?php } ?>>--Female--</option>
          <option value="Male" <?php if($p_gender=="Male") { ?> selected <?php } ?>>--Male--</option>
          <option value="Child" <?php if($p_gender=="Child") { ?> selected <?php } ?>>--Child--</option>
            </select>  
       </div>
      </div>
      <div class="col-md-12">
      <br>
         <div class="col-md-2">
         <label  class="control-label">Tests<sup></sup></label>
         <select class="form-control validate[required] input-sm" id="lab_tests" tabindex="3" name="lab_tests"> 
          <option value="0">--Select--</option>
           <?php foreach ($lab_tests as $key => $lab_test) {
                ?>
              <option value="<?php echo $lab_test['ts_id']?>"><?php echo $lab_test['ts_test']?></option>
                <?php
              }?>
          </select>  
       </div>
        <div class="col-md-2">
         <label  class="control-label">Test Name<sup></sup></label>
         <select class="form-control validate[required] input-sm" id="lab_tname" tabindex="4" name="lab_tname">
          <option value="0">--Select--</option>
            </select>  
       </div>
       <div class="col-md-2">
         <label  class="control-label">Price<sup></sup></label>
         <input class="form-control validate[required] input-sm" id="lab_tprice" tabindex="4" name="lab_tprice"> 
       </div>

        <div class="col-md-2">
        <label  class="control-label">Credit<sup></sup></label><br>
         <input id="payment_type" type="checkbox" name="payment_type" value="<?=$ve_status?>">
       </div>

     </div>
          </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-md-12">
                <div>
                  <a class="button btn btn-primary" >Add to List</a>
                  <input class="btn-large btn-default btn"  tabindex="9" type="reset" value="Reset">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Test Name</th>
                        <th>Price</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                  <tbody> 

                  </tbody>
                  </table> 
                  <table class="table table-bordered">
                    <tr><th width="80%">Total</th>
                      <th><input type="hidden" name="lab_mrp" readonly id="lab_mrp" value="<?=$ve_apayable ?>">Rs. <label id="lab_mrp1"><?=$ve_apayable ?></label></th></tr>
                    </table>

                    <a href="#" class="btn btn-success" onclick="save();" accesskey="s" title="short key-ALT+S"><i class="fa fa-floppy-o"></i> Save</a>
                  <input type="hidden" id="Printid"> 
                    <a href="#" class="btn btn-warning" onclick="save_print();" accesskey="p" title="short key-ALT+P" ><i class="fa fa-print"></i> Save & Print</a>
                
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</form>
</div>
</section>

</section><!-- /.right-side -->

<script>
function save(){

      var postData = $("#post_form").serializeArray();
      var ve_id    = $('#ve_id').val();
      var formURL  = "<?= base_url() ?>investigation/investigationUpdate/"+ve_id;
      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          alert("Success");
          $('#Printid').val(data);
           window.location = "<?php echo base_url();?>index.php/investigation";
        }
      });
}
 function save_print(){

      var postData = $("#post_form").serializeArray();
      var ve_id    = $('#ve_id').val();
      var formURL  = "<?= base_url() ?>investigation/investigationUpdate/"+ve_id;

      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
           window.location = "<?php echo base_url();?>index.php/investigation/getBillPrint?Printid="+ve_id;
        }
      });
}
</script>

<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });</script>
<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>