<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<script> 
$(document).ready(function() {
  $('#p_brand').change(function(){
    var p_br = $('#p_brand').val();
    var p_ty = $('#p_type').val();


    if(p_br != 0 && p_ty == 0)
    {
      var uRL  = "<?= base_url() ?>inventory/getBproduct/"+p_br;
      var pr_tp = "br";
    }
    if(p_br == 0 && p_ty != 0)
    {
      var uRL  = "<?= base_url() ?>inventory/getTproduct/"+p_ty;
      var pr_tp = "ty";
    }
    if(p_br != 0 && p_ty != 0)
    {
      var uRL  = "<?= base_url() ?>inventory/getBTproduct/"+p_br+"/"+p_ty;
      var pr_tp = "brty";
    }

    $.ajax({
     dataType: "json",
     url: uRL,
     success: function(data){
      if(data=="0")
      {
        $('#product').empty();
        $("#product").append('<option value="x">---No Product---</option>');
      }
      else
      {
      $('#product').empty();
      $("#product").append('<option value='+pr_tp+'>---All Product---</option>');
      $.each(data, function(index) {
        $("#product").append('<option value=' + data[index].pd_code +'>'+data[index].pd_product+'</option>');
      });
    }
    }
  })
  });

  $('#p_type').change(function(){
    
    var p_ty = $('#p_type').val();
    var p_br = $('#p_brand').val();

    if(p_br == 0 && p_ty != 0)
    {
      var uRL = "<?= base_url() ?>inventory/getTproduct/"+p_ty;
      var pr_tp = "ty";
    }
    if(p_br != 0 && p_ty == 0)
    {
      var uRL  = "<?= base_url() ?>inventory/getBproduct/"+p_br;
      var pr_tp = "br";

    }
    if(p_br != 0 && p_ty != 0)
    {
      var uRL  = "<?= base_url() ?>inventory/getBTproduct/"+p_br+"/"+p_ty;
      var pr_tp = "brty";
    }

    $.ajax({
     dataType: "json",
     url: uRL,
     success: function(data){
       if(data=="0")
      {
        $('#product').empty();
        $("#product").append('<option value="x">---No Product---</option>');
      }
      else
      {
      $('#product').empty();
      $("#product").append('<option value='+pr_tp+'>---All Product---</option>');
      $.each(data, function(index) {
        $("#product").append('<option value=' + data[index].pd_code +'>'+data[index].pd_product+'</option>');
      });
    }
    }
  })
  });


});
</script>