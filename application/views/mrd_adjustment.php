<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script> 
$(document).ready(function() {
  $('.bkno').hide();
  var op_mrd = "";

  $('#op_mrd').change(function(){

    var uRL1   = "<?= base_url() ?>opregister/get_mrd/"+$(this).val();
      $.ajax({

        url : uRL1,
        dataType: "json",
        success:function(data3)
        {
           $.each(data3, function(index) {
        $("#bk_name").val(data3[index].p_title+" "+data3[index].p_name);
        $("#bk_phone").val(data3[index].p_phone);
        $("#bk_address").val(data3[index].p_address);
        $("#bk_street").val(data3[index].p_street);
        $("#bk_city").val(data3[index].p_city);

      });
       var op_mrd =  $('#op_mrd').val();    
       $.ajax({

        url : "<?= base_url() ?>mrd_room/get_availability",
        type: "POST",
        data : {op_mrd:op_mrd},
        success: function(data5){
        if(data5 == "1")
        {
          alert("MRD not available in MRD Room");
        }
     }
   });
       $.ajax({

        url : "<?= base_url() ?>mrd_room/get_op_no",
        type: "POST",
        data : {op_mrd:op_mrd},
        success: function(data6){
          $('#bk_op').val(data6); 
     }
   });
  }

      });

  });

$('#bk_phone').change(function(){

    var uRL1   = "<?= base_url() ?>opregister/get_phone/"+$(this).val();
      $.ajax({

        url : uRL1,
        dataType: "json",
        success:function(data3)
        {
           $.each(data3, function(index) {
        $("#bk_name").val(data3[index].p_title+" "+data3[index].p_name);
        $("#op_mrd").val(data3[index].p_mrd_no);
        $("#rw_days").val(data3[index].p_renew_days);
        $("#rw_count").val(data3[index].p_renew_count);
        $("#p_renew_visits").val(data3[index].p_renew_visits);
        $("#p_renew_date").val(data3[index].p_renew_date);
        $("#bk_address").val(data3[index].p_address);
        $("#bk_street").val(data3[index].p_street);
        $("#bk_city").val(data3[index].p_city);

      });

    var op_mrd =  $('#op_mrd').val();    
       $.ajax({

        url : "<?= base_url() ?>mrd_room/get_availability",
        type: "POST",
        data : {op_mrd:op_mrd},
        success: function(data5){
        if(data5 == "1")
        {
          alert("MRD not available in MRD Room");
        }
     }
   });

  }

      });
       

  });

  });
  </script>


  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#customer_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       MRD Room
       <small>Adjustment</small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."mrd_room"; ?>">MRD Room</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div class="box-tools">
      <a class="btn btn-sm btn-success pull-right  view-btn-create" href="<?php echo base_url();?>index.php/mrd_room">
        <i class="fa fa-reply-all"></i> Back
      </a>
    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>

      <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row"> 
       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">Date<sup>*</sup></label>
          <input class="form-control validate[required]" data-prompt-position="topRight:150" type="text" name="mr_date" id="datepicker"  tabindex="1" value="<?=date("Y-m-d") ?>">                
        </div>
      
       <div class="col-md-2">
         <label  class="control-label">MRD<sup>*</sup></label>
         <input class="form-control validate[required] input-sm" data-prompt-position="bottomRight:100" tabindex="2" type="text" name="mr_mrd" id="op_mrd"><input class="form-control" type="hidden" id="rw_days">  
       </div>
       <div class="col-md-2">
         <label  class="control-label">OP No.<sup></sup></label>
         <input class="form-control input-sm" tabindex="4" type="text" id="bk_op" name="mr_tno">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Patient's Name<sup></sup></label>
         <input class="form-control input-sm" type="text" tabindex="3" id="bk_name">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Phone No.<sup></sup></label>
         <input class="form-control input-sm" tabindex="4" type="text" id="bk_phone">
       </div>
        <div class="col-md-2">
         <label  class="control-label">Doctor<sup>*</sup></label>
         <select class="form-control validate[required] input-sm" id="op_doctor" tabindex="5" tabindex="3" name="mr_doc">
          <option value="0">--Select--</option>
             <?php foreach ($doctors as $key => $doctors) {
                ?>
              <option value="<?php echo $doctors['u_emp_id']?>"><?php echo $doctors['u_name']?></option>
                <?php
              }?> 
            </select>    
       </div>
      </div>
      <div class="col-md-12">
       <div class="col-md-4">
         <label  class="control-label">Remark<sup></sup></label>
         <textarea class="form-control input-sm"  tabindex="6" name="mr_remark">
         </textarea>
       </div>

     </div>
          </div>
          <br><br>
          <div class="box-footer">
            <div class="row">
              <div class="col-md-12">
                <div>
                  <button class="button btn btn-primary" tabindex="7" type="submit">Transfer from MRD Room</button>
                  <input class="btn-large btn-default btn" type="reset" tabindex="utf-8" value="Reset">
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php echo form_close(); ?>

</div>
</section>

</section><!-- /.right-side -->


<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });</script>
<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>