<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<script>
  $(document).ready(function() {
    var amount1        = $('#amount').val();
  $('#amount').change(function() {
    var amount        = $('#amount').val();
    var blnce         = $('#blnce').val();
    var blnce         = parseFloat(amount)-parseFloat(amount1);
    $('#blnce').val(blnce);
  });
});
    </script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>


<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
       <?php echo $page_title; ?>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."expence"; ?>">Expense</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div align="right">
         <a title="short key-ALT+B" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."expence"; ?>"accesskey="b" ><i class="fa fa-mail-reply-all"></i> Back</a>
       </div>

     </div>

     <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row">

        <div class="col-md-2 ">
           <label  class="control-label">Date<sup></sup></label>
            <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control input-sm" id="datepicker" name="ve_date" value=<?=$ve_date  ?> >
                </div>
              </div>

                <div class="col-md-2 ">
                <label  class="control-label">Voucher No.<sup></sup></label>
                  <input type="text" class="form-control input-sm" readonly name="ve_vno" value=<?=$ve_vno  ?> >
                </div>

                <div class="col-md-2 ">
                <label  class="control-label">Bill No.<sup></sup></label>

                  <input type="text" class="form-control input-sm" name="ve_bill_no" value=<?=$ve_bill_no  ?> >
                </div>

          <div class="col-md-3">
            <label  class="control-label">Category<sup></sup></label>
            <select class="form-control input-sm"   name="ve_customer"> 
              <option value="Select">--Select--</option>
              <?php foreach ($categories as $key => $excategory) {
                ?>
              <option value="<?php echo $excategory['id']?>"<?php if($excategory['id']==$ve_customer) { ?> selected="selected" <?php } ?>><?php echo $excategory['category']?></option>
                <?php
              }?>
            </select>               
          </div>


          <div class="col-md-2 ">
            <label  class="control-label">Amount<sup></sup></label>
            <input class="form-control validate[required] input-sm" type="text" name="ve_apayable" value=<?=$ve_apayable  ?>>
             <input class="form-control"  type="hidden" name="ve_id" value="<?=$ve_id ?>">           
          </div>


        </div>
      </div>
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            <div>
              <input class="btn-large btn-primary btn" type="submit" value="Submit" name="submit" accesskey="s" title="short key-ALT+S"> 
              <input class="btn-large btn-default btn" type="reset" value="Reset">
            </div>
          </div>

        </div>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</section>
 <br><br><br><br><br><br><br> 
</section><!-- /.right-side -->

<script>
 $('#datepicker').datepicker({
      autoclose: true
    });</script>