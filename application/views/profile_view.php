
<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
 
  <section class="right-side" style="min-height:700px;">
    <section class="content-header">
        <h1>
            Profile
            <small>User Profile</small>
        </h1>

        <ol class="breadcrumb">
            <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="">Profile</a></li>
            <li class="active">User Profile</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
                <div class="box-header">
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
              <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
     <?php } ?>
                    
               
 
            
          <!--  <div class="box-body">
            <div class="row">
                <div class="col-md-6 ">
                    <div class="form-group required">
                  <label for="category" class="control-label">Name<sup></sup></label>
                  <input class="form-control" required="required"  type="text" name="name" value="<?php echo $update_users['user_name'];?>">                
                         
              </div>
                  
                  <label for="category" class="control-label">Email<sup></sup></label>
                        <input class="form-control" required="required"  type="text" name="email" value="<?php echo $update_users['user_email'];?>">                
                                        
               
              <div class="form-group required">
                  <label for="category" class="control-label">Date<sup></sup></label>
                        <input class="form-control" required="required"  type="text" name="date" value="<?php echo $update_users['user_date'];?> ">                
              </div>
          </div>
      </div>
  </div> -->
<section>
 
        <div class="col-xs-12">

          <h2 class="page-header">

            <i class="fa fa-th"></i> Profile
            <small class="pull-right"><?php echo $update_users['user_date'];?></small>
          </h2>
        </div>
        <!-- /.col -->
     
 <div class="col-xs-12">
        <div class="col-sm-4 invoice-col">
          Name
          <address>
            <strong><?php echo $update_users['user_name'];?></strong>
             
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          Email
          <address>
            <strong><?php echo $update_users['user_email'];?></strong>
            
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Priviliges</b><br>
          <br>
          <b>Order ID:</b> 4F3S8J<br>
          <b>Payment Due:</b> 2/22/2014<br>
          <b>Account:</b> 968-34567
        </div>
        <!-- /.col -->
      </div>
         
 
</div>
</div>
 </section>
 </section><!-- /.right-side -->


