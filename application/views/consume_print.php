<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
 <body onload="window.print();" >
  <?php foreach($company as $company) {
    $company_name     = $company['company'];
    $company_lic      = $company['licence_no'];
    $company_address  = $company['address'];
    $company_street   = $company['street'];
    $company_city     = $company['city'];
    $company_state    = $company['state'];
    $company_country  = $company['country'];
    $company_zip      = $company['zip'];
    $company_phone    = $company['phone'];
    $company_email    = $company['email'];
    $company_fax      = $company['fax'];
    $company_website  = $company['website'];

   }?>

     <?php
foreach($consumes as $consumes)
{
  $consumption_id = $consumes['con_id'];
  $bill_number    = $consumes['bill_number'];
  $bill_date      = $consumes['consumption_date'];
}

?>

<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i> <?= $company_name ?>
          <small class="pull-right">Consume Date: <?= $bill_date?></small>
        </h2>
      </div>


      <div class="dontprint">
     <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/consume/add">
        <i class="fa fa-plus-circle"></i> Create New
      </a>

    </div> </div>

    
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        <address>
          <strong><?= $company_name ?></strong><br>
          <strong>Lic No.:<?= $company_lic ?></strong><br>
          <?= $company_address.", ".$company_street  ?><br>
          <?= $company_city.", ".$company_state.", ".$company_country." - ".$company_zip ?> <br>
          <b>Phone:</b> <?= $company_phone ?>, Fax:  <?= $company_fax ?><br>
          <b>Email:</b> <?= $company_email ?><br>
          <b>Website:</b> <?= $company_website ?>
        </address>
        </div>
      <!-- /.col -->
      <div class="col-sm-6 invoice-col">        <br>
        
      </div>
      <div class="col-sm-2 invoice-col">
        <b>Consume No. : <?= $bill_number ?></b><br>
        <br>
        
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
          <tr>
            <tr><th colspan=3 style="text-align:center">Consumption</th></tr>
            <th>Sl.No</th>
            <th>Product</th>
            <th>Qty</th>
          </tr>
          </thead>
          <tbody> <?php
            $slno=1;
                  foreach($consume as $consumes)
                  { ?> 
                    <tr>
                      <td align="center"><?= $slno++  ?></td>
                      <td><?=$consumes['consume_item'] ?></td>
                      <td><?=$consumes['consume_item_qty']." ".$consumes['consume_item_unit'] ?></td>
                    </tr>
                    <?php } ?>

          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
      <center> THANK YOU VISIT AGAIN</center>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
 
<style type="text/css" media="print">
.dontprint
{ display: none; }
</style>
