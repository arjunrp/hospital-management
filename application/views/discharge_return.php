<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
<?php
foreach ($discharges as $discharge) {

  $ve_vno         = $discharge['ve_vno'];
  $ve_date        = $discharge['ve_date'];
  $ve_customer    = $discharge['ve_customer'];
  $ve_mrd         = $discharge['ve_mrd'];
  $p_name         = $discharge['p_title']." ".$discharge['p_name'];
  $p_phone        = $discharge['p_phone'];
  $u_name         = $discharge['u_name'];
  $dp_department  = $discharge['dp_department'];
  $ve_user        = $discharge['ve_user'];
  $ve_amount      = $discharge['ve_amount'];
  $ve_gtotal      = $discharge['ve_gtotal'];
  $ve_discount    = $discharge['ve_discount'];
  $ve_discounta   = $discharge['ve_discounta'];
  $ve_apayable    = $discharge['ve_apayable'];
  $ve_round       = $discharge['ve_round'];
}
 ?>

  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       Discharge
       <small>Return</small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."discharge"; ?>">Discharge</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-success pull-right  view-btn-create" href="<?php echo base_url();?>index.php/discharge">
        <i class="fa fa-mail-reply-all"></i> Back
      </a>
    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>

      <form id="post_form">
     <div class="box-body">
      <div class="row"> 
       <div class="col-md-12">
        <div class="col-md-3">
          <label  class="control-label">Voucher No. : <sup></sup></label> <?= $bill_no?>  
          <input type="hidden" name="ve_bill_no" value="<?= $ve_vno?>">
          <input type="hidden" name="ve_vno" value="<?= $bill_no?>">
          <input type="hidden" name="ve_date" value="<?= $ve_date?>">
          <input type="hidden" name="ve_customer" value="<?= $ve_customer?>">
          <input type="hidden" name="ve_mrd" value="<?= $ve_mrd?>">
          <input type="hidden" name="apayable" value="<?= $ve_amount?>">
          <input type="hidden" name="discountp" value="<?= $ve_discount?>">
          <input type="hidden" name="discounta" value="<?= $ve_discounta?>">
          <input type="hidden" name="sum" value="<?= $ve_apayable?>">
          <input type="hidden" name="sum2" value="<?= $ve_gtotal?>">
          <input type="hidden" name="roundoff" value="<?= $ve_round?>">
          <input type="hidden" name="user_type" value="<?= $ve_user?>">
        </div>
        <div class="col-md-3">
          <label  class="control-label">Bill No. : <sup></sup></label> <?= $ve_vno?>             
        </div>
        <div class="col-md-3">
          <label  class="control-label">Date : <sup></sup></label> <?= date("d-m-Y h:i A",strtotime($ve_date)) ?>               
        </div>
         
          <div class="col-md-3" >
         <label  class="control-label">MRD : <sup></sup></label> <?= $ve_mrd?>    
               </div>
     </div>
     <div class="col-md-12">
       <div class="col-md-3">
         <label  class="control-label">Patient's Name : <sup></sup></label> <?= $p_name?>    
       </div>

       <div class="col-md-3">
         <label  class="control-label">Phone No. : <sup></sup></label> <?= $p_phone?>     
       </div>

      <div class="col-md-3">
         <label  class="control-label">IP No. : <sup></sup></label> <?= $ve_customer?>  
       </div>
         <div class="col-md-3">
         <label  class="control-label">Department : <sup></sup></label> <?= $dp_department?>    
       </div>
        <div class="col-md-3">
         <label  class="control-label">Doctor : <sup></sup></label> <?= $u_name?>      
       </div>
       <div class="col-md-2">
         <label  class="control-label">User Id : <sup></sup></label> <?= $ve_user?>      
       </div>
     </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-bordered table-hover gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Sl No.</th>
                        <th colspan="8">Particulars</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                  <tbody> 

                    <?php $slno = 1; $grand_total=0; if($rooms) $slno1 =1; { ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Rooms</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th>Rooms</th><th>Admission Date</th><th>Vacated Date</th><th>Total Days</th><th>Rent</th><th>Nursing Charge</th><th>House Keeping Charge</th><th>Credit Amount</th></tr>
                    <?php foreach ($rooms as $room) {
                      // $date1=date_create($room['rs_adm']);
                      // $date2=date_create($room['rs_dis']);
                      // $d1= new DateTime($room['rs_adm']); 
                      // $d2= new DateTime($room['rs_dis']);
                      // $interval= $d1->diff($d2);
                      // $date_diff = ($interval->days * 24) + $interval->h;

                      // $date_diff = date_diff($date1,$date2);
                      // $date_diff = $date_diff->format("%a")+1;

                      $diff       = date_create($room['rs_adm'])->diff(date_create($room['rs_dis']));
                      // $date_diff  =  $diff->format("%a days\n%h hours\n%i minutes\n%s seconds\n");
                      $days       = $diff->format("%a");
                      $hours      = $diff->format("%h");
                      $minutes    = $diff->format("%i");

                      if($hours>6)
                      {
                        $date_diff = $days + 1;
                      }
                      else if($hours == 6 && $minutes > 0 )
                      {
                        $date_diff = $days + 1;
                      }
                      else  if($hours <= 6 && $hours > 0 )
                      {
                        $date_diff = $days + .5;
                      }
                      else
                      {
                        $date_diff = $days;
                      }

                      $total = ($room['rs_rt'] + $room['rs_nc'] + $room['rs_hc']) * $room['rs_days'];
                      $grand_total = $grand_total + $total;
                      ?> <tr><td></td><td><?=$slno1?></td><td><?=$room['rm_no']?></td><td><?=$room['rs_adm']?></td><td><?=$room['rs_dis']?></td><td><?=$room['rs_days']?></td><td><?=$room['rs_rt']?></td><td><?=$room['rs_nc']?></td><td><?=$room['rs_hc']?></td><td><?=$total?></td></tr> <?php
                    $slno1++;  } $slno++; ?>

                    <?php } ?>


                    <?php if($voucher_bills) { $slno3 =1;  ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Lab</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Credit Amount</th></tr>
                    <?php foreach ($voucher_bills as $voucher_bill) {
                      $grand_total = $grand_total + $voucher_bill['ve_apayable'];
                      ?> <tr><td></td><td><?=$slno3?></td><td colspan="3"><?=$voucher_bill['ve_vno']?></td><td colspan="4"><?=$voucher_bill['ve_date']?></td><td><?=$voucher_bill['ve_apayable']?></td></tr>
                       <?php
                    $slno3++;  } $slno++;  ?>

                    <?php } ?>

                     <?php if($scanning_bills) { $slno7 =1; ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Scanning</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Credit Amount</th></tr>
                    <?php foreach ($scanning_bills as $scanning_bill) {
                      $grand_total = $grand_total + $scanning_bill['ve_apayable'];
                      ?> <tr><td></td><td><?=$slno7?></td><td colspan="3"><?=$scanning_bill['ve_vno']?></td><td colspan="4"><?=$scanning_bill['ve_date']?></td><td><?=$scanning_bill['ve_apayable']?></td></tr>
                       <?php
                    $slno7++;  } $slno++;  ?>

                    <?php } ?>

                    <?php if($xray_bills) { $slno8 =1; ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Xray</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Credit Amount</th></tr>
                    <?php foreach ($xray_bills as $xray_bill) {
                      $grand_total = $grand_total + $xray_bill['ve_apayable'];
                      ?> <tr><td></td><td><?=$slno8?></td><td colspan="3"><?=$xray_bill['ve_vno']?></td><td colspan="4"><?=$xray_bill['ve_date']?></td><td><?=$xray_bill['ve_apayable']?></td></tr>
                       <?php
                    $slno8++;  } $slno++;  ?>

                    <?php } ?>

                    <?php if($pharmacy_bills) { $slno2 =1; ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Pharmacy</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Credit Amount</th></tr>
                    <?php foreach ($pharmacy_bills as $pharmacy_bill) {
                      $grand_total = $grand_total + $pharmacy_bill['ve_apayable'];
                      ?> <tr><td></td><td><?=$slno2?></td><td colspan="3"><?=$pharmacy_bill['ve_vno']?></td><td colspan="4"><?=$pharmacy_bill['ve_date']?></td><td><?=$pharmacy_bill['ve_apayable']?></td></tr>
                       <?php
                    $slno2++;  } $slno++;  ?>

                    <?php } ?>

                    <?php if($pharmacy_returns) { $slno4 =1; ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Medicine Return</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Debit Amount</th></tr>
                    <?php foreach ($pharmacy_returns as $pharmacy_return) {
                      $grand_total = $grand_total - $pharmacy_return['ve_apayable'];
                      ?> <tr><td></td><td><?=$slno4?></td><td colspan="3"><?=$pharmacy_return['ve_vno']?></td><td colspan="4"><?=$pharmacy_return['ve_date']?></td><td><?=$pharmacy_return['ve_apayable']?></td></tr>
                       <?php
                    $slno4++;  } $slno++;  ?>

                    <?php } ?>

                    <?php if($advances) { $total_advance=0; $slno5 =1; ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Advance Payment</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Debit Amount</th></tr>
                    <?php foreach ($advances as $advance) {
                      $total_advance = $total_advance + $advance['ve_apayable'];

                      ?> <tr><td></td><td><?=$slno5?></td><td colspan="3"><?=$advance['ve_vno']?></td><td colspan="4"><?=$advance['ve_date']?></td><td><?=$advance['ve_apayable']?></td></tr>
                       <?php
                    $slno5++;  } $slno++;  ?>

                    <?php } ?>

                    <?php if($additionals) { $slno6 =1; ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Others</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th colspan="4">Particulars</th><th colspan="2">Price</th><th colspan="1">Qty</th><th>Credit Amount</th></tr>
                    <?php foreach ($additionals as $additional) {
                      $grand_total = $grand_total + $additional['ved_gtotal'];
                      ?> <tr><td></td><td><?=$slno6?></td><td colspan="4"><input type="hidden" name="ved_itemid[]" value="<?=$additional['ved_itemid']?>"><input type="hidden" name="ved_item[]" value="<?=$additional['ved_item']?>"><?=$additional['ved_item']?></td>
                      <td colspan="2"><input type="hidden" name="ved_price[]" value="<?=$additional['ved_price']?>"><?=$additional['ved_price']?></td><td colspan="1"><input type="hidden" name="ved_qty[]" value="<?=$additional['ved_qty']?>"><?=$additional['ved_qty']?></td>
                      <td><input type="hidden" name="ved_gtotal[]" value="<?=$additional['ved_gtotal']?>"><?=$additional['ved_gtotal']?></td></tr>
                       <?php
                    $slno6++;  } $slno++;  ?>

                    <?php } ?>



                  </tbody>
                  </table>
                 <?php if($advances==0)  { $total_advance=0;  }

                  $total_amount = $grand_total - $total_advance;
                  $advance_amt  = $total_amount * $ve_discount/100;
                  $amt_payable  = $total_amount - $advance_amt;
                  ?>

                  <table class="font_reduce table table-bordered">
                    <tr><th width="60%"></th><th>Total</th>
                      <th>Rs. <label><?=$grand_total?></label></th></tr>
                      <tr><th width="60%"></th><th>Advance Paid</th>
                      <th>Rs. <label><?=$total_advance?></label></th></tr>
                      <tr><th width="60%"></th><th>Total Amount</th>
                      <th>Rs. <label><?=$total_amount?></label></th></tr>
                       <tr><th width="60%"></th><th>Discount</th><th>
                      Rs. <label><?=$advance_amt?></label> (<label><?= $ve_discount?></label> %)</th></tr>

                      <tr><th width="60%"></th><th>Amount Payable</th>
                      <th>Rs. <label><?=$amt_payable?></label></th></tr>
                      <tr><th width="60%"></th><th>Round Off</th>
                      <th>Rs. <label><?=$ve_round?></label></th></tr>
                    </table>
                  <a href="#" class="btn btn-success" onclick="save();" accesskey="s" title="short key-ALT+S"><i class="fa fa-floppy-o"></i> Save</a>
                  <input type="hidden" id="Printid"> 
                   <!--  <a href="#" class="btn btn-warning" onclick="save_print();" accesskey="p" title="short key-ALT+P" ><i class="fa fa-print"></i> Save & Print</a>   -->
                
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</form>
</div>
</section>

</section><!-- /.right-side -->
<script>
function save(){

      var postData = $("#post_form").serializeArray();
      var formURL  = "<?= base_url() ?>discharge/dischargeReturn";

      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          // alert("Success");
          $('#Printid').val(data);
           window.location = "<?php echo base_url();?>index.php/discharge/add";
        }
      });
}
function save_print(){

          
      var postData = $("#post_form").serializeArray();
      var formURL  = "<?= base_url() ?>discharge/dischargeReturn";

      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          alert(data);
           window.location = "<?php echo base_url();?>index.php/discharge/getPrint?Printid="+data;
        }
      });

}
</script>

<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });</script>
<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>