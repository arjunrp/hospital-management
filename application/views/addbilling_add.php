<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>


  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="<?= base_url() ?>/confirms/bootpopup.js"></script>

  <script> 
$(document).ready(function() {
  $('#ve_mrd').change(function(){
    var uRL1   = "<?= base_url() ?>opregister/get_mrd/"+$(this).val();
      $.ajax({

        url : uRL1,
        dataType: "json",
        success:function(data3)
        {
           $.each(data3, function(index) {
          $("#bk_name").val(data3[index].p_name);
          $("#bk_phone").val(data3[index].p_phone);
          });
        }

      });

  });

$('#bk_phone').change(function(){

    var uRL1   = "<?= base_url() ?>opregister/get_phone/"+$(this).val();
      $.ajax({

        url : uRL1,
        dataType: "json",
        success:function(data3)
        {
           $.each(data3, function(index) {
           $("#bk_name").val(data3[index].p_name);
           $("#ve_mrd").val(data3[index].p_mrd_no);
          });
        }
      });
    });

});
  </script>
  
   <script type="text/javascript" charset="utf-8">

  $(function() {
    $("#product").autocomplete({
      source: [<?php
      $i=0;
      foreach ($additional_bills as $additional_bill){
        if ($i>0) {echo ",";}
        echo '{value:"' . $additional_bill['ab_name'] . '",price:"' . $additional_bill['ab_amount'] . '",productid:"' . $additional_bill['ab_id'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          $("#productid").val(ui.item ? ui.item.productid : '');
          $("#product_price").val(ui.item ? ui.item.price : '');
        }
      });   
  });

  </script>
  <script> 
$(document).ready(function() {

  $('#ip_doctor').change(function(){

    var doctor =$('#ip_doctor option:selected').text();
    $('#ve_doctor_id').val(doctor);

      var uRL1   = "<?= base_url() ?>ipregister/getDepartment/"+$(this).val();
      $.ajax({
        url : uRL1,
        dataType: "json",
        success:function(data3)
        {
           $.each(data3, function(index) {
        $("#ip_department_id").val(data3[index].dp_id);
        $("#ip_department").val(data3[index].dp_department);
      });  
         }
       });
    });

  var slno5 = 1;
   $('.button').click(function() {
    var productid       = $('#productid').val();
    var product         = $('#product').val();
    var product_price   = $('#product_price').val();
    var qty             = "1";
    var total           = parseFloat(product_price) * parseFloat(qty);

    var newrow      = '<tr><td><b>'+slno5+'</b></td><td><input type="hidden" value="'+productid+'" name="ved_itemid[]"><input class="form-control input-sm" type="hidden" value="'+product+'" name="ved_item[]"><b>' + product + '</b></td><td><input type="hidden" value="'+product_price+'" name="ved_price[]"><input type="hidden" value="'+qty+'" name="ved_qty[]"><input class="gtotal3" type="hidden" value="'+total+'" name="ved_total[]"><b>Rs. </b><label>'+total+'</label></td><td><button class="btn btn-xs btn-danger dlte"><i class="fa fa-times-circle-o"></i></button></td></tr>';
    slno5++;

    var lab_mrp     = $('#lab_mrp').val();

    lab_mrp         = parseFloat(lab_mrp) + parseFloat(total);
    lab_mrp         = (Math.round(lab_mrp * 100) / 100).toFixed(2);

    $('#lab_mrp').val(lab_mrp);
    $('#lab_mrp1').html(lab_mrp);

    var pDiscount         =  $('#discountp').val();

    var pDiscount1        =  parseFloat(lab_mrp) * parseFloat(pDiscount)/100;
    var sum               =  parseFloat(lab_mrp) - parseFloat(pDiscount1);
    sum                   =  (Math.round(sum * 100) / 100).toFixed(2);

    pDiscount1            =  (Math.round(pDiscount1 * 100) / 100).toFixed(2);

    gtotal = Math.round(sum);

    $('#discountp').val(pDiscount);
    $('#discountp1').html(pDiscount);
    $('#discounta').val(pDiscount1);
    $('#discounta1').html(pDiscount1);

    $('#sum').val(gtotal);
    $('#sum1').html(gtotal);

    var roundoff    =   Math.round(sum) - parseFloat(sum);
    roundoff       =  (Math.round(roundoff * 100) / 100).toFixed(2);
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);

    $('#item_table tr:last').after(newrow);

    document.getElementById('productid').value = "";
    document.getElementById('product').value = "";
    document.getElementById('product_price').value = "";

});

  });
  </script>

  <script>
  $(document).ready(function() {

  
});
    </script>

  <script>
  $(document).ready(function() {

    var gtotal3 = 0;

$(document).on('click','button.dlte', function() {
    var $row    = $(this).closest("tr");    // Find the row

    var r = confirm("Are you sure want to delete!");
    if (r == true) {
    $(this).closest('tr').remove();

    $('.gtotal3').each(function(){
        gtotal3 += parseFloat(this.value);
    });

    lab_mrp        =  (Math.round(gtotal3 * 100) / 100).toFixed(2);

    $('#lab_mrp').val(lab_mrp);
    $('#lab_mrp1').html(lab_mrp);

    var pDiscount         =  $('#discountp').val();

    var pDiscount1        =  parseFloat(lab_mrp) * parseFloat(pDiscount)/100;

    var sum               =  parseFloat(lab_mrp) - parseFloat(pDiscount1);
    sum                   =  (Math.round(sum * 100) / 100).toFixed(2);

    pDiscount1            =  (Math.round(pDiscount1 * 100) / 100).toFixed(2);

    grand_total = Math.round(sum);

    $('#discountp').val(pDiscount);
    $('#discountp1').html(pDiscount);
    $('#discounta').val(pDiscount1);
    $('#discounta1').html(pDiscount1);

    $('#sum').val(grand_total);
    $('#sum1').html(grand_total);

    var roundoff   =   Math.round(sum) - parseFloat(sum);
    roundoff       =  (Math.round(roundoff * 100) / 100).toFixed(2);
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);
    } 
    });


  });
  </script>


  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#customer_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>

  <!-- Discount Add -->
  <script>
  $(document).ready(function() {
    $( "#dialog" ).dialog({
        modal: true,
        bgiframe: true,
        autoOpen: false,
      show: {
        effect: "blind",
        duration: 100
      },
      hide: {
        effect: "explode",
        duration: 500
      }
    });

  $('.alldisc').click(function() {
      // $( "#dialog" ).dialog( "open" );
      var user_pswd         = $('#user_pswd').val();
      var user_pswd         = ""+user_pswd+"";
      bootpopup.prompt("Password","password",
    function(data) { if(JSON.stringify(data)==JSON.stringify(user_pswd)){
      
    var sum               =  0;
    var pDiscount         =  $('#pDiscount').val();
    var lab_mrp           =  $('#lab_mrp').val();

    var pDiscount1        =  parseFloat(lab_mrp) * parseFloat(pDiscount)/100;
    var sum               =  parseFloat(lab_mrp) - parseFloat(pDiscount1);
    sum                   =  (Math.round(sum * 100) / 100).toFixed(2);

    pDiscount1            =   (Math.round(pDiscount1 * 100) / 100).toFixed(2);

    gtotal = Math.round(sum);

    $('#discountp').val(pDiscount);
    $('#discountp1').html(pDiscount);
    $('#discounta').val(pDiscount1);
    $('#discounta1').html(pDiscount1);

    $('#sum').val(gtotal);
    $('#sum1').html(gtotal);

    var roundoff    =   Math.round(sum) - parseFloat(sum);
    roundoff        =   (Math.round(roundoff * 100) / 100).toFixed(2);
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);

    } 
else
{
  alert("Invalid Password!");
}
  }
);
  });
  });
  </script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <div id="dialog" title="Confirm Password">
    <input type="password" size="25" />
    </div>
   <input stle="text-color:#000" type="hidden" value="<?=$admin_pswd?>" id="user_pswd">
    <section class="content-header">
      <h1>
       Additional Billing
       <small>New </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."addbilling/add"; ?>">Additional Billing</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div class="box-tools">
     <!--  <a class="btn btn-sm btn-success pull-right  view-btn-create" href="<?php echo base_url();?>index.php/addbilling">
        <i class="fa fa-reply-all"></i> Back
      </a> -->
    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>

      <form id="post_form">
     <div class="box-body">
      <div class="row"> 
       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">Voucher No.<sup></sup></label>
          <input class="form-control input-sm" readonly type="text" id="ve_vno" name="ve_vno" tabindex="1" value="<?=$bill_no?>"> 
          <input class="form-control" type="hidden" name="user_type" value="<?=$user_type?>">              
        </div>
        <div class="col-md-2">
          <label  class="control-label">Date<sup></sup></label>
          <input class="form-control validate[required] input-sm"  tabindex="2" data-prompt-position="topRight:150" type="text" name="ve_date" id="datepicker" value="<?=date("Y-m-d") ?>">                
        </div>
        <div class="col-md-4"></div>
         <div class="col-md-2">
         
         <input class="form-control input-sm" data-prompt-position="topLeft:190" tabindex="5" type="hidden" name="ve_customer" id="ve_customer"></div>

          <div class="col-md-2">
         <label  class="control-label">MRD<sup></sup></label>
         <input class="form-control validate[required] input-sm" data-prompt-position="topLeft:190" tabindex="5" type="text" name="ve_mrd" id="ve_mrd"></div>
               </div>
      <div class="col-md-12">
      <br>
       <div class="col-md-4">
         <label  class="control-label">Patient's Name<sup></sup></label>
         <input class="form-control validate[required] input-sm"  data-prompt-position="bottomLeft:190"  tabindex="6" type="text" id="bk_name" name="ve_patient">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Phone No.<sup></sup></label>
         <input class="form-control validate[required] input-sm" data-prompt-position="topLeft:190" tabindex="7" type="text" id="bk_phone" name="ve_phone">
       </div>
       <div class="col-md-2"></div>
         <div class="col-md-2">
         <label  class="control-label">Doctor<sup></sup></label>
         <select class="form-control input-sm" id="ip_doctor" tabindex="7">
          <option value="">--Select Doctor--</option>
          <?php foreach ($doctors as $key => $doctor) {
            ?> <option value="<?=$doctor['u_emp_id'] ?>"><?=$doctor['u_name'] ?></option> <?php 
          } ?>
         </select> 
          <input type="hidden" id="ve_doctor_id" name="ve_doctor"> 
       </div>
         <div class="col-md-2">
         <label  class="control-label">Department<sup></sup></label>
         <input class="form-control input-sm validate[required]" readonly data-prompt-position="bottomLeft:190" type="text" id="ip_department">
         <input type="hidden" id="ip_department_id" name="ve_supplier"> 
       </div>
     </div>
     <div class="col-md-12">
      <br>
      <div class="col-md-4">
        <div class="form-group required">
          <label  class="control-label">Item<sup></sup></label>
          <input class="form-control input-sm" type="text" id="product">
          <input class="form-control" type="hidden" name="productid" id="productid">
        </div> </div>
        
          <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Unit Price<sup></sup></label>
              <input class="form-control input-sm" id="product_price"  type="text" name="product_price">  
               </div> </div>

            </div>
          </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-md-12">
                <div>
                  <a class="button btn btn-primary" >Add to List</a>
                  <input class="btn-large btn-default btn"  tabindex="9" type="reset" value="Reset">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                      <tr>
                        <td><b>DISCOUNT(%)</b></td>
                        <td>
                          <input type="text" name="pDiscount" required="required" id="pDiscount" class="form-control" value="0" />
                        </td>          
                        <td><button class="btn btn-info alldisc" type="button">Add</button></td>
                      </tr>
                  </table>


                    <table class="table table-striped table-bordered table-hover gridView" id="item_table">
                    <thead>
                      <tr>
                        <th width="10%">Sl No.</th>
                        <th width="65%">Particulars</th>
                        <th width="20%">Amount</th>
                        <th width="5%">Delete</th>
                      </tr>
                    </thead>
                  <tbody> 

                  </tbody>
                  </table> 
                  <table class="table table-bordered">
                    <tr><th width="60%"></th><th>Total</th>
                      <th><input type="hidden" name="lab_mrp" readonly id="lab_mrp" value="0">Rs. <label id="lab_mrp1">0</label>
                      </th></tr>
                       <tr><th width="60%"></th><th>Discount</th><th>
                      <input type="hidden" readonly name="discountp" id="discountp" value="0"><input type="hidden" readonly name="discounta" id="discounta" value="0">Rs. <label id="discounta1"></label> (<label id="discountp1">0</label> %)</th></tr>
                      <tr><th width="60%"></th><th>Amount Payable</th>
                      <th><input type="hidden" name="sum" readonly id="sum" value="0">Rs. <label id="sum1">0</label></th></tr>
                      <tr><th width="60%"></th><th>Round Off</th>
                      <th><input type="hidden" name="roundoff" readonly id="roundoff" value="0">Rs. <label id="roundoff1">0</label></th></tr>
                    </table>

                    <a href="#" class="btn btn-success" onclick="save();" accesskey="s" title="short key-ALT+S"><i class="fa fa-floppy-o"></i> Save</a>
                  <input type="hidden" id="Printid"> 
                    <a href="#" class="btn btn-warning" onclick="save_print();" accesskey="p" title="short key-ALT+P" ><i class="fa fa-print"></i> Save & Print</a> 
                
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</form>
</div>
</section>

</section><!-- /.right-side -->

<script>
function save(){
      var ip_doctor = $("#ip_doctor").val();
      var postData = $("#post_form").serializeArray();
      var formURL  = "<?= base_url() ?>addbilling/addbillingAdd";
      if(ip_doctor=="")
      {
        alert("Select Doctor");
      }
      else
      {
        $.ajax(
        {
          url : formURL,
          type: "POST",
          data : postData,
          success:function(data, textStatus, jqXHR)
          {
            // alert("Success");
            $('#Printid').val(data);
            window.location = "<?php echo base_url();?>index.php/addbilling/add";
          }
        });
      }
}
function save_print(){

      var ip_doctor = $("#ip_doctor").val();    
      var postData = $("#post_form").serializeArray();
      var formURL  = "<?= base_url() ?>addbilling/addbillingAdd";
      if(ip_doctor=="")
      {
        alert("Select Doctor");
      }
      else
      {
        $.ajax(
        {
          url : formURL,
          type: "POST",
          data : postData,
          success:function(data, textStatus, jqXHR)
          {
            alert(data);
            window.location = "<?php echo base_url();?>index.php/addbilling/getPrint?Printid="+data;
          }
        });
      }

}
</script>

<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });</script>
<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>