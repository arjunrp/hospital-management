
<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<html>
<head>
<link rel="stylesheet" href='<?= base_url() ?>asset/plugins/jvectormap/jquery-jvectormap-1.2.2.css' type="text/css"/>
  <link media="all" type="text/css" rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.css">
   <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.min.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.min.css' type="text/css"/>
   <link rel="stylesheet" href="<?= base_url() ?>asset/css/skins/_all-skins.min.css"> 
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
 
  <!-- <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datepicker/datepicker3.css"> -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<!-- <link rel="stylesheet" href="<?= base_url() ?>asset/css/AdminLTE.min.css" type="text/css"/>  -->
   <link rel="stylesheet" href="<?= base_url() ?>asset/bootstrap/css/AdminLTE.min.css" type="text/css"/>
   <script  src="<?= base_url() ?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
   <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
   <style type="text/css">
   .font_reduce
   {
     font-size:small;
   }
   .font_reduce1
   {
     font-size:x-small;
   }
   .font_reduce2
   {
     font-size:xx-small;
   }
   .font_reduce3
   {
     font-size:medium;
   }

   .u {
    text-decoration: underline;
}
   </style>
</head>

 <body onload="window.print();" style="margin-top:0%;display:block;height:100%;width:80%;margin-left:9%;margin-right:6% ">

<!-- onload="window.print();"  -->

<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
            
        <hr>
         <center class="font_reduce pull-center"> <b>CASH REPORT - FROM <?=$from_date ?> TO <?=$to_date ?></b></center><br>
         <center class="font_reduce pull-center"> <b><u> <?=$dept ?> Department</u></b></center>
  <div class="box-tools">
              <a title="Back" class="dontprint btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."ccounter/ccounterreport"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>
    </div> </div>
    <!-- info row -->

     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">
            <br>
             <?php if($type=="d") { ?>
             <table class="table table-condensed dataTable no-footer gridView font_reduce2" id="item_table">
                    <thead>
                      <tr>
                        <th>Sl. No </th>
                        <th>Bill Date</th>
                        <th>Bill No</th>
                        <th>MRD</th>
                        <th>Patient Name</th>
                        <th>Patient Ph</th>
                        <th>Payment Date</th>
                        <th>Department</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                  <tbody>
                   <?php
                  $slno=1;
                  $total_amount = 0;
                  if($treports!=0) {
                  foreach($treports as $treport)
                  { 
                    $total_amount = $total_amount + $treport['ve_apaid'];
                    if($treport['ve_type'] == "sr")
                    {
                      $section = " - Sales Return";
                    }
                    else if($treport['ve_type'] == "ad")
                    {
                      $section = " - Advance Payment";
                    }
                    else if($treport['ve_type'] == "adr")
                    {
                      $section = " - Advance Return";
                    }
                    else if($treport['ve_type'] == "dis")
                    {
                      $section = " - Discharge";
                    }
                     else if($treport['ve_type'] == "dsr")
                    {
                      $section = " - Discharge Return";
                    }
                    else
                    {
                      $section = "";
                    }
                    ?> 
                    <tr>
                      <td><?= $slno++  ?></td>
                      <td><?=date("d-m-Y",strtotime($treport['ve_cash_date'])) ?></td>
                       <td><?=$treport['ve_vno'] ?></td>
                       <td><?=$treport['ve_mrd'] ?></td>
                       <td><?=$treport['ve_patient'] ?></td>
                       <td><?=$treport['ve_phone'] ?></td>
                      <td><?=date("d-m-Y",strtotime($treport['ve_cash_date'])) ?></td>
                      <td><?=$treport['dp_department'].$section ?></td>
                      <td><?="Rs. ".$treport['ve_apaid'] ?></td>
                    </tr>
                    <?php } }  ?>

                  </tbody>
                  </table> 
                                    
                  <table class="table font_reduce2">
                    <tr><th width="68%"></th><th>Total</th>
                    <th><?="Rs. ".$total_amount?></th></tr>

                    </table>
                    <?php } ?>
                    <?php if($type=="s") { ?>  
                    <table class="table table-condensed dataTable no-footer gridView item_table font_reduce2">
                    <thead>
                      <tr>
                        <th>Sl. No </th>
                        <th>Date</th>
                        <th>Department</th>
                        <th>Bill No From</th>
                        <th>Bill No To</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                  <tbody>
                  <?php
                  $slno=1;
                  $total_amount = 0;
                  $sect = 1;
                   $sumpharm  = 0;
                  $sumpharmr = 0;
                  $sumpharmb = 0;
                  $count = 1;
                  $count1 = 1;
                  $count2 = 1;
                  if($sreports!=0) {
                  foreach($sreports as $sreport)
                  { 
                    $total_amount = $total_amount + $sreport['apaid'];
                    ?> 
                    <tr>
                      <td><?= $slno++  ?></td>
                      <td><?=date("d-m-Y",strtotime($sreport['ve_cash_date'])) ?></td>
                      <td><?php if($sreport['ve_type'] != "opbl")
                          { echo $sreport['dp_department']; }
                          
                          if($sreport['ve_type'] == "si")
                          {
                            echo $sect++;
                          }
                          else if($sreport['ve_type'] == "so")
                          {
                            echo $sect++;
                          }
                          else if($sreport['ve_type'] == "sr")
                          {
                            echo " - Sales Return";
                          }
                          else if($sreport['ve_type'] == "opbl")
                          {
                            echo "OP Billing";
                          }
                          else if($sreport['ve_type'] == "ad")
                          {
                            echo " - Advance Payment";
                          }
                          else if($sreport['ve_type'] == "adr")
                          {
                            echo " - Advance Return";
                          }
                          else if($sreport['ve_type'] == "dis")
                          {
                            echo " - Discharge";
                          }
                          else if($sreport['ve_type'] == "dsr")
                          {
                            echo " - Discharge Return";
                          }

                      ?></td>
                      <td><?=$sreport['mi_veno'] ?></td>
                      <td><?=$sreport['mx_veno'] ?></td>
                      <td><?="Rs. ".$sreport['apaid'] ?></td>
                      <?php if($sreport['ve_type'] == "si") { $count++;  ?><td><b><?php $sumpharm = $sumpharm + $sreport['apaid']; if($count>2) { echo"Rs. ".$sumpharm; } ?></b></td> <?php } ?>
                      <?php if($sreport['ve_type'] == "sr") { $count1++; ?><td><b><?php $sumpharmr = $sumpharmr + $sreport['apaid']; if($count1>2) { echo "Rs. ".$sumpharmr; } ?></b></td> <?php } ?>
                      <?php if($sreport['ve_type'] == "opbl") { $count2++; ?><td><b><?php $sumpharmb = $sumpharmb + $sreport['apaid']; if($count2>2) { echo "Rs. ".$sumpharmb; } ?></b></td> <?php } ?>
                    </tr>
                    <?php }  ?>

                  </tbody>
                  </table> 
                  <table class="table font_reduce2">
                    <tr><th width="60%"></th><th>Total</th>
                    <th><?="Rs. ".$total_amount?></th></tr>
                    </table>
                    <?php } } ?>

   </div>
      <!-- /.col -->

    <div class="row">

        <div class="col-xs-8 font_reduce2">
          Authorized Signature  with Seal<br><br><br><br><br><br><br>
      </div>
      <!-- /.col -->
    </div>

     </div>
       
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
 
 <style type="text/css" media="print">
.dontprint
{ display: none; }
</style>
</body>
</html>