<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<html>
<head>
<link rel="stylesheet" href='<?= base_url() ?>asset/plugins/jvectormap/jquery-jvectormap-1.2.2.css' type="text/css"/>
  <link media="all" type="text/css" rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.css">
   <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.min.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.min.css' type="text/css"/>
   <link rel="stylesheet" href="<?= base_url() ?>asset/css/skins/_all-skins.min.css"> 
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
 
  <!-- <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datepicker/datepicker3.css"> -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<!-- <link rel="stylesheet" href="<?= base_url() ?>asset/css/AdminLTE.min.css" type="text/css"/>  -->
   <link rel="stylesheet" href="<?= base_url() ?>asset/bootstrap/css/AdminLTE.min.css" type="text/css"/>
   <script  src="<?= base_url() ?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
   <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
   <style type="text/css">
   .font_reduce
   {
     font-size:small;
   }
   .font_reduce1
   {
     font-size:x-small;
   }
   </style>
</head>
 <body onload="window.print();" style="margin-top:0%;display:block;height:100%;width:80%;margin-left:9%;margin-right:6% ">

<!-- onload="window.print();"  -->
  <?php foreach($company as $company) {
    $company_name     = $company['company'];
    $company_address  = $company['address'];
    $company_street   = $company['street'];
    $company_city     = $company['ctname'];
    $company_state    = $company['stname'];
    $company_country  = $company['coname'];
    $company_zip      = $company['zip'];
    $company_phone    = $company['phone'];
    $company_email    = $company['email'];
    $company_website  = $company['website'];
    $company_gst_no   = $company['gst_no'];
    $company_drug_lic = $company['drug_lic'];
    $company_tags     = $company['tags'];
}
?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {

 oTable = $('#category_table').dataTable({
});
});
</script>


<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h4 style="border-bottom: thin dotted #000; font-family: Arial Black, Georgia, Serif; padding-bottom:5px">
          <center><b><?= strtoupper($company_name) ?></b></center>
                    </h4>

 <center><b><u>OP TOKEN</u></b></center>

  <div class="box-tools">
              <a title="Back" class="dontprint btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."opregister/add"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>
    </div> </div>
    <!-- info row -->

     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">
           <br>

           <table class="font_reduce1" width="100%">
            <tr>
            <th>Token No.   : <?= $op_opno?></th>
            <th colspan="3" style="text-align:right">Date        : <?= date("d-m-Y",strtotime($op_date))?></th>
            </tr>
            <tr><td><br></td></tr>
          <tr>
            <th>Doctor      : <?= $op_doctor?></th>
            <th>Department  : <?= $op_dept?></th>
            <th colspan="2" style="text-align:right">Shift       : <?php if($op_shift=="m") { echo "Morning"; } else { echo "Evening"; }?></th>
            </tr>
            <tr><td><br></td></tr>
          <tr>
            <th>MRD No      : <?= $p_mrd_no?></th>
            <th>Patient     : <?= $p_name?> </th>
            <th style="text-align:right">Age         : <?= $p_age?> </th>
            <th style="text-align:right">Gender      : <?php if($p_sex=="F") { echo "Female"; } else if($p_sex=="M") { echo "Male"; }else { echo "Other"; }?> </th>
          </tr>
           </table>

    </div>
      <!-- /.col -->

    <!-- /.row -->

    <!-- Table row -->
     </div>
       
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
 
 <style type="text/css" media="print">
.dontprint
{ display: none; }
</style>
</body>
</html>