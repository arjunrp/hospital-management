<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

   <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
   <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
  <script type="text/javascript" charset="utf-8">
  $(function()
  {
    $("#po_pono").autocomplete({
      source: [<?php
      $i=0;
      foreach ($porders as $porder){
        if ($i>0) {echo ",";}
        echo '{value:"' . $porder['po_pono'] . '",vid:"' . $porder['sp_id'] . '",vendorr:"' . $porder['sp_vendor'] . '",vendorph:"' . $porder['sp_phone'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          $("#vendorid").val(ui.item ? ui.item.vid : '');
          $("#vendor").val(ui.item ? ui.item.vendorr : '');
          $("#vendorph").val(ui.item ? ui.item.vendorph : '');
        }
      });   
  });  

  

  </script>


  <script type="text/javascript" charset="utf-8">
  $(function()
  {
    $("#product").autocomplete({
      source: [<?php
      $i=0;
      $unit="";
      foreach ($products as $product){

        if ($i>0) {echo ",";}
        echo '{value:"' .$product['pd_product'] . '",pid:"' . $product['pd_code'] . '",productname:"' . $product['pd_product'] . '",productunit:"' . $product['pd_unit'] . '",productsgst:"' . $product['pd_sgst'] . '",productcgst:"' . $product['pd_cgst'] . '",pd_qty:"' . $product['pd_qty'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){

          $("#productid").val(ui.item ? ui.item.pid : '');
          $("#product_name").val(ui.item ? ui.item.productname : '');
          $("#product_unit").val(ui.item ? ui.item.productunit : '');
          $("#pod_sgst").val(ui.item ? ui.item.productsgst : '');
          $("#pod_cgst").val(ui.item ? ui.item.productcgst : '');
          $("#pd_qty").val(ui.item ? ui.item.pd_qty : '');
        }
      });   
  });  
  </script>
  <script type="text/javascript" charset="utf-8">
  function calcu(p)
  {
    var grand = 0;
    var sgrand = 0;
    var sgsta1 = 0;
    var cgsta1 = 0;
    var pp = parseFloat($('#pod_price'+p).val());
    var qty = parseFloat($('#pod_qty'+p).val());
    var sgstp = parseFloat($('#pod_sgstp'+p).val());
    var cgstp = parseFloat($('#pod_cgstp'+p).val());

    var total = pp * qty;

    var sgsta       = (parseFloat(total) * parseFloat(sgstp))/100;
    var cgsta       = (parseFloat(total) * parseFloat(cgstp))/100;
    var pod_total   = parseFloat(total) + parseFloat(sgsta) + parseFloat(cgsta);
    // alert(sgstp+","+cgstp+","+sgsta+","+cgsta+","+pod_total);

    $('#pod_sgsta'+p).val((Math.round(sgsta * 100) / 100).toFixed(2));
    $('#pod_1sgsta'+p).html((Math.round(sgsta * 100) / 100).toFixed(2));
    $('#pod_cgsta'+p).val((Math.round(cgsta * 100) / 100).toFixed(2));
    $('#pod_1cgsta'+p).html((Math.round(cgsta * 100) / 100).toFixed(2));
    $('#pod_total'+p).val((Math.round(pod_total * 100) / 100).toFixed(2));
    $('#ftotal1'+p).html((Math.round(pod_total * 100) / 100).toFixed(2));
    $('#pod_stotal'+p).val((Math.round(total * 100) / 100).toFixed(2));

    $('.ftotal').each(function(){
        grand += parseFloat(this.value);
    });
    $('.stotal').each(function(){
        sgrand += parseFloat(this.value);
    });
    $('.cgsta').each(function(){
        cgsta1 += parseFloat(this.value);
    });
    $('.sgsta').each(function(){
        sgsta1 += parseFloat(this.value);
    });


    grand1   = (Math.round(grand * 100) / 100).toFixed(2);
    sgrand  = (Math.round(sgrand * 100) / 100).toFixed(2);
    cgsta1  = (Math.round(cgsta1 * 100) / 100).toFixed(2);
    sgsta1  = (Math.round(sgsta1 * 100) / 100).toFixed(2);

    var pDiscount   =  $('#pDiscount').val();
    var grand       =  parseFloat(grand1) - (parseFloat(grand1) * parseFloat(pDiscount))/100; 
    var pDiscount1  =  parseFloat(grand1) * parseFloat(pDiscount)/100;

    $('#discountp').val(pDiscount);
    $('#discountp1').html(pDiscount);
    $('#discounta').val(pDiscount1);
    $('#discounta1').html(pDiscount1);

    gtotal = Math.round(grand);

    $('#gtotal').val(grand);
    $('#gtotal1').html(grand);
    $('#apayable').val(gtotal);
    $('#apayable1').html(gtotal);
    $('#sum1').html(sgrand);
    $('#sum').val(sgrand);
    $('#vecgst1').html(cgsta1);
    $('#vecgst').val(cgsta1);
    $('#vesgst1').html(sgsta1);
    $('#vesgst').val(sgsta1);

    var roundoff    =   Math.round(grand) - parseFloat(grand);
    roundoff        =   (Math.round(roundoff * 100) / 100).toFixed(2);
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);

  }

  $(function() {
    $( "#purchase_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>

<!-- Product Delete -->
<script>
  $(document).ready(function() {
    setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);

    <?php
$js_array = json_encode($purchases);
echo "var purchases = ". $js_array . ";\n";
?>
var count = 1;
var sgstot = 0;
var cgstot = 0;
var sum = 0;
var differ = 0;
var po_amount = 0;
var sgstot = 0;
var cgstot = 0;

$.each(purchases, function(index) {
        // alert(sum+","+vesgst+","+vecgst+","+apayable);


         var newrow      = '<tr><td width="20%"><input type="hidden" value="'+purchases[index].ved_itemid+'" name="ved_itemid[]"><input class="form-control input-sm" type="hidden" value="'+purchases[index].ved_item+'" name="ved_item[]">' + purchases[index].ved_item + '</td><td width="8%"><input class="form-control input-sm" type="hidden" name="ved_batch[]" value="'+purchases[index].ved_batch+'">'+purchases[index].ved_batch+'</td><td width="9%"><input class="form-control input-sm" type="input" placeholder="yy-mm-dd" name="ved_expiry[]" value="'+purchases[index].ved_expiry+'"></td><td width="8%"><input class="form-control input-sm" type="input" value="'+purchases[index].ved_price+'" id="pod_price'+count+'" onkeyup="calcu('+count+')" name="ved_price[]"></td><td width="9%"><input class="form-control input-sm" type="input" value="'+purchases[index].ved_slprice+'" name="ved_slprice[]"></td><td width="6%"><input class="form-control input-sm" type="input" value="'+purchases[index].ved_qty/purchases[index].ved_uqty+'" id="pod_qty'+count+'" name="ved_qty[]" onkeyup="calcu('+count+')"></td><td width="5%"><input class="form-control input-sm" type="input" name="ved_free[]" value="'+purchases[index].ved_free/purchases[index].ved_uqty+'"><input class="form-control input-sm" type="hidden" name="ved_uqty[]" value="'+purchases[index].ved_uqty+'"></td><td width="8%"><input class="form-control input-sm" type="hidden" value="'+purchases[index].ved_unit+'" name="ved_unit[]">'+purchases[index].ved_unit+'</td><td width="8%"><input type="hidden" value="'+purchases[index].ved_cgstp+'" id="pod_cgstp'+count+'" name="ved_cgstp[]"><input class="cgsta" type="hidden" value="'+purchases[index].ved_cgsta+'" id="pod_cgsta'+count+'" name="ved_cgsta[]"><b>Rs. </b><label id="pod_1cgsta'+count+'">'+purchases[index].ved_cgsta+'</label> ('+purchases[index].ved_cgstp+'%)</td><td width="8%"><input type="hidden" value="'+purchases[index].ved_sgstp+'" id="pod_sgstp'+count+'" name="ved_sgstp[]"><input class="sgsta" type="hidden" value="'+purchases[index].ved_sgsta+'" id="pod_sgsta'+count+'" name="ved_sgsta[]"><b>Rs. </b><label id="pod_1sgsta'+count+'">'+purchases[index].ved_sgsta+'</label> ('+purchases[index].ved_sgstp+'%)</td><td width="8%"><input type="hidden" value="'+purchases[index].ved_gtotal+'" class="ftotal" id="pod_total'+count+'" name="ved_gtotal[]"><b>Rs. </b><label id="ftotal1'+count+'">'+purchases[index].ved_gtotal+'</label><input class="stotal" type="hidden" value="'+purchases[index].ved_total+'" id="pod_stotal'+count+'" name="ved_total[]"></td><td width="3%"><button class="btn btn-xs btn-danger dlte"><i class="fa fa-times-circle-o"></i></button></td></tr>';
        $('#item_table tr:last').after(newrow);
        count++;
      });


  $(document).on('click','button.dlte', function() {
    var $row    = $(this).closest("tr");    // Find the row


    var r = confirm("Are you sure want to delete!");
    if (r == true) {
    $(this).closest('tr').remove();

    var grand = 0;
    var sgrand = 0;
    var sgsta1 = 0;
    var cgsta1 = 0;
    var gtotal = 0;
    $('.ftotal').each(function(){
        grand += parseFloat(this.value);
    });
    $('.stotal').each(function(){
        sgrand += parseFloat(this.value);
    });
    $('.cgsta').each(function(){
        cgsta1 += parseFloat(this.value);
    });
    $('.sgsta').each(function(){
        sgsta1 += parseFloat(this.value);
    });
    grand           =   (Math.round(grand * 100) / 100).toFixed(2);
    sgrand          =   (Math.round(sgrand * 100) / 100).toFixed(2);
    cgsta1          =   (Math.round(cgsta1 * 100) / 100).toFixed(2);
    sgsta1          =   (Math.round(sgsta1 * 100) / 100).toFixed(2);

    var pDiscount   =  $('#pDiscount').val(); 
    var pDiscount1  =  parseFloat(grand) * parseFloat(pDiscount)/100;
    var grand       =  parseFloat(grand) - (parseFloat(grand) * parseFloat(pDiscount))/100;
    pDiscount1      =  (Math.round(pDiscount1 * 100) / 100).toFixed(2);
    grand           =  (Math.round(grand * 100) / 100).toFixed(2);

    $('#discountp').val(pDiscount);
    $('#discountp1').html(pDiscount);
    $('#discounta').val(pDiscount1);
    $('#discounta1').html(pDiscount1);

    gtotal = Math.round(grand);

    $('#gtotal').val(grand);
    $('#gtotal1').html(grand);

    $('#apayable').val(gtotal);
    $('#apayable1').html(gtotal);
    $('#sum1').html(sgrand);
    $('#sum').val(sgrand);
    $('#vecgst1').html(cgsta1);
    $('#vecgst').val(cgsta1);
    $('#vesgst1').html(sgsta1);
    $('#vesgst').val(sgsta1);

    var roundoff    =   Math.round(grand) - parseFloat(grand);
    roundoff        =   (Math.round(roundoff * 100) / 100).toFixed(2);
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);
    var apaid             =  $('#apaid').val();
    var balance = parseFloat(gtotal) - parseFloat(apaid);
    $('#balance').val(balance);
    $('#balance1').html(balance);
    } 
    });
  });
  </script>
<!-- Product Add -->
  <script>
  $(document).ready(function() {
  $('.button').click(function() {
    var item        = $('#product_name').val();
    var itemid      = $('#productid').val();
    var qty         = $('#product_quantity').val();
    var fqty        = $('#free_quantity').val();
    var pd_qty      = $('#pd_qty').val();
    var unt         = $('#product_unit').val();
    var batch       = $('#batch').val();
    var expiry      = $('#expiry').val();
    var price       = $('#product_price').val();
    var total       = $('#product_quantity').val() * $('#product_price').val();

    var sgstp       = $('#pod_sgst').val();
    var cgstp       = $('#pod_cgst').val();

    var sgsta       = (parseFloat(total) * parseFloat(sgstp))/100;
    var cgsta       = (parseFloat(total) * parseFloat(cgstp))/100;
    var pod_total   = parseFloat(total) + parseFloat(sgsta) + parseFloat(cgsta);
    
    var sgsta       = (Math.round(sgsta * 100) / 100).toFixed(2);
    var cgsta       = (Math.round(cgsta * 100) / 100).toFixed(2);
    var pod_total   = (Math.round(pod_total * 100) / 100).toFixed(2);

    if(unt == "Strip") { ved_unit = "Str"; }
    else if(unt == "Bottle") { ved_unit = "Bot"; }
    else if(unt == "Packet") { ved_unit = "Pkt"; }
    else if(unt == "No.s") { ved_unit = "No.s"; }

    // alert(unt);

    var newrow      = '<tr><td width="20%"><input type="hidden" value="'+itemid+'" name="ved_itemid[]"><input class="form-control input-sm" type="hidden" value="'+item+'" name="ved_item[]">' + item + '</td><td width="9%"><input class="form-control input-sm" type="hidden" value="'+batch+'" name="ved_batch[]">'+batch+'</td><td width="8%"><input class="form-control" value="'+expiry+'" type="hidden" name="ved_expiry[]">'+expiry+'</td><td width="9%"><input type="hidden" value="'+price+'" name="ved_price[]">'+price+'</td><td width="8%"><input type="hidden" value="'+qty+'" name="ved_qty[]">'+qty+'</td><td width="5%"><input class="form-control input-sm" type="hidden" value="'+fqty+'" name="ved_free[]">'+fqty+'<input class="form-control input-sm" type="hidden" value="'+pd_qty+'" name="ved_uqty[]"></td><td width="9%"><input type="hidden" value="'+ved_unit+'" name="ved_unit[]">'+unt+'</td><td width="9%"><input type="hidden" value="'+cgstp+'" name="ved_cgstp[]"><input class="cgsta" type="hidden" value="'+cgsta+'" name="ved_cgsta[]"><b>Rs. </b><label>'+cgsta+'</label> ('+cgstp+'%)</td><td width="9%"><input type="hidden" value="'+sgstp+'" name="ved_sgstp[]"><input class="sgsta" type="hidden" value="'+sgsta+'" name="ved_sgsta[]"><b>Rs. </b><label>'+sgsta+'</label> ('+sgstp+'%)</td><td width="9%"><input type="hidden" value="'+pod_total+'" class="ftotal" name="ved_gtotal[]"><b>Rs. </b><label>'+pod_total+'</label><input class="stotal" type="hidden" value="'+total+'" name="ved_total[]"></td><td width="5%"><button class="btn btn-xs btn-danger dlte"><i class="fa fa-times-circle-o"></i></button></td></tr>';

    var sum         = $('#sum').val();
    var vesgst      = $('#vesgst').val();
    var vecgst      = $('#vecgst').val();
    var pDiscount   = $('#pDiscount').val();

    sum       = parseFloat(sum) + parseFloat(total);
    vesgst    = parseFloat(vesgst) + parseFloat(sgsta);
    vecgst    = parseFloat(vecgst) + parseFloat(cgsta);
    apayable  = parseFloat(sum) + parseFloat(vesgst) + parseFloat(vecgst);

    var pDiscount1  =  parseFloat(apayable) * parseFloat(pDiscount)/100;
    var apayable    =  parseFloat(apayable) - (parseFloat(apayable) * parseFloat(pDiscount))/100;

    pDiscount1      =   (Math.round(pDiscount1 * 100) / 100).toFixed(2);
    apayable        =   (Math.round(apayable * 100) / 100).toFixed(2);

    $('#discountp').val(pDiscount);
    $('#discountp1').html(pDiscount);
    $('#discounta').val(pDiscount1);
    $('#discounta1').html(pDiscount1);

    gtotal = Math.round(apayable);

    $('#gtotal').val(apayable);
    $('#gtotal1').html(apayable);

    $('#apayable').val(gtotal);
    $('#apayable1').html(gtotal);
    $('#sum1').html(sum);
    $('#sum').val(sum);
    $('#vecgst1').html(vecgst);
    $('#vecgst').val(vecgst);
    $('#vesgst1').html(vesgst);
    $('#vesgst').val(vesgst);

    var roundoff    =   Math.round(apayable) - parseFloat(apayable);
    roundoff        =   (Math.round(roundoff * 100) / 100).toFixed(2);
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);

    var apaid             =  $('#apaid').val();
    var balance = parseFloat(gtotal) - parseFloat(apaid);
    $('#balance').val(balance);
    $('#balance1').html(balance);

    $('#item_table tr:last').after(newrow);

    document.getElementById('product').value = "";
    document.getElementById('productid').value = "";
    document.getElementById('product_quantity').value = "";
    document.getElementById('product_unit').value = "";
    document.getElementById('product_price').value = "";
    document.getElementById('batch').value = "";
    document.getElementById('expiry').value = "";
    document.getElementById('pod_sgst').value = "";
    document.getElementById('pod_cgst').value = "";
});
  });
  </script>
<!-- Discount Add -->
  <script>
  $(document).ready(function() {
  $('.alldisc').click(function() {

    var grand             =  0;
    var pDiscount         =  $('#pDiscount').val();
    var sum               =  $('#sum').val();
    var vesgst            =  $('#vesgst').val();
    var vecgst            =  $('#vecgst').val();
    sum1                  =  parseFloat(sum) + parseFloat(vesgst) + parseFloat(vecgst);

    var pDiscount1        =  parseFloat(sum1) * parseFloat(pDiscount)/100;
    var grand             =  parseFloat(sum1) - parseFloat(pDiscount1);
    grand                 =  (Math.round(grand * 100) / 100).toFixed(2);

    pDiscount1            =   (Math.round(pDiscount1 * 100) / 100).toFixed(2);

    gtotal = Math.round(grand);
    var apaid             =  $('#apaid').val();
    var balance = parseFloat(gtotal) - parseFloat(apaid);
    $('#balance').val(balance);
    $('#balance1').html(balance);

    $('#discountp').val(pDiscount);
    $('#discountp1').html(pDiscount);
    $('#discounta').val(pDiscount1);
    $('#discounta1').html(pDiscount1);

    $('#gtotal').val(grand);
    $('#gtotal1').html(grand);

    $('#apayable').val(gtotal);
    $('#apayable1').html(gtotal);

    var roundoff    =   Math.round(grand) - parseFloat(grand);
    roundoff        =   (Math.round(roundoff * 100) / 100).toFixed(2);
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);


  });
  });
  </script>

    <!-- Amount Paid -->
  <script>
  $(document).ready(function() {
    // $('#reset').click(function() {
    //   window.location="<?= base_url() ?>purchase/add"
    // });
  $('#apaid').change(function() {
    var payamount   =  $('#apayable').val();
    var payamount   =  Math.round(payamount);
    var paid        =  $('#apaid').val();
    var topay       =  parseFloat(payamount) - parseFloat(paid);
    
    if(topay<0)
    {
      alert("Invalid Amount");
      // window.location="<?= base_url() ?>purchase/add";
      $('#apaid').val("");
      document.getElementById("apaid").focus();
    }
    $('#balance').val(topay);
    $('#balance1').html(topay);
  });
  });
  </script>

  <?php
foreach ($purchases as $key => $purchase) {

    $ve_id            = $purchase['ve_id'];
    $ve_bill_no       = $purchase['ve_bill_no'];
    $ve_vno           = $purchase['ve_vno'];
    $ve_pono          = $purchase['ve_pono'];
    $ve_date          = date("d-m-Y",strtotime($purchase['ve_date']));
    $ve_supplier      = $purchase['ve_supplier'];
    $ve_customer      = $purchase['ve_customer'];
    $ve_amount        = $purchase['ve_amount'];
    $ve_discount      = $purchase['ve_discount'];
    $ve_discounta     = $purchase['ve_discounta'];
    $ve_sgst          = $purchase['ve_sgst'];
    $ve_cgst          = $purchase['ve_cgst'];
    $ve_gtotal        = $purchase['ve_gtotal'];
    $ve_apayable      = $purchase['ve_apayable'];
    $ve_apaid         = $purchase['ve_apaid'];
    $ve_balance       = $purchase['ve_balance'];
    $ve_round         = $purchase['ve_round'];
    $po_suppliers     = $purchase['sp_vendor'];
    $po_supplierph    = $purchase['sp_phone'];
    // $u_emp_id         = $purchase['u_emp_id'];    
  }

  ?>


  <section class="right-side" style="min-height:700px;"> <?php 
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       Purchase
       <small>Edit Purchase </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."purchase"; ?>">Purchase</a></li>
      <li class="active"> <?=$page_title ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?= $page_title?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/purchase/add">
        <i class="fa fa-plus-circle"></i> Create New
      </a>
    </div>
     </div><br>
      <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success"> 
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>

     <?php //echo form_open_multipart('purchase') ?>
      <form id="post_form">
     <div class="box-body">
        
      <div class="row">                

       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">Voucher No. : <sup> </sup></label><?= $ve_vno?>           
          <input class="form-control"  type="hidden" name="user_type" value="<?= $user_type;?>">
          <input  type="hidden" id="ve_id" name="ve_id" value="<?= $ve_id;?>">
        </div>
        <div class="col-md-2">
          <label  class="control-label">Date : <sup> </sup></label><?= $ve_date?>  
          <input value="<?= date("Y-m-d",strtotime($ve_date))?>" type="hidden" name="ve_date">
        </div>

        <div class="col-md-2">
         <label  class="control-label">PO : <sup> </sup></label><?= $ve_pono?> 
         <input name="po_pono" type="hidden" value="<?= $ve_pono?>">
       </div>

         <div class="col-md-4"> </div>

          <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Supplier Bill No.<sup></sup></label>
              <input class="form-control validate[required] input-sm" type="text" name="ve_bill_no" id="ve_bill_no" value="<?=$ve_bill_no?>">                
            </div> </div>

       <div class="col-md-2">
         <label  class="control-label">Supplier ID : <sup> </sup></label><?= $ve_supplier?>    
         <input type="hidden" name="ve_customer" value="<?=$ve_customer ?>">     
       </div>
       <div class="col-md-7">
         <label  class="control-label">Supplier : <sup> </sup></label><?= $po_suppliers?>   
       </div>
       <div class="col-md-3" style="text-align:right">
         <label  class="control-label">Supplier Ph. : <sup> </sup></label><?= $po_supplierph?>   
       </div>


     </div>

     <div class="col-md-12">
      <br>
      <!-- <div class="col-md-3">
        <div class="form-group required">
          <label  class="control-label">Item<sup></sup></label>
          <input class="form-control input-sm"   type="text" id="product">
          <input class="form-control"   type="hidden" id="product_name">
          <input class="form-control"   required="required"  type="hidden" name="productid" id="productid"><input class="form-control"  type="hidden" id="pod_sgst">
          <input class="form-control"  type="hidden" id="pod_cgst">
          <input class="form-control"  type="hidden" id="pd_qty">
        </div> </div>
        
        <div class="col-md-1">
          <div class="form-group required">
            <label  class="control-label">Quantity<sup></sup></label>
            <input class="form-control input-sm"  data-prompt-position="topRight:150"  id="product_quantity"  name="product_quantity">                
          </div> </div>

          <div class="col-md-1">
          <div class="form-group required">
            <label  class="control-label">Free<sup></sup></label>
            <input class="form-control input-sm"  data-prompt-position="topRight:150"  id="free_quantity"  name="free_quantity" value="0">                
          </div> </div>

          <div class="col-md-1">
          <div class="form-group required">
            <label  class="control-label">Unit<sup></sup></label>
            <input class="form-control input-sm" readonly  type="text" id="product_unit"  name="product_unit">                
          </div> </div>


          <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Unit Price<sup></sup></label>
              <input class="form-control input-sm"   id="product_price"  type="text" name="product_price">                
            </div> </div>
            <div class="col-md-1">
            <div class="form-group required">
              <label  class="control-label">Batch<sup></sup></label>
              <input class="form-control input-sm" type="text" id="batch">                
            </div> </div>
            <div class="col-md-1">
            <div class="form-group required">
              <label  class="control-label">Expiry<sup></sup></label>
              <input class="form-control input-sm" placeholder="mm/yy" type="text" id="expiry">                
            </div> </div> -->



            </div>
          </div>
          <div class="box-footer">
            <div class="row">
              <!-- <div class="col-md-12">
                <div>
                  <a class="button btn btn-primary" >Add to Table</a>
                  <input class="btn-large btn-default btn" id="reset" type="reset" value="Reset">
                </div>
              </div> -->

            </div>
          </div>
          <?php //echo form_close(); ?>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">                

                      <tr>
                        <td><b>DISCOUNT(%)</b></td>
                        <td>
                          <input type="text" required="required" name="pDiscount" id="pDiscount" class="form-control" value="0" />
                        </td>          
                        <td><button class="btn btn-info alldisc" type="button">Add</button></td>
                      </tr>
                  </table>

               
                                    <table class="table table-striped table-bordered table-hover gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Product</th>
                        <th>Batch</th>
                        <th>Expiry</th>
                        <th>Unit Price</th>
                        <th>Sell Price</th>
                        <th>Quantity</th>
                        <th>Free</th>
                        <th>Unit</th>
                        <th>CGST</th>
                        <th>SGST</th>
                        <th>Sub Total</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                  <tbody> 
                  </tbody>
                  </table> 
                
                  <table class="table table-bordered">
                    <tr><th width="67.5%">Total</th>
                      <th><input type="hidden" name="sum" readonly id="sum" value="<?=$ve_amount ?>">Rs. <label id="sum1"><?=$ve_amount ?></label></th></tr>
                      <tr><th width="80%">SGST Total</th>
                      <th><input type="hidden" name="ve_sgst" readonly id="vesgst" value="<?=$ve_sgst ?>">Rs. <label id="vesgst1"><?=$ve_sgst ?></label></th></tr>
                      <tr><th width="80%">CGST Total</th>
                      <th><input type="hidden" name="ve_cgst" readonly id="vecgst" value="<?=$ve_cgst ?>">Rs. <label id="vecgst1"><?=$ve_cgst ?></label></th></tr>

                    <tr><th width="67.5%">Discount</th><th>
                      <input type="hidden" readonly name="discountp" id="discountp" value="<?=$ve_discount ?>"><input type="hidden" readonly name="discounta" id="discounta" value="<?=$ve_discounta ?>">Rs. <label id="discounta1"><?=$ve_discounta ?></label> (<label id="discountp1"><?=$ve_discount ?></label> %)</th></tr>
                    <tr><th width="80%">Grand Total</th><th><input type="hidden" name="gtotal" readonly id="gtotal" value="<?=$ve_gtotal ?>">Rs. <label id="gtotal1"><?=$ve_gtotal ?></label></th></tr>
                    <tr><th width="80%">Amount Payable</th><th><input type="hidden" name="apayable" readonly id="apayable" value="<?=$ve_apayable ?>">Rs. <label id="apayable1"><?=$ve_apayable ?></label></th></tr>
                    <tr><th width="80%">Round off Value</th><th><input type="hidden" name="roundoff" readonly id="roundoff" value="<?=$ve_round ?>">Rs. <label id="roundoff1"><?=$ve_round ?></label></th></tr>
                     <tr><th width="80%">Amount Paid</th><th><input  class="form-control validate[required,custom[number]]" type="input" data-prompt-position="topRight:150" name="apaid" id="apaid" value="<?=$ve_apaid ?>">
                     </th></tr>
                     <tr><th width="80%">Balance to Pay</th><th><input type="hidden" class="form-control" name="balance" readonly id="balance" value="<?=$ve_balance ?>">Rs. <label id="balance1"><?=$ve_balance ?></label></th></tr>

                    </table>
   <button class="btn-large btn-success btn" type="submit" onclick="save();" name="submit1" accesskey="s" title="short key-ALT+S"><i class='fa fa-floppy-o'></i> Update</button>
   <button class="btn-large btn-warning btn" type="submit" name="submit1" onclick="save_print();" accesskey="p" title="short key-ALT+P"><i class="fa fa-print"></i> Update & Print</button>
   <input type="hidden" id="Printid">

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
</div>
</section>

</section><!-- /.right-side -->

<script>

function save(){

      var postData = $("#post_form").serializeArray();
      var apaid         =  $('#apaid').val();

      if(apaid!="")
          {
            var ve_id         =  $('#ve_id').val();
      var formURL  = "<?= base_url() ?>purchase/purchaseUpdate/"+ve_id;
      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          $('#Printid').val(data);
          window.location = "<?php echo base_url();?>index.php/purchase";
          <?php 
          $this->session->set_flashdata('Success','purchased'); ?>
        }
      });
      }
}

function save_print(){

      var postData = $("#post_form").serializeArray();
      var apaid         =  $('#apaid').val();

      if(apaid!="")
          {
            var ve_id         =  $('#ve_id').val();
      var formURL  = "<?= base_url() ?>purchase/purchaseUpdate/"+ve_id;
      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          // alert(data);
          window.location = "<?php echo base_url();?>index.php/purchase/getPrint?pPrintid="+data;
        }
      });
      }
} 
    </script>

<script>
 $('.datepicker').datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true
    });</script>