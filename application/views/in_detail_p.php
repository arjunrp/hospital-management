<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
 
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
var r_type= "<?= $r_type ?>";
if(r_type==1)
{
  $('.tbhide').show();
  $('.tbs3').show();
  $('.tbs2').hide();
  $('.tbs4').hide();
  $('.tbs5').hide();
}
else if(r_type==2)
{
  $('.tbhide').show();
  $('.tbs5').show();
  $('.tbs2').hide();
  $('.tbs4').hide();
  $('.tbs3').hide();
}
else if(r_type==3)
{
  $('.tbhide').show();
  $('.tbs4').show();
  $('.tbs2').hide();
  $('.tbs5').hide();
  $('.tbs3').hide();
}
else if(r_type==0)
{
  $('.tbhide').show();
  $('.tbs4').show();
  $('.tbs2').show();
  $('.tbs5').show();
  $('.tbs3').show();
}

  oTable = $('#patient_table').dataTable({
    "aaSorting": [[ 0, "dec" ]],
    "bJQueryUI": true,
    "sPaginationType": "full_numbers",
    "iDisplayLength": 10,
    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
  });
});
</script>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     <?php echo $page_title; ?>
    </h1>
     <ol class="breadcrumb">
      <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
      
      <li class="active"><?php echo $page_title; ?></li>
    </ol>&nbsp;&nbsp;
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
              <?php echo form_open_multipart($action) ?>
                  
                  <input type="hidden" name="fdate" value="<?=$fdate ?>">
                  <input type="hidden" name="tdate" value="<?=$tdate ?>">
                  <input type="hidden" name="product1" value="<?=$product ?>">
                  <input type="hidden" name="r_type1" value="<?=$r_type ?>">           
            
            <div class="row">
              <div class="col-md-12">
                <div class="box-header  ">
                  <center><h3 class="box-title">Stocks Report from <b><?=$fdate ?></b>&nbsp;<b></b> to <b><?=$tdate ?></b> of <b><?php if($product==0) { echo "All Products"; } else { echo $products;  } ?></b></h3></center>
                </div>
                <div class="col-md-12"><hr></div>
                <center>     
                <button type="submit" name="submit" class="btn btn-primary" >Print</button>
              </center>

            <div class="row">
              
              <div class="col-md-12"><br>
                  <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-12">
                      <table id="" class="table table-condensed dataTable no-footer">
                       <thead><b>

                        <tr class="tbhide">
                          <th class="tbhide">Date</th>
                          <th class="tbhide">Item</th>
                          <th class="tbs2">Opening Stock</th>
                          <th class="tbs3">Purchase</th>
                          <th class="tbs4">Purchase Return</th>
                          <th class="tbs5">Sale</th>
                          <th class="tbs4">Sale Return</th>
                          <th class="tbs2">Closing Stock</th></tr>
                        </b>
                      </thead>
                      <tbody>
                      <?php if($r_type==1 || $r_type==2) { echo $output; } else if($r_type==0) {
                      foreach ($output as $output):  
                        ?>
                      <tr>
                         <td><?= date('d-m-Y', strtotime($output['sale_date']))   ; ?></td>
                         <td><?= $output['sale_item']; ?></td>
                         <td><?= $output['openingStock'] ?> </td>
                         <td><?php if(empty($output['purchase_qty'])) { echo "0"; } else { echo $output['purchase_qty']; } ?> </td>
                         <td><?php if(empty($output['purchase_return'])) { echo "0"; } else { echo $output['purchase_return']; } ?> </td>
                         <td><?php if(empty($output['sales_qty'])) { echo "0"; } else { echo $output['sales_qty']; } ?> </td>
                         <td><?php if(empty($output['sales_return'])) { echo "0"; } else { echo $output['sales_return']; } ?> </td>
                         <td><?= $output['closingstock'] ?> </td>
                       </tr>
                     <?php endforeach; } 
                      else if($r_type==3) {
                      foreach ($output as $output):  
                        ?>
                      <tr>
                         <td><?= date('d-m-Y', strtotime($output['sale_date']))   ; ?></td>
                         <td><?= $output['sale_item']; ?></td>
                         <td><?php if(empty($output['purchase_return'])) { echo "0"; } else { echo $output['purchase_return']; } ?> </td>
                         <td><?php if(empty($output['sales_return'])) { echo "0"; } else { echo $output['sales_return']; } ?> </td>
                       </tr>
                     <?php endforeach; }
                     ?>
               </tbody>
            </table>
      
        </div>
      </div>
    </div>

  </div>
</div>

</div>
</div>

<?php echo form_close(); ?>    

</div></div></div>
</div></div></div>
</section></div>


<script>
 $('#datepicker').datepicker({
      autoclose: true
    });</script>


    <script>
 $('#datepicker2').datepicker({
      autoclose: true
    });</script>



