<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
 
<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
      Type
      <small>Update  Type </small>
    </h1>
    <ol class="breadcrumb">
      <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="">Type</a></li>
      <li class="active">Update Type</li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <?php if($this->session->flashdata('TypeSuccess')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
        <h3 class="box-title">
         <i class="fa fa-th"></i> Update Type
       </h3>

     </div>

     <?php echo form_open_multipart('types/updateType') ?>
     <div class="box-body">
      <div class="row">
        <div class="col-md-6 ">

          
            <input class="form-control" required="required"  type="hidden" name="typeid" value="<?php echo $type['id']; ?>">                

          <div class="form-group required">
            <label  class="control-label">Type<sup></sup></label>
            <input class="form-control" required="required"  type="text" name="type" value="<?php echo $type['type']; ?>">                
          </div>

        </div>
      </div>
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            <div>
              <input class="btn-large btn-primary btn" type="submit" value="Submit" name="submit"> 
              <input class="btn-large btn-default btn" type="reset" value="Reset">
            </div>
          </div>

        </div>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</section>

</section><!-- /.right-side -->


