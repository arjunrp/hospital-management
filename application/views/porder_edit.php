<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  
   <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
   <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
  <script type="text/javascript" charset="utf-8">
  $(function()
  {
    $("#sp_id").autocomplete({
      source: [<?php
      $i=0;
      foreach ($suppliers as $supplier){
        if ($i>0) {echo ",";}
        echo '{value:"' . $supplier['sp_id'] . '",vendorr:"' . $supplier['sp_vendor'] . '",vendorph:"' . $supplier['sp_phone'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          // $("#vendorid").val(ui.item ? ui.item.vid : '');
          $("#sp_vendor").val(ui.item ? ui.item.vendorr : '');
          $("#sp_phone").val(ui.item ? ui.item.vendorph : '');
        }
      });   
  });  

 $(function()
  {
    $("#sp_vendor").autocomplete({
      source: [<?php
      $i=0;
      foreach ($suppliers as $supplier){
        if ($i>0) {echo ",";}
        echo '{value:"' . $supplier['sp_vendor'] . '",vid:"' . $supplier['sp_id'] . '",vendorph:"' . $supplier['sp_phone'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          $("#sp_id").val(ui.item ? ui.item.vid : '');
          // $("#vendor").val(ui.item ? ui.item.vendorr : '');
          $("#sp_phone").val(ui.item ? ui.item.vendorph : '');
        }
      });   
  });

 $(function()
  {
    $("#sp_phone").autocomplete({
      source: [<?php
      $i=0;
      foreach ($suppliers as $supplier){
        if ($i>0) {echo ",";}
        echo '{value:"' . $supplier['sp_phone'] . '",vid:"' . $supplier['sp_id'] . '",vendorr:"' . $supplier['sp_vendor'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          $("#sp_id").val(ui.item ? ui.item.vid : '');
          $("#sp_vendor").val(ui.item ? ui.item.vendorr : '');
          // $("#vendorph").val(ui.item ? ui.item.vendorph : '');
        }
      });   
  });

  </script>


  <script type="text/javascript" charset="utf-8">
  $(function()
  {
    $("#pod_item_name").autocomplete({
      source: [<?php
      $i=0;
      $unit="";
      foreach ($products as $product){
        if ($i>0) {echo ",";}
        echo '{value:"' .$product['pd_product'] . '",pid:"' . $product['pd_code'] . '",productname:"' . $product['pd_product'] . '",productunit:"' . $product['pd_unit'] . '",productsgst:"' . $product['pd_sgst'] . '",productcgst:"' . $product['pd_cgst'] . '"}';
        $i++;
      }
      ?>],
        minLength: 3,//search after one characters
        delay: 300 ,
        select: function(event,ui){

          $("#pod_item_id").val(ui.item ? ui.item.pid : '');
          $("#pod_item").val(ui.item ? ui.item.productname : '');
          $("#pod_unit").val(ui.item ? ui.item.productunit : '');
          $("#pod_sgst").val(ui.item ? ui.item.productsgst : '');
          $("#pod_cgst").val(ui.item ? ui.item.productcgst : '');
        }
      });   
  });  
  </script>
  <script type="text/javascript" charset="utf-8">
  function calcu(p)
  {
    var grand = 0;
    var sgrand = 0;
    var sgsta1 = 0;
    var cgsta1 = 0;
    var pp = parseFloat($('#pod_price'+p).val());
    var qty = parseFloat($('#pod_qty'+p).val());
    var sgstp = parseFloat($('#pod_sgstp'+p).val());
    var cgstp = parseFloat($('#pod_cgstp'+p).val());
    var total = pp * qty;

    var sgsta       = (parseFloat(total) * parseFloat(sgstp))/100;
    var cgsta       = (parseFloat(total) * parseFloat(cgstp))/100;
    var pod_total   = parseFloat(total) + parseFloat(sgsta) + parseFloat(cgsta);
    // alert(sgstp+","+cgstp+","+sgsta+","+cgsta+","+pod_total);

    $('#pod_sgsta'+p).val((Math.round(sgsta * 100) / 100).toFixed(2));
    $('#pod_1sgsta'+p).html((Math.round(sgsta * 100) / 100).toFixed(2));
    $('#pod_cgsta'+p).val((Math.round(cgsta * 100) / 100).toFixed(2));
    $('#pod_1cgsta'+p).html((Math.round(cgsta * 100) / 100).toFixed(2));
    $('#pod_total'+p).val((Math.round(pod_total * 100) / 100).toFixed(2));
    $('#ftotal1'+p).html((Math.round(pod_total * 100) / 100).toFixed(2));
    $('#pod_stotal'+p).val((Math.round(total * 100) / 100).toFixed(2));

    $('.ftotal').each(function(){
        grand += parseFloat(this.value);
    });
    $('.stotal').each(function(){
        sgrand += parseFloat(this.value);
    });
    $('.cgsta').each(function(){
        cgsta1 += parseFloat(this.value);
    });
    $('.sgsta').each(function(){
        sgsta1 += parseFloat(this.value);
    });


    $('#po_amount').val(grand);
    $('#po_amount1').html(grand);
    $('#sum').html(sgrand);
    $('#sum1').val(sgrand);
    $('#cgstot').html(cgsta1);
    $('#cgstot1').val(cgsta1);
    $('#sgstot').html(sgsta1);
    $('#sgstot1').val(sgsta1);

  }

  $(function() {
    $( "#purchase_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>

<!-- Product Delete -->
<script>
  $(document).ready(function() {
    setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);

  $(document).on('click','button.dlte', function() {
    var $row    = $(this).closest("tr");    // Find the row


    var r = confirm("Are you sure want to delete!");
    if (r == true) {
    $(this).closest('tr').remove();
    var grand = 0;
    var sgrand = 0;
    var sgsta1 = 0;
    var cgsta1 = 0;
    $('.ftotal').each(function(){
        grand += parseFloat(this.value);
    });
    $('.stotal').each(function(){
        sgrand += parseFloat(this.value);
    });
    $('.cgsta').each(function(){
        cgsta1 += parseFloat(this.value);
    });
    $('.sgsta').each(function(){
        sgsta1 += parseFloat(this.value);
    });
    grand         =   (Math.round(grand * 100) / 100).toFixed(2);
    sgrand         =   (Math.round(sgrand * 100) / 100).toFixed(2);
    cgsta1         =   (Math.round(cgsta1 * 100) / 100).toFixed(2);
    sgsta1         =   (Math.round(sgsta1 * 100) / 100).toFixed(2);

    $('#po_amount').val(grand);
    $('#po_amount1').html(grand);
    $('#sum').html(sgrand);
    $('#sum1').val(sgrand);
    $('#cgstot').html(cgsta1);
    $('#cgstot1').val(cgsta1);
    $('#sgstot').html(sgsta1);
    $('#sgstot1').val(sgsta1);
    
    } 
    });
  });
  </script>
<!-- Product Add -->
  <script>
  $(document).ready(function() {

<?php
$js_array = json_encode($porders);
echo "var porders = ". $js_array . ";\n";
?>
var count = 1;
var sgstot = 0;
var cgstot = 0;
var sum = 0;
var differ = 0;
var po_amount = 0;
var sgstot = 0;
var cgstot = 0;

$.each(porders, function(index) {
  price     = parseFloat(porders[index].pod_price)*parseFloat(porders[index].pod_qty);

   var newrow      = '<tr><td><input type="hidden" value="'+porders[index].pod_item_id+'" name="pod_item_id[]"><input type="hidden" value="'+porders[index].pod_item+'" name="pod_item[]">' + porders[index].pod_item + '</td><td width="10%"><input class="form-control input-sm" type="input" value="'+porders[index].pod_price+'" id="pod_price'+count+'" onkeyup="calcu('+count+')" name="pod_price[]"></td><td width="10%"><input class="form-control input-sm" type="input" value="'+porders[index].pod_qty+'" id="pod_qty'+count+'" name="pod_qty[]" onkeyup="calcu('+count+')"></td><td width="10%"><input class="form-control input-sm" type="input" value="'+porders[index].pod_offer_qty+'"  name="pod_offer_qty[]"></td><td><input type="hidden" value="'+porders[index].pod_unit+'" id="pod_unit'+count+'" name="pod_unit[]">'+porders[index].pod_unit+'</td><td><input type="hidden" value="'+porders[index].pod_cgstp+'" id="pod_cgstp'+count+'" name="pod_cgstp[]"><input class="cgsta" type="hidden" value="'+porders[index].pod_csgta+'" id="pod_cgsta'+count+'" name="pod_cgsta[]"><label id="pod_1cgsta'+count+'">'+porders[index].pod_csgta+'</label>('+porders[index].pod_cgstp+'%)</td><td><input type="hidden" value="'+porders[index].pod_sgstp+'" id="pod_sgstp'+count+'" name="pod_sgstp[]"><input class="sgsta" type="hidden" value="'+porders[index].pod_sgsta+'" id="pod_sgsta'+count+'" name="pod_sgsta[]"><label id="pod_1sgsta'+count+'">'+porders[index].pod_sgsta+'</label> ('+porders[index].pod_sgstp+'%)</td><td><input type="hidden" value="'+porders[index].pod_total+'" class="ftotal" id="pod_total'+count+'" name="pod_total[]"><b>Rs. </b><label id="ftotal1'+count+'">'+porders[index].pod_total+'</label><input class="stotal" type="hidden" value="'+price+'" id="pod_stotal'+count+'"></td><td><button class="btn btn-xs btn-danger dlte"><i class="fa fa-times-circle-o"></i></button></td></tr>';

   $('#item_table tr:last').after(newrow);
    count++;
  });



  $('.button').click(function() {
    var pod_item       = $('#pod_item').val();
    var pod_item_id    = $('#pod_item_id').val();
    var pod_qty        = $('#pod_qty').val();
    var pod_offer_qty  = $('#pod_offer_qty').val();
    var pod_unit       = $('#pod_unit').val();
    var pod_price      = $('#pod_price').val();
    var sgst           = $('#pod_sgst').val();
    var cgst           = $('#pod_cgst').val();
    var pod_total      = $('#pod_qty').val() * $('#pod_price').val();
    var price          = (Math.round(pod_total * 100) / 100).toFixed(2);

    var pod_sgst       = (parseFloat(pod_total) * parseFloat(sgst))/100;
    var pod_cgst       = (parseFloat(pod_total) * parseFloat(cgst))/100;
    var pod_total      = parseFloat(pod_total) + parseFloat(pod_sgst) + parseFloat(pod_cgst);
    
    var pod_sgst       = (Math.round(pod_sgst * 100) / 100).toFixed(2);
    var pod_cgst       = (Math.round(pod_cgst * 100) / 100).toFixed(2);
    var pod_total      = (Math.round(pod_total * 100) / 100).toFixed(2);

    var po_amount      = $('#po_amount').val();
    var sum            = $('#sum').text();
    var cgstot         = $('#cgstot').text();
    var sgstot         = $('#sgstot').text();

    var newrow      = '<tr><td><input type="hidden" value="'+pod_item_id+'" name="pod_item_id[]"><input type="hidden" value="'+pod_item+'" name="pod_item[]">' + pod_item + '</td><td><input type="hidden" value="'+pod_price+'" name="pod_price[]">' + pod_price + '</td><td><input type="hidden" value="'+pod_qty+'" name="pod_qty[]">' + pod_qty + '</td><td><input type="hidden" value="'+pod_offer_qty+'" name="pod_offer_qty[]">' + pod_offer_qty + '</td><td><input type="hidden" value="'+pod_unit+'" name="pod_unit[]">' + pod_unit + '</td><td><input type="hidden" value="'+cgst+'" name="pod_cgstp[]"><input class="cgsta" type="hidden" value="'+pod_cgst+'" name="pod_cgsta[]"> <label>'+pod_cgst+'</label> ('+cgst+'%)</td><td><input type="hidden" value="'+sgst+'" name="pod_sgstp[]"><input class="sgsta" type="hidden" value="'+pod_sgst+'" name="pod_sgsta[]"><label>'+pod_sgst+'</label> ('+sgst+'%)</td><td><input type="hidden" class="ftotal" value="'+pod_total+'" name="pod_total[]"><label>Rs. ' + pod_total + '</label><input class="stotal" type="hidden" value="'+price+'"></td><td><button class="btn btn-xs btn-danger dlte"><i class="fa fa-times-circle-o"></i></button></td></tr>';


    sum          = parseFloat(sum) + parseFloat(price);
    sum          = (Math.round(sum * 100) / 100).toFixed(2);
    cgstot       = parseFloat(cgstot) + parseFloat(pod_cgst);
    cgstot       = (Math.round(cgstot * 100) / 100).toFixed(2);
    sgstot       = parseFloat(sgstot) + parseFloat(pod_sgst);
    sgstot       = (Math.round(sgstot * 100) / 100).toFixed(2);

    $('#sum').html(sum);
    $('#sum1').val(sum);
    $('#cgstot').html(cgstot);
    $('#cgstot1').val(cgstot);
    $('#sgstot').html(sgstot);
    $('#sgstot1').val(sgstot);
    po_amount          = parseFloat(po_amount) + parseFloat(pod_total);
    po_amount      = (Math.round(po_amount * 100) / 100).toFixed(2);
    $('#po_amount').val(po_amount);
    $('#po_amount1').html(po_amount);


    document.getElementById('pod_item_name').value = "";
    document.getElementById('pod_item').value = "";
    document.getElementById('pod_item_id').value = "";
    document.getElementById('pod_qty').value = "";
    document.getElementById('pod_unit').value = "";
    document.getElementById('pod_price').value = "";
    document.getElementById('pod_sgst').value = "";
    document.getElementById('pod_cgst').value = "";
    $('#item_table tr:last').after(newrow);
});
  });
  </script>

  <?php
foreach ($porders as $key => $porder) {

    $po_id            = $porder['po_id'];
    $po_pono          = $porder['po_pono'];
    $po_date          = date("d-m-Y",strtotime($porder['po_date']));
    $po_supplier      = $porder['po_supplier'];
    $po_dlvry_date    = date("d-m-Y",strtotime($porder['po_dlvry_date']));
    $po_dlvry_type    = $porder['po_dlvry_type'];
    $po_suppliers     = $porder['sp_vendor'];
    $po_supplierph    = $porder['sp_phone'];
    // $u_emp_id         = $porder['u_name'];
    $po_total         = $porder['po_total'];
    $po_sgst          = $porder['po_sgst'];
    $po_cgst          = $porder['po_cgst'];
    $po_amount        = $porder['po_amount'];
  }

  ?>

  <section class="right-side" style="min-height:700px;"> <?php 
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       Purchase Order
       <small>Create Purchase Order</small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."porder"; ?>">Purchase Order</a></li>
      <li class="active"> <?=$page_title ?>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?=$page_title ?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/porder/add">
        <i class="fa fa-plus-circle"></i> Create New
      </a>
    </div>
     </div>
<br>
      <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success"> 
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
      <form id="post_form">
     <div class="box-body"> 
      <div class="row">                
       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">PO No. : <sup></sup></label>
          <?= $po_pono;?>
          <input type="hidden" name="po_id" value="<?php echo $po_id;?>">  
          <input type="hidden" name="user_type" value="<?= $user_type;?>">           
        </div>
        <div class="col-md-2">
          <label class="control-label">Date : <sup></sup></label>
          <?=$po_date?> 
          <input type="hidden" name="po_date" value="<?= date("Y-m-d",strtotime($po_date));?>">               
        </div>


       <div class="col-md-2">
         <label  class="control-label">Supplier ID : <sup></sup></label>
         <?=$po_supplier?>    
       </div>
       <div class="col-md-3">
         <label  class="control-label">Supplier : <sup></sup></label>
         <?=$po_suppliers?>
       </div>
       <div class="col-md-3">
         <label  class="control-label">Supplier Ph. :<sup></sup></label>
         <?=$po_supplierph?>
       </div>
     </div>

     <div class="col-md-12">
      <br>
      <div class="col-md-4">
        <div class="form-group required">
          <label  class="control-label">Item<sup></sup></label>
          <input class="form-control input-sm"   type="text" id="pod_item_name">
          <input class="form-control"  type="hidden" id="pod_item">
          <input class="form-control"  type="hidden" name="pod_item_id" id="pod_item_id">
          <input class="form-control"  type="hidden" id="pod_sgst">
          <input class="form-control"  type="hidden" id="pod_cgst">
        </div> </div>
        
        <div class="col-md-1">
          <div class="form-group required">
            <label  class="control-label">Qty<sup></sup></label>
            <input class="form-control input-sm"  data-prompt-position="topRight:150"  id="pod_qty"  name="pod_qty">                
          </div> </div>

          <div class="col-md-1">
          <div class="form-group required">
            <label  class="control-label">Offer Qty<sup></sup></label>
            <input class="form-control input-sm"  data-prompt-position="topRight:150"  id="pod_offer_qty"  name="pod_offer_qty">                
          </div> </div>

          <div class="col-md-1">
          <div class="form-group required">
            <label  class="control-label">Unit<sup></sup></label>
            <input class="form-control input-sm" readonly type="text" name="pod_unit" id="pod_unit">
          </div> </div>


          <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Unit Price<sup></sup></label>
              <input class="form-control input-sm" id="pod_price"  type="text" name="pod_price">
               </div> </div>

                <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Delivery Date<sup></sup></label>
              <input class="form-control input-sm datepicker" id="po_dlvry_date"  type="text" name="po_dlvry_date" value="<?=$po_dlvry_date?>">
               </div> </div>

               <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Delivery Type<sup></sup></label>
              <select class="form-control input-sm" id="po_dlvry_type" name="po_dlvry_type">
                <option value="Door" <?php if($po_dlvry_date=="Door"){ ?> selected <?php } ?>>Door</option>
                <option value="Self" <?php if($po_dlvry_date=="Self"){ ?> selected <?php } ?>>Self</option>
              </select>
               </div> </div>

            </div>
          </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-md-12">
                <div>
                  <a class="button btn btn-primary" >Add to Table</a>
                  <input class="btn-large btn-default btn" type="reset" value="Reset">
                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
 
                    <table class="table table-striped table-bordered table-hover gridView" id="item_table">
                    <thead>
                      <tr>

                        <th>Product</th>
                        <th>Unit Price</th>
                        <th>Qty</th>
                        <th>Offer Qty</th>
                        <th>Unit</th>
                        <th>CGST</th>
                        <th>SGST</th>
                        <th>Sub Total</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                  <tbody> 
                  </tbody>
                  </table> 
                
                  <table class="table table-bordered">
                    <tr><th width="80%">Total</th>
                      <th><input type="hidden" name="sum1" readonly id="sum1" value="<?=$po_total ?>">Rs. <label id="sum"><?=$po_total ?></label></th></tr>
                      <tr><th width="80%">SGST Total</th>
                      <th><input type="hidden" name="sgstot1" readonly id="sgstot1" value="<?=$po_sgst ?>">Rs. <label id="sgstot"><?=$po_sgst ?></label></th></tr>
                      <tr><th width="80%">CGST Total</th>
                      <th><input type="hidden" name="cgstot1" readonly id="cgstot1" value="<?=$po_cgst ?>">Rs. <label id="cgstot"><?=$po_cgst ?></label></th></tr>
                      <tr><th width="80%">Grand Total</th>
                      <th><input type="hidden" name="po_amount" readonly id="po_amount" value="<?=$po_amount ?>">Rs. <label id="po_amount1"><?=$po_amount ?></label></th></tr>
                  
                    </table>
   <button class="btn-large btn-success btn" type="submit" onclick="update();" name="submit" accesskey="s" title="short key-ALT+S"><i class='fa fa-floppy-o'></i> Update</button>
   <button class="btn-large btn-warning btn" type="submit" name="submit1" onclick="save_print();" accesskey="p" title="short key-ALT+P"><i class="fa fa-print"></i> Update & Print</button>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
</div>
</section>

</section><!-- /.right-side -->

<script>

function update(){
      var postData = $("#post_form").serializeArray();
      var formURL  = "<?= base_url() ?>porder/porderUpdate";

      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          $('#Printid').val(data);
          window.location = "<?php echo base_url();?>index.php/porder";
          <?php 
          $this->session->set_flashdata('Success','Purchase Order Updated'); ?>
        }
      });
}

function save_print(){

      var postData = $("#post_form").serializeArray();
      var formURL  = "<?= base_url() ?>porder/porderUpdate";
      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {

          window.location = "<?php echo base_url();?>index.php/porder/getPrint?pPrintid="+data;
          <?php 
          $this->session->set_flashdata('Success','Purchase Order Updated'); ?>
        }
      });
      }

    </script>

<script>
 $('.datepicker').datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true
    });</script>

