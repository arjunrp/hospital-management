<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<script type="text/javascript" charset="utf-8">
$(function()
{
  $(".confirmClick").click( function() { 
    if ($(this).attr('title')) {
      var question = 'Are you sure you want to ' + $(this).attr('title').toLowerCase() + '?';
    } else {
      var question = 'Are you sure you want to do this action?';
    }
    if ( confirm( question ) ) {
      [removed].href = this.src;
    } else {
      return false;
    }
  });

});

$(document).ready(function() {
  oTable = $('#patient_table').dataTable({
    "aaSorting": [[ 2, "asc" ]],
    "bJQueryUI": true,
    "sPaginationType": "full_numbers",
    "iDisplayLength": 10,
    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]

  });
} );

$(function() {
  $( "#stock_from_date" ).datepicker({
    dateFormat: "dd-mm-yy",
    showButtonPanel: true,
    changeMonth: true,
    changeYear: true

  });
});




$(function() {
  $( "#stock_to_date" ).datepicker({
    dateFormat: "dd-mm-yy",
    showButtonPanel: true,
    changeMonth: true,
    changeYear: true

  });
});
</script>
<?php

$level = $this->session->userdata('category');
?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h4 class="headColor">
      Sale Report
    </h4>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
      
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">

                <!-- <h2><?php //echo $this->lang->line("op_department")." ".$this->lang->line("report");?></h2> -->
                <span class="err"><?php echo validation_errors(); ?></span>

                <?php echo form_open('stock/stocks_report_filter'); ?>

                <div class="row">
                  <div class="col-md-2">
                    <label><?php echo $this->lang->line("from_date");?></label>
                  </div>

                  <div class="col-md-2">
                    <label><?php echo $this->lang->line("to_date")?></label>
                  </div>
                  <div class="col-md-3">
                    <label><?php echo $this->lang->line("department")?></label>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-2">
                       <label>From Date</label>
                    <input class="form-control" type="text" name="stock_from_date" id="stock_from_date" value="" />
                  </div>
                  <div class="col-md-2">
                     <label>To Date</label>
                    <input class="form-control" type="text" name="stock_to_date" id="stock_to_date" value="" />
                  </div>
                 <!--  <div class="col-md-3">
                    <select class="form-control" name="department">
                      <option value="">Select a Department</option>
                      <?php foreach ($departments as $department) : ?>

                      <option value="<?= $department['department']?>"><?= $department['department']?></option>
                    <?php endforeach ?>
                  </select>      
                </div> -->

                <div class="col-md-2">
                  <button type="submit" name="submit" class="btn btn-sm btn-success"><?php echo $this->lang->line("go");?></button>
                </div>
              </div>
            </form>
            <hr>
            <div class="row">
              <div class="col-md-12">
                <?php $stock_from_date = date('d-m-Y', strtotime($this->input->post('stock_from_date')));  ?>  
                <?php $stock_to_date = date('d-m-Y', strtotime($this->input->post('stock_to_date')));  ?>       
                <?php $department = $this->input->post('department');  ?>
                <div class="box-header with-border">
                  <center><h3 class="box-title">Sale Report from &nbsp;<b><?php echo $stock_from_date; ?></b> to <b><?php echo $stock_to_date; ?></b> in <b><?php echo $department; ?></b> <?php echo $this->lang->line("department");?></h3></center>
                </div>
                <?php echo form_open('stock/print_stocks_report/'.$stock_from_date.'/'.$stock_to_date.'/'.$department ); ?>
                <input type="hidden" name="stock_from_date" value="<?=$stock_from_date ; ?>" />
                <input type="hidden" name="stock_to_date" value="<?=$stock_to_date ; ?>" />
                <input type="hidden" name="department" id="department" value="<?=$department ?>" /> 
                <center>  

                <br>    
                <button type="submit" name="submit" class="btn btn-primary" ><?php echo $this->lang->line("print");?> <?php echo $this->lang->line("report");?></button>
              </center>
            </form>
            <div class="row">
              <div class="col-md-12">
                <div id="example_wrapper" class="table-responsive">
                  <div class="row">
                   <?php 
                   $deptmnt='';
                   if(isset($_POST['department']) ){

                    $deptmnt=$_POST['department'];?>
                    <input type="hidden" name="depa_rtment" value="<?=$deptmnt ; ?>" />
                    <?php

                  }
                  ?>
                  <div class="col-md-12">
                    <div class="col-md-12">
                      <table id="patient_table" class="display table table-bordered table-striped">

                       <thead><b>
                        <tr><th>Date</th><th><?php echo $this->lang->line("item");?></th><th>Opening Stock</th>
                           <?php if($deptmnt=='Central Store'){
                        ?>
                          <th>Purchase</th>

                          <?php
                        }
                          ?>
                          <th>Sale</th>
                          <th>From Stock</th>

                         
                          <th><?php if($deptmnt=='Central Store'){ ?>To Stock <?php } else { ?>Purchase <?php } ?></th>

                         
                          <!-- <th>Stock From</th> --><!-- <th>Stock To</th> --><th>Closing Stock</th></tr>
                        </b>
                      </thead>
                      <tbody>
                        <?php $opening_stock=0;
                        print_r($view_stock_report);
                        foreach ($view_stock_report as $view_stock_report):
                          ?>
                        <tr>

                         <td><?= date('d-m-Y', strtotime($view_stock_report['voucher_date']))   ; ?></td>
                         <td><?= $view_stock_report['item_name']; ?></td>
                         <td><?= $view_stock_report['openingStock'] ?> </td>
                         <?php 
                         if($deptmnt=='Central Store'){
                          ?> 
                          <td><?= $view_stock_report['PurchaseStockCentralStore']+$view_stock_report['PurchaseStockCentralStorefree'];  ?> </td>
                          <?php


                        }
                        


                         ?>
                        <td><?= $view_stock_report['sales']; ?></td>
                        <?php
                      
                      if($deptmnt=='Central Store'){
                        ?>
                        <td><?= $view_stock_report['FromStockCentralStore']; ?></td>

                        <?php

                      }
                      else
                      { ?>
                        <td><?= $view_stock_report['FromStockAll']; ?></td> <?php
                      }
                      ?>

                      <?php
                  
                    if($deptmnt=='Central Store'){
                      ?>
                      <td><?= $view_stock_report['ToStockCentralStore']; ?></td>

                      <?php

                    }
                    else{
                      ?>
                      <td><?= $view_stock_report['ToStockAll']; ?></td>

                      <?php

                    }

                    ?>
                   <td><?= $view_stock_report['closingstock'] ?> </td>
                    <!-- <td><?php //echo (($view_stock_report['PurchaseStockCentralStore']+$view_stock_report['PurchaseStockCentralStorefree']) + $view_stock_report['openingStock']+$view_stock_report['ToStockCentralStore'])-$view_stock_report['FromStockCentralStore']; ?></td> -->


                  </tr>
                <?php endforeach ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

</div>
</div>
</form>

</div></div></div>
</div></div></div>
</section></div>