<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#purchase_table').dataTable({
  "aaSorting": [[ 0, "desc" ]],
  "bJQueryUI": true,
  "sPaginationType": "full_numbers",
  "iDisplayLength": 10,
  "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
});
});
</script>

<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
     Purchase Return
     <small><?=$page_title ?></small>
   </h1>
   <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="">Purchase Return</a></li>
    <li class="active"><?=$page_title ?></li>
  </ol>&nbsp;&nbsp;
</section>
<section class="content">    <!-- Success-Messages -->
  <div class="box box-info">
    <div class="box-header">
     
    <h3 class="box-title"><?=$page_title ?>
    </h3>
    <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/preturn/return_padd" accesskey="n" title="short key-ALT+N">
        <i class="fa fa-plus-circle"></i> Create New
      </a>

    </div>
  </div><!-- /.box-header -->

  <div class="box-body">
    <div id="example_wrapper" class="table-responsive">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-12">
            <table id="purchase_table" class="table table-condensed dataTable no-footer">
              <thead>
                <tr>
                  <th>Return No.</th>
                  <th>Date</th>
                  <th>Supplied Id</th>
                  <th>Supplier Name</th>
                  <th>Supplier Ph.</th>
                  <th>Options</th> 
                </tr>
              </thead>
              <tbody>
                <?=$output ?> 
            </tbody>
          </table>
        </div> 


      </div> 
      </div></div> 
    </div><!-- /.box-body -->
  </div>
  <br><br><br><br><br><br><br>   
</section>
</section><!-- /.right-side -->

