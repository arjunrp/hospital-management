<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#purchase_table').dataTable({
  "aaSorting": [[ 0, "desc" ]],
  "bJQueryUI": true,
  "sPaginationType": "full_numbers",
  "iDisplayLength": 10,
  "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
});
});
</script>
<?php
$nfp = "<font color='#FF0000'><b>Not Completed</b></font>"; 
$fp  = "<font color='#006600'><b>Completed</b></font>" ;
?>
<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
     Purchase
     <small>Purchase</small>
   </h1>
   <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
   <li class="active">Purchase</a></li>
  </ol>&nbsp;&nbsp; 
<!-- 
  <?php if($this->session->flashdata('Success')){ ?>
     <div class="alert alert-success">
      <a href="#" class="close" data-dismiss="alert">&times;</a>
      <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
    </div>
    <?php }else if($this->session->flashdata('Error')){  ?>
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert">&times;</a>
      <strong>Success!</strong> <?php echo $this->session->flashdata('Error'); ?>
    </div>
    <?php } ?> -->
    
</section>
<section class="content">    <!-- Success-Messages -->
  <div class="box box-info">
    <div class="box-header">
     
    <h3 class="box-title"> <?= $page_title?>
    </h3>
    <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/purchase/add" accesskey="n" title="short key-ALT+N">
        <i class="fa fa-plus-circle"></i> Create New
      </a>

    </div>
  </div><!-- /.box-header -->

  <div class="box-body">
    <div id="example_wrapper" class="table-responsive">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-12">
            <table id="purchase_table" class="table table-condensed dataTable no-footer">
              <thead>
                <tr>
                  <th>Purchase No.</th>
                  <th>Purchase Date</th>
                  <th>Bill No.</th>
                  <th>Supplier</th>
                  <th>PO No.</th>
                  <th>Payment</th>
                  <th>Options</th> 
                </tr>
              </thead>
              <tbody>
                <?=$output?>      
            </tbody>
          </table>
        </div> </div> 
      </div> </div>
    </div><!-- /.box-body -->
  </div>
</section>
</section><!-- /.right-side -->

