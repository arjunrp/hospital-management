<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
<?php
foreach ($discharges as $discharge) {

  $ve_id          = $discharge['ve_id'];
  $ve_vno         = $discharge['ve_vno'];
  $ve_date        = $discharge['ve_date'];
  $ve_customer    = $discharge['ve_customer'];
  $ve_mrd         = $discharge['ve_mrd'];
  $p_name         = $discharge['p_title']." ".$discharge['p_name'];
  $p_phone        = $discharge['p_phone'];
  $u_name         = $discharge['u_name'];
  $dp_department  = $discharge['dp_department'];
  $ve_user        = $discharge['ve_user'];
  $ve_amount      = $discharge['ve_amount'];
  $ve_discount    = $discharge['ve_discount'];
  $ve_discounta   = $discharge['ve_discounta'];
  $ve_apayable    = $discharge['ve_apayable'];
  $ve_round       = $discharge['ve_round'];
}
 ?>

  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       Discharge
       <small>Discharge Bill </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."discharge"; ?>">Discharge</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-success pull-right  view-btn-create" href="<?php echo base_url();?>index.php/ccounter">
        <i class="fa fa-mail-reply-all"></i> Back
      </a>
    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>

      <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row"> 
       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">Voucher No. : <sup></sup></label><?= $ve_vno?>             
        </div>
        <div class="col-md-2">
          <label  class="control-label">Date : <sup></sup></label><?= date("d-m-Y",strtotime($ve_date)) ?>
          <input type ="hidden" name="ve_date" value="<?=$ve_date?>">
          <input type ="hidden" name="ve_id" value="<?=$ve_id?>">
          <input type="hidden" name="ve_customer" value="<?= $ve_customer?>">
          </div>               

         
          <div class="col-md-2" >
         <label  class="control-label">MRD : <sup></sup></label><?= $ve_mrd?>    
               </div>

       <div class="col-md-2">
         <label  class="control-label">Phone No. : <sup></sup></label><?= $p_phone?>     
       </div>

       <div class="col-md-4" style="text-align:center">
         <label  class="control-label">Patient's Name : <sup></sup></label><?= $p_name?>    
       </div>
     </div>
     <div class="col-md-12"><br>
      <div class="col-md-2">
         <label  class="control-label">IP No. : <sup></sup></label><?= $ve_customer?> 
       </div>
         <div class="col-md-4">
         <label  class="control-label">Department : <sup></sup></label><?= $dp_department?>    
       </div>
        <div class="col-md-3">
         <label  class="control-label">Doctor : <sup></sup></label><?= $u_name?>      
       </div>
       <div class="col-md-2">
         <label  class="control-label">User Id : <sup></sup></label><?= $ve_user?>      
       </div>

     </div>

          </div>
          <div class="box-footer">
            <div class="row">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-bordered table-hover gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Sl No.</th>
                        <th colspan="8">Particulars</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                  <tbody> 

                    <?php $slno = 1; $grand_total=0; if($rooms) $slno1 =1; { ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Rooms</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th>Rooms</th><th>Admission Date</th><th>Vacated Date</th><th>Total Days</th><th>Rent</th><th>Nursing Charge</th><th>House Keeping Charge</th><th>Credit Amount</th></tr>
                    <?php foreach ($rooms as $room) {
                       $diff       = date_create($room['rs_adm'])->diff(date_create($room['rs_dis']));
                      // $date_diff  =  $diff->format("%a days\n%h hours\n%i minutes\n%s seconds\n");
                      $days       = $diff->format("%a");
                      $hours      = $diff->format("%h");
                      $minutes    = $diff->format("%i");

                      if($hours>6)
                      {
                        $date_diff = $days + 1;
                      }
                      else if($hours == 6 && $minutes > 0 )
                      {
                        $date_diff = $days + 1;
                      }
                      else  if($hours <= 6 && $hours > 0 )
                      {
                        $date_diff = $days + .5;
                      }
                      else
                      {
                        $date_diff = $days;
                      }
                      $total = ($room['rm_fees'] + $room['rm_nurse'] + $room['rm_hc']) * $date_diff;
                      $grand_total = $grand_total + $total;
                      ?> <tr><td></td><td><?=$slno1?></td><td><?=$room['rm_no']?></td><td><?=$room['rs_adm']?></td><td><?=$room['rs_dis']?></td><td><?=$date_diff?></td><td><?=$room['rm_fees']?></td><td><?=$room['rm_nurse']?></td><td><?=$room['rm_hc']?></td><td><?=$total?></td></tr> <?php
                    $slno1++;  } $slno++; ?>

                    <?php } ?>


                    <?php if($voucher_bills) { $slno3 =1; ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Lab</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Credit Amount</th></tr>
                    <?php foreach ($voucher_bills as $voucher_bill) {
                      $grand_total = $grand_total + $voucher_bill['ve_apayable'];
                      ?> <tr><td></td><td><?=$slno3?></td><td colspan="3"><?=$voucher_bill['ve_vno']?></td><td colspan="4"><?=$voucher_bill['ve_date']?></td><td><?=$voucher_bill['ve_apayable']?></td></tr>
                       <?php
                    $slno3++;  } $slno++;  ?>

                    <?php } ?>

                    <?php if($scanning_bills!=0) { $slno7 =1; ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Scanning</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Credit Amount</th></tr>
                    <?php foreach ($scanning_bills as $scanning_bill) {
                      $grand_total = $grand_total + $scanning_bill['ve_apayable'];
                      ?> <tr><td></td><td><?=$slno7?></td><td colspan="3"><?=$scanning_bill['ve_vno']?></td><td colspan="4"><?=$scanning_bill['ve_date']?></td><td><?=$scanning_bill['ve_apayable']?></td></tr>
                       <?php
                    $slno7++;  } $slno++;  ?>

                    <?php } ?>

                    <?php if($xray_bills!=0) { $slno8 =1; ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Xray</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Credit Amount</th></tr>
                    <?php foreach ($xray_bills as $xray_bill) {
                      $grand_total = $grand_total + $xray_bill['ve_apayable'];
                      ?> <tr><td></td><td><?=$slno8?></td><td colspan="3"><?=$xray_bill['ve_vno']?></td><td colspan="4"><?=$xray_bill['ve_date']?></td><td><?=$xray_bill['ve_apayable']?></td></tr>
                       <?php
                    $slno8++;  } $slno++;  ?>

                    <?php } ?>

                    <?php if($pharmacy_bills) { $slno2 =1; ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Pharmacy</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Credit Amount</th></tr>
                    <?php foreach ($pharmacy_bills as $pharmacy_bill) {
                      $grand_total = $grand_total + $pharmacy_bill['ve_apayable'];
                      ?> <tr><td></td><td><?=$slno2?></td><td colspan="3"><?=$pharmacy_bill['ve_vno']?></td><td colspan="4"><?=$pharmacy_bill['ve_date']?></td><td><?=$pharmacy_bill['ve_apayable']?></td></tr>
                       <?php
                    $slno2++;  } $slno++;  ?>

                    <?php } ?>

                    <?php if($pharmacy_returns) { $slno4 =1; ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Medicine Return</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Debit Amount</th></tr>
                    <?php foreach ($pharmacy_returns as $pharmacy_return) {
                      $grand_total = $grand_total - $pharmacy_return['ve_apayable'];
                      ?> <tr><td></td><td><?=$slno4?></td><td colspan="3"><?=$pharmacy_return['ve_vno']?></td><td colspan="4"><?=$pharmacy_return['ve_date']?></td><td><?=$pharmacy_return['ve_apayable']?></td></tr>
                       <?php
                    $slno4++;  } $slno++;  ?>

                    <?php } ?>

                    <?php if($advances) { $total_advance=0; $slno5 =1; ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Advance Payment</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Debit Amount</th></tr>
                    <?php foreach ($advances as $advance) {
                      $total_advance = $total_advance + $advance['ve_apayable'];

                      ?> <tr><td></td><td><?=$slno5?></td><td colspan="3"><?=$advance['ve_vno']?></td><td colspan="4"><?=$advance['ve_date']?></td><td><?=$advance['ve_apayable']?></td></tr>
                       <?php
                    $slno5++;  } $slno++;  ?>

                    <?php } ?>

                    <?php if($additionals) { $slno6 =1; ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Others</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th colspan="4">Particulars</th> <th colspan="2">Rate</th>
                        <th>Qty</th><th>Credit Amount</th></tr>
                    <?php foreach ($additionals as $additional) {
                      $grand_total = $grand_total + $additional['ved_gtotal'];
                      ?> <tr><td></td><td><?=$slno6?></td><td colspan="4"><?=$additional['ved_item']?></td>
                      <td colspan="2"><?=$additional['ved_price']?></td>
                      <td><?=$additional['ved_qty']?></td>
                      <td><?=$additional['ved_gtotal']?></td></tr>
                       <?php
                    $slno6++;  } $slno++;  ?>

                    <?php } ?>



                  </tbody>
                  </table> 
                  <?php if($advances==0)  { $total_advance=0;  }

                  $total_amount = $grand_total - $total_advance;
                  $advance_amt  = $total_amount * $ve_discount/100;
                  $amt_payable  = $total_amount - $advance_amt;
                  ?>

                  <table class="font_reduce table table-bordered">
                    <tr><th width="60%"></th><th>Total</th>
                      <th>Rs. <label><?=$grand_total?></label></th></tr>
                      <tr><th width="60%"></th><th>Advance Paid</th>
                      <th>Rs. <label><?=$total_advance?></label></th></tr>
                      <tr><th width="60%"></th><th>Total Amount</th>
                      <th>Rs. <label><?=$total_amount?></label></th></tr>
                       <tr><th width="60%"></th><th>Discount</th><th>
                      Rs. <label><?=$advance_amt?></label> (<label><?= $ve_discount?></label> %)</th></tr>

                      <tr><th width="60%"></th><th>Amount Payable</th>
                      <th>Rs. <label><?=$amt_payable?></label></th></tr>
                      <tr><th width="60%"></th><th>Round Off</th>
                      <th>Rs. <label><?=$ve_round?></label></th></tr>
                      <input type="hidden" name="grandtotal" value="<?= $ve_apayable   ?>">
                    </table>

                  <button class="btn btn-success" type="submit"><i class="fa fa-floppy-o"></i> Pay</button>
                
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
</div>
</section>

</section><!-- /.right-side -->

<script>

function print(){

          var ve_customer         =  $('#ve_customer').val();

          window.location = "<?php echo base_url();?>index.php/discharge/getPrint?Printid="+ve_customer;


}
</script>

<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });</script>
<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>