<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<html>
<head>
<link rel="stylesheet" href='<?= base_url() ?>asset/plugins/jvectormap/jquery-jvectormap-1.2.2.css' type="text/css"/>
  <link media="all" type="text/css" rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.css">
   <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.min.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.min.css' type="text/css"/>
   <link rel="stylesheet" href="<?= base_url() ?>asset/css/skins/_all-skins.min.css"> 
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
 
  <!-- <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datepicker/datepicker3.css"> -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- <link rel="stylesheet" href="<?= base_url() ?>asset/css/AdminLTE.min.css" type="text/css"/> -->
   <link rel="stylesheet" href="<?= base_url() ?>asset/bootstrap/css/AdminLTE.min.css" type="text/css"/>
   <script  src="<?= base_url() ?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
</head>
 <body onload="window.print();" style="margin-top:0%;display:block;height:100%;width: 100%;" >
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
  $('.tbhide').hide();
  $('.tbs3').hide();
  $('.tbs2').hide();
  $('.tbs4').hide();
  $('.tbs5').hide();
var r_type= "<?= $r_type ?>";
if(r_type==1)
{
  $('.tbs2').hide();
  $('.tbs4').hide();
  $('.tbs5').hide();
  $('.tbhide').show();
  $('.tbs3').show();
}
else if(r_type==2)
{
  $('.tbs2').hide();
  $('.tbs4').hide();
  $('.tbs3').hide();
  $('.tbhide').show();
  $('.tbs5').show();
}
else if(r_type==3)
{
  $('.tbs2').hide();
  $('.tbs5').hide();
  $('.tbs3').hide();
  $('.tbhide').show();
  $('.tbs4').show();
}
else if(r_type==0)
{
  $('.tbs2').show();
  $('.tbs5').show();
  $('.tbs3').show();
  $('.tbhide').show();
  $('.tbs4').show();
}

});
</script>
 <body onload="printform();" >
  <?php foreach($company as $company) {
    $company_name     = $company['company'];
    $company_lic      = $company['licence_no'];
    $company_address  = $company['address'];
    $company_street   = $company['street'];
    $company_city     = $company['city'];
    $company_state    = $company['state'];
    $company_country  = $company['country'];
    $company_zip      = $company['zip'];
    $company_phone    = $company['phone'];
    $company_email    = $company['email'];
    $company_fax      = $company['fax'];
    $company_website  = $company['website'];

   }?>


  

<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i> <?= $company_name ?>
         <!--  <small class="pull-right">Invoice Date:</small> -->
        </h2>
      </div>
      <!-- /.col -->
    </div>
    
      <div class="dontprint" align="right"> 
        <a title="Back" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."inventory/indetail"; ?>"><i class="fa fa-mail-reply-all"></i> Back</a>
        </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        <address>
          <strong><?= $company_name ?></strong><br>
          <strong>Lic No.:<?= $company_lic ?></strong><br>
          <?= $company_address.", ".$company_street  ?><br>
          <?= $company_city.", ".$company_state.", ".$company_country." - ".$company_zip ?> <br>
          <b>Phone:</b> <?= $company_phone ?>, Fax:  <?= $company_fax ?><br>
          <b>Email:</b> <?= $company_email ?><br>
          <b>Website:</b> <?= $company_website ?>
        </address>
        </div>
      <!-- /.col -->
      
    </div>
   <div class="row">
              <div class="col-md-12">
                <div class="box-header  ">
                  <center><h3 class="box-title">Stocks Report from <b><?=date("d-m-Y",strtotime($fdate)) ?></b>&nbsp;<b></b> to <b><?=date("d-m-Y",strtotime($tdate)) ?></b> of <b><?php if($product==0) { echo "All Products"; } else { echo $products;  } ?></b></h3></center>
                </div>
                <div class="col-md-12"><hr></div>
                <center>     
                
              </center>

            <div class="row">
              
              <div class="col-md-12"><br>
                  <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-12">
                      <table id="" class="table table-condensed dataTable no-footer">
                       <thead><b>

                        <tr class="tbhide">
                          <th class="tbhide">Date</th>
                          <th class="tbhide">Item</th>
                          <th class="tbs2">Opening Stock</th>
                          <th class="tbs3">Purchase</th>
                          <th class="tbs4">Purchase Return</th>
                          <th class="tbs5">Sale</th>
                          <th class="tbs4">Sale Return</th>
                          <th class="tbs2">Closing Stock</th></tr>
                        </b>
                      </thead>
                      <tbody>
                      <?php if($r_type==1 || $r_type==2) { echo $output; } else if($r_type==0) {
                      foreach ($output as $output):  
                        ?>
                      <tr>
                         <td><?= date('d-m-Y', strtotime($output['sale_date']))   ; ?></td>
                         <td><?= $output['sale_item']; ?></td>
                         <td><?= $output['openingStock'] ?> </td>
                         <td><?php if(empty($output['purchase_qty'])) { echo "0"; } else { echo $output['purchase_qty']; } ?> </td>
                         <td><?= $output['sales_qty'] ?> </td>
                         <td><?= $output['closingstock'] ?> </td>
                       </tr>
                     <?php endforeach; } 
                     else if($r_type==3) {
                      foreach ($output as $output):  
                        ?>
                      <tr>
                         <td><?= date('d-m-Y', strtotime($output['sale_date']))   ; ?></td>
                         <td><?= $output['sale_item']; ?></td>
                         <td><?php if(empty($output['purchase_return'])) { echo "0"; } else { echo $output['purchase_return']; } ?> </td>
                         <td><?php if(empty($output['sales_return'])) { echo "0"; } else { echo $output['sales_return']; } ?> </td>
                       </tr>
                     <?php endforeach; }?>
               </tbody>
            </table>
      
        </div>
      </div>
    </div>

  </div>
</div>

</div>
</div>

    <div class="row">
      <!-- accepted payments column -->
       <div class="col-xs-8">
       </div>
      
      

     </div>
       
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
  </body>
</html>
<style type="text/css" media="print">
.dontprint
{ display: none; }
</style>