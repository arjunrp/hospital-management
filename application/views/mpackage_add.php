<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  
   <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
   <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
  <script type="text/javascript" charset="utf-8">
  $(function()
  {
    $("#sp_id").autocomplete({
      source: [<?php
      $i=0;
      foreach ($suppliers as $supplier){
        if ($i>0) {echo ",";}
        echo '{value:"' . $supplier['sp_id'] . '",vendorr:"' . $supplier['sp_vendor'] . '",vendorph:"' . $supplier['sp_phone'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          // $("#vendorid").val(ui.item ? ui.item.vid : '');
          $("#sp_vendor").val(ui.item ? ui.item.vendorr : '');
          $("#sp_phone").val(ui.item ? ui.item.vendorph : '');
        }
      });   
  });  

 $(function()
  {
    $("#sp_vendor").autocomplete({
      source: [<?php
      $i=0;
      foreach ($suppliers as $supplier){
        if ($i>0) {echo ",";}
        echo '{value:"' . $supplier['sp_vendor'] . '",vid:"' . $supplier['sp_id'] . '",vendorph:"' . $supplier['sp_phone'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          $("#sp_id").val(ui.item ? ui.item.vid : '');
          // $("#vendor").val(ui.item ? ui.item.vendorr : '');
          $("#sp_phone").val(ui.item ? ui.item.vendorph : '');
        }
      });   
  });

 $(function()
  {
    $("#sp_phone").autocomplete({
      source: [<?php
      $i=0;
      foreach ($suppliers as $supplier){
        if ($i>0) {echo ",";}
        echo '{value:"' . $supplier['sp_phone'] . '",vid:"' . $supplier['sp_id'] . '",vendorr:"' . $supplier['sp_vendor'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          $("#sp_id").val(ui.item ? ui.item.vid : '');
          $("#sp_vendor").val(ui.item ? ui.item.vendorr : '');
          // $("#vendorph").val(ui.item ? ui.item.vendorph : '');
        }
      });   
  });

  </script>


  <script type="text/javascript" charset="utf-8">
  $(function()
  {
    $("#pod_item_name").autocomplete({
      source: [<?php
      $i=0;
      $unit="";
      foreach ($products as $product){
        if ($i>0) {echo ",";}
        echo '{value:"' .$product['pd_product'] . '",pid:"' . $product['pd_code'] . '",productname:"' . $product['pd_product'] . '",productunit:"' . $product['pd_unit'] . '",productsgst:"' . $product['pd_sgst'] . '",productcgst:"' . $product['pd_cgst'] . '"}';
        $i++;
      }
      ?>],
        minLength: 3,//search after one characters
        select: function(event,ui){

          $("#pod_item_id").val(ui.item ? ui.item.pid : '');
          $("#pod_item").val(ui.item ? ui.item.productname : '');
          $("#pod_unit").val(ui.item ? ui.item.productunit : '');
          $("#pod_sgst").val(ui.item ? ui.item.productsgst : '');
          $("#pod_cgst").val(ui.item ? ui.item.productcgst : '');
        }
      });   
  });  
  </script>
  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#purchase_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>

<!-- Product Delete -->
<script>
  $(document).ready(function() {
    setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);

  $(document).on('click','button.dlte', function() {
    var $row    = $(this).closest("tr");    // Find the row

    var r = confirm("Are you sure want to delete!");
    if (r == true) {
    $(this).closest('tr').remove();
    } 
    });
  });
  </script>
<!-- Product Add -->
  <script>
  $(document).ready(function() {
  $('.button').click(function() {
    var pod_item       = $('#pod_item').val();
    var pod_item_id    = $('#pod_item_id').val();
    var pod_qty        = $('#pod_qty').val();
    var pod_unit       = $('#pod_unit').val();
    var pod_price      = $('#pod_price').val();
    var sgst           = $('#pod_sgst').val();
    var cgst           = $('#pod_cgst').val();
    var pod_total      = $('#pod_qty').val() * $('#pod_price').val();
    var pod_sgst       = (parseFloat(pod_total) * parseFloat(sgst))/100;
    var pod_cgst       = (parseFloat(pod_total) * parseFloat(cgst))/100;
    var pod_total      = parseFloat(pod_total) + parseFloat(pod_sgst) + parseFloat(pod_cgst);
    
    var pod_sgst       = (Math.round(pod_sgst * 100) / 100).toFixed(2);
    var pod_cgst       = (Math.round(pod_cgst * 100) / 100).toFixed(2);
    var pod_total      = (Math.round(pod_total * 100) / 100).toFixed(2);

    var newrow      = '<tr><td><input type="hidden" value="'+pod_item_id+'" name="pod_item_id[]"><input type="hidden" value="'+pod_item+'" name="pod_item[]">' + pod_item + '</td><td><input type="hidden" value="'+pod_price+'" name="pod_price[]">' + pod_price + '</td><td><input type="hidden" value="'+pod_qty+'" name="pod_qty[]">' + pod_qty + '</td><td><input type="hidden" value="'+pod_unit+'" name="pod_unit[]">' + pod_unit + '</td><td><input type="hidden" value="'+sgst+'" name="mtd_sgstp[]"><input type="hidden" value="'+cgst+'" name="mtd_cgstp[]"><input type="hidden" value="'+pod_sgst+'" name="mtd_sgsta[]"><input type="hidden" value="'+pod_cgst+'" name="mtd_cgsta[]"><input type="hidden" value="'+pod_total+'" name="pod_total[]"><button class="btn btn-xs btn-danger dlte"><i class="fa fa-times-circle-o"></i></button></td></tr>';

    document.getElementById('pod_item_name').value = "";
    document.getElementById('pod_item').value = "";
    document.getElementById('pod_item_id').value = "";
    document.getElementById('pod_qty').value = "";
    document.getElementById('pod_unit').value = "";
    document.getElementById('pod_price').value = "";
    document.getElementById('pod_sgst').value = "";
    document.getElementById('pod_cgst').value = "";
    $('#item_table tr:last').after(newrow);
});
  });
  </script>

  <section class="right-side" style="min-height:700px;"> <?php 
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       Product Package
       <small>Create Product Package</small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."mpackage"; ?>">Product Package</a></li>
      <li class="active"> <?=$page_title ?>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?=$page_title?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/mpackage/add">
        <i class="fa fa-plus-circle"></i> Create New
      </a>
    </div>
     </div>
<br>
      <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success"> 
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
      <form id="post_form">
     <div class="box-body"> 
      <div class="row">                
       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">Pkg No.<sup></sup></label>
          <input class="form-control input-sm" readonly required="required" tabindex="1"  type="text" name="mt_pkgno" value="<?= $mt_pkgno;?>">                
          <input class="form-control"  type="hidden" name="user_type" value="<?= $user_type;?>">
        </div>
        <div class="col-md-2">
          <label  class="control-label">Package Name<sup>*</sup></label>
          <input  class="form-control input-sm validate[required]" tabindex="2" data-prompt-position="topRight:150" id="mt_pakcage" type="text" name="mt_pakcage">                
        </div>

        <div class="col-md-2">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Supplier ID<sup></sup></label>
         <input class="form-control input-sm validate[required]" tabindex="2" data-prompt-position="topRight:150" type="text" name="mt_supplier" id="sp_id">     
       </div>
       <div class="col-md-2">
         <label  class="control-label">Supplier<sup></sup></label>
         <input  class="form-control input-sm validate[required]"  tabindex="3" data-prompt-position="topRight:150" type="text" id="sp_vendor">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Supplier Ph.<sup></sup></label>
         <input class="form-control input-sm" tabindex="4" type="text" id="sp_phone">
       </div>
     </div>

     <div class="col-md-12">
      <br>
      <div class="col-md-4">
        <div class="form-group required">
          <label  class="control-label">Item<sup></sup></label>
          <input class="form-control input-sm"  tabindex="5"  type="text" id="pod_item_name">
          <input class="form-control"  type="hidden" id="pod_item">
          <input class="form-control"  type="hidden" id="pod_item_id">
          <input class="form-control"  type="hidden" id="pod_sgst">
          <input class="form-control"  type="hidden" id="pod_cgst">
        </div> </div>
        
        <div class="col-md-2">
          <div class="form-group required">
            <label  class="control-label">Quantity<sup></sup></label>
            <input class="form-control input-sm" tabindex="6"  data-prompt-position="topRight:150"  id="pod_qty"  name="mtd_qty">                
          </div> </div>

          <div class="col-md-2">
          <div class="form-group required">
            <label  class="control-label">Unit<sup></sup></label>
            <input class="form-control input-sm"   tabindex="6" readonly type="text" name="mtd_unit" id="pod_unit">
          </div> </div>


          <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Unit Price<sup></sup></label>
              <input class="form-control input-sm"  tabindex="6" id="pod_price"  type="text" name="mtd_price">
               </div> </div>


            </div>
          </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-md-12">
                <div>
                  <a class="button btn btn-primary" >Add to Table</a>
                  <input class="btn-large btn-default btn" tabindex="6" type="reset" value="Reset">
                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
 
                    <table class="table table-striped table-bordered table-hover gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Product</th>
                        <th>Unit Price</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                  <tbody> 
                  </tbody>
                  </table> 
                
   <button class="btn-large btn-success btn" type="submit" tabindex="7"  onclick="save();" name="submit" accesskey="s" title="short key-ALT+S"><i class='fa fa-floppy-o'></i> Save</button>
   <input type="hidden" id="Printid">

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
</div>
</section>

</section><!-- /.right-side -->

<script>

function save(){

        var mt_pakcage = $("#mt_pakcage").val();
        var sp_id      = $("#sp_id").val();
        var sp_vendor  = $("#sp_vendor").val();

        if(mt_pakcage== "" || sp_id=="" || sp_vendor=="")
        {
          alert("Empty Fields");
        }
        else
        {
          var postData = $("#post_form").serializeArray();
          var formURL  = "<?= base_url() ?>mpackage/mpackageAdd";

          $.ajax(
          {
            url : formURL,
            type: "POST",
            data : postData,
            success:function(data, textStatus, jqXHR)
            {
              window.location = "<?php echo base_url();?>index.php/mpackage/add";
              <?php 
              $this->session->set_flashdata('Success','Medicine Package Created'); ?>
            }
          });
        }
      
}

    </script>

<script>
 $('.datepicker').datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true
    });</script>