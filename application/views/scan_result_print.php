<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<html>
<head>
<link rel="stylesheet" href='<?= base_url() ?>asset/plugins/jvectormap/jquery-jvectormap-1.2.2.css' type="text/css"/>
  <link media="all" type="text/css" rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.css">
   <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.min.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.min.css' type="text/css"/>
   <link rel="stylesheet" href="<?= base_url() ?>asset/css/skins/_all-skins.min.css"> 
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
 
  <!-- <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datepicker/datepicker3.css"> -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<!-- <link rel="stylesheet" href="<?= base_url() ?>asset/css/AdminLTE.min.css" type="text/css"/>  -->
   <link rel="stylesheet" href="<?= base_url() ?>asset/bootstrap/css/AdminLTE.min.css" type="text/css"/>
   <script  src="<?= base_url() ?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
   <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
   <style type="text/css">
   .font_reduce
   {
     font-size:small;
   }
   .font_reduce1
   {
     font-size:x-small;
   }
   .font_reduce2
   {
     font-size:xx-small;
   }

   .u {
    text-decoration: underline;
}
   </style>
</head>
 <body onload="window.print();" style="margin-top:0%;display:block;height:100%;width:80%;margin-left:9%;margin-right:9% ">

<!-- onload="window.print();"  -->
     <?php foreach($company as $company) {
    $company_name     = $company['company'];
    $company_address  = $company['address'];
    $company_street   = $company['street'];
    $company_city     = $company['ctname'];
    $company_state    = $company['stname'];
    $company_country  = $company['coname'];
    $company_zip      = $company['zip'];
    $company_phone    = $company['phone'];
    $company_email    = $company['email'];
    $company_website  = $company['website'];
    $company_gst_no   = $company['gst_no'];
    $company_drug_lic = $company['drug_lic'];
    $company_tags     = $company['tags'];
}

foreach ($scan_bills as $key => $scan_bill) {

    $ve_id            = $scan_bill['ve_id'];
    $ve_vno           = $scan_bill['ve_vno'];
    $ve_date          = $scan_bill['ve_date'];
    $ve_customer      = $scan_bill['ve_customer'];
    $ve_mrd           = $scan_bill['ve_mrd'];
    $p_name           = $scan_bill['ve_patient'];
    $p_phone          = $scan_bill['ve_phone'];
    $p_age            = $scan_bill['ve_amount'];
    $p_gender         = $scan_bill['ve_pono'];
    $ve_apayable      = $scan_bill['ve_apayable'];
    $ve_apaid         = $scan_bill['ve_apaid'];
    $ve_type          = $scan_bill['ve_type']; 
    $doctor           = $scan_bill['ve_doctor']; 
  }
?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>


<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h4 style="border-bottom: thin dotted #000; font-family: Arial Black, Georgia, Serif; padding-bottom:5px">
          <center class="font_reduce"><b><?= strtoupper($company_name) ?></b></center>
          <small class="font_reduce2"><center><b><?= "[ ".strtoupper($company_tags)." ]" ?></b></center></small>
           <center class="pull-center font_reduce2"><?= strtoupper($company_address).", ".strtoupper($company_street)." ,".strtoupper($company_city)." - ".$company_zip.", PH. : ".$company_phone  ?></center>
           <center class="pull-center font_reduce2"><?= "GST : ".strtoupper($company_gst_no).", Drug Lic No : ".strtoupper($company_drug_lic) ?> </center> </b>
                    </h4>

 <center><small class="pull-center"><b><u>SCAN RESULT</u></b></small></center>


  <div class="box-tools">
              <a title="Back" class="dontprint btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."scanning"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>
    </div> </div>
    <!-- info row -->

      <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">
          <table class="font_reduce2" width="100%">
            <tr>
            <th width="20%">Bill No.    : <?= $ve_vno?></th>
            <th width="20%">Date        : <?= date("d-m-Y",strtotime($ve_date))?></th>
            <th width="25%" colpsan="4">Doctor      : <?= $doctor?></th>
          </tr>
            <tr>
             <?php if($ve_type=="lbi") { ?>
            <th width="20%">IP No.      : <?= $ve_customer?> </th>
            <?php } ?>
            <th width="20%">MRD         : <?= $ve_mrd?> </th>
            <th width="20%">Patient     : <?= $p_name?> </th>
            <th width="20%">Phone No.   : <?= $p_phone?> </th>
            <th width="10%">Gender   : <?php
              if($p_gender=="M") { echo "Male"; } 
              if($p_gender=="F") { echo "Female"; }
              if($p_gender=="C") { echo "Child"; }
           ?></th>
            <th width="10%">Age.   : <?= $p_age?> </th>
          </tr>
           </table>
      </div>

    </div>
      <!-- /.col -->

    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 ">
       <table width="100%" class="font_reduce2" style="border-top: thin dashed #000;border-bottom: thin dashed #000; margin-bottom:10px">
          <thead>
                      <tr style="border-bottom: thin dashed #000">
                        <th height="25px">Sl No.</th>
                        <th width="40%">Particular</th>
                        <th>Value</th>
                        <th>Normal Value</th>
                        <th>Remarks</th>
                      </tr>
                    </thead>
                   <tbody> 
                    <?php
                    $slno=1;
                    $ts_test = "";
                    foreach ($scan_bills as $key => $scan_bill)
                      { ?> 
                    <tr>
                      <th height="10px" colspan="3" style="text-align:center"><u><?php if($ts_test != $scan_bill['sc_test'])
                      {
                        echo $scan_bill['sc_test'];
                      }  ?>
                    </u></th> <tr>
                     <?php  $ts_test = $scan_bill['sc_test']; ?>
                     <tr>  <th height="10px"><?= "[".$slno."]" ?></th><th colspan="2"><?=$scan_bill['ved_item'] ?></th></tr>
                      <?php
                      $slno1=1;
                      foreach ($scan_contents as $key => $scan_content)
                      {                         
                        foreach ($scan_content['scnt_content'] as $value)
                      { 
                        if($value['scnt_scname']==$scan_bill['ved_itemid']) {
                        ?>
                      <tr>
                        <td height="15px"><?=$slno1.")" ?></td>
                        <td><?=$value['ind_content'] ?></td>
                        <td><?=$value['ind_result'] ?></td>
                         <?php if($p_gender=="M") { ?><td><?=$value['scnt_men']?></td> <?php } ?>
                         <?php if($p_gender=="F") { ?><td><?=$value['scnt_women'] ?></td> <?php } ?>
                         <?php if($p_gender=="C") { ?><td><?=$value['scnt_chid'] ?></td> <?php } ?>
                        <td><?=$value['ind_remarks'] ?></td>
                      </tr>
                    <?php $slno1++;
                  } }
                  } $slno++;  }?>
                  </tbody>
                  </table> 
                
        </div>

        <div class="col-xs-12 font_reduce2" style="text-align:right">
          Authorized Signature  with Seal<br><br><br><br><br><br><br>
      </div>
      <!-- /.col -->
    </div>

     </div>
       
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
 
 <style type="text/css" media="print">
.dontprint
{ display: none; }
</style>
</body>
</html>