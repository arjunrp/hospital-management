<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#customer_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       MRD Room
       <small>Adjustment Edit</small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."mrd_room"; ?>">MRD Room</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div class="box-tools">
      <a class="btn btn-sm btn-success pull-right  view-btn-create" href="<?php echo base_url();?>index.php/mrd_room">
        <i class="fa fa-reply-all"></i> Back
      </a>
    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>

      <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row"> 
       <div class="col-md-12">
        <div class="col-md-2">
          <input type="hidden" name="mr_id" value="<?=$mr_id ?>">
          <label  class="control-label">Date<sup>*</sup></label><br><?=$mr_date ?>   
        </div>
      
       <div class="col-md-2">
         <label  class="control-label">MRD<sup>*</sup></label><br><?=$mr_mrd ?> 

       </div>
       <div class="col-md-2">
         <label  class="control-label">OP No.<sup></sup></label><br><?=$mr_tno ?> 
       </div>
       <div class="col-md-2">
         <label  class="control-label">Patient's Name<sup></sup></label><br><?=$mr_patient ?> 
       </div>
       <div class="col-md-2">
         <label  class="control-label">Phone No.<sup></sup></label><br><?=$mr_phone ?> 
       </div>
        <div class="col-md-2">
         <label  class="control-label">Doctor<sup>*</sup></label>
         <select class="form-control validate[required] input-sm" id="op_doctor" tabindex="5" tabindex="3" name="mr_doc">
          <option value="0">--Select--</option>
             <?php foreach ($doctors as $key => $doctors) {
                ?>
              <option value="<?php echo $doctors['u_emp_id']?>" <?php if($mr_doc == $doctors['u_emp_id']) { ?> selected <?php } ?>><?php echo $doctors['u_name']?></option>
                <?php
              }?> 
            </select>    
       </div>
      </div>
      <div class="col-md-12">
       <div class="col-md-4">
         <label  class="control-label">Remark<sup></sup></label>
         <textarea class="form-control input-sm"  tabindex="6" name="mr_remark"><?=$mr_remark ?> </textarea>
       </div>

     </div>
          </div>
          <br><br>
          <div class="box-footer">
            <div class="row">
              <div class="col-md-12">
                <div>
                  <button class="button btn btn-primary" tabindex="7" type="submit">Transfer from MRD Room</button>
                  <input class="btn-large btn-default btn" type="reset" tabindex="utf-8" value="Reset">
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php echo form_close(); ?>

</div>
</section>

</section><!-- /.right-side -->


<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });</script>
<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>