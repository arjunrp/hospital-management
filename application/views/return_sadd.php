<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script type="text/javascript" charset="utf-8">
 $(document).ready(function() {

  $('input[type="checkbox"]'). click(function(){
  if($(this). prop("checked") == true){
  $("#payment_type").val("True");
  }
  else if($(this). prop("checked") == false){
  $("#payment_type").val("False");
  } 
  });

  $("#billRNumber").change(function() {
    var bRNo = $("#billRNumber").val();
    // alert("<?= base_url() ?>preturn/returnSBillno/"+bRNo)
        $.ajax({
        dataType: "json",
        url: "<?= base_url() ?>preturn/returnSBillno/"+bRNo,
        success: function(data){
          if(data==0)
          {
            $('#ve_mrd').val("");
            $('#ve_customer').val("");
            $('#ve_patient').val("");
            $('#ve_phone').val("");
            $('#ved_id').empty();
          }
          else
          {
        jQuery.each(data, function(index, itemData) {
            $('#ve_mrd').val(itemData.ve_mrd);
            $('#ve_customer').val(itemData.ve_customer);
            $('#patient').val(itemData.ve_patient);
            $('#patientph').val(itemData.ve_phone);
          });
          $('#ved_id').empty();
          $("#ved_id").append('<option value=0>Select Item</option>');
          $.each(data, function(index) {
          $("#ved_id").append('<option value=' + data[index].ved_id +'>'+data[index].ved_item+" - "+data[index].ved_batch+'</option>');
          });
        }
        }
      })

  });

  $("#ved_id").change(function() {
    var bRNo = $("#billRNumber").val();
    var r_item=$('#ved_id').val();
    // var sel = document.getElementById("ved_itemid");
    // var value = sel.options[sel.selectedIndex].value; // or sel.value
    // var text = sel.options[sel.selectedIndex].text; 

        $.ajax({
        dataType: "json",
        url: "<?= base_url() ?>preturn/returnSPrice/"+bRNo+"/"+r_item,
        success: function(data){
          
          $('#ved_sunit').empty();
          $("#ved_sunit").append('<option value="0">--Select--</option>');
        jQuery.each(data, function(index, itemDatas) {
             var qunit = itemDatas.ved_unit; 
            $('#ved_itemid').val(itemDatas.ved_itemid);
            $('#ved_item').val(itemDatas.ved_item);
            $('#ved_batch').val(itemDatas.ved_batch);
            $('#ved_expiry').val(itemDatas.ved_expiry);
            $('#ved_price').val(itemDatas.ved_price);
            if(qunit!="No.s")
            {
              $('#ved_pqty').val(itemDatas.ved_qty/itemDatas.ved_uqty);
            }
            else
            {
              $('#ved_pqty').val(itemDatas.ved_qty);
            }
            $('#ved_unit').val(qunit);
            $('#ved_unit1').val(itemDatas.ved_unit);
            $('#ve_pdate').val(itemDatas.ve_date);
            $('#ve_cgstp').val(itemDatas.ved_cgstp);
            $('#ve_sgstp').val(itemDatas.ved_sgstp);
            $('#pd_qty').val(itemDatas.pd_qty);
            $("#ved_sunit").append('<option value=' + itemDatas.ved_unit +'>-- '+itemDatas.ved_unit+' --</option>');
            if(qunit != "No.s") {
              $("#ved_sunit").append('<option value="No.s">-- No.s --</option>');
            }
          });
          }
        })
    });

  $("#ved_qty").change(function() {
    var ved_qty     = $("#ved_qty").val();
    var ved_pqty    = $('#pd_qty').val();
    var chk_qty     = $('#chk_qty').val();
    var ved_sunit   = $('#ved_sunit').val();
    var ved_unit1   = $('#ved_unit1').val();

    var ved_price  = $('#ved_price').val();

        if(parseFloat(ved_qty)>parseFloat(chk_qty))
        {
          alert("Quantity Mismatch");
          $("#ved_qty").val("");
          $("#ved_qty").focus();
        }
        // alert(ved_price+","+ved_pqty)
        if(ved_sunit=="No.s" && ved_unit1!="No.s") {
          var chk_price = (Math.round(parseFloat(ved_price) / parseFloat(ved_pqty)*100)/100).toFixed(2);
        }
        else
        {
          var chk_price = (Math.round(parseFloat(ved_price) *100)/100).toFixed(2);
        }
        // var chk_price = Math.round()
         $('#chk_price').val(chk_price);


  });

  $('#ved_sunit').change(function(){
      var ved_sunit       = $('#ved_sunit').val();
      var ved_unit1       = $('#ved_unit1').val();
      var pd_qty          = $('#pd_qty').val();
      $('#ved_qty').val("");
      $('#chk_price').val("");  
      if(ved_sunit=="No.s" && ved_unit1=="No.s")
      {
        var pd_stock  = $('#ved_pqty').val();
      }
      else if(ved_sunit=="No.s" && ved_unit1!="No.s")
      {
        var pd_stock   = $('#ved_pqty').val() * $('#pd_qty').val();
      }
      else if(ved_sunit!="No.s" && ved_unit1!="No.s")
      {
        var pd_stock  = $('#ved_pqty').val();
      }

      $('#chk_qty').val(pd_stock);
    });

  });
  </script>


<!-- Product Delete -->
<script>
  $(document).ready(function() {

  $(document).on('click','button.dlte', function() {
    var $row      = $(this).closest("tr");    // Find the row
  
    var r = confirm("Are you sure want to delete!");
    if (r == true) {
    $(this).closest('tr').remove();
    var stotal = 0;
    var ved_sgsta = 0;
    var ved_cgsta = 0;
    var ftotal = 0;
    $('.stotal').each(function(){
        stotal += parseFloat(this.value);
    });
    $('.ved_sgsta').each(function(){
        ved_sgsta += parseFloat(this.value);
    });
    $('.ved_cgsta').each(function(){
        ved_cgsta += parseFloat(this.value);
    });
    $('.ftotal').each(function(){
        ftotal += parseFloat(this.value);
    });
    sum           =   (Math.round(stotal * 100) / 100).toFixed(2);
    tsgsta        =   (Math.round(ved_sgsta * 100) / 100).toFixed(2);
    tcgsta        =   (Math.round(ved_cgsta * 100) / 100).toFixed(2);
    grand         =   (Math.round(ftotal * 100) / 100).toFixed(2);

    grand         =  (Math.round(grand * 100) / 100).toFixed(2);

    gtotal        = Math.round(grand);

    var roundoff    =   parseFloat(gtotal) - parseFloat(grand);
    roundoff        =   (Math.round(roundoff * 100) / 100).toFixed(2);


    $('#sum').val(sum);
    $('#sum1').html(sum);

    $('#cgsta').val(tcgsta);
    $('#cgsta1').html(tcgsta);
    $('#sgsta').val(tsgsta);
    $('#sgsta1').html(tsgsta);
    $('#apayable').val(grand);
    $('#apayable1').html(grand);
    $('#gtotal').val(gtotal);
    $('#gtotal1').html(gtotal);
    
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);
    } 
    });
  });
  </script>
<!-- Product Add -->
  <script>
  $(document).ready(function() {
  $('.button').click(function() {

    var sgstp       = $('#ve_sgstp').val();
    var cgstp       = $('#ve_cgstp').val();
    var item        = $('#ved_item').val();
    var itemid      = $('#ved_itemid').val();
    var batch       = $('#ved_batch').val();
    var expiry      = $('#ved_expiry').val();
    var qty         = $('#ved_qty').val();
    var uqty        = $('#pd_qty').val();
    var unt         = $('#ved_sunit').val();
    var price       = $('#chk_price').val();
    var total       = $('#ved_qty').val() * $('#chk_price').val();

    var sgsta       = parseFloat(total) * parseFloat(sgstp) / 100;
    var cgsta       = parseFloat(total) * parseFloat(cgstp) / 100;
    var amount      = parseFloat(total) + parseFloat(sgsta) + parseFloat(cgsta);

    var sgsta       = (Math.round(sgsta * 100) / 100).toFixed(2);
    var cgsta       = (Math.round(cgsta * 100) / 100).toFixed(2);  
    var amount      = (Math.round(amount * 100) / 100).toFixed(2);    

    var newrow      = '<tr><td><input type="hidden" value="'+itemid+'" name="ved_itemid[]"><input type="hidden" value="'+item+'" name="ved_item[]">' + item + '</td><td><input type="hidden" value="'+price+'" name="ved_price[]">' + price + '</td><td><input type="hidden" value="'+qty+'" name="ved_qty[]">' + qty + '<input type="hidden" value="'+uqty+'" name="ved_uqty[]"></td><td><input type="hidden" value="'+unt+'" name="ved_unit[]">' + unt + '</td><td><input type="hidden" value="'+batch+'" name="ved_batch[]">' + batch + '</td><td><input type="hidden" value="'+expiry+'" name="ved_expiry[]">' + expiry + '</td><td><input class="stotal" type="hidden" value="'+total+'" name="ved_total[]">' + total + '</td><td><input class="ved_sgsta" type="hidden" value="'+sgsta+'" name="ved_sgsta[]">' + sgsta +'<input type="hidden" value="'+sgstp+'" name="ved_sgstp[]"> (' + sgstp +' %)</td><td><input class="ved_cgsta" type="hidden" value="'+cgsta+'" name="ved_cgsta[]">' + cgsta +'<input type="hidden" value="'+cgstp+'" name="ved_cgstp[]"> (' + cgstp +' %)</td><td><input class="ftotal" type="hidden" value="'+amount+'" name="ved_gtotal[]">' + amount + '</td><td><button class="btn btn-xs btn-danger dlte"><i class="fa fa-times-circle-o"></i></button></td></tr>';

    var sum         = $('#sum').val();
    var tcgsta      = $('#cgsta').val();
    var tsgsta      = $('#sgsta').val();

    var sum       = parseFloat(sum) + parseFloat(total);
    var tcgsta    = parseFloat(tcgsta) + parseFloat(cgsta);
    var tsgsta    = parseFloat(tsgsta) + parseFloat(sgsta);
    var apayable  = parseFloat(sum) + parseFloat(tcgsta) + parseFloat(tsgsta);
    apayable      = (Math.round(apayable * 100) / 100).toFixed(2);
    grand         = Math.round(apayable);

    sum       = parseFloat(Math.round(sum * 100) / 100).toFixed(2);
    tcgsta    = parseFloat(Math.round(tcgsta * 100) / 100).toFixed(2);
    tsgsta    = parseFloat(Math.round(tsgsta * 100) / 100).toFixed(2);

    roundoff  = Math.round(apayable) - parseFloat(apayable);
    roundoff  = parseFloat(Math.round(roundoff * 100) / 100).toFixed(2);

    $('#sum').val(sum);
    $('#sum1').html(sum);
    $('#cgsta').val(tcgsta);
    $('#cgsta1').html(tcgsta);
    $('#sgsta').val(tsgsta);
    $('#sgsta1').html(tsgsta);
    $('#apayable').val(apayable);
    $('#apayable1').html(apayable);
    $('#gtotal').val(grand);
    $('#gtotal1').html(grand);
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);

    $('#item_table tr:last').after(newrow);

  
});
  });
  </script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       Return
       <small>Sales Return</small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."preturn/return_Slist"; ?>">Sales Return</a></li>
      <li class="active"> <?=$page_title ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?=$page_title ?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/preturn/return_sadd">
        <i class="fa fa-plus-circle"></i> Create New
      </a>

    </div>
     </div>

      <form id="post_form">
     <div class="box-body">
        
      <div class="row"> 
       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">Return No.<sup></sup></label>
          <input class="form-control input-sm" readonly  type="text" name="ve_vno" value="<?= $returnid;?>">   
          <input class="form-control"  type="hidden" name="user_type" value="<?= $user_type;?>">               
        </div>
        <div class="col-md-2">
          <label  class="control-label">Return Date<sup></sup></label>
          <input class="form-control validate[required] input-sm"  type="text" name="ve_date" id="datepicker" value="<?=date("Y-m-d")?>" readonly> 
        </div>

           <div class="col-md-2">
         <label  class="control-label">Bill No.<sup></sup></label>
         <input class="form-control input-sm"  tabindex="1" type="text" name="ve_bill_no" id="billRNumber"> 
       </div>

       <div class="vend">
        <div class="col-md-2">
         <label  class="control-label">Patient MRD<sup></sup></label>
         <input class="form-control input-sm" readonly  type="text" name="ve_mrd" id="ve_mrd">
          <input class="form-control"  type="hidden" name="ve_customer" id="ve_customer">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Patient<sup></sup></label>
          <input class="form-control"  type="hidden" name="ve_supplier" value="3">
         <input class="form-control input-sm" readonly  type="text" id="patient" name="ve_patient">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Ph. No<sup></sup></label>
         <input class="form-control input-sm" readonly  type="text" id="patientph" name="ve_phone">
       </div></div>
     </div>
     </div>
     <div class="row"> 
     <div class="col-md-12">
      <div class="col-md-4">
        <div class="form-group required">
          <label  class="control-label">Item<sup></sup></label>
          <select  id="ved_id" class="form-control input-sm"  tabindex="2">
          </select>
          <input class="form-control" type="hidden" id="ved_itemid" name="ved_itemid">
          <input class="form-control" type="hidden" id="ved_item" name="ved_item">  
          <input class="form-control" type="hidden" id="ve_sgstp" name="ve_sgstp">  
          <input class="form-control" type="hidden" id="ve_cgstp" name="ve_cgstp">  
          <input class="form-control" type="hidden" id="pd_qty">   
          <input class="form-control" type="hidden" id="chk_qty">      
          <input class="form-control" type="hidden" id="chk_price">          
        </div> </div>
        <div class="col-md-4"></div>

          <div class="col-md-2">
          <div class="form-group required">
            <label  class="control-label">Unit<sup></sup></label>
              <select class="form-control input-sm validate[required]"  tabindex="4" id="ved_sunit" name="ved_unit">
                <option value="0">--Select--</option>
              </select>
          </div> </div>

         <div class="col-md-2">
          <div class="form-group required">
            <label  class="control-label">Quantity<sup></sup></label>
            <input class="form-control validate[required] input-sm"  tabindex="5" data-prompt-position="bottomRight:150" name="ved_qty" id="ved_qty">                
          </div> </div>


        <div class="col-md-1 vend cust">
          <div class="form-group required">
            <label  class="control-label vend">Saled<sup></sup></label>
            <input class="form-control input-sm" type="input"  readonly  data-prompt-position="bottomRight:150" id="ved_pqty">            
          </div> </div>
          <div class="col-md-2 vend cust">
          <div class="form-group required">
            <label  class="control-label">Unit<sup></sup></label>
            <input class="form-control input-sm" id="ved_unit" readonly  type="text">
            <input class="form-control input-sm" id="ved_unit1" readonly  type="hidden">
          </div> </div>
          <div class="col-md-2 vend cust">
          <div class="form-group required">
            <label class="control-label">Date<sup></sup></label>
            <input class="form-control vend cust input-sm"  readonly  data-prompt-position="bottomRight:150" id="ve_pdate">                
          </div> </div>

          <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Batch<sup></sup></label>
              <input class="form-control input-sm" readonly id="ved_batch" name="ved_batch"  type="text">
            </div> </div>
            <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Expiry<sup></sup></label>
              <input class="form-control input-sm" readonly id="ved_expiry" name="ved_expiry"  type="text">  
            </div> </div>

          <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Unit Price<sup></sup></label>
              <input class="form-control input-sm" readonly id="ved_price" name="ved_price"  type="text">             
            </div> </div>

            <div class="col-md-1">
        <label  class="control-label">Credit<sup></sup></label><br>
         <input type="checkbox" tabindex="2">
         <input id="payment_type" type="hidden" name="payment_type" value="False">
       </div>

            
          <div class="col-md-3">
              <label>&emsp;</label><br>
                  <a class="button btn btn-primary"  tabindex="5" >Add to Table</a>
                  <input class="btn-large btn-default btn" tabindex="6" type="reset" value="Reset">
          </div></div></div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Product</th>
                        <th>Unit Price</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                        <th>Batch</th>
                        <th>Expiry</th>
                        <th>Sub Total</th>
                        <th>SGST</th>
                        <th>CGST</th>
                        <th>Sub Total(Inc. GST)</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                  <tbody> 
                  </tbody>
                  </table> 
                  <table class="table table-bordered">
                    <tr><th width="85%">Total</th>
                      <th><input type="hidden" name="sum" readonly id="sum" value="0">Rs. <label id="sum1">0</label></th></tr>
                      <tr><th width="85%">Total CGST</th>
                      <th><input type="hidden" name="cgsta" readonly id="cgsta" value="0">Rs. <label id="cgsta1">0</label></th></tr>
                      <tr><th width="85%">Total SGST</th>
                      <th><input type="hidden" name="sgsta" readonly id="sgsta" value="0">Rs. <label id="sgsta1">0</label></th></tr>
                      <tr><th width="85%">Total (Inc. GST)</th>
                      <th><input type="hidden" name="apayable" readonly id="apayable" value="0">Rs. <label id="apayable1"></label></th></tr>
                      <tr><th width="85%">Round Off</th>
                      <th><input type="hidden" name="roundoff" readonly id="roundoff" value="0">Rs. <label id="roundoff1"></label></th></tr>
                      <tr><th width="85%">Amount Payable</th>
                      <th><input type="hidden" name="gtotal" readonly id="gtotal" value="0">Rs. <label id="gtotal1"></label></th></tr>
                    </table>
   
<a href="#" class="btn btn-success" onclick="save();" accesskey="s" title="short key-ALT+S"><i class="fa fa-floppy-o"></i> Save</a>
<input type="hidden" id="Printid">
<a href="#" class="btn btn-warning" onclick="print();" accesskey="p" title="short key-ALT+P" ><i class="fa fa-print"></i> Save and Print</a>
<!-- <input class="btn-large btn-primary btn" type="submit" value="Save and Print" name="submit">  -->

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
</div>
</section>

</section><!-- /.right-side -->

<script>

function save(){

      var postData = $("#post_form").serializeArray();
      var formURL  = "<?= base_url() ?>preturn/returnSAdd";

      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          // alert(data);
          $('#Printid').val(data);
           window.location = "<?php echo base_url();?>index.php/preturn/return_Sadd";
        }
      });
}
function print(){

      var postData = $("#post_form").serializeArray();
      var formURL  = "<?= base_url() ?>preturn/returnSAdd";

      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          // alert(data);
           window.location = "<?php echo base_url();?>index.php/preturn/getSprint?sPrintid="+data;
        }
      });

}
</script>
<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'dd-mm-yyyy',
      today:true
    });</script>
