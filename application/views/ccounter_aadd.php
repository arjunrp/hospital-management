<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
 
<link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});

</script>

<?php
    $user_type = $this->session->id;
    ?>

<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
      Advance Payment
      <small>
       <?php echo $page_title; ?></small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."advance"; ?>">Advance Payment</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div align="right">
         <a title="short key-ALT+B" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."ccounter"; ?>"accesskey="b" ><i class="fa fa-mail-reply-all"></i> Back</a>
       </div>

     </div>
      <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row">
       <div class="col-md-12">
        <div class="col-md-2">
        <div class="form-group required">
          <label  class="control-label">Voucher No : <sup></sup></label> <?=$ve_vno?>               
        </div> </div>
        <!-- <div class="col-md-8"></div> -->
        <div class="col-md-2">
        <div class="form-group required">
         <label  class="control-label">Date : <sup></sup></label> <?=$ve_date ?>             
        </div></div> 
        <div class="col-md-2">
         <div class="form-group required">
          <label  class="control-label">IP No. : <sup></sup></label> <?=$ve_customer?> 
        </div> </div>

        <div class="col-md-2">
         <div class="form-group required">
          <label  class="control-label">MRD No. :<sup></sup></label> <?=$ve_mrd?> 
        </div> </div>

        <div class="col-md-3">
       <div class="form-group required">
          <label  class="control-label">Name :<sup></sup></label> <?=$p_name?>             
        </div></div>
         <div class="col-md-2">
         <div class="form-group required">
          <label  class="control-label">Amount : <sup></sup></label> <?=$ve_apayable?>  
            <input type="hidden" name="ve_date" value="<?=$ve_date ?>">
            <input type="hidden" name="grandtotal" value="<?=$ve_apayable ?>">
            <input type="hidden" name="ve_id" value="<?=$ve_id ?>">
        </div></div>

    </div>
   <div class="col-md-12">
        <div class="col-md-2">
         <div class="form-group required">
          <button class="btn btn-success" type="submit"><i class="fa fa-floppy-o"></i> Pay</button>
        </div>
       </div></div>
  </div>
</div>
  <?php echo form_close(); ?>
</div>

</section>
  
</section><!-- /.right-side -->

 <script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });</script>
<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>