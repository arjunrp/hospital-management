<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
  oTable = $('#patient_table').dataTable({
    "aaSorting": [[ 0, "dec" ]],
    "bJQueryUI": true,
    "sPaginationType": "full_numbers",
    "iDisplayLength": 10,
    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
  });
});
</script>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     <?php echo $page_title; ?>
    </h1>
     <ol class="breadcrumb">
      <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
      
      <li class="active"><?php echo $page_title; ?></li>
    </ol>&nbsp;&nbsp;
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
              <?php echo form_open_multipart($action) ?>
            <div class="col-md-12"><hr></div>
            
            <div class="row">
              <div class="col-md-12">
                <div class="box-header  ">
                  <center><h3 class="box-title">General Report of <b><?php if($type==1) { echo "All Types"; } else if($type==2) { echo "Customers";}  else if($type==3) { echo "Vendors";}?></b> as <b><?php if($p_type=="2cr" || $p_type=="2de") { echo "Cash Payment Type"; } else if($p_type==0 || $p_type=="cr" || $p_type=="de") { echo "All Payment Types"; } else if($p_type==3) { echo "Debit Payment Type"; } else if($p_type==4) { echo "Credit Payment Type"; }?></b></h3></center>
                <input class="form-control"  type="hidden" name="p_type" value="<?=$p_type?>">
                 <input class="form-control"  type="hidden" name="type" value="<?=$type?>">
                </div>
                <div class="col-md-12"><hr></div>
                <center>     
                <button type="submit" name="submit" class="btn btn-primary" >Print</button>
              </center>

            <div class="row">
              
              <div class="col-md-12"><br>
                  <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-12">
                      <?php if($type==2 || $type==3) { ?>
                      <table id="" class="table table-condensed dataTable no-footer">
                       <thead>
                        <tr>
                          <th>Sl. No.</th>
                          <th<?php if($type==3 || $type==0) { ?> style="display:none" <?php } ?>>Customer</th>
                          <th<?php if($type==3 || $type==0) { ?> style="display:none" <?php } ?>>Recent Sale Date</th>
                          <th<?php if($type==3 || $type==0) { ?> style="display:none" <?php } ?>>Sell Amount</th>
                          <th<?php if($type==2 || $type==0) { ?> style="display:none" <?php } ?>>Vendor</th>
                          <th<?php if($type==2 || $type==0) { ?> style="display:none" <?php } ?>>Recent Purchase Date</th>
                          <th<?php if($type==2 || $type==0) { ?> style="display:none" <?php } ?>>Purchased Amount</th>
                          <th>Payment Type</th>
                          <th>Amount Paid</th>
                          <th>Balance To Pay</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php  echo $output;  ?>
                      </tbody>
             </table>
<?php } ?>

<?php if($type==1) { ?>
             <table class="table table-bordered">
              <tr>
                <td>
                  <table class="table table-bordered">
                    <tr><th style="text-align: center;" colspan=7>Sale</th></tr>
                    <tr>
                  <th>Sl. No.</th>
                  <th>Customer</th>
                  <th>Recent Sale Date</th>
                  <th>Sell Amount</th>
                  <th>Payment Type</th>
                  <th>Amount Paid</th>
                  <th>Balance To Pay</th>
                </tr>
                <?php $slno=1;?>

                         <?php foreach ($output as $output2) {  ?>
                          <tr>
                         <td><?= $slno++ ?> </td>
                         <td><?= $output2['customer'] ?> </td>
                         <td><?= $output2['maxdate1'] ?> </td>
                         <td><?= $output2['pamount1'] ?> </td>
                         <td><?= $output2['payment1'] ?> </td>
                         <td><?= $output2['ppdamount1'] ?> </td>
                         <td><?= $output2['pblamount1'] ?> </td>
                       </tr>
                       <?php }  ?>
              </table>
              </td>
              <td>
                <table class="table table-bordered">
                  <tr><th style="text-align: center;" colspan=7>Purchase</th></tr>
                    <tr>
                  <th>Sl. No.</th>
                  <th>Vendor</th>
                  <th>Recent Purchase Date</th>
                  <th>Purchased Amount</th>
                  <th>Payment Type</th>
                  <th>Amount Paid</th>
                  <th>Balance To Pay</th>
                </tr>
                <?php $slno1=1;?>

                         <?php foreach ($output as $output1) {  ?>
                          <tr>
                         <td><?= $slno1++ ?> </td>
                         <td><?= $output1['vendor'] ?> </td>
                         <td><?= $output1['maxdate'] ?> </td>
                         <td><?= $output1['pamount'] ?> </td>
                         <td><?= $output1['payment'] ?> </td>
                         <td><?= $output1['ppdamount'] ?> </td>
                         <td><?= $output1['pblamount'] ?> </td>
                       </tr>
                       <?php  } ?>
              </table>
              </td>
              </tr>
             </table>
             <?php } ?>

             <?php if($type==2 || $type==3) { ?>
             <table class="table table-bordered">
              <?php
                  $samount=0;
                  $spdamount=0;
                  $sblamount=0;

                  if (is_array($output1) || is_object($output1))
                  {
                  foreach($output1 as $tsum)
                  {
                   if($type==3) { 
                    $samount    = $samount   + $tsum['samount'];
                    $spdamount  = $spdamount + $tsum['spdamount'];
                    $sblamount  = $sblamount + $tsum['sblamount'];
                    }
                    else if($type==2) {     
                    $samount    = $samount   + $tsum['samount1'];
                    $spdamount  = $spdamount + $tsum['spdamount1'];
                    $sblamount  = $sblamount + $tsum['sblamount1']; 
                  }
                  }
                }
              ?>
              <thead>
                <tr><th width="85%">Total <?php if($type==3) { echo "Purchase"; } else if($type==2) { echo "Sale"; }?> Amount</th><th><?="Rs. ".$samount ?></th></tr>
                <tr><th width="85%">Total Amount Paid</th><th><?="Rs. ".$spdamount ?></th></tr>
                <tr><th width="85%">Total Balance to Pay</th><th><?="Rs. ".$sblamount ?></th></tr>
              </thead>
             </table>
             <?php } ?>

            <!--  <?php if($type==1) { ?>
             <table class="table table-bordered">
              <?php
                  $spamount=0;
                  $sppdamount=0;
                  $spblamount=0;
                  $spamount1=0;
                  $sppdamount1=0;
                  $spblamount1=0;

                  if (is_array($output1) || is_object($output1))
                  {
                  foreach($output1 as $tsum)
                  {
                    $spamount    = $spamount   + $tsum['spamount'];
                    $sppdamount  = $sppdamount + $tsum['sppdamount'];
                    $spblamount  = $spblamount + $tsum['spblamount'];    
                    $spamount1    = $spamount1   + $tsum['spamount1'];
                    $sppdamount1  = $sppdamount1 + $tsum['sppdamount1'];
                    $spblamount1  = $spblamount1 + $tsum['spblamount1']; 
                  }
                }
              ?>
              <tr>
                <td>
                  <table class="table table-bordered">
                    <tr><th width="85%">Total Sale Amount</th><th><?="Rs. ".$spamount1 ?></th></tr>
                    <tr><th width="85%">Total Amount Paid</th><th><?="Rs. ".$sppdamount1 ?></th></tr>
                    <tr><th width="85%">Total Balance to Pay</th><th><?="Rs. ".$spblamount1 ?></th></tr>
              </table>
              </td>
              <td>
                <table class="table table-bordered">
                  <tr><th width="85%">Total Purchase Amount</th><th><?="Rs. ".$spamount ?></th></tr>
                    <tr><th width="85%">Total Amount Paid</th><th><?="Rs. ".$sppdamount ?></th></tr>
                    <tr><th width="85%">Total Balance to Pay</th><th><?="Rs. ".$spblamount ?></th></tr>
              </table>
              </td>
              </tr>
             </table>
             <?php } ?> -->

        </div>
      </div>
    </div>

  </div>
</div>

</div>
</div>

<?php echo form_close(); ?>    

</div></div></div>
</div></div></div>
</section></div>


<script>
 $('#datepicker').datepicker({
      autoclose: true
    });</script>


    <script>
 $('#datepicker2').datepicker({
      autoclose: true
    });</script>



