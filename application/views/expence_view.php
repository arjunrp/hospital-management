<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>

<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
      Expense
      <small>View   Expense </small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."expence"; ?>"> expence</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">

      
      <div class="box-header">


        <h3 class="box-title">
         <i class="fa fa-th"></i> Expence
       </h3>

       <div align="right">
         <!--  <a title="Edit" class="btn btn-sm btn btn-info" href="<?php echo $this->config->item('admin_url')."users/edit/$id"; ?>"><i class="fa fa-pencil-square-o"></i> Edit</a> -->
        <a title="Back" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."expence"; ?>"><i class="fa fa-mail-reply-all"></i> Back</a>
        </div>
     </div>


     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">
       
            <div class="col-md-2 ">
           <label  class="control-label">Date<sup></sup></label>
            <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control input-sm" id="datepicker" name="ve_date" value=<?=$ve_date  ?> >
                </div>
              </div>

                <div class="col-md-2 ">
                <label  class="control-label">Voucher No.<sup></sup></label>
                  <input type="text" class="form-control input-sm" readonly name="ve_vno" value=<?=$ve_vno  ?> >
                </div>

                <div class="col-md-2 ">
                <label  class="control-label">Bill No.<sup></sup></label>

                  <input type="text" class="form-control input-sm" name="ve_bill_no" value=<?=$ve_bill_no  ?> >
                </div>

          <div class="col-md-3">
            <label  class="control-label">Category<sup></sup></label>
            <select class="form-control input-sm"   name="ve_customer"> 
              <option value="Select">--Select--</option>
              <?php foreach ($categories as $key => $excategory) {
                ?>
              <option value="<?php echo $excategory['id']?>"<?php if($excategory['id']==$ve_customer) { ?> selected="selected" <?php } ?>><?php echo $excategory['category']?></option>
                <?php
              }?>
            </select>               
          </div>


          <div class="col-md-2 ">
            <label  class="control-label">Amount<sup></sup></label>
            <input class="form-control validate[required] input-sm" type="text" name="ve_apayable" value=<?=$ve_apayable  ?>>
             <input class="form-control"  type="hidden" name="ve_id" value="<?=$ve_id ?>">           
          </div>

        </div>
      </div>
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            <!-- <div class="box-tools">
              <a title="Back" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."users"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div> -->
          </div>

        </div>
      </div>
      
    </div>
  </div>
   <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br> 
</section>

</section><!-- /.right-side -->


