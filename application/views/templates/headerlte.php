<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Retail360</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <script src="<?= base_url() ?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?= base_url() ?>asset/plugins/dlist/app.min.js"></script>
    <script src="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url() ?>asset/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<?= base_url() ?>asset/bootstrap/js/bootstrap.min.js"></script>
    <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
    <script src="<?= base_url() ?>asset/New folder/icon-picker.min.js"></script>
    <script src="<?= base_url() ?>asset/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="<?= base_url() ?>asset/js/daterangepicker.js"></script>
    <script src="<?= base_url() ?>assets/admin/validation/js/jquery.js"></script>
    <script src="<?= base_url() ?>assets/admin/login/js/script.js"></script>

    

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>R</b>360</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Retail</b>360</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
           <a href="" class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?= base_url() ?>asset/img/m3.png" class="user-image" alt="User Image">
            <span>Super User<i class="caret"></i></span>
            </a>
            <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header bg-light-blue">
            <img src="<?= base_url() ?>asset/img/m3.png" class="img-circle" alt="User Image" />
            <p>
                Super User - Administrator
                <small>Member since -  May, 2014</small>
            </p>
            </li>
            <li class="user-footer">
            <div class="pull-left">
                <a href="" class="btn btn-default btn-flat">Profile</a>
            </div>
            <div class="pull-right">
                <a href="<?php echo $this->config->item('admin_url')."Login/logout"; ?>" class="btn btn-default btn-flat">Sign out</a>
            </div>
             </li>
             </ul>
            </li>
          <!-- Control Sidebar Toggle Button -->
          <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>
    </nav>
  </header>