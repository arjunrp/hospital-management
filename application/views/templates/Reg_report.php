<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reg_report extends MX_Controller 
{
  public $tab_groups;
  public $form_image;
  public $tab_data;

  function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('logged_in'))
    {
      redirect($this->config->item('admin_url'));
    }
    $this->load->model('Reg_report_model','Reg_report',TRUE);
    $this->load->model('Ccounter_model','Ccounter',TRUE);
    $this->load->model('Sale_model','Sales',TRUE);
    $this->load->model('Purchase_model','Purchase',TRUE);
    $this->load->model('Stock_model','Stock',TRUE);
    $this->load->model('Investigation_model','Investigation',TRUE);
    $this->load->model('Discharge_model','Discharge',TRUE);
    $this->load->model('Advance_model','Advance',TRUE);
    $this->load->model('Opbilling_model','Opbilling',TRUE);
    $this->template->set('title','Sales');
    $this->base_uri       = $this->config->item('admin_url')."ccounter";
  }


  function todays_report(){

    $data['treports']       =  $this->Ccounter->getTodayreport();
     // print_r($data['treports'] );
    // exit();
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('tcash_report',$data);
    $this->load->view('templates/footer');
  }

function ccounterreport(){

    $data['action']        = $this->base_uri.'/total_cash_report';
    $data['action1']       = "";
    $data['treports']      =  0;
    $data['departments']   =  $this->Ccounter->getDepartment();

    $data['from_date1']    =  "";
    $data['to_date1']      =  "";
    $data['dept1']         =  "";

    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('totcash_report',$data);
    $this->load->view('templates/footer');
  }

  function total_cash_report(){

    $data['action']       = $this->base_uri.'/total_cash_report';

    $data['action1']      = $this->base_uri.'/totreport_print';

    $from_date            = date("Y-m-d",strtotime($this->input->post('from_date')));
    $to_date              = date("Y-m-d",strtotime($this->input->post('to_date')));
    $dept                 = $this->input->post('dept');
    $data['treports']     =  $this->Ccounter->getTotalCreport($from_date,$to_date,$dept);

    $data['departments']  =  $this->Ccounter->getDepartment();

    $data['from_date1']   =  $from_date ;
    $data['to_date1']     =  $to_date;
    $data['dept1']        =  $dept;
    // print_r($data['treports']);

    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('totcash_report',$data);
    $this->load->view('templates/footer');
  }

  function totreport_print(){

    $from_date            = date("Y-m-d",strtotime($this->input->post('from_date1')));
    $to_date              = date("Y-m-d",strtotime($this->input->post('to_date1')));
    $dept                 = $this->input->post('dept1');   

    $data['from_date']    = date("d-m-Y",strtotime($from_date));
    $data['to_date']      = date("d-m-Y",strtotime($to_date));
    if($dept == 0 || $dept == "")
    {
      $data['dept']         = "All";
    }
    else
    {
      $data['dept']         = $this->Ccounter->get_department($dept);
    }
    
    $data['treports']     =  $this->Ccounter->getTotalCreport($from_date,$to_date,$dept);

    $this->load->view('totcash_print',$data);
  }

  function treport_print(){

    $data['treports']       =  $this->Ccounter->getTodayreport();
    // print_r($data['treports'] );
    // exit();       
    $this->load->view('tcash_print',$data);
  }

  function cash_PView($sid){

    $data['page_title']    = "Cash Payment";
    $data['action']        = $this->base_uri.'/insert';
    $data['sales']         = $this->Ccounter->getCashview($sid);
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('ccounter_add', $data);
    $this->load->view('templates/footer');  
  }

  function cash_OView($sid){

    $data['page_title']    = "Cash Payment";
    $data['action']        = $this->base_uri.'/insert';
    $data['additionals']   = $this->Opbilling->get_additional_items($sid);
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('ccounter_oadd', $data);
    $this->load->view('templates/footer');  
  }

  function cash_DView($ipno){

    $data['page_title']    = "Cash Payment";
    $data['action']        = $this->base_uri.'/update';
    $data['discharges']       = $this->Discharge->get_discharge($ipno);
    // print_r($data['discharges']);
    $data['rooms']            = $this->Discharge->get_room($ipno);
    $data['voucher_bills']    = $this->Discharge->get_voucher_bills($ipno);
    $data['pharmacy_bills']   = $this->Discharge->get_pharmacy_bills($ipno);
    $data['pharmacy_returns'] = $this->Discharge->get_pharmacy_returns($ipno);
    $data['advances']         = $this->Discharge->get_advances($ipno);
    $data['additionals']       = $this->Discharge->get_additional_items($ipno);
    // print_r($data['sales']);
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('ccounter_dadd', $data);
    $this->load->view('templates/footer');  
  }

  function cash_LView($sid){

    $data['page_title']    = "Cash Payment";
    $data['action']    = $this->base_uri.'/insert';
    $data['lab_bills']    = $this->Investigation->getInvestigationview($sid);
    // print_r($data['sales']);
    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('ccounter_ladd', $data);
    $this->load->view('templates/footer');  
  }

  function cash_AView($sid){

    $data['page_title']    = "Cash Payment";
    $data['action']        = $this->base_uri.'/insert';
    $row                   = $this->Advance->get_one_banner($sid);
    $data['ve_id']         = $row->ve_id;
    $data['ve_vno']        = $row->ve_vno;
    $data['ve_date']       = $row->ve_date;
    $data['ve_customer']   = $row->ve_customer;
    $data['ve_mrd']        = $row->ve_mrd;
    $data['p_name']        = $row->p_title." ".$row->p_name;
    $data['p_phone']       = $row->p_phone;
    $data['ve_apayable']   = $row->ve_apayable;


    $this->load->view('templates/header');  
    $this->load->view('templates/sidebar');        
    $this->load->view('ccounter_aadd', $data);
    $this->load->view('templates/footer');  
  }

   function insert()
  {
    $ve_id                               =  $this->input->post('ve_id');
    $sale_date                           =  $this->input->post('ve_date');
    $amount_paid                         =  $this->input->post('grandtotal');
    $this->tab_groups['ve_apaid']        =  $amount_paid;
    $this->tab_groups['ve_balance']      =  "0";
    $this->tab_groups['ve_pstaus']       =  "FP";
    $this->tab_groups['ve_status']       =  "acc";
    $this->tab_groups['ve_cash_date']    =  date("Y-m-d");

    $this->db->update('voucher_entry',$this->tab_groups,array('ve_id'=>$ve_id));

    $this->Sales->set_sale_balance($amount_paid,$sale_date);

    $this->session->set_flashdata('Success','Cash Received');
    redirect($this->base_uri);
  }

  function update()
  {
    $ve_id                               =  $this->input->post('ve_id');
    $ve_customer                         =  $this->input->post('ve_customer');
    $sale_date                           =  $this->input->post('ve_date');
    $amount_paid                         =  $this->input->post('grandtotal');
    $this->tab_groups['ve_apaid']        =  $amount_paid;
    $this->tab_groups['ve_balance']      =  "0";
    $this->tab_groups['ve_pstaus']       =  "FP";
    $this->tab_groups['ve_status']       =  "acc";
    $this->tab_groups['ve_cash_date']    =  date("Y-m-d");

    $this->Sales->set_sale_balance($amount_paid,$sale_date);
    $this->db->update('voucher_entry',$this->tab_groups,array('ve_id'=>$ve_id));

    $this->db->select('*');
    $this->db->from('voucher_entry');
    $this->db->where('ve_customer',$ve_customer);
    $this->db->where('ve_status','cr');
    $query = $this->db->get();
    $result= $query->result_array();

    foreach ($result as $value) {

      $ve_apaid = $value['ve_apayable'];
      $ve_id    = $value['ve_id'];

      $this->tab_data['ve_apaid']        = $ve_apaid;
      $this->tab_data['ve_balance']      = "0";
      $this->tab_data['ve_pstaus']       = "FP";
      $this->tab_data['ve_status']       = "acr";
      $this->tab_data['ve_cash_date']    =  date("Y-m-d");
      $this->db->update('voucher_entry',$this->tab_data,array('ve_id'=>$ve_id));
    }

    $this->session->set_flashdata('Success','Cash Received');
    redirect($this->base_uri);
  }




}