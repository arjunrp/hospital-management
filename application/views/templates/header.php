<!DOCTYPE html>
<html>
<head>


  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Demo</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <meta name="description" content="">
  <meta property="og:type" content="software">
  <meta property="og:url" content="http://mediware360.rannsinfo.com">
  <meta property="og:image" content="<?= base_url() ?>asset/img/retail360logblack.png">
  <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>asset/img/favicon.ico" >
  <link rel="stylesheet" href='<?= base_url() ?>asset/plugins/jvectormap/jquery-jvectormap-1.2.2.css' type="text/css"/>
  <link media="all" type="text/css" rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.css">
  <!-- <link media="all" type="text/css" rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.min.css"> -->
  <!-- <link rel="stylesheet" href='<?= base_url() ?>asset/cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css' type="text/css"/> -->
  <!-- <link rel="stylesheet" href='<?= base_url() ?>asset/cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css' type="text/css"/> -->
   <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.min.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.min.css' type="text/css"/>
  <link rel="stylesheet" href="<?= base_url() ?>asset/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
  <!-- <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style> -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datepicker/datepicker3.css">

  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>asset/css/AdminLTE.min.css" type="text/css"/>
  <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/select2/dist/css/select2.min.css">
   


    <script  src="<?= base_url() ?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
    
<!-- validation -->

    <script src="<?php echo $this->config->item('base_url');?>assets/admin/validation/js/jquery.js"></script>        
        <link rel="stylesheet" href="<?php echo $this->config->item('base_url');?>assets/admin/validation/css/validationEngine.jquery.css" type="text/css"/>
 <script type="text/javascript" src="<?php echo $this->config->item('base_url');?>assets/admin/validation/js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('base_url');?>assets/admin/validation/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>

            <script>
                $(document).ready(function(){
                    $("form").validationEngine();
                });
            </script>


<!-- validation -->


    <script src="<?= base_url() ?>assets/admin/login/js/script.js"></script>
    <script src="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.js" ></script>
     <script src="<?= base_url() ?>asset/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="<?= base_url() ?>asset/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="<?= base_url() ?>asset/bootstrap/js/bootstrap.min.js"></script>
   <!--  delete -->

<script type="text/javascript" src="<?= base_url() ?>delete/bootbox.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>delete/deleteRecords.js"></script>

<!--  delete -->
  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">


    
    <!-- Logo -->
    <a href="<?php echo base_url();?>index.php/dashboard" class="logo">


<?php
    $this->db->select('*');
    $this->db->from('company');
    $query      = $this->db->get();
    foreach($query->result() as $row)
    {
      $image=$row->company_logo;
     }

    $user_type = $this->session->type;

           $this->db->select('ut_user_type');
           $this->db->from('user_type'); 
           $this->db->where('ut_id',$user_type);
           $query=$this->db->get();
            if($query->num_rows()==0)
            {
                return '';   
            }else{
                $user_type=$query->row()->ut_user_type;
            }

?>
              <img src="<?php echo $this->config->item('admin_url')."uploads/company/".$image; ?>" height="" width="210" class="img-responsive">

      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>R</b>360</span>
      <!-- logo for regular state and mobile devices -->
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
           <a href="" class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?= base_url() ?>uploads/users/<?php echo $_SESSION['logo']; ?>" class="user-image" alt="User Image">
            <span><?php echo $_SESSION["username"]?> <i class="caret"></i></span>
            </a>
            <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header bg-light-blue">
            <img src="<?= base_url() ?>uploads/users/<?php echo $_SESSION['logo']; ?>" class="img-circle" alt="User Image" />
            <p>
                <?php echo $_SESSION["username"]?></br>
                <small><?php echo $user_type?></small>
                <small>Logged at - <?php echo $this->session->date;?></small>
            </p>
            </li>

 

 
            <li class="user-footer">
            <div class="pull-left">
                <a href="<?php echo $this->config->item('admin_url')."users/edit/".$_SESSION["id"]; ?>" class="btn btn-default btn-flat">Profile</a>
            </div>
            <div class="pull-right">
                <a href="<?php echo $this->config->item('admin_url')."login/logout"; ?>" class="btn btn-default btn-flat">Sign out</a>
            </div>
             </li>
             </ul>
            </li>
             <li style="padding:7px 5px 5px"> 
              <button type="button" class="btn btn-primary" value="Full Screen" class="toggle-expand-btn btn btn-sm" onclick="toggleFullScreen(document.body)"><i class="fa fa-arrows-alt"></i></button>
               
            </li> 
          <!-- Control Sidebar Toggle Button -->
          <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>
    </nav>
  </header>



<!-- fullscreen -->

   <script>
function toggleFullScreen() {
  if (!document.fullscreenElement &&    // alternative standard method
      !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
    if (document.documentElement.requestFullscreen) {
      document.documentElement.requestFullscreen();
    } else if (document.documentElement.msRequestFullscreen) {
      document.documentElement.msRequestFullscreen();
    } else if (document.documentElement.mozRequestFullScreen) {
      document.documentElement.mozRequestFullScreen();
    } else if (document.documentElement.webkitRequestFullscreen) {
      document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
    }
  } else {
    if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.msExitFullscreen) {
      document.msExitFullscreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
      document.webkitExitFullscreen();
    }
  }
}




</script>


<!-- fullscreen -->


 