<?php
           $url = $_SERVER['REQUEST_URI'];

            $token = explode('/', $url);

            $end   = $token[sizeof($token)-3];
            $end1   = $token[sizeof($token)-2];
            $end2   = $token[sizeof($token)-1];
           $tdate=date("Y-m-d"); 
           $mon = date("m");   
           $yr  = date("Y");  
           $this->db->select('*');
           $this->db->from('voucher_entry'); 
           $this->db->where('MONTH(ve_date)',$mon);
           $this->db->where('YEAR(ve_date)',$yr);
           $this->db->where('ve_type','p');
           $query=$this->db->get();
           $p_count=$query->num_rows();

           $this->db->select('*');
           $this->db->from('voucher_entry'); 
           $this->db->where('MONTH(ve_date)',$mon);
           $this->db->where('YEAR(ve_date)',$yr);
           $this->db->where('ve_type','si');
           $this->db->or_where('ve_type','si');
           $query=$this->db->get();
           $p_count=$query->num_rows();

           $user_type = $this->session->type;

           $this->db->select('ut_user_type');
           $this->db->from('user_type'); 
           $this->db->where('ut_id',$user_type);
           $query=$this->db->get();
           $user_type=$query->row()->ut_user_type;

           if($user_type=="Administrator" )
           {
            $permission=0;
           }
           else if($user_type=="Doctor")
           {
            $permission=1;
           }
           else if($user_type=="Accountant")
           {
            $permission=2;
           }
           else if($user_type=="Receptionist")
           {
            $permission=3;
           }
           else if($user_type=="Store in charge(General)")
           {
            $permission=4;
           }
           else if($user_type=="Pharmacist")
           {
            $permission=5;
           }
           else if($user_type=="Lab Technician")
           {
            $permission=6;
           }
           else if($user_type=="Cashier")
           {
            $permission=7;
           }
           else if($user_type=="IPD")
           {
            $permission=8;
           }
           else if($user_type=="OPD")
           {
            $permission=9;
           }
           else if($user_type=="Store in charge(Pharmacy)")
           {
            $permission=10;
           }
           else if($user_type=="MRD in Charge")
           {
            $permission=11;
           }
           else if($user_type=="Xray Technician")
           {
            $permission=12;
           }
           else if($user_type=="Scan Technician")
           {
            $permission=13;
           }

            // $CI =&get_instance();
            // $CI->load->model('Company_model');
            // $p_count = $CI->Company_model->purchase_count();
            // $s_count = $CI->Company_model->sale_count();
           ?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= base_url() ?>uploads/users/<?php echo $_SESSION['logo']; ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">

                <p><?php echo $_SESSION["username"]?></p>
                  <a href="#"><i class="fa fa-circle text-success"></i><?php echo $user_type?></a>
            </div>
        </div>

        <ul  class="sidebar-menu">
         
            <li <?php if($end1=="dashboard") {  ?> class="active" <?php } ?>>
                <a href="<?php echo base_url();?>index.php/dashboard" >
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>&nbsp;&nbsp;&nbsp;              
                </a>
            </li>

 <?php if($permission==0 || $permission==3 || $permission==8 || $permission==9 || $permission==11 || $permission==5) { ?>
<li <?php if($end=="patient" || $end2=="patient" || $end1=="patient" || $end=="booking" || $end2=="booking" || $end1=="booking" || $end=="opregister" || $end2=="opregister" || $end1=="opregister" || $end=="ipregister" || $end2=="ipregister" || $end1=="ipregister" || $end=="discharge" || $end2=="discharge" || $end1=="discharge" || $end=="advance" || $end2=="advance" || $end1=="advance" || $end=="mrd_room" || $end2=="mrd_room" || $end1=="mrd_room") {  ?> class="active treeview" <?php } ?> >
                 <a href="#">
            <i class="fa fa-user"></i> <span>Patient</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          
                <ul class="treeview-menu">
                   <?php if($permission==0 || $permission==9) { ?>
                <li <?php if($end=="patient" || $end2=="patient" || $end1=="patient" && $end2 !="search_patient" && $end2 !="mrd_request" && $end2 !="mrd_given" && $end1 !="mrd_request" && $end1 !="mrd_given") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."patient/"; ?>">
                    <i class="fa fa-wheelchair"></i>
                    <span>Patient Registration</span>&nbsp;&nbsp;&nbsp;
                </a>
            </li>
            <?php } ?>

            <?php if($permission==0 || $permission==11) { ?>
            <li <?php if($end=="mrd_room" || $end2=="mrd_room" || $end1=="mrd_room") {  ?> class="active treeview" <?php } ?>>
            <a href="#">
            <i class="fa fa-institution"></i>
              <span>MRD Room</span>
              <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          
                <ul class="treeview-menu">
                <li <?php if($end=="adjustment" || $end2=="adjustment" || $end1=="adjustment") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."mrd_room/"; ?>">
                    <i class="fa fa-wheelchair"></i>
                    <span>MRD Adjustment</span>
                </a>
            </li>

            <li <?php if($end=="mrd_request" || $end2=="mrd_request" || $end1=="mrd_request" || $end=="mrd_given" || $end2=="mrd_given" || $end1=="mrd_given") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."mrd_room/mrd_request"; ?>">
                    <i class="fa fa-institution"></i>
                    <span>MRD Transfer</span>
          </a>
            </li>
          </ul>
        </li>
           <?php } ?>

                  <?php if($permission==0 || $permission==3 || $permission==9) { ?>
                <li <?php if($end=="booking" || $end2=="booking" || $end1=="booking") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."booking"; ?>">
                          <i class="fa fa-calendar"></i><span>Booking</span></a>
                    </li>
                    <?php } ?>
                 <?php if($permission==0 || $permission==9) { ?>
                <li <?php if($end=="opregister" || $end2=="opregister" || $end1=="opregister") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."opregister"; ?>">
                          <i class="fa fa-stethoscope "></i><span>OP Registration</span></a>
                    </li>
                    <?php } ?>
                <?php if($permission==0 || $permission==3) { ?>
                <li <?php if($end=="ipregister" || $end2=="ipregister" || $end1=="ipregister") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."ipregister"; ?>">
                          <i class="fa fa-hotel"></i><span>IP Registration</span></a>
                    </li>
                    <?php } ?>
                <?php if($permission==0 || $permission==8) { ?>
                <li <?php if($end=="discharge" || $end2=="discharge" || $end1=="discharge") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."discharge"; ?>">
                          <i class="fa fa-file-text-o"></i><span>Discharge</span></a>
                    </li>
                    <?php } ?>

                     <?php if($permission==0 || $permission==8 || $permission==5) { ?>
                <li <?php if($end=="opbilling" || $end2=="opbilling" || $end1=="opbilling") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."opbilling"; ?>">
                          <i class="fa fa-calculator"></i><span>OP Billing</span></a>
                    </li>
                    <?php } ?>

                    <?php if($permission==0 || $permission==8) { ?>
                <li <?php if($end=="addbilling" || $end2=="addbilling" || $end1=="addbilling") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."addbilling/add"; ?>">
                          <i class="fa fa-calculator"></i><span>Additional Billing</span></a>
                    </li>
                    <?php } ?>
              </ul>
            </li>
            <?php } ?>

  <?php if($permission==0 || $permission==6) { ?>
   <li <?php if($end=="investigation" || $end2=="investigation" || $end1=="investigation" || $end=="xraytest" || $end2=="xraytest" || $end1=="xraytest" || $end=="scanning" || $end2=="scanning" || $end1=="scanning") {  ?> class="active treeview" <?php } ?>>

            <a href="#">
            <i class="fa fa-search"></i> <span>Tests</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
                <ul class="treeview-menu">
                  <?php if($permission==0 || $permission==6) { ?>
                <li <?php if($end=="investigation" || $end2=="investigation" || $end1=="investigation") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."investigation"; ?>">
                    <i class="fa fa-thermometer-half"></i>
                    <span>Lab</span>&nbsp;&nbsp;&nbsp;
                </a>
            </li>
            <?php } ?>

            <?php if($permission==0 || $permission==12) { ?>
                <li <?php if($end=="xraytest" || $end2=="xraytest" || $end1=="xraytest") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."xraytest"; ?>">
                    <i class="fa fa-road"></i>
                    <span>Xray</span>&nbsp;&nbsp;&nbsp;
                </a>
            </li>
            <?php } ?>

            <?php if($permission==0 || $permission==13) { ?>
              <li <?php if($end=="scanning" || $end2=="scanning" || $end1=="scanning") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."scanning"; ?>">
                    <i class="fa fa-qrcode"></i>
                    <span>Scanning</span>&nbsp;&nbsp;&nbsp;
                </a>
            </li>
            <?php } ?>
            
              </ul>
            </li>
            <?php } ?>

    <?php if($permission==0 || $permission==10 || $permission==5 || $permission==8 || $permission==4) { ?>
  <li <?php if($end=="porder" || $end2=="porder" || $end1=="porder" || $end=="purchase" || $end2=="purchase" || $end1=="purchase" || $end=="stocktransfer" || $end2=="stocktransfer" || $end1=="stocktransfer" || $end=="sales" || $end2=="sales" || $end1=="sales" || $end1=="preturn" || $end=="preturn" || $end2=="preturn" || $end1=="preturn" || $end=="preturn" || $end2=="preturn" || $end=="consume" || $end2=="consume" || $end1=="consume" || $end=="stock" || $end2=="stock" || $end1=="stock" || $end=="stock_adjust" || $end2=="stock_adjust" || $end1=="stock_adjust") {  ?> class="active treeview" <?php } ?>>
                 <a href="#">
            <i class="fa fa-building"></i> <span>Inventory</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
                <ul class="treeview-menu">
                  <?php if($permission==0 || $permission==10) { ?>
                <li <?php if($end=="porder" || $end2=="porder" || $end1=="porder") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."porder"; ?>">
                    <i class="fa fa-shopping-cart"></i>
                    <span>Purchase Order</span>&nbsp;&nbsp;&nbsp;
                </a>
            </li>
            <?php } ?>

           
                <?php if($permission==0 || $permission==10 || $permission==4) { ?>
                <li <?php if($end=="purchase" || $end2=="purchase" || $end1=="purchase") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."purchase"; ?>">
                          <i class="fa fa-shopping-cart"></i><span>Purchase</span></a>
                    </li>
                    <?php } ?>
                <?php if($permission==0 || $permission==10 || $permission==4) { ?>
                <li <?php if($end=="stocktransfer" || $end2=="stocktransfer" || $end1=="stocktransfer") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."stocktransfer"; ?>">
                          <i class="fa fa-retweet"></i><span>Stock Transfer</span></a>
                    </li>
                    <?php } ?>
                <?php if($permission==0 || $permission==5 || $permission==8) { ?>
                <li <?php if($end=="sales" || $end2=="sales" || $end1=="sales") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."sales"; ?>">
                          <i class="fa fa-shopping-basket"></i><span>Sale</span></a>
                    </li>
                    <?php } ?>
                <?php if($permission==0 || $permission==10 || $permission==5 || $permission==8 || $permission==4) { ?>
                <li <?php if($end1=="preturn" || $end=="preturn" || $end2=="preturn" || $end1=="preturn" || $end=="preturn" || $end2=="preturn") {  ?> class="active treeview" <?php } ?>>
                        <a href="#">
                          <i class="fa fa-mail-reply-all"></i> <span>Return</span>
                          <span class="pull-right-container">
                           <i class="fa fa-angle-left pull-right"></i>
                          </span>
                        </a>
                            
                            <ul class="treeview-menu">
                              <?php if($permission==0 || $permission==10 || $permission==4) { ?>
                              <li <?php if($end1=="preturn" || $end=="preturn" || $end2=="preturn") {  ?> class="active treeview" <?php } ?>>
                            <a href="<?php echo $this->config->item('admin_url')."preturn/return_Plist"; ?>" ><i class="fa fa-mail-forward"></i><span>Purchase Return</span></a>
                                  </li>
                                  <?php } ?>
                              <?php if($permission==0 || $permission==5 || $permission==8) { ?>
                              <li <?php if($end1=="preturn" || $end=="preturn" || $end2=="preturn") {  ?> class="active treeview" <?php } ?>>
                                      <a href="<?php echo $this->config->item('admin_url')."preturn/return_Slist"; ?>"><i class="fa fa-mail-reply"></i><span>Sales Return</span></a>                         
                                  </li>
                                  <?php } ?>
                                 
                            </ul>
                          </li>
                          <?php } if($permission==0 || $permission==10 || $permission==5 || $permission==4) { ?>
                
                <li <?php if($end=="consume" || $end2=="consume" || $end1=="consume") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."consume"; ?>">
                          <i class="fa fa-coffee"></i><span>Consumption</span></a>
                    </li>

                    <li <?php if($end=="stock_adjust" || $end2=="stock_adjust" || $end1=="stock_adjust") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."stock_adjust"; ?>">
                          <i class="fa fa-american-sign-language-interpreting"></i><span>Stock Adjustment</span></a>
                    </li>
                   
                <?php if($permission==0 || $permission==10 || $permission==5 || $permission==4) { ?>
                <li <?php if($end=="stock" || $end2=="stock" || $end1=="stock") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."stock"; ?>">
                          <i class="fa fa-pie-chart"></i><span>Stock</span></a>
                    </li>
                    <?php } } ?>
              </ul>
            </li>
            <?php } ?>
            
  <?php if($permission==0) { ?>
  <li <?php if($end=="users" || $end2=="users" || $end1=="users") {  ?> class="active treeview" <?php } ?>>
                 <a href="#">
            <i class="fa fa-child "></i> <span>HR Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
  </li>
  <?php } ?>


  <?php if($permission==0 || $permission==2 || $permission==7) { ?>
  <li <?php if($end=="expence" || $end2=="expence" || $end1=="expence") {  ?> class="active treeview" <?php } ?>>
                 <a href="#">
            <i class="fa fa-book"></i> <span>Accounts</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
                <ul class="treeview-menu">
                  <?php if($permission==0 || $permission==2) { ?>
                <li <?php if($end=="expence" || $end2=="expence" || $end1=="expence") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."expence"; ?>">
                    <i class="fa fa-inr"></i>
                    <span>Expense</span>&nbsp;&nbsp;&nbsp;
                </a>
            </li> 
            <?php } ?>
            <?php if($permission==0 || $permission==7) { ?>         
            <li <?php if($end=="ccounter" || $end2=="ccounter" || $end1=="ccounter" && $end1!="reg_report" &&  $end2!="reg_report") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."ccounter"; ?>">
                    <i class="fa fa-cc"></i>
                    <span>Cash Counter</span>&nbsp;&nbsp;&nbsp;
                </a>
            </li>
            <?php } ?>
             <?php if($permission==0 || $permission==7) { ?>
                <li <?php if($end=="advance" || $end2=="advance" || $end1=="advance") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."advance"; ?>">
                          <i class="fa fa-money"></i><span>Advance Payment</span></a>
                    </li>
                    <?php } ?>
            <?php if($permission==0) { ?>
                <li <?php if($end=="reg_report" || $end2=="reg_report" || $end1=="reg_report") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."ccounter/reg_report"; ?>">
                    <i class="fa fa-universal-access"></i>
                    <span>Registration Fee </span>&nbsp;&nbsp;&nbsp;
                </a>
            </li>      
            <?php } ?>
            </ul>
  </li>
  <?php } ?>

  <?php if($permission==0 || $permission==2 || $permission==10 || $permission==7 || $permission==6 || $permission==5 || $permission!=9) { ?>
  <li <?php if($end=="accountreport" || $end1=="accountreport" || $end1=="inventory" || $end=="inventory") {  ?> class="active treeview" <?php } ?>>
          <a href="#">
            <i class="fa fa-file-excel-o"></i> <span>Report</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if($permission==0 || $permission==2 || $permission==7 || $permission==6 || $permission==5 || $permission!=9 || $permission==10) { ?>
            <li <?php if($end=="accountreport" || $end1=="accountreport") {  ?> class="active" <?php } ?>>
              <a href="#"><i class="fa fa-book"></i>Account
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                 <?php if($permission==0 || $permission==2 || $permission==10) { ?>
                <li <?php if($end1=="accountreport") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."accountreport"; ?>" ><i class="fa fa-file"></i><span>Summary</span></a>
                    </li>
                    <?php } ?>
                 <?php if($permission==0 || $permission==2 || $permission==10) { ?>
                <li <?php if($end1=="acdetail") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."accountreport/acdetail"; ?>"><i class="fa fa-file-text-o"></i><span> Detail</span></a>                         
                    </li>
                    <?php } ?>
                     <?php if($permission==0 || $permission==2 || $permission==10) { ?>
                    <li <?php if($end1=="general" || $end=="general") {  ?> class="active" <?php } ?>>
              <a href="<?php echo $this->config->item('admin_url')."generalreport"; ?>"><i class="fa fa-money"></i><span>GST & Medicine Category  
                </span>
              </a></li>
              <?php } ?>

              <?php if($permission!=9) { ?>
                    <li <?php if($end1=="ccounterreport" || $end=="ccounterreport") {  ?> class="active" <?php } ?>>
              <a href="<?php echo $this->config->item('admin_url')."ccounter/ccounterreport"; ?>"><i class="fa fa-money"></i><span>Cash Collection
                </span>
              </a></li>
              <?php } ?>
              </ul>
            </li>
            <?php } ?>
            <?php if($permission==0 || $permission==10) { ?>
              <li <?php if($end1=="inventory" || $end=="inventory") {  ?> class="active" <?php } ?>>
              <a href="#"><i class="fa fa-building"></i> Inventory 
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php if($permission==0 || $permission==10) { ?>
                <li <?php if($end1=="inventory") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."inventory"; ?>"><i class="fa fa-file"></i><span> Summary</span></a>
                    </li>
                    <?php } ?>
                <?php if($permission==0 || $permission==10) { ?>
                <li <?php if($end1=="indetail") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."inventory/indetail"; ?>"><i class="fa fa-file-text-o"></i><span>Detail</span></a>
                    </li>
                    <?php } ?>
               </ul>
            </li>
            <?php } ?>

            <?php if($permission==0 || $permission==2) { ?>
             <!--  <li <?php if($end1=="allreport" || $end=="allreport") {  ?> class="active" <?php } ?>>
              <a href="<?php echo $this->config->item('admin_url')."allreport"; ?>"><i class="fa fa-pencil-square-o"></i> All in One </a>
            </li> -->
            <?php } ?>

          </ul>
</li>
<?php } ?>


           



            

<?php if($permission==0 || $permission==10 || $permission==4) { ?>
<li <?php if($end=="brand" || $end1=="brand" || $end2=="brand" || $end=="mcontents" || $end1=="mcontents" || $end2=="mcontents" || $end=="suppliers" || $end1=="suppliers" || $end2=="suppliers" || $end=="excategory" || $end1=="excategory" || $end2=="excategory" || $end=="tests" || $end1=="tests" || $end2=="tests" || $end=="testsname" || $end1=="testsname" || $end2=="testsname" || $end=="testscontent" || $end1=="testscontent" || $end2=="testscontent" || $end=="designation" || $end1=="designation" || $end2=="designation" || $end=="user_type" || $end1=="user_type" || $end2=="user_type" || $end=="booking_token" || $end1=="booking_token" || $end2=="booking_token" || $end=="booking_token" || $end1=="booking_token" || $end2=="booking_token" || $end=="room" || $end1=="room" || $end2=="room" || $end=="bed" || $end1=="bed" || $end2=="bed" || $end=="mpackage" || $end1=="mpackage" || $end2=="mpackage" || $end=="additionalbill" || $end1=="additionalbill" || $end2=="additionalbill" || $end1=="department" || $end=="department" || $end2=="department" || $end=="product" || $end1=="product" || $end2=="product") {  ?> class="active treeview" <?php } ?>>
          <a href="#">
            <i class="fa fa-newspaper-o"></i> <span>Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <?php if($permission==0 || $permission==10) { ?>
            <li <?php if($end1=="brand" || $end=="brand" || $end2=="brand" || $end=="mcontents" || $end1=="mcontents" || $end2=="mcontents" || $end=="product" || $end1=="product" || $end2=="product") {  ?> class="active" <?php } ?>>
              <a href="#"><i class="fa fa-dropbox"></i> Product
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php if($permission==0 || $permission==10) { ?>
                <li <?php if($end=="product" || $end1=="product" || $end2=="product") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."product"; ?>"><i class="fa fa-medkit"></i><span>Manage Product</span></a>
                    </li> 
                    <?php } ?>
                <?php if($permission==0 || $permission==10) { ?>
                <li <?php if($end=="brand" || $end1=="brand" || $end2=="brand") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."brand"; ?>"><i class="fa fa-bitbucket"></i><span>Manage Manufacturers</span></a>
                    </li>
                    <?php } ?>  
              <!--   <li <?php if($end=="pcategory" || $end1=="pcategory" || $end2=="pcategory") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."pcategory"; ?>"><i class="fa fa-futbol-o"></i><span>Manage Category</span></a>
                    </li>
                <li <?php if($end=="subcategory" || $end1=="subcategory" || $end2=="subcategory") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."subcategory"; ?>" ><i class="fa fa-tags"></i><span>Manage Sub Category</span></a>
                    </li> -->
               <!--  <li <?php if($end=="types") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."types"; ?>"><i class="fa fa-cogs"></i><span>Manage Types</span></a>
                    </li> -->
                <?php if($permission==0 || $permission==10) { ?>
                <li <?php if($end=="mcontents" || $end1=="mcontents" || $end2=="mcontents") {  ?> class="active treeview" <?php } ?>>
                    <a href="<?php echo $this->config->item('admin_url')."mcontents"; ?>"><i class="fa fa-envira"></i><span>Manage Medicine Contents</span></a>
                </li>
                <?php } ?>
              </ul>
            </li>
            <?php } ?>

            <?php if($permission==0 || $permission==10) { ?>
            <li <?php if($end1=="suppliers" || $end2=="suppliers" || $end=="suppliers" ) {  ?> class="active treeview" <?php } ?>>
            <a href="<?php echo $this->config->item('admin_url')."suppliers"; ?>" >
                <i class="fa fa-user-secret"></i>
                <span>Suppliers</span>&nbsp;&nbsp;&nbsp;
            </a>
        </li>
        <?php } ?>

            <?php if($permission==0 || $permission==2) { ?>
            <li <?php if($end1=="excategory" || $end=="excategory" || $end2=="excategory") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."excategory"; ?>"><i class="fa fa-inr"></i><span>Expense Category</span></a>
                    </li> 
            <?php } ?>

            <?php if($permission==0 || $permission==6) { ?>
            <li <?php if($end1=="tests" || $end=="tests" || $end2=="tests" || $end=="testsname" || $end1=="testsname" || $end2=="testsname" || $end=="testscontent" || $end1=="testscontent" || $end2=="testscontent") {  ?> class="active" <?php } ?>>
              <a href="#"><i class="fa fa-thermometer-half"></i> Lab
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
                    <ul class="treeview-menu">
                    <?php if($permission==0 || $permission==6) { ?>
                     <li <?php if($end1=="tests" || $end=="tests" || $end2=="tests") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."tests"; ?>"><i class="fa fa-thermometer-half"></i><span>Tests</span></a>
                    </li>
                    <?php } ?>
 <?php if($permission==0 || $permission==6) { ?>
 <li <?php if($end1=="testsname" || $end=="testsname" || $end2=="testsname") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."testsname"; ?>"><i class="fa fa-thermometer-full"></i><span>Test Name</span></a>
                    </li>
                    <?php } ?> 

<?php if($permission==0 || $permission==6) { ?>
<li <?php if($end1=="testscontent" || $end=="testscontent" || $end2=="testscontent") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."testscontent"; ?>"><i class="fa fa-thermometer-empty"></i><span>Test content</span></a>
                    </li> 
                    <?php } ?>
                  </ul>
                  </li>
                  <?php } ?> 


                  <?php if($permission==0 || $permission==6) { ?>
            <li <?php if($end1=="xray" || $end=="xray" || $end2=="xray" || $end=="xrayname" || $end1=="xrayname" || $end2=="xrayname") {  ?> class="active" <?php } ?>>
              <a href="#"><i class="fa fa-thermometer-half"></i> Xray
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
                    <ul class="treeview-menu">
                    <?php if($permission==0 || $permission==6) { ?>
                     <li <?php if($end1=="xray" || $end=="xray" || $end2=="xray") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."xray"; ?>"><i class="fa fa-thermometer-half"></i><span>Xray</span></a>
                    </li>
                    <?php } ?>
 <?php if($permission==0 || $permission==6) { ?>
 <li <?php if($end1=="testsname" || $end=="xrayname" || $end2=="xrayname") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."xrayname"; ?>"><i class="fa fa-thermometer-full"></i><span>Xray Name</span></a>
                    </li>
                    <?php } ?> 

 
                  </ul>
                  </li>
                  <?php } ?> 


<?php if($permission==0 || $permission==6) { ?>
            <li <?php if($end1=="scan" || $end=="scan" || $end2=="scan" || $end=="scanname" || $end1=="scanname" || $end2=="scanname" || $end=="scancontent" || $end1=="scancontent" || $end2=="scancontent") {  ?> class="active" <?php } ?>>
              <a href="#"><i class="fa fa-thermometer-half"></i>Scan
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
                    <ul class="treeview-menu">
                    <?php if($permission==0 || $permission==6) { ?>
                     <li <?php if($end1=="scan" || $end=="scan" || $end2=="scan") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."scan"; ?>"><i class="fa fa-thermometer-half"></i><span>Scan</span></a>
                    </li>
                    <?php } ?>
 <?php if($permission==0 || $permission==6) { ?>
 <li <?php if($end1=="scanname" || $end=="scanname" || $end2=="scanname") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."scanname"; ?>"><i class="fa fa-thermometer-full"></i><span>Scan Name</span></a>
                    </li>
                    <?php } ?> 

 <?php if($permission==0 || $permission==6) { ?>
<li <?php if($end1=="scancontent" || $end=="scancontent" || $end2=="scancontent") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."scancontent"; ?>"><i class="fa fa-thermometer-empty"></i><span>Scan content</span></a>
                    </li> 
                    <?php } ?>
                  </ul>
                  </li>
                  <?php } ?> 






                    <?php if($permission==0) { ?>
                    <li <?php if($end1=="designation" || $end=="designation" || $end2=="designation") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."designation"; ?>"><i class="fa fa-universal-access"></i><span>Designation</span></a>
                    </li>
                    <?php } ?>
                    <?php if($permission==0) { ?>   
                    <li <?php if($end1=="department" || $end=="department" || $end2=="department") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."department"; ?>"><i class="fa fa-university"></i><span>Department</span></a>
                    </li>
                    <?php } ?>
                    <?php if($permission==0) { ?>
                    <li <?php if($end1=="user_type" || $end=="user_type" || $end2=="user_type") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."user_type"; ?>"><i class="fa fa-user-circle-o"></i><span>User Type</span></a>
                    </li>
                    <?php } ?>
                    <?php if($permission==0 || $permission==3) { ?>
                    <li <?php if($end1=="booking_token" || $end=="booking_token" || $end2=="booking_token") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."booking_token"; ?>"><i class="fa fa-ticket"></i><span>Booking Token</span></a>
                    </li>
                    <?php } ?>
                    <?php if($permission==0 || $permission==3) { ?>
                    <li <?php if($end1=="room" || $end=="room" || $end2=="room") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."room"; ?>"><i class="fa fa-building-o"></i><span>Rooms</span></a>
                    </li>
                    <?php } ?>
                    <?php if($permission==0 || $permission==3) { ?>
                    <li <?php if($end1=="bed" || $end=="bed" || $end2=="bed") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."bed"; ?>"><i class="fa fa-bed"></i><span>Beds</span></a>
                    </li>
                    <?php } ?>
                    <?php if($permission==0 || $permission==10) { ?>
                    <li <?php if($end1=="mpackage" || $end=="mpackage" || $end2=="mpackage") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."mpackage"; ?>"><i class="fa fa-cart-plus"></i><span>Medicine Package</span></a>
                    </li>
                    <?php } ?>
                    <?php if($permission==0 || $permission==8) { ?>
                    <li <?php if($end1=="additionalbill" || $end=="additionalbill" || $end2=="additionalbill") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."additionalbill"; ?>"><i class="fa fa-file-text"></i><span>Additional Billing</span></a>
                    </li>
                    <?php } ?>
              
            </li>
            <?php } ?>
          </ul>
        </li>



          <?php if($permission==0) { ?>
           <li <?php if($end=="users" || $end2=="users" || $end1=="users" || $end=="priviliges" || $end2=="priviliges" || $end1=="priviliges" || $end=="company" || $end2=="company" || $end1=="company") {  ?> class="active treeview" <?php } ?>>
                 <a href="#">
            <i class="fa fa-cog"></i> <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
                <ul class="treeview-menu">
                <li <?php if($end=="users" || $end2=="users" || $end1=="users") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."users"; ?>">
                    <i class="fa  fa-users"></i>
                    <span>Users</span>&nbsp;&nbsp;&nbsp;
                </a>
            </li>
            
                <li <?php if($end=="company" || $end2=="company" || $end1=="company") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."company/view"; ?>"><i class="fa fa-hospital-o"></i><span>Hospital</span></a>
                    </li>
                <li class="treeview"><a href="<?php echo $this->config->item('admin_url');?>login/dbBackup">
              <i class="fa fa-database"></i><span>Back Up</span></a></li>
            
                <li <?php if($end=="restore" || $end2=="restore" || $end1=="restore") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."login/dbRestore"; ?>"><i class="fa fa-window-restore"></i><span>Restore</span></a>
                </li>
              </ul>
            </li>


            
                 
                
              <?php } ?>
            
              
        </section>
    <!-- /.sidebar -->
  </aside>