<?php
           $url = $_SERVER['REQUEST_URI'];

            $token = explode('/', $url);
            $end   = $token[sizeof($token)-2];
            $end1   = $token[sizeof($token)-1];
           
           ?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= base_url() ?>asset/img/m3.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Hello, Super User</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Administrator</a>
            </div>
        </div>

        <ul  class="sidebar-menu">
            <li <?php if($end=="index.php" && $end1!="sales") {  ?> class="active" <?php } ?>>
                <a href="<?php echo base_url();?>index.php/dashboard">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>&nbsp;&nbsp;&nbsp;              
                </a>
            </li> 
          <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Product</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($end=="product") {  ?> class="active treeview" <?php } ?>>
                <a href="<?php echo $this->config->item('admin_url')."product"; ?>">
                    <i class="fa  fa-shield"></i>
                    <span>Product</span>&nbsp;&nbsp;&nbsp;
                </a>
            </li>
            <li>
              <a href="#"><i class="fa fa-circle-o"></i> Product Category
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li <?php if($end=="brand") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."brand"; ?>"><i class="fa fa-cogs"></i><span>Manage  Brand</span></a>
                    </li>  
                <li <?php if($end=="pcategory") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."pcategory"; ?>"><i class="fa fa-cogs"></i><span>Manage Category</span></a>
                    </li>
                <li <?php if($end=="subcategory") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."subcategory"; ?>"><i class="fa fa-cogs"></i><span>Manage Sub Category</span></a>
                    </li>
                <li <?php if($end=="types") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."types"; ?>"><i class="fa fa-cogs"></i><span>Manage Types</span></a>
                    </li>
                <li <?php if($end=="groups") {  ?> class="active treeview" <?php } ?>>
                    <a href="<?php echo $this->config->item('admin_url')."groups"; ?>"><i class="fa fa-cogs"></i><span>Manage Groups</span></a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li <?php if($end=="vendor") {  ?> class="active treeview" <?php } ?>>
            <a href="<?php echo $this->config->item('admin_url')."vendor"; ?>">
                <i class="fa fa-bookmark"></i>
                <span>Vendors</span>&nbsp;&nbsp;&nbsp;
            </a>
        </li>
        <li <?php if($end=="customer") {  ?> class="active treeview" <?php } ?>>
            <a href="<?php echo $this->config->item('admin_url')."customer"; ?>">
                <i class="fa  fa-user"></i>
                <span>Customers</span>&nbsp;&nbsp;&nbsp;
            </a>
        </li>
        <li <?php if($end=="purchase") {  ?> class="active treeview" <?php } ?>>
          <a href="<?php echo base_url();?>index.php/purchase/getPurchase">
              <i class="fa fa-inr"></i>
              <span>Purchase</span>&nbsp;&nbsp;&nbsp;
              <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
              </span>
          </a>
        </li>
        <li <?php if($end1=="sales") {  ?> class="" <?php } ?>>
            <a href="<?php echo base_url();?>index.php/sales/sale_list">
                <i class="fa fa-line-chart"></i>
                <span>Sales</span>&nbsp;&nbsp;&nbsp;
                <span class="pull-right-container">
                <span class="label label-primary pull-right">4</span>
                </span>
            </a>
        </li>
        <li <?php if($end=="stock") {  ?> class="active treeview" <?php } ?>>
            <a href="<?php echo $this->config->item('admin_url')."stock"; ?>">
                <i class="fa fa-pie-chart"></i>
                <span>Stock</span>&nbsp;&nbsp;&nbsp;
            </a>
        </li>
        <li <?php if($end=="Users") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."Users"; ?>">
                    <i class="fa  fa-users"></i>
                    <span>Users</span>&nbsp;&nbsp;&nbsp;
                </a>
            </li>

             <li <?php if($end=="expence") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."expence"; ?>">
                    <i class="fa  fa-users"></i><span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                    <span>Expence</span>&nbsp;&nbsp;&nbsp;
                </a>
 <ul class="treeview-menu">
                <li <?php if($end=="excategory") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."excategory"; ?>"><i class="fa fa-cogs"></i><span>Expence Category</span></a>
                    </li>
                <li <?php if($end=="expence") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."expence"; ?>"><i class="fa fa-cogs"></i><span>Manage Expence</span></a>
                    </li>
                 
                 
                 
              </ul>
            </li>

           <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Report</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="#"><i class="fa fa-circle-o"></i> Account Report
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li <?php if($end=="income") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."income"; ?>"><i class="fa fa-cogs"></i><span>Income Report</span></a>
                    </li>
                <li <?php if($end=="balancesheet") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."balancesheet"; ?>"><i class="fa fa-cogs"></i><span>Balance sheet</span></a>
                    </li>
                <li <?php if($end=="expencesheet") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."expencesheet"; ?>"><i class="fa fa-cogs"></i><span>Expence Sheet</span></a>
                    </li>
                 
                   
                   
              </ul>
            </li>
              <li>
              <a href="#"><i class="fa fa-circle-o"></i> Inventory Report
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li <?php if($end=="taxreport") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."taxreport"; ?>"><i class="fa fa-cogs"></i><span>Tax Report</span></a>
                    </li>
                <li <?php if($end=="salesreport") {  ?> class="active treeview" <?php } ?>>
                        <a href="<?php echo $this->config->item('admin_url')."salesreport"; ?>"><i class="fa fa-cogs"></i><span>Sales Report</span></a>
                    </li>
                <li <?php if($end=="stockreport") {  ?> class="active treeview" <?php } ?>>
               <a href="<?php echo $this->config->item('admin_url')."stockreport"; ?>"><i class="fa fa-cogs"></i><span>Stock Report </span></a>
                    </li>
                 
                   
                   
              </ul>


            </li>
          </ul>

           <li <?php if($end=="company/view/") {  ?> class="active treeview" <?php } ?>>
                <a href="<?php echo $this->config->item('admin_url')."company/view/"; ?>">
                    <i class="fa  fa-user"></i>
                    <span>Company</span>&nbsp;&nbsp;&nbsp;
                </a>
            </li>
        </li>
        </section>
    <!-- /.sidebar -->
  </aside>