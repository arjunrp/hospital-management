<link rel="stylesheet" href='https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css' type="text/css"/>
<link rel="stylesheet" href='https://cdn.datatables.net/scroller/1.4.3/css/scroller.dataTables.min.css' type="text/css"/>
<script src="//code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/scroller/1.4.3/js/dataTables.scroller.min.js"></script>

<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>


<section class="right-side" style="min-height:700px;">
    <section class="content-header">
        <h1>
           <?php echo $page_title; ?>
       </h1>
       <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?php echo $page_title; ?></li>
    </ol>&nbsp;&nbsp;
     <?php if($this->session->flashdata('Success')){ ?>
           <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
  
</section>

<section class="content">    <!-- Success-Messages -->
    <div class="box box-info">
        <div class="box-header">
           
        <h3 class="box-title"><i class="fa fa-th"></i> <?php echo $page_title; ?>
        </h3>
        <div class="box-tools">
            <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo $this->config->item('admin_url')."patient/add"; ?>" accesskey="n" title="short key-ALT+N" >
                <i class="fa fa-plus-circle"></i> Create New
            </a>

        </div>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div >
            <div class="row">
                <div class="col-md-12">
                    <?php 
        $attr = array("class" => "form-horizontal", "role" => "form", "id" => "form1", "name" => "form1");
        echo form_open("patient/search", $attr);?>
        <div class="col-md-2">
                    <input class="form-control input-sm" id="pmrd" name="pmrd" placeholder="MRD" type="text" value="<?php echo set_value('pmrd'); ?>" />
                  </div>
                    <div class="col-md-2">
                    <input class="form-control input-sm" id="pname" name="pname" placeholder="Patient Name" type="text" value="<?php echo set_value('pname'); ?>" /></div>
                    <div class="col-md-2">
                    <input class="form-control input-sm" id="pphone" name="pphone" placeholder="Patient Phone" type="text" value="<?php echo set_value('pphone'); ?>" /></div>
                    <div class="col-md-2">
                    <input class="form-control input-sm" id="paddress" name="paddress" placeholder="Address" type="text" value="<?php echo set_value('paddress'); ?>" /></div>
                    <div class="col-md-2">
                    <input class="form-control input-sm" id="pcareof" name="pcareof" placeholder="Care of" type="text" value="<?php echo set_value('pcareof'); ?>" /></div>
                <div class="col-md-2">
                    <input id="btn_search" name="btn_search" type="submit" class="btn btn-warning btn-sm" value="Search" />
                    <a href="<?php echo base_url(). "index.php/patient/index"; ?>" class="btn btn-primary btn-sm">Show All</a>
                </div>
                    <div class="col-md-12">
                        <br>
                        <table id="example" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>MRD No.</th>
                                    <th>Patient</th>
                                    <th>Phone</th>
                                    <th>Age</th>
                                    <th>Address</th>
                                    <th>Care Of</th>
                                    <th>Blood Group</th>
                                    <th>Options</th>
                                </tr>
                                 <?php if(!empty($results)) 
                                 { 
                                  
                                  $tdate=date("Y-m-d");  foreach ($results as $data) { 
                                    ?>
                        <tr>
                            <td><?php echo $data->p_mrd_no ?></td>
                            <td><?php echo $data->p_title." ".$data->p_name ?></td>
                            <td><?php echo $data->p_phone ?></td>
                            <td><?php echo $data->p_age ?></td>
                            <td><?php echo $data->p_address ?></td>
                            <td><?php echo $data->p_guardian ?></td>
                            <td><?php echo $data->p_blood ?></td>
                            <?php
                            $p_renew_date  = date_create($data->p_renew_date);
                            $today_date    = date_create($tdate);
                            $diff          = date_diff($p_renew_date,$today_date);
                            $date_count    = $diff->format("%a");
                            $p_renew_days  = $data->p_renew_days;
                            $p_renew_count = $data->p_renew_count;
                            $p_renew_visits= $data->p_renew_visits;
                            $status = "disabled";
                            if($date_count>=$p_renew_days || $p_renew_count<=$p_renew_visits) {
                              $status = "";
                            }

                             ?>
                            <td class='btn-group  btn-group-xs'><a href=" <?=$this->config->item('admin_url')."patient/renew/".$data->p_mrd_no ?>" <?=$status ?> class='btn btn-warning view-btn-edit' title='Renew patient'><i class='fa fa-print'></i></a>
                              <a href=" <?=$this->config->item('admin_url')."opregister/get_op/".$data->p_mrd_no ?>" class='btn btn-primary view-btn-edit' title='Create OP'><i class='fa fa-stethoscope'></i></a>
                              <a href=" <?=$this->config->item('admin_url')."patient/view/".$data->p_mrd_no ?>" class='btn btn-default view-btn-edit' title='View'><i class='fa fa-eye'></i></a>
             <a href="<?= $this->config->item('admin_url')."patient/edit/".$data->p_mrd_no ?>" class='btn btn-info view-btn-edit' title='Edit'><i class='fa fa-pencil-square-o'></i></a>  
             <a class='btn btn-danger' onclick="return confirm('Are you sure you want to delete this item?');" href="<?= $this->config->item('admin_url')."patient/delete/".$data->p_mrd_no ?>" title='Delete'><i class='fa fa-times-circle-o'></i></a>
              <!-- <a class='btn btn-danger delete_employee' data-emp-id=" <?= $this->config->item('admin_url')."patient/delete/".$data->p_mrd_no ?>" href='javascript:void(0)' title='Delete'><i class='fa fa-times-circle-o'></i></a> -->
            </td>
                        </tr>
                    <?php } }
else
{
  ?> <script> alert("No results found");
  window.location = "<?php echo base_url(). "index.php/patient" ?>";
   </script> <?php
}
                     ?>
                            </thead>
                            <tbody>
                            
                                         

                    </tbody>
                </table>

              
            </div> </div>  
        </div>
        <div class="row">
            <div class="col-md-12" >
                  <?php if (isset($links)) { ?>
                <?php echo $links ?>
            <?php } ?>
            </div>
        </div>  
    </div></div><!-- /.box-body -->
</div>
</section>
</section><!-- /.right-side -->


<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>

<style type="text/css">
.codpage{
    box-sizing: border-box;
  display: inline-block;
  min-width: 1.5em;
  padding: 0.5em 1em;
  margin-left: 2px;
  text-align: center;
  text-decoration: none !important;
  cursor: pointer;
  *cursor: hand;
  color: #333 !important;
  border: 1px solid transparent;

}

.codpage:hover {
  color: #333 !important;
  border: 1px solid #cacaca;
  background-color: white;
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, white), color-stop(100%, #dcdcdc));
  /* Chrome,Safari4+ */
  background: -webkit-linear-gradient(top, white 0%, #dcdcdc 100%);
  /* Chrome10+,Safari5.1+ */
  background: -moz-linear-gradient(top, white 0%, #dcdcdc 100%);
  /* FF3.6+ */
  background: -ms-linear-gradient(top, white 0%, #dcdcdc 100%);
  /* IE10+ */
  background: -o-linear-gradient(top, white 0%, #dcdcdc 100%);
  /* Opera 11.10+ */
  background: linear-gradient(to bottom, white 0%, #dcdcdc 100%);
  /* W3C */
}
.codpage:active {
outline: none;
  background-color: #48a0e2;
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #48a0e2), color-stop(100%, #48a0e2));
  /* Chrome,Safari4+ */
  background: -webkit-linear-gradient(top, #48a0e2 0%, #48a0e2 100%);
  /* Chrome10+,Safari5.1+ */
  background: -moz-linear-gradient(top, #72d3f9 0%, #48a0e2 100%);
  /* FF3.6+ */
  background: -ms-linear-gradient(top, #72d3f9 0%, #48a0e2 100%);
  /* IE10+ */
  background: -o-linear-gradient(top, #72d3f9 0%, #48a0e2 100%);
  /* Opera 11.10+ */
  background: linear-gradient(to bottom, #72d3f9 0%, #48a0e2 100%);
  /* W3C */
  box-shadow: inset 0 0 3px #111;
}

</style>

<!-- box-sizing: border-box;
  display: inline-block;
  min-width: 1.5em;
  padding: 0.5em 1em;
  margin-left: 2px;
  text-align: center;
  text-decoration: none !important;
  cursor: pointer;
  *cursor: hand;
  color: #333 !important;
  border: 1px solid transparent; -->
 
