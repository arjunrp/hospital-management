<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<!-- <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.0.min.js"></script>
 enable validation not working -->
<script> 
$(document).ready(function() {
});
</script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>

<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Product
      <small>Create  Product </small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."product"; ?>">Product</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>

        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?> 
        </h3>
       <div align="right">
         <a title="short key-ALT+B" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."product"; ?>" accesskey="b"><i class="fa fa-mail-reply-all"></i> Back</a>
       </div>


     </div>
     <?php echo form_open_multipart($action); ?>
     <div class="box-body">
      <div class="row">
        <div class="col-md-12">


  <?php
                        if($this->uri->segment(1)=="add")
                        $clas   =   'required="required"';
                      else
                      $clas = '';
                    ?> 
           <div class="col-md-3">
          <div class="form-group">
                        <label for="focusinput" class="control-label">Image(1600 X 400)</label>
                        
                         <?php if($image != "") { echo "<img id='blah' width='139px' height='150px' src='".$this->config->item("base_url")."uploads/product/".$image."'>"; } else { echo "<img id='blah' width='150px' height='150px' src='".$this->config->item("base_url")."uploads/product.png"."'>"; } ?>
                      <input class="form-control" type="hidden" name="pd_id" value="<?php echo $pd_id;?>">
                        <input type="file" name="image" onchange="readURL(this);" <?php echo $clas; ?>>
                      </div>
                    </div>
           
            <div class="form-group col-md-3">
            <label  class="control-label">Product Code <sup>*</sup></label>
          <input class="form-control validate[required] input-sm" readonly  tabindex="1" style="width:190px;height:29px";readonly type="text" name="pd_code" value="<?=$pd_code ?>" >      
          <input class="form-control"  type="hidden" name="pd_id" value="<?=$pd_id?>">
        </div>    
           <div class="form-group col-md-3">
             <label  class="control-label">Product <sup>*</sup></label>
          <input class="form-control validate[required] input-sm"  data-prompt-position="topLeft:210" tabindex="3" style="width:190px;height:29px"; type="text" name="pd_product" value="<?=$pd_product?>"> 
        </div>

            <div class="form-group col-md-3">
           <label for="category" class="control-label">Manufacturer <sup>*</sup></label><br>
          <select class="form-control validate[required] input-sm"  tabindex="4" style="width:190px;height:29px";   name="pd_brandid" id="pd_brandid"> 
           <option value="0" selected="selected">Select </option>
            <?php foreach ($brands as $key => $brand) {
              ?>
              <option value="<?php echo $brand['br_id']?>" <?php if($brand['br_id']==$pd_brandid) { ?> selected="selected" <?php } ?>><?php echo $brand['br_brand']?></option>
              <?php
            }?>
          </select>    
          </div>    
             
             <div class="form-group col-md-3">
              <label for="category" class="control-label">Main Content <sup>*</sup></label>
          <select class="form-control validate[required] select2" multiple="multiple" data-prompt-position="topLeft:350"  tabindex="5" style="width:190px;height:29px"; name="pd_contentid[]" id="pd_contentid"  > 
            <?php foreach ($contents as $key => $content) {
              $e1 = explode("+", $pd_contentid);
              ?>
              <option value="<?php echo $content['mc_id']?>" <?php if(in_array($content['mc_id'],$e1)) { ?> selected="selected" <?php } ?>><?php echo $content['mc_content']?></option>
              <?php
            }?>
          </select>
          </div> 
           <div class="form-group col-md-3">
          <label for="category" class="control-label">Suppliers <sup>*</sup></label>
          <select class="form-control validate[required] select2" data-prompt-position="bottomLeft:160" multiple="multiple" tabindex="6" style="width:190px;height:29px"; name="pd_supplier[]" id="pd_supplier"> 
              <?php foreach ($suppliers as $key => $supplier) {
                $e = explode(",", $pd_supplier);
              ?>
              <option value="<?php echo $supplier['sp_id']?>" <?php if(in_array($supplier['sp_id'],$e)) { ?> selected="selected" <?php } ?>><?php echo $supplier['sp_vendor']?></option>
              <?php
            }?>
           </option>
          </select>             
          </div>
          
          <div class="form-group col-md-3">
             <label  class="control-label">Type<sup>*</sup></label>
             <select class="form-control validate[required] input-sm" name="pd_type" data-prompt-position="bottomLeft:160" tabindex="16" style="width:190px;height:29px;">
              <option value="0">--Select--</option>
              <option value="m"<?php if($pd_type=="m") { ?> selected="selected" <?php } ?>>--Medical--</option>
              <option value="g"<?php if($pd_type=="g") { ?> selected="selected" <?php } ?>>--General--</option>
             </select>   
          </div>
          
          <div class="form-group col-md-12"><br></div>
            <div class="form-group col-md-3">
             <label  class="control-label">Minimum Quantity <sup>*</sup></label>
          <input class="form-control validate[required,custom[onlyNumberSp]] input-sm"  data-prompt-position="topLeft:290"  tabindex="7"  style="width:190px;height:29px";  type="text" name="pd_min" value="<?=$pd_min  ?>" >        
          </div>
   
            <div class="form-group col-md-3">
           <label  class="control-label">Maximum Quantity <sup>*</sup></label>
          <input class="form-control validate[required,custom[onlyNumberSp]] input-sm" data-prompt-position="topLeft:290"  tabindex="8" style="width:190px;height:29px;";  type="text" name="pd_max" value="<?=$pd_max  ?>" > 
        </div>    
             
             <div class="form-group col-md-3">
              <label  class="control-label">GST-State(%) <sup>*</sup></label><br>
          <input class=" form-control validate[required,custom[number]] text-input"  tabindex="9"  data-prompt-position="topLeft:260"  style="width:190px;height:29px;"; name="pd_sgst" value="<?=$pd_sgst  ?>" >                
          </div> 
           <div class="form-group col-md-3">
             <label  class="control-label">GST-Central(%) <sup>*</sup></label>
          <input  class=" form-control validate[required,custom[number]] input-sm"  data-prompt-position="topLeft:260" tabindex="10"  style="width:190px;height:29px;" name="pd_cgst" value="<?=$pd_cgst  ?>" >
          </div>

          <div class="form-group col-md-3">
          <label  class="control-label">Unit <sup>*</sup></label>
          <select class="form-control validate[required] input-sm"  data-prompt-position="topLeft:260"  tabindex="11"  style="width:190px";  name="pd_unit"> 
          <option value="">Select the Unit</option>
          <option value="No.s"<?php if($pd_unit=="No.s") { ?> selected="selected" <?php } ?>>No.s</option>
          <option value="Bottle"<?php if($pd_unit=="Bottle") { ?> selected="selected" <?php } ?>>Bottle</option>
          <option value="Strip"<?php if($pd_unit=="Strip") { ?> selected="selected" <?php } ?>>Strip</option>
          <option value="Packet"<?php if($pd_unit=="Packet") { ?> selected="selected" <?php } ?>>Packet</option>
          <option value="Ampule"<?php if($pd_unit=="Ampule") { ?> selected="selected" <?php } ?>>Ampule</option>
          <option value="Vial"<?php if($pd_unit=="Vial") { ?> selected="selected" <?php } ?>>Vial</option>
          <option value="Syrup"<?php if($pd_unit=="Syrup") { ?> selected="selected" <?php } ?>>Syrup</option>
          <option value="Drops"<?php if($pd_unit=="Drops") { ?> selected="selected" <?php } ?>>Drops</option>
          <option value="Sacket"<?php if($pd_unit=="Sacket") { ?> selected="selected" <?php } ?>>Sacket</option>
          <option value="Tube"<?php if($pd_unit=="Tube") { ?> selected="selected" <?php } ?>>Tube</option>
         </select>   
          </div>
                 
            <div class="form-group col-md-3">
             <label for="category" class="control-label">No.s <sup>*</sup></label>  
            <input class="form-control validate[required,custom[onlyNumberSp]] input-sm"  data-prompt-position="bottomLeft:160"tabindex="13" type="input" name="pd_qty"  style="width:190px;height:29px"; value="<?=$pd_qty?>" >
          </div>     

          <div class="form-group col-md-3">
             <label for="category" class="control-label">Medicine Category <sup>*</sup></label>  
            <input class="form-control input-sm"  data-prompt-position="bottomLeft:160"tabindex="14" type="input" name="pd_catg"  style="width:190px;height:29px"; value="<?=$pd_catg?>" >
          </div>   

                  </div> 
        </div>
      </div>
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            <div>
              <input class="btn-large btn-primary btn" tabindex="16" type="submit" value="Submit" name="submit" accesskey="s" title="short key-ALT+S"> 
              <input class="btn-large btn-default btn"  tabindex="17" type="reset" value="Reset">
            </div>
          </div>

        </div>
      </div>
      <?php echo form_close(); ?>

      <script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
            .attr('src', e.target.result)
            .width(139)
            .height(150);
        };

        reader.readAsDataURL(input.files[0]);
        
    }
}

</script>
    </div>
   
</section>

</div><!-- /.right-side -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
      })
</script>

