<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {

$('#country').change(function(){
    var ctid = $('#country').val();
    $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>company/get_state/"+ctid,
     success: function(data){
      $("#state").empty();
      $.each(data, function(index) {
        $("#state").append('<option value=' + data[index].id +'>'+data[index].name+'</option>');
      });
    }
  })
  });

$('#state').change(function(){
    var ctid = $('#state').val();
    $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>company/get_city/"+ctid,
     success: function(data){
      $("#city").empty();
      $.each(data, function(index) {
        $("#city").append('<option value=' + data[index].id +'>'+data[index].name+'</option>');
      });
    }
  })
  });

 oTable = $('#category_table').dataTable({
});
 });
</script>

<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
      Company
      <small>Create  Company </small>
    </h1>

    <ol class="breadcrumb">
        <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."company/view"; ?>">Company</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div align="right">
         <a title="short key-ALT+B" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."company/view"; ?>"accesskey="b"><i class="fa fa-mail-reply-all"></i> Back</a>
       </div>

     </div>

     <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row">
        <div class="col-md-12">

 
  <?php
                        if($this->uri->segment(1)=="add")
                        $clas   =   'required="required"';
                      else
                      $clas = '';
                    ?> 
           <div class="col-md-3">
          <div class="form-group">
                        <label for="focusinput" class="control-label">Image(1600 X 400)</label>
                        
                       <input class="form-control" type="hidden" name="id" value="<?php echo $id;?>">
                        <input type="file" name="image" onchange="readURL(this);" <?php echo $clas; ?>>
                        <?php if($image != "") { echo "<img id='blah' width='180px' height='65px' src='".$this->config->item("base_url")."uploads/company/".$image."'>"; } else { echo "<img id='blah' width='150px' height='150px' src='".$this->config->item("base_url")."uploads/Default2.png"."'>"; } ?>

                      </div>
                    </div>
           
 
 
          
            <div class="form-group col-md-3">
            <label  class="control-label">Company<sup>*</sup></label>
             <input class="form-control"  type="hidden" name="id" value="<?=$id ?>">
            <input class="form-control input-sm"  tabindex="1" style="width:190px;height:29px"; required="required"  tabindex="1" type="text" name="vendor" value="<?= $name ?>" >                              
          </div>    
             
             <div class="form-group col-md-3">
              <label  class="control-label">Tag Line</label>
          <input class="form-control input-sm" style="width:190px;height:29px";    tabindex="2"   type="text" name="tags" value="<?= $tags ?>" >                
          </div> 
           <div class="form-group col-md-3">
            <label  class="control-label">Drug Lic.No<sup>*</sup></label>
          <input class="form-control input-sm validate[required]" style="width:190px;height:29px"; tabindex="3" type="text" name="drug_lic" value="<?= $drug_lic ?>" >                                             
          </div>


           <div class="form-group col-md-3">
            <label  class="control-label">GST.No<sup>*</sup></label>
          <input class="form-control input-sm validate[required]" style="width:190px;height:29px"; tabindex="3"  type="text" name="gst_no" value="<?= $gst_no ?>" >                
          </div>

           <div class="form-group col-md-3">
            <label  class="control-label">Reg Fee<sup>*</sup></label>
          <input class="form-control input-sm validate[required]" style="width:190px;height:29px"; placeholder="Reg Fee"  tabindex="2"   type="text" name="reg_fee" value="<?= $reg_fee ?>" >        
          </div>
           <div class="form-group col-md-3">
            <label  class="control-label">Renew Fee</label>
            <input class="form-control input-sm validate[required]"  tabindex="12" placeholder="Renew Fee" style="width:190px;height:29px";  type="text" name="renew_fee" value="<?= $renew_fee  ?>">                         
          </div>

<div class="form-group col-md-3">
            <label  class="control-label">Address (Street 1)<sup>*</sup></label>
            <input name="address" style="width:190px;height:29px"; required="required" tabindex="4" type="text" class="form-control input-sm" value="<?= $address ?>" >
          </div>
           <div class="form-group col-md-3">
            <label  class="control-label">Phone<sup>*</sup></label>
          <input class="form-control input-sm" style="width:190px;height:29px"; required="required"  tabindex="5" type="text" name="phone" value="<?= $phone ?>" >                
          </div>
           <div class="form-group col-md-3">
            <label  class="control-label">Email </label>
            <input class="form-control validate[required,custom[email]] input-sm"  tabindex="6" placeholder="Email" style="width:190px;height:29px";  type="text" name="email" value="<?= $email  ?>">  
          </div>


           <div class="form-group col-md-3">
             <label  class="control-label"> &emsp; </label>
             <select class="form-control input-sm"  style="width:190px;height:29px"; required="required" tabindex="6" tabindex="7"  name="city"  id="city"> 
              <option value="Select">--City--</option>
              <?php foreach ($cities as $key => $company) {
                ?>
              <option value="<?php echo $company['id']?>"<?php if($company['id']==$city) { ?> selected="selected" <?php } ?>><?php echo $company['name']?></option>
                <?php
              }?>
            </select>
          </div>
           <div class="form-group col-md-3">
            <label  class="control-label"> </label>
            <input    type="text"    placeholder="Street 2" style="width:190px;height:29px";  tabindex="7" name="street" class="form-control input-sm" value="<?= $street ?>">
          </div>
           <div class="form-group col-md-3">
             <label  class="control-label">Website<sup>*</sup></label>
             <br>
          <input   class=" form-control input-sm" style="width:190px;height:29px"; required="required" tabindex="9" type="text" name="website" value="<?= $website ?>" >                
          </div> 

          <div class="form-group col-md-3">
            <label  class="control-label">  </label>
           <!--  <input class="form-control" placeholder="State" tabindex="7"  style="width:190px;height:29px";  type="text" name="state" value="<?= $state  ?>">                 -->
          <select class="form-control input-sm"  style="width:190px;height:29px"; required="required"  tabindex="10"  name="state"   id="state"> 
              <option value="Select">--State--</option>
              <?php foreach ($states as $key => $company) {
                ?>
              <option value="<?php echo $company['id']?>"<?php if($company['id']==$state) { ?> selected="selected" <?php } ?>><?php echo $company['name']?></option>
                <?php
              }?>
            </select>
          </div> 
          <div class="form-group col-md-3">
             <label  class="control-label">  </label>

            <select class="form-control input-sm"  style="width:190px;height:29px"; required="required"  tabindex="11"  name="country"  id="country"> 
              <option value="Select">--Country--</option>
              <?php foreach ($countries as $key => $company) {
                ?>
              <option value="<?php echo $company['id']?>"<?php if($company['id']==$country) { ?> selected="selected" <?php } ?>><?php echo $company['name']?></option>
                <?php
              }?>
            </select>
          </div>
           </div>
            <div class="col-md-12">
    <div class="form-group col-md-2">
           <label  class="control-label">  </label>
            <input class="form-control validate[custom[zip]] input-sm  "  data-prompt-position="topRight:10" style="width:190px;height:29px";    tabindex="12" type="text" name="zip" value="<?= $zip ?>">                
          </div>
        </div>
      </div> </div>
    <div class="box-footer">
      <div class="row">
        <div class="col-md-12">
          <div>
            <input  class="btn-large btn-primary btn" type="submit" value="Submit"  tabindex="15"  accesskey="s" title="short key-ALT+S" name="submit"> 
            <input class="btn-large btn-default btn" type="reset" value="Reset">
          </div>
        </div>

      </div>
    </div>
    <?php echo form_close(); ?>
    <script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
            .attr('src', e.target.result)
            .width(180)
            .height(65);
        };

        reader.readAsDataURL(input.files[0]);
        
    }
}

</script>
  
</section>

</section><!-- /.right-side -->


