
<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>


<?php
foreach($preturns as $return)
{
  $ve_id          = $return['ve_id'];
  $ve_bill_no     = $return['ve_bill_no'];
  $ve_vno         = $return['ve_vno'];
  $ve_date        = $return['ve_date'];
  $ve_mrd         = $return['ve_mrd'];
  $ve_customer    = $return['ve_customer'];
  $patient        = $return['ve_patient'];
  $patient_phone  = $return['ve_phone'];
  $ve_amount      = $return['ve_amount'];
  $ve_sgst        = $return['ve_sgst'];
  $ve_cgst        = $return['ve_cgst'];
  $ve_gtotal      = $return['ve_gtotal']; 
  $ve_apayable    = $return['ve_apayable']; 
  $ve_round       = $return['ve_round']; 
  $ve_type        = $return['ve_user']; 
}

?>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {

 oTable = $('#category_table').dataTable({
});
});
</script>

<div class="content-wrapper">
  <section class="content-header">
    <h1>
     Sales Return
      <small>View Sales Return</small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."preturn/return_Slist"; ?>">Sales Return</a></li>
      <li class="active"><?= $page_title?></li>
    </ol>
    <?php if($this->session->flashdata('Success')){ ?>
           <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
  </section>
  <section class="content">
    <div class="box box-primary">
      
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?= $page_title?>
       </h3>
        <div class="box-tools">
              <a title="Back" class="btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."preturn/return_Slist"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>
     </div>


      <form id="post_form">
     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">

        <div class="form-group col-md-2">
            <label  class="control-label">Return No. : </label> <?=  $ve_bill_no ?><br>
             <input type="hidden" value="<?=$ve_id?>" id="ve_id">
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Return Date : </label> <?= date("d-m-Y",strtotime($ve_date)) ?><br>
             <input type ="hidden" name="ve_date" value="<?=$ve_date?>">
          </div>
          <div class="form-group col-md-6"></div>

          <div class="form-group col-md-2">
            <label  class="control-label">Sale Bill No. : </label> <?=  $ve_vno ?><br>
          </div>

          
            <div class="form-group col-md-2">
            <label  class="control-label">Patient MRD : </label> <?= $ve_mrd ?><br>      
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">IP No : </label> <?= $ve_customer ?><br>
          </div>

          <div class="form-group col-md-3">
            <label  class="control-label">Patient : </label> <?= $patient ?><br>
          </div>

          <div class="form-group col-md-3">
            <label  class="control-label">Phone No. : </label> <?= $patient_phone ?> <br>            
          </div>
          <div class="form-group col-md-2">    
          <label class="control-label">User Name : </label> <?= $ve_type ?>
          </div>
            </div>
          </div>

          <div class="row">
          <div class="col-md-12">
             
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                  <table class="table table-condensed dataTable no-footer gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Sl. No </th>
                        <th>Product</th>
                        <th>Unit Price</th>
                        <th>Quantity</th>
                        <th>Batch</th>
                        <th>Expiry</th>
                        <th>Sub Total</th>
                        <th>SGST</th>
                        <th>CGST</th>
                        <th>Sub Total(Inc. GST)</th>
                      </tr>
                    </thead>
                  <tbody>
                  <?php
                  $slno=1;
                  foreach($preturns as $preturn)
                  { 
                    ?> 
                    <tr>
                      <td align="center"><?= $slno++  ?></td>
                      <td><a href="<?= $this->config->item('admin_url')."product/view/".$preturn['ved_itemid'] ?>"><?=$preturn['ved_item'] ?></a></td>
                      <td><?=$preturn['ved_price'] ?></td>
                      <td><?php if($preturn['ved_unit']!="No.s") 
                      {
                        echo $preturn['ved_qty']/$preturn['ved_uqty'];
                      } 
                      else
                        {
                          echo $preturn['ved_qty'];
                        }
                      echo " ".$preturn['ved_unit'] ?></td>
                      <td><?=$preturn['ved_batch'] ?></td>
                      <td><?=$preturn['ved_expiry'] ?></td>
                      <td><?=$preturn['ved_total'] ?></td>
                      <td><?=$preturn['ved_sgsta']." (".$preturn['ved_sgstp']." %)" ?></td>
                      <td><?=$preturn['ved_cgsta']." (".$preturn['ved_cgstp']." %)" ?></td>
                      <td><?=$preturn['ved_gtotal'] ?></td>
                    </tr>
                    <?php } ?>

                  </tbody>
                  </table> 
                
                  <table class="table table-bordered">
                    <tr><th width="80%">Total</th>
                      <th>Rs. <label><?=$ve_amount?></label></th></tr>
                      <tr><th width="80%">Total CGST</th>
                      <th>Rs. <label><?=$ve_cgst?></label></th></tr>
                      <tr><th width="80%">Total SGST</th>
                      <th>Rs. <label><?=$ve_sgst?></label></th></tr>
                      <tr><th width="80%">Total (Inc. GST)</th>
                      <th>Rs. <label><?=$ve_gtotal?></label></th></tr>
                      <tr><th width="80%">Round Off</th>
                      <th>Rs. <label><?=$ve_round?></label></th></tr>
                      <tr><th width="80%">Amount Payable</th>
                      <th>Rs. <label><?=$ve_apayable?></label></th></tr>
                  
                    </table>
<input type="hidden" id="Printid" value="<?=$ve_id  ?>">
<a href="#" class="btn btn-warning" onclick="print();"><i class="fa fa-print"></i> Print</a>
<!-- <input class="btn-large btn-primary btn" type="submit" value="Save and Print" name="submit">  -->

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

           

        </div>
      </div>
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            
          </div>

        </div>
      </div>
     
    </form>
   
</section>

</div><!-- /.right-side -->

<script>
function print(){

          var Printid         =  $('#Printid').val();
          window.location = "<?php echo base_url();?>index.php/preturn/getSprint?sPrintid="+Printid;

}
</script>


