          <input class="form-control input-sm"   type="text" id="product" tabindex="10" >
          <input class="form-control" type="hidden" id="product_name">
          <input class="form-control" type="hidden" name="productid" id="productid">
          <input class="form-control" type="hidden" id="product_sgst">
          <input class="form-control" type="hidden" id="product_cgst"> 
          <input class="form-control" type="hidden" id="product_stock">   
          <input class="form-control" type="hidden" id="product_ustock">   
          <input class="form-control" type="hidden" id="product_uqty">   
          <input class="form-control" type="hidden" id="product_uprice"> 
          <input class="form-control" type="hidden" id="product_price">   
<script type="text/javascript">
  $("#product").autocomplete({
      source: [<?php
      $i=0;
      foreach ($products as $product){
        if ($i>0) {echo ",";}
        echo '{value:"' .$product['ved_item']." - ". $product['ved_batch'].'",pid:"' . $product['ved_itemid'] . '",productname:"' . $product['ved_item'] . '",productbatch:"' . $product['ved_batch'] . '",productexpiry:"' . $product['ved_expiry']. '",cgst:"' . $product['pd_cgst'] . '",sgst:"' . $product['pd_sgst'] . '",productstock:"' . $product['stock_qty'] / $product['pd_qty']  . '",productustock:"' . $product['stock_qty']. '",productuqty:"' . $product['pd_qty'] . '",productuprice:"' . $product['ved_slprice']/$product['pd_qty'] . '",productprice:"' . $product['ved_slprice'] . '"}';
        $i++;
      }
      ?>],
        minLength: 3,//search after one characters
        delay: 300 ,
        select: function(event,ui){

          $("#productid").val(ui.item ? ui.item.pid : '');
          $("#product_name").val(ui.item ? ui.item.productname : '');
          $("#product_batch").val(ui.item ? ui.item.productbatch : '');
          $("#product_expiry").val(ui.item ? ui.item.productexpiry : '');
          $("#product_sgst").val(ui.item ? ui.item.sgst : '');
          $("#product_cgst").val(ui.item ? ui.item.cgst : '');
          $("#product_stock").val(ui.item ? ui.item.productstock : '');
          $("#product_ustock").val(ui.item ? ui.item.productustock : '');
          $("#product_uqty").val(ui.item ? ui.item.productuqty : '');
          $("#product_uprice").val(ui.item ? ui.item.productuprice : '');
          $("#product_price").val(ui.item ? ui.item.productprice : '');
        }
      });  
</script>