<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>

  <section class="right-side" style="min-height:700px;">
    <section class="content-header">
        <h1>
            Users
            <small>Create  Users </small>
        </h1>

        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."priviliges"; ?>">Users</a></li>
      <li class="active"><?php echo $page_title; ?></li>
        </ol>
    </section>
    <section class="content">

        <div class="box box-primary">
                <div class="box-header">
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Successssss!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
              <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
     <?php } ?>
                    <h3 class="box-title">
                       <i class="fa fa-th"></i> Add Priviliges
                   </h3>
       <div align="right">
         <a title="short key-ALT+B" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."priviliges"; ?>" accesskey="b"><i class="fa fa-mail-reply-all"></i> Back</a>
       </div>

               </div>

           <?php echo form_open_multipart($action) ?>
           <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                  <div class="col-md-11">

                  <!-- <div class="col-md-6 ">
                    <div class="form-group required">
                     <label for="category" class="control-label">User Name<sup>*</sup></label>
                    <input class="form-control"  type="hidden" name="id" value="<?=$id ?>">
                        <input class="form-control" required="required"  type="text" name="name" value="<?= $name ?>">

                        </textarea>               
                   </div></div> -->
                   <!--  <div class="col-md-6">
                 <div class="form-group required">
                 <label for="category" class="control-label">Email<sup>*</sup></label>
                        <input class="form-control" required="required"  type="text" name="email" value="<?= $email ?>">                
                                    

              </div></div> -->
               <!--   
               <div class="col-md-6 ">
              <div class="form-group required">
                  <label for="category" class="control-label">Password<sup>*</sup></label>
                        <input class="form-control" required="required"  type="text" name="password" value="<?= $password ?>">                                
              </div></div>
 -->
              <!-- <div class="col-md-6 ">  
              <div class="form-group required">
                  <label for="category" class="control-label">Repeat Password<sup>*</sup></label>
                        <input class="form-control" required="required"  type="text" name="password" value="<?= $password ?>"> 
              </div></div>
               -->
               <div class="col-md-6">
              <div class="form-group required">
                  <label for="category" class="control-label">User Priviliges<sup>*</sup></label>
                        <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
                  <option selected="selected"> </option>
                  <option><a href="feature.php">Features</option>
                  <option>Services</option>
                  <option>Portfolio</option>
                  <option>Clients</option>
                  <option>Blog</option>
                </select>
              </div></div>
              <div class="col-md-6">  
               
<!-- <div class="form-group required">
                     <label for="category" class="control-label">Date<sup>*</sup></label>
                        <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="date" class="form-control pull-right" id="datepicker"  value="<?= $date ?>">
                </div> </div>
 -->

                <?php
                        if($this->uri->segment(1)=="add")
                        $clas   =   'required="required"';
                      else
                      $clas = '';
                    ?> 

          <div class="form-group">
            <div align="form-group">
               <div class="col-md-6">  
                        <label for="focusinput" class="control-label">Image(1600 X 400)</label>
                        <input class="form-control" type="hidden" name="id" value="<?php echo $id;?>">
                        <input type="file" name="image" onchange="readURL(this);" <?php echo $clas; ?>>
                        <?php if($image != "") { echo "<img id='blah' width='139px' height='150px' src='".$this->config->item("base_url")."uploads/users/".$image."'>"; } else { echo "<img id='blah' width='150px' height='150px' src='".$this->config->item("base_url")."uploads/Default2.png"."'>"; } ?>
                      </div>
              </div></div></div>

          </div></div>
      </div>
  </div>


 <div class="nav-tabs-custom">
            
           <!--  <ul class="nav nav-tabs">
              <li class="active"><a href="#Features" data-toggle="tab">Features</a></li>
              <li><a href="#Services" data-toggle="tab">Services</a></li>
              <li><a href="#Portfolio" data-toggle="tab">Portfolio</a></li>
               <li><a href="#Clients" data-toggle="tab">Clients</a></li>
                <li><a href="#Blog" data-toggle="tab">Blog</a></li>
            </ul> -->
            <div class="box box-info">  </div>
            
            <div class="tab-content">
              <div class="active tab-pane"  id="Features">
                <div class="col-md-2">
                <label>Create</label>&nbsp;&nbsp;&nbsp;
                  <input type="checkbox" class="minimal">
                <br>
                  <label>View</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="checkbox" class="minimal">

               </div>
                  <label>Edit</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="checkbox" class="minimal"><br>
                <label>Delete</label>&nbsp;&nbsp;
                  <input type="checkbox" class="minimal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class=" " type="submit" value="Submit">
              </div>
        </div>

            <div class="box-body">
            <div id="example_wrapper" class="table-responsive">
                <div class="row">
                        <div class="col-md-12">
                        <div class="col-md-12">
            <table id="category_table" class="table table-condensed dataTable no-footer">
    <thead>
    <tr>
        <th>Heading</th>
         
        <th>Description</th>
        <th>Feature Icon</th>
         
    </tr>
     </thead>
    <tbody>
        
            <tr>
                <td> ghjk</td>                  
                <td>hhj </td>
                <td> hhh</td>

     
    </tr>
          <tr>
                <td> ghjk</td>                  
                <td>hhj </td>
                <td> hhh</td>

     
    </tr>   
         
          
    </tbody>
    </table>
</div> </div> </div> 
</div>             
      <!-- </div> -->

 <div class="box-footer">
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <input class="btn-large btn-primary btn" type="submit" value="Submit" accesskey="s" title="short key-ALT+S" > 
                        <input class="btn-large btn-default btn" type="reset" value="Reset">
                    </div>
                </div>

            </div>
        </div>
        <script>
 $('#datepicker').datepicker({
      autoclose: true
    });</script>
    
<?php echo form_close(); ?>



 <script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
            .attr('src', e.target.result)
            .width(139)
            .height(150);
        };

        reader.readAsDataURL(input.files[0]);
        
    }
}

</script>
</div>

</section>

</section> 

