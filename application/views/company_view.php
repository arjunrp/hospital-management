<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>

<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
     Company
      <small>View  Company </small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">    
    <div class="box box-primary">
      <div class="box-header">
 <div align="right">
         <a title="short key-ALT+N" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."company/edit"; ?>" accesskey="n" title="short key-ALT+N"><i class="fa fa-pencil-square-o"></i> Edit</a>
       </div>
        <h3 class="box-title">
          <input class="form-control"  type="hidden" name="id" value="<?=$id ?>">
         <i class="fa fa-th"></i>  <?=$name  ?><br>
        &emsp;<small>[ <?=$tags ?> ]</small>
       </h3>
     </div>
     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">
          <div class="form-group col-md-3">
          <label for="focusinput" class="control-label">Image</label><br>
        <?php if($image != "") { echo "<img width='180px' height='65px' src='".$this->config->item("base_url")."uploads/company/".$image."'>"; } else { echo "<img width='150px' height='300px' src='".$this->config->item("base_url")."uploads/Default.png"."'>"; } ?>
          </div>
 
          <div class="form-group col-md-3">
              <label  class="control-label">Address<sup>*</sup></label><br>
             <?= $address ?> 
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Street<sup>*</sup></label><br>
            <?= $street ?>             
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">City<sup>*</sup></label><br>
           <?php foreach ($cities as $key => $cities) {
              if($cities['id']==$city) { echo $cities['name']; } 
              } ?>  
          </div>

          <div class="form-group col-md-2">
             <label  class="control-label">State<sup>*</sup></label><br>
            <?php foreach ($states as $key => $states) {
              if($states['id']==$state) { echo $states['name']; } 
              } ?>                
          </div>



          <div class="form-group col-md-3">
            <label  class="control-label">Country<sup>*</sup></label><br>
             <?php foreach ($countries as $key => $countries) {
              if($countries['id']==$country) { echo $countries['name']; } 
              } ?>
            </div>

           <div class="form-group col-md-2">
            <label  class="control-label">Zip<sup>*</sup></label><br>
            <?= $zip ?>
          </div>

          <div class="form-group col-md-2">
                 <label  class="control-label">Phone<sup>*</sup></label><br>
                <?= $phone  ?>                 
          </div> 
           <div class="form-group col-md-2">
            <label  class="control-label">Drug Lic No.<sup>*</sup></label><br>
            <?= $drug_lic  ?>             
          </div> 
           <div class="form-group col-md-3">         
          </div> 
            
          <div class="form-group col-md-3">
            <label  class="control-label">Email<sup>*</sup></label><br>
            <?= $email  ?>             
          </div> 

            <div class="form-group col-md-4">   
            <label  class="control-label">Website<sup>*</sup></label><br>
            <?= $website  ?>                
            </div> 

            <div class="form-group col-md-2">   
            <label  class="control-label">GST.No<sup>*</sup></label><br>
            <?= $gst_no  ?>                
            </div> 
            <div class="form-group col-md-3"></div>
             <div class="form-group col-md-3">   
            <label  class="control-label">Reg Fee<sup>*</sup></label><br>
            <?= $reg_fee  ?>                
            </div> 

              <div class="form-group col-md-6">   
            <label  class="control-label">Renew Fee<sup>*</sup></label><br>
            <?= $renew_fee  ?>                
            </div>

             

        </div>
      </div>
      <div class="box-footer">
        <div class="row">
          

        </div>
      </div>
    </div>
  </div>
</section>
</section> 


