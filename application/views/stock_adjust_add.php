<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script type="text/javascript" charset="utf-8">
  $(function()
  {
    $("#product").autocomplete({
                source: [<?php
                $i=0;
                $unit="";
                foreach ($products as $product){
                  if ($i>0) {echo ",";}
                  echo '{value:"' .$product['ved_item']." - ". $product['ved_batch']  . '",productname:"' . $product['ved_item'] . '",pid:"' . $product['ved_itemid'] . '",productunit:"' .$product['pd_unit']. '",productbatch:"' . $product['ved_batch'] . '",productexpiry:"' . $product['ved_expiry'] . '",productstock:"' . $product['stock_qty'] / $product['pd_qty'] . '",productuqty:"' . $product['stock_qty'] . '",ved_uqty:"' . $product['pd_qty'] . '"}';
                  $i++;
                }
                ?>
                ],
                minLength: 3,//search after one characters
                delay: 300 ,
                select: function(event,ui){

                  $("#productid").val(ui.item ? ui.item.pid : '');
                  $("#product_name").val(ui.item ? ui.item.productname : '');
                  $("#product_uqty").val(ui.item ? ui.item.productuqty : '');
                  $("#product_unit").val(ui.item ? ui.item.productunit : '');
                  $("#batch").val(ui.item ? ui.item.productbatch : '');
                  $("#expiry").val(ui.item ? ui.item.productexpiry : '');
                  $("#product_stock").val(ui.item ? ui.item.productstock : '');
                  $("#product_ustock").val(ui.item ? ui.item.productstock : '');
                  $("#ved_uqty").val(ui.item ? ui.item.ved_uqty : '');
                }
              });  
  });  
  </script>
  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#customer_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>

<!-- Product Delete -->
<script>
  $(document).ready(function() {

    setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);

  $(document).on('click','button.dlte', function() {
    var $row    = $(this).closest("tr");    // Find the row
    var r = confirm("Are you sure want to delete!");
    if (r == true) {
    $(this).closest('tr').remove();
     } 
    });
  });
  </script>
<!-- Product Add -->
  <script>
  $(document).ready(function() {
  $('.button').click(function() {

    var item        = $('#product_name').val();
    var itemid      = $('#productid').val();
    var qty         = $('#product_quantity').val();
    var uqty        = $('#ved_uqty').val();
    var unt         = $('#product_unit').val();
    var batch       = $('#batch').val();
    var expiry      = $('#expiry').val();

     var newrow      = '<tr><td><input type="hidden" value="'+itemid+'" name="ved_itemid[]"><input class="form-control input-sm" type="hidden" value="'+item+'" name="ved_item[]">' + item + '</td><td><input class="form-control input-sm" type="hidden" value="'+batch+'" name="ved_batch[]">'+batch+'</td><td><input type="hidden" value="'+expiry+'" name="ved_expiry[]">'+expiry+'</td><td><input type="hidden" value="'+qty+'" name="ved_qty[]">'+qty+'<input type="hidden" value="'+uqty+'"name="ved_uqty[]"></td><td><input type="hidden" value="'+unt+'" name="ved_unit[]">'+unt+'</td><td><button class="btn btn-xs btn-danger dlte"><i class="fa fa-times-circle-o"></i></button></td></tr>';


    $('#item_table tr:last').after(newrow);

});
  });
  </script>

  <!-- Stock Validation -->
  <script>
  $(document).ready(function() {

    $('#product_stock').blur(function(){
    var product_id = $('#productid').val();
    var unit_name = "";
     $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>sales/getProduct_unit/"+product_id,
     success: function(data){
      $('#ved_unit').empty();
      $("#ved_unit").append('<option value="0">--Select--</option>');
      $.each(data, function(index) {
      unit_name = data[index].pd_unit; 
        $("#ved_unit").append('<option value=' + data[index].pd_unit +'>--'+data[index].pd_unit+'--</option>');
          });
          if(unit_name != "No.s") {
            $("#ved_unit").append('<option value="No.s">--No.s--</option>');
          }
        }
      })
     });


  $('#ved_unit').change(function(){
      var ved_unit        = $('#ved_unit').val();
      $('#product_quantity').val("");
      if(ved_unit=="No.s")
      {
        var pd_stock  = $('#product_uqty').val();
      }
      else
      {
        var pd_stock   = $('#product_ustock').val();
      }
      $('#product_stock').val(pd_stock);
    });

  });
  </script>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>

  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       Stock Adjust
       <small><?=$page_title ?></small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."stock_adjust/"; ?>">Stock Adjust</a></li>
      <li class="active"><?=$page_title ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?=$page_title ?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/stock_adjust/add">
        <i class="fa fa-plus-circle"></i> Create New
      </a>

    </div>
     </div>

      <form id="post_form">
     <div class="box-body"> 
      <div class="row"> 

       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">Transaction No.<sup></sup></label>
          <input class="form-control  input-sm " readonly  type="text" name="ve_vno" value="<?= $stock_adjust_id;?>">                
          <input class="form-control"  type="hidden" name="user_type" value="<?= $user_type;?>"> 
        </div>

        <div class="col-md-2">
          <label  class="control-label">Transaction Date<sup></sup></label>
          <input class="form-control validate[required] input-sm"  tabindex="1" type="text" name="ve_date" id="datepicker" value="<?=date("Y-m-d") ?>"></div>
          <div class="col-md-5"></div>

          <div class="col-md-3">
         <label  class="control-label">Department<sup></sup></label> 
         <input type="input" readonly class="form-control input-sm" value="Central Store"> 
         <input type="hidden" name="ve_supplier" value="15"> 
       </div>
     </div>
       <div class="col-md-12">
      <br>
      <div class="col-md-4">
        <div class="form-group required">
          <label  class="control-label">Item<sup></sup></label>
          <input class="form-control input-sm"  tabindex="2" type="text" id="product">
          <input class="form-control" type="hidden" id="product_name">
          <input class="form-control" type="hidden" name="productid" id="productid">
          <input class="form-control" type="hidden" id="product_uqty">
          <input class="form-control" type="hidden" id="ved_uqty">
        </div> </div>

          <div class="col-md-1">
          <div class="form-group required">
            <label  class="control-label">In Stock<sup></sup></label>
            <input class="form-control input-sm" readonly  type="text" id="product_stock">   
            <input class="form-control" readonly  type="hidden" id="product_ustock">           
          </div> </div>

          <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Batch<sup></sup></label>
              <input class="form-control input-sm" type="text" id="batch">                
            </div> </div>
            <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Expiry<sup></sup></label>
              <input class="form-control input-sm" type="text" id="expiry">                
            </div> </div>

          <div class="col-md-2">
          <div class="form-group required">
            <label  class="control-label">Unit<sup></sup></label>
              <input class="form-control input-sm" readonly id="product_unit" name="ved_unit">
          </div> </div>

        <div class="col-md-1">
          <div class="form-group required">
            <label  class="control-label">Quantity<sup></sup></label>
            <input  class="form-control validate[required] input-sm"  tabindex="3"   data-prompt-position="topRight:150"  type="text" name="productquantity" id="product_quantity">                
          </div> </div>

          <div class="col-md-4">
          <div class="form-group required">
            <label  class="control-label">Remark<sup></sup></label>
            <input type="text" class="form-control validate[required] input-sm"   tabindex="4" data-prompt-position="topRight:150" type="hidden" name="ve_customer">                 
          </div> </div>
        </div>
      </div>

      <div class="box-footer">
            <div class="row">
              <div class="col-md-12">
                <div>
                  <a class="button btn btn-primary"  tabindex="4" >Add to Table</a>
                  <input class="btn-large btn-default btn" id="reset" tabindex="5" type="reset" value="Reset">
                </div>
              </div>

            </div>
          </div>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Product</th>
                        <th>Batch</th>
                        <th>Expiry</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                  <tbody> 
                  </tbody>
                  </table> 
                
                     
<a href="#" class="btn btn-success" onclick="save();" accesskey="s" title="short key-ALT+S"><i class="fa fa-floppy-o"></i> Save</a>
<input type="hidden" id="Printid">
<!-- <input class="btn-large btn-primary btn" type="submit" value="Save and Print" name="submit">  -->

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form> 
</div>


</section>
</section><!-- /.right-side -->

<script>

function save(){

      var postData = $("#post_form").serializeArray();

      var formURL  = "<?= base_url() ?>stock_adjust/stadjustAdd";
      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          $('#Printid').val(data);
          window.location = "<?php echo base_url();?>index.php/stock_adjust/add";
          <?php 
          $this->session->set_flashdata('Success','Stock Adjustment Done'); ?>
        }

      });
}
function print(){

          var Printid         =  $('#Printid').val();
          if(Printid=="")
          {
            alert("Click Save Before Print");
          }
          else
          {
          window.location = "<?php echo base_url();?>index.php/stock_adjust/getPrint?saPrintid="+Printid;
          }

}
</script>
<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'dd-mm-yyyy',
      today:true
    });</script>
