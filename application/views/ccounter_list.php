<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
<!-- <meta http-equiv="refresh" content="30" /> -->
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#purchase_table').dataTable({
  "aaSorting": [[ 0, "desc" ]],
  "bJQueryUI": true,
  "sPaginationType": "full_numbers",
  "iDisplayLength": 10,
  "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
});
});
</script>

<?php
$nfp = "<font color='#FF0000'><b>Not Completed</b></font>"; 
$fp  = "<font color='#006600'><b>Completed</b></font>" ;
?>
<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
     Cash Counter
     <small>Cash</small>
   </h1>
   <ol class="breadcrumb">
    <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Cash Counter</li>
  </ol>&nbsp;&nbsp;

 <!--  <?php if($this->session->flashdata('Success')){ ?>
     <div class="alert alert-success">
      <a href="#" class="close" data-dismiss="alert">&times;</a>
      <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
    </div>
    <?php }else if($this->session->flashdata('Error')){  ?>
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert">&times;</a>
      <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
    </div>
    <?php } ?>
     -->
</section>
<section class="content">    <!-- Success-Messages -->
  <div class="box box-info">
    <div class="box-header">
     
    <h3 class="box-title"> <i class="fa fa-th"></i> Cash Counter
    </h3>
    <div class="box-tools">
              <a title="Today's Report" class="btn btn-sm btn-primary pull-right" href="<?php echo $this->config->item('admin_url')."ccounter/todays_report"; ?>">
                <i class="fa fa-area-chart"></i> Today's Report</a>
            </div>
  </div><!-- /.box-header -->

  <div class="box-body">
    <div id="example_wrapper" class="table-responsive">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-12">
            <table id="purchase_table" class="table table-condensed dataTable no-footer">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Bill No.</th>
                  <th>Department</th>
                  <th>Amount</th>
                  <th>Pay</th> 
                </tr>
              </thead>
              <tbody>
<?php 
  foreach ($output as $key => $row) {
    if($row['ve_type']=="lbi" || $row['ve_type']=="si" || $row['ve_type']=="dis" || $row['ve_type']=="ad" || $row['ve_type']=="scani" || $row['ve_type']=="xrayi") { $ip_no = $row['ve_customer'];  }
      else { $ip_no = "0";  }

      if($row['ve_type']=="lbi" || $row['ve_type']=="lbo")
      {
        $pay_path = "insert/".$row['ve_id']."/".$row['ve_date']."/".$row['ve_apayable'];
        $dp_department = $row['dp_department'];
      }
      if($row['ve_type']=="scani" || $row['ve_type']=="scano")
      {
        $pay_path = "insert/".$row['ve_id']."/".$row['ve_date']."/".$row['ve_apayable'];
        $dp_department = $row['dp_department'];
      }
      if($row['ve_type']=="xrayi" || $row['ve_type']=="xrayo")
      {
        $pay_path = "insert/".$row['ve_id']."/".$row['ve_date']."/".$row['ve_apayable'];
        $dp_department = $row['dp_department'];
      }
      if($row['ve_type']=="si" || $row['ve_type']=="so")
      {
        $pay_path = "insert/".$row['ve_id']."/".$row['ve_date']."/".$row['ve_apayable'];
        $dp_department = $row['dp_department'];
      }
      if($row['ve_type']=="dis")
      {
        $pay_path = "update/".$row['ve_id']."/".$row['ve_date']."/".$row['ve_customer']."/".$row['ve_apayable'];
        $dp_department = "Discharge - ".$row['dp_department'];
      }
      if($row['ve_type']=="dsr")
      {
        $pay_path = "update1/".$row['ve_id']."/".$row['ve_date']."/".$row['ve_customer']."/".$row['ve_apayable'];
        $dp_department = "Discharge Return - ".$row['dp_department'];
      }
      if($row['ve_type']=="opbl")
      {
        $pay_path = "insert/".$row['ve_id']."/".$row['ve_date']."/".$row['ve_apayable'];
        $dp_department = $row['dp_department'];
      }
      if($row['ve_type']=="ad")
      {
        $pay_path = "insert/".$row['ve_id']."/".$row['ve_date']."/".$row['ve_apayable'];
        $dp_department = "Advance - ".$row['dp_department'];
      }
      if($row['ve_type']=="extra")
      {
        $pay_path = "insert1/".$row['ve_id']."/".$row['ve_date']."/".$row['ve_apayable'];
        $dp_department = "Billing - ".$row['dp_department'];
      }
      if($row['ve_type']=="sr")
      {
        $pay_path = "insert/".$row['ve_id']."/".$row['ve_date']."/".$row['ve_apayable'];
        $dp_department = "Sale Return - ".$row['dp_department'];
      }
      ?>
      <tr>
        <td><?=date("d-m-Y",strtotime($row['ve_date']))?></td>
        <td><?=$row['ve_vno']?></td>
        <td><?=$dp_department?></td>
        <td><?php if($row['ve_type']=="dis" || $row['ve_type']=="dsr")
            { echo $row['ve_gtotal']; } else { echo $row['ve_apayable']; } ?></td>
        <td class='btn-group  btn-group-xs'>
          <a class='btn btn-success' onclick="return confirm('Confirm Payment?');" href="<?= $this->config->item('admin_url')."ccounter/".$pay_path ?>" title='Pay'><i class='fa fa-floppy-o'></i></a>
          <a class='btn btn-danger' onclick="return confirm('Are your sure ?');" href="<?= $this->config->item('admin_url')."ccounter/delete/".$row['ve_id'] ?>" title='Delete'><i class='fa fa-times-circle-o'></i></a>
         <!--  <a onclick='return confirm('Are you sure you want to delete this item?');' href='<?=$this->config->item('admin_url')."ccounter/".$pay_path ?>'class='btn btn-success view-btn-edit' title='Pay'><i class='fa fa-floppy-o'></i></a> -->
           </td> 
        </tr><?php
      }
      ?>
            </tbody>
          </table>
        </div> </div> 
      </div></div> 
    </div><!-- /.box-body -->
  </div>
</section>
</section><!-- /.right-side -->

