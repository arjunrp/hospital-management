<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
    <script src="<?= base_url() ?>asset/plugins/timepicker/bootstrap-timepicker.min.js"></script>
     <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/timepicker/bootstrap-timepicker.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script type="text/javascript" charset="utf-8">
 $(function()
  {
    $("#rs_rmno").autocomplete({
      source: [<?php
      $i=0;
      foreach ($get_rooms as $get_room){
        if ($i>0) {echo ",";}
        echo '{value:"' . $get_room['rm_no'] . '",id:"' . $get_room['rm_id'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          // $("#customerid").val(ui.item ? ui.item.vid : '');
          $("#rm_id").val(ui.item ? ui.item.id : '');
        }
      });   
  });   

  </script>
  <script> 
$(document).ready(function() {

  $('#rs_rmno').change(function(){
    var rm_id = $('#rs_rmno').val();
     $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>room_shift/getBeds/"+rm_id,
     success: function(data){
      $('#rs_bdno').empty();
      $("#rs_bdno").append('<option value="0">--Select--</option>');
      $.each(data, function(index) {
        $("#rs_bdno").append('<option value=' + data[index].bd_id +'>'+data[index].bd_no+'</option>');
      });
    }
  })
  });

  });
  </script>


  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#customer_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       IP 
       <small>Room Shift </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."ipregister"; ?>">IP</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/ipregister">
        <i class="fa fa-plus-circle"></i> Today's IP List
      </a>
    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>
      <?php
      foreach ($get_ips as $key => $get_ip) {
      $ip_ipno              = $get_ip['ip_ipno'];
      $ip_date              = date("d-m-Y",strtotime($get_ip['ip_date']));
      $ip_doctor            = $get_ip['u_name'];
      $ip_department        = $get_ip['dp_department'];
      $ip_mrd               = $get_ip['ip_mrd'];
      $bk_name              = $get_ip['p_title']." ".$get_ip['p_name'];
      $bk_phone             = $get_ip['p_phone'];
    }
      ?>

      <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row"> 
       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">Date : <sup></sup></label>
          <?=$ip_date ?>               
        </div>
        <div class="col-md-4">
          <label  class="control-label">IP No. :<sup></sup></label>
          <?=$ip_ipno ?>              
        </div>
         <div class="col-md-4"></div>
        <div class="col-md-3">
         <label  class="control-label">Doctor : <sup></sup></label>
          <?=$ip_doctor ?>
       </div>
         <div class="col-md-3">
         <label  class="control-label">Department : <sup></sup></label>
         <?=$ip_department ?>
       </div>

      </div>
      <div class="col-md-12"><br>
       <div class="col-md-2">
         <label  class="control-label">MRD : <sup></sup></label>
         <?=$ip_mrd ?>
       </div>
       <div class="col-md-4">
         <label  class="control-label">Patient's Name : <sup></sup></label>
         <?=$bk_name ?>
       </div>
       <div class="col-md-3">
         <label  class="control-label">Phone No. : <sup></sup></label>
         <?=$bk_phone ?>
       </div>
     </div>
          </div>
          <div class="row">
          <div class="col-md-12">
            <hr>

        <div class="col-md-2">
          <label  class="control-label">Room No.<sup>*</sup></label>
          <select onchange='this.size=0;' onblur="this.size=0;" onmousedown="if(this.options.length>6){this.size=5;}" class="form-control validate[required]" id="rs_rmno" tabindex="3" name="rs_rmno"> 
            <option value="">---Select---</option>
             <?php foreach ($get_rooms as $get_room){ ?>
             <option value="<?=$get_room['rm_id']?>"><?=$get_room['rm_no']?></option>
             <?php } ?>
          </select>
          <!-- <input class="form-control validate[required]" tabindex="2"  type="text" id="rs_rmno">  -->
          <input class="form-control" type="hidden" name="rs_mrd" value="<?=$ip_mrd ?>">    
          <input class="form-control" type="hidden" name="rs_ip" value="<?=$ip_ipno ?>  "> 
<!--           <input class="form-control" type="hidden" name="rs_rmno" id="rm_id">  -->                
        </div>

        <div class="col-md-2">
          <label  class="control-label">Bed No.<sup>*</sup></label>
          <select class="form-control validate[required]" id="rs_bdno" tabindex="3" name="rs_bdno"> 
            <option value="0">--Select--</option>
          </select>       
        </div>
        <div class="bootstrap-timepicker">
                <div class="form-group col-md-2">
                  <label>Time:</label>
                  <div class="input-group">
                    <input type="text" name="rs_time" class="form-control timepicker">
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              </div>

        <div class="col-md-2">
          <label  class="control-label">Date<sup>*</sup></label>
          <input class="form-control validate[required]" tabindex="1"  type="text" id="datepicker" name="rs_adm" value="<?=date("Y-m-d")?>"> 
        </div>
      </div>
    </div>

          <div class="box-footer">
       <br>
            <div class="row">
              <div class="col-md-12">
                <div>
                  <button class="button btn btn-primary" tabindex="6" type="submit">Room Shift</button>
                  <input class="btn-large btn-default btn" type="reset" value="Reset">
                  <a title="Room Occupancy" class="btn btn-sm btn btn-success btn-sm" href="#" data-toggle="modal" data-target="#myModal1"><i class="fa fa-clone"></i> Occupancy</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php echo form_close(); ?>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover gridView" id="category_table">
                    <thead>
                      <tr>
                        <th>Room No.</th>
                        <th>Bed No</th>
                        <th>Admitted Date</th>
                        <th>Admitted Time</th>
                        <th>Vacated Date</th>
                        <th>Vacated Time</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                  <tbody> 
                    <?php if($output) { echo $output; } ?>
                  </tbody>
                  </table> 
                

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none; width=100%">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                      <h4 class="modal-title" id="myModalLabel"><b>Room Occupancy</b></h4>
                    </div>                                                
        
                    <div class="modal-body">
                      <div class="box-body">
                  <div class="row">
                <div class="col-md-12">
                <div class="col-md-12">

                  <div class="row">
                  <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <?php
                                $count=1;
                                 foreach ($bed_occup as $key => $get_room) {
                                  ?>   <?php if($count==1)  { echo "<tr>"; } if($count>5) { echo "<tr>"; }  ?> <th><span class="label 

                                  <?php if($get_room['bd_avail']==0){ echo "label-danger"; } 
                                  else 
                                  {
                                    echo "label-primary";
                                  }
                                  ?>
                                  ">

                                  <?=$get_room['rm_no']." - ".$get_room['bd_no']  ?></span>
                                  </th>
                                  <?php $count++;
                                   if($count>5) { echo "</tr>"; $count=1; }  ?>
                                 <?php
                               } ?>
                                  </tr>
                            </thead>
                            <tbody>
                              
                    </tbody>
                </table>
            </div> </div> 
            </div>       
                    </div>
                  </div>
              </div> </div>
                  
            </div></div></div>

</div>
</section>

</section><!-- /.right-side -->


<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });
$('.timepicker').timepicker({
      showInputs: false,
      minuteStep: 1,
    });


 </script>
<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>