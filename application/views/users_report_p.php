

<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
 <html>
<head>
  <link rel="stylesheet" href='<?= base_url() ?>asset/plugins/jvectormap/jquery-jvectormap-1.2.2.css' type="text/css"/>
  <link media="all" type="text/css" rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.css">
   <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.min.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.min.css' type="text/css"/>
   <link rel="stylesheet" href="<?= base_url() ?>asset/css/skins/_all-skins.min.css"> 
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
 
  <!-- <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datepicker/datepicker3.css"> -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- <link rel="stylesheet" href="<?= base_url() ?>asset/css/AdminLTE.min.css" type="text/css"/> -->
   <link rel="stylesheet" href="<?= base_url() ?>asset/bootstrap/css/AdminLTE.min.css" type="text/css"/>
   <script  src="<?= base_url() ?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
</head>
 <body onload="window.print();" style="margin-top:0%;display:block;height:100%;width: 100%;" >
  <script type="text/javascript" charset="utf-8">
$(document).ready(function() {
  $('.tbsal').hide();
  $('.tbbal').hide();
  $('.tbret').hide();
  $('.tbpur').hide();
var r_type= "<?= $r_type ?>";
if(r_type==1)
{
  $('.tbsal').hide();
  $('.tbbal').hide();
  $('.tbret').hide();
  $('.tbpur').show();

}
else if(r_type==2)
{
  $('.tbsal').show();
  $('.tbret').hide();
  $('.tbbal').hide();
  $('.tbpur').show();
}
else if(r_type==3)
{
  $('.tbsal').hide();
  $('.tbpur').hide();
  $('.tbbal').hide();
  $('.tbret').show();
}
else if(r_type==0)
{
  $('.tbsal').hide();
  $('.tbpur').hide();
  $('.tbret').hide();
  $('.tbbal').show();
}

});
</script>
  
  <?php foreach($company as $company) {
    $company_name     = $company['company'];
    $company_lic      = $company['licence_no'];
    $company_address  = $company['address'];
    $company_street   = $company['street'];
    $company_city     = $company['city'];
    $company_state    = $company['state'];
    $company_country  = $company['country'];
    $company_zip      = $company['zip'];
    $company_phone    = $company['phone'];
    $company_email    = $company['email'];
    $company_fax      = $company['fax'];
    $company_website  = $company['website'];

   }?>


  

<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i> <?= $company_name ?>
         <!--  <small class="pull-right">Invoice Date:</small> -->
        </h2>
      </div>
      <!-- /.col -->
    </div>
     <div class="dontprint" align="right"> 
        <a title="Back" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."users/view/$id"; ?>"><i class="fa fa-mail-reply-all"></i> Back</a>
        </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        <address>
          <strong><?= $company_name ?></strong><br>
          <strong>Lic No.:<?= $company_lic ?></strong><br>
          <?= $company_address.", ".$company_street  ?><br>
          <?= $company_city.", ".$company_state.", ".$company_country." - ".$company_zip ?> <br>
          <b>Phone:</b> <?= $company_phone ?>, Fax:  <?= $company_fax ?><br>
          <b>Email:</b> <?= $company_email ?><br>
          <b>Website:</b> <?= $company_website ?>
        </address>
        </div>
      <!-- /.col -->
      
    </div>
    <div class="row">
              <div class="col-md-12">
                <div class="box-header  ">
                  <center><h3 class="box-title"><b><?php if($r_type==1) { echo "Purchase"; } else if($r_type==2) { echo "Sales"; } else if($r_type==0) { echo "Balance Sheet"; } else if($r_type==3) { echo "Return"; } ?></b> Report from <b><?=date("d-m-Y",strtotime($fdate)) ?></b>&nbsp;<b></b> to <b><?=date("d-m-Y",strtotime($tdate)) ?></b> of <b><?php if($product==0) { echo "All Products"; } else { echo $products;  } ?></b></h3></center>
                </div>
                
                 

            <div class="row">
              
              <div class="col-md-12"><br>
                  <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-12">

                      <table id="" class="table table-condensed dataTable no-footer tbret">
                       <thead><b>
                        <tr><th colspan="7" style="text-align:center">Sale Return</th></tr>
                          <tr>
                          <th>Date</th>
                          <th>Product</th>
                          <th>Unit Price</th>
                          <th>Qty</th>
                          <th>Sale Amount</th>
                          <th>GST</th>
                          <th>Amount</th>
                        </tr>
                        </b>
                      </thead><?php if($r_type==3){
                      echo $sroutput;  }
                        ?>
             </table>

                      <table id="" class="table table-condensed dataTable no-footer tbret">
                       <thead><b>
                        <tr><th colspan="6" style="text-align:center">Purchase Return</th></tr>
                          <tr>
                          <th>Date</th>
                          <th>Product</th>
                          <th>Unit Price</th>
                          <th>Qty</th>
                          <th>Amount</th>
                        </tr>
                        </b>
                      </thead>
                      <?php if($r_type==3){
                      echo $proutput;  }
                        ?>
             </table>
                      <table id="" class="table table-condensed dataTable no-footer">
                       <thead><b>
                        <tr>
                          <th class="tbpur">Date</th>
                          <th class="tbpur">Product</th>
                          <th class="tbpur">Unit Price</th>
                          <th class="tbpur">Qty</th>
                          <th class="tbpur">Discount</th>
                          <th class="tbsal">Sale Amount</th>
                          <th class="tbsal">GST</th>
                          <th class="tbpur">Amount</th>
                        </tr>
                        </b>
                      </thead>
                      <tbody>
                      <?php if($r_type==1 || $r_type==2) { echo $output; } ?>
                      </tbody>
                      <?php
                  $sumgsttot=0;
                  $sumam=0;
                  $sumdi=0;
                  $sumay=0;
                  $sumad=0;
                  $sumab=0;                
                  $sumgst=0;
                  $sumrf=0;

                  if (is_array($output1) || is_object($output1))
                  {
                  foreach($output1 as $tsum)
                  {
                    $sumgsttot  = $sumgsttot + $tsum['sumgsttot'];
                    $sumam  = $sumam + $tsum['sumam'];
                    $sumdi  = $sumdi + $tsum['sumdi'];
                    $sumay  = $sumay + $tsum['sumay'];
                    $sumad  = $sumad + $tsum['sumad'];
                    $sumab  = $sumab + $tsum['sumab'];
                       if($r_type==2) {            
                    $sumgst = $sumgst + $tsum['sumgst'];
                    $sumrf  = $sumrf + $tsum['sumrf']; }
                  } } 
              ?>
              <thead>
                <tr<?php if($r_type==3 || $r_type==0) { ?> style="display:none" <?php } ?>><th width="85%">Overall <?php if($r_type==1) { echo "Purchase"; } else if($r_type==2) { echo "Sale"; }?> Discount</th><th><?=$sumdi ?></th></tr>
                <tr<?php if($r_type==3 || $r_type==0) { ?> style="display:none" <?php } ?>><th width="85%">Total Amount Payable</th><th><?=$sumay ?></th></tr>
                <tr<?php if($r_type!=2 || $r_type==0) { ?> style="display:none" <?php } ?>><th width="85%">Total Round Off</th><th><?=$sumrf ?></th></tr>
                <tr<?php if($r_type==3 || $r_type==0) { ?> style="display:none" <?php } ?>><th width="85%">Total Amount Paid</th><th><?=$sumad ?></th></tr>
                <tr<?php if($r_type==3 || $r_type==0) { ?> style="display:none" <?php } ?>><th width="85%">Balance Amount</th><th><?=$sumab ?></th></tr>
              </thead>
             </table>
                       <?php
                       if($r_type==0) {
                      foreach ($output as $output):  
                        ?>
                      <div class="col-md-12">
                      <table class="table table-condensed dataTable no-footer tbbal">
                      <thead>
                        <tr>
                        <th colspan=4 style="text-align: center;"><?= date('d-m-Y', strtotime($output['psdate'])) ?></th></tr>
                        <tr><td colspan=2 style="text-align: center;"><b>Opening Balance : </b> <?= "Rs. ".$output['openingBalance'] ?></td><td colspan=2 style="text-align: center;"><b>Closing Balance : </b> <?= "Rs. ".$output['closingBalance'] ?></td></tr>
                        <tr>
                         <th>Income</th>
                         <th>Amount</th>
                         <th>Expence</th>
                         <th>Amount</th>
                       </tr>
                        </thead>
                         <tr>
                         <td>Sales</td>
                         <td><?php if(empty($output['sales_sum'])) { echo "Rs. 0";} else { echo "Rs.".$output['sales_sum']; } ?></td>
                         <td>Purchase</td>
                         <td><?php if(empty($output['purchase_sum'])) { echo "Rs. 0";} else { echo "Rs.".$output['purchase_sum']; } ?> </td>
                         </tr>
                         <?php if(!empty($output['expense'])) { ?>
                         <?php foreach ($output['expense'] as $outputs) {  ?>
                          <tr>
                          <td colspan=2></td>
                         <td><?= $outputs['category'] ?> </td>
                         <td><?= "Rs.".$outputs['amount'] ?> </td>
                       </tr>
                       <?php } } ?>
                     <tr></tr>
                        <tr><td><br></td></tr>
                        </table>
                      </div>
                     <?php endforeach; } ?>
               
             
              
             
      
        </div>
      </div>
    </div>

  </div>
</div>

</div>
</div>

    <div class="row">
      <!-- accepted payments column -->
       <div class="col-xs-8">
       </div>
      
      

     </div>
       
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
</body>
</html> 
 <style type="text/css" media="print">
.dontprint
{ display: none; }
</style>