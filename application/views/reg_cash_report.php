
<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
 
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>

<div class="content-wrapper">
  <section class="content-header">
    <h1>
     Patient Registration
      <small>Today's Report</small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."patient"; ?>">Patient</a></li>
      <li class="active">Today's Registration Report</li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> Today's Registeration Report
       </h3>
        <div class="box-tools">
              <a title="Back" class="btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."patient"; ?>">
                <i class="fa fa-mail-reply-all"></i> Patients' List</a>
            </div>
     </div>


      <form id="post_form">
     <div class="box-body">

          <div class="row">
          <div class="col-md-12">
             
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">

                     <table class="table table-condensed dataTable no-footer gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Sl. No </th>
                        <th>Date</th>
                        <th>MRD</th>
                        <th>Patient</th>
                        <th>Phone</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                  <tbody>
                  <?php
                  $slno=1;
                  $total_amount = 0;
                  foreach($reg_reports as $reg_report)
                  { 
                    $total_amount = $total_amount + $reg_report['ve_apaid'];
                    ?> 
                    <tr>
                      <td><?= $slno++  ?></td>
                      <td><?=date("d-m-Y",strtotime($reg_report['ve_date'])) ?></td>
                      <td><?=$reg_report['ve_mrd'] ?></td>
                      <td><?=$reg_report['p_title']." ".$reg_report['p_name'] ?></td>
                      <td><?=$reg_report['p_phone'] ?></td>
                      <td><?="Rs. ".$reg_report['ve_apaid'] ?></td>
                    </tr>
                    <?php }  ?>

                  </tbody>
                  </table> 
                
                  <table class="table table-bordered">
                    <tr><th width="60%"></th><th>Total</th>
                    <th><?="Rs. ".$total_amount?></th></tr>

                    </table>
   
<!-- <br><?php if($ve_balance!=0) { ?><a href="#" class="btn btn-success" onclick="save();"><i class="fa fa-floppy-o"></i> Save</a> <?php } ?> -->
<!-- <input type="input" id="Printid"> -->
 <a href="#" class="btn btn-warning" onclick="print();"><i class="fa fa-print"></i> Print</a>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
           

        </div>
      </div>
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            
          </div>

        </div>
      </div>
     
    </form>
   
</section>

</div><!-- /.right-side -->

<script>
function print(){

          window.location = "<?php echo base_url();?>index.php/ccounter/reg_report_print";

}
</script>


