<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
      Sale
      <small>Update  Sale </small>
    </h1>

    <ol class="breadcrumb">
      <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="">Sale</a></li>
      <li class="active">Update Sale</li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
        <h3 class="box-title">
         <i class="fa fa-th"></i>Update Sale
       </h3>

     </div>

     <?php echo form_open_multipart('sales/update_sales') ?>
     <div class="box-body">
      <div class="row">
        <div class="col-md-6 ">

          <div class="form-group required">
            <label  class="control-label">Date<sup></sup></label>
            <input class="form-control" required="required"  type="text" name="customer" value="<?php echo $customer['customer'];?>">                
          </div>
            <input class="form-control" required="required"  type="hidden" name="customerid" value="<?php echo $customer['id'];?>">                

          <div class="form-group required">
            <label  class="control-label">Item<sup></sup></label>
            <textarea name="address" class="form-control"><?php echo $customer['item'];?></textarea>
          </div>

          <div class="form-group required">
            <label  class="control-label">Quantity<sup></sup></label>
            <input class="form-control" required="required"  type="text" name="quantity" value="<?php echo $customer['sale_item_qty'];?>">                
          </div> 

          <div class="form-group required">
            <label  class="control-label">Unit Price<sup></sup></label>
            <input class="form-control" required="required"  type="text" name="price" value="<?php echo $customer['sale_unit_price'];?>">                
          </div>

          <div class="form-group required">
            <label  class="control-label">Discount<sup></sup></label>
            <input class="form-control" required="required"  type="text" name="discount" value="<?php echo $customer['sale_discount'];?>">                
          </div>

        </div>
        <div class="col-md-6 ">

         <div class="form-group required">
          <label  class="control-label">Total<sup></sup></label>
          <input class="form-control" required="required"  type="text" name="state" value="<?php echo $customer['state'];?>">                
        </div>

         
      </div>
    </div>
    <div class="box-footer">
      <div class="row">
        <div class="col-md-12">
          <div>
            <input class="btn-large btn-primary btn" type="submit" value="Submit" name="submit"> 
            <input class="btn-large btn-default btn" type="reset" value="Reset">
          </div>
        </div>

      </div>
    </div>
    <?php echo form_close(); ?>
  </div>
</div>
</section>

</section><!-- /.right-side -->


