<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script> 
$(document).ready(function() {

    $('#op_shift').change(function(){
    var sft_id  = $('#op_shift').val();
    var doc_id  = $('#op_doctor').val();
    var type_id = $('#op_type').val();
    var op_date = $('#datepicker').val();
    // alert(sft_id)
    // alert(op_date)
    if(doc_id==0)
    {
      alert("Select Doctor");
       $('#op_shift').empty();
       $("#op_shift").append('<option value="0">--Select--</option>');
       $("#op_shift").append('<option value="m">--Morning--</option>');
       $("#op_shift").append('<option value="e">--Evening--</option>');
    }
    else {
    var uRL1   = "<?= base_url() ?>opregister/get_token";
    $.ajax({
        url : uRL1,
        type: "POST",
        data : {sft_id:sft_id,doc_id:doc_id,op_date:op_date},
        success:function(data1, textStatus, jqXHR)
        {
          $('#op_opno').val(data1.trim());
        }
  }) 
  } 
  });

  $('#op_shift').blur(function(){
    var sft_id  = $('#op_shift').val();
    var doc_id  = $('#op_doctor').val();
    var op_date = $('#datepicker').val();
    var type_id = $('#op_type').val();

    var uRL    = "<?= base_url() ?>opregister/verify_tokens";
    $.ajax({
        url : uRL,
        type: "POST",
        data : {sft_id:sft_id,doc_id:doc_id,op_date:op_date},
        success:function(data, textStatus, jqXHR)
        {
          $('#token').val(data);
          // alert(data)
          if(data<0)
          {
            alert("Maximum Token Exceeded");
          }
        }
  })
  });

    $('#op_doctor').change(function(){

      $('#op_shift').empty();
      $("#op_shift").append('<option value="0">--Select--</option>');
      $("#op_shift").append('<option value="m">--Morning--</option>');
      $("#op_shift").append('<option value="e">--Evening--</option>');

    var doc_id = $('#op_doctor').val();
    var uRL1   = "<?= base_url() ?>opregister/get_doctor_fee";
    $.ajax({
        url : uRL1,
        type: "POST",
        data : {doc_id:doc_id},
        success:function(data2, textStatus, jqXHR)
        {
          $('#op_docfee').val(data2);

        var op_mrd = $("#op_mrd").val();
        $.ajax({
        url : "<?= base_url() ?>opregister/get_previuos_visit",
        type: "POST",
        data : {op_mrd:op_mrd},
        success: function(data5){
      $('#pre_date').val(data5);
     }
   });

        }
  })
  });

    $('#op_doctor').change(function(){
    var doc_id = $('#op_doctor').val();
    var op_doc_name = $('#op_doctor option:selected').text();
    $('#op_doc_name').val(op_doc_name);
    var uRL1   = "<?= base_url() ?>opregister/get_doctor_dept";
    $.ajax({
        url : uRL1,
        type: "POST",
        data : {doc_id:doc_id},
        success:function(data3, textStatus, jqXHR)
        {
          $('#op_department').val(data3);
        }
  })
  });

  });
  </script>


  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#customer_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>

<?php
foreach ($patients as $key => $patient) {

 $op_mrd       = $patient['p_mrd_no'];
 $op_patient   = $patient['p_name'];
 $op_age       = $patient['p_age'];
 $op_sex       = $patient['p_sex'];
 $op_phone     = $patient['p_phone'];
 $op_address   = $patient['p_address'];
 $op_street    = $patient['p_street'];
 $renew_date   = $patient['p_renew_date'];
 $renew_days   = $patient['p_renew_days'];
 $renew_visits = $patient['p_renew_visits'];
 $renew_count  = $patient['p_renew_count'];
}

?>

  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       OP
       <small>Create OP </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."opregister"; ?>">OP</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/opregister">
        <i class="fa fa-plus-circle"></i> Today's OP List
      </a>
    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>

      <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row"> 
       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">OP No.<sup>*</sup></label>
          <input class="form-control input-sm"  type="text" name="op_opno" id="op_opno" autofocus>
          <input class="form-control" type="hidden" id="token" value="0"> 
          <input class="form-control" type="hidden" name="user_type" value="<?=$user_type?>">              
        </div>
        <div class="col-md-2">
          <label  class="control-label">Date<sup>*</sup></label>
          <input class="form-control input-sm" id="datepicker" tabindex="1" data-prompt-position="topRight:150" type="text" name="op_date" value="<?=date("Y-m-d") ?>">                
        </div>
         
          <div class="col-md-2">
         <label  class="control-label">OP Type<sup>*</sup></label>
         <select class="form-control input-sm" id="op_type" tabindex="2" name="op_type"> 
          <option value="nb">--Non-Booking--</option>
          </select>  
       </div>
        <div class="col-md-3">
         <label  class="control-label">Doctor<sup>*</sup></label>
         <select class="form-control input-sm" id="op_doctor" tabindex="4" name="op_doctor">
          <option value="0">--Select--</option>
          <?php foreach ($doctors as $key => $doctor) {
            ?> <option value="<?=$doctor['u_emp_id'] ?>"><?=$doctor['u_name'] ?></option> <?php 
          } ?>
            </select> 
            <input class="form-control" type="hidden" name="op_doc_name" id="op_doc_name"> 
            <input class="form-control" type="hidden" name="op_department" id="op_department">   
            <input class="form-control" type="hidden" name="op_docfee" id="op_docfee">    
       </div>
       <div class="col-md-2">
         <label  class="control-label">Shift<sup>*</sup></label>
         <select class="form-control input-sm" tabindex="5" id="op_shift" name="op_shift"> 
          <option value="0">--Select--</option>
              <option value="m">--Morning--</option>
             <option value="e">--Evening--</option>
            </select> 
       </div>
      </div>
      <div class="col-md-12">
       <div class="col-md-2">
         <label  class="control-label">MRD<sup>*</sup></label>
         <input class="form-control input-sm" readonly tabindex="6" type="text" id="op_mrd" name="op_mrd" value="<?=$op_mrd ?>">
         <input class="form-control" type="hidden" id="rw_days" value="<?=$renew_days ?>"> 
         <input class="form-control" type="hidden" id="rw_count" value="<?=$renew_count ?>">    
       </div>
       <div class="col-md-2">
         <label  class="control-label">Patient's Name<sup></sup></label>
         <input class="form-control" readonly type="text" tabindex="7" value="<?=$op_patient ?>" name="op_name">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Phone No.<sup></sup></label>
         <input class="form-control" type="text" tabindex="8" value="<?=$op_phone ?>" name="p_phone">
       </div>
       <div class="col-md-1">
         <label  class="control-label">Age<sup></sup></label>
         <input class="form-control input-sm" type="text" value="<?=$op_age ?>" name="p_age">
       </div>
       <div class="col-md-2">
         <label class="control-label">Sex<sup></sup></label>
         <select class="form-control input-sm" name="p_sex" id="p_sex">
          <option value="F" <?php if($op_sex == "F") { ?>selected  <?php } ?>>Female</option>
          <option value="M" <?php if($op_sex == "M") {?> selected <?php } ?>>Male</option>
          <option value="C" <?php if($op_sex == "C") {?>selected <?php } ?>>Child</option>
         </select>
       </div>
       <div class="col-md-3">
         <label  class="control-label">Address<sup></sup></label>
         <input class="form-control input-sm" type="text" value="<?=$op_address?>" name="p_address">
       </div>     </div>
     <div class="col-md-12">
      <br>
       <div class="col-md-2">
         <label  class="control-label">Street<sup></sup></label>
         <input class="form-control input-sm" type="text" value="<?=$op_street?>" name="p_street">
       </div>

       <div class="col-md-2">
         <label  class="control-label">Prevoius Visit Date<sup></sup></label>
         <input class="form-control input-sm" readonly type="text" id="pre_date">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Last Renewal<sup></sup></label>
         <input readonly class="form-control" readonly tabindex="9" type="text" id="p_renew_date" value="<?=$renew_date ?>">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Visits after renewal<sup></sup></label>
         <input readonly class="form-control" readonly tabindex="10" type="text" id="p_renew_visits" value="<?=$renew_visits ?>">
       </div>
     </div>
          </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-md-12">
                <div>
                  <button class="button btn btn-primary" id="sbt_btn" tabindex="6" type="submit">Add to OP Register</button>
                  <input class="btn-large btn-default btn" type="reset" value="Reset">
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php echo form_close(); ?>

       <!--  <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover gridView" id="category_table">
                    <thead>
                      <tr>
                        <th>OP No.</th>
                        <th>Date</th>
                        <th>Doctor</th>
                        <th>Shift</th>
                        <th>MRD</th>
                        <th>Patient's Name</th>
                        <th>Phone No.</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                  <tbody> 
                    <?php if($output) { echo $output; } ?>
                  </tbody>
                  </table> 
                

          </div>
        </div>
      </div>
    </div>
  </div>
</div> -->
</div>
</section>

</section><!-- /.right-side -->


<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });</script>
<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>