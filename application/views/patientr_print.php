<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<html>
<head>
<link rel="stylesheet" href='<?= base_url() ?>asset/plugins/jvectormap/jquery-jvectormap-1.2.2.css' type="text/css"/>
  <link media="all" type="text/css" rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.css">
   <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.min.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.min.css' type="text/css"/>
   <link rel="stylesheet" href="<?= base_url() ?>asset/css/skins/_all-skins.min.css"> 
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
 
  <!-- <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datepicker/datepicker3.css"> -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<!-- <link rel="stylesheet" href="<?= base_url() ?>asset/css/AdminLTE.min.css" type="text/css"/>  -->
   <link rel="stylesheet" href="<?= base_url() ?>asset/bootstrap/css/AdminLTE.min.css" type="text/css"/>
   <script  src="<?= base_url() ?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
   <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
    <style type="text/css">
   .font_reduce
   {
     font-size:small;
   }
   .font_reduce1
   {
     font-size:x-small;
   }
   .u {
    text-decoration: underline;
}
   </style>
</head>
 <body style="margin-top:0%;display:block;height:100%;width:80%;margin-left:9%;margin-right:6% " onload="window.print();" >

<!-- onload="window.print();"  -->
  <?php foreach($company as $company) {
    $company_name     = $company['company'];
    $company_address  = $company['address'];
    $company_street   = $company['street'];
    $company_city     = $company['ctname'];
    $company_state    = $company['stname'];
    $company_country  = $company['coname'];
    $company_zip      = $company['zip'];
    $company_phone    = $company['phone'];
    $company_email    = $company['email'];
    $company_website  = $company['website'];
    $company_gst_no   = $company['gst_no'];
    $company_drug_lic = $company['drug_lic'];
    $company_tags     = $company['tags'];
}


foreach($patients as $patient)
{
  $ve_vno         = $patient['ve_vno'];
  $ve_date        = $patient['ve_date'];
  $ve_mrd         = $patient['ve_mrd'];
  $p_name         = $patient['p_title']." ".$patient['p_name'];
  $p_phone        = $patient['p_phone'];
  $ve_apayable    = $patient['ve_apayable'];

   }?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {

 oTable = $('#category_table').dataTable({
});
});
</script>


<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h4 style="border-bottom: thin dotted #000; font-family: Arial Black, Georgia, Serif; padding-bottom:5px">
          <center><b><?= strtoupper($company_name) ?></b></center>
          <small class="font_reduce"><center><b><?= "[ ".strtoupper($company_tags)." ]" ?></b></center></small>
           <center class="pull-center font_reduce1"><?= strtoupper($company_address).", ".strtoupper($company_street)." ,".strtoupper($company_city)." - ".$company_zip.", PH. : ".$company_phone  ?></center>
           <center class="pull-center font_reduce1"><?= "GST : ".strtoupper($company_gst_no).", Drug Lic No : ".strtoupper($company_drug_lic) ?> </center> </b>
                    </h4>

 <center><b><u>CASH BILL</u></b></center>
      </div>
    </div>

  <div class="box-tools">
              <a title="Back" class="dontprint btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."patient"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>
    </div> </div>
    <!-- info row -->

     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">
           <br>
            <table class="font_reduce" width="100%">
            <tr>
            <th width="20%">Bill No.    : <?= $ve_vno?></th>
            <th width="18%">Date        : <?= date("d-m-Y",strtotime($ve_date))?></th>
            <th width="17%">MRD No      : <?= $ve_mrd?></th>
            <th width="25%">Patient     : <?= $p_name?> </th>
            <th width="20%">Phone No.   : <?= $p_phone?> </th>
          </tr>
           </table>



    </div>
      <!-- /.col -->

    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive" id="example-wrapper">
         <br>
       <table width="100%" class="font_reduce" style="border-top: thin dashed #000">
          <thead>
                      <tr style="border-bottom: thin dashed #000">
                        <th height="35px">Sl. No.</th>
                        <th>Patricular</th>
                        <th>Amount</th>
                    </thead>
                   <tbody> 

                    <tr>
                      <td height="30px"><?= "1"  ?></td>
                      <td><?= "Patient Renewal"  ?></td>
                      <td><?= $ve_apayable?></td>
                    </tr>
                  </tbody>
                  </table> 
                
                    <table class="table font_reduce">
                    <tr><th width="70%"> </th><th>Total</th><th>Rs. <label><?= $ve_apayable ?></label></th></tr>
                      
                 
                    </table>
        </div>
        <div class="col-xs-12" style="text-align:right">
          Authorized Signature  with Seal<br><br><br><br><br><br><br>
      </div>
      <!-- /.col -->
    </div>

     </div>
       
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
 
 <style type="text/css" media="print">
.dontprint
{ display: none; }
</style>
</body>
</html>