<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="<?= base_url() ?>/confirms/bootpopup.js"></script>
<script type="text/javascript" charset="utf-8">
 $(function()
  {
    $("#ve_mrd").autocomplete({
      source: [<?php
      $i=0;
      foreach ($patient as $patientmrd){
        if ($i>0) {echo ",";}
        echo '{value:"' . $patientmrd['op_mrd'] . '",patient:"' .$patientmrd['p_title'] ." " . $patientmrd['p_name'] . '",patientph:"' . $patientmrd['p_phone'] . '",patientop:"' . $patientmrd['op_id'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          // $("#customerid").val(ui.item ? ui.item.vid : '');
          $("#bk_name").val(ui.item ? ui.item.patient : '');
          $("#bk_phone").val(ui.item ? ui.item.patientph : '');
          $("#ve_customer").val(ui.item ? ui.item.patientop : '');
        }
      });   
  });   

  $(function() {
    $("#bk_phone").autocomplete({
      source: [<?php
      $i=0;
      foreach ($patient as $patientmrd){
        if ($i>0) {echo ",";}
        echo '{value:"' . $patientmrd['p_phone'] . '",patient:"' .$patientmrd['p_title'] ." " . $patientmrd['p_name'] . '",patientmrd:"' . $patientmrd['op_mrd'] . '",patientop:"' . $patientmrd['op_id'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          // $("#customerid").val(ui.item ? ui.item.vid : '');
          $("#bk_name").val(ui.item ? ui.item.patient : '');
          $("#ve_mrd").val(ui.item ? ui.item.patientmrd : '');
          $("#ve_customer").val(ui.item ? ui.item.patientop : '');
        }
      });   
  }); 

  $(function() {
    $("#ve_customer").autocomplete({
      source: [<?php
      $i=0;
      foreach ($ippatient as $ipno){
        if ($i>0) {echo ",";}
        echo '{value:"' . $ipno['ip_ipno'] . '",mrd:"' . $ipno['ip_mrd'] . '",patient:"'.$ipno['p_title'] ." " . $ipno['p_name'] . '",patientph:"' . $ipno['p_phone'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          $("#ve_mrd").val(ui.item ? ui.item.mrd : '');
          $("#bk_name").val(ui.item ? ui.item.patient : '');
          $("#bk_phone").val(ui.item ? ui.item.patientph : '');
        }
      });   
  });   

  </script>


  <script type="text/javascript" charset="utf-8">
  $(function()
  {
    $("#product").autocomplete({
      source: [<?php
      $i=0;
      foreach ($products as $product){
        if ($i>0) {echo ",";}
        echo '{value:"' .$product['ved_item']." - ". $product['ved_batch'].'",pid:"' . $product['ved_itemid'] . '",productname:"' . $product['ved_item'] . '",productbatch:"' . $product['ved_batch'] . '",productexpiry:"' . $product['ved_expiry']. '",cgst:"' . $product['pd_cgst'] . '",sgst:"' . $product['pd_sgst'] . '",productstock:"' . $product['stock_qty'] / $product['pd_qty']  . '",productustock:"' . $product['stock_qty']. '",productuqty:"' . $product['pd_qty'] . '",productuprice:"' . $product['ved_slprice']/$product['pd_qty'] . '",productprice:"' . $product['ved_slprice'] . '"}';
        $i++;
      }
      ?>],
        minLength: 3,//search after one characters
        delay: 300 ,
        select: function(event,ui){

          $("#productid").val(ui.item ? ui.item.pid : '');
          $("#product_name").val(ui.item ? ui.item.productname : '');
          $("#product_batch").val(ui.item ? ui.item.productbatch : '');
          $("#product_expiry").val(ui.item ? ui.item.productexpiry : '');
          $("#product_sgst").val(ui.item ? ui.item.sgst : '');
          $("#product_cgst").val(ui.item ? ui.item.cgst : '');
          $("#product_stock").val(ui.item ? ui.item.productstock : '');
          $("#product_ustock").val(ui.item ? ui.item.productustock : '');
          $("#product_uqty").val(ui.item ? ui.item.productuqty : '');
          $("#product_uprice").val(ui.item ? ui.item.productuprice : '');
          $("#product_price").val(ui.item ? ui.item.productprice : '');
        }
      });   
  });  
  </script>
  <script type="text/javascript" charset="utf-8">
  function calcu(p)
  {
    var grand = 0;
    var sgrand = 0;
    var sgsta1 = 0;
    var cgsta1 = 0;
    var pp = parseFloat($('#pod_price'+p).val());
    var qty = parseFloat($('#pod_qty'+p).val());
    var sgstp = parseFloat($('#pod_sgstp'+p).val());
    var cgstp = parseFloat($('#pod_cgstp'+p).val());
    var total = pp * qty;

    var sgsta       = (parseFloat(total) * parseFloat(sgstp))/100;
    var cgsta       = (parseFloat(total) * parseFloat(cgstp))/100;
    var pod_total   = parseFloat(total) + parseFloat(sgsta) + parseFloat(cgsta);
    // alert(sgstp+","+cgstp+","+sgsta+","+cgsta+","+pod_total);

    $('#pod_sgsta'+p).val((Math.round(sgsta * 100) / 100).toFixed(2));
    $('#pod_1sgsta'+p).html((Math.round(sgsta * 100) / 100).toFixed(2));
    $('#pod_cgsta'+p).val((Math.round(cgsta * 100) / 100).toFixed(2));
    $('#pod_1cgsta'+p).html((Math.round(cgsta * 100) / 100).toFixed(2));
    $('#pod_total'+p).val((Math.round(pod_total * 100) / 100).toFixed(2));
    $('#ftotal1'+p).html((Math.round(pod_total * 100) / 100).toFixed(2));
    $('#pod_stotal'+p).val((Math.round(total * 100) / 100).toFixed(2));

    $('.ftotal').each(function(){
        grand += parseFloat(this.value);
    });
    $('.stotal').each(function(){
        sgrand += parseFloat(this.value);
    });
    $('.cgsta').each(function(){
        cgsta1 += parseFloat(this.value);
    });
    $('.sgsta').each(function(){
        sgsta1 += parseFloat(this.value);
    });


    grand1   = (Math.round(grand * 100) / 100).toFixed(2);
    sgrand  = (Math.round(sgrand * 100) / 100).toFixed(2);
    cgsta1  = (Math.round(cgsta1 * 100) / 100).toFixed(2);
    sgsta1  = (Math.round(sgsta1 * 100) / 100).toFixed(2);

    var pDiscount   =  $('#pDiscount').val();
    var grand       =  parseFloat(grand1) - (parseFloat(grand1) * parseFloat(pDiscount))/100; 
    var pDiscount1  =  parseFloat(grand1) * parseFloat(pDiscount)/100;

    $('#discountp').val(pDiscount);
    $('#discountp1').html(pDiscount);
    $('#discounta').val(pDiscount1);
    $('#discounta1').html(pDiscount1);

    gtotal = Math.round(grand);

    $('#gtotal').val(grand);
    $('#gtotal1').html(grand);
    $('#apayable').val(gtotal);
    $('#apayable1').html(gtotal);
    $('#sum1').html(sgrand);
    $('#sum').val(sgrand);
    $('#vecgst1').html(cgsta1);
    $('#vecgst').val(cgsta1);
    $('#vesgst1').html(sgsta1);
    $('#vesgst').val(sgsta1);

    var roundoff    =   Math.round(grand) - parseFloat(grand);
    roundoff        =   (Math.round(roundoff * 100) / 100).toFixed(2);
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);

  }

  $(function() {
    $( "#customer_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>

<!-- Product Delete -->
<script>
  $(document).ready(function() {
  $(".saver").show();
  $('input[type="checkbox"]'). click(function(){
  if($("#payment_type"). prop("checked") == true){
  $("#payment_type1").val("True");
  }
  else {
  $("#payment_type1").val("False");
  } 
  });

    setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);

    $('.ipno').hide();
    $('#ip_op').change(function(){

        $('#ve_mrd').val('');
        $('#bk_name').val('');
        $('#bk_phone').val('');
        $('#ve_customer').val('');

    var ip_op = $('#ip_op').val();
    {
      if(ip_op=="ip")
      {
        $('.ipno').show();
        $('.ipno1').hide();
      }
      else 
      {
        $('.ipno').hide();
        $('.ipno1').show();
      }
    }
  });

     $('#product').change(function(){
      var product_id = $('#productid').val();
      var unit_name = "";
     $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>sales/getProduct_unit/"+product_id,
     success: function(data){
      $('#ved_unit').empty();
      $("#ved_unit").append('<option value="0">--Select--</option>');
      $.each(data, function(index) {
        var unit_name = data[index].pd_unit;
        $("#ved_unit").append('<option value=' + data[index].pd_unit +'>--'+data[index].pd_unit+'--</option>');
          });
          if(unit_name != "No.s") {
            $("#ved_unit").append('<option value="No.s">--No.s--</option>');
          }
        }
      })
     });

     $('#ved_unit').change(function(){
      var ved_unit        = $('#ved_unit').val();
      $('#product_quantity').val("");
      $('#product_qty').val("");
      if(ved_unit=="No.s")
      {
        var pd_stock  = $('#product_ustock').val();
        var pd_price  = $('#product_uprice').val();
      }
      else
      {
        var pd_stock   = $('#product_stock').val();
        var pd_price   = $('#product_price').val();
      }
      pd_price = (Math.round(pd_price * 100) / 100).toFixed(2);
      $('#pd_stock').val(pd_stock);
      $('#pd_price').val(pd_price);
    });

    $('#product_quantity').change(function() {
    var quantity    =  $('#product_quantity').val();
    var stock       =  $('#pd_stock').val();
    if(parseFloat(quantity)>parseFloat(stock))
    {
      alert("Insufficent Stock");
      $('#product_quantity').val("");
      $('#product_quantity').focus();
    }

  });


  $(document).on('click','button.dlte', function() {
    var $row    = $(this).closest("tr");    // Find the row


    var r = confirm("Are you sure want to delete!");
    if (r == true) {
    $(this).closest('tr').remove();

    var grand = 0;
    var sgrand = 0;
    var sgsta1 = 0;
    var cgsta1 = 0;
    var gtotal = 0;
    $('.ftotal').each(function(){
        grand += parseFloat(this.value);
    });
    $('.stotal').each(function(){
        sgrand += parseFloat(this.value);
    });
    $('.cgsta').each(function(){
        cgsta1 += parseFloat(this.value);
    });
    $('.sgsta').each(function(){
        sgsta1 += parseFloat(this.value);
    });
    grand           =   (Math.round(grand * 100) / 100).toFixed(2);
    sgrand          =   (Math.round(sgrand * 100) / 100).toFixed(2);
    cgsta1          =   (Math.round(cgsta1 * 100) / 100).toFixed(2);
    sgsta1          =   (Math.round(sgsta1 * 100) / 100).toFixed(2);

    var pDiscount   =  $('#pDiscount').val(); 
    var pDiscount1  =  parseFloat(grand) * parseFloat(pDiscount)/100;
    var grand       =  parseFloat(grand) - (parseFloat(grand) * parseFloat(pDiscount))/100;
    pDiscount1      =  (Math.round(pDiscount1 * 100) / 100).toFixed(2);
    grand           =  (Math.round(grand * 100) / 100).toFixed(2);

    $('#discountp').val(pDiscount);
    $('#discountp1').html(pDiscount);
    $('#discounta').val(pDiscount1);
    $('#discounta1').html(pDiscount1);

    gtotal = Math.round(grand);

    $('#gtotal').val(grand);
    $('#gtotal1').html(grand);

    $('#apayable').val(gtotal);
    $('#apayable1').html(gtotal);
    $('#sum1').html(sgrand);
    $('#sum').val(sgrand);
    $('#vecgst1').html(cgsta1);
    $('#vecgst').val(cgsta1);
    $('#vesgst1').html(sgsta1);
    $('#vesgst').val(sgsta1);

    var roundoff    =   Math.round(grand) - parseFloat(grand);
    roundoff        =   (Math.round(roundoff * 100) / 100).toFixed(2);
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);
    } 
    });
  });
  </script>
<!-- Product Add -->
  <script>
  $(document).ready(function() {
    var slnos = "1";
    var count = 1;
  $('.button').click(function() {
    var item        = $('#product_name').val();
    var itemid      = $('#productid').val();
    var qty         = $('#product_quantity').val();
    var uqty        = $('#product_uqty').val();
    var unt         = $('#ved_unit').val();
    var batch       = $('#product_batch').val();
    var expiry      = $('#product_expiry').val();
    var price       = $('#pd_price').val();
    var total       = $('#product_quantity').val() * $('#pd_price').val(); 

    var sgstp       = $('#product_sgst').val();
    var cgstp       = $('#product_cgst').val();

    var sgsta       = (parseFloat(total) * parseFloat(sgstp))/100;
    var cgsta       = (parseFloat(total) * parseFloat(cgstp))/100;
    var pod_total   = parseFloat(total) + parseFloat(sgsta) + parseFloat(cgsta);
    
    var sgsta       = (Math.round(sgsta * 100) / 100).toFixed(2);
    var cgsta       = (Math.round(cgsta * 100) / 100).toFixed(2);
    var pod_total   = (Math.round(pod_total * 100) / 100).toFixed(2);
    var total       = (Math.round(total * 100) / 100).toFixed(2);

    // alert(unt);
    var date2 = new Date();
    var date1 = new Date(expiry);
    seconds = Math.floor((date1 - (date2))/1000);
    minutes = Math.floor(seconds/60);
    hours = Math.floor(minutes/60);    
    days = Math.floor(hours/24);
    if(days >= 0)
    {
      if(days<=30 && days >= 0)
      {
        alert("Product expires in "+days+++" days")
      }

    var newrow      = '<tr><td width="2%">'+slnos+++'</td><td width="20%"><input type="hidden" value="'+itemid+'" name="ved_itemid[]"><input class="form-control input-sm" type="hidden" value="'+item+'" name="ved_item[]">' + item + '</td><td width="9%"><input class="form-control input-sm" type="hidden" value="'+batch+'" name="ved_batch[]">'+batch+'</td><td width="8%"><input class="form-control" value="'+expiry+'" type="hidden" name="ved_expiry[]">'+expiry+'</td><td width="7%"><input type="hidden" value="'+price+'" id="pod_price'+count+'" name="ved_price[]">'+price+'</td><td width="6%"><input class="form-control input-sm" type="input" value="'+qty+'" id="pod_qty'+count+'" onkeyup="calcu('+count+')" name="ved_qty[]"><input type="hidden" value="'+uqty+'" name="ved_uqty[]"></td><td width="9%"><input type="hidden" value="'+unt+'" name="ved_unit[]">'+unt+'</td><td width="10%"><input type="hidden" value="'+cgstp+'" id="pod_cgstp'+count+'" name="ved_cgstp[]"><input class="cgsta" type="hidden" value="'+cgsta+'" id="pod_cgsta'+count+'" name="ved_cgsta[]"><b>Rs. </b><label id="pod_1cgsta'+count+'">'+cgsta+'</label> ('+cgstp+'%)</td><td width="10%"><input type="hidden" value="'+sgstp+'" id="pod_sgstp'+count+'" name="ved_sgstp[]"><input class="sgsta" type="hidden" value="'+sgsta+'" id="pod_sgsta'+count+'" name="ved_sgsta[]"><b>Rs. </b><label id="pod_1sgsta'+count+'">'+sgsta+'</label> ('+sgstp+'%)</td><td width="9%"><input type="hidden" value="'+pod_total+'" id="pod_total'+count+'" class="ftotal" name="ved_gtotal[]"><b>Rs. </b><label id="ftotal1'+count+'">'+pod_total+'</label><input class="stotal" type="hidden" value="'+total+'" id="pod_stotal'+count+'" name="ved_total[]"></td><td width="5%"><button class="btn btn-xs btn-danger dlte"><i class="fa fa-times-circle-o"></i></button></td></tr>';

    var sum         = $('#sum').val();
    var vesgst      = $('#vesgst').val();
    var vecgst      = $('#vecgst').val();
    var pDiscount   = $('#pDiscount').val();

    sum       = parseFloat(sum) + parseFloat(total);
    vesgst    = parseFloat(vesgst) + parseFloat(sgsta);
    vecgst    = parseFloat(vecgst) + parseFloat(cgsta);
    apayable  = parseFloat(sum) + parseFloat(vesgst) + parseFloat(vecgst);

    var pDiscount1  =  parseFloat(apayable) * parseFloat(pDiscount)/100;
    var apayable    =  parseFloat(apayable) - (parseFloat(apayable) * parseFloat(pDiscount))/100;

    vesgst        =   (Math.round(vesgst * 100) / 100).toFixed(2);
    vecgst        =   (Math.round(vecgst * 100) / 100).toFixed(2);
    pDiscount1    =   (Math.round(pDiscount1 * 100) / 100).toFixed(2);
    apayable      =   (Math.round(apayable * 100) / 100).toFixed(2);

    $('#discountp').val(pDiscount);
    $('#discountp1').html(pDiscount);
    $('#discounta').val(pDiscount1);
    $('#discounta1').html(pDiscount1);

    gtotal = Math.round(apayable);

    $('#gtotal').val(apayable);
    $('#gtotal1').html(apayable);

    $('#apayable').val(gtotal);
    $('#apayable1').html(gtotal);
    $('#sum1').html(sum);
    $('#sum').val(sum);
    $('#vecgst1').html(vecgst);
    $('#vecgst').val(vecgst);
    $('#vesgst1').html(vesgst);
    $('#vesgst').val(vesgst);

    var roundoff    =   Math.round(apayable) - parseFloat(apayable);
    roundoff        =   (Math.round(roundoff * 100) / 100).toFixed(2);
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);

    $('#item_table tr:first').after(newrow);
    count++;
    $("#product").focus();

    document.getElementById('product').value = "";
    document.getElementById('productid').value = "";
    document.getElementById('product_sgst').value = "";
    document.getElementById('product_cgst').value = "";
    document.getElementById('product_stock').value = "";
    document.getElementById('product_ustock').value = "";
    document.getElementById('product_uqty').value = "";
    document.getElementById('product_uprice').value = "";
    document.getElementById('product_price').value = "";
    document.getElementById('product_batch').value = "";
    document.getElementById('product_expiry').value = "";
    document.getElementById('product_quantity').value = "";
    document.getElementById('pd_stock').value = "";
    document.getElementById('pd_price').value = "";

  }
  else
  {
    alert("Product Expired")
    $('#product_expiry').focus();
  }

});
  });
  </script>
<!-- Discount Add -->
  <script>
  $(document).ready(function() {
    $( "#dialog" ).dialog({
        modal: true,
        bgiframe: true,
        autoOpen: false,
      show: {
        effect: "blind",
        duration: 100
      },
      hide: {
        effect: "explode",
        duration: 500
      }
    });
  $('.alldisc').click(function() {
      // $( "#dialog" ).dialog( "open" );
      var user_pswd         = $('#user_pswd').val();
      var user_pswd         = ""+user_pswd+"";
      bootpopup.prompt("Password","password",
    function(data) { if(JSON.stringify(data)==JSON.stringify(user_pswd)){
      
      var grand             =  0;
    var pDiscount         =  $('#pDiscount').val();
    var sum               =  $('#sum').val();
    var vesgst            =  $('#vesgst').val();
    var vecgst            =  $('#vecgst').val();
    sum1                  =  parseFloat(sum) + parseFloat(vesgst) + parseFloat(vecgst);

    var pDiscount1        =  parseFloat(sum1) * parseFloat(pDiscount)/100;
    var grand             =  parseFloat(sum1) - parseFloat(pDiscount1);
    grand                 =  (Math.round(grand * 100) / 100).toFixed(2);

    pDiscount1            =   (Math.round(pDiscount1 * 100) / 100).toFixed(2);

    gtotal = Math.round(grand);

    $('#discountp').val(pDiscount);
    $('#discountp1').html(pDiscount);
    $('#discounta').val(pDiscount1);
    $('#discounta1').html(pDiscount1);

    $('#gtotal').val(grand);
    $('#gtotal1').html(grand);

    $('#apayable').val(gtotal);
    $('#apayable1').html(gtotal);

    var roundoff    =   Math.round(grand) - parseFloat(grand);
    roundoff        =   (Math.round(roundoff * 100) / 100).toFixed(2);
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);

    } 
else
{
  alert("Invalid Password!");
}
  }
);
  });
  });
  </script>

  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    $password  = $this->session->password;
    ?>
    <div id="dialog" title="Confirm Password">
    <input type="password" size="25" />
  </div>
   <input stle="text-color:#000" type="hidden" value="<?=$password?>" id="user_pswd">
    <section class="content-header">
      <h1>
       Sale
       <small>Create Sale </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."sales/"; ?>">Sale</a></li>
      <li class="active">Add Sale</li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
         
        
        <h3 class="box-title">
         <i class="fa fa-th"></i> Add New Sale
       </h3>

       <div class="box-tools">

      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/sales/add">
       
        <i class="fa fa-plus-circle"></i> Create New
      </a>

    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>

      <form id="post_form">
     <div class="box-body">
        
      <div class="row"> 
       <div class="col-md-12">
         <div class="col-md-3">
         <label  class="control-label">Doctor<sup>*</sup></label>
         <select class="form-control validate[required] input-sm" id="ve_doctor" name="ve_doctor" tabindex="1">
          <option value="0">--Select--</option>
             <?php foreach ($doctors as $key => $doctors) {
                ?>
              <option value="<?php echo $doctors['u_emp_id']?>"><?php echo $doctors['u_name']?></option>
                <?php
              }?>
            </select> 
          </div>
            <div class="col-md-2">
        <label  class="control-label">Credit<sup></sup></label><br>
         <input id="payment_type" type="checkbox" tabindex="2">
         <input  id="payment_type1" type="hidden" name="payment_type" value="False">
       </div>
     </div>
       <div class="col-md-12"><br>
        <div class="col-md-2">
          <label  class="control-label">Invoice No.<sup></sup></label>
          <input class="form-control input-sm" readonly required="required" tabindex="3" type="text" name="ve_vno" value="<?= $sale_ID;?>">   
          <input class="form-control" type="hidden" name="user_type" value="<?= $user_type;?>">   
          <input class="form-control" type="hidden" name="ve_id" value="0">             
        </div>
        <div class="col-md-2">
          <label  class="control-label">Invoice Date<sup></sup></label>
          <input class="form-control input-sm"  tabindex="4" value="<?=date("Y-m-d") ?>"  data-prompt-position="topRight:150"  type="text" name="ve_date"  id="datepicker">                
        </div>

        <div class="col-md-2">
          <label  class="control-label">IP/OP<sup></sup></label>
          <select class="form-control validate[required] input-sm"  tabindex="5" name="ip_op" id="ip_op">
            <option value="op">---OP---</option>
            <option value="ip">---IP---</option>
          </select>
         </div>
         <div class="col-md-2 ipno">
         <label  class="control-label">IP No.<sup></sup></label>
         <input class="form-control input-sm"  tabindex="6" data-prompt-position="topLeft:190" tabindex="5" type="text" name="ve_customer" id="ve_customer"></div>

          <div class="col-md-2 ipno1">
         <label  class="control-label">MRD<sup></sup></label>
         <input class="form-control input-sm" tabindex="7" data-prompt-position="topLeft:190" tabindex="5" type="text" name="ve_mrd" id="ve_mrd"></div>
       <div class="col-md-2">
         <label  class="control-label">Patient's Name<sup></sup></label>
         <input class="form-control input-sm" tabindex="8" data-prompt-position="bottomLeft:190"  tabindex="6" type="text" name="ve_patient" id="bk_name">
         <input type="hidden" name="ve_supplier" value="3">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Phone No.<sup></sup></label>
         <input class="form-control input-sm" data-prompt-position="topLeft:190" tabindex="9" type="text" id="bk_phone" name="ve_phone">
       </div>
      </div>

     <div class="col-md-12">
      <br>
      <div class="col-md-3">
        <div class="form-group required">
          <label  class="control-label">Item<sup></sup></label>
          <input class="form-control input-sm"   type="text" id="product" tabindex="10" >
          <input class="form-control" type="hidden" id="product_name">
          <input class="form-control" type="hidden" name="productid" id="productid">
          <input class="form-control" type="hidden" id="product_sgst">
          <input class="form-control" type="hidden" id="product_cgst"> 
          <input class="form-control" type="hidden" id="product_stock">   
          <input class="form-control" type="hidden" id="product_ustock">   
          <input class="form-control" type="hidden" id="product_uqty">   
          <input class="form-control" type="hidden" id="product_uprice"> 
          <input class="form-control" type="hidden" id="product_price">             
        </div> </div>

          <div class="col-md-2">
          <div class="form-group required">
            <label  class="control-label">Batch<sup></sup></label>
            <input class="form-control input-sm" readonly data-prompt-position="bottomRight:150" name="ved_batch" id="product_batch" tabindex="11">                
          </div> </div>

          <div class="col-md-2">
          <div class="form-group required">
            <label  class="control-label">Expiry<sup></sup></label>
            <input class="form-control input-sm"  data-prompt-position="bottomRight:150" name="ved_expiry" id="product_expiry" tabindex="12">                
          </div> </div>

          <div class="col-md-2">
          <div class="form-group required">
            <label  class="control-label">Unit<sup></sup></label>
              <select class="form-control input-sm" id="ved_unit" name="ved_unit" tabindex="13">
                <option value="0">--Select--</option>
              </select>
          </div> </div>        

          <div class="col-md-1">
          <div class="form-group required">
            <label  class="control-label">Stock<sup></sup></label>
            <input class="form-control input-sm" readonly  type="text" id="pd_stock" tabindex="14">             
          </div> </div>


          <div class="col-md-1">
            <div class="form-group required">
              <label  class="control-label">U.Price<sup></sup></label> 
              <input class="form-control input-sm" type="text" name="ved_price" id="pd_price" tabindex="15">               
            </div> </div>

           <div class="col-md-1">
          <div class="form-group required">
            <label  class="control-label">Qty<sup></sup></label>
            <input class="form-control input-sm validate[required]" data-prompt-position="bottomRight:150" id="product_quantity" name="ved_qty" tabindex="16">   
             <input type="hidden" name="ved_stype" value="dir">                
          </div> </div>

            </div>
          </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-md-12">
                <div>
                  <input class="button btn btn-primary" tabindex="17" type="button" value="Add to Table" >
                  <input class="btn-large btn-default btn" type="reset" value="Reset" tabindex="18">
                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                      <tr>
                        <td><b>DISCOUNT(%)</b></td>
                        <td>
                          <input type="text" name="pDiscount" required="required" id="pDiscount" class="form-control" value="0" />
                        </td>          
                        <td><button class="btn btn-info alldisc" type="button">Add</button></td>
                      </tr>
                  </table>
               
                    <table class="table table-striped table-bordered table-hover gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Sl. No.</th>
                        <th>Product</th>
                        <th>Batch</th>
                        <th>Expiry</th>
                        <th>U.Price</th>
                        <th>Qty</th>
                        <th>Unit</th>
                        <th>CGST</th>
                        <th>SGST</th>
                        <th>Total</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                  <tbody> 
                  </tbody>
                  </table> 
                
                  <table class="table table-bordered">
                    <tr><th width="67.5%">Total</th>
                      <th><input type="hidden" name="sum" readonly id="sum" value="0">Rs. <label id="sum1">0</label></th></tr>

                      <tr><th width="80%">SGST Total</th>
                      <th><input type="hidden" name="ve_sgst" readonly id="vesgst" value="0">Rs. <label id="vesgst1">0</label></th></tr>
                      <tr><th width="80%">CGST Total</th>
                      <th><input type="hidden" name="ve_cgst" readonly id="vecgst" value="0">Rs. <label id="vecgst1">0</label></th></tr>

                    <tr><th width="67.5%">Discount</th><th>
                      <input type="hidden" readonly name="discountp" id="discountp" value="0"><input type="hidden" readonly name="discounta" id="discounta" value="0">Rs. <label id="discounta1"></label> (<label id="discountp1">0</label> %)</th></tr>
                    <tr><th width="80%">Grand Total</th><th><input type="hidden" name="gtotal" readonly id="gtotal" value="0">Rs. <label id="gtotal1"></label></th></tr>
                    <tr><th width="80%">Amount Payable</th><th><input type="hidden" name="apayable" readonly id="apayable" value="0">Rs. <label id="apayable1">0</label></th></tr>
                    <tr><th width="80%">Round off Value</th><th><input type="hidden" name="roundoff" readonly id="roundoff" value="0">Rs. <label id="roundoff1"></label></th></tr>
                    </table>
   <button class="btn-large btn-success btn saver" type="submit" onclick="save();" name="submit1" accesskey="s" title="short key-ALT+S"><i class='fa fa-floppy-o'></i> Save</button>
   <button class="btn-large btn-warning btn saver" type="submit" name="submit1" onclick="save_print();" accesskey="p" title="short key-ALT+P"><i class="fa fa-print"></i> Save & Print</button>
   <button class="btn-large btn-primary btn saver" type="submit" onclick="save_pending();" name="submit1" accesskey="sp" title="short key-ALT+S+P"><i class='fa fa-clock-o'></i> Pending</button>
   <input type="hidden" id="Printid">

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
</div>
</section>

</section><!-- /.right-side -->

<script>

function save(){
  $(".saver").hide();
      var postData = $("#post_form").serializeArray();
      var amount_paid         =  $('#amount_paid').val();
      if(amount_paid!="")
          {
      var formURL  = "<?= base_url() ?>sales/saleAdd";
      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          $('#Printid').val(data);
           window.location = "<?php echo base_url();?>index.php/sales/add";
           <?php 
          $this->session->set_flashdata('Success','saled'); ?>

        }

      });
    }
}

function save_pending(){
  $(".saver").hide();
      var postData = $("#post_form").serializeArray();
      var amount_paid         =  $('#amount_paid').val();
      if(amount_paid!="")
          {
      var formURL  = "<?= base_url() ?>sales/salePending";
      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          $('#Printid').val(data);
           window.location = "<?php echo base_url();?>index.php/sales/add";
           <?php 
          $this->session->set_flashdata('Success','Sale Pending'); ?>

        }

      });
    }
}

function save_print(){
      $(".saver").hide();
      var postData = $("#post_form").serializeArray();
      var amount_paid         =  $('#amount_paid').val();
      if(amount_paid!="")
          {
      var formURL  = "<?= base_url() ?>sales/saleAdd";
      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          window.location     = "<?php echo base_url();?>index.php/sales/getPrint?sPrintid="+data;
        }
      });
      }
} 

</script>

<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });</script>
