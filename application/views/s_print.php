<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<html>
<head>
<link rel="stylesheet" href='<?= base_url() ?>asset/plugins/jvectormap/jquery-jvectormap-1.2.2.css' type="text/css"/>
  <link media="all" type="text/css" rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.css">
   <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.min.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.min.css' type="text/css"/>
   <link rel="stylesheet" href="<?= base_url() ?>asset/css/skins/_all-skins.min.css"> 
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
 
  <!-- <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datepicker/datepicker3.css"> -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<!-- <link rel="stylesheet" href="<?= base_url() ?>asset/css/AdminLTE.min.css" type="text/css"/>  -->
   <link rel="stylesheet" href="<?= base_url() ?>asset/bootstrap/css/AdminLTE.min.css" type="text/css"/>
   <script  src="<?= base_url() ?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
   <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
   <style type="text/css">
   .font_reduce
   {
     font-size:small;
   }
   .font_reduce1
   {
     font-size:x-small;
   }
   .font_reduce2
   {
     font-size:xx-small;
   }
   .font_reduce3
   {
     font-size:medium;
   }

   .u {
    text-decoration: underline;
}
   </style>
</head>


 <body onload="window.print();" style="margin-top:0%;display:block;height:100%;width:100%;padding-left:3%">

<!-- onload="window.print();"  -->
  <?php foreach($company as $company) {
    $company_name     = $company['company'];
    $company_address  = $company['address'];
    $company_street   = $company['street'];
    $company_city     = $company['ctname'];
    $company_state    = $company['stname'];
    $company_country  = $company['coname'];
    $company_zip      = $company['zip'];
    $company_phone    = $company['phone'];
    $company_email    = $company['email'];
    $company_website  = $company['website'];
    $company_gst_no   = $company['gst_no'];
    $company_drug_lic = $company['drug_lic'];
    $company_tags     = $company['tags'];
}


foreach($sales as $sale)
{
  $ve_id          = $sale['ve_id'];
  $ve_vno         = $sale['ve_vno'];
  $ve_date        = $sale['ve_date'];
  $ve_mrd         = $sale['ve_mrd'];
  $ve_customer    = $sale['ve_customer'];
  $ve_doctor      = $sale['u_name'];
  $p_name         = $sale['ve_patient'];
  $p_phone        = $sale['ve_phone'];
  $ve_amount      = $sale['ve_amount'];
  $ve_discount    = $sale['ve_discount'];
  $ve_discounta   = $sale['ve_discounta'];
  $ve_sgst        = $sale['ve_sgst'];
  $ve_cgst        = $sale['ve_cgst'];
  $ve_gtotal      = $sale['ve_gtotal'];
  $ve_apayable    = $sale['ve_apayable'];
  $ve_apaid       = $sale['ve_apaid'];
  $ve_balance     = $sale['ve_balance'];
  $ve_round       = $sale['ve_round'];
  $ve_pstaus      = $sale['ve_pstaus'];
  $ve_type        = $sale['ve_type'];
  $user_name      = $sale['ve_user'];
  $ve_status      = $sale['ve_status'];

   }?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 
 oTable = $('#category_table').dataTable({
});
});
</script>


<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <table width="100%"> <tr>
          <td width="30%" style="border-right: thin dashed #000;padding-right:4%;padding-left:1%">
            <h4 style="border-bottom: thin dotted #000; font-family: Arial Black, Georgia, Serif; padding-bottom:5px">
          <center class="font_reduce1"><b><?= strtoupper($company_name) ?></b></center>
          <small class="font_reduce2"><center><b><?= "[ ".strtoupper($company_tags)." ]" ?></b></center></small>
        </h4>
         <center> <small class="pull-center font_reduce2"><b><u>BILL COPY</u></b></small></center>

    <!-- info row -->

     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">

            <input type ="hidden" id="ve_customer" value="<?=$ve_customer?>">
            <input type ="hidden" id="ve_type" value="<?=$ve_type?>">

          <table class="font_reduce2" width="100%">
            <tr>
              <th>No.            : <?= $ve_vno?></th>
              <th style="text-align:right">Date</th><tr>
              <tr><th colspan="2" style="text-align:right"><?= date("d-m-Y h:i A",strtotime($ve_date))?></th></tr>
            <tr><th colspan="2">Patient   : <?= $p_name?>     </th></tr>
            <tr><th colspan="2">Doctor    : <?= $ve_doctor?>  </th></tr>
           </table>

   </div>
      <!-- /.col -->

    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12">
         <br>
       <table cellspacing="10" width="100%" class="font_reduce2" style="border-top: thin dashed #000;border-bottom: thin solid #000; margin-bottom:10px">
          <thead>
                      <tr style="border-bottom: thin dashed #000">
                        <th height="35px">Sl. No </th>
                        <th>Item</th>
                        <th>Batch</th>
                        <th>Qty</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                   <tbody> 
                    <?php
                    $slno=1;
                    foreach($sales as $sale)
                      { ?> 
                    <tr>
                      <td height="30px"><?= $slno++  ?></td>
                      <td><?=$sale['ved_item'] ?></td>
                      <td><?=$sale['ved_batch'] ?></td>
                      <?php if($sale['ved_unit']!="No.s") { ?>
                      <td><?=$sale['ved_qty']/$sale['ved_uqty']." ".$sale['ved_unit']  ?></td>
                      <?php } 
                      else { ?>
                      <td><?=$sale['ved_qty']." ".$sale['ved_unit']  ?></td>
                      <?php } ?>
                      <td><?="Rs. ".$sale['ved_gtotal'] ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                  </table> 
                
                    <table width="100%" class="font_reduce2" style="border-bottom: thin dashed #000; margin-bottom:10px">
                    <tr><th class="u">Total</th><th class="u" style="text-align:right">Grand Total</th></tr>
                    <tr>
                      <th>Rs. <label><?= $ve_amount ?></label></th>
                      <th style="text-align:right">Rs. <label><?= $ve_apayable ?></label></th>
                    </tr>
                    </table>
        </div>
        <div class="col-xs-12 font_reduce2" style="text-align:right">
          Authorized Signature  with Seal<br><br><br><br><br><br><br>
      </div>
      <!-- /.col -->
    </div>

     </div>
       </div>
          </td>

           <td width="70%" style="padding-left:4%;padding-right:4%">
            <h4 style="border-bottom: thin dotted #000; font-family: Arial Black, Georgia, Serif; padding-bottom:5px">
          <center class="font_reduce1"><b><?= strtoupper($company_name) ?></b></center>
          <small class="font_reduce2"><center><b><?= "[ ".strtoupper($company_tags)." ]" ?></b></center></small>
           <center class="pull-center font_reduce2"><?= strtoupper($company_address).", ".strtoupper($company_street)." ,".strtoupper($company_city)." - ".$company_zip.", PH. : ".$company_phone  ?></center>
           <center class="pull-center font_reduce2"><?= "GST : ".strtoupper($company_gst_no).", Drug Lic No : ".strtoupper($company_drug_lic) ?> </center> </b>
                    </h4>
         <center> <small class="pull-center font_reduce2"><b><u>MEDICINE - <?php if($ve_status=="cr") { echo "CREDIT"; } else { echo "CASH"; } ?> BILL </u></b></small></center>

  <div class="box-tools">
              <a title="Back" class="dontprint btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."sales/add"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>
    <!-- info row -->

     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">
          <table class="font_reduce2" width="100%">
            <tr>

            <th>Bill No.            : <?= $ve_vno?></th>
            <th colspan="3" style="text-align:center">Bill Date           : <?= date("d-m-Y",strtotime($ve_date))?></th>
            
          </tr>
            <tr>
               <?php if($ve_type=="so") { ?>
                <th>MRD No.         : <?= $ve_mrd?> </th>
               <?php } else { ?>
               <th>IP No.           : <?= $ve_customer?> </th>
                <?php } ?>
                <th>Patient         : <?= $p_name?> </th>
                <th>Doctor          : <?= $ve_doctor?> </th>
          </tr>
           </table>

   </div>
      <!-- /.col -->

    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12">
         <br>
       <table cellspacing="10" width="100%" class="font_reduce2" style="border-top: thin dashed #000;border-bottom: thin solid #000; margin-bottom:10px">
          <thead>
                      <tr style="border-bottom: thin dashed #000">
                        <th height="35px">Sl. No </th>
                        <th>Item</th>
                        <th>Batch</th>
                        <th>Expiry</th>
                        <th>Unit Price</th>
                        <th>Quantity</th>
                        <th>Sub Total</th>
                      </tr>
                    </thead>
                   <tbody> 
                    <?php
                    $slno=1;
                    foreach($sales as $sale)
                      { ?> 
                    <tr>
                      <td height="30px"><?= $slno++  ?></td>
                      <td><?=$sale['ved_item'] ?></td>
                      <td><?=$sale['ved_batch'] ?></td>
                      <td><?=$sale['ved_expiry'] ?></td>
                      <td><?="Rs. ".$sale['ved_price'] ?></td>
                      <?php if($sale['ved_unit']!="No.s") { ?>
                      <td><?=$sale['ved_qty']/$sale['ved_uqty']." ".$sale['ved_unit']  ?></td>
                      <?php } 
                      else { ?>
                      <td><?=$sale['ved_qty']." ".$sale['ved_unit']  ?></td>
                      <?php } ?>
                      <td><?="Rs. ".$sale['ved_gtotal'] ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                  </table> 
                
                    <table width="100%" class="font_reduce2" style="border-bottom: thin dashed #000; margin-bottom:10px">
                    <tr><th class="u">Amount</th><th class="u">SGST</th><th class="u">CGST</th><th class="u">Disc.</th><th class="u">Rnd.f</th><th class="u font_reduce3">Grand Total</th></tr>
                    <tr>
                      <th>Rs. <label><?= $ve_amount ?></label></th>
                      <th>Rs. <label><?= $ve_sgst ?></label></th>
                      <th>Rs. <label><?= $ve_cgst ?></label></th>
                      <th>Rs.<label><?= $ve_discounta ?></label></th>
                      <th>Rs. <label><?= $ve_round ?></label></th>
                      <th>Rs. <label><?= $ve_apayable ?></label></th>
                    </tr>
                    </table>
        </div>
        <div class="col-xs-12 font_reduce2" style="text-align:right">
          Authorized Signature  with Seal<br><br><br><br><br><br><br>
      </div>
      <!-- /.col -->
    </div>

     </div>
       </div>
          </td>
        </tr>

      </table>
        </div>
     </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
 
 <style type="text/css" media="print">
.dontprint
{ display: none; }
</style>
</body>
</html>