
<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
 
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
$('#b_type').hide();

$('#d_dept').change(function() {
var d_dept = $('#d_dept').val();
if(d_dept=="3")
{
  $('#b_type').show();
}
else
{
  $('#b_type').hide();
}
});
 oTable = $('.item_table').dataTable({
});
});
</script>
<?php
    $user_dept = $this->session->u_dept;
    ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
     Cash Counter
      <small>Report</small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."ccounter"; ?>">Cash Counter</a></li>
      <li class="active">Report</li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> Cash Report
       </h3>
        <div class="box-tools">
              <a title="Back" class="btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."ccounter"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>
     </div>


      
     <div class="box-body">
          <div class="row">
      <?php echo form_open_multipart($action) ?>
          <div class="col-md-12">
             <div class="col-md-2">
              <label  class="control-label">From<sup></sup></label>
              <input  class="form-control validate[required] input-sm datepicker" data-prompt-position="bottomRight:100" type="text" name="from_date">             
             </div>
             <div class="col-md-2">
              <label  class="control-label">To<sup></sup></label>
              <input  class="form-control validate[required] input-sm datepicker" data-prompt-position="bottomRight:100" type="text" name="to_date">             
             </div>
             <div class="col-md-2">
              <label  class="control-label">Department<sup></sup></label>
              <select class="form-control input-sm" name="dept" id="d_dept">
                <?php if($user_dept=="5") {
                  ?>
                <option value="0">--All--</option>
                <?php } ?>
                <?php if($user_dept=="19") {
                  ?>
                <option value="x">--All--</option>
                <?php } ?>
                <?php foreach ($departments as $department) { 
                  if($user_dept=="5") {
                  ?>
                 <option value="<?=$department['dp_id'] ?>"><?=$department['dp_department'] ?></option>
               <?php }
                  else if($user_dept=="19") {
                  ?>
                 <option value="<?=$department['dp_id'] ?>" <?php if($department['dp_id']=="17") { ?> style="display:none" <?php } ?>><?=$department['dp_department'] ?></option>
               <?php }
               else if($user_dept==$department['dp_id'])
               {
                ?><option value="<?=$department['dp_id'] ?>"><?=$department['dp_department'] ?></option> <?php
                }
              }
                 ?>
                
              </select>
             </div>
             <div class="col-md-2" id="b_type">
              <label  class="control-label">Billing Type<sup></sup></label>
              <select class="form-control input-sm" name="b_type">
                <option value="0">Select</option>
                <option value="sa">Sales</option>
                <option value="re">Others</option>
              </select>
            </div>
              <div class="col-md-2">
              <label  class="control-label">Report Type<sup></sup></label>
              <select class="form-control input-sm" name="f_type">
                <option value="s">Summary</option>
                <option value="d">Detail</option>
              </select>
            </div>
             <div class="col-md-2">
              <label  class="control-label">&emsp;<sup></sup></label><br>
              <button class="btn btn-primary btn-sm" type="submit" name="submit">Filter</button>
             </div>
           </div>
   <?php echo form_close(); ?>
            
            
           <?php echo form_open_multipart($action1) ?>
            <?php if($action1!="") { ?>           
            <div class="col-md-12">
              <br>
              <div class="panel panel-default">   
                <div class="panel-body">

                  <div class="table-responsive">
                      <?php if($type=="d") { ?>    
                    <table class="table table-condensed dataTable no-footer gridView item_table">
                    <thead>
                      <tr>
                        <th>Sl. No </th>
                        <th>Bill Date</th>
                        <th>Bill No</th>
                        <th>MRD</th>
                        <th>Patient Name</th>
                        <th>Patient Ph</th>
                        <th>Payment Date</th>
                        <th>Department</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                  <tbody>
                  <?php
                  $slno=1;
                  $total_amount = 0;
                  if($treports!=0) {
                  foreach($treports as $treport)
                  { 
                    $total_amount = $total_amount + $treport['ve_apaid'];

                   if($treport['ve_type'] == "sr")
                    {
                      $section = " - Sales Return";
                    }
                    else if($treport['ve_type'] == "opbl")
                    {
                      $section = " - OP Billing";
                    }
                    else if($treport['ve_type'] == "ad")
                    {
                      $section = " - Advance Payment";
                    }
                    else if($treport['ve_type'] == "adr")
                    {
                      $section = " - Advance Return";
                    }
                     else if($treport['ve_type'] == "dis")
                    {
                      $section = " - Discharge";
                    }
                     else if($treport['ve_type'] == "dsr")
                    {
                      $section = " - Discharge Return";
                    }
                    else
                    {
                      $section = "";
                    }
                    ?> 
                    <tr>
                      <td><?= $slno++  ?></td>
                      <td><?=date("d-m-Y",strtotime($treport['ve_cash_date'])) ?></td>
                       <td><?=$treport['ve_vno'] ?></td>
                       <td><?=$treport['ve_mrd'] ?></td>
                       <td><?=$treport['ve_patient'] ?></td>
                       <td><?=$treport['ve_phone'] ?></td>
                      <td><?=date("d-m-Y",strtotime($treport['ve_cash_date'])) ?></td>
                      <td><?=$treport['dp_department'].$section ?></td>
                      <td><?="Rs. ".$treport['ve_apaid'] ?></td>
                    </tr>
                    <?php }  ?>

                  </tbody>
                  </table> 
                                    
                  <table class="table table-bordered">
                    <tr><th width="60%"></th><th>Total Collection</th>
                    <th><?="Rs. ".$total_amount?></th></tr>
                    </table>
                    <?php } } ?>
                    <?php if($type=="s") { ?>  
                     <table class="table table-condensed dataTable no-footer gridView item_table">
                    <thead>
                      <tr>
                        <th>Sl. No </th>
                        <th>Date</th>
                        <th>Department</th>
                        <th>Bill No From</th>
                        <th>Bill No To</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                  <tbody>
                  <?php
                  $slno=1;
                  $total_amount = 0;
                  $sect = 1;
                  $sumpharm  = 0;
                  $sumpharmr = 0;
                  $sumpharmb = 0;
                  $count = 1;
                  $count1 = 1;
                  $count2 = 1;
                  if($sreports!=0) {
                  foreach($sreports as $sreport)
                  { 
                    $total_amount = $total_amount + $sreport['apaid'];
                    ?> 
                    <tr>
                      <td><?= $slno++  ?></td>
                      <td><?=date("d-m-Y",strtotime($sreport['ve_cash_date'])) ?></td>
                      <td><?php if($sreport['ve_type'] != "opbl")
                          { echo $sreport['dp_department']; }
                          if($sreport['ve_type'] == "si")
                          {
                            echo $sect++;
                          }
                          else if($sreport['ve_type'] == "so")
                          {
                            echo $sect++;
                          }
                          else if($sreport['ve_type'] == "sr")
                          {
                            echo " - Sales Return";
                          }
                          else if($sreport['ve_type'] == "opbl")
                          {
                            echo "OP Billing";
                          }
                          else if($sreport['ve_type'] == "ad")
                          {
                            echo " - Advance Payment";
                          }
                          else if($sreport['ve_type'] == "adr")
                          {
                            echo " - Advance Return";
                          }
                          else if($sreport['ve_type'] == "dis")
                          {
                            echo " - Discharge";
                          }
                          else if($sreport['ve_type'] == "dsr")
                          {
                            echo " - Discharge Return";
                          }

                      ?></td>
                      <td><?=$sreport['mi_veno'] ?></td>
                      <td><?=$sreport['mx_veno'] ?></td>
                      <td><?="Rs. ".$sreport['apaid'] ?></td>
                      <?php if($sreport['ve_type'] == "si") { $count++;  ?><td><b><?php $sumpharm = $sumpharm + $sreport['apaid']; if($count>2) { echo"Rs. ".$sumpharm; } ?></b></td> <?php } ?>
                      <?php if($sreport['ve_type'] == "sr") { $count1++; ?><td><b><?php $sumpharmr = $sumpharmr + $sreport['apaid']; if($count1>2) { echo "Rs. ".$sumpharmr; } ?></b></td> <?php } ?>
                      <?php if($sreport['ve_type'] == "opbl") { $count2++; ?><td><b><?php $sumpharmb = $sumpharmb + $sreport['apaid']; if($count2>2) { echo "Rs. ".$sumpharmb; } ?></b></td> <?php } ?>
                    </tr>
                    <?php }  ?>

                  </tbody>
                  </table> 
                  <table class="table table-bordered">
                    <tr><th width="60%"></th><th>Total</th>
                    <th><?="Rs. ".$total_amount?></th></tr>
                    </table>
                    <?php } } ?>

 <input type="hidden" name="from_date1" value="<?=$from_date1 ?>">   
 <input type="hidden" name="to_date1" value="<?=$to_date1 ?>">   
 <input type="hidden" name="dept1" value="<?=$dept1 ?>">   
 <input type="hidden" name="f_type1" value="<?=$type ?>">  
 <input type="hidden" name="b_type1" value="<?=$b_type1 ?>">   
 <button class="btn btn-warning btn-sm" type="submit" name="submit1"><i class="fa fa-print"></i> Print</button>

          </div>
        </div>
      </div>
    </div>
<?php } echo form_close(); ?>
  </div>
</div>
           
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            
          </div>

        </div>
      </div>
             </div>
   
</section>

</div><!-- /.right-side -->

<script>
function print(){

          window.location = "<?php echo base_url();?>index.php/ccounter/treport_print";

}
</script>

<script>
 $('.datepicker').datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true
    });</script>


