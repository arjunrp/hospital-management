<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<html>
<head>
<link rel="stylesheet" href='<?= base_url() ?>asset/plugins/jvectormap/jquery-jvectormap-1.2.2.css' type="text/css"/>
  <link media="all" type="text/css" rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.css">
   <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.min.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.min.css' type="text/css"/>
   <link rel="stylesheet" href="<?= base_url() ?>asset/css/skins/_all-skins.min.css"> 
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
 
  <!-- <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datepicker/datepicker3.css"> -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<!-- <link rel="stylesheet" href="<?= base_url() ?>asset/css/AdminLTE.min.css" type="text/css"/>  -->
   <link rel="stylesheet" href="<?= base_url() ?>asset/bootstrap/css/AdminLTE.min.css" type="text/css"/>
   <script  src="<?= base_url() ?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
   <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
   <style type="text/css">
  .font_reduce
   {
     font-size:small;
   }
   .font_reduce1
   {
     font-size:x-small;
   }
   .font_reduce2
   {
     font-size:xx-small;
   }

   .u {
    text-decoration: underline;
}
   </style>
</head>
 <body onload="window.print();" style="margin-top:0%;display:block;height:100%;width:100%; padding-left:10px; padding-right:10px">

<!-- onload="window.print();"  -->
    <?php foreach($company as $company) {
    $company_name     = $company['company'];
    $company_address  = $company['address'];
    $company_street   = $company['street'];
    $company_city     = $company['ctname'];
    $company_state    = $company['stname'];
    $company_country  = $company['coname'];
    $company_zip      = $company['zip'];
    $company_phone    = $company['phone'];
    $company_email    = $company['email'];
    $company_website  = $company['website'];
    $company_gst_no   = $company['gst_no'];
    $company_drug_lic = $company['drug_lic'];
    $company_tags     = $company['tags'];
}

foreach ($lab_bills as $key => $lab_bill) {

    $ve_id            = $lab_bill['ve_id'];
    $ve_vno           = $lab_bill['ve_vno'];
    $ve_date          = $lab_bill['ve_date'];
    $ve_customer      = $lab_bill['ve_customer'];
    $ve_mrd           = $lab_bill['ve_mrd'];
    $p_name           = $lab_bill['ve_patient'];
    $p_phone          = $lab_bill['ve_phone'];
    $p_age            = $lab_bill['ve_amount'];
    $p_sex            = $lab_bill['ve_pono'];
    $ve_apayable      = $lab_bill['ve_apayable'];
    $ve_apaid         = $lab_bill['ve_apaid'];
    $ve_type          = $lab_bill['ve_type']; 
    $doctor           = $lab_bill['ve_doctor']; 
    $ve_status        = $lab_bill['ve_status'];
  }
?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>


       <h4 style="border-bottom: thin dotted #000; font-family: Arial Black, Georgia, Serif; padding-bottom:5px">
          <center class="font_reduce1"><b><?= strtoupper($company_name) ?></b></center>
          <small class="font_reduce2"><center><b><?= "[ ".strtoupper($company_tags)." ]" ?></b></center></small>
           <center class="pull-center font_reduce2"><?= strtoupper($company_address).", ".strtoupper($company_street)." ,".strtoupper($company_city)." - ".$company_zip.", PH. : ".$company_phone  ?></center>
           <center class="pull-center font_reduce2"><?= "GST : ".strtoupper($company_gst_no).", Drug Lic No : ".strtoupper($company_drug_lic) ?> </center> </b>
                    </h4>

 <center class="pull-center font_reduce2"><b><u><?php if($ve_status=="cr") { echo "CREDIT"; } else { echo "CASH"; } ?> - BILL </u></b></center>



              <a title="Back" class="dontprint btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."investigation"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>

    <!-- info row -->
          <table class="font_reduce2" width="100%">
            <tr>
            <th>Bill No.    : <?= $ve_vno?></th>
            <th>Date        : <?= date("d-m-Y h:i A",strtotime($ve_date))?> </th>
            <th colspan="2">Doctor      : <?= $doctor?></th>
            <?php if($ve_type=="lbi") { ?>
            <th>IP No.      : <?= $ve_customer?> </th>
            <?php } ?>
          </tr>
            <tr><td><br></td>
              </tr>
            <tr>
            <th >MRD         : <?= $ve_mrd?> </th>
            <th>Patient     : <?= $p_name?> </th>
            <th>Age         : <?= $p_age?> </th>
            <th>Gender      : <?php if($p_sex=="M") { echo "Male"; } if($p_sex=="F") { echo "Female"; } if($p_sex=="C") { echo "Child"; } ?> </th>
            <th>Phone No.   : <?= $p_phone?> </th>
          </tr>
           </table>

      <!-- /.col -->

    <!-- /.row -->

    <!-- Table row -->

        <table width="100%" class="font_reduce2" style="border-top: thin dashed #000;border-bottom: thin solid #000; margin-bottom:10px">
          <thead>
                      <tr style="border-bottom: thin dashed #000">
                        <th height="20px"></th>
                        <th>Investigation</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                   <tbody> 
                    <?php
                    $slno=1;
                    $ts_test = "";
                    foreach ($lab_bills as $key => $lab_bill)
                      { ?> 
                    <tr>
                      <th height="10px"><?php if($ts_test != $lab_bill['ts_test'])
                      {
                        echo $lab_bill['ts_test'];
                      }  ?></th>
                     <?php  $ts_test = $lab_bill['ts_test']; ?>
                      <td height="15px"><?=$lab_bill['ved_item'] ?></td>
                      <td><?=$lab_bill['ved_gtotal'] ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                  </table> 
                
                    <table width="100%" class="font_reduce2" style="border-bottom: thin dashed #000; margin-bottom:10px">
                    <tbody><tr><th width="60%"> </th><th width="20%">Total</th><th width="20%">Rs. <label><?= $ve_apayable ?></label></th></tr></tbody>
                  </table>
                  <div class="col-md-12 font_reduce2" style="text-align:right">
          Authorized Signature  with Seal<br><br><br><br><br><br><br>
        </div>
<!-- ./wrapper -->
 
 <style type="text/css" media="print">
.dontprint
{ display: none; }
</style>
</body>
</html>