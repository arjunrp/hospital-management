<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
   oTable = $('#category_table').dataTable({
    "aaSorting": [[ 0, "asc" ]],
    "bJQueryUI": true,
    "sPaginationType": "full_numbers",
    "iDisplayLength": 10,
    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
});
});
</script>
<section class="right-side" style="min-height:768px;">
    <section class="content-header">
        <h1>
           Room Occupancy
       </h1>
       <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Room Occupancy</li>
    </ol>&nbsp;&nbsp;
      <?php if($this->session->flashdata('Success')){ ?>
           <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?> 
</section>

<section class="content">    <!-- Success-Messages -->
    <div class="box box-info">
        <div class="box-header">
          
        <h3 class="box-title"><i class="fa fa-th"></i>Room Occupancy
        </h3>
        
    </div><!-- /.box-header -->
   <div id="example_wrapper" class="table-responsive">
    <div class="box-body">
          <table  class="table table-bordered">
            <thead>
                <tr>
                    <th style="width: 10px">Date</th>
                    <th style="width: 10px">Room</th>
                    <th style="width: 10px">Room</th>
                    <th style="width: 10px">Room</th>
                    <th style="width: 10px">Room</th>
                </tr>
                </thead>  

                <tbody>
                  <tr>
                  <td style="width:91px">14-2-2017</td>
                  <td class ="booked_column"><span class="label label-warning">Booked </span></td>
                 <td class ="filled_column"><a href="ff.php"><span class="label label-primary">Book Now</span></a></td>
                  <td class ="booked_column"><span class="label label-warning">Booked </span></td>
                 <td class ="filled_column"><span class="label label-primary">Book Now </span></td>
                  </tr>
                <tbody>
              </table>
    </div><!-- /.box-body -->
</div></div>
</section>
</section><!-- /.right-side -->

<style type="text/css">
  .#booked_column {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}

#filled_column {
  background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
</style>