<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<html>
<head>
<link rel="stylesheet" href='<?= base_url() ?>asset/plugins/jvectormap/jquery-jvectormap-1.2.2.css' type="text/css"/>
  <link media="all" type="text/css" rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.css">
   <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.min.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.min.css' type="text/css"/>
   <link rel="stylesheet" href="<?= base_url() ?>asset/css/skins/_all-skins.min.css"> 
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
 
  <!-- <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datepicker/datepicker3.css"> -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- <link rel="stylesheet" href="<?= base_url() ?>asset/css/AdminLTE.min.css" type="text/css"/> -->
   <link rel="stylesheet" href="<?= base_url() ?>asset/bootstrap/css/AdminLTE.min.css" type="text/css"/>
   <script  src="<?= base_url() ?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
   <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
   <style type="text/css">
    .font_reduce
   {
     font-size:small;
   }
   .font_reduce1
   {
     font-size:x-small;
   }
   .font_reduce2
   {
     font-size:xx-small;
   }
   .font_reduce3
   {
     font-size:medium;
   }

   .u {
    text-decoration: underline;
}
   </style>
</head>
 <body onload="window.print();" style="margin-top:0%;display:block;height:100%;width:90%;margin-left:5%;margin-right:5% ">

<!-- onload="window.print();"  -->
   <?php foreach($company as $company) {
    $company_name     = $company['company'];
    $company_address  = $company['address'];
    $company_street   = $company['street'];
    $company_city     = $company['ctname'];
    $company_state    = $company['stname'];
    $company_country  = $company['coname'];
    $company_zip      = $company['zip'];
    $company_phone    = $company['phone'];
    $company_email    = $company['email'];
    $company_website  = $company['website'];
    $company_gst_no   = $company['gst_no'];
    $company_drug_lic = $company['drug_lic'];
    $company_tags     = $company['tags'];
}

foreach($porders as $porder)
{
  $po_id          = $porder['po_id'];
  $po_pono        = $porder['po_pono'];
  $po_date        = $porder['po_date'];
  $po_supplier    = $porder['po_supplier'];
  $sp_vendor      = $porder['sp_vendor'];
  $po_amount      = $porder['po_amount'];
  $po_dlvry_date  = $porder['po_dlvry_date'];
  $po_dlvry_type  = $porder['po_dlvry_type'];
  $po_avail       = $porder['po_avail'];
  $u_emp_id       = $porder['po_user'];


   }?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>


<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h4 style="border-bottom: thin dotted #000; font-family: Arial Black, Georgia, Serif; padding-bottom:5px">
          <center class="font_reduce1"><b><?= strtoupper($company_name) ?></b></center>
          <small class="font_reduce2"><center><b><?= "[ ".strtoupper($company_tags)." ]" ?></b></center></small>
           <center class="pull-center font_reduce2"><?= strtoupper($company_address).", ".strtoupper($company_street)." ,".strtoupper($company_city)." - ".$company_zip.", PH. : ".$company_phone  ?></center>
           <center class="pull-center font_reduce2"><?= "GST : ".strtoupper($company_gst_no).", Drug Lic No : ".strtoupper($company_drug_lic) ?> </center> </b>
                    </h4>
 <center> <small class="pull-center font_reduce2"><b><u>PURCHASE ORDER</u></b></small></center>

 <div class="box-tools">
              <a title="Back" class="dontprint btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."porder/add"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>

    </div> </div>
    <!-- info row -->

      <div class="col-md-12">
         <br>
          <table class="font_reduce2" width="100%">
            <tr>
              <th width="25%">PO No.          : <?= $po_pono?></th>
              <th width="30%">Order To        : <?= $sp_vendor?></th>
              <th width="25%">Date            : <?= date("d-m-Y",strtotime($po_date))?></th>
          </tr>

            <tr>
            <th width="20%">Delivery Date   : <?= date("d-m-Y",strtotime($po_dlvry_date))?> </th>
            <th width="20%">Delivery Type   : <?= $po_dlvry_type?> </th>
            <th width="20%">Prepared By     : <?= $u_emp_id?> </th>
          </tr>
           </table>

    </div>
      <!-- /.col -->

    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
         <br>
       <table width="100%" class="font_reduce1" style="border-top: thin dashed #000;border-bottom: thin solid #000; margin-bottom:10px">
          <thead>
                      <tr style="border-bottom: thin dashed #000">
                        <th height="30px">Sl. No</font> </th>
                        <th>Product</th>
                        <th>Unit Price</th>
                        <th>Qty</th>
                        <th>Offer Qty</th>
                        <th>SGST</th>
                        <th>CGST</th>
                        <th>Sub Total</th>
                      </tr>
                    </thead>
                  <tbody> 
                    <?php
                    $slno=1;
                    $sum=0;
                    $cgst=0;
                    $sgst=0;
                    foreach($porders as $porder)
                      { ?> 
                    <tr>
                      <td height="25px"><?= $slno++  ?></td></small>
                      <td><?=$porder['pod_item'] ?></a></td>
                      <td><?=$porder['pod_price'] ?></td>
                      <td><?=$porder['pod_qty']." ".$porder['pod_unit']  ?></td>
                      <td><?=$porder['pod_offer_qty']." ".$porder['pod_unit']  ?></td>
                      <td><?=$porder['pod_sgsta']." (".$porder['pod_sgstp']."%)"  ?></td>
                      <td><?=$porder['pod_csgta']." (".$porder['pod_cgstp']."%)"  ?></td>
                      <td><?=$porder['pod_total'] ?></td>
                      <?php
                      $sum     =     $sum + ($porder['pod_price'] * $porder['pod_qty']);
                      $cgst    =     $cgst + $porder['pod_csgta'];
                      $sgst    =     $sgst + $porder['pod_sgsta'];
                      ?>
                    </tr>
                    <?php } ?>
                  </tbody>
                  </table> 
                
                 <table width="100%" class="font_reduce2" style="border-bottom: thin dashed #000; margin-bottom:10px;border-collapse: collapse; border: .5px dotted black;">
                    <tr><th>Total : Rs. <label><?= $sum ?></label></th><th>SGST Total : Rs. <label><?= $sgst ?></label></th><th>CGST Total : Rs. <label><?= $cgst ?></label></th><th>Grand Total : Rs. <label><?= $po_amount ?></label></th></tr>
                    </table>
        </div>
        <div class="col-xs-12 font_reduce2" style="text-align:right">
          Authorized Signature  with Seal<br><br><br><br><br><br><br>
      </div>
      <!-- /.col -->
    </div>

     </div>
       
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
 
 <style type="text/css" media="print">
.dontprint
{ display: none; }
</style>
