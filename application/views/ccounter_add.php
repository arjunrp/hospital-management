<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

    <!-- Amount Paid -->
  <script>
  $(document).ready(function() {
  $('#balance_paid').keyup(function() {

    var amount_paid     =  $('#amount_paid').val();
    var amountpaid      =  $('#amountpaid').val();
    var amount_balance  =  $('#amount_balance').val();
    var amountbalance   =  $('#amountbalance').val();
    var balancepaid     =  $('#balance_paid').val();

    if(balancepaid>=0) {
      amount_paid     =  parseFloat(amountpaid);
      amount_balance  =  parseFloat(amountbalance);
    }

    var balance_pay       = parseFloat(amount_balance) - parseFloat(balancepaid);
    var amount_paid       = parseFloat(amount_paid) + parseFloat(balancepaid);


    $('#amount_paid').val(amount_paid);
    $('#amount_balance').val(balance_pay);
    $('#amount_paid1').html(amount_paid);
    $('#amount_balance1').html(balance_pay);


    if(balance_pay<0)
    {
      var Printid   =  $('#Printid').val();
      alert("Invalid Amount");
      $('#balance_paid').val("");
      $('#balance_paid').focus();
    }


  });
  });
  </script>

<?php
foreach($sales as $sale)
{
  $ve_id          = $sale['ve_id'];
  $ve_vno         = $sale['ve_vno'];
  $ve_date        = $sale['ve_date'];
  $ve_mrd         = $sale['ve_mrd'];
  $ve_customer    = $sale['ve_customer'];
  $p_name         = $sale['ve_patient'];
  $p_phone        = $sale['ve_phone'];
  $ve_amount      = $sale['ve_amount'];
  $ve_discount    = $sale['ve_discount'];
  $ve_discounta   = $sale['ve_discounta'];
  $ve_sgst        = $sale['ve_sgst'];
  $ve_cgst        = $sale['ve_cgst'];
  $ve_gtotal      = $sale['ve_gtotal'];
  $ve_apayable    = $sale['ve_apayable'];
  $ve_apaid       = $sale['ve_apaid'];
  $ve_balance     = $sale['ve_balance'];
  $ve_round       = $sale['ve_round'];
  $ve_pstaus      = $sale['ve_pstaus'];
  $ve_type        = $sale['ve_type'];
  $user_name      = $sale['ve_user'];
}

?>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>

<div class="content-wrapper">
  <section class="content-header">
    <h1>
     Cash Counter
      <small>View  Bill</small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."ccounter"; ?>">Cash Counter</a></li>
      <li class="active">View Cash Bill</li>
    </ol>
    <?php if($this->session->flashdata('Success')){ ?>
           <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
  </section>
  <section class="content">
    <div class="box box-primary">
      
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> View Bill
       </h3>
        <div class="box-tools">
              <a title="Back" class="btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."ccounter"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>
     </div>


      <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">

        <div class="form-group col-md-2">
            <label  class="control-label">Bill No. : </label> <?=  $ve_vno ?><br>
             
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Bill Date : </label> <?= date("d-m-Y",strtotime($ve_date)) ?><br>
             <input type ="hidden" name="ve_date" value="<?=$ve_date?>">
             <input type ="hidden" name="ve_id" value="<?=$ve_id?>">
          </div>

          <div class="form-group col-md-6"></div>

          <div class="form-group col-md-2">    
          <label class="control-label">User Name : </label> <?= $user_name ?>
          </div>


          <div class="form-group col-md-2">
            <label  class="control-label">MRD : </label> <?= $ve_mrd ?><br>
                       
          </div>
          <?php if($ve_type=="si") { ?>
          <div class="form-group col-md-5">
            <label  class="control-label">IP No : </label> <?= $ve_customer ?><br>
          </div>
          <?php } ?>

          <div class="form-group col-md-3">
            <label  class="control-label">Patient : </label> <?= $p_name ?><br>
          </div>

          <div class="form-group col-md-2">
            <label  class="control-label">Phone No. : </label> <?= $p_phone ?> <br>
                         
          </div>
            </div>
          </div>

          <div class="row">
          <div class="col-md-12">
             
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                      <tr>
                      </tr>
                  </table>

               
                     <table class="table table-condensed dataTable no-footer gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Sl. No </th>
                        <th>Product</th>
                        <th>Batch</th>
                        <th>Expiry</th>
                        <th>Unit Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                        <th>CGST</th>
                        <th>SGST</th>
                        <th>Sub Total</th>
                      </tr>
                    </thead>
                  <tbody>
                  <?php
                  $slno=1;
                  foreach($sales as $sale)
                  { ?> 
                    <tr>
                      <td align="center"><?= $slno++  ?></td>
                      <td><a href="<?= $this->config->item('admin_url')."product/view/".$sale['ved_itemid'] ?>"><?=$sale['ved_item'] ?></a></td>
                      <td><?=$sale['ved_batch'] ?></td>
                      <td><?=$sale['ved_expiry'] ?></td>
                      <td><?="Rs. ".$sale['ved_price'] ?></td>
                      <td><?php 
                      if($sale['ved_unit'] != "No.s") 
                        { echo $sale['ved_qty']/$sale['ved_uqty']; } 
                      else
                        { echo $sale['ved_qty']; } 
                        echo " ".$sale['ved_unit'] ?></td>
                      <td><?="Rs. ".$sale['ved_total'] ?></td>
                      <td><?="Rs. ".$sale['ved_cgsta']." (".$sale['ved_cgstp']." %)" ?></td>
                      <td><?="Rs. ".$sale['ved_sgsta']." (".$sale['ved_sgstp']." %)" ?></td>
                      <td><?="Rs. ".$sale['ved_gtotal'] ?></td>
                    </tr>
                    <?php } ?>

                  </tbody>
                  </table> 
                
                  <table class="table table-bordered">
                    <tr><th width="80%">Total</th>
                      <th>Rs. <label><?= $ve_amount ?></label></th></tr>
                      <tr><th width="80%">SGST Total</th>
                      <th>Rs. <label><?= $ve_sgst ?></label></th></tr>
                      <tr><th width="80%">CGST Total</th>
                      <th>Rs. <label><?= $ve_cgst ?></label></th></tr>
                      <tr><th width="80%">Discount</th>
                    <th>Rs.<label><?= $ve_discounta ?></label>( <?= $ve_discount ?>%)</th></tr>

                    <tr><th width="80%">Grand Total</th>
                    <th>Rs. <label><?= $ve_gtotal ?></label></th></tr>
                    <tr><th width="80%">Amount Payable</th>
                      <th><input type="hidden" name="grandtotal" readonly id="grand_total" value="<?= $ve_apayable ?>">Rs. <label id="grandtotal1"><?= $ve_apayable ?></label></th></tr>
                      <tr><th width="80%">Round off Value</th>
                      <th>Rs. <label><?= $ve_round ?></label></th></tr>
                    </table>
   
<button class="btn btn-success" type="submit"><i class="fa fa-floppy-o"></i> Pay</button>


          </div>
        </div>
      </div>
    </div>
  </div>
</div>
           

        </div>
      </div>
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            
          </div>

        </div>
      </div>
     
   <?php echo form_close(); ?>
   
</section>

</div><!-- /.right-side -->

<script>

function save(){

      var postData = $("#post_form").serializeArray();
      var amount_paid         =  $('#amount_paid').val();
      if(amount_paid=="")
          {
            alert("Enter the amount paid");
          }
          else
          {
      var Printid         =  $('#Printid').val();
      var formURL  = "<?= base_url() ?>sales/saleUpdate/?sUpid="+Printid;
      // alert(formURL);
      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
           window.location = "<?php echo base_url();?>index.php/sales/sale_View/"+Printid;

        }

      });
    }
}
function print(){

          var Printid         =  $('#Printid').val();
          window.location = "<?php echo base_url();?>index.php/sales/getPrint?sPrintid="+Printid;

}
</script>


