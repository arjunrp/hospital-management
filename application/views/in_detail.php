<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<?php 
$this->load->view('product_script');
?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 $('.tbhide').hide();
  $('.tball').hide();
  $('.tbpur').hide();
  $('.tbret').hide();
  $('.tbcon').hide();
  $('.tbsal').hide();
var r_type= "<?= $r_type ?>";
if(r_type==1)
{
  $('.tball').hide();
  $('.tbret').hide();
  $('.tbcon').hide();
  $('.tbsal').hide();
  $('.tbhide').show();
  $('.tbpur').show();

}
else if(r_type==2)
{
  $('.tball').hide();
  $('.tbret').hide();
  $('.tbcon').hide();
  $('.tbpur').hide();
  $('.tbhide').show();
  $('.tbsal').show();
}
else if(r_type==3)
{
  $('.tball').hide();
  $('.tbcon').hide();
  $('.tbpur').hide();
  $('.tbsal').hide();
  $('.tbhide').show();
  $('.tbret').show();
}
else if(r_type==0)
{
  $('.tbcon').show();
  $('.tbpur').show();
  $('.tbsal').show();
  $('.tbret').show();
  $('.tbhide').show();
  $('.tball').show();
}
else if(r_type==5)
{
  $('.tball').hide();
  $('.tbpur').hide();
  $('.tbsal').hide();
  $('.tbhide').show();
  $('.tbret').hide();
  $('.tbcon').show();
}
  oTable = $('#patient_table').dataTable({
    "aaSorting": [[ 0, "dec" ]],
    "bJQueryUI": true,
    "sPaginationType": "full_numbers",
    "iDisplayLength": 10,
    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
  });
});
</script>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     <?php echo $page_title; ?>
    </h1>
     <ol class="breadcrumb">
      <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
      
      <li class="active"><?php echo $page_title; ?></li>
    </ol>&nbsp;&nbsp;
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
              <?php echo form_open_multipart($action) ?>
                 <div class="form-group">
                   <div class="col-md-2">
                    <label>From Date</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker" name="fdate">
                </div>
              </div>
                   <div class="col-md-2">
                    <label>To Date</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker2" name="tdate">
                </div>
                <!-- /.input group -->
              </div>

                   <div class="col-md-2" id="d_brand">
                    <label>Brand</label>
                    <select class="form-control" name="p_brand" id="p_brand">
                      <option value="0">Select a Brand</option>
                      <?php foreach ($brands as $brand) {
                       ?> <option value="<?=$brand['id'] ?>"><?=$brand['brand'] ?></option> <?php
                      } ?>
                  </select>      
                </div>
                <div class="col-md-2" id="d_category">
                  <label>Category</label>
                    <select class="form-control" name="p_category" id="p_category">
                      <option value="0">Select a Category</option>
                  </select>      
                </div>
                <div class="col-md-2" id="d_scategory">
                   <label>Sub-Category</label>
                    <select class="form-control" name="p_scategory" id="p_scategory">
                      <option value="0">Select a Sub-Category</option>
                  </select>      
                </div>

                <div class="col-md-2" id="d_group">
                  <label>Group</label>
                    <select class="form-control" name="p_group" id="p_group">
                      <option value="0">Select a Group</option>
                      <?php foreach ($groups as $group) {
                       ?> <option value="<?=$group['id'] ?>"><?=$group['groups'] ?></option> <?php
                      } ?>
                  </select>      
                </div>
                <div class="col-md-2" id="d_type">
                   <label>Type</label>
                    <select class="form-control" name="p_type" id="p_type">
                      <option value="0">Select a Type</option>
                       <?php foreach ($types as $type) {
                       ?> <option value="<?=$type['id'] ?>"><?=$type['type'] ?></option> <?php
                      } ?>
                  </select>      
                </div>
                <div class="col-md-2">
                  <label>Product</label>
                    <select class="form-control" name="product" id="product">
                      <option value=0>Select a Product</option>
                  </select>      
                </div>
                <div class="col-md-2">
                  <label>Report Type</label>
                    <select class="form-control" name="r_type" id="r_type">
                      <option value=0>Select Type</option>
                      <option value=1>Purchase</option>
                      <option value=2>Sale</option>
                      <option value=3>Return</option>
                      <option value=5>Consumption</option>
                  </select>      
                </div>
                <div class="col-md-2">
                  <label>&emsp;</label><br>
                 <input type="submit" class="btn btn-success" value="Apply">    
                </div>
              </div>
            
            
            <div class="col-md-12"><hr></div>

<?php echo form_close(); ?>             
            
            <div class="row">
              <div class="col-md-12">
                <div class="box-header  ">
                  <center><h3 class="box-title">Stocks Report from <b><?=date("d-m-Y",strtotime($fdate)) ?></b>&nbsp;<b></b> to <b><?=date("d-m-Y",strtotime($tdate)) ?></b> of <b><?php if($product==0) { echo "All Products"; } else { echo $products;  } ?></b></h3></center>
                </div>
                <div class="col-md-12"><hr></div>
                <?php echo form_open_multipart($action1) ?>
                  
                  <input type="hidden" name="fdate1" value="<?=$fdate ?>">
                  <input type="hidden" name="tdate1" value="<?=$tdate ?>">
                  <input type="hidden" name="product1" value="<?=$product ?>">
                  <input type="hidden" name="r_type1" value="<?=$r_type ?>">  
                <center>     
                <button type="submit" name="submit" class="btn btn-primary" >Print</button>
              </center>
              <?php echo form_close(); ?>  

            <div class="row">
              
              <div class="col-md-12"><br>
                  <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-12">
                      <table id="" class="table table-condensed dataTable no-footer">
                       <thead><b>

                        <tr class="tbhide">
                          <th>Date</th>
                          <th>Item</th>
                          <th class="tball">Opening Stock</th>
                          <th class="tbpur">Purchase</th>
                          <th class="tbret">Purchase Return</th>
                          <th class="tbsal">Sale</th>
                          <th class="tbret">Sale Return</th>
                          <th class="tbcon">Consumption</th>
                          <th class="tball">Closing Stock</th></tr>
                        </b>
                      </thead>
                      <tbody>
                      <?php if($r_type==1 || $r_type==2 || $r_type==5) { echo $output; } else if($r_type==0) {
                      foreach ($output as $output):  
                        ?>
                      <tr>
                         <td><?= date('d-m-Y', strtotime($output['sale_date']))   ; ?></td>
                         <td><?= $output['sale_item']; ?></td>
                         <td style="text-align:center"><?= $output['openingStock'] ?> </td>
                         <td style="text-align:center"><?php if(empty($output['purchase_qty'])) { echo "0"; } else { echo $output['purchase_qty']; } ?> </td>
                         <td style="text-align:center"><?php if(empty($output['purchase_return'])) { echo "0"; } else { echo $output['purchase_return']; } ?> </td>
                         <td style="text-align:center"><?php if(empty($output['sales_qty'])) { echo "0"; } else { echo $output['sales_qty']; } ?> </td>
                         <td style="text-align:center"><?php if(empty($output['sales_return'])) { echo "0"; } else { echo $output['sales_return']; } ?> </td>
                         <td style="text-align:center"><?php if(empty($output['consume'])) { echo "0"; } else { echo $output['consume']; } ?> </td>
                         <td style="text-align:center"><?= $output['closingstock'] ?> </td>
                       </tr>
                     <?php endforeach; } 
                      else if($r_type==3) {
                      foreach ($output as $output):  
                        ?>
                      <tr>
                         <td><?= date('d-m-Y', strtotime($output['sale_date']))   ; ?></td>
                         <td><?= $output['sale_item']; ?></td>
                         <td style="text-align:center"><?php if(empty($output['purchase_return'])) { echo "0"; } else { echo $output['purchase_return']; } ?> </td>
                         <td style="text-align:center"><?php if(empty($output['sales_return'])) { echo "0"; } else { echo $output['sales_return']; } ?> </td>
                       </tr>
                     <?php endforeach; }
                     ?>
               </tbody>
            </table>
      
        </div>
      </div>
    </div>

  </div>
</div>

</div>
</div>


</div></div></div>
</div></div></div>
</section></div>


<script>
 $('#datepicker').datepicker({
      autoclose: true
    });</script>


    <script>
 $('#datepicker2').datepicker({
      autoclose: true
    });</script>



