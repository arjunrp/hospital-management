<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
   <script src="<?= base_url() ?>asset/plugins/timepicker/bootstrap-timepicker.min.js"></script>
     <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/timepicker/bootstrap-timepicker.min.css">
  <script type="text/javascript" charset="utf-8">
  $(document).ready(function() {

  var checkbox = $('#ip_dis');
  var ip_dis   = $('#ip_dis').val();
  if(ip_dis=="0")
  { checkbox.attr('checked','checked'); }
  else
  { checkbox.removeAttr('checked'); }

  $('input[type="checkbox"]'). click(function(){
  if($(this). prop("checked") == true){
  $("#ip_dis1").val(0);
  }
  else if($(this). prop("checked") == false){
  $("#ip_dis1").val(1);
  } 

  });
  $('#p_phone').change(function(){
      var uRL1   = "<?= base_url() ?>ipregister/get_phone/"+$(this).val();
      $.ajax({
        url : uRL1,
        dataType: "json",
        success:function(data3)
        {
           $.each(data3, function(index) {
        $("#bk_name").val(data3[index].p_title+" "+data3[index].p_name);
        $("#ip_mrd").val(data3[index].p_mrd_no);
        $("#p_guardian").val(data3[index].p_guardian);
        $("#p_address").val(data3[index].p_address);
        $("#p_street").val(data3[index].p_street);
        $("#p_age").val(data3[index].p_age);
          });
        }

      });

  });

  $('#ip_mrd').change(function(){
      var uRL1   = "<?= base_url() ?>ipregister/get_mrd/"+$(this).val();
      $.ajax({
        url : uRL1,
        dataType: "json",
        success:function(data3)
        {
           $.each(data3, function(index) {
        $("#bk_name").val(data3[index].p_title+" "+data3[index].p_name);
        $("#p_phone").val(data3[index].p_phone);
        $("#p_guardian").val(data3[index].p_guardian);
        $("#p_address").val(data3[index].p_address);
        $("#p_street").val(data3[index].p_street);
        $("#p_age").val(data3[index].p_age);
      });  
         }
       });
    });

    $('#ip_doctor').change(function(){
      var ip_doc_name = $('#ip_doctor option:selected').text();
      $('#ip_doc_name').val(ip_doc_name);
      var uRL1   = "<?= base_url() ?>ipregister/getDepartment/"+$(this).val();
      $.ajax({
        url : uRL1,
        dataType: "json",
        success:function(data3)
        {
           $.each(data3, function(index) {
        $("#ip_department_id").val(data3[index].dp_id);
        $("#ip_department").val(data3[index].dp_department);
      });  
         }
       });
    });

});
  </script>
 <script> 
$(document).ready(function() {

  var ip_doc_name = $('#ip_doctor option:selected').text();
  $('#ip_doc_name').val(ip_doc_name);

  $('input[type="checkbox"]'). click(function(){
  if($(this). prop("checked") == true){
  $("#ip_dis").val(0);
  }
  else if($(this). prop("checked") == false){
  $("#ip_dis").val(1);
  } 
  });

   $('.mrd').change(function(){
    var ip_mrd = $('#ip_mrd').val();
    var uRL1   = "<?= base_url() ?>ipregister/get_discharge";
    $.ajax({
        url : uRL1,
        type: "POST",
        data : {ip_mrd:ip_mrd},
        success:function(data3, textStatus, jqXHR)
        {
          if(data3=="0000-00-00")
          {
            alert("Patient not discharged yet");
            $('#ip_mrd').val("");
            $('#bk_name').val("");
            $("#p_phone").val("");
            $("#p_guardian").val("");
            $("#p_address").val("");
            $("#p_street").val("");
            $("#p_age").val("");
            $("#ip_department_id").val("");
            $("#ip_department").val("");
          }
        }
  })

   });

  });
  </script>


  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#customer_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       IP
       <small>Edit IP </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."ipregister"; ?>">IP</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/ipregister/add">
        <i class="fa fa-plus-circle"></i> Create New
      </a>
    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>

      <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row"> 
       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">IP No.<sup></sup></label>
          <input class="form-control input-sm"  type="text" id="ip_ipno" name="ip_ipno" value=<?=$ip_ipno ?>> 
          <input class="form-control" type="hidden" name="user_type" tabindex="1" value="<?=$user_type?>"> 
          <input class="form-control" type="hidden" name="ip_id" value="<?=$ip_id?>"> 
        </div>
        <div class="col-md-2">
          <label  class="control-label">Date<sup></sup></label>
          <input class="form-control validate[required] input-sm datepicker"  tabindex="2" data-prompt-position="topRight:150" type="text" name="ip_date" value="<?=$ip_date ?>">                
        </div>
        <div class="bootstrap-timepicker">
                <div class="form-group col-md-2">
                  <label>Time:</label>
                  <div class="input-group">
                    <input type="text" name="ip_time" value="<?=date('h:i A',strtotime($ip_time)) ?>" class="form-control timepicker input-sm">
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              </div>
          <div class="col-md-2">
         <label  class="control-label">MRD<sup></sup></label>
         <input class="mrd form-control validate[required] input-sm" data-prompt-position="bottomLeft:190" tabindex="5" type="text" name="ip_mrd" id="ip_mrd" value="<?=$ip_mrd?>"></div>
       <div class="col-md-2">
         <label  class="control-label">Patient's Name<sup></sup></label>
         <input class="mrd form-control input-sm"  data-prompt-position="bottomLeft:190"  tabindex="6" type="text" id="bk_name" value="<?=$p_name?>" name="ip_name">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Phone No.<sup></sup></label>
         <input class="mrd form-control text-input input-sm" data-prompt-position="bottomLeft:190" tabindex="7" type="text" id="p_phone" name="p_phone" value="<?=$p_phone?>">
       </div>

      </div>
      <div class="col-md-12">
       <div class="col-md-2">
         <label  class="control-label">Address<sup></sup></label>
         <input class="form-control input-sm" type="text" id="p_address" name="p_address" value="<?=$p_address?>">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Street<sup></sup></label>
         <input class="form-control input-sm" type="text" id="p_street" name="p_street" value="<?=$p_street?>">
       </div>
       <div class="col-md-1">
         <label  class="control-label">Age<sup></sup></label>
         <input class="form-control input-sm" type="text" name="p_age" id="p_age" value="<?=$p_age?>">
       </div><div class="col-md-1">
         <label class="control-label">Sex<sup></sup></label>
         <select class="form-control input-sm" name="p_sex" id="p_sex">
          <option value="Male" <?php if($p_sex=="Male") { ?> selected <?php } ?>>Male</option>
          <option value="Female" <?php if($p_sex=="Female") { ?> selected <?php } ?>>Female</option>
          <option value="Others" <?php if($p_sex=="Others") { ?> selected <?php } ?>>Others</option>
         </select>
       </div>
       <div class="col-md-2">
         <label  class="control-label">Guardian<sup></sup></label>
         <input class="form-control input-sm" type="text" name="p_guardian" id="p_guardian" value="<?=$p_guardian?>">
       </div>
        <div class="col-md-2">
         <label  class="control-label">Doctor<sup></sup></label>
         <select class="form-control input-sm" name="ip_doctor" id="ip_doctor">
          <option value="">--Select Doctor--</option>
          <?php foreach ($doctors as $key => $doctor) {
            ?> <option value="<?=$doctor['u_emp_id'] ?>" <?php if($ip_doctor==$doctor['u_emp_id']) { ?> selected <?php } ?> ><?=$doctor['u_name'] ?></option> <?php 
          } ?>
         </select> 
         <input type="hidden" name="ip_doc_name" id="ip_doc_name">
       </div>
         <div class="col-md-2">
         <label  class="control-label">Department<sup></sup></label>
         <input class="form-control input-sm" readonly data-prompt-position="bottomLeft:190" tabindex="7" type="text" id="ip_department" value="<?=$department ?>" name="ip_dept_name">
         <input type="hidden" name="ip_department" id="ip_department_id" value="<?=$ip_department ?>"> 
       </div>

       </div> 
       <div class="col-md-12">
        <br>
       <div class="col-md-2">
         <label  class="control-label">Discharge Date<sup></sup></label>
         <input class="form-control input-sm datepicker" data-prompt-position="bottomLeft:190" tabindex="8" type="text" name="ip_discharge" value="<?php if($ip_discharge!="0000-00-00") { echo $ip_discharge; } ?>">
       </div>
       <div class="bootstrap-timepicker">
                <div class="form-group col-md-2">
                  <label>Discharge Time:</label>
                  <div class="input-group">
                    <input type="text" name="ip_dtime" class="form-control timepicker input-sm" value="<?=date('h:i A',strtotime($ip_dtime)) ?>" >
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              </div>

              <div class="col-md-2">
         <label  class="control-label">Discharged<sup></sup></label><br>
         <input tabindex="9" type="checkbox" id="ip_dis">
         <input tabindex="9" type="hidden" name="ip_dis1" id="ip_dis1" value="<?=$ip_dis?>">
       </div>
          </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-md-12">
                <div>
                  <button class="button btn btn-primary" type="submit">Update</button>
                  <input class="btn-large btn-default btn" type="reset" value="Reset">
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php echo form_close(); ?>


</div>
</div>
</section>

</section><!-- /.right-side -->


<script>
 $('.datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });
 $('.timepicker').timepicker({
      showInputs: false,
      minuteStep: 1,
    });
 </script>

    <script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>
