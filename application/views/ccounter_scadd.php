<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script> 
$(document).ready(function() {

 <?php
$js_array = json_encode($scan_bills);
echo "var scan_bill = ". $js_array . ";\n";
?>

$.each(scan_bill, function(index) {
        // alert(sum+","+vesgst+","+vecgst+","+apayable);

         var newrow      = '<tr><td><input type="hidden" value="'+scan_bill[index].ved_itemid+'" name="ved_itemid[]"><input class="form-control input-sm" type="hidden" value="'+scan_bill[index].ved_item+'" name="ved_item[]">' + scan_bill[index].ved_item + '</td><td><input class="labmrp" type="hidden" value="'+scan_bill[index].ved_total+'" name="ved_total[]"><b>Rs. </b><label>'+scan_bill[index].ved_total+'</label></td></tr>';
        $('#item_table tr:last').after(newrow);
      });
  });
  </script>



  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#customer_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>

 <?php
foreach ($scan_bills as $key => $scan_bill) {

    $ve_id            = $scan_bill['ve_id'];
    $ve_vno           = $scan_bill['ve_vno'];
    $ve_date          = $scan_bill['ve_date'];
    $ve_customer      = $scan_bill['ve_customer'];
    $ve_mrd           = $scan_bill['ve_mrd'];
    $p_name           = $scan_bill['ve_patient'];
    $p_phone          = $scan_bill['ve_phone'];
    $ve_apayable      = $scan_bill['ve_apayable'];
    $ve_apaid         = $scan_bill['ve_apaid'];
    $ve_user          = $scan_bill['ve_user'];
    $ve_type          = $scan_bill['ve_type']; 
    $doctor           = $scan_bill['ve_doctor']; 
  }

  ?>
  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       Cash Counter
       <small>Scanning </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."ccounter"; ?>">Cash Counter</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div class="box-tools">
      
    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>

      <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row"> 
       <div class="col-md-12">

        <div class="col-md-4">
          <label  class="control-label">Bill No. :<sup></sup></label><?=$ve_vno?>

          <input class="form-control" type="hidden" name="ve_date" value="<?=$ve_date?>">
          <input class="form-control" type="hidden" id="ve_id" name="ve_id" value="<?=$ve_id?>">            
        </div>
        <div class="col-md-4">
          <label  class="control-label">Date : <sup></sup></label> <?=date("d-m-Y",strtotime($ve_date)) ?>                
        </div>

       <div class="col-md-4">
         <label  class="control-label">Doctor  : <sup></sup></label> <?=$doctor?>
       </div>
        <div class="col-md-12"><br></div>
        <?php if($ve_type=="lbi") { ?>
         <div class="col-md-3">
         <label  class="control-label">IP No. : <sup></sup></label> <?=$ve_customer?>
       </div>
       <?php } ?>

          <div class="col-md-3 ipno1">
         <label  class="control-label">MRD : <sup></sup></label> <?=$ve_mrd?>
       </div>
       <div class="col-md-4">
         <label  class="control-label">Patient's : <sup></sup></label> <?=$p_name?>
       </div>
       <div class="col-md-2">
         <label  class="control-label">Phone No. : <sup></sup></label> <?=$p_phone?>
       </div>
      </div>
      <div class="col-md-12">
      <br>


     </div>
          </div>



        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Particular</th>
                        <th>MRP</th>
                        </tr>
                    </thead>
                  <tbody> 

                  </tbody>
                  </table> 
                  <table class="table table-bordered">
                    <tr><th width="40%"></th><th width="20%">Total</th>
                      <th><input type="hidden" name="grandtotal" value="<?=$ve_apayable ?>">Rs. <label><?=$ve_apayable ?></label></th></tr>
                    </table>

                    <button class="btn btn-success" type="submit"><i class="fa fa-floppy-o"></i> Pay</button>
                
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php echo form_close(); ?>
</div>
</section>

</section><!-- /.right-side -->

<script>
function save(){

      var postData = $("#post_form").serializeArray();
      var ve_id    = $('#ve_id').val();
      var formURL  = "<?= base_url() ?>investigation/investigationUpdate/"+ve_id;

      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          alert("Success");
          $('#Printid').val(data);
           window.location = "<?php echo base_url();?>index.php/investigation";
        }
      });
}
 function save_print(){

       var postData = $("#post_form").serializeArray();
      var ve_id    = $('#ve_id').val();
      var formURL  = "<?= base_url() ?>investigation/investigationUpdate/"+ve_id;

      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {

           window.location = "<?php echo base_url();?>index.php/investigation/getBillPrint?Printid="+data;
        }
      });
}
</script>

<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });</script>
<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>