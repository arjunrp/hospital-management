<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
<script>
function printform(){
        window.print();
        window.close();
      }
</script>
 <body onload="printform();" >


  <?php foreach($company as $company) {
    $company_name     = $company['company'];
    $company_lic      = $company['licence_no'];
    $company_address  = $company['address'];
    $company_street   = $company['street'];
    $company_city     = $company['city'];
    $company_state    = $company['state'];
    $company_country  = $company['country'];
    $company_zip      = $company['zip'];
    $company_phone    = $company['phone'];
    $company_email    = $company['email'];
    $company_fax      = $company['fax'];
    $company_website  = $company['website'];

   }?>




<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <center><?= $company_name ?></center> 
        <center> <small class="pull-center"><?= $company_lic ?></small> </center> 
          <center> <small class="pull-center"><?= $company_address.", ".$company_street  ?></small> </center> 
            <center> <small class="pull-center"><?= $company_phone.", ".$company_email.",".$company_website ?></small> </center>  
        </h2>
      </div>
      <!-- /.col -->
    </div>
 <center> <small class="pull-center"><b>CASH BILL</b></small> </center>  
<div class="dontprint">
     <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/purchase">
        <i class="fa fa-plus-circle"></i>Create New
      </a>

    </div> </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        <address>
           <b>Bill no &emsp;&emsp;:S </b><br>
          <b>Dr &emsp;&emsp;&emsp;:S </b><br>
          <b>Patient id &emsp;&emsp;&emsp; :S </b>  <br> 
           <b>Mob &emsp;&emsp;&emsp; :S </b>  <br> 
        </address>
        </div>
      <!-- /.col -->
      <div class="col-sm-3 invoice-col">
        <address>
          <strong> </strong><br>
          
           
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        <b>Date &emsp;&emsp;  :  S </b><br>
        <b>Delivery Date &emsp;&emsp;:  S </b><br>
        <b>Patient name &emsp;&emsp;:  S   </b><br>
         
        
         
        
        
        
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
          <tr>
            <th>Code</th>
            <th> </th>
            <th>Amount Rs</th>
             
          </tr>
          </thead>
          <tbody>
            
          <tr>
            <td>C</td>
            <td>C </td>
            <td>C </td>
             
          </tr>
          

          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <!-- accepted payments column -->
       <div class="col-xs-8">
       </div>
      <!-- /.col -->
      <div class="col-xs-12">

        <div class="table-responsive" >
          <table class="table">
            <tr >
              <th align="right"  style="width:50%">
               Grand  Total      :</th>
              <th>Rs.  </th>
            </tr>
            <tr>
              <th style="width:50%">
               </th>
              <th> </th>
            </tr>
          </table>
        </div>
         
        <div class="col-xs-8">
        Signature <br><br><br><br><br><br><br>
      </div>
      <!-- /.col -->
    </div>

     </div>
       
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
 
 <style type="text/css" media="print">
.dontprint
{ display: none; }
</style>
