<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
   oTable = $('#category_table').dataTable({
    "aaSorting": [[ 0, "asc" ]],
    "bJQueryUI": true,
    "sPaginationType": "full_numbers",
    "iDisplayLength": 10,
    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
});
});
</script>
<section class="right-side" style="min-height:700px;">
    <section class="content-header">
        <h1>
           <?php echo $page_head; ?>
       </h1>
       <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
       <li class="active"><?php echo $page_head; ?></li>
    </ol>&nbsp;&nbsp;

</section>

<section class="content">    <!-- Success-Messages -->
    <div class="box box-info">
        <div class="box-header">
           
        <h3 class="box-title"><i class="fa fa-th"></i> <?php echo $page_title; ?>
        </h3>
        <div class="box-tools">
            <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo $this->config->item('admin_url')."ipregister/add"; ?>" accesskey="n" title="short key-ALT+N">
                <i class="fa fa-plus-circle"></i> Create New
            </a>

        </div>
    </div><!-- /.box-header -->

    <div class="box-body">
         <div id="example_wrapper" class="table-responsive">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-12">
                        <table id="category_table" class="table table-condensed dataTable no-footer">
                            <thead>
                                <tr>
                                    <th>IP No.</th>
                                    <th>Date.</th>
                                    <th>Doctor</th>
                                    <th>Department</th>
                                    <th>MRD</th>
                                    <th>Patient's Name</th>
                                    <th>Phone No.</th>
                                    <th>Room No.</th>
                                    <th>Room Shift</th>
                                    <th>Options</th>
                                </tr>
                            </thead>
                        <tbody> 
                    <?php if($output) { echo $output; } ?>
                    </tbody>
                </table>
            </div> </div>
        </div></div>
    </div> 
</div>
</section>
</section> 
