
<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
 <body onload="window.print();" >
  <?php foreach($company as $company) {
    $company_name     = $company['company'];
    $company_lic      = $company['licence_no'];
    $company_address  = $company['address'];
    $company_street   = $company['street'];
    $company_city     = $company['city'];
    $company_state    = $company['state'];
    $company_country  = $company['country'];
    $company_zip      = $company['zip'];
    $company_phone    = $company['phone'];
    $company_email    = $company['email'];
    $company_fax      = $company['fax'];
    $company_website  = $company['website'];

   }?>

     <?php foreach($sales as $sale) {
    $customer_name     = $sale['customer'];
    $customer_address  = $sale['address'];
    $customer_street   = $sale['street'];
    $customer_city     = $sale['city'];
    $customer_state    = $sale['state'];
    
    $customer_country  = $sale['country'];
    $customer_zip      = $sale['zip'];
    $customer_phone    = $sale['phone'];
    $customer_email    = $sale['email'];
    $customer_fax      = $sale['fax'];
    $customer_website  = $sale['website'];
    $invoice_date      = date("d-m-Y",strtotime($sale['sale_date']));
    $invoice_no        = $sale['bill_number'];
    $amount            = $sale['sale_amount'];
    $discount          = $sale['salediscount'];
    $gst_total         = $sale['gst_total'];
    $total_wgst        = $sale['total_wgst'];
    $amount_payable    = $sale['amount_payable'];
    $round_off         = $sale['round_off'];
    $amount_paid       = $sale['amount_paid'];
    $balance           = $sale['amount_balance'];
     } ?>

<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i> <?= $company_name ?><br>
          <small class="pull-left">  <b> ADDRESS:</b>  CVXVXVXVXX </small> 
           <small class="pull-right"> <b>  GST:</b>  CVXVXVXVXX </small><br>
            <small class="pull-right"><b>Lic No:</b> SADCAS</small><br>

        </h2>
      </div>
      <!-- /.col -->
    </div>


    <div class="dontprint"> <div class="box-tools">

      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/sales">
       
        <i class="fa fa-plus-circle"></i> Create New
      </a>

    </div></div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        <address>
           
            <strong>BILL No: SDSFD</strong><br>
          <strong>DATE: SADCAS &emsp; TIME:03.00.AM</strong><br>

          
            
        </address>
        </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        <address>
          <b>PATIENT:</b>  SDCVSD <br>
          <b>DOCTOR:</b>  SDCSD<br>
        </address>
      </div>
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
          <tr>
            <th>Sl.No</th>
            <th>Particulars</th>
            <th>Batch</th>
            <th>Rack</th>
            <th>Mfr</th>
            <th> Exp</th>
            <th> Qty</th>
            <th> Rate</th>
            <th>Disc</th>
            <th>CGST</th>
             <th>SGST</th>
          </tr>
          </thead>
          <tbody>
            
          <tr>
            <td>g </td>
            <td> g</td>
            <td>g </td>
            <td>g </td>
            <td> g</td>
            <td> g</td>
            <td>g </td>
            <td>g </td>
            <td>g </td>
             <td>g </td>
              <td> cv</td>
          </tr>
          

          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

<div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        <address>
           
            <strong> Gross Amt : S</strong><br>
          <strong> No Of Items : S</strong><br>
          
        </address>
        </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        <address>
          <b> CGST :</b>   S<br>
          <b> SGST :</b> S <br>
          
          
        </address>
      </div>
  <div class="col-sm-4 invoice-col">
        <address>
          <b> Total Disc :</b>  XAS <br>
          <b> NET AMT :</b>  SACA <br>
          
          
        </address>
      </div>
      <!-- /.col -->
       
      <!-- /.col -->
    </div>



 

      <center>  WISH YOU SPEEDY RECOVERY</center>
       <left><b>Pharmacist:</b>...............</left>


      <style type="text/css" media="print">
.dontprint
{ display: none; }
</style>


    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
 
