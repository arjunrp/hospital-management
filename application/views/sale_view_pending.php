<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
<?php
foreach($sales as $sale)
{
  $ve_id          = $sale['ps_id'];
  $ve_date        = $sale['ps_date'];
  $ve_mrd         = $sale['ps_mrd'];
  $p_name         = $sale['ps_patient'];
  $p_phone        = $sale['ps_phone'];
  $ve_amount      = $sale['ps_amount'];
  $ve_discount    = $sale['ps_discount'];
  $ve_discounta   = $sale['ps_discounta'];
  $ve_sgst        = $sale['ps_sgst'];
  $ve_cgst        = $sale['ps_cgst'];
  $ve_gtotal      = $sale['ps_gtotal'];
  $ve_apayable    = $sale['ps_apayable'];
  $user_name      = $sale['ps_user'];
}

?>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>

<div class="content-wrapper">
  <section class="content-header">
    <h1>
     Sale
      <small>Pending Sales</small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."sales/"; ?>">Sales</a></li>
      <li class="active">Pending Sales</li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> View Pending Sales
       </h3>
        <div class="box-tools">
              <a title="Back" class="btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."sales/pendinglist"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>
     </div>


      <form id="post_form">
     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">
          <div class="form-group col-md-8">
            <label  class="control-label">Bill Date : </label> <?= date("d-m-Y h:i A",strtotime($ve_date)) ?><br>
             <input type ="hidden" name="sale_date" value="<?=$ve_date?>">
          </div>



          <div class="form-group col-md-2">    
          <label class="control-label">User Name : </label> <?= $user_name ?>
          </div>


          <div class="form-group col-md-2">
            <label  class="control-label">MRD : </label> <?= $ve_mrd ?><br>
                       
          </div>

          <div class="form-group col-md-3">
            <label  class="control-label">Patient : </label> <?= $p_name ?><br>
          </div>

          <div class="form-group col-md-2">
            <label  class="control-label">Phone No. : </label> <?= $p_phone ?> <br>
                         
          </div>
            </div>
          </div>

          <div class="row">
          <div class="col-md-12">
             
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                      <tr>
                      </tr>
                  </table>

               
                     <table class="table table-condensed dataTable no-footer gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Sl. No </th>
                        <th>Product</th>
                        <th>Batch</th>
                        <th>Expiry</th>
                        <th>Unit Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                        <th>CGST</th>
                        <th>SGST</th>
                        <th>Sub Total</th>
                      </tr>
                    </thead>
                  <tbody>
                  <?php
                  $slno=1;
                  foreach($sales as $sale)
                  { ?> 
                    <tr>
                      <td align="center"><?= $slno++  ?></td>
                      <td><a href="<?= $this->config->item('admin_url')."product/view/".$sale['psd_itemid'] ?>"><?=$sale['psd_item'] ?></a></td>
                      <td><?=$sale['psd_batch'] ?></td>
                      <td><?=$sale['psd_expiry'] ?></td>
                      <td><?="Rs. ".$sale['psd_price'] ?></td>
                      <td><?php 
                      if($sale['psd_unit'] != "No.s") 
                        { echo $sale['psd_qty']/$sale['psd_uqty']; } 
                      else
                        { echo $sale['psd_qty']; } 
                        echo " ".$sale['psd_unit'] ?></td>
                      <td><?="Rs. ".$sale['psd_total'] ?></td>
                      <td><?="Rs. ".$sale['psd_sgsta']." (".$sale['psd_sgstp']." %)" ?></td>
                      <td><?="Rs. ".$sale['psd_cgsta']." (".$sale['psd_cgstp']." %)" ?></td>
                      <td><?="Rs. ".$sale['psd_gtotal'] ?></td>
                    </tr>
                    <?php } ?>

                  </tbody>
                  </table> 
                
                  <table class="table table-bordered">
                    <tr><th width="80%">Total</th>
                      <th>Rs. <label><?= $ve_amount ?></label></th></tr>
                      <tr><th width="80%">SGST Total</th>
                      <th>Rs. <label><?= $ve_sgst ?></label></th></tr>
                      <tr><th width="80%">CGST Total</th>
                      <th>Rs. <label><?= $ve_cgst ?></label></th></tr>
                      <tr><th width="80%">Discount</th>
                    <th>Rs.<label><?= $ve_discounta ?></label>( <?= $ve_discount ?>%)</th></tr>

                    <tr><th width="80%">Grand Total</th>
                    <th>Rs. <label><?= $ve_gtotal ?></label></th></tr>
                    <tr><th width="80%">Amount Payable</th>
                      <th><input type="hidden" name="grandtotal" readonly id="grand_total" value="<?= $ve_apayable ?>">Rs. <label id="grandtotal1"><?= $ve_apayable ?></label></th></tr>
                    <input type="hidden" class="form-control" id="Printid" value="<?= $ve_id ?>"></th></tr>

                    </table>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
           

        </div>
      </div>
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            
          </div>

        </div>
      </div>
     
    </form>
   
</section>

</div><!-- /.right-side -->



