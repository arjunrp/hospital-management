<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script type="text/javascript" charset="utf-8">
 $(function()
  {
    $("#ip_mrd").autocomplete({
      source: [<?php
      $i=0;
      foreach ($patient as $patientmrd){
        if ($i>0) {echo ",";}
        echo '{value:"' . $patientmrd['p_mrd_no'] . '",patient:"' . $patientmrd['p_name'] . '",patientph:"' . $patientmrd['p_phone'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          // $("#customerid").val(ui.item ? ui.item.vid : '');
          $("#bk_name").val(ui.item ? ui.item.patient : '');
          $("#bk_phone").val(ui.item ? ui.item.patientph : '');
        }
      });   
  });   

  $(function()
  {
    $("#bk_phone").autocomplete({
      source: [<?php
      $i=0;
      foreach ($patient as $patientmrd){
        if ($i>0) {echo ",";}
        echo '{value:"' . $patientmrd['p_phone'] . '",patient:"' . $patientmrd['p_name'] . '",patientmrd:"' . $patientmrd['p_mrd_no'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          // $("#customerid").val(ui.item ? ui.item.vid : '');
          $("#bk_name").val(ui.item ? ui.item.patient : '');
          $("#ip_mrd").val(ui.item ? ui.item.patientmrd : '');
        }
      });   
  });  

  </script>
  <script> 
$(document).ready(function() {

  $('#ip_department').change(function(){
    var ip_department = $('#ip_department').val();
     $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>ipregister/getDoctors/"+ip_department,
     success: function(data){
      $('#ip_doctor').empty();
      $("#ip_doctor").append('<option value="0">--Select--</option>');
      $.each(data, function(index) {
        $("#ip_doctor").append('<option value=' + data[index].u_emp_id +'>'+data[index].u_name+'</option>');
      });
    }
  })
  });

   $('#ip_department').change(function(){
    var ip_department = $('#ip_department').val();
    var uRL1   = "<?= base_url() ?>ipregister/get_ipno";
    $.ajax({
        url : uRL1,
        type: "POST",
        data : {ip_dept:ip_department},
        success:function(data2, textStatus, jqXHR)
        {
          $('#ip_ipno').val(data2);
        }
  })
  });

  });
  </script>


  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#customer_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       IP
       <small>Edit IP </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."ipregister"; ?>">IP</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/ipregister/add">
        <i class="fa fa-plus-circle"></i> Create New
      </a>
    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>

      <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row"> 
       <div class="col-md-12">
          <div class="form-group col-md-2">
          <label for="focusinput" class="control-label">Image</label><br>
         <?php if($image != "") { echo "<img width='150px' height='150px' src='".$this->config->item("base_url")."uploads/patient/".$image."'>"; } else { echo "<img width='150px' height='150px' src='".$this->config->item("base_url")."uploads/Default.png"."'>"; } ?>
          </div>
        <div class="col-md-2">
          <label  class="control-label">IP No.<sup></sup></label>
          <input class="form-control validate[required]" readonly type="text" id="ip_ipno" name="ip_ipno" value="<?=$ip_ipno ?>"> 
          <input class="form-control validate[required]" readonly type="hidden" name="old_ipno" value="<?=$ip_ipno ?>">
          <input class="form-control" type="hidden" name="ip_id" value="<?=$ip_id?>"> 
          <input class="form-control" type="hidden" name="user_type" value="<?=$user_type?>">              
        </div>
        <div class="col-md-2">
          <label  class="control-label">Date<sup></sup></label>
          <input class="form-control validate[required]"  data-prompt-position="topRight:150" type="text" name="ip_date" id="datepicker" value="<?=$ip_date ?>">                
        </div>
         <div class="col-md-2">
         <label  class="control-label">Department<sup></sup></label>
         <select class="form-control validate[required]" id="ip_department" tabindex="1" name="ip_department"> 
          <option value="0">--Select--</option>
           <?php foreach ($departments as $key => $department) {
                ?>
              <option value="<?php echo $department['dp_id']?>"<?php if($department['dp_id']==$ip_department) { ?> selected="selected" <?php } ?> ><?php echo $department['dp_department']?></option>
                <?php
              }?>
          </select>  
       </div>
        <div class="col-md-3">
         <label  class="control-label">Doctor<sup></sup></label>
         <select class="form-control validate[required]" id="ip_doctor" tabindex="3" name="ip_doctor">
          <option value="0">--Select--</option>
          <?php foreach ($doctors as $key => $doctor) {
                ?>
              <option value="<?php echo $doctor['u_emp_id']?>"<?php if($doctor['u_emp_id']==$ip_doctor) { ?> selected="selected" <?php } ?> ><?php echo $doctor['u_name']?></option>
                <?php
              }?>
            </select>  
       </div>
       <div class="col-md-10"><br></div>
       <div class="col-md-2">
         <label  class="control-label">MRD<sup></sup></label>
         <input class="form-control validate[required]" tabindex="5" type="text" name="ip_mrd" id="ip_mrd" value="<?=$ip_mrd ?>"></div>
       <div class="col-md-2">
         <label  class="control-label">Patient's Name<sup></sup></label>
         <input class="form-control validate[required]" type="text" id="bk_name" value="<?=$bk_name ?>">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Phone No.<sup></sup></label>
         <input class="form-control validate[required]" type="text" id="bk_phone" value="<?=$bk_phone ?>">
       </div>
       </div>
          </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-md-12">
                <div>
                  <button class="button btn btn-primary" type="submit">Update</button>
                  <input class="btn-large btn-default btn" type="reset" value="Reset">
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php echo form_close(); ?>


</div>
</section>

</section><!-- /.right-side -->


<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });</script>

    <script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>
