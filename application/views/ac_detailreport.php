<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
<?php 
$this->load->view('product_script');
?>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
  $('.tbother').hide();
  $('.tbbal').hide();

var r_type= "<?= $r_type ?>";
if(r_type==1 || r_type==2 || r_type==3 || r_type==4)
{
  $('.tbbal').hide();
  $('.tbother').show();
}

else if(r_type==0)
{
  $('.tbother').hide();
  $('.tbbal').show();
}
  oTable = $('#patient_table').dataTable({
    "aaSorting": [[ 0, "dec" ]],
    "bJQueryUI": true,
    "sPaginationType": "full_numbers",
    "iDisplayLength": 10,
    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
  });
});
</script>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     <?php echo $page_title; ?>
    </h1>
     <ol class="breadcrumb">
      <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
      
      <li class="active"><?php echo $page_title; ?></li>
    </ol>&nbsp;&nbsp;
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
              <?php echo form_open_multipart($action) ?>
                 <div class="col-md-12">
                   <div class="col-md-2">
                    <label>From Date</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right input-sm" id="datepicker" name="fdate">
                </div>
              </div>
                   <div class="col-md-2">
                    <label>To Date</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right input-sm" id="datepicker2" name="tdate">
                </div>
                <!-- /.input group -->
              </div>

                   
                   <div class="col-md-3">
                    <label>Brand</label>
                    <select class="form-control input-sm" name="p_brand" id="p_brand">
                      <option value="0">Select a Brand</option>
                      <?php foreach ($brands as $brand) {
                       ?> <option value="<?=$brand['br_id'] ?>"><?=$brand['br_brand'] ?></option> <?php
                      } ?>
                  </select>      
                </div>
                <div class="col-md-2">
                   <label>Type</label>
                    <select class="form-control input-sm" name="p_type" id="p_type">
                      <option value="0">---Select a Type--</option>
                      <option value="m">---Medical Item---</option>
                      <option value="g">---General Item---</option>
                      
                  </select>      
                </div>
                <div class="col-md-3">
                  <label>Product</label>
                    <select class="form-control input-sm" name="product" id="product">
                      <option value="0">Select a Product</option>
                  </select>      
                </div>
                </div>

                <div class="col-md-12">
                  <br>
                <!-- <div class="col-md-2">
                    <label>Department</label>
                    <select class="form-control input-sm" name="p_dept" id="p_dept">
                      <option value="0">Select Department</option>
                      <?php foreach ($departments as $department) {
                       ?> <option value="<?=$department['dp_id'] ?>"><?=$department['dp_department'] ?></option> <?php
                      } ?>
                  </select>      
                </div> -->

                <div class="col-md-2">
                  <label>Report Type</label>
                    <select class="form-control input-sm" name="r_type" id="r_type">
                      <option value="">Select Type</option>
                      <option value=1>Purchase</option>
                      <option value=2>Sale</option>
                      <!-- <option value=0>Balance Sheet</option> -->
                      <option value=3>Purchase Return</option>
                      <option value=4>Sales Return</option>
                  </select>      
                </div>
                <div class="col-md-2">
                  <label>&emsp;</label><br>
                 <input type="submit" class="btn btn-success btn-sm" value="Apply">    
                </div>
              </div>

<?php echo form_close(); ?>    

<div class="col-md-12"><hr></div>
            
            <div class="row">
              <div class="col-md-12">
                <div class="box-header tbret tbpur tbsal tbbal">
                   <center><h3 class="box-title"><b><?php if($r_type==1) { echo "Purchase"; } else if($r_type==2) { echo "Sales"; } else if($r_type==0) { echo "Balance Sheet"; } else if($r_type==3) { echo "Purchase Return"; } else if($r_type==4) { echo "Sale Return"; }   ?></b> Report from <b><?=date("d-m-Y",strtotime($fdate)) ?></b>&nbsp;<b></b> to <b><?=date("d-m-Y",strtotime($tdate)) ?></b> of <b><?= $products ?></b></h3></center>
                </div>
                <div class="col-md-12"><hr></div>
                <?php echo form_open_multipart($action1) ?>
                 <input type="hidden" name="fdate1" value="<?=$fdate ?>">
                  <input type="hidden" name="tdate1" value="<?=$tdate ?>">
                  <input type="hidden" name="product1" value="<?=$product ?>">
                  <input type="hidden" name="r_type1" value="<?=$r_type ?>"> 
                <center>     
                <button type="submit" name="submit" class="btn btn-primary tbret tbpur tbsal tbbal" >Print</button>
              </center>
              <?php echo form_close(); ?>  

            <div class="row">
              
              <div class="col-md-12"><br>
                  <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-12 tbother">
                      <table id="patient_table" class="table table-condensed dataTable no-footer">
                       <thead><b>
                        <tr>
                          <th>Date</th>
                          <th>Product</th>
                          <th>Batch</th>
                          <th>Unit Price</th>
                          <th>Qty</th>
                          <th>Amount</th>
                          <th>SGST</th>
                          <th>SGST</th>
                          <th>Sub Total</th>
                        </tr>
                        </b>
                      </thead>
                      <tbody>
                      <?php if($r_type != "0") { echo $output; } ?>
                      <?php
                  $ve_discounta=0;
                  $ve_sgst=0;
                  $ve_cgst=0;
                  $ve_gtotal=0;                
                  $ve_apayable=0;
                  $ve_apaid=0;
                  $ve_balance=0;
                  $ve_round=0;
                  if (is_array($output1) || is_object($output1) )
                  {
                  foreach($output1 as $tsum)
                  {
                   

                    $ve_discounta = $ve_discounta   + $tsum['ve_discounta'];
                    $ve_gtotal    = $ve_gtotal      + $tsum['ve_gtotal'];
                    $ve_apayable  = $ve_apayable    + $tsum['ve_apayable'];
                    $ve_apaid     = $ve_apaid       + $tsum['ve_apaid'];
                    $ve_balance   = $ve_balance     + $tsum['ve_balance'];
                    $ve_round     = $ve_round       + $tsum['ve_round'];
                  } }
              ?>
               <?php if($product=="0" && !empty($output1)){ ?>
                <tr<?php if($r_type==0 || $r_type== "") { ?> style="display:none" <?php } ?>><th width="85%">Overall Discount</th><th><?="Rs. ".$ve_discounta ?></th></tr>
                <tr><th width="85%">Total Amount</th><th><?="Rs. ".$ve_gtotal ?></th></tr>
                <tr><th width="85%">Total Round Off</th><th><?="Rs. ".$ve_round ?></th></tr>
                <tr><th width="85%">Total Amount Payable</th><th><?php echo "Rs. "; echo $ve_apayable; ?></th></tr>
                <tr><th width="85%">Total Amount Paid</th><th><?="Rs. ".$ve_apaid ?></th></tr>
                <tr><th width="85%">Balance Amount</th><th><?="Rs. ".$ve_balance ?></th></tr>
                <?php } ?>
              </tbody>
             </table> 
          </div>

             <?php
                       if($r_type==0) {
                      foreach ($output as $output):  
                        ?>
             <div class="col-md-12">
             <table class="table table-condensed dataTable no-footer tbbal">
              <thead>
              <tr>
                <th colspan=2 style="text-align: center;"><?= date('d-m-Y', strtotime($output['psdate'])) ?></th></tr>
                <tr><td width="50%" style="text-align: center;"><b>Opening Balance : </b> <?= "Rs. ".$output['openingBalance'] ?></td><td style="text-align: center;"><b>Closing Balance : </b> <?= "Rs. ".$output['closingBalance'] ?></td></tr>
            </thead>
              <tr>
                <td>
                  <div class="col-md-12">
                  <table class="table table-condensed dataTable no-footer">
                    <tr><th>Income</th><th>Amount</th></tr>
                    <?php if(!empty($output['sales_sum'])) { ?>
                         <?php foreach ($output['sales_sum'] as $output2) {  ?>
                          <tr>
                         <td><?= $output2['ved_item']." - ".$output2['ved_batch']." - [Sale]" ?> </td>
                         <td><?= "Rs.".$output2['samount'] ?> </td>
                       </tr>
                       <?php } } ?>
                        <?php if(!empty($output['preturn_sum'])) { ?>
                         <?php foreach ($output['preturn_sum'] as $output3) {  ?>
                          <tr>
                         <td><?= $output3['ved_item']." - ".$output3['ved_batch']." - [Purchase Return]" ?> </td>
                         <td><?= "Rs.".$output3['pramount'] ?> </td>
                       </tr>
                       <?php } } ?>
                  </table>
                </div>
              </td>
              <td>
                  <div class="col-md-12">
                  <table class="table table-condensed dataTable no-footer">
                    <tr><th>Expence</th><th>Amount</th></tr>
                    <?php if(!empty($output['purchase_sum'])) { ?>
                         <?php foreach ($output['purchase_sum'] as $output4) {  ?>
                          <tr>
                         <td><?= $output4['ved_item']." - ".$output4['ved_batch']." - [Purchase]" ?> </td>
                         <td><?= "Rs.".$output4['pamount'] ?> </td>
                       </tr>
                       <?php } } ?>
                         <?php if(!empty($output['expense'])) { ?>
                         <?php foreach ($output['expense'] as $output5) {  ?>
                          <tr>
                         <td><?= $output5['category'] ?> </td>
                         <td><?= "Rs.".$output5['amount'] ?> </td>
                       </tr>
                       <?php } } ?>
                       <?php if(!empty($output['sreturn_sum'])) { ?>
                         <?php foreach ($output['sreturn_sum'] as $output6) {  ?>
                          <tr>
                         <td><?= $output6['ved_item']." - ".$output6['ved_batch']." - [Sale Return]" ?> </td>
                         <td><?= "Rs.".$output6['sramount'] ?> </td>
                       </tr>
                       <?php } } ?>
                  </table>
                </div>
                </td>
              </tr>
              <tr><td><br></td></tr>
             </table>
           </div>
           <?php endforeach; } ?>             
      
        </div>
      </div>
    </div>

  </div>
</div>

</div>

</div></div></div>
</div></div></div>
</section></div>


<script>
 $('#datepicker').datepicker({
      autoclose: true
    });</script>


    <script>
 $('#datepicker2').datepicker({
      autoclose: true
    });</script>



