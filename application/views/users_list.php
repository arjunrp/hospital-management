

<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
 
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
     oTable = $('#category_table').dataTable({
        "aaSorting": [[ 2, "asc" ]],
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
    });
});
</script>
  <section class="right-side" style="min-height:700px;">
    <section class="content-header">
        
                    <h1>
                       Users
                        <small>Manage Users</small>
                    </h1>

                    <ol class="breadcrumb">
                        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="">Users</a></li>
                        <li class="active">Manage Users</li>
                    </ol>&nbsp;&nbsp;
   
    </section>

<section class="content">    <!-- Success-Messages -->
        <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-th"></i> Users
</h3>
            <div class="box-tools">
                <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/Users">
    <i class="fa fa-plus-circle"></i> Create New
</a>

            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
                <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12">
            <table id="category_table" class="table table-condensed dataTable no-footer">
    <thead>
    <tr>
         <th>Name</th>
        <th>Email</th>
         <th>Password</th>
         <th>Repeat Password</th>
           <th>Date</th>
         <th width="140">Optionss</th>
    </tr>
     </thead>
    <tbody>
        <?php foreach($login as $loginus){
            ?>
            <tr>
                                 
                <td><?php echo $loginus['user_name'];?></td>
                <td><?php echo $loginus['user_email']?></td>
                 <td><?php echo $loginus['user_password']?></td>
                 <td><?php echo $loginus['user_password']?></td>
                 <td><?php echo date("d-m-Y",strtotime($loginus['user_date']));?></td>

     <td>
        <div class="btn-group  btn-group-xs">
            <a type="button" class="btn btn-info   view-btn-edit" href="<?php echo base_url();?>index.php/users/updateUser/<?php echo $loginus['user_id'];?>" title="Update user"><i class="fa fa-pencil-square-o"></i></a>
            <a type="button" class="btn btn-danger action_confirm   view-btn-delete" data-method="delete" href="<?php echo base_url();?>index.php/users/deleteUser/<?php echo $loginus['user_id'];?>" onclick="return confirm('Are you sure?')" 
 title="Delete user"><i class="fa fa-times-circle-o"></i></a>
        </div>
    </td>
    </tr>
            <?php
        } ?>
        
         
     
    </tbody>
    </table>
</div> </div> 
</div>
        </div><!-- /.box-body -->
    </div>
</section>
            </section><!-- /.right-side -->
