<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {


$('#country').change(function(){
    var ctid = $('#country').val();
    $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>patient/get_state/"+ctid,
     success: function(data){
      $("#state").empty();
      $.each(data, function(index) {
        $("#state").append('<option value=' + data[index].id +'>'+data[index].name+'</option>');
      });
    }
  })
  });

$('#state').change(function(){
    var ctid = $('#state').val();
    $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>patient/get_city/"+ctid,
     success: function(data){
      $("#city").empty();
      $.each(data, function(index) {
        $("#city").append('<option value=' + data[index].id +'>'+data[index].name+'</option>');
      });
    }
  })
  });

// $('#p_phone').change(function(){
//     var ctid = $('#p_phone').val();
//     $.ajax({
//      dataType: "json",
//      url: "<?= base_url() ?>patient/duplication/"+ctid,
//      success: function(data){
//       if(data!=0)
//       {
//         alert("Phone Number Already Registered");
//         $("#p_phone").val("");
//       }
//     }
//   })
//   });


// $('#datepicker').change(function() {
//   var dateString = $('#datepicker').val();
//   var today = new Date();
//   var birthDate = new Date(dateString);

//   var age = today.getFullYear() - birthDate.getFullYear();
//   var m   = today.getMonth() - birthDate.getMonth();
//   if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
//         age--;
//     }
//   $('#p_age').val(age);
// });

 oTable = $('.category_table').dataTable({
});

});
</script>


<div class="content-wrapper">
  <?php
  $status = "1";
    $user_type = $this->session->id;
    // echo $user_type."/".$output;
    if($user_type!="NAH_001" && $output=="1")
    {
      $status = "readonly";
    }

    ?>
  <section class="content-header">
    <h1>
     Patient
      <small>Create  Patient </small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."patient"; ?>">Patients</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">Patient
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php } ?>
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div align="right">
        <?php if($action == $this->base_uri.'/update') { ?>
        <a title="Merge" class="btn btn-sm btn btn-warning" href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-clone"></i> Merge MRD No.</a>
        <?php } ?>
        <a title="Cancelled MRDs" class="btn btn-sm btn btn-danger" href="#" data-toggle="modal" data-target="#myModal1"><i class="fa fa-clone"></i> Cancelled MRDs</a>
         <a title="short key-ALT+B" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."patient"; ?>" accesskey="b" ><i class="fa fa-mail-reply-all"></i> Back</a>
       </div>

     </div>

     <?php echo form_open_multipart($action); ?>
     <div class="box-body">
      <div class="row">
        <div class="col-md-12">


  <?php
                        if($this->uri->segment(1)=="add")
                        $clas   =   'required="required"';
                      else
                      $clas = '';
                    ?> 
           <div class="col-md-3">
          <div class="form-group">
                        <label for="focusinput" class="control-label">Image(1600 X 400)</label>
                        
                        <?php if($image != "") { echo "<img id='blah' width='139px' height='150px' src='".$this->config->item("base_url")."uploads/patient/".$image."'>"; } else { echo "<img id='blah' width='150px' height='150px' src='".$this->config->item("base_url")."uploads/Default2.png"."'>"; } ?>
                        <input type="file" name="image" onchange="readURL(this);" <?php echo $clas; ?>>
                  <input class="form-control"  type="hidden" name="user_type" value="<?= $user_type;?>">
                      </div>
                    </div>
          <div class="form-group col-md-2">
            <label  class="control-label">MRD No<sup>*</sup></label>
            <input class="form-control input-sm" tabindex="1" style="height:29px;" type="text" name="p_mrd_no" value="<?=$p_mrd_no ?>" >
            <input class="form-control input-sm" style="height:29px;" type="hidden" name="p_date" value="<?=$p_date ?>">                
          </div>    
             <div class="form-group col-md-2">
             <label  class="control-label">Title <sup>*</sup></label>
             <select class="form-control input-sm validate[required]" style="height:29px";  tabindex="2" required="required" name="p_title"> 
              <option value="Mrs."<?php if($p_title=='Mrs.') { ?> selected="selected" <?php } ?>>Mrs.</option>
              <option value="Mr."<?php if($p_title=='Mr.') { ?> selected="selected" <?php } ?>>Mr.</option>
              <option value="Miss."<?php if($p_title=='Miss.') { ?> selected="selected" <?php } ?>>Miss.</option>
              <option value="Ma."<?php if($p_title=='Ma.') { ?> selected="selected" <?php } ?>>Ma.</option>
            </select>
          </div> 
           <div class="form-group col-md-3">
            <label  class="control-label">Name <sup>*</sup></label>
            <input class="form-control input-sm validate[required]"  tabindex="3" placeholder="Patient Name"style="height:29px";  type="text" name="p_name" id="p_name" value="<?= $p_name  ?>">
            <input type="hidden" name="p_regfee" value="<?= $p_regfee  ?>">
          </div>
          <!-- <div class="form-group col-md-2">
            <label  class="control-label">DOB </label>
            <input class="form-control input-sm " data-prompt-position="topLeft:160" tabindex="4" placeholder="Patient DOB" id="datepicker" style="height:29px"; type="text" name="p_dob" value="<?= $p_dob  ?>">
          </div> -->
          <div class="form-group col-md-2">
            <label  class="control-label">Gender <sup>*</sup></label>
            <select class="form-control input-sm validate[required]"  data-prompt-position="bottomLeft:160" style="height:29px";  tabindex="5" required="required"   name="p_sex"> 
              <option value="Select">--Select Gender--</option>
              <option value="M"<?php if($p_sex=='M') { ?> selected="selected" <?php } ?>>Male</option>
              <option value="F"<?php if($p_sex=='F') { ?> selected="selected" <?php } ?>>Female</option>
              <option value="C"<?php if($p_sex=='C') { ?> selected="selected" <?php } ?>>Child</option>
            </select>
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Blood </label>
             <select class="form-control input-sm " style="height:29px";  tabindex="6" required="required" name="p_blood"> 
              <option value="Select">Select Group</option>
              <option value="O +"<?php if($p_blood=='O +') { ?> selected="selected" <?php } ?>>O +</option>
              <option value="O -"<?php if($p_blood=='O -') { ?> selected="selected" <?php } ?>>O -</option>
              <option value="A +"<?php if($p_blood=='A +') { ?> selected="selected" <?php } ?>>A +</option>
              <option value="A -"<?php if($p_blood=='A -') { ?> selected="selected" <?php } ?>>A -</option>
              <option value="B +"<?php if($p_blood=='B +') { ?> selected="selected" <?php } ?>>B +</option>
              <option value="B -"<?php if($p_blood=='B -') { ?> selected="selected" <?php } ?>>B -</option>
              <option value="AB +"<?php if($p_blood=='AB +') { ?> selected="selected" <?php } ?>>AB +</option>
              <option value="AB -"<?php if($p_blood=='AB -') { ?> selected="selected" <?php } ?>>AB -</option>
            </select>
          </div>
          <div class="form-group col-md-1">
            <label  class="control-label">Age <sup>*</sup></label>
            <input class="form-control input-sm validate[required,custom[onlyNumberSp]]"  data-prompt-position="bottomLeft:160"  tabindex="7" placeholder="Age" style="height:29px";  type="text" name="p_age" value="<?= $p_age ?>" id="p_age">
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Phone<sup>*</sup></label>
            <input  type="text" name="p_phone" tabindex="8" placeholder="Patient Phone" style="height:29px"; class="form-control input-sm validate[required,custom[phone]]"  data-prompt-position="bottomLeft:160" id="p_phone" value="<?= $p_phone ?>">
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Email </label>
            <input class="form-control input-sm" data-prompt-position="bottomLeft:160" tabindex="9" placeholder="Patient Email" style="height:29px";  type="text" name="p_email" value="<?= $p_email  ?>">
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Renewed Date </label>
            <input class="form-control input-sm" <?=$status?> id="datepicker1" placeholder="Last Renewal" style="height:29px";  type="text" name="p_renew_date" value="<?= $p_renew_date ?>">
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Vists after Renewal </label>
            <input class="form-control input-sm" <?=$status?>  placeholder="Visits after last Renewal" style="height:29px";  type="text" name="p_renew_visits" value="<?= $p_renew_visits ?>">
          </div>
          <div class="form-group col-md-12"></div>
          <div class="form-group col-md-3">
           <label  class="control-label"> Renewal Visits <sup>*</sup></label>
          <input class="form-control input-sm" <?=$status?>  tabindex="17"  placeholder="Renewal Visits" style="width:150px;height:29px";  type="text" name="p_renew_count" value="<?= $p_renew_count ?>">
          </div>
           <div class="form-group col-md-3">
            <label  class="control-label">Address</label>
            <input type="text" name="p_address" tabindex="10" placeholder="Patient Address" style="height:29px"; class="form-control input-sm" value="<?= $p_address ?>">
          </div>
           <div class="form-group col-md-3">
            <label  class="control-label"> &nbsp;</label>
            <input type="text"  tabindex="11" placeholder="Street" style="height:29px"; name="p_street" class="form-control input-sm" value="<?= $p_street ?>">
          </div>
          <div class="form-group col-md-3">
             <label  class="control-label">  </label>
             <select class="form-control input-sm validate[required]"  style="height:29px";   tabindex="12" name="p_country" id="country"> 
              <option value="Select">--Country--</option>
              <?php foreach ($countries as $key => $company) {
                ?>
              <option value="<?php echo $company['id']?>"<?php if($company['id']==$p_country) { ?> selected="selected" <?php } ?>><?php echo $company['name']?></option>
                <?php
              }?>
            </select>
          </div>
        </div>
          <div class="col-md-12"> 
           <div class="form-group col-md-3">
           <label  class="control-label"> Renewal Days <sup>*</sup></label>
          <input class="form-control input-sm" <?=$status?>  tabindex="18"  placeholder="Renewal Days" style="width:150px;height:29px";  type="text" name="p_renew_days" value="<?= $p_renew_days ?>">
          </div>
          <div class="form-group col-md-3">
          <select class="form-control input-sm validate[required]"  style="height:29px";  tabindex="13" name="p_state" id="state"> 
              <option value="Select">--State--</option>
              <?php foreach ($states as $key => $company) {
                ?>
              <option value="<?php echo $company['id']?>"<?php if($company['id']==$p_state) { ?> selected="selected" <?php } ?>><?php echo $company['name']?></option>
                <?php
              }?>
            </select>
          </div> 
          <div class="form-group col-md-3">
          <select class="form-control input-sm"  style="height:29px";  tabindex="14" required="required"   name="p_city"  id="city"> 
              <option value="Select">--City--</option>
              <?php foreach ($cities as $key => $company) {
                ?>
              <option value="<?php echo $company['id']?>"<?php if($company['id']==$p_city) { ?> selected="selected" <?php } ?>><?php echo $company['name']?></option>
                <?php
              }?>
            </select>
          </div>
          <div class="form-group col-md-3">
            <input class="form-control input-sm"  tabindex="15"  placeholder="Zip Code" style="height:29px";  type="text" name="p_zip" value="<?= $p_zip ?>">
          </div>
           <div class="form-group col-md-3">
            <label  class="control-label">Care Of</label>
            <input class="form-control input-sm"  tabindex="15"  placeholder="Care of" style="height:29px";  type="text" name="p_guardian" value="<?= $p_guardian ?>">
          </div>
           <div class="form-group col-md-3">
            <label  class="control-label">ID Type </label>
            <select class="form-control input-sm"  style="height:29px";  tabindex="16" name="p_id_type">
              <option value="Select">--Patient ID Type--</option>
              <option value="Aadhar"<?php if($p_id_type=='Aadhar') { ?> selected="selected" <?php } ?>>Aadhar</option>
              <option value="Voters ID"<?php if($p_id_type=='Voters ID') { ?> selected="selected" <?php } ?>>Voters ID</option>
              <option value="Driving Licence"<?php if($p_id_type=='Driving Licence') { ?> selected="selected" <?php } ?>>Driving Licence</option>
              <option value="PAN Card"<?php if($p_id_type=='PAN Card') { ?> selected="selected" <?php } ?>>PAN Card</option>
              <option value="Passport"<?php if($p_id_type=='Passport') { ?> selected="selected" <?php } ?>>Passport</option>
            </select>              
          </div> 

          <div class="form-group col-md-3">
           <label  class="control-label">ID No.</label>
          <input class="form-control input-sm"  tabindex="16"  placeholder="Patient ID No" style="height:29px";  type="text" name="p_id_no" value="<?= $p_id_no ?>">
          </div>
            </div> 
        </div>
      </div>

      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            <?php if($p_name == "") { ?>
            <button class="btn-large btn-warning btn" type="submit" name="submit1" accesskey="p" title="short key-ALT+P"><i class="fa fa-print"></i> Registeration Card</button>
            <?php } else { ?>
            <button class="btn-large btn-success btn" type="submit" name="submit1" accesskey="p" title="short key-ALT+P"><i class="fa fa-floppy-o"></i> Update </button>
            <?php } ?>
              <input class="btn-large btn-default btn" type="reset" value="Reset">
          </div>
        </div>
      </div>

      <?php echo form_close(); ?>

      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                      <h4 class="modal-title" id="myModalLabel"><b>Merge Duplicate MRD</b></h4>
                    </div>                                                
          
                    <?php echo form_open_multipart($action1); ?>
                    <div class="modal-body">
                      <div class="box-body">
                  <div class="row">
                <div class="col-md-12">
                <div class="col-md-12">

                 <!--  <div class="row">
                  <div class="col-md-12">
                        <table class="category_table table table-condensed dataTable no-footer">
                            <thead>
                                <tr>
                                    <th>MRD No.</th>
                                    <th>Patient</th>
                                    <th>Phone</th>
                                    <th>Age</th>
                                    <th>Blood Group</th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php echo $output; ?>  
                    </tbody>
                </table>
            </div> </div>  -->
            <div class="col-md-3">
            <label  class="control-label">MRD</label>
            <input class="form-control input-sm" style="height:29px;" type="input" name="p_mrd_new" value="<?=$p_mrd_no ?>">
             </div>
                    <div class="col-md-3">
                      <label  class="control-label">Duplicate MRD</label>
                      <input class="form-control input-sm" style="height:29px;" type="input" name="p_mrd_old" value="">
                    </div> 
        </div>
    </div><!-- /.box-body -->       
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input id="add_inquiry_submit" type="submit" name="submit" value="Save" class="btn btn-primary"/>
                    </div>
                  </div>
              </div>
                    <?php echo form_close(); ?>
                  
            </div></div></div>

            <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                      <h4 class="modal-title" id="myModalLabel"><b>Cancelled MRDs</b></h4>
                    </div>                                                
        
                    <div class="modal-body">
                      <div class="box-body">
                  <div class="row">
                <div class="col-md-12">
                <div class="col-md-12">

                  <div class="row">
                  <div class="col-md-12">
                        <table class="category_table table table-condensed dataTable no-footer">
                            <thead>
                                <tr>
                                    <th>MRD No.</th>
                                    <th>Use</th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php echo $output1; ?>  
                    </tbody>
                </table>
            </div> </div> 
            </div>       
                    </div>
                  </div>
              </div> </div>
                  
            </div></div></div>
 
      <script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#blah')
            .attr('src', e.target.result)
            .width(139)
            .height(150);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
</script>

    </div>
   <br> <br> <br> <br>  <br> 
</section>

</div><!-- /.right-side -->

<script>
 $('#datepicker').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true
    });
 $('#datepicker1').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true
    });
 </script>


<script>
function save_print(){
      var postData = $("#post_form").serializeArray();
      var p_name         = $('#p_name').val();
      if(p_name!="")
          {
      var formURL  = "<?= base_url() ?>patient/add";
      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          var Printid     = "<?php echo base_url();?>index.php/patient/getPrint?sPrintid="+data;
          window.location = Printid;
        }
      });
      }
} 

</script>
