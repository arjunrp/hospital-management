
<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
<?php
foreach ($additionals as $additional) {

  $ve_id          = $additional['ve_id'];
  $ve_vno         = $additional['ve_vno'];
  $ve_date        = $additional['ve_date'];
  $ve_mrd         = $additional['ve_mrd'];
  $p_name         = $additional['ve_patient'];
  $p_phone        = $additional['ve_phone'];
  $u_name         = $additional['ve_doctor'];
  // $dp_department  = $additional['dp_department'];
  $ve_user        = $additional['ve_user'];
  $ve_amount      = $additional['ve_amount'];
  $ve_discount    = $additional['ve_discount'];
  $ve_discounta   = $additional['ve_discounta'];
  $ve_apayable    = $additional['ve_apayable'];
  $ve_round       = $additional['ve_round'];
}
 ?>

  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       OP Billing
       <small>View </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."opbilling"; ?>">OP Billing</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-success pull-right  view-btn-create" href="<?php echo base_url();?>index.php/opbilling">
        <i class="fa fa-mail-reply-all"></i> Back
      </a>
    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>

      <form id="post_form">
     <div class="box-body">
      <div class="row"> 
       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">Voucher No. : <sup></sup></label> <?= $ve_vno?>             
        </div>
        <div class="col-md-2">
          <label  class="control-label">Date : <sup></sup></label> <?= date("d-m-Y",strtotime($ve_date)) ?>               
        </div>
         
          <div class="col-md-2" >
         <label  class="control-label">MRD : <sup></sup></label> <?= $ve_mrd?>    
               </div>

       <div class="col-md-2">
         <label  class="control-label">Phone No. : <sup></sup></label> <?= $p_phone?>     
       </div>

       <div class="col-md-4" style="text-align:center">
         <label  class="control-label">Patient's Name : <sup></sup></label> <?= $p_name?>    
       </div>
     </div>
     <div class="col-md-12"><br>

         <!-- <div class="col-md-3">
         <label  class="control-label">Department : <sup></sup></label> <?= $dp_department?>    
       </div> -->
        <div class="col-md-3">
         <label  class="control-label">Doctor : <sup></sup></label> <?= $u_name?>      
       </div>
       <div class="col-md-2">
         <label  class="control-label">User Id : <sup></sup></label> <?= $ve_user?>      
       </div>

     </div>

          </div>
          <div class="box-footer">
            <div class="row">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-bordered table-hover gridView" id="item_table">
                    <thead>
                      <tr>
                        <th width="10%">Sl No.</th>
                        <th width="50%">Particulars</th>
                        <th width="15%">Rate</th>
                        <th width="10%">Qty</th>
                        <th width="15%">Amount</th>
                      </tr>
                    </thead>
                  <tbody> 

                    <?php $slno6 =1; ?>
                    <?php foreach ($additionals as $additional) {
                      ?> <tr><td><?=$slno6?></td><td><?=$additional['ved_item']?></td>
                      <td><?=$additional['ved_price']?></td>
                      <td><?=$additional['ved_qty']?></td>
                      <td><?=$additional['ved_gtotal']?></td></tr>
                       <?php
                    $slno6++;  } ?>
                  </tbody>
                  </table>
                  <table class="table table-bordered">
                    <tr><th width="60%"></th><th>Total</th>
                      <th>Rs. <label><?=$ve_amount?></label></th></tr>
                       <tr><th width="60%"></th><th>Discount</th><th>
                      Rs. <label><?=$ve_discounta?></label> (<label><?=$ve_discount?></label> %)</th></tr>

                      <tr><th width="60%"></th><th>Amount Payable</th>
                      <th>Rs. <label><?=$ve_apayable?></label></th></tr>
                      <tr><th width="60%"></th><th>Round Off</th>
                      <th>Rs. <label><?=$ve_round?></label></th></tr>
                    </table>
                  <input type="hidden" id="Printid" value="<?=$ve_id?>"> 
                    <a href="#" class="btn btn-warning" onclick="print();" accesskey="p" title="short key-ALT+P" ><i class="fa fa-print"></i> Print</a> 
                
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</form>
</div>
</section>

</section><!-- /.right-side -->

<script>

function print(){

          var ve_id         =  $('#Printid').val();

          window.location = "<?php echo base_url();?>index.php/opbilling/getPrint?Printid="+ve_id;


}
</script>

<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });</script>
<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>