<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
  <script src="<?= base_url() ?>asset/plugins/timepicker/bootstrap-timepicker.min.js"></script>
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/timepicker/bootstrap-timepicker.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="<?= base_url() ?>/confirms/bootpopup.js"></script>
  <script type="text/javascript" charset="utf-8">
 $(function()
  {
    $("#ve_mrd").autocomplete({
      source: [<?php
      $i=0;
      foreach ($patients as $patient){
        if ($i>0) {echo ",";}
        echo '{value:"' . $patient['ip_mrd'] . '",patient:"' .$patient['p_title'] ." " . $patient['p_name'] . '",patientph:"' . $patient['p_phone'] . '",patientop:"' . $patient['ip_ipno'] . '",department:"' . $patient['dp_department'] . '",doctor:"' . $patient['u_name'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          // $("#customerid").val(ui.item ? ui.item.vid : '');
          $("#bk_name").val(ui.item ? ui.item.patient : '');
          $("#bk_phone").val(ui.item ? ui.item.patientph : '');
          $("#ve_customer").val(ui.item ? ui.item.patientop : '');
          $("#ip_department").val(ui.item ? ui.item.department : '');
          $("#ip_doctor").val(ui.item ? ui.item.doctor : '');
        }
      });   
  });   

  $(function() {
    $("#bk_phone").autocomplete({
      source: [<?php
      $i=0;
      foreach ($patients as $patient){
        if ($i>0) {echo ",";}
        echo '{value:"' . $patient['p_phone'] . '",patient:"' .$patient['p_title'] ." " . $patient['p_name'] . '",patientmrd:"' . $patient['ip_mrd'] . '",patientop:"' . $patient['ip_ipno'] . '",department:"' . $patient['dp_department'] . '",doctor:"' . $patient['u_name'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          // $("#customerid").val(ui.item ? ui.item.vid : '');
          $("#bk_name").val(ui.item ? ui.item.patient : '');
          $("#ve_mrd").val(ui.item ? ui.item.patientmrd : '');
          $("#ve_customer").val(ui.item ? ui.item.patientop : '');
          $("#ip_department").val(ui.item ? ui.item.department : '');
          $("#ip_doctor").val(ui.item ? ui.item.doctor : '');
        }
      });   
  }); 

  $(function() {
    $("#ve_customer").autocomplete({
      source: [<?php
      $i=0;
      foreach ($patients as $patient){
        if ($i>0) {echo ",";}
        echo '{value:"' . $patient['ip_ipno'] . '",mrd:"' . $patient['ip_mrd'] . '",patient:"'.$patient['p_title'] ." " . $patient['p_name'] . '",patientph:"' . $patient['p_phone'] . '",department:"' . $patient['dp_department'] . '",doctor:"' . $patient['u_name'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          $("#ve_mrd").val(ui.item ? ui.item.mrd : '');
          $("#bk_name").val(ui.item ? ui.item.patient : '');
          $("#bk_phone").val(ui.item ? ui.item.patientph : '');
          $("#ip_department").val(ui.item ? ui.item.department : '');
          $("#ip_doctor").val(ui.item ? ui.item.doctor : '');
        }
      });   
  });   

  $(function() {
    $("#product").autocomplete({
      source: [<?php
      $i=0;
      foreach ($additional_bills as $additional_bill){
        if ($i>0) {echo ",";}
        echo '{value:"' . $additional_bill['ab_name'] . '",price:"' . $additional_bill['ab_amount'] . '",productid:"' . $additional_bill['ab_id'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          $("#productid").val(ui.item ? ui.item.productid : '');
          $("#product_price").val(ui.item ? ui.item.price : '');
        }
      });   
  });

  </script>
  <script type="text/javascript">
function calcu(p)
  {

    var gtotal7 = 0;
    var gtotal6 = 0;
    var gtotal5 = 0;
    var gtotal4 = 0;
    var gtotal3 = 0;
    var gtotal2 = 0;
    var gtotal1 = 0;
    var gtotal  = 0;

    var rm_fees   = parseFloat($('#rm_fees'+p).val());
    var rm_nurse  = parseFloat($('#rm_nurse'+p).val());
    var rm_hc     = parseFloat($('#rm_hc'+p).val());
    var diffDays  = parseFloat($('#diffDays'+p).val());
    var totrent   = parseFloat(rm_fees)+parseFloat(rm_nurse)+parseFloat(rm_hc);
    var total     = totrent * diffDays;

    // alert(total)

    // // alert(sgstp+","+cgstp+","+sgsta+","+cgsta+","+pod_total);

    $('#rm_total'+p).val((Math.round(total * 100) / 100).toFixed(2));
    $('#rm_totals'+p).html((Math.round(total * 100) / 100).toFixed(2));

    $('.gtotal6').each(function(){
        gtotal6 += parseFloat(this.value);
    });
    $('.gtotal7').each(function(){
        gtotal7 += parseFloat(this.value);
    });
    $('.gtotal5').each(function(){
        gtotal5 += parseFloat(this.value);
    });
    $('.gtotal4').each(function(){
        gtotal4 += parseFloat(this.value);
    });

    $('.gtotal3').each(function(){
        gtotal3 += parseFloat(this.value);
    });
    $('.gtotal1').each(function(){
        gtotal1 += parseFloat(this.value);
    });
    $('.gtotal2').each(function(){
        gtotal2 += parseFloat(this.value);
    });
    $('.gtotal').each(function(){
        gtotal += parseFloat(this.value);
    });

    lab_mrp        =  parseFloat(gtotal) + parseFloat(gtotal3);
    lab_mrp1       =  parseFloat(gtotal) + parseFloat(gtotal1) + parseFloat(gtotal3) + parseFloat(gtotal4) + parseFloat(gtotal7) + parseFloat(gtotal6)   - parseFloat(gtotal5);
    lab_mrp        =  (Math.round(lab_mrp * 100) / 100).toFixed(2);
    lab_mrp1       =  (Math.round(lab_mrp1 * 100) / 100).toFixed(2);

    gtotal2        =  (Math.round(gtotal2 * 100) / 100).toFixed(2);

    amount         =  parseFloat(lab_mrp) - parseFloat(gtotal2);
    amount         =  (Math.round(amount * 100) / 100).toFixed(2);
    payable        =  Math.round(amount);

    amount1         =  parseFloat(lab_mrp1) - parseFloat(gtotal2);
    amount1         =  (Math.round(amount1 * 100) / 100).toFixed(2);
    payable1        =  Math.round(amount1);


    $('#lab_mrp').val(lab_mrp);
    $('#lab_mrp1').html(lab_mrp1);
    $('#advance').val(gtotal2);
    $('#advance1').html(gtotal2);

    $('#apayable').val(payable);
    $('#apayable1').html(payable1);
            // alert(payable1)

    var pDiscount         =  $('#discountp').val();

    var pDiscount1        =  parseFloat(payable1) * parseFloat(pDiscount)/100;
    var sum               =  parseFloat(payable1) - parseFloat(pDiscount1);
    sum                   =  (Math.round(sum * 100) / 100).toFixed(2);
    pDiscount1            =  (Math.round(pDiscount1 * 100) / 100).toFixed(2);

    var pDiscounto1       =  parseFloat(payable) * parseFloat(pDiscount)/100;
    var sum1              =  parseFloat(payable) - parseFloat(pDiscounto1);
    sum1                  =  (Math.round(sum1 * 100) / 100).toFixed(2);
    pDiscounto1           =  (Math.round(pDiscounto1 * 100) / 100).toFixed(2);

    grand_total  = Math.round(sum);
    grand_totalo = Math.round(sum1);

    $('#discountp').val(pDiscount);
    $('#discountp1').html(pDiscount);
    $('#discounta').val(pDiscounto1);
    $('#discounta1').html(pDiscount1);

    $('#sum').val(grand_totalo);
    $('#sum1').html(grand_total);
    $('#sum2').val(grand_total);

    var roundoff   =   Math.round(sum) - parseFloat(sum);
    roundoff       =  (Math.round(roundoff * 100) / 100).toFixed(2);

    var roundoffo   =   Math.round(sum1) - parseFloat(sum1);
    roundoffo       =  (Math.round(roundoffo * 100) / 100).toFixed(2);
    $('#roundoff').val(roundoffo);
    $('#roundoff1').html(roundoff);

  }

  </script>
  <script> 
$(document).ready(function() {
    var slno1 = 1;
  $('#ve_customer').change(function(){

    $('#item_table').find("tr:gt(0)").remove();
    var ve_cust = $('#ve_customer').val();
    var grand   = 0;
    var ip_charge   = 0;
    var grand2  = 0;
    var grand1 = 0;
     $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>discharge/get_room/"+ve_cust,
     success: function(data){
      if(data!=0) {
      var slno  = 1;
      var count = 1;
      var newhead     = '<tr><th>'+slno1+'</th><th colspan="9">Rooms</th></tr> <tr><th></th><th>Sl.no.</th><th>Rooms</th><th>Admission Date</th><th>Discharged Date</th><th>Total Days</th><th>Rent</th><th>Nursing Charge</th><th>H.K Charge</th><th>Credit Amount</th></tr>';
      $('#item_table tr:last').after(newhead);
      $.each(data, function(index) {
        var date1 = new Date(data[index].rs_adm);
        var date2 = data[index].rs_dis;
        if(date2=="0000-00-00 00:00:00")
        {          
          var date2   = new Date();
          var dd      = date2.getDate();
          var mm      = date2.getMonth()+1;
          var yyyy    = date2.getFullYear();
          var hour    = date2.getHours();
          var minute  = date2.getMinutes();
          var second  = date2.getSeconds(); 

          if(dd<10) { dd='0'+dd; } 
          if(mm<10) { mm='0'+mm; }
          var date3   = yyyy+'-'+mm+'-'+dd+ ' '+hour+':'+minute+':'+second;   

           var date2   = new Date(date2);
           // var diff = date2.getTime() - date1.getTime();
           // var hr = diff / 60000;
           // diffDays = Math.round(hr / 60);
          // var timeDiff = Math.abs(date2.getTime() - date1.getTime());
          // var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

          seconds = Math.floor((date2 - (date1))/1000);
          minutes = Math.floor(seconds/60);
          hours = Math.floor(minutes/60);
          
          days = Math.floor(hours/24);
          hours = hours-(days*24);
          minutes = minutes-(days*24*60)-(hours*60);
          if(hours >= 12 && hours <= 24)
          {
            diffDays = days + 1;
          }
          else if(hours >= 6 && hours < 12)
          {
            diffDays = days + .5;
          }
          else 
          {
            diffDays = days;
          }     

        }
        else
        {
          var date3 = data[index].rs_dis;
          var date2 = new Date(date2);

          seconds = Math.floor((date2 - (date1))/1000);
          minutes = Math.floor(seconds/60);
          hours = Math.floor(minutes/60);
          
          days = Math.floor(hours/24);
          hours = hours-(days*24);
          minutes = minutes-(days*24*60)-(hours*60);
          if(hours >= 12 && hours <= 24)
          {
            diffDays = days + 1;
          }
          else if(hours >= 6 && hours < 12)
          {
            diffDays = days + .5;
          }
          else 
          {
            diffDays = days;
          }

          // var diff = date2.getTime() - date1.getTime();
          // var hr = diff / 60000;
          // diffDays = Math.round(hr / 60);

          // var timeDiff = Math.abs(date2.getTime() - date1.getTime());
          // var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24))+1; 
        }
        var total    = parseFloat(data[index].rm_nurse) + parseFloat(data[index].rm_fees) + parseFloat(data[index].rm_hc);
        var gtotal    = (parseFloat(data[index].rm_nurse) + parseFloat(data[index].rm_fees) + parseFloat(data[index].rm_hc)) * parseFloat(diffDays);
        gtotal = (Math.round(gtotal * 100) / 100).toFixed(2);
         var newrow      = '<tr><td> </td><td>'+slno+'</td><td><input type="hidden" value="'+data[index].rs_id+'" name="rs_id[]"><input class="form-control input-sm" type="hidden" value="'+data[index].rm_no+'">' + data[index].rm_no + '</td><td>'+data[index].rs_adm+'</td><td>'+date3+'</td><td><input class="form-control input-sm" type="input" value="'+diffDays+'" id="diffDays'+count+'" onkeyup="calcu('+count+')" name="rs_days[]"></td><td width="100px"><input class="form-control input-sm" type="input" value="'+data[index].rm_fees+'" id="rm_fees'+count+'" onkeyup="calcu('+count+')" name="rs_rt[]"></td><td width="150px"><input class="form-control input-sm" type="input" value="'+data[index].rm_nurse+'" id="rm_nurse'+count+'" onkeyup="calcu('+count+')" name="rs_nc[]"></td><td width="100px"><input class="form-control input-sm" type="input" value="'+data[index].rm_hc+'" id="rm_hc'+count+'" onkeyup="calcu('+count+')" name="rs_hc[]"></td><td><input type="hidden" value="'+total+'"><input id="rm_total'+count+'" class="gtotal" type="hidden" value="'+gtotal+'"><b>Rs. </b><label id="rm_totals'+count+'">'+gtotal+'</label></td></tr>';
        $('#item_table tr:last').after(newrow);
        count++;
        slno++;
      });
        slno1++;
        $('.gtotal').each(function(){
        grand  += parseFloat(this.value);
    });
        ip_charge = grand;
        grand2 = grand;
        ip_charge = (Math.round(ip_charge * 100) / 100).toFixed(2);
        grand = (Math.round(grand * 100) / 100).toFixed(2);
      $('#lab_mrp').val(ip_charge);
      $('#lab_mrp1').html(grand);

      var apayable   =  parseFloat(grand);
      amount         =  (Math.round(apayable * 100) / 100).toFixed(2);
      payable        =  Math.round(amount);

      $('#apayable').val(ip_charge);
      $('#apayable1').html(payable);
      $('#sum').val(ip_charge);
      $('#sum1').html(payable);
      $('#sum2').val(payable);

      roundoff       =  parseFloat(payable) - parseFloat(amount); 
      roundoff       =  (Math.round(roundoff * 100) / 100).toFixed(2);
      $('#roundoff').val(roundoff);
      $('#roundoff1').html(roundoff);
    } 

 $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>discharge/get_voucher_bills/"+ve_cust,
     success: function(data1){
      if(data1!=0) {
      var slno3 = 1;
      var o_lab = 0;
        var newhead1      = '<tr><th>'+slno1+'</th><th colspan="9">Lab</th></tr> <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Credit Amount</th></tr>'
        $('#item_table tr:last').after(newhead1);
      $.each(data1, function(index) {
         var newrow1      = '<tr><td></td><td>'+slno3+'</td><td colspan="3"><input class="form-control input-sm" type="hidden" value="Lab Bill">' + data1[index].ve_date + '</td><td colspan="4">'+data1[index].ve_vno+'</td><td><input class="gtotal1" type="hidden" value="'+data1[index].ve_apayable+'"><b>Rs. </b><label>'+data1[index].ve_apayable+'</label></td></tr>';
        $('#item_table tr:last').after(newrow1);
        slno3++;
      });
        slno1++;
        $('.gtotal1').each(function(){
        o_lab += parseFloat(this.value);
    });

        grand = parseFloat(grand) + parseFloat(o_lab);
      $('#lab_mrp').val(ip_charge);
      $('#lab_mrp1').html(grand);

      amount         =  (Math.round(grand * 100) / 100).toFixed(2);
      payable        =  Math.round(amount);


      $('#apayable').val(ip_charge);
      $('#apayable1').html(payable);
      $('#sum').val(ip_charge);
      $('#sum1').html(payable);
      $('#sum2').val(payable);

      roundoff       =  parseFloat(payable) - parseFloat(amount); 
      roundoff       =  (Math.round(roundoff * 100) / 100).toFixed(2);
      $('#roundoff').val(roundoff);
      $('#roundoff1').html(roundoff);
    } 


    $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>discharge/get_scanning_bills/"+ve_cust,
     success: function(data5){
      if(data5!=0) {
      var slno10 = 1;
      var o_scan = 0;
        var newhead1      = '<tr><th>'+slno1+'</th><th colspan="9">Scanning</th></tr> <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Credit Amount</th></tr>'
        $('#item_table tr:last').after(newhead1);
      $.each(data5, function(index) {
         var newrow1      = '<tr><td></td><td>'+slno10+'</td><td colspan="3"><input class="form-control input-sm" type="hidden" value="Scanning Bill">' + data5[index].ve_date + '</td><td colspan="4">'+data5[index].ve_vno+'</td><td><input class="gtotal6" type="hidden" value="'+data5[index].ve_apayable+'"><b>Rs. </b><label>'+data5[index].ve_apayable+'</label></td></tr>';
        $('#item_table tr:last').after(newrow1);
        slno10++;
      });
        slno1++;
        $('.gtotal6').each(function(){
        o_scan += parseFloat(this.value);
    });

        grand = parseFloat(grand) + parseFloat(o_scan);
      $('#lab_mrp').val(ip_charge);
      $('#lab_mrp1').html(grand);

      amount         =  (Math.round(grand * 100) / 100).toFixed(2);
      payable        =  Math.round(amount);


      $('#apayable').val(ip_charge);
      $('#apayable1').html(payable);
      $('#sum').val(ip_charge);
      $('#sum1').html(payable);
      $('#sum2').val(payable);

      roundoff       =  parseFloat(payable) - parseFloat(amount); 
      roundoff       =  (Math.round(roundoff * 100) / 100).toFixed(2);
      $('#roundoff').val(roundoff);
      $('#roundoff1').html(roundoff);
    } 

    $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>discharge/get_xray_bills/"+ve_cust,
     success: function(data6){
      if(data6!=0) {
      var slno11 = 1;
      var o_xray = 0;
        var newhead1      = '<tr><th>'+slno1+'</th><th colspan="9">Xray</th></tr> <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Credit Amount</th></tr>'
        $('#item_table tr:last').after(newhead1);
      $.each(data6, function(index) {
         var newrow1      = '<tr><td></td><td>'+slno11+'</td><td colspan="3"><input class="form-control input-sm" type="hidden" value="Xray Bill">' + data6[index].ve_date + '</td><td colspan="4">'+data6[index].ve_vno+'</td><td><input class="gtotal7" type="hidden" value="'+data6[index].ve_apayable+'"><b>Rs. </b><label>'+data6[index].ve_apayable+'</label></td></tr>';
        $('#item_table tr:last').after(newrow1);
        slno11++;
      });
        slno1++;
        $('.gtotal7').each(function(){
        o_xray += parseFloat(this.value);
    });

        grand = parseFloat(grand) + parseFloat(o_xray);
      $('#lab_mrp').val(ip_charge);
      $('#lab_mrp1').html(grand);


      amount         =  (Math.round(grand * 100) / 100).toFixed(2);
      payable        =  Math.round(amount);


      $('#apayable').val(ip_charge);
      $('#apayable1').html(payable);
      $('#sum').val(ip_charge);
      $('#sum1').html(payable);
      $('#sum2').val(payable);

      roundoff       =  parseFloat(payable) - parseFloat(amount); 
      roundoff       =  (Math.round(roundoff * 100) / 100).toFixed(2);
      $('#roundoff').val(roundoff);
      $('#roundoff1').html(roundoff);
    } 

    $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>discharge/get_pharmacy_bills/"+ve_cust,
     success: function(data2){
      if(data2!=0) {
      var slno6 = 1;
      var o_pharmacy = 0;
        var newhead2      = '<tr><th>'+slno1+'</th><th colspan="9">Pharmacy</th></tr> <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Credit Amount</th></tr>'
        $('#item_table tr:last').after(newhead2);
      $.each(data2, function(index) {
         var newrow2      = '<tr><td></td><td>'+slno6+'</td><td colspan="3"><input class="form-control input-sm" type="hidden" value="Pharmacy Bill">' + data2[index].ve_date + '</td><td colspan="4">'+data2[index].ve_vno+'</td><td><input class="gtotal4" type="hidden" value="'+data2[index].ve_apayable+'"><b>Rs. </b><label>'+data2[index].ve_apayable+'</label></td></tr>';
        $('#item_table tr:last').after(newrow2);
        slno6++;
      });
        slno1++;
        $('.gtotal4').each(function(){
        o_pharmacy += parseFloat(this.value);
    });
        grand  = parseFloat(grand) + parseFloat(o_pharmacy);
      $('#lab_mrp').val(ip_charge);
      $('#lab_mrp1').html(grand);

      amount         =  (Math.round(grand * 100) / 100).toFixed(2);
      payable        =  Math.round(amount);

      $('#apayable').val(ip_charge);
      $('#apayable1').html(payable);
      $('#sum').val(ip_charge);
      $('#sum1').html(payable);
      $('#sum2').val(payable);

      roundoff       =  parseFloat(payable) - parseFloat(amount); 
      roundoff       =  (Math.round(roundoff * 100) / 100).toFixed(2);
      $('#roundoff').val(roundoff);
      $('#roundoff1').html(roundoff);
    } 

    $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>discharge/get_pharmacy_returns/"+ve_cust,
     success: function(data3){
      if(data3!=0) {
      var slno7 = 1;
      var o_return = 0;
        var newhead3      = '<tr><th>'+slno1+'</th><th colspan="9">Medicine Return</th></tr> <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Debit Amount</th></tr>'
        $('#item_table tr:last').after(newhead3);
      $.each(data3, function(index) {
         var newrow3      = '<tr><td></td><td>'+slno7+'</td><td colspan="3"><input class="form-control input-sm" type="hidden" value="Pharmacy Return Bill">' + data3[index].ve_date + '</td><td colspan="4">'+data3[index].ve_vno+'</td><td><input class="gtotal5" type="hidden" value="'+data3[index].ve_apayable+'"><b>Rs. </b><label>'+data3[index].ve_apayable+'</label></td></tr>';
        $('#item_table tr:last').after(newrow3);
        slno7++;
      });
        slno1++;
        $('.gtotal5').each(function(){
        o_return -= parseFloat(this.value);
    });
        grand     = parseFloat(grand) + parseFloat(o_return);
      $('#lab_mrp').val(ip_charge);
      $('#lab_mrp1').html(grand);

      amount         =  (Math.round(grand * 100) / 100).toFixed(2);
      payable        =  Math.round(amount);

      $('#apayable').val(ip_charge);
      $('#apayable1').html(payable);
      $('#sum').val(ip_charge);
      $('#sum1').html(payable);
      $('#sum2').val(payable);

      roundoff       =  parseFloat(payable) - parseFloat(amount); 
      roundoff       =  (Math.round(roundoff * 100) / 100).toFixed(2);
      $('#roundoff').val(roundoff);
      $('#roundoff1').html(roundoff);
    } 

    $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>discharge/get_advances/"+ve_cust,
     success: function(data4){
      if(data4!=0) {
      var slno4 = 1;
        var newhead4      = '<tr><th>'+slno1+'</th><th colspan="9">Advance Payments</th></tr> <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Debit Amount</th></tr>'
        $('#item_table tr:last').after(newhead4);
      $.each(data4, function(index) {
         var newrow4      = '<tr><td></td><td>'+slno4+'</td><td colspan="3">' + data4[index].ve_date + '</td><td colspan="4">'+data4[index].ve_vno+'</td><td><input class="gtotal2" type="hidden" value="'+data4[index].ve_apayable+'"><b>Rs. </b><label>'+data4[index].ve_apayable+'</label></td></tr>';
        $('#item_table tr:last').after(newrow4);
        slno4++;
      });
        slno1++;
        $('.gtotal2').each(function(){
        grand1 += parseFloat(this.value);
    });
      $('#lab_mrp').val(ip_charge);
      $('#lab_mrp1').html(grand);
      $('#advance').val(grand1);
      $('#advance1').html(grand1);

      var apayable   = parseFloat(grand) - parseFloat(grand1);
      amount         =  (Math.round(apayable * 100) / 100).toFixed(2);
      payable        =  Math.round(amount);

      var apayable1   = parseFloat(ip_charge) - parseFloat(grand1);
      amount1         = (Math.round(apayable1 * 100) / 100).toFixed(2);
      payable1        = Math.round(amount1);

      $('#apayable').val(payable1);
      $('#apayable1').html(payable);
      $('#sum').val(payable1);
      $('#sum1').html(payable);
      $('#sum2').val(payable);

      roundoff       =  parseFloat(payable) - parseFloat(amount); 
      roundoff       =  (Math.round(roundoff * 100) / 100).toFixed(2);

      roundoffo       =  parseFloat(payable1) - parseFloat(amount1); 
      roundoffo       =  (Math.round(roundoffo * 100) / 100).toFixed(2);
      $('#roundoff').val(roundoff);
      $('#roundoff1').html(roundoff);
    } 
  }//5th ajax succes
  }) //5th ajax finish


  }// 4th ajax succes
  })//4th ajax finish


  }// 4th ajax succes
  })//4th ajax finish


  }// 4th ajax succes
  })//4th ajax finish


  }// 3rd ajax succes
  }) //3rd ajax finish


  }// 2nd ajax succes
  }) //2 ajaxfinished


  } // 1st ajax succes
  })


    // grand           =   (Math.round(grand * 100) / 100).toFixed(2);

    // $('#lab_mrp').val(grand);
    // $('#lab_mrp1').html(grand);
  });


   $('.button').click(function() {

    var slno5 = 1;
    var productid       = $('#productid').val();
    var product         = $('#product').val();
    var product_price   = $('#product_price').val();
    var qty             = $('#product_qty').val();
    var total           = parseFloat(product_price) * parseFloat(qty);
    
     var newhead5     = '<tr><th>'+slno1+'</th><th colspan="9">Others</th></tr> <tr><th></th><th>Sl.no.</th><th colspan="4">Patricular</th><th colspan="2">Unit Price</th><th colspan="1">Qty</th><th>Amount</th></tr>';

     $('#item_table tr:last').after(newhead5);

    var newrow5      = '<tr><td></td><td>'+slno5+'</td><td colspan="4"><input type="hidden" value="'+productid+'" name="ved_itemid[]"><input class="form-control input-sm" type="hidden" value="'+product+'" name="ved_item[]"><b>' + product + '</b></td><td colspan="2"><input class="form-control input-sm" type="hidden" value="'+product_price+'" name="ved_price[]"><b>' + product_price + '</b></td><td colspan="1"><input class="form-control input-sm" type="hidden" value="'+qty+'" name="ved_qty[]"><b>' + qty + '</b></td><td><input class="gtotal3" type="hidden" value="'+total+'" name="ved_total[]"><b>Rs. </b><label>'+total+'</label></td><td><button class="btn btn-xs btn-danger dlte"><i class="fa fa-times-circle-o"></i></button></td></tr>';

    var ip_charge   = $('#lab_mrp').val();
    var lab_mrp     = $('#lab_mrp1').html();
    var advance     = $('#advance').val();

    lab_mrp         = parseFloat(lab_mrp) + parseFloat(total);
    lab_mrp         = (Math.round(lab_mrp * 100) / 100).toFixed(2);
    ip_charge       = parseFloat(ip_charge) + parseFloat(total);
    ip_charge       = (Math.round(ip_charge * 100) / 100).toFixed(2);

    $('#lab_mrp').val(ip_charge);
    $('#lab_mrp1').html(lab_mrp);

      var apayable   = parseFloat(lab_mrp) - parseFloat(advance);
      amount         =  (Math.round(apayable * 100) / 100).toFixed(2);

      var apayable1   =  parseFloat(ip_charge) - parseFloat(advance);
      amount1         =  (Math.round(apayable1 * 100) / 100).toFixed(2);

      $('#apayable').val(amount1);
      $('#apayable1').html(amount);

      var pDiscount         =  $('#discountp').val();

      var pDiscount1        =  parseFloat(apayable) * parseFloat(pDiscount)/100;
      var sum               =  parseFloat(apayable) - parseFloat(pDiscount1);
      sum                   =  (Math.round(sum * 100) / 100).toFixed(2);

      pDiscount1            =  (Math.round(pDiscount1 * 100) / 100).toFixed(2);


      var pDiscounto1       =  parseFloat(apayable1) * parseFloat(pDiscount)/100;
      var sum1              =  parseFloat(apayable1) - parseFloat(pDiscounto1);
      sum1                  =  (Math.round(sum1 * 100) / 100).toFixed(2);

      pDiscounto1           =  (Math.round(pDiscounto1 * 100) / 100).toFixed(2);

      gtotal  = Math.round(sum);
      gtotalo = Math.round(sum1);

      $('#discountp').val(pDiscount);
      $('#discountp1').html(pDiscount);
      $('#discounta').val(pDiscounto1);
      $('#discounta1').html(pDiscount1);

      $('#sum').val(gtotalo);
      $('#sum1').html(gtotal);
      $('#sum2').val(gtotal);

    var roundoff    =   Math.round(sum) - parseFloat(sum);
    roundoff        =  (Math.round(roundoff * 100) / 100).toFixed(2);

    var roundoffo   =   Math.round(sum1) - parseFloat(sum1);
    roundoffo       =  (Math.round(roundoffo * 100) / 100).toFixed(2);

    $('#roundoff').val(roundoffo);
    $('#roundoff1').html(roundoff);

    $('#item_table tr:last').after(newrow5);
    slno1++;
    document.getElementById('productid').value = "";
    document.getElementById('product').value = "";
    document.getElementById('product_price').value = "";

});

  });
  </script>

  <script>
  $(document).ready(function() {

  
});
    </script>

  <script>
  $(document).ready(function() {
    $('input[type="checkbox"]'). click(function(){
    if($("#pending"). prop("checked") == true){
    $("#pending1").val("True");
    }
    else{
    $("#pending1").val("False");
    } 
   });

    var gtotal7 = 0;
    var gtotal6 = 0;
    var gtotal5 = 0;
    var gtotal4 = 0;
    var gtotal3 = 0;
    var gtotal2 = 0;
    var gtotal1 = 0;
    var gtotal = 0;
$(document).on('click','button.dlte', function() {
    var $row    = $(this).closest("tr");    // Find the row

    var r = confirm("Are you sure want to delete!");
    if (r == true) {
    $(this).closest('tr').remove();

    $('.gtotal6').each(function(){
        gtotal6 += parseFloat(this.value);
    });
    $('.gtotal7').each(function(){
        gtotal7 += parseFloat(this.value);
    });
    $('.gtotal5').each(function(){
        gtotal5 += parseFloat(this.value);
    });
    $('.gtotal4').each(function(){
        gtotal4 += parseFloat(this.value);
    });

    $('.gtotal3').each(function(){
        gtotal3 += parseFloat(this.value);
    });
    $('.gtotal1').each(function(){
        gtotal1 += parseFloat(this.value);
    });
    $('.gtotal2').each(function(){
        gtotal2 += parseFloat(this.value);
    });
    $('.gtotal').each(function(){
        gtotal += parseFloat(this.value);
    });

    lab_mrp        =  parseFloat(gtotal) + parseFloat(gtotal3);
    lab_mrp1       =  parseFloat(gtotal) + parseFloat(gtotal1) + parseFloat(gtotal3) + parseFloat(gtotal4) + parseFloat(gtotal6) + parseFloat(gtotal7) - parseFloat(gtotal5);
    lab_mrp        =  (Math.round(lab_mrp * 100) / 100).toFixed(2);
    lab_mrp1       =  (Math.round(lab_mrp1 * 100) / 100).toFixed(2);

    gtotal2        =  (Math.round(gtotal2 * 100) / 100).toFixed(2);

    amount         =  parseFloat(lab_mrp) - parseFloat(gtotal2);
    amount         =  (Math.round(amount * 100) / 100).toFixed(2);
    payable        =  Math.round(amount);

    amount1         =  parseFloat(lab_mrp1) - parseFloat(gtotal2);
    amount1         =  (Math.round(amount1 * 100) / 100).toFixed(2);
    payable1        =  Math.round(amount1);


    $('#lab_mrp').val(lab_mrp);
    $('#lab_mrp1').html(lab_mrp1);
    $('#advance').val(gtotal2);
    $('#advance1').html(gtotal2);

    $('#apayable').val(payable);
    $('#apayable1').html(payable1);
            alert(payable1)

    var pDiscount         =  $('#discountp').val();

    var pDiscount1        =  parseFloat(payable1) * parseFloat(pDiscount)/100;
    var sum               =  parseFloat(payable1) - parseFloat(pDiscount1);
    sum                   =  (Math.round(sum * 100) / 100).toFixed(2);
    pDiscount1            =  (Math.round(pDiscount1 * 100) / 100).toFixed(2);

    var pDiscounto1       =  parseFloat(payable) * parseFloat(pDiscount)/100;
    var sum1              =  parseFloat(payable) - parseFloat(pDiscounto1);
    sum1                  =  (Math.round(sum1 * 100) / 100).toFixed(2);
    pDiscounto1           =  (Math.round(pDiscounto1 * 100) / 100).toFixed(2);

    grand_total  = Math.round(sum);
    grand_totalo = Math.round(sum1);

    $('#discountp').val(pDiscount);
    $('#discountp1').html(pDiscount);
    $('#discounta').val(pDiscounto1);
    $('#discounta1').html(pDiscount1);

    $('#sum').val(grand_totalo);
    $('#sum1').html(grand_total);
    $('#sum2').val(grand_total);

    var roundoff   =   Math.round(sum) - parseFloat(sum);
    roundoff       =  (Math.round(roundoff * 100) / 100).toFixed(2);

    var roundoffo   =   Math.round(sum1) - parseFloat(sum1);
    roundoffo       =  (Math.round(roundoffo * 100) / 100).toFixed(2);
    $('#roundoff').val(roundoffo);
    $('#roundoff1').html(roundoff);
    } 
    });


  });
  </script>


  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#customer_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>

  <!-- Discount Add -->
  <script>
  $(document).ready(function() {
    $( "#dialog" ).dialog({
        modal: true,
        bgiframe: true,
        autoOpen: false,
      show: {
        effect: "blind",
        duration: 100
      },
      hide: {
        effect: "explode",
        duration: 500
      }
    });

  $('.alldisc').click(function() {
      // $( "#dialog" ).dialog( "open" );
      var user_pswd         = $('#user_pswd').val();
      var user_pswd         = ""+user_pswd+"";
      bootpopup.prompt("Password","password",
    function(data) { if(JSON.stringify(data)==JSON.stringify(user_pswd)){
      
    var sum               =  0;
    var pDiscount         =  $('#pDiscount').val();
    var apayable          =  $('#apayable1').html();
    var apayable1         =  $('#apayable').val();

    var pDiscount1        =  parseFloat(apayable) * parseFloat(pDiscount)/100;
    var sum               =  parseFloat(apayable) - parseFloat(pDiscount1);
    sum                   =  (Math.round(sum * 100) / 100).toFixed(2);
    pDiscount1            =   (Math.round(pDiscount1 * 100) / 100).toFixed(2);

    var pDiscounto1       =  parseFloat(apayable1) * parseFloat(pDiscount)/100;
    var sum1              =  parseFloat(apayable1) - parseFloat(pDiscounto1);
    sum1                  =  (Math.round(sum1 * 100) / 100).toFixed(2);
    pDiscounto1           =  (Math.round(pDiscounto1 * 100) / 100).toFixed(2);

    gtotal  = Math.round(sum);
    gtotalo = Math.round(sum1);

    $('#discountp').val(pDiscount);
    $('#discountp1').html(pDiscount);
    $('#discounta').val(pDiscounto1);
    $('#discounta1').html(pDiscount1);

    $('#sum').val(gtotalo);
    $('#sum1').html(gtotal);
    $('#sum2').val(gtotal);

    var roundoff     =   Math.round(sum) - parseFloat(sum);
    roundoff         =   (Math.round(roundoff * 100) / 100).toFixed(2);
    var roundoffo    =   Math.round(sum1) - parseFloat(sum1);
    roundoffo        =   (Math.round(roundoffo * 100) / 100).toFixed(2);
    $('#roundoff').val(roundoffo);
    $('#roundoff1').html(roundoff);

    } 
else
{
  alert("Invalid Password!");
}
  }
);
  });
  });
  </script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <div id="dialog" title="Confirm Password">
    <input type="password" size="25" />
    </div>
   <input stle="text-color:#000" type="hidden" value="<?=$admin_pswd?>" id="user_pswd">
    <section class="content-header">
      <h1>
       Discharge
       <small>New Discharge </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."discharge"; ?>">Discharge</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-success pull-right  view-btn-create" href="<?php echo base_url();?>index.php/discharge">
        <i class="fa fa-reply-all"></i> Back
      </a>
    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>

      <form id="post_form">
     <div class="box-body">
      <div class="row"> 
       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">Voucher No.<sup></sup></label>
          <input class="form-control input-sm" readonly type="text" id="ve_vno" name="ve_vno" tabindex="1" value="<?=$bill_no?>"> 
          <input class="form-control" type="hidden" name="user_type" value="<?=$user_type?>">              
        </div>
        <div class="col-md-2">
          <label  class="control-label">Date<sup></sup></label>
          <input class="form-control validate[required] input-sm"  tabindex="2" data-prompt-position="topRight:150" type="text" name="ve_date" id="datepicker" value="<?=date("Y-m-d") ?>">                
        </div>
        <div class="bootstrap-timepicker">
                <div class="form-group col-md-2">
                  <label>Time:</label>
                  <div class="input-group">
                    <input type="text" name="dis_time" class="form-control timepicker">
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              </div>
        <div class="col-md-2"></div>
         <div class="col-md-2">
         <label  class="control-label">IP No.<sup></sup></label>
         <input class="form-control input-sm" data-prompt-position="topLeft:190" tabindex="5" type="text" name="ve_customer" id="ve_customer"></div>

          <div class="col-md-2">
         <label  class="control-label">MRD<sup></sup></label>
         <input class="form-control validate[required] input-sm" data-prompt-position="topLeft:190" tabindex="5" type="text" name="ve_mrd" id="ve_mrd"></div>
               </div>
      <div class="col-md-12">
      <br>
       <div class="col-md-4">
         <label  class="control-label">Patient's Name<sup></sup></label>
         <input class="form-control validate[required] input-sm"  data-prompt-position="bottomLeft:190"  tabindex="6" type="text" id="bk_name">
         <input type="hidden" name="ve_supplier" value="16">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Phone No.<sup></sup></label>
         <input class="form-control validate[required] input-sm" data-prompt-position="topLeft:190" tabindex="7" type="text" id="bk_phone">
       </div>
       <div class="col-md-2"></div>
         <div class="col-md-2">
         <label  class="control-label">Department<sup></sup></label>
         <input class="form-control validate[required] input-sm" data-prompt-position="bottomLeft:190" tabindex="7" type="text" id="ip_department"> 
       </div>
        <div class="col-md-2">
         <label  class="control-label">Doctor<sup></sup></label>
         <input class="form-control validate[required] input-sm" data-prompt-position="bottomLeft:190" tabindex="8" type="text" id="ip_doctor">  
       </div>
     </div>
     <div class="col-md-12">
      <br>
      <div class="col-md-4">
        <div class="form-group required">
          <label  class="control-label">Item<sup></sup></label>
          <input class="form-control input-sm" tabindex="9" type="text" id="product">
          <input class="form-control" type="hidden" name="productid" id="productid">
        </div> </div>
        
          <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Unit Price<sup></sup></label>
              <input class="form-control input-sm" id="product_price" tabindex="10"  type="text" name="product_price">  
               </div> </div>

               <div class="col-md-1">
            <div class="form-group required">
              <label  class="control-label">Qty<sup></sup></label>
              <input class="form-control input-sm" id="product_qty" tabindex="10"  type="text" name="product_qty" value="1">  
               </div> </div>

                <div class="col-md-2">
        <label  class="control-label">Pending<sup></sup></label><br>
         <input id="pending" type="checkbox">
        <input id="pending1" type="hidden" name="pending" value="False">
       </div>



            </div>
          </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-md-12">
                <div>
                  <a class="button btn btn-primary" tabindex="11"  >Add to List</a>
                  <input class="btn-large btn-default btn"  tabindex="12" type="reset" value="Reset">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                      <tr>
                        <td><b>DISCOUNT(%)</b></td>
                        <td>
                          <input type="text" name="pDiscount" required="required" id="pDiscount" class="form-control" value="0" />
                        </td>          
                        <td><button class="btn btn-info alldisc" type="button">Add</button></td>
                      </tr>
                  </table>


                    <table class="table table-striped table-bordered table-hover gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Sl No.</th>
                        <th colspan="8">Particulars</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                  <tbody> 

                  </tbody>
                  </table> 
                  <table class="table table-bordered">
                    <tr><th width="90%">Total</th>
                      <th><input type="hidden" name="lab_mrp" readonly id="lab_mrp" value="0">Rs. <label id="lab_mrp1">0</label>
                      </th></tr>
                      <tr><th width="90%">Advance Paid</th>
                      <th><input type="hidden" name="advance" readonly id="advance" value="0">Rs. <label id="advance1">0</label></th></tr>
                      <tr><th width="90%">Total Amount</th>
                      <th><input type="hidden" name="apayable" readonly id="apayable" value="0">Rs. <label id="apayable1">0</label></th></tr>
                       <tr><th width="67.5%">Discount</th><th>
                      <input type="hidden" readonly name="discountp" id="discountp" value="0"><input type="hidden" readonly name="discounta" id="discounta" value="0">Rs. <label id="discounta1"></label> (<label id="discountp1">0</label> %)</th></tr>
                      <tr><th width="90%">Amount Payable</th>
                      <th><input type="hidden" name="sum" readonly id="sum" value="0">
                        <input type="hidden" name="sum2" readonly id="sum2" value="0">
                        Rs. <label id="sum1">0</label></th></tr>
                      <tr><th width="90%">Round Off</th>
                      <th><input type="hidden" name="roundoff" readonly id="roundoff" value="0">Rs. <label id="roundoff1">0</label></th></tr>
                    </table>

                    <a href="#" class="btn btn-success" onclick="save();" accesskey="s" title="short key-ALT+S"><i class="fa fa-floppy-o"></i> Save</a>
                  <input type="hidden" id="Printid"> 
                    <a href="#" class="btn btn-warning" onclick="save_print();" accesskey="p" title="short key-ALT+P" ><i class="fa fa-print"></i> Save & Print</a> 
                
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</form>
</div>
</section>

</section><!-- /.right-side -->

<script>
function save(){

      var postData = $("#post_form").serializeArray();
      var formURL  = "<?= base_url() ?>discharge/dischargeAdd";

      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          // alert("Success");
          $('#Printid').val(data);
           window.location = "<?php echo base_url();?>index.php/discharge/add";
        }
      });
}
function save_print(){

          
      var postData = $("#post_form").serializeArray();
      var formURL  = "<?= base_url() ?>discharge/dischargeAdd";

      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          // alert(data);
           window.location = "<?php echo base_url();?>index.php/discharge/getPrint?Printid="+data;
        }
      });

}
</script>

<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });
 $('.timepicker').timepicker({
      showInputs: false,
      minuteStep: 1,
    });


 </script>
<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>