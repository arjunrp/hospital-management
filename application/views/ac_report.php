<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
<?php 
$this->load->view('product_script');
?>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
  $('.tbsal').hide();
  $('.tbbal').hide();
  $('.tbret').hide();
  $('.tbpur').hide();
var r_type= "<?= $r_type ?>";
if(r_type==1)
{
  $('.tbsal').hide();
  $('.tbbal').hide();
  $('.tbret').hide();
  $('.tbpur').show();

}
else if(r_type==2)
{
  $('.tbsal').show();
  $('.tbret').hide();
  $('.tbbal').hide();
  $('.tbpur').show();
}
else if(r_type==3)
{
  $('.tbsal').hide();
  $('.tbpur').hide();
  $('.tbbal').hide();
  $('.tbret').show();
}
else if(r_type==0)
{
  $('.tbsal').hide();
  $('.tbpur').hide();
  $('.tbret').hide();
  $('.tbbal').show();
}
  oTable = $('#patient_table').dataTable({
    "aaSorting": [[ 0, "dec" ]],
    "bJQueryUI": true,
    "sPaginationType": "full_numbers",
    "iDisplayLength": 10,
    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
  });
});
</script>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     <?php echo $page_title; ?>
    </h1>
     <ol class="breadcrumb">
      <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
      
      <li class="active"><?php echo $page_title; ?></li>
    </ol>&nbsp;&nbsp;
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <?php echo form_open_multipart($action) ?>
                 <div class="form-group">
                   <div class="col-md-2">
                    <label>From Date</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker" name="fdate">
                </div>
              </div>
                   <div class="col-md-2">
                    <label>To Date</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker2" name="tdate">
                </div>
                <!-- /.input group -->
              </div>

                   <div class="col-md-2" id="d_brand">
                    <label>Brand</label>
                    <select class="form-control" name="p_brand" id="p_brand">
                      <option value="0">Select a Brand</option>
                      <?php foreach ($brands as $brand) {
                       ?> <option value="<?=$brand['id'] ?>"><?=$brand['brand'] ?></option> <?php
                      } ?>
                  </select>      
                </div>
                <div class="col-md-2" id="d_category">
                  <label>Category</label>
                    <select class="form-control" name="p_category" id="p_category">
                      <option value="0">Select a Category</option>
                  </select>      
                </div>
                <div class="col-md-2" id="d_scategory">
                   <label>Sub-Category</label>
                    <select class="form-control" name="p_scategory" id="p_scategory">
                      <option value="0">Select a Sub-Category</option>
                  </select>      
                </div>

                <div class="col-md-2" id="d_group">
                  <label>Group</label>
                    <select class="form-control" name="p_group" id="p_group">
                      <option value="0">Select a Group</option>
                      <?php foreach ($groups as $group) {
                       ?> <option value="<?=$group['id'] ?>"><?=$group['groups'] ?></option> <?php
                      } ?>
                  </select>      
                </div>
                <div class="col-md-2" id="d_type">
                   <label>Type</label>
                    <select class="form-control" name="p_type" id="p_type">
                      <option value="0">Select a Type</option>
                       <?php foreach ($types as $type) {
                       ?> <option value="<?=$type['id'] ?>"><?=$type['type'] ?></option> <?php
                      } ?>
                  </select>      
                </div>
                <div class="col-md-2">
                  <label>Product</label>
                    <select class="form-control" name="product" id="product">
                      <option value=0>Select a Product</option>
                  </select>      
                </div>
                <div class="col-md-2">
                  <label>Report Type</label>
                    <select class="form-control" name="r_type" id="r_type">
                      <option value="">Select Type</option>
                      <option value=1>Purchase</option>
                      <option value=2>Sale</option>
                      <option value=0>Balance Sheet</option>
                      <option value=3>Return</option>
                  </select>      
                </div>
                <div class="col-md-2">
                  <label>&emsp;</label><br>
                 <input type="submit" class="btn btn-success" value="Apply">    
                </div>
              </div>

<?php echo form_close(); ?>  
              
            <div class="col-md-12"><hr></div>
            
            <div class="row">
              <div class="col-md-12">
                <div class="box-header tbret tbpur tbsal tbbal">
                  <center><h3 class="box-title"><b><?php if($r_type==1) { echo "Purchase"; } else if($r_type==2) { echo "Sales"; } else if($r_type==0) { echo "Balance Sheet"; } else if($r_type==3) { echo "Return"; } ?></b> Report from <b><?=date("d-m-Y",strtotime($fdate)) ?></b>&nbsp;<b></b> to <b><?=date("d-m-Y",strtotime($tdate)) ?></b> of <b><?php if($product==0) { echo "All Products"; } else { echo $products;  } ?></b></h3></center>
                </div>
                <div class="col-md-12"><hr></div>
                <?php echo form_open_multipart($action1) ?>
                 <input type="hidden" name="fdate1" value="<?=$fdate ?>">
                  <input type="hidden" name="tdate1" value="<?=$tdate ?>">
                  <input type="hidden" name="product1" value="<?=$product ?>">
                  <input type="hidden" name="r_type1" value="<?=$r_type ?>"> 
                <center>     
                <button type="submit" name="submit" class="btn btn-primary tbret tbpur tbsal tbbal" >Print</button>
              </center>
              <?php echo form_close(); ?>  

            <div class="row">
              
              <div class="col-md-12"><br>
                  <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-12">
                      <table id="" class="table table-condensed dataTable no-footer tbret">
                       <thead><b>
                        <tr><th colspan="7" style="text-align:center">Sale Return</th></tr>
                          <tr>
                          <th>Date</th>
                          <th>Product</th>
                          <th>Unit Price</th>
                          <th>Qty</th>
                          <th>Sale Amount</th>
                          <th>GST</th>
                          <th>Amount</th>
                        </tr>
                        </b>
                      </thead><?php if($r_type==3){
                      echo $sroutput;  }
                        ?>
             </table>
                      <table id="" class="table table-condensed dataTable no-footer tbret">
                       <thead><b>
                        <tr><th colspan="6" style="text-align:center">Purchase Return</th></tr>
                          <tr>
                          <th>Date</th>
                          <th>Product</th>
                          <th>Unit Price</th>
                          <th>Qty</th>
                          <th>Amount</th>
                        </tr>
                        </b>
                      </thead>
                      <?php if($r_type==3){
                      echo $proutput;  }
                        ?>
             </table>

                      <table id="" class="table table-condensed dataTable no-footer">
                       <thead><b>
                        <tr>
                          <th class="tbpur">Date</th>
                          <th class="tbpur">Product</th>
                          <th class="tbpur">Unit Price</th>
                          <th class="tbpur">Qty</th>
                          <th class="tbpur">Discount</th>
                          <th class="tbsal">Sale Amount</th>
                          <th class="tbsal">GST</th>
                          <th class="tbpur">Amount</th>
                        </tr>
                        </b>
                      </thead>
                      <tbody>
                      <?php if($r_type==1 || $r_type==2) { echo $output; } ?>
                      </tbody>
                      <?php
                  $sumgsttot=0;
                  $sumam=0;
                  $sumdi=0;
                  $sumay=0;
                  $sumad=0;
                  $sumab=0;                
                  $sumgst=0;
                  $sumrf=0;
                  $sumtot=0;
                  $sumtot1=0;
                  $sumgst1=0;

                  if (is_array($output1) || is_object($output1))
                  {
                  foreach($output1 as $tsum)
                  {
                   
                    $sumgsttot  = $sumgsttot + $tsum['sumgsttot'];
                    $sumam  = $sumam + $tsum['sumam'];
                    $sumdi  = $sumdi + $tsum['sumdi'];
                    $sumay  = $sumay + $tsum['sumay'];
                    $sumad  = $sumad + $tsum['sumad'];
                    $sumab  = $sumab + $tsum['sumab'];
                    
                       if($r_type==2) {     
                        $sumgst1  = $sumgst1 + $tsum['sumgst1'];
                        $sumtot1 = $sumtot1 + $tsum['sumtot1'];       
                    $sumgst = $sumgst + $tsum['sumgst'];
                    $sumtot = $sumtot + $tsum['sumtot'];
                    $sumrf  = $sumrf + $tsum['sumrf']; }
                  } } 
              ?>
              <thead class="tbpur">
               <?php if($product==0){ ?>
                <tr<?php if($r_type==3 || $r_type==0) { ?> style="display:none" <?php } ?>><th width="85%">Overall <?php if($r_type==1) { echo "Purchase"; } else if($r_type==2) { echo "Sale"; }?> Discount</th><th><?="Rs. ".$sumdi ?></th></tr>
                <tr<?php if($r_type!=2) { ?> style="display:none" <?php } ?>><th width="85%">Total Round Off</th><th><?="Rs. ".$sumrf ?></th></tr>
                <tr<?php if($r_type==3 || $r_type==0) { ?> style="display:none" <?php } ?>><th width="85%">Total Amount Payable</th><th><?php echo "Rs. "; echo $sumay+$sumrf; ?></th></tr>
                <tr<?php if($r_type==3 || $r_type==0) { ?> style="display:none" <?php } ?>><th width="85%">Total Amount Paid</th><th><?="Rs. ".$sumad ?></th></tr>
                <tr<?php if($r_type==3 || $r_type==0) { ?> style="display:none" <?php } ?>><th width="85%">Balance Amount</th><th><?="Rs. ".$sumab ?></th></tr>
                <?php } ?>
              </thead>
             </table>
           </div>

             <?php
                       if($r_type==0) {
                      foreach ($output as $output):  
                        ?>
             <div class="col-md-12">
             <table class="table table-condensed dataTable no-footer tbbal">
              <thead>
              <tr>
                <th colspan=2 style="text-align: center;"><?= date('d-m-Y', strtotime($output['psdate'])) ?></th></tr>
                <tr><td colspan=2 width="50%" style="text-align: center;"><b>Opening Balance : </b> <?= "Rs. ".$output['openingBalance'] ?></td></tr>
            </thead>
          </table>
                  <table class="table table-condensed dataTable no-footer">
                    <tr><th colspan=7 style="text-align:center"><u>Sale & Sale Return</u></th></tr>
                    <tr><th>Item</th><th>Unit Price</th><th>Quantity</th><th>Discount</th><th>Total</th><th>GST</th><th>Sub Total</th></tr>

                    <?php $sales_total=0;$sales_gst=0;$sales_gtotal=0; 
                      if(!empty($output['sales_sum'])) {
                     ?>
                         <?php foreach ($output['sales_sum'] as $output2) {  ?>
                          <tr>
                         <td><?= $output2['sale_item']." - [Sales]" ?> </td>
                         <td><?= "Rs.".$output2['sale_unit_price'] ?> </td>
                         <td><?= $output2['sale_item_qty']." ".$output2['sale_item_unit'] ?> </td>
                         <td><?= "Rs.".$output2['sale_discount'] ?> </td>
                         <td><?= "Rs.".$output2['sale_total'] ?> </td>
                         <td><?= "Rs.".$output2['gst_amount']. " (".$output2['gst']." %)" ?> </td>
                         <td><?= "Rs.".$output2['samount'] ?> </td>
                         <?php $sales_total   = $sales_total  + $output2['sale_total'];
                               $sales_gst     = $sales_gst    + $output2['gst_amount'];
                               $sales_gtotal  = $sales_gtotal + $output2['samount']; ?>
                       </tr>
                       <?php } } ?>
                        <?php if(!empty($output['sreturn_sum'])) { ?>
                         <?php foreach ($output['sreturn_sum'] as $output2) {  ?>
                          <tr>
                         <td><?=$output2['preturn_details_item']." - [Return]" ?> </td>
                         <td><?="Rs.".$output2['preturn_details_price'] ?> </td>
                         <td><?=$output2['preturn_details_qtys']." ".$output2['preturn_details_unit'] ?> </td>
                         <td><?="N/A" ?> </td>
                         <td><?="Rs.".$output2['preturn_details_sprices'] ?> </td>
                         <td><?="Rs.".$output2['preturn_details_gstamnts']. " (".$output2['preturn_details_gst']." %)" ?> </td>
                         <td><?="Rs.".$output2['sramount'] ?> </td>
                       <?php   $sales_total     = $sales_total  - $output2['preturn_details_sprices'];
                               $sales_gst     = $sales_gst    - $output2['preturn_details_gstamnts'];
                               $sales_gtotal  = $sales_gtotal - $output2['sramount']; ?>
                       </tr>
                       <?php } } ?> 
                       <tr><th colspan="4" style="border-top:1px solid black">Grand Total :</th><th style="border-top:1px solid black"><?= "Rs. ".$sales_total?></th><th style="border-top:1px solid black"><?="Rs. ".$sales_gst?></th><th style="border-top:1px solid black"><?="Rs. ".$sales_gtotal?></th></tr>
                  </table>
                  <table class="table table-condensed dataTable no-footer">
                    <tr>
                      <td>
                        <table class="table table-condensed dataTable no-footer">
                    <tr><th colspan=5 style="text-align:center"><u>Purchase & Purchase Return</u></th></tr>
                    <tr><th>Item</th><th>Unit Price</th><th>Quantity</th><th>Discount</th><th>Sub Total</th></tr>
                    <?php $purchases_total=0;
                       if(!empty($output['purchase_sum'])) { ?>
                         <?php foreach ($output['purchase_sum'] as $output2) {  ?>
                          <tr>
                         <td><?=$output2['purchase_item']." - [Purchase]" ?> </td>
                         <td><?="Rs.".$output2['purchase_unit_price'] ?> </td>
                         <td><?=$output2['purchase_product_qty']." ".$output2['purchase_product_unit'] ?> </td>
                         <td><?= "Rs.".$output2['purchase_discount'] ?> </td>
                         <td><?= "Rs.".$output2['pamount'] ?> </td>
                       </tr>
                       <?php $purchases_total   =   $purchases_total+$output2['pamount']; } } ?>
                       <?php if(!empty($output['preturn_sum'])) { ?>
                         <?php foreach ($output['preturn_sum'] as $output2) {  ?>
                          <tr>
                         <td><?=$output2['preturn_details_item']." - [Return]" ?> </td>
                         <td><?="Rs.".$output2['preturn_details_price'] ?> </td>
                         <td><?=$output2['preturn_details_qtyp']." ".$output2['preturn_details_unit'] ?> </td>
                         <td><?="N/A" ?> </td>
                         <td><?="Rs.".$output2['pramount'] ?> </td>
                       </tr>
                       <?php  $purchases_total   =   $purchases_total - $output2['pramount']; } } ?> 
                        
                       <tr><th colspan="4" style="border-top:1px solid black">Grand Total :</th><th style="border-top:1px solid black"><?="Rs. ".$purchases_total?></th></tr>
                  </table>
                      </td>
                      <td>
                        <table class="table table-condensed dataTable no-footer">
                    <tr><th colspan=2 style="text-align:center">Expense</th></tr>
                    <tr><th>Category</th><th>Amount</th></tr>
                    <?php $expence_total=0; 
                    if(!empty($output['expense'])) { ?>
                         <?php foreach ($output['expense'] as $outputs) {  ?>
                          <tr>
                         <td><?= $outputs['category'] ?> </td>
                         <td><?= "Rs.".$outputs['amount'] ?> </td>
                       </tr>
                       <?php $expence_total   =   $expence_total + $outputs['amount']; } } ?>
                        
                       <tr><th>Grand Total :</th><th><?="Rs. ".$expence_total?></th></tr>
                  </table>
                      </td>
                    </tr>
                  <table class="table table-condensed dataTable no-footer">
                    <thead>
               <tr><td colspan=2 style="text-align: center;"><b>Closing Balance : </b> <?= "Rs. ".$output['closingBalance'] ?></td></tr>
              <tr><td><br></td></tr>
            </thead>
             </table>
           </div>
           <?php endforeach; } ?>             
      
        </div>
      </div>
    </div>

  </div>
</div>

</div>
</div>  

</div></div></div>
</div></div></div>
</section></div>


<script>
 $('#datepicker').datepicker({
      autoclose: true
    });</script>


    <script>
 $('#datepicker2').datepicker({
      autoclose: true
    });</script>



