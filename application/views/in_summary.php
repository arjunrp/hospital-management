<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<?php 
$this->load->view('product_script');
?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {

  oTable = $('#patient_table').dataTable({
    "aaSorting": [[ 0, "dec" ]],
    "bJQueryUI": true,
    "sPaginationType": "full_numbers",
    "iDisplayLength": 10,
    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
  });
});
</script>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     <?php echo $page_title; ?>
    </h1>
     <ol class="breadcrumb">
      <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
      
      <li class="active"><?php echo $page_title; ?></li>
    </ol>&nbsp;&nbsp;
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
              <?php echo form_open_multipart($action) ?>
                 <div class="form-group">
                  <div class="col-md-12">
                   <div class="col-md-2">
                    <label>From Date</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right input-sm" id="datepicker" name="fdate">
                </div>
              </div>
                   <div class="col-md-2">
                    <label>To Date</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right input-sm" id="datepicker2" name="tdate">
                </div>
                <!-- /.input group -->
              </div>

                   <div class="col-md-3" id="d_brand">
                    <label>Brand</label>
                    <select class="form-control input-sm" name="p_brand" id="p_brand">
                      <option value="0">Select a Brand</option>
                      <?php foreach ($brands as $brand) {
                       ?> <option value="<?=$brand['br_id'] ?>"><?=$brand['br_brand'] ?></option> <?php
                      } ?>
                  </select>      
                </div>
                <div class="col-md-2" id="d_type">
                   <label>Type</label>
                    <select class="form-control input-sm" name="p_type" id="p_type">
                      <option value="0">---Select a Type--</option>
                      <option value="m">---Medical Item---</option>
                      <option value="g">---General Item---</option>
                      
                  </select>      
                </div>
                <div class="col-md-3">
                  <label>Product</label>
                    <select class="form-control input-sm" name="product" id="product">
                      <option value=0>Select a Product</option>
                  </select>      
                </div>
              </div>

              <div class="col-md-12">
                <br>
                <div class="col-md-3" id="d_brand">
                    <label>Department</label>
                    <select class="form-control input-sm" name="p_dept" id="p_brand">
                      <option value="">Select Department</option>
                      <?php foreach ($departments as $department) {
                       ?> <option value="<?=$department['dp_id'] ?>"><?=$department['dp_department'] ?></option> <?php
                      } ?>
                  </select>      
                </div>
<!-- 
                <div class="col-md-2">
                  <label>Report Type</label>
                    <select class="form-control input-sm" name="r_type" id="r_type">
                      <option value=4>Select Type</option>
                      <option value=0>All</option>
                      <option value=1>Purchase</option>
                      <option value=2>Sale</option>
                      <option value=3>Purchase Return</option>
                      <option value=3>Sale Return</option>
                      <option value=5>Consumption</option>
                  </select>      
                </div> -->
                <div class="col-md-2">
                  <label>&emsp;</label><br>
                 <input type="submit" class="btn btn-success" value="Apply">    
                </div>
              </div>
            
            
            <div class="col-md-12"><hr></div>
<?php echo form_close(); ?>       
            
            <div class="row">
              <div class="col-md-12">
                <div class="box-header tbhide tbs3 tbs2 tbs4 tbs5">
                  <center><h3 class="box-title">Stocks Report from <b><?=date("d-m-Y",strtotime($fdate)) ?></b>&nbsp;<b></b> to <b><?=date("d-m-Y",strtotime($tdate)) ?></b> of <b><?php if($product==0) { echo "All Products"; } else { echo $products;  } ?></b></h3></center>
                </div>
                <div class="col-md-12"><hr></div>
                 <?php echo form_open_multipart($action1) ?>
                  <input type="hidden" name="fdate1" value="<?=$fdate ?>">
                  <input type="hidden" name="tdate1" value="<?=$tdate ?>">
                  <input type="hidden" name="product1" value="<?=$product ?>">
                  <input type="hidden" name="p_brand1" value="<?=$p_brand ?>">   
                <center>     
                <button type="submit" name="submit" class="btn btn-primary tbhide tbs3 tbs2 tbs4 tbs5" >Print</button>
              </center>
              <?php echo form_close(); ?> 

            <div class="row">
              
              <div class="col-md-12"><br>
                  <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-12">
                      <table id="patient_table" class="table table-condensed dataTable no-footer">
                       <thead><b>

                        <tr class="tbhide">
                          <th>Date</th>
                          <th>Item</th>
                          <th class="tball">Opening Stock</th>
                          <th class="tbpur">Purchase</th>
                          <th class="tbret">Purchase Return</th>
                          <th class="tbsal">Sale</th>
                          <th class="tbret">Sale Return</th>
                          <th class="tbret">Stock Trasnfer From</th>
                          <th class="tbret">Stock Trasnfer To</th>
                          <th class="tbcon">Consumption</th>
                          <th class="tball">Closing Stock</th></tr>
                        </b>
                      </thead>
                      <tbody>
                      <?php if($r_type==1 || $r_type==2 || $r_type==5) { echo $output; } else if($r_type==0) {
                      foreach ($output as $output):  
                        ?>
                      <tr>
                         <td><?= date('d-m-Y', strtotime($output['sale_date']))   ; ?></td>
                         <td><?= $output['sale_item']; ?></td>
                         <td style="text-align:center"><?= $output['openingStock'] ?> </td>
                         <td style="text-align:center"><?php if(empty($output['purchase_qty'])) { echo "0"; } else { echo $output['purchase_qty']; } ?> </td>
                         <td style="text-align:center"><?php if(empty($output['purchase_return'])) { echo "0"; } else { echo $output['purchase_return']; } ?> </td>
                         <td style="text-align:center"><?php if(empty($output['sales_qty'])) { echo "0"; } else { echo $output['sales_qty']; } ?> </td>
                         <td style="text-align:center"><?php if(empty($output['sales_return'])) { echo "0"; } else { echo $output['sales_return']; } ?> </td>
                         <td style="text-align:center"><?php if(empty($output['consume'])) { echo "0"; } else { echo $output['consume']; } ?> </td>
                         <td style="text-align:center"><?= $output['closingstock'] ?> </td>
                       </tr>
                     <?php endforeach; } 
                      else if($r_type==3) {
                      foreach ($output as $output):  
                        ?>
                      <tr>
                         <td><?= date('d-m-Y', strtotime($output['sale_date']))   ; ?></td>
                         <td><?= $output['sale_item']; ?></td>
                         <td style="text-align:center"><?php if(empty($output['purchase_return'])) { echo "0"; } else { echo $output['purchase_return']; } ?> </td>
                         <td style="text-align:center"><?php if(empty($output['sales_return'])) { echo "0"; } else { echo $output['sales_return']; } ?> </td>
                       </tr>
                     <?php endforeach; }
                     ?>
               </tbody>
            </table>
      
        </div>
      </div>
    </div>

  </div>
</div>

</div>   

</div></div></div>
</div></div></div>
</section></div>


<script>
 $('#datepicker').datepicker({
      autoclose: true
    });</script>


    <script>
 $('#datepicker2').datepicker({
      autoclose: true
    });</script>



