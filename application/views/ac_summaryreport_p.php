<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
var r_type= "<?= $r_type ?>";
if(r_type==1)
{
  $('.tbsal').hide();
  $('.tbret').hide();
  $('.tbpur').show();

}
else if(r_type==2)
{
  $('.tbsal').show();
  $('.tbret').hide();
  $('.tbpur').show();
}
else if(r_type==3)
{
  $('.tbsal').hide();
  $('.tbret').show();
  $('.tbpur').hide();
}
else if(r_type==0)
{
  $('.tbsal').hide();
  $('.tbpur').hide();
  $('.tbret').hide();
}
  oTable = $('#patient_table').dataTable({
    "aaSorting": [[ 0, "dec" ]],
    "bJQueryUI": true,
    "sPaginationType": "full_numbers",
    "iDisplayLength": 10,
    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
  });
});
</script>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     <?php echo $page_title; ?>
    </h1>
     <ol class="breadcrumb">
      <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
      
      <li class="active"><?php echo $page_title; ?></li>
    </ol>&nbsp;&nbsp;
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
              <?php echo form_open_multipart($action) ?>
                  <input type="hidden" name="fdate" value="<?=$fdate ?>">
                  <input type="hidden" name="tdate" value="<?=$tdate ?>">
                  <input type="hidden" name="product1" value="<?=$product ?>">
                  <input type="hidden" name="r_type1" value="<?=$r_type ?>"> 


            <div class="col-md-12"><hr></div>
            
            <div class="row">
              <div class="col-md-12">
                <div class="box-header  ">
                  <center><h3 class="box-title"><b><?php if($r_type==1) { echo "Purchase"; } else if($r_type==2) { echo "Sales"; } else if($r_type==0) { echo "Balance Sheet"; } ?></b> Report from <b><?=date("d-m-Y",strtotime($fdate)) ?></b>&nbsp;<b></b> to <b><?=date("d-m-Y",strtotime($tdate)) ?></b> of <b><?php if($product==0) { echo "All Products"; } else { echo $products;  } ?></b></h3></center>
                </div>
                <div class="col-md-12"><hr></div>
                <center>     
                <button type="submit" name="submit" class="btn btn-primary" >Print</button>
              </center>

            <div class="row">
              
              <div class="col-md-12"><br>
                  <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-12">
                      <table id="" class="table table-condensed dataTable no-footer tbret">
                       <thead><b>
                        <tr><th colspan="7" style="text-align:center">Sale Return</th></tr>
                          <tr>
                          <th>Date</th>
                          <th>Product</th>
                          <th>Unit Price</th>
                          <th>Qty</th>
                          <th>Sale Amount</th>
                          <th>GST</th>
                          <th>Amount</th>
                        </tr>
                        </b>
                      </thead><?php if($r_type==3){
                      echo $sroutput;  }
                        ?>
             </table>

                      <table id="" class="table table-condensed dataTable no-footer tbret">
                       <thead><b>
                        <tr><th colspan="6" style="text-align:center">Purchase Return</th></tr>
                          <tr>
                          <th>Date</th>
                          <th>Product</th>
                          <th>Unit Price</th>
                          <th>Qty</th>
                          <th>Amount</th>
                        </tr>
                        </b>
                      </thead>
                      <?php if($r_type==3){
                      echo $proutput;  }
                        ?>
             </table> 


                      <table id="" class="table table-condensed dataTable no-footer">
                       <thead><b>
                        <tr>
                          <th class="tbpur">Date</th>
                          <th class="tbpur">Product</th>
                          <th class="tbpur">Unit Price</th>
                          <th class="tbpur">Qty</th>
                          <th class="tbpur">Discount</th>
                          <th class="tbsal">Sale Amount</th>
                          <th class="tbsal">GST</th>
                          <th class="tbpur">Amount</th>
                        </tr>
                        </b>
                      </thead>
                      <tbody>
                      <?php if($r_type==1 || $r_type==2) { echo $output; } ?>
                      </tbody>
                      <?php
                  $sumgsttot=0;
                  $sumam=0;
                  $sumdi=0;
                  $sumay=0;
                  $sumad=0;
                  $sumab=0;                
                  $sumgst=0;
                  $sumrf=0;
                  $sumtot=0;
                  $sumtot1=0;
                  $sumgst1=0;

                  if (is_array($output1) || is_object($output1))
                  {
                  foreach($output1 as $tsum)
                  {
                   
                    $sumgsttot  = $sumgsttot + $tsum['sumgsttot'];
                    $sumam  = $sumam + $tsum['sumam'];
                    $sumdi  = $sumdi + $tsum['sumdi'];
                    $sumay  = $sumay + $tsum['sumay'];
                    $sumad  = $sumad + $tsum['sumad'];
                    $sumab  = $sumab + $tsum['sumab'];
                    
                       if($r_type==2) {     
                        $sumgst1  = $sumgst1 + $tsum['sumgst1'];
                        $sumtot1 = $sumtot1 + $tsum['sumtot1'];       
                    $sumgst = $sumgst + $tsum['sumgst'];
                    $sumtot = $sumtot + $tsum['sumtot'];
                    $sumrf  = $sumrf + $tsum['sumrf']; }
                  } } 
              ?>
              <thead class="tbpur">
               <?php if($product==0){ ?>
                <tr<?php if($r_type==3 || $r_type==0) { ?> style="display:none" <?php } ?>><th width="85%">Overall <?php if($r_type==1) { echo "Purchase"; } else if($r_type==2) { echo "Sale"; }?> Discount</th><th><?="Rs. ".$sumdi ?></th></tr>
                <tr<?php if($r_type!=2) { ?> style="display:none" <?php } ?>><th width="85%">Total Round Off</th><th><?="Rs. ".$sumrf ?></th></tr>
                <tr<?php if($r_type==3 || $r_type==0) { ?> style="display:none" <?php } ?>><th width="85%">Total Amount Payable</th><th><?php echo "Rs. "; echo $sumay+$sumrf; ?></th></tr>
                <tr<?php if($r_type==3 || $r_type==0) { ?> style="display:none" <?php } ?>><th width="85%">Total Amount Paid</th><th><?="Rs. ".$sumad ?></th></tr>
                <tr<?php if($r_type==3 || $r_type==0) { ?> style="display:none" <?php } ?>><th width="85%">Balance Amount</th><th><?="Rs. ".$sumab ?></th></tr>
                <?php } ?>
              </thead>
             </table>
          
                    <table class="table table-condensed dataTable no-footer">
                       <?php
                       if($r_type==0) {
                      foreach ($output as $output):  
                        ?>
                      <thead>
                        <tr>
                        <th colspan=4 style="text-align: center;"><?= date('d-m-Y', strtotime($output['psdate'])) ?></th></tr>
                        <tr><td colspan=2 style="text-align: center;"><b>Opening Balance : </b> <?= "Rs. ".$output['openingBalance'] ?></td><td colspan=2 style="text-align: center;"><b>Closing Balance : </b> <?= "Rs. ".$output['closingBalance'] ?></td></tr>
                        <tr>
                         <th>Income</th>
                         <th>Amount</th>
                         <th>Expence</th>
                         <th>Amount</th>
                       </tr>
                        </thead>
                         <tr>
                         <td>Sales</td>
                         <td><?php if(empty($output['sales_sum'])) { echo "Rs. 0";} else { echo "Rs.".$output['sales_sum']; } ?></td>
                         <td>Purchase</td>
                         <td><?php if(empty($output['purchase_sum'])) { echo "Rs. 0";} else { echo "Rs.".$output['purchase_sum']; } ?> </td>
                       </tr>
                       <tr>
                         <td>Purchase Return</td>
                         <td><?php if(empty($output['prBalance'])) { echo "Rs. 0";} else { echo "Rs.".$output['prBalance']; } ?></td>
                         <td>Sales Return</td>
                         <td><?php if(empty($output['srBalance'])) { echo "Rs. 0";} else { echo "Rs.".$output['srBalance']; } ?></td>
                         </tr>
                         <?php if(!empty($output['expense'])) { ?>
                         <?php foreach ($output['expense'] as $outputs) {  ?>
                          <tr>
                          <td colspan=2></td>
                         <td><?= $outputs['category'] ?> </td>
                         <td><?= "Rs.".$outputs['amount'] ?> </td>
                       </tr>
                       <?php } } ?>
                     <tr></tr>
                        <tr><td><br></td></tr>
                     <?php endforeach; } ?>
                   </table>
              
              </div>
      
        </div>
      </div>
    </div>

  </div>
</div>

</div>
</div>

<?php echo form_close(); ?>    

</div></div></div>
</div></div></div>
</section></div>


<script>
 $('#datepicker').datepicker({
      autoclose: true
    });</script>


    <script>
 $('#datepicker2').datepicker({
      autoclose: true
    });</script>



