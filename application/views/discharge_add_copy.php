  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script type="text/javascript" charset="utf-8">
 $(function()
  {
    $("#ve_mrd").autocomplete({
      source: [<?php
      $i=0;
      foreach ($patients as $patient){
        if ($i>0) {echo ",";}
        echo '{value:"' . $patient['ip_mrd'] . '",patient:"' .$patient['p_title'] ." " . $patient['p_name'] . '",patientph:"' . $patient['p_phone'] . '",patientop:"' . $patient['ip_ipno'] . '",department:"' . $patient['dp_department'] . '",doctor:"' . $patient['u_name'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          // $("#customerid").val(ui.item ? ui.item.vid : '');
          $("#bk_name").val(ui.item ? ui.item.patient : '');
          $("#bk_phone").val(ui.item ? ui.item.patientph : '');
          $("#ve_customer").val(ui.item ? ui.item.patientop : '');
          $("#ip_department").val(ui.item ? ui.item.department : '');
          $("#ip_doctor").val(ui.item ? ui.item.doctor : '');
        }
      });   
  });   

  $(function() {
    $("#bk_phone").autocomplete({
      source: [<?php
      $i=0;
      foreach ($patients as $patient){
        if ($i>0) {echo ",";}
        echo '{value:"' . $patient['p_phone'] . '",patient:"' .$patient['p_title'] ." " . $patient['p_name'] . '",patientmrd:"' . $patient['ip_mrd'] . '",patientop:"' . $patient['ip_ipno'] . '",department:"' . $patient['dp_department'] . '",doctor:"' . $patient['u_name'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          // $("#customerid").val(ui.item ? ui.item.vid : '');
          $("#bk_name").val(ui.item ? ui.item.patient : '');
          $("#ve_mrd").val(ui.item ? ui.item.patientmrd : '');
          $("#ve_customer").val(ui.item ? ui.item.patientop : '');
          $("#ip_department").val(ui.item ? ui.item.department : '');
          $("#ip_doctor").val(ui.item ? ui.item.doctor : '');
        }
      });   
  }); 

  $(function() {
    $("#ve_customer").autocomplete({
      source: [<?php
      $i=0;
      foreach ($patients as $patient){
        if ($i>0) {echo ",";}
        echo '{value:"' . $patient['ip_ipno'] . '",mrd:"' . $patient['ip_mrd'] . '",patient:"'.$patient['p_title'] ." " . $patient['p_name'] . '",patientph:"' . $patient['p_phone'] . '",department:"' . $patient['dp_department'] . '",doctor:"' . $patient['u_name'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          $("#ve_mrd").val(ui.item ? ui.item.mrd : '');
          $("#bk_name").val(ui.item ? ui.item.patient : '');
          $("#bk_phone").val(ui.item ? ui.item.patientph : '');
          $("#ip_department").val(ui.item ? ui.item.department : '');
          $("#ip_doctor").val(ui.item ? ui.item.doctor : '');
        }
      });   
  });   

  $(function() {
    $("#product").autocomplete({
      source: [<?php
      $i=0;
      foreach ($additional_bills as $additional_bill){
        if ($i>0) {echo ",";}
        echo '{value:"' . $additional_bill['ab_name'] . '",price:"' . $additional_bill['ab_amount'] . '",productid:"' . $additional_bill['ab_id'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          $("#productid").val(ui.item ? ui.item.productid : '');
          $("#product_price").val(ui.item ? ui.item.price : '');
        }
      });   
  });

  </script>
  <script> 
$(document).ready(function() {
    var slno1 = 1;
  $('#ve_customer').change(function(){

    $('#item_table').find("tr:gt(0)").remove();
    var ve_cust = $('#ve_customer').val();
    var grand = 0;
     $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>discharge/get_room/"+ve_cust,
     success: function(data){
      if(data!=0) {
          var slno = 1;
      var newhead     = '<tr><th>'+slno1+'</th><th colspan="9">Rooms</th></tr> <tr><th></th><th>Sl.no.</th><th>Rooms</th><th>Admission Date</th><th>Vacated Date</th><th>Total Dates</th><th>Rent</th><th>Nursing Charge</th><th>House Keeping Charge</th><th>Credit Amount</th></tr>';
      $('#item_table tr:last').after(newhead);
      $.each(data, function(index) {
        var date2 = data[index].rs_dis;
        if(date2=="0000-00-00")
        {          
          var date2 = new Date();
          var dd = date2.getDate();
          var mm = date2.getMonth()+1;
          var yyyy = date2.getFullYear();
          if(dd<10) { dd='0'+dd; } 
          if(mm<10) { mm='0'+mm; }
          var date3 = yyyy+'-'+mm+'-'+dd;   
        }
        else
        {
          var date3 = data[index].rs_dis;
        }

        var date1 = new Date(data[index].rs_adm);
        var date2 = new Date(date2);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
        var total    = parseFloat(data[index].rm_nurse) + parseFloat(data[index].rm_fees) + parseFloat(data[index].rm_hc);
        var gtotal    = (parseFloat(data[index].rm_nurse) + parseFloat(data[index].rm_fees) + parseFloat(data[index].rm_hc)) * parseFloat(diffDays);
         var newrow      = '<tr><td> </td><td>'+slno+'</td><td><input type="hidden" value="'+data[index].rm_id+'" name="ved_itemid[]"><input class="form-control input-sm" type="hidden" value="'+data[index].rm_no+'" name="ved_item[]">' + data[index].rm_no + '</td><td>'+data[index].rs_adm+'</td><td>'+date3+'</td><td><input class="form-control input-sm" type="hidden" value="'+diffDays+'" name="ved_qty[]">'+diffDays+'</td><td>Rs. ' + data[index].rm_fees + '</td><td>Rs.'+data[index].rm_nurse+'</td><td>Rs. '+data[index].rm_hc+'</td><td><input type="hidden" value="'+total+'" name="ved_price[]"><input class="gtotal" type="hidden" value="'+gtotal+'" name="ved_total[]"><b>Rs. </b><label>'+gtotal+'</label></td></tr>';
        $('#item_table tr:last').after(newrow);
        slno++;
      });
        slno1++;
        $('.gtotal').each(function(){
        grand += parseFloat(this.value);
    });

      $('#lab_mrp').val(grand);
      $('#lab_mrp1').html(grand);
    } 

 $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>discharge/get_voucher_bills/"+ve_cust,
     success: function(data1){
      if(data1!=0) {
      var slno3 = 1;
        var newhead1      = '<tr><th>'+slno1+'</th><th colspan="9">Lab</th></tr> <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Credit Amount</th></tr>'
        $('#item_table tr:last').after(newhead1);
      $.each(data1, function(index) {
         var newrow1      = '<tr><td></td><td>'+slno3+'</td><td colspan="3"><input class="form-control input-sm" type="hidden" value="Lab Bill" name="ved_item[]">' + data1[index].ve_date + '</td><td colspan="4">'+data1[index].ve_vno+'</td><td><input class="gtotal1" type="hidden" value="'+data1[index].ve_apayable+'" name="ved_total[]"><b>Rs. </b><label>'+data1[index].ve_apayable+'</label></td></tr>';
        $('#item_table tr:last').after(newrow1);
        slno3++;
      });
        slno1++;
        $('.gtotal1').each(function(){
        grand += parseFloat(this.value);
    });
      $('#lab_mrp').val(grand);
      $('#lab_mrp1').html(grand);
    } 

    $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>discharge/get_pharmacy_bills/"+ve_cust,
     success: function(data2){
      if(data2!=0) {
      var slno6 = 1;
        var newhead2      = '<tr><th>'+slno1+'</th><th colspan="9">Pharmacy</th></tr> <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Credit Amount</th></tr>'
        $('#item_table tr:last').after(newhead2);
      $.each(data2, function(index) {
         var newrow2      = '<tr><td></td><td>'+slno6+'</td><td colspan="3"><input class="form-control input-sm" type="hidden" value="Pharmacy Bill" name="ved_item[]">' + data2[index].ve_date + '</td><td colspan="4">'+data2[index].ve_vno+'</td><td><input class="gtotal4" type="hidden" value="'+data2[index].ve_apayable+'" name="ved_total[]"><b>Rs. </b><label>'+data2[index].ve_apayable+'</label></td></tr>';
        $('#item_table tr:last').after(newrow2);
        slno6++;
      });
        slno1++;
        $('.gtotal4').each(function(){
        grand += parseFloat(this.value);
    });
      $('#lab_mrp').val(grand);
      $('#lab_mrp1').html(grand);
    } 

    $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>discharge/get_pharmacy_returns/"+ve_cust,
     success: function(data3){
      if(data3!=0) {
      var slno7 = 1;
        var newhead3      = '<tr><th>'+slno1+'</th><th colspan="9">Medicine Return</th></tr> <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Debit Amount</th></tr>'
        $('#item_table tr:last').after(newhead3);
      $.each(data3, function(index) {
         var newrow3      = '<tr><td></td><td>'+slno7+'</td><td colspan="3"><input class="form-control input-sm" type="hidden" value="Pharmacy Return Bill" name="ved_item[]">' + data3[index].ve_date + '</td><td colspan="4">'+data3[index].ve_vno+'</td><td><input class="gtotal5" type="hidden" value="'+data3[index].ve_apayable+'" name="ved_total[]"><b>Rs. </b><label>'+data3[index].ve_apayable+'</label></td></tr>';
        $('#item_table tr:last').after(newrow3);
        slno7++;
      });
        slno1++;
        $('.gtotal5').each(function(){
        grand -= parseFloat(this.value);
    });
      $('#lab_mrp').val(grand);
      $('#lab_mrp1').html(grand);
    } 

    $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>discharge/get_advances/"+ve_cust,
     success: function(data4){
      if(data4!=0) {
      var slno4 = 1;
      var grand1 = 0;
        var newhead4      = '<tr><th>'+slno1+'</th><th colspan="9">Advance Payments</th></tr> <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Debit Amount</th></tr>'
        $('#item_table tr:last').after(newhead4);
      $.each(data4, function(index) {
         var newrow4      = '<tr><td></td><td>'+slno4+'</td><td colspan="3">' + data4[index].ve_date + '</td><td colspan="4">'+data4[index].ve_vno+'</td><td><input class="gtotal2" type="hidden" value="'+data4[index].ve_apayable+'"><b>Rs. </b><label>'+data4[index].ve_apayable+'</label></td></tr>';
        $('#item_table tr:last').after(newrow4);
        slno4++;
      });
        slno1++;
        $('.gtotal2').each(function(){
        grand1 += parseFloat(this.value);
    });
      $('#lab_mrp').val(grand);
      $('#lab_mrp1').html(grand);
      $('#advance').val(grand1);
      $('#advance1').html(grand1);

      var apayable = parseFloat(grand) - parseFloat(grand1);
      amount         =  (Math.round(apayable * 100) / 100).toFixed(2);
      payable        =  Math.round(amount);

      $('#apayable').val(payable);
      $('#apayable1').html(payable);

      roundoff       =  parseFloat(payable) - parseFloat(amount); 
      roundoff       =  (Math.round(roundoff * 100) / 100).toFixed(2);
      $('#roundoff').val(roundoff);
      $('#roundoff1').html(roundoff);
    } 
  }//5th ajax succes
  }) //5th ajax finish



  }// 4th ajax succes
  })//4th ajax finish



  }// 3rd ajax succes
  }) //3rd ajax finish




  }// 2nd ajax succes
  }) //2 ajaxfinished

  } // 1st ajax succes
  })


    // grand           =   (Math.round(grand * 100) / 100).toFixed(2);

    // $('#lab_mrp').val(grand);
    // $('#lab_mrp1').html(grand);
  });

   $('#lab_tname').change(function(){
    var lab_tname = $('#lab_tname').val();
    var uRL1   = "<?= base_url() ?>investigation/test_price";
    $.ajax({
        url : uRL1,
        type: "POST",
        data : {tname:lab_tname},
        success:function(data2, textStatus, jqXHR)
        {
          $('#lab_tprice').val(data2);
        }
  })
  });

   $('.button').click(function() {

    var slno5 = 1;
    var productid       = $('#productid').val();
    var product         = $('#product').val();
    var product_price   = $('#product_price').val();
    var qty             = "1";
    var total           = parseFloat(product_price) * parseFloat(qty);
    
     var newhead5     = '<tr><th>'+slno1+'</th><th colspan="9">Others</th></tr> <tr><th></th><th>Sl.no.</th><th colspan="7">Patricular</th><th></th></tr>';

     $('#item_table tr:last').after(newhead5);

    var newrow5      = '<tr><td></td><td>'+slno5+'</td><td colspan="7"><input type="hidden" value="'+productid+'" name="ved_itemid[]"><input class="form-control input-sm" type="hidden" value="'+product+'" name="ved_item[]"><b>' + product + '</b></td><td><input class="gtotal3" type="hidden" value="'+total+'" name="ved_total[]"><b>Rs. </b><label>'+total+'</label></td><td><button class="btn btn-xs btn-danger dlte"><i class="fa fa-times-circle-o"></i></button></td></tr>';

    var lab_mrp     = $('#lab_mrp').val();
    var advance     = $('#advance').val();

    lab_mrp         = parseFloat(lab_mrp) + parseFloat(total);
    lab_mrp         = (Math.round(lab_mrp * 100) / 100).toFixed(2);

    $('#lab_mrp').val(lab_mrp);
    $('#lab_mrp1').html(lab_mrp);

      var apayable   = parseFloat(lab_mrp) - parseFloat(advance);
      amount         =  (Math.round(apayable * 100) / 100).toFixed(2);
      payable        =  Math.round(amount);

      $('#apayable').val(payable);
      $('#apayable1').html(payable);

    roundoff       =  parseFloat(payable) - parseFloat(amount); 
    roundoff       =  (Math.round(roundoff * 100) / 100).toFixed(2);
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);

    $('#item_table tr:last').after(newrow5);
    slno1++;
    document.getElementById('productid').value = "";
    document.getElementById('product').value = "";
    document.getElementById('product_price').value = "";

});

  });
  </script>

  <script>
  $(document).ready(function() {

  
});
    </script>

  <script>
  $(document).ready(function() {
    var gtotal5 = 0;
    var gtotal4 = 0;
    var gtotal3 = 0;
    var gtotal2 = 0;
    var gtotal1 = 0;
    var gtotal = 0;
$(document).on('click','button.dlte', function() {
    var $row    = $(this).closest("tr");    // Find the row

    var r = confirm("Are you sure want to delete!");
    if (r == true) {
    $(this).closest('tr').remove();

    $('.gtotal5').each(function(){
        gtotal5 += parseFloat(this.value);
    });
    $('.gtotal4').each(function(){
        gtotal4 += parseFloat(this.value);
    });

    $('.gtotal3').each(function(){
        gtotal3 += parseFloat(this.value);
    });
    $('.gtotal1').each(function(){
        gtotal1 += parseFloat(this.value);
    });
    $('.gtotal2').each(function(){
        gtotal2 += parseFloat(this.value);
    });
    $('.gtotal').each(function(){
        gtotal += parseFloat(this.value);
    });

    lab_mrp        =  parseFloat(gtotal) + parseFloat(gtotal1) + parseFloat(gtotal3) + parseFloat(gtotal4) - parseFloat(gtotal5);
    lab_mrp        =  (Math.round(lab_mrp * 100) / 100).toFixed(2);

    gtotal2        =  (Math.round(gtotal2 * 100) / 100).toFixed(2);

    amount         =  parseFloat(lab_mrp) - parseFloat(gtotal2);
    amount         =  (Math.round(amount * 100) / 100).toFixed(2);
    payable        =  Math.round(amount);


    roundoff       =  parseFloat(payable) - parseFloat(amount); 
    roundoff       =  (Math.round(roundoff * 100) / 100).toFixed(2);

    $('#lab_mrp').val(lab_mrp);
    $('#lab_mrp1').html(lab_mrp);
    $('#advance').val(gtotal2);
    $('#advance1').html(gtotal2);

    $('#apayable').val(payable);
    $('#apayable1').html(payable);
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);
    } 
    });


  });
  </script>


  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#customer_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       Discharge
       <small>New Discharge </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."ipregister"; ?>">IP</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/ipregister">
        <i class="fa fa-plus-circle"></i> Active IP List
      </a>
    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>

      <form id="post_form">
     <div class="box-body">
      <div class="row"> 
       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">Voucher No.<sup></sup></label>
          <input class="form-control input-sm" readonly type="text" id="ve_vno" name="ve_vno" tabindex="1" value="<?=$bill_no?>"> 
          <input class="form-control" type="hidden" name="user_type" value="<?=$user_type?>">              
        </div>
        <div class="col-md-2">
          <label  class="control-label">Date<sup></sup></label>
          <input class="form-control validate[required] input-sm"  tabindex="2" data-prompt-position="topRight:150" type="text" name="ve_date" id="datepicker" value="<?=date("Y-m-d") ?>">                
        </div>
        <div class="col-md-4"></div>
         <div class="col-md-2">
         <label  class="control-label">IP No.<sup></sup></label>
         <input class="form-control input-sm" data-prompt-position="topLeft:190" tabindex="5" type="text" name="ve_customer" id="ve_customer"></div>

          <div class="col-md-2">
         <label  class="control-label">MRD<sup></sup></label>
         <input class="form-control validate[required] input-sm" data-prompt-position="topLeft:190" tabindex="5" type="text" name="ve_mrd" id="ve_mrd"></div>
               </div>
      <div class="col-md-12">
      <br>
       <div class="col-md-4">
         <label  class="control-label">Patient's Name<sup></sup></label>
         <input class="form-control validate[required] input-sm"  data-prompt-position="bottomLeft:190"  tabindex="6" type="text" id="bk_name">
         <input type="hidden" name="ve_supplier" value="Lab">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Phone No.<sup></sup></label>
         <input class="form-control validate[required] input-sm" data-prompt-position="topLeft:190" tabindex="7" type="text" id="bk_phone">
       </div>
       <div class="col-md-2"></div>
         <div class="col-md-2">
         <label  class="control-label">Department<sup></sup></label>
         <input class="form-control validate[required] input-sm" data-prompt-position="topLeft:190" tabindex="7" type="text" id="ip_department"> 
       </div>
        <div class="col-md-2">
         <label  class="control-label">Doctor<sup></sup></label>
         <input class="form-control validate[required] input-sm" data-prompt-position="topLeft:190" tabindex="7" type="text" id="ip_doctor">  
       </div>
     </div>
     <div class="col-md-12">
      <br>
      <div class="col-md-4">
        <div class="form-group required">
          <label  class="control-label">Item<sup></sup></label>
          <input class="form-control input-sm" type="text" id="product">
          <input class="form-control" type="hidden" name="productid" id="productid">
        </div> </div>
        
          <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Unit Price<sup></sup></label>
              <input class="form-control input-sm" id="product_price"  type="text" name="product_price">  
               </div> </div>



            </div>
          </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-md-12">
                <div>
                  <a class="button btn btn-primary" >Add to List</a>
                  <input class="btn-large btn-default btn"  tabindex="9" type="reset" value="Reset">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Sl No.</th>
                        <th colspan="8">Particulars</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                  <tbody> 

                  </tbody>
                  </table> 
                  <table class="table table-bordered">
                    <tr><th width="90%">Total</th>
                      <th><input type="hidden" name="lab_mrp" readonly id="lab_mrp" value="0">Rs. <label id="lab_mrp1">0</label></th></tr>
                      <tr><th width="90%">Advance Paid</th>
                      <th><input type="hidden" name="advance" readonly id="advance" value="0">Rs. <label id="advance1">0</label></th></tr>
                      <tr><th width="90%">Amount Payable</th>
                      <th><input type="hidden" name="apayable" readonly id="apayable" value="0">Rs. <label id="apayable1">0</label></th></tr>
                      <tr><th width="90%">Round Off</th>
                      <th><input type="hidden" name="roundoff" readonly id="roundoff" value="0">Rs. <label id="roundoff1">0</label></th></tr>
                    </table>

                    <a href="#" class="btn btn-success" onclick="save();" accesskey="s" title="short key-ALT+S"><i class="fa fa-floppy-o"></i> Save</a>
                  <input type="hidden" id="Printid"> 
                    <a href="#" class="btn btn-warning" onclick="print();" accesskey="p" title="short key-ALT+P" ><i class="fa fa-print"></i> Print</a> 
                
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</form>
</div>
</section>

</section><!-- /.right-side -->

<script>
function save(){

      var postData = $("#post_form").serializeArray();
      var formURL  = "<?= base_url() ?>discharge/dischargeAdd";

      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          // alert("Success");
          $('#Printid').val(data);
           window.location = "<?php echo base_url();?>index.php/discharge";
        }
      });
}
// function print(){

//           var Printid         =  $('#Printid').val();
//           if(Printid=="")
//           {
//             alert("Click Save Before Print");
//           }
//           else
//           {
//           window.location = "<?php echo base_url();?>index.php/sales/getPrint?sPrintid="+Printid;
//           }

// }
</script>

<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });</script>
<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>