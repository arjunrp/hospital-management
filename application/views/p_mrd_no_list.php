<input class="form-control validate[required]" type="text" name="bk_mrd" id="bk_mrd">
<script>
 $(document).ready(function() {

 $("#bk_mrd").autocomplete({
      source: [<?php
      $i=0;
      foreach ($patients as $patientmrd){
        if ($i>0) {echo ",";}
        echo '{value:"' . $patientmrd['p_mrd_no'] . '",patient:"' . $patientmrd['p_name'] . '",patientph:"' . $patientmrd['p_phone'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          // $("#customerid").val(ui.item ? ui.item.vid : '');
          $("#bk_name").val(ui.item ? ui.item.patient : '');
          $("#bk_phone").val(ui.item ? ui.item.patientph : '');
        }
      }); 
      </script>  