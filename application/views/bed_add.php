<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {

$("#bd_rmid").change(function(){ 
      var bd_rmid =$("#bd_rmid").val();  
     $.ajax({
           type:"POST",
           dataType: "json",
           url: "<?= base_url() ?>/bed/get_beds/",
           data:{room:bd_rmid},
              success: function(data){
                $('#beds').empty();  
                // alert(data)
                      $.each(data, function(index) {
                    $('#beds').append('<option value="'+data[index].bd_id+'" disabled>'+data[index].bd_no+'</option>');
                  }); 
                  }
                });
});

 oTable = $('#category_table').dataTable({
});
});
</script>

<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
      Beds
      <small> Beds </small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."bed"; ?>">Beds</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div align="right">
         <a title="short key-ALT+B" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."bed"; ?>" accesskey="b"><i class="fa fa-mail-reply-all"></i> Back</a>
       </div>

     </div>

     <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-2">
          <div class="form-group required">
            <label  class="control-label">Room No.<sup>*</sup></label>
            <select class="form-control validate[required] input-sm" id="bd_rmid" tabindex="1" name="bd_rmid" value="<?=$bd_rmid  ?>"> 
              <option value="Select">--Select--</option>
              <?php foreach ($rm_no as $key => $rm_no) {
                ?>
              <option value="<?php echo $rm_no['rm_id']?>"<?php if($rm_no['rm_id']==$bd_rmid) { ?> selected="selected" <?php } ?>><?php echo $rm_no['rm_no']?></option>
                <?php
              }?>
            </select>               
          </div>
        </div>
          <div class="col-md-2">
          <div class="form-group required">
            <label  class="control-label">Bed No<sup>*</sup></label>
            <input  class="form-control input-sm" tabindex="2" type="text" name="bd_no" value="<?=$bd_no  ?>">    
            <input type="hidden" name="bd_id" value="<?=$bd_id  ?>">        
          </div>
        </div>

        <div class="col-md-3">
          <label>Beds Added</label>
               <select class="form-control" id="beds" multiple>
                <option value="">---Shows Nothing---</option>
              </select>
              </div>
      </div>
      </div>
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            <div>
              <input class="btn-large btn-primary btn" tabindex="5" type="submit" value="Submit" name="submit" accesskey="s" title="short key-ALT+S"> 
              <input class="btn-large btn-default btn" type="reset" value="Reset">
            </div>
          </div>

        </div>
      </div>
      <?php echo form_close(); ?>
    </div>

  </div>
  <br><br><br><br><br><br><br><br><br><br><br> 
</section>

</section><!-- /.right-side -->


