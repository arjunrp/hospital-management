<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script type="text/javascript" charset="utf-8">
 $(document).ready(function() {

  var bRNo = $("#ve_bill_no").val();

        $.ajax({
        dataType: "json",
        url: "<?= base_url() ?>preturn/returnPBillno/"+bRNo,
        success: function(data){

          $('#ved_id').empty();
          $("#ved_id").append('<option value=0>Select Item</option>');
          $.each(data, function(index) {
          $("#ved_id").append('<option value=' + data[index].ved_id +'>'+data[index].ved_item+" - "+data[index].ved_batch+'</option>');
          });
        }
      })

<?php
$js_array = json_encode($preturns);
echo "var preturn = ". $js_array . ";\n";
?>

$.each(preturn, function(index) {

         var newrow      = '<tr><td><input type="hidden" value="'+preturn[index].ved_itemid+'" name="ved_itemid[]"><input class="form-control input-sm" type="hidden" value="'+preturn[index].ved_item+'" name="ved_item[]">' + preturn[index].ved_item + '</td><td><input class="form-control input-sm" type="hidden" name="ved_price[]" value="'+preturn[index].ved_price+'">'+preturn[index].ved_price+'</td><td><input class="form-control input-sm" type="input" value="'+preturn[index].ved_qty/preturn[index].ved_uqty+'" name="ved_qty[]">'+preturn[index].ved_qty/preturn[index].ved_uqty+'<input class="form-control input-sm" type="hidden" value="'+preturn[index].ved_uqty+'" name="ved_uqty[]"></td><td><input class="form-control input-sm" type="hidden" value="'+preturn[index].ved_unit+'" name="ved_unit[]">'+preturn[index].ved_unit+'</td><td><input class="form-control input-sm" type="hidden" name="ved_batch[]" value="'+preturn[index].ved_batch+'">'+preturn[index].ved_batch+'</td><td><input class="form-control input-sm" type="hidden" name="ved_expiry[]" value="'+preturn[index].ved_expiry+'">'+preturn[index].ved_expiry+'</td><td><input class="stotal" type="hidden" value="'+preturn[index].ved_total+'" name="ved_total[]">' + preturn[index].ved_total + '</td><td><input class="ved_sgsta" type="hidden" value="'+preturn[index].ved_sgsta+'" name="ved_sgsta[]">' + preturn[index].ved_sgsta +'<input type="hidden" value="'+preturn[index].ved_sgstp+'" name="ved_sgstp[]"> (' + preturn[index].ved_sgstp +' %)</td><td><input class="ved_cgsta" type="hidden" value="'+preturn[index].ved_cgsta+'" name="ved_cgsta[]">' + preturn[index].ved_cgsta +'<input type="hidden" value="'+preturn[index].ved_cgstp+'" name="ved_cgstp[]"> (' + preturn[index].ved_cgstp +' %)</td><td><input class="ftotal" type="hidden" value="'+preturn[index].ved_gtotal+'" name="ved_gtotal[]">' + preturn[index].ved_gtotal + '</td><td><button class="btn btn-xs btn-danger dlte"><i class="fa fa-times-circle-o"></i></button></td></tr>';
        $('#item_table tr:last').after(newrow);
      });


  $("#ved_id").change(function() {
    var bRNo = $("#ve_bill_no").val();
    var r_item=$('#ved_id').val();


        $.ajax({
        dataType: "json",
        url: "<?= base_url() ?>preturn/returnPPrice/"+bRNo+"/"+r_item,
        success: function(data){
        jQuery.each(data, function(index, itemDatas) {

            $('#ved_itemid').val(itemDatas.ved_itemid);
            $('#ved_item').val(itemDatas.ved_item);
            $('#ved_batch').val(itemDatas.ved_batch);
            $('#ved_expiry').val(itemDatas.ved_expiry);
            $('#ved_price').val(itemDatas.ved_price);
            $('#ved_pqty').val(itemDatas.ved_qty/itemDatas.pd_qty);
            $('.ved_unit').val(itemDatas.ved_unit);
            $('#ve_pdate').val(itemDatas.ve_date);
            $('#ve_cgstp').val(itemDatas.ved_cgstp);
            $('#ve_sgstp').val(itemDatas.ved_sgstp);
            $('#pd_qty').val(itemDatas.pd_qty);
          });
            // var price     = $('#purchase_price').val();
            // var qty       = $('#purchase_quanity').val();
            // var uprice    = parseFloat(price) / parseFloat(qty);
            // $('#product_price').val(uprice);
          }
        })
    });

  $("#ved_qty").change(function() {
    var ved_qty   = $("#ved_qty").val();
    var ved_pqty  = $('#ved_pqty').val();

        if(parseFloat(ved_qty)>parseFloat(ved_pqty))
        {
          alert("Quantity Mismatch");
          $("#ved_qty").val("");
        }

  });

  });
  </script>


<!-- Product Delete -->
<script>
  $(document).ready(function() {

  $(document).on('click','button.dlte', function() {
    var $row      = $(this).closest("tr");    // Find the row
  
    var r = confirm("Are you sure want to delete!");
    if (r == true) {
    $(this).closest('tr').remove();
    var stotal = 0;
    var ved_sgsta = 0;
    var ved_cgsta = 0;
    var ftotal = 0;
    $('.stotal').each(function(){
        stotal += parseFloat(this.value);
    });
    $('.ved_sgsta').each(function(){
        ved_sgsta += parseFloat(this.value);
    });
    $('.ved_cgsta').each(function(){
        ved_cgsta += parseFloat(this.value);
    });
    $('.ftotal').each(function(){
        ftotal += parseFloat(this.value);
    });
    sum           =   (Math.round(stotal * 100) / 100).toFixed(2);
    tsgsta        =   (Math.round(ved_sgsta * 100) / 100).toFixed(2);
    tcgsta        =   (Math.round(ved_cgsta * 100) / 100).toFixed(2);
    grand         =   (Math.round(ftotal * 100) / 100).toFixed(2);

    grand         =  (Math.round(grand * 100) / 100).toFixed(2);

    gtotal        = Math.round(grand);

    var roundoff    =   parseFloat(gtotal) - parseFloat(grand);
    roundoff        =   (Math.round(roundoff * 100) / 100).toFixed(2);


    $('#sum').val(sum);
    $('#sum1').html(sum);

    $('#cgsta').val(tcgsta);
    $('#cgsta1').html(tcgsta);
    $('#sgsta').val(tsgsta);
    $('#sgsta1').html(tsgsta);
    $('#apayable').val(grand);
    $('#apayable1').html(grand);
    $('#gtotal').val(gtotal);
    $('#gtotal1').html(gtotal);
    
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);
    } 
    });
  });
  </script>
<!-- Product Add -->
  <script>
  $(document).ready(function() {
  $('.add_table').click(function() {

    var sgstp       = $('#ve_sgstp').val();
    var cgstp       = $('#ve_cgstp').val();
    var item        = $('#ved_item').val();
    var itemid      = $('#ved_itemid').val();
    var batch       = $('#ved_batch').val();
    var expiry      = $('#ved_expiry').val();
    var qty         = $('#ved_qty').val();
    var pd_qty      = $('#pd_qty').val();
    var unt         = $('.ved_unit').val();
    var price       = $('#ved_price').val();
    var total       = $('#ved_qty').val() * $('#ved_price').val();

    var sgsta       = parseFloat(total) * parseFloat(sgstp) / 100;
    var cgsta       = parseFloat(total) * parseFloat(cgstp) / 100;
    var amount      = parseFloat(total) + parseFloat(sgsta) + parseFloat(cgsta);

    var sgsta       = (Math.round(sgsta * 100) / 100).toFixed(2);
    var cgsta       = (Math.round(cgsta * 100) / 100).toFixed(2);  
    var amount      = (Math.round(amount * 100) / 100).toFixed(2);    

     var newrow      = '<tr><td><input type="hidden" value="'+itemid+'" name="ved_itemid[]"><input type="hidden" value="'+item+'" name="ved_item[]">' + item + '</td><td><input type="hidden" value="'+price+'" name="ved_price[]">' + price + '</td><td><input type="hidden" value="'+qty+'" name="ved_qty[]">' + qty + '<input type="hidden" value="'+uqty+'" name="ved_uqty[]"></td><td><input type="hidden" value="'+unt+'" name="ved_unit[]">' + unt + '</td><td><input type="hidden" value="'+batch+'" name="ved_batch[]">' + batch + '</td><td><input type="hidden" value="'+expiry+'" name="ved_expiry[]">' + expiry + '</td><td><input class="stotal" type="hidden" value="'+total+'" name="ved_total[]">' + total + '</td><td><input class="ved_sgsta" type="hidden" value="'+sgsta+'" name="ved_sgsta[]">' + sgsta +'<input type="hidden" value="'+sgstp+'" name="ved_sgstp[]"> (' + sgstp +' %)</td><td><input class="ved_cgsta" type="hidden" value="'+cgsta+'" name="ved_cgsta[]">' + cgsta +'<input type="hidden" value="'+cgstp+'" name="ved_cgstp[]"> (' + cgstp +' %)</td><td><input class="ftotal" type="hidden" value="'+amount+'" name="ved_gtotal[]">' + amount + '</td><td><button class="btn btn-xs btn-danger dlte"><i class="fa fa-times-circle-o"></i></button></td></tr>';

    var sum         = $('#sum').val();
    var tcgsta      = $('#cgsta').val();
    var tsgsta      = $('#sgsta').val();

    var sum       = parseFloat(sum) + parseFloat(total);
    var tcgsta    = parseFloat(tcgsta) + parseFloat(cgsta);
    var tsgsta    = parseFloat(tsgsta) + parseFloat(sgsta);
    var apayable  = parseFloat(sum) + parseFloat(tcgsta) + parseFloat(tsgsta);
    apayable      = (Math.round(apayable * 100) / 100).toFixed(2);
    grand         = Math.round(apayable);

    sum       = parseFloat(Math.round(sum * 100) / 100).toFixed(2);
    tcgsta    = parseFloat(Math.round(tcgsta * 100) / 100).toFixed(2);
    tsgsta    = parseFloat(Math.round(tsgsta * 100) / 100).toFixed(2);

    roundoff  = Math.round(apayable) - parseFloat(apayable);
    roundoff  = parseFloat(Math.round(roundoff * 100) / 100).toFixed(2);

    $('#sum').val(sum);
    $('#sum1').html(sum);
    $('#cgsta').val(tcgsta);
    $('#cgsta1').html(tcgsta);
    $('#sgsta').val(tsgsta);
    $('#sgsta1').html(tsgsta);
    $('#apayable').val(apayable);
    $('#apayable1').html(apayable);
    $('#gtotal').val(grand);
    $('#gtotal1').html(grand);
    $('#roundoff').val(roundoff);
    $('#roundoff1').html(roundoff);

    $('#item_table tr:last').after(newrow);

  
});
  });
  </script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
  <section class="right-side" style="min-height:700px;"> 

    <?php
foreach($preturns as $preturn)
{
  $ve_id          = $preturn['ve_id'];
  $ve_bill_no     = $preturn['ve_bill_no'];
  $ve_vno         = $preturn['ve_vno'];
  $ve_date        = $preturn['ve_date'];
  $sp_id          = $preturn['sp_id'];
  $sp_vendor      = $preturn['sp_vendor'];
  $sp_phone       = $preturn['sp_phone'];
  $ve_amount      = $preturn['ve_amount'];
  $ve_sgst        = $preturn['ve_sgst'];
  $ve_cgst        = $preturn['ve_cgst'];
  $ve_gtotal      = $preturn['ve_gtotal']; 
  $ve_apayable    = $preturn['ve_apayable']; 
  $ve_round       = $preturn['ve_round']; 
  $ve_type        = $preturn['ve_user']; 
}

?>

    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       Purchase Return
       <small>Edit</small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."preturn/return_Plist"; ?>">Purchase Return</a></li>
      <li class="active"> <?=$page_title ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?=$page_title ?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/preturn/return_Padd">
        <i class="fa fa-plus-circle"></i> Create New
      </a>

    </div>
     </div>

      <form id="post_form">
     <div class="box-body">
        
      <div class="row"> 
       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">Return No. : <sup></sup></label><?= $ve_vno;?>   
          <input class="form-control"  type="hidden" name="user_type" value="<?= $user_type;?>">
          <input class="form-control"  type="hidden" id="ve_id" name="ve_id" value="<?= $ve_id;?>">               
        </div>
        <div class="col-md-6">
          <label  class="control-label">Return Date : <sup></sup></label><?= date("d-m-Y",strtotime($ve_date))?>
          <input class="form-control"  type="hidden" id="ve_date" value="<?= $ve_date;?>">
        </div>

           <div class="col-md-2">
         <label  class="control-label">Bill No. : <sup></sup></label><?= $ve_bill_no;?> 
         <input class="form-control"  type="hidden" id="ve_bill_no" value="<?= $ve_bill_no;?>">
       </div>
       <div class="col-md-2">
         <label  class="control-label">User ID : <sup></sup></label><?= $ve_type;?> 
       </div>
       <div class="col-md-12"><br></div>
       <div class="col-md-2">
         <label  class="control-label">Supplier ID : <sup></sup></label><?= $sp_id;?> 
       </div>
       <div class="col-md-6">
         <label  class="control-label">Supplier Name : <sup></sup></label><?= $sp_vendor;?> 
       </div>
       <div class="col-md-3">
         <label  class="control-label">Supplier Ph. : <sup></sup></label><?= $sp_phone;?> 
       </div>
     </div>
     </div>
     <div class="col-md-12"><br></div>
     <div class="row"> 
     <div class="col-md-12">
      <div class="col-md-4">
        <div class="form-group required">
          <label  class="control-label">Item<sup></sup></label>
          <select  id="ved_id" class="form-control input-sm">
          </select>
          <input class="form-control" type="hidden" id="ved_itemid">
          <input class="form-control" type="hidden" id="ved_item">  
          <input class="form-control" type="hidden" id="ve_sgstp">  
          <input class="form-control" type="hidden" id="ve_cgstp">           
        </div> </div>
        <div class="col-md-4"></div>

         <div class="col-md-2">
          <div class="form-group required">
            <label  class="control-label">Quantity<sup></sup></label>
            <input class="form-control validate[required] input-sm" data-prompt-position="bottomRight:150" id="ved_qty">  
            <input class="form-control input-sm" type="hidden" id="pd_qty">                 
          </div> </div>

          <div class="col-md-2">
          <div class="form-group required">
            <label  class="control-label">Unit<sup></sup></label>
            <input class="form-control validate[required] ved_unit input-sm" data-prompt-position="bottomRight:150" readonly>
            </select>      
          </div> </div>


        <div class="col-md-2 vend cust">
          <div class="form-group required">
            <label  class="control-label vend">Purchased<sup></sup></label>
            <input class="form-control input-sm" type="input"  readonly  data-prompt-position="bottomRight:150" id="ved_pqty">            
          </div> </div>
          <div class="col-md-2 vend cust">
          <div class="form-group required">
            <label  class="control-label">Unit<sup></sup></label>
            <input class="form-control ved_unit input-sm" readonly  type="text">
          </div> </div>
          <div class="col-md-2 vend cust">
          <div class="form-group required">
            <label class="control-label">Date<sup></sup></label>
            <input class="form-control vend cust input-sm"  readonly  data-prompt-position="bottomRight:150" id="ve_pdate">                
          </div> </div>

          <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Batch<sup></sup></label>
              <input class="form-control input-sm" readonly id="ved_batch" type="text">
            </div> </div>
            <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Expiry<sup></sup></label>
              <input class="form-control input-sm" readonly id="ved_expiry" type="text">  
            </div> </div>
         


          <div class="col-md-2">
            <div class="form-group required">
              <label  class="control-label">Unit Price<sup></sup></label>
              <input class="form-control input-sm" readonly id="ved_price" type="text">             
            </div> </div>

            
          <div class="col-md-3">
              <label>&emsp;</label><br>
                  <a class="button btn btn-primary add_table" >Add to Table</a>
                  <input class="btn-large btn-default btn" type="reset" value="Reset">
          </div></div></div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Product</th>
                        <th>Unit Price</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                        <th>Batch</th>
                        <th>Expiry</th>
                        <th>Sub Total</th>
                        <th>SGST</th>
                        <th>CGST</th>
                        <th>Sub Total(Inc. GST)</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                  <tbody> 
                  </tbody>
                  </table> 
                  <table class="table table-bordered">
                    <tr><th width="85%">Total</th>
                      <th><input type="hidden" name="sum" readonly id="sum" value="<?= $ve_amount;?> ">Rs. <label id="sum1"><?= $ve_amount;?> </label></th></tr>
                      <tr><th width="85%">Total CGST</th>
                      <th><input type="hidden" name="cgsta" readonly id="cgsta" value="<?= $ve_cgst;?> ">Rs. <label id="cgsta1"><?= $ve_cgst;?> </label></th></tr>
                      <tr><th width="85%">Total SGST</th>
                      <th><input type="hidden" name="sgsta" readonly id="sgsta" value="<?= $ve_sgst;?> ">Rs. <label id="sgsta1"><?= $ve_sgst;?> </label></th></tr>
                      <tr><th width="85%">Total (Inc. GST)</th>
                      <th><input type="hidden" name="apayable" readonly id="apayable" value="<?= $ve_gtotal;?> ">Rs. <label id="apayable1"><?= $ve_gtotal;?> </label></th></tr>
                      <tr><th width="85%">Round Off</th>
                      <th><input type="hidden" name="roundoff" readonly id="roundoff" value="<?= $ve_round;?> ">Rs. <label id="roundoff1"><?= $ve_round;?> </label></th></tr>
                      <tr><th width="85%">Amount Payable</th>
                      <th><input type="hidden" name="gtotal" readonly id="gtotal" value="<?= $ve_apayable;?> ">Rs. <label id="gtotal1"><?= $ve_apayable;?> </label></th></tr>
                    </table>
   
<a href="#" class="btn btn-success" onclick="save();" accesskey="s" title="short key-ALT+S"><i class="fa fa-floppy-o"></i> Save</a>
<input type="hidden" id="Printid">
<!-- <a href="#" class="btn btn-warning" onclick="print();" accesskey="p" title="short key-ALT+P" ><i class="fa fa-print"></i> Print</a> -->
<!-- <input class="btn-large btn-primary btn" type="submit" value="Save and Print" name="submit">  -->

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
</div>
</section>

</section><!-- /.right-side -->

<script>

function save(){

      var postData = $("#post_form").serializeArray();
      // var ve_id = $('#ve_id').val();
      var formURL  = "<?= base_url() ?>preturn/returnPUpdate";

      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          // alert("Success");
          $('#Printid').val(data);
           // window.location = "<?php echo base_url();?>index.php/preturn/return_Plist";
        }
      });
}
// function print(){

//           var Printid         =  $('#Printid').val();
//           if(Printid=="")
//           {
//             alert("Click Save Before Print");
//           }
//           else
//           {
//           window.location = "<?php echo base_url();?>index.php/sales/getPrint?sPrintid="+Printid;
//           }

// }
</script>
<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'dd-mm-yyyy',
      today:true
    });</script>
