<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<!DOCTYPE html>
<html>


<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Hospital Management</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <meta property="og:image" content="<?= base_url() ?>asset/img/retail360logblack.png">
  <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>asset/img/favicon.ico">
  <link rel="stylesheet" href="<?= base_url() ?>asset/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->



  <script type="text/javascript">
function showmsg()
{
  alert("Njanjaayi....");
}
  </script>
</head>
<body class="hold-transition login-page">



<div class="login-box">
  <br> <br><br> <br>
  <div class="login-logo">
    
    <!-- <a href="../../index2.html"> </a> -->
  </div>
  <!-- /.login-logo -->


  <div class="login-box-body">

     <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>invalid</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
              <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Invalid</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
    <?php } ?>
    <p class="login-box-msg"><center> <img src="<?= base_url() ?>uploads/company/6.png" height="100" width="170" class="img-responsive"> </center></p>
           <?php //echo form_open('Login/log/') ?>

 <form action="<?php echo base_url();?>login/log" method="post">
 <div class="form-group has-feedback">
       <label for="username" class="control-label">Username</label>
        <input type="User Name" style="border-radius:5px" name="username" id='username'  class="form-control" >
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>

      <div class="form-group has-feedback">
        <label for="password" class="control-label">Password</label>
        <input type="password" style="border-radius:5px" name="password"  id="pass" class="form-control">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>

      

      <div class="form-group has-feedback">
        <button type="submit" class="btn btn-info btn-sm btn-block" >Sign In</button>
      </div>
  <div class="checkbox icheck">
            <label>
              <input value="remember-me" id="remember_me" type="checkbox" > Remember Password
            </label>
          </div>
  <p class="login-box-msg">  </p>
      <div class="row">
        <div class="col-xs-4">
          
        </div>
        <center><img src="<?= base_url() ?>uploads/retail360logblack.png"  height="80" width="120" ALIGN="RIGHT"  class="img-responsive"> </center>  
        <!-- /.col -->
       <!--  <div class="col-xs-4">
          <button type="submit" class="btn btn-warning">Sign In</button>
        </div> -->
        <!-- /.col -->
      </div>
    </form>

    <!-- <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a>
    </div> -->
    <!-- /.social-auth-links -->

   <!--  <a href="" onclick="showmsg();">I forgot my password</a><br> -->
    <!-- <a href="<?php echo base_url();?>index.php/Login/getregister" class="text-center">Register a new membership</a> -->

  </div>
   
 <!--   <P><h3><font color="red">USERNAME:<font color="BLUE"> admin<h3></P>
  <P><h3><font color="red">PASSWORD:<font color="BLUE"> user<h3></P> -->
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="<?= base_url() ?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?= base_url() ?>asset/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?= base_url() ?>asset/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
 
 <!-- remember password -->

  <!-- <script src="http://code.jquery.com/jquery-1.9.1.js"></script> -->
        <script>
            $(function() {
 
                if (localStorage.chkbx && localStorage.chkbx != '') {
                    $('#remember_me').attr('checked', 'checked');
                    $('#username').val(localStorage.usrname);
                    $('#pass').val(localStorage.pass);
                } else {
                    $('#remember_me').removeAttr('checked');
                    $('#username').val('');
                    $('#pass').val('');
                }
 
                $('#remember_me').click(function() {
 
                    if ($('#remember_me').is(':checked')) {
                        // save username and password
                        localStorage.usrname = $('#username').val();
                        localStorage.pass = $('#pass').val();
                        localStorage.chkbx = $('#remember_me').val();
                    } else {
                        localStorage.usrname = '';
                        localStorage.pass = '';
                        localStorage.chkbx = '';
                    }
                });
            });
 
        </script>
 
 <!-- remember password -->

</body>
</html>



