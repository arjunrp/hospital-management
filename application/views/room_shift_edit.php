<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="<?= base_url() ?>asset/plugins/timepicker/bootstrap-timepicker.min.js"></script>
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/timepicker/bootstrap-timepicker.min.css">
  <script type="text/javascript" charset="utf-8">
 $(function()
  {
    $("#rs_rmno").autocomplete({
      source: [<?php
      $i=0;
      foreach ($get_rooms as $get_room){
        if ($i>0) {echo ",";}
        echo '{value:"' . $get_room['rm_no'] . '",id:"' . $get_room['rm_id'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          // $("#customerid").val(ui.item ? ui.item.vid : '');
          $("#rm_id").val(ui.item ? ui.item.id : '');
        }
      });   
  });   

  </script>
  <script> 
$(document).ready(function() {

  $('#rs_rmno').change(function(){
    var rm_id = $('#rm_id').val();
     $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>room_shift/getBeds/"+rm_id,
     success: function(data){
      $('#rs_bdno').empty();
      $("#rs_bdno").append('<option value="0">--Select--</option>');
      $.each(data, function(index) {
        $("#rs_bdno").append('<option value=' + data[index].bd_id +'>'+data[index].bd_no+'</option>');
      });
    }
  })
  });

  });
  </script>


  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#customer_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       IP 
       <small>Room Shift </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."ipregister"; ?>">IP</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/ipregister/add">
        <i class="fa fa-plus-circle"></i> Today's IP List
      </a>
    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>
      <?php
      foreach ($get_ips as $key => $get_ip) {
      $ip_ipno              = $get_ip['ip_ipno'];
      $ip_date              = date("d-m-Y",strtotime($get_ip['ip_date']));
      $ip_doctor            = $get_ip['u_name'];
      $ip_department        = $get_ip['dp_department'];
      $ip_mrd               = $get_ip['ip_mrd'];
      $bk_name              = $get_ip['p_title']." ".$get_ip['p_name'];
      $bk_phone             = $get_ip['p_phone'];
    }
      ?>

      <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row"> 
       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">Date : <sup></sup></label>
          <?=$ip_date ?>               
        </div>
        <div class="col-md-4">
          <label  class="control-label">IP No. :<sup></sup></label>
          <?=$ip_ipno ?>              
        </div>
         <div class="col-md-4"></div>
        <div class="col-md-3">
         <label  class="control-label">Doctor : <sup></sup></label>
          <?=$ip_doctor ?>
       </div>
         <div class="col-md-3">
         <label  class="control-label">Department : <sup></sup></label>
         <?=$ip_department ?>
       </div>

      </div>
      <div class="col-md-12"><br>
       <div class="col-md-2">
         <label  class="control-label">MRD : <sup></sup></label>
         <?=$ip_mrd ?>
       </div>
       <div class="col-md-4">
         <label  class="control-label">Patient's Name : <sup></sup></label>
         <?=$bk_name ?>
       </div>
       <div class="col-md-3">
         <label  class="control-label">Phone No. : <sup></sup></label>
         <?=$bk_phone ?>
       </div>
     </div>
          </div>
          <div class="row">
          <div class="col-md-12">
            <hr>

        <div class="col-md-2">
          <label  class="control-label">Adm Date<sup></sup></label>
          <input class="form-control validate[required] datepicker" tabindex="1"  type="text" name="rs_adm" value="<?=date("Y-m-d",strtotime($rs_adm)) ?>"> 
          <input class="form-control" type="hidden" name="rs_id" value="<?=$rs_id ?>"> 
        </div>
        <div class="bootstrap-timepicker">
                <div class="form-group col-md-2">
                  <label>Adm Time</label>
                  <div class="input-group">
                    <input type="text" name="rs_time" class="form-control timepicker" value="<?=date("h:i A",strtotime($rs_adm)) ?>">
                    <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                  </div>
                </div>
              </div>
        <div class="col-md-2">
          <label  class="control-label">Vacated Date<sup></sup></label>
          <input class="form-control datepicker" tabindex="2"  type="text" name="rs_dis" value="<?php if($rs_dis=="0000-00-00 00:00:00") { echo "0000-00-00"; } else { echo date("Y-m-d",strtotime($rs_dis)); } ?>"> 
        </div>
        <div class="bootstrap-timepicker">
                <div class="form-group col-md-2">
                  <label>Vac Time</label>
                  <div class="input-group">
                    <input type="text" name="rs_dtime" class="form-control timepicker" value="<?php if($rs_dis=="0000-00-00 00:00:00") { echo "00:00:00"; } else { echo date("h:i A",strtotime($rs_dis)); } ?>">
                    <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                  </div>
                </div>
              </div>
        <div class="col-md-2">
          <label  class="control-label">Room No.<sup></sup></label>
          <input class="form-control validate[required]" tabindex="3"  type="text" id="rs_rmno" value="<?=$rs_room ?>"> 
          <input class="form-control" type="hidden" name="rs_mrd" value="<?=$ip_mrd ?>">    
          <input class="form-control" type="hidden" name="rs_ip" value="<?=$ip_ipno ?>  "> 
          <input class="form-control" type="hidden" name="rs_rmno" id="rm_id" value="<?=$rs_rmno ?>">                 
        </div>

        <div class="col-md-2">
          <label  class="control-label">Bed No.<sup></sup></label>
          <select class="form-control validate[required]" id="rs_bdno" tabindex="4" name="rs_bdno"> 
            <option value="0">--Select--</option>
            <?php foreach ($rs_beds as $key => $rs_bed) {
                ?>
              <option value="<?php echo $rs_bed['bd_id']?>"<?php if($rs_bed['bd_id']==$rs_bdno) { ?> selected="selected" <?php } ?> ><?php echo $rs_bed['bd_no']?></option>
                <?php
              }?>
          </select>       
        </div>
      </div>
    </div>

          <div class="box-footer">
       <br>
            <div class="row">
              <div class="col-md-12">
                <div>
                  <button class="button btn btn-primary" tabindex="5" type="submit">Update</button>
                  <input class="btn-large btn-default btn" type="reset" value="Reset">
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php echo form_close(); ?>

        
</div>
</section>

</section><!-- /.right-side -->


<script>
 $('.datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });
$('.timepicker').timepicker({
      showInputs: false,
      minuteStep: 1,
    });
 </script>
<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>