
<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
 
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>


<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
       Test Contents
       <small> <?php echo $page_title; ?> </small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."testsubcategory"; ?>">Test Contents</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div align="right">
         <a title="short key-ALT+B" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."testscontent"; ?>"accesskey="b" ><i class="fa fa-mail-reply-all"></i> Back</a>
       </div>

     </div>

     <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row">
      <div class="col-md-12">
        <div class="col-md-3">
          <div class="form-group required">
            <label  class="control-label">Category<sup></sup></label>
            <select class="form-control input-sm" tabindex="1"  name="tc_tnameid"> 
              <option value="Select">--Select--</option>
              <?php foreach ($categories as $key =>$mw_test_name) {
                ?>
              <option value="<?php echo $mw_test_name['tn_id']?>"<?php if($mw_test_name['tn_id']==$catid) { ?> selected="selected" <?php } ?>><?php echo $mw_test_name['tn_name']?></option>
                <?php
              }?>
            </select>               
          </div>
        </div>
        <div class="col-md-3">
        <div class="form-group required">
          <label  class="control-label">Content Name<sup></sup></label>
          <input  class="form-control validate[required] input-sm" tabindex="2"  data-prompt-position="topRight:100" type="text" name="tc_content" value="<?=$tc_content  ?>">             
        </div>
      </div>
        <div class="col-md-3">
        <div class="form-group required">
          <label  class="control-label">Child<sup></sup></label>
          <input  class="form-control validate[required] input-sm" tabindex="3"  data-prompt-position="topRight:100" type="text" name="tc_child" value="<?=$tc_child  ?>">             
        </div>
      </div>
  <div class="col-md-3">
           <div class="form-group required">
          <label  class="control-label">Unit<sup></sup></label>
          <input  class="form-control validate[required] input-sm"  tabindex="4"  data-prompt-position="topRight:50" type="text" name="tc_unit" value="<?=$tc_unit  ?>">
           <input class="form-control"  type="hidden" name="tc_id" value="<?=$tc_id ?>">                
        </div>  
        </div>

         <div class="col-md-3">
         <div class="form-group required">
          <label  class="control-label">Men<sup></sup></label>
          <input  class="form-control validate[required] input-sm" tabindex="5" data-prompt-position="bottomRight:50" type="text" name="tc_men" value="<?=$tc_men  ?>">  
        </div>
        </div>

         <div class="col-md-3">
         <div class="form-group required">
          <label  class="control-label">Women<sup></sup></label>
          <input  class="form-control validate[required] input-sm"  tabindex="6" data-prompt-position="bottomRight:50"  type="text" name="tc_women" value="<?=$tc_women  ?>">  
        </div>
        </div>
      </div>



    </div>
      <br>
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            <div>
              <input class="btn-large btn-primary btn" type="submit"   tabindex="7" value="Submit" name="submit" accesskey="s" title="short key-ALT+S"> 
              <input class="btn-large btn-default btn" type="reset"  tabindex="8" value="Reset">
            </div>
          </div>

        </div>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</section>
  
</section><!-- /.right-side -->

 