<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script> 
$(document).ready(function() {

  $('#sbt_btn1').hide();
    var op_mrd          = $('#op_mrd').val();
    var rw_days         = $('#rw_days').val();
    var rw_count        = $('#rw_count').val();
    var p_renew_date    = $('#p_renew_date').val();
    var p_renew_visits  = $('#p_renew_visits').val();
    // alert(op_mrd)
    // alert(rw_days)
    // alert(rw_count)
    // alert(p_renew_date)
    // alert(p_renew_visits)

    var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
    var firstDate   = new Date(p_renew_date);
    var secondDate  = new Date();

    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

    // alert(firstDate)
    // alert(secondDate)
    // alert(diffDays)
    // alert(rw_days)

    if(diffDays>rw_days || rw_count<=p_renew_visits)  
    {
      
      $('#chk').val("r");
      $('#sbt_btn').hide();
      $('#sbt_btn1').show();
      // window.location = "<?= base_url() ?>patient/view/"+op_mrd;
    }



    $('#op_opno').blur(function(){
    var op_mrd          = $('#op_mrd').val();
    $.ajax({

        url : "<?= base_url() ?>opregister/get_previuos_visit",
        type: "POST",
        data : {op_mrd:op_mrd},
        success: function(data5){
      $('#pre_date').val(data5.trim());
     }
   });

  });

  });
  </script>


  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#customer_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>

<?php
foreach ($booking as $key => $bookings) {
 $bk_id       = $bookings['bk_id'];
 $op_no       = $bookings['bk_no'];
 $op_date     = $bookings['bk_date'];
 $op_bno      = $bookings['bk_no'];
 $op_shift    = $bookings['bk_shift'];
 $op_docid    = $bookings['bk_doc'];
 $op_doc      = $bookings['u_name'];
 $op_dep      = $bookings['bk_dept'];
 $op_fee      = $bookings['u_fees'];
 $op_mrd      = $bookings['bk_mrd'];
 $op_patient  = $bookings['bk_name'];
 $op_phone    = $bookings['bk_phone'];
 $op_address  = $bookings['p_address'];
 $op_street   = $bookings['p_street'];
 $op_age      = $bookings['p_age'];
 $op_sex      = $bookings['p_sex'];

 $renew_date   = $bookings['p_renew_date'];
 $renew_days   = $bookings['p_renew_days'];
 $renew_visits = $bookings['p_renew_visits'];
 $renew_count  = $bookings['p_renew_count'];

 $op_user     = $bookings['bk_user'];
}

?>

  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       OP
       <small>Create OP </small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."opregister"; ?>">OP</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div class="box-tools">
      <a class="btn   btn-sm btn-info pull-right  view-btn-create" href="<?php echo base_url();?>index.php/opregister">
        <i class="fa fa-plus-circle"></i> Today's OP List
      </a>
    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>

      <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row"> 
       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">OP No.<sup>*</sup></label>
          <input class="form-control input-sm" readonly type="text" value="<?= $op_no?>" name="op_opno" id="op_opno" autofocus>
          <input class="form-control" type="hidden" name="bk_id" value="<?=$bk_id?>"> 
          <input class="form-control" type="hidden" id="token" value="0"> 
          <input class="form-control" type="hidden" name="user_type" value="<?=$op_user?>">              
        </div>
        <div class="col-md-2">
          <label  class="control-label">Date<sup>*</sup></label>
          <input class="form-control input-sm" tabindex="1" readonly data-prompt-position="topRight:150" type="text" name="op_date" value="<?=$op_date ?>">                
        </div>
         
          <div class="col-md-2">
         <label  class="control-label">OP Type<sup>*</sup></label>
         <select class="form-control input-sm" id="op_type" tabindex="2" name="op_type"> 
          <option value="b">--Booking--</option>
          </select>  
       </div>
       <div class="col-md-1 bkno">
        <label  class="control-label">Bk No.<sup>*</sup></label>
          <input class="form-control input-sm" readonly tabindex="3" type="text" value="<?=$op_bno?>" name="bk_no">
       </div>
        <div class="col-md-3">
         <label  class="control-label">Doctor<sup>*</sup></label>
         <select class="form-control input-sm" id="op_doctor" tabindex="4" name="op_doctor">
          <option value="<?=$op_docid ?>"><?=$op_doc ?></option>
            </select> 
            <input class="form-control" type="hidden" name="op_doc_name" value="<?=$op_doc ?>"> 
            <input class="form-control" type="hidden" name="op_department" value="<?=$op_dep?>">   
            <input class="form-control" type="hidden" name="op_docfee" value="<?=$op_fee?>">    
       </div>
       <div class="col-md-2">
         <label  class="control-label">Shift<sup>*</sup></label>
         <select class="form-control input-sm" tabindex="5" id="op_shift" name="op_shift"> 
              <?php if($op_shift=="m") { ?><option value="m">--Morning--</option> <?php } ?>
              <?php if($op_shift=="e") { ?><option value="e">--Evening--</option> <?php } ?>
            </select> 
       </div>
      </div>
      <div class="col-md-12">
       <div class="col-md-2">
         <label  class="control-label">MRD<sup>*</sup></label>
         <input class="form-control input-sm" readonly tabindex="6" type="text" id="op_mrd" name="op_mrd" value="<?=$op_mrd ?>">
         <input class="form-control" type="hidden" id="rw_days" value="<?=$renew_days ?>"> 
         <input class="form-control" type="hidden" id="rw_count" value="<?=$renew_count ?>">    
       </div>
       <div class="col-md-2">
         <label  class="control-label">Patient's Name<sup></sup></label>
         <input class="form-control" readonly type="text" tabindex="7" value="<?=$op_patient ?>" name="op_name">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Phone No.<sup></sup></label>
         <input class="form-control" type="text" tabindex="8" value="<?=$op_phone ?>" name="p_phone">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Address<sup></sup></label>
         <input class="form-control input-sm" type="text" name="p_address" value="<?=$op_address?>">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Street<sup></sup></label>
         <input class="form-control input-sm" type="text" name="p_street" value="<?=$op_street?>">
       </div>      
       <div class="col-md-1">
         <label  class="control-label">Age<sup></sup></label>
         <input class="form-control input-sm" type="text" value="<?=$op_age ?>" name="p_age">
       </div>
     </div>
     <div class="col-md-12">

       <div class="col-md-2">
         <label class="control-label">Sex<sup></sup></label>
         <select class="form-control input-sm" name="p_sex" id="p_sex">
          <option value="F" <?php if($op_sex == "F") { ?>selected  <?php } ?>>Female</option>
          <option value="M" <?php if($op_sex == "M") {?> selected <?php } ?>>Male</option>
          <option value="C" <?php if($op_sex == "C") {?>selected <?php } ?>>Child</option>
         </select>
       </div>
       <div class="col-md-2">
         <label  class="control-label">Prevoius Visit Date<sup></sup></label>
         <input class="form-control input-sm" readonly type="text" id="pre_date">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Last Renewal<sup></sup></label>
         <input readonly class="form-control" tabindex="9" type="text" id="p_renew_date" value="<?=$renew_date ?>">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Visits after renewal<sup></sup></label>
         <input readonly class="form-control" tabindex="10" type="text" id="p_renew_visits" value="<?=$renew_visits ?>">
          <input class="form-control" type="hidden" id="chk" name="renew_chk" value="nr">
       </div>
     </div>
          </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-md-12">
                <div>
                  <button class="button btn btn-primary" id="sbt_btn" tabindex="6" type="submit">Add OP</button>
                  <button class="button btn btn-primary" id="sbt_btn1" tabindex="6" type="submit">Renew & Add OP</button>
                  <input class="btn-large btn-default btn" type="reset" value="Reset" tabindex="7">
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php echo form_close(); ?>

       <!--  <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover gridView" id="category_table">
                    <thead>
                      <tr>
                        <th>OP No.</th>
                        <th>Date</th>
                        <th>Doctor</th>
                        <th>Shift</th>
                        <th>MRD</th>
                        <th>Patient's Name</th>
                        <th>Phone No.</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                  <tbody> 
                    <?php if($output) { echo $output; } ?>
                  </tbody>
                  </table> 
                

          </div>
        </div>
      </div>
    </div>
  </div>
</div> -->
</div>
</section>

</section><!-- /.right-side -->


<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });</script>
<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>