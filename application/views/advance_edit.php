<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
<link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});

</script>

<?php
    $user_type = $this->session->id;
    ?>

<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
      Advance Payment
      <small>
       <?php echo $page_title; ?></small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."advance"; ?>">Advance Payment</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div align="right">
         <a title="short key-ALT+B" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."advance"; ?>"accesskey="b" ><i class="fa fa-mail-reply-all"></i> Back</a>
       </div>

     </div>

     <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row">
       <div class="col-md-12">
        <div class="col-md-2">
        <div class="form-group required">
          <label  class="control-label">Voucher No : <sup></sup></label> <?=$ve_vno?>               
        </div> </div>
        <!-- <div class="col-md-8"></div> -->
        <div class="col-md-2">
        <div class="form-group required">
         <label  class="control-label">Date : <sup></sup></label> <?=date("d-m-Y",strtotime($ve_date)) ?>             
        </div></div> 
        <div class="col-md-2">
         <div class="form-group required">
          <label  class="control-label">IP No. : <sup></sup></label> <?=$ve_customer?> 
        </div> </div>

        <div class="col-md-2">
         <div class="form-group required">
          <label  class="control-label">MRD :<sup></sup></label> <?=$ve_mrd?> 
        </div> </div>

        <div class="col-md-3">
       <div class="form-group required">
          <label  class="control-label">Name :<sup></sup></label> <?=$p_name?>             
        </div></div>
         <div class="col-md-2">
         <div class="form-group required">
          <label  class="control-label">Amount<sup></sup></label>
          <input  class="form-control validate[required]"  type="text" name="ve_apayable" value="<?=$ve_apayable?>">  
           <input type="hidden" name="ve_id" value="<?=$ve_id?>">    
            <input type="hidden" name="ve_user" value="<?=$user_type?>">             
        </div>
       </div>

     <div class="col-md-12"><br>
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            <div>
              <input class="btn-large btn-primary btn" type="submit" value="Submit" name="submit" accesskey="s" title="short key-ALT+S"> 
              <input class="btn-large btn-default btn" type="reset" value="Reset">
            </div>
          </div>

        </div>
      </div>
    </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</section>
  
</section><!-- /.right-side -->

 <script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });</script>
<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>