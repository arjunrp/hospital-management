

<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script> 

  <div class="content-wrapper">
  <section class="content-header">
    <h1>
     Vendor
      <small>View  Vendor </small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."vendor"; ?>">Vendor</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>

  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> View Supplier
       </h3>
       <div align="right">
        <a title="Edit" class="btn btn-sm btn btn-info" href="<?php echo $this->config->item('admin_url')."suppliers/edit/$sp_id"; ?>"><i class="fa fa-pencil-square-o"></i> Edit</a>
        <a title="Back" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."suppliers"; ?>"><i class="fa fa-mail-reply-all"></i> Back</a>
        </div>
     </div>
        

              <div class="box-body">
      <div class="row">
        <div class="col-md-12">


           <div class="col-md-3">
          <div class="form-group">
                        <label for="focusinput" class="control-label">Image(1600 X 400)</label>
                        
                        <?php if($image != "") { echo "<img id='blah' width='139px' height='150px' src='".$this->config->item("base_url")."uploads/supplier/".$image."'>"; } else { echo "<img id='blah' width='150px' height='150px' src='".$this->config->item("base_url")."uploads/Default2.png"."'>"; } ?>

                      </div>
                    </div>
          <div class="form-group col-md-3">
            <label  class="control-label">Supplier<sup>*</sup></label><br>
            <?=$sp_vendor ?>               
          </div>    
             <div class="form-group col-md-2">
             <label  class="control-label">LIC No. <sup>*</sup></label><br>
             <?=$sp_lic ?>
          </div> 
           <div class="form-group col-md-2">
            <label  class="control-label">GST No. <sup>*</sup></label><br>
            <?=$sp_gst_no ?>
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Phone <sup>*</sup></label><br>
            <?=$sp_phone ?>
          </div>
          <div class="form-group col-md-3">
            <label  class="control-label">Address <sup>*</sup></label><br>
            <?= $sp_address ?>
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Street <sup>*</sup></label><br>
            <?= $sp_street ?>
          </div>
          <div class="form-group col-md-2">
           <label  class="control-label"> Zip Code <sup>*</sup></label><br>
           <?= $sp_zip ?>
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Fax <sup>*</sup></label><br>
            <?=$sp_fax ?>  
          </div>
          <div class="form-group col-md-3">
             <label  class="control-label">Country <sup>*</sup></label><br>
              <?php foreach ($countries as $key => $countries) {
                 if($countries['id']==$sp_country) { echo $countries['name']; } } ?>
          </div>
        <div class="form-group col-md-2">
          <label  class="control-label">State <sup>*</sup></label><br>
              <?php foreach ($states as $key => $states) { 
                if($states['id']==$sp_state) { echo $states['name'];  } }
              ?>
          </div> 
          <div class="form-group col-md-2">
            <label  class="control-label">City <sup>*</sup></label><br>
              <?php foreach ($cities as $key => $cities) {
                 if($cities['id']==$sp_city) { echo $cities['name']; }
              } ?>
            </select>
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Website <sup>*</sup></label><br>
            <?=$sp_website ?>
          </div>
           <div class="form-group col-md-3">
            <label  class="control-label">Email <sup>*</sup></label><br>
            <?= $sp_email ?>
          </div>
           <div class="form-group col-md-3">
           <label  class="control-label"> Payement Type <sup>*</sup></label><br>
           <?php if($sp_payment=='c') {  echo "Cash"; } else if($sp_payment=='nc') {  echo "Credit" ; } ?>
          </div>
          
            </div> 
        </div>
      </div>        
</div>
</section>
</div><!-- /.right-side -->


