<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

  <link rel="stylesheet" href="<?= base_url() ?>asset/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script type="text/javascript" charset="utf-8">
 $(function()
  {
    $("#ve_mrd").autocomplete({
      source: [<?php
      $i=0;
      foreach ($patient as $patientmrd){
        if ($i>0) {echo ",";}
        echo '{value:"' . $patientmrd['op_mrd'] . '",patient:"' .$patientmrd['p_title'] ." " . $patientmrd['p_name'] . '",patientph:"' . $patientmrd['p_phone'] . '",patientop:"' . $patientmrd['op_id'] . '",doctor:"' . $patientmrd['u_name'] . '",patientage:"' . $patientmrd['p_age'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          // $("#customerid").val(ui.item ? ui.item.vid : '');
          $("#bk_name").val(ui.item ? ui.item.patient : '');
          $("#bk_phone").val(ui.item ? ui.item.patientph : '');
          $("#ve_customer").val(ui.item ? ui.item.patientop : '');
          $("#doctor").val(ui.item ? ui.item.doctor : '');
          $("#bk_age").val(ui.item ? ui.item.patientage : '');
        }
      });   
  });   

  $(function() {
    $("#bk_phone").autocomplete({
      source: [<?php
      $i=0;
      foreach ($patient as $patientmrd){
        if ($i>0) {echo ",";}
        echo '{value:"' . $patientmrd['p_phone'] . '",patient:"' .$patientmrd['p_title'] ." " . $patientmrd['p_name'] . '",patientmrd:"' . $patientmrd['op_mrd'] . '",patientop:"' . $patientmrd['op_id'] . '",doctor:"' . $patientmrd['u_name'] . '",patientage:"' . $patientmrd['p_age'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          // $("#customerid").val(ui.item ? ui.item.vid : '');
          $("#bk_name").val(ui.item ? ui.item.patient : '');
          $("#ve_mrd").val(ui.item ? ui.item.patientmrd : '');
          $("#ve_customer").val(ui.item ? ui.item.patientop : '');
          $("#doctor").val(ui.item ? ui.item.doctor : '');
          $("#bk_age").val(ui.item ? ui.item.patientage : '');
        }
      });   
  }); 

  $(function() {
    $("#ve_customer").autocomplete({
      source: [<?php
      $i=0;
      foreach ($ippatient as $ipno){
        if ($i>0) {echo ",";}
        echo '{value:"' . $ipno['ip_ipno'] . '",mrd:"' . $ipno['ip_mrd'] . '",patient:"'.$ipno['p_title'] ." " . $ipno['p_name'] . '",patientph:"' . $ipno['p_phone'] . '",doctor:"' . $ipno['u_name'] . '",patientage:"' . $ipno['p_age'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          $("#ve_mrd").val(ui.item ? ui.item.mrd : '');
          $("#bk_name").val(ui.item ? ui.item.patient : '');
          $("#bk_phone").val(ui.item ? ui.item.patientph : '');
          $("#doctor").val(ui.item ? ui.item.doctor : '');
          $("#bk_age").val(ui.item ? ui.item.patientage : '');
        }
      });  

     $(function() {
    $("#doctor").autocomplete({
      source: [<?php
      $i=0;
      foreach ($doctors as $doctor){
        if ($i>0) {echo ",";}
        echo '{value:"' . $doctor['u_name'] . '"}';
        $i++;
      }
      ?>],
        minLength: 1,//search after one characters
        select: function(event,ui){
          // $("#customerid").val(ui.item ? ui.item.vid : '');
        }
      });   
  }); 

  });   

  </script>
  <script> 
$(document).ready(function() {

  $('#ve_mrd').change(function(){

    var uRL1   = "<?= base_url() ?>investigation/get_gender/"+$(this).val();

    $.ajax({

        url : uRL1,
        dataType: "json",
        success:function(data3)
        {
          $.each(data3, function(index) {
            var selected = "selected";

            if(data3[index].p_sex=="F") { var selected1 = selected; }
            else if(data3[index].p_sex=="M") { var selected2 = selected; }
            else { var selected3 = selected; }

              $("#bk_gender").empty();
              $("#bk_gender").append('<option value="F" '+selected1+'>Female</option>');
              $("#bk_gender").append('<option value="M" '+selected2+'>Male</option>');
              $("#bk_gender").append('<option value="C" '+selected3+'>Child</option>');
          });
        }
      });
  });

  $('#ve_customer').change(function(){
    var ve_mrd = $('#ve_mrd').val();
    var uRL1   = "<?= base_url() ?>investigation/get_gender/"+ve_mrd;
    $.ajax({

        url : uRL1,
        dataType: "json",
        success:function(data3)
        {
          $.each(data3, function(index) {
            var selected = "selected";
            if(data3[index].p_sex=="F") { var selected1 = selected; }
            else if(data3[index].p_sex=="M") { var selected2 = selected; }
            else { var selected3 = selected; }

              $("#bk_gender").empty();
              $("#bk_gender").append('<option value="F" '+selected1+'>Female</option>');
              $("#bk_gender").append('<option value="M" '+selected2+'>Male</option>');
              $("#bk_gender").append('<option value="C" '+selected3+'>Child</option>');
          });
        }
      });
  });

  $('input[type="checkbox"]'). click(function(){
  if($(this). prop("checked") == true){
  $("#payment_type").val("True");
  }
  else if($(this). prop("checked") == false){
  $("#payment_type").val("False");
  } 
  });

  $('.ipno').hide();
  $('#ip_op').change(function(){

        $('#ve_mrd').val('');
        $('#bk_name').val('');
        $('#bk_phone').val('');
        $('#ve_customer').val('');
        $('#bk_age').val('');
        $('#bk_gender').val('');

    var ip_op = $('#ip_op').val();
    {
      if(ip_op=="ip")
      {
        $('.ipno').show();
        $('.ipno1').hide();
      }
      else 
      {
        $('.ipno').hide();
        $('.ipno1').show();
      }
    }
  });

  $('#xray_tests').change(function(){
    var xray_tests = $('#xray_tests').val();
     $.ajax({
     dataType: "json",
     url: "<?= base_url() ?>xraytest/getXraynames/"+xray_tests,
     success: function(data){
      $('#xray_xname').empty();
      $('#xray_xprice').val("");
      $("#xray_xname").append('<option value="0">--Select--</option>');
      $.each(data, function(index) {
        $("#xray_xname").append('<option value=' + data[index].xt_id +'>'+data[index].xt_name+'</option>');
      });
    }
  })
  });

   $('#xray_xname').change(function(){
    var xray_sname = $('#xray_xname').val();
    var uRL1   = "<?= base_url() ?>xraytest/xray_price";
    $.ajax({
        url : uRL1,
        type: "POST",
        data : {xname:xray_sname},
        success:function(data2, textStatus, jqXHR)
        {
          $('#xray_xprice').val(data2);
        }
  })
  });

  });
  </script>

  <script>
  $(document).ready(function() {

  $('.button').click(function() {
    var sel = document.getElementById("xray_xname");
    var value = sel.options[sel.selectedIndex].value; // or sel.value
    var text = sel.options[sel.selectedIndex].text; 

    var xray_xid        = value;
    var xray_xname      = text;
    var xray_xprice     = $('#xray_xprice').val();
    
    var newrow      = '<tr><td><input type="hidden" value="'+xray_xid+'" name="ved_itemid[]"><input class="form-control input-sm" type="hidden" value="'+xray_xname+'" name="ved_item[]">' + xray_xname + '</td><td><input class="labmrp" type="hidden" value="'+xray_xprice+'" name="ved_total[]"><b>Rs. </b><label>'+xray_xprice+'</label></td><td><button class="btn btn-xs btn-danger dlte"><i class="fa fa-times-circle-o"></i></button></td></tr>';
    var lab_mrp     = $('#lab_mrp').val();

    lab_mrp         = parseFloat(lab_mrp) + parseFloat(xray_xprice);
    lab_mrp         = (Math.round(lab_mrp * 100) / 100).toFixed(2);

    $('#lab_mrp').val(lab_mrp);
    $('#lab_mrp1').html(lab_mrp);

    $('#item_table tr:last').after(newrow);
});

$(document).on('click','button.dlte', function() {
    var $row    = $(this).closest("tr");    // Find the row

    var r = confirm("Are you sure want to delete!");
    if (r == true) {
    $(this).closest('tr').remove();

    var grand = 0;

    $('.labmrp').each(function(){
        grand += parseFloat(this.value);
    });

    grand           =   (Math.round(grand * 100) / 100).toFixed(2);

    $('#lab_mrp').val(grand);
    $('#lab_mrp1').html(grand);

    } 
    });

  });
  </script>


  <script type="text/javascript" charset="utf-8">
  $(function() {
    $( "#customer_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
      autoclose: true
    });
  });
  </script>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
  <section class="right-side" style="min-height:700px;"> 
    <?php
    $user_type = $this->session->id;
    ?>
    <section class="content-header">
      <h1>
       Xray Room
       <small>New</small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."xraytest"; ?>">Xray List</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div class="box-tools">
     
    </div>
 <br><br>
    <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
     </div>

      <form id="post_form">
     <div class="box-body">
      <div class="row"> 
       <div class="col-md-12">
        <div class="col-md-2">
          <label  class="control-label">Voucher No.<sup>*</sup></label>
          <input class="form-control input-sm" type="text" id="ve_vno" name="ve_vno" tabindex="1" value="<?=$xray_no?>"> 
          <input class="form-control" type="hidden" name="user_type" value="<?=$user_type?>">              
        </div>
        <div class="col-md-2">
          <label  class="control-label">Date<sup></sup>*</label>
          <input readonly class="form-control validate[required] input-sm"  tabindex="2" data-prompt-position="topRight:150" type="text" name="ve_date" id="datepicker" value="<?=date("Y-m-d") ?>"> 
          <input readonly class="form-control input-sm" type="hidden" name="ve_time" value="<?=date("h:i A") ?>">           
        </div>
         <div class="col-md-2">
          <label  class="control-label">IP/OP<sup>*</sup></label>
          <select class="form-control validate[required] input-sm"  tabindex="3" name="ip_op" id="ip_op">
            <option value="op">---OP---</option>
            <option value="ip">---IP---</option>
          </select>
         </div>
       </div>
       <div class="col-md-12"><br>
         <div class="col-md-2 ipno">
         <label  class="control-label">IP No.<sup>*</sup></label>
         <input class="form-control validate[required] input-sm" data-prompt-position="topLeft:190" tabindex="5" type="text" name="ve_customer" id="ve_customer"></div>

          <div class="col-md-2 ipno1">
         <label  class="control-label">MRD<sup>*</sup></label>
         <input class="form-control validate[required] input-sm" data-prompt-position="topLeft:190" tabindex="4" type="text" name="ve_mrd" id="ve_mrd"></div>
       <div class="col-md-2">
         <label  class="control-label">Patient's Name<sup></sup></label>
         <input class="form-control  input-sm"  data-prompt-position="bottomLeft:190"  tabindex="5" type="text" id="bk_name" name="ve_patient">
         <input type="hidden" name="ve_supplier" value="21">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Phone No.<sup></sup></label>
         <input class="form-control   input-sm" data-prompt-position="topLeft:190" tabindex="6" type="text" id="bk_phone" name="ve_phone">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Age<sup></sup></label>
         <input class="form-control input-sm validate[required]" data-prompt-position="topLeft:190" tabindex="6" type="text" id="bk_age" name="ve_age">
       </div>
       <div class="col-md-2">
         <label  class="control-label">Gender<sup></sup></label>
         <select class="form-control validate[required] input-sm" id="bk_gender" tabindex="8" name="ve_gender">
          <option value="F">Female</option>
          <option value="M">Male</option>
          <option value="C">Child</option>
            </select>  
       </div>

      </div>
      <div class="col-md-12">
      <br>
         <div class="col-md-2">
         <label  class="control-label">Scans<sup></sup></label>
         <select class="form-control validate[required] input-sm" id="xray_tests" tabindex="7" name="xray_tests"> 
          <option value="0">--Select--</option>
           <?php foreach ($xray_tests as $key => $xray_test) {
                ?>
              <option value="<?php echo $xray_test['xr_id']?>"><?php echo $xray_test['xr_test']?></option>
                <?php
              }?>
          </select>  
       </div>
        <div class="col-md-2">
         <label  class="control-label">Scan Name<sup></sup></label>
         <select class="form-control validate[required] input-sm" id="xray_xname" tabindex="8" name="xray_xname">
          <option value="0">--Select--</option>
            </select>  
       </div>
       <div class="col-md-2">
         <label  class="control-label">Price<sup></sup></label>
         <input class="form-control validate[required,custom[number]] input-sm" id="xray_xprice" tabindex="9" data-prompt-position="bottomRight:140" name="xray_xprice"> 
       </div>
       <div class="col-md-2">
        <label  class="control-label">Credit<sup></sup></label><br>
         <input id="payment_type" type="checkbox" name="payment_type">
       </div>
       <div class="col-md-2"></div>
       <div class="col-md-2">
         <label  class="control-label">Doctor<sup></sup></label>
         <input class="form-control input-sm" id="doctor" name="ve_doctor"> 
       </div>
     </div>
          </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-md-12">
                <div>
                  <a class="button btn btn-primary" >Add to List</a>
                  <input class="btn-large btn-default btn"  tabindex="9" type="reset" value="Reset">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Particular</th>
                        <th>Price</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                  <tbody> 

                  </tbody>
                  </table> 
                  <table class="table table-bordered">
                    <tr><th width="80%">Total</th>
                      <th><input type="hidden" name="lab_mrp" readonly id="lab_mrp" value="0">Rs. <label id="lab_mrp1">0</label></th></tr>
                    </table>

                    <a href="#" class="btn btn-success" onclick="save();" accesskey="s" title="short key-ALT+S"><i class="fa fa-floppy-o"></i> Save</a>
                  <input type="hidden" id="Printid"> 
                    <a href="#" class="btn btn-warning" onclick="save_print();" accesskey="p" title="short key-ALT+P" ><i class="fa fa-print"></i> Save & Print</a> 
                
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</form>
</div>
</section>

</section><!-- /.right-side -->

<script>
function save(){

      var postData = $("#post_form").serializeArray();
      var formURL  = "<?= base_url() ?>xraytest/xraytestAdd";

      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          alert("Success");
          $('#Printid').val(data);
           window.location = "<?php echo base_url();?>index.php/xraytest/add";
        }
      });
}
function save_print(){

      var postData = $("#post_form").serializeArray();
      var formURL  = "<?= base_url() ?>xraytest/xraytestAdd";

      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
           window.location = "<?php echo base_url();?>index.php/xraytest/getBillPrint?Printid="+data;
        }
      });
}
</script>

<script>
 $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      today:true
    });</script>
<script>
 setTimeout(function() {
          $('.alert').fadeOut('fast');
        }, 1000);
</script>