          <input class="form-control input-sm"   type="text" id="product">
          <input class="form-control"   type="hidden" id="product_name">
          <input class="form-control"   required="required"  type="hidden" name="productid" id="productid">
          <input class="form-control" type="hidden" id="product_uqty">
             <script type="text/javascript">

              $("#product").autocomplete({

                source: [<?php
                $i=0;
                $unit="";
                foreach ($products as $product){
                  if ($i>0) {echo ",";}
                  echo '{value:"' .$product['ved_item']." - ". $product['ved_batch']  . '",productname:"' . $product['ved_item'] . '",pid:"' . $product['ved_itemid'] . '",productunit:"' .$product['pd_unit']. '",productbatch:"' . $product['ved_batch'] . '",productexpiry:"' . $product['ved_expiry'] . '",productstock:"' . $product['stock_qty'] / $product['pd_qty'] . '",productuqty:"' . $product['pd_qty'] . '"}';
                  $i++;
                }
                ?>
                ],
                minLength: 3,//search after one characters
                delay: 300 ,
                select: function(event,ui){

                  $("#productid").val(ui.item ? ui.item.pid : '');
                  $("#product_name").val(ui.item ? ui.item.productname : '');
                  $("#product_uqty").val(ui.item ? ui.item.productuqty : '');
                  $("#product_unit").val(ui.item ? ui.item.productunit : '');
                  $("#batch").val(ui.item ? ui.item.productbatch : '');
                  $("#expiry").val(ui.item ? ui.item.productexpiry : '');
                  $("#product_stock").val(ui.item ? ui.item.productstock : '');
                }
              });
             </script>