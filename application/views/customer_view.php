<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>
<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
     Customer
      <small>View  Customer </small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."customer"; ?>">Customers</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?=$customer  ?>
       </h3>

       <div align="right">
        <a title="Edit" class="btn btn-sm btn btn-info" href="<?php echo $this->config->item('admin_url')."customer/edit/$id"; ?>"><i class="fa fa-pencil-square-o"></i> Edit</a>
        <a title="Back" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."customer"; ?>"><i class="fa fa-mail-reply-all"></i> Back</a>
        </div>
     </div>


     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">
          <div class="form-group col-md-2">
          <label for="focusinput" class="control-label">Image</label><br>
         <?php if($image != "") { echo "<img width='150px' height='150px' src='".$this->config->item("base_url")."uploads/customer/".$image."'>"; } else { echo "<img width='150px' height='150px' src='".$this->config->item("base_url")."uploads/Default.png"."'>"; } ?>
          </div>

          <div class="form-group col-md-3">
            <label  class="control-label">Address(street1)<sup>*</sup></label><br>
            <?= $address ?>
          </div>


          <div class="form-group col-md-3">
            <label  class="control-label">Street<sup>*</sup></label><br>
            <?= $street ?>             
          </div>

          <div class="form-group col-md-2">
            <label  class="control-label">City<sup>*</sup></label><br>
            <?php foreach ($cities as $key => $cities) {
              if($cities['id']==$city) { echo $cities['name']; } 
              } ?>
          </div>


          <div class="form-group col-md-2">
            <label  class="control-label">State<sup>*</sup></label><br>
           <?php foreach ($states as $key => $states) {
              if($states['id']==$state) { echo $states['name']; } 
              } ?>                 
          </div>


          <div class="form-group col-md-3">
            <label  class="control-label">Country<sup>*</sup></label><br>
            <?php foreach ($countries as $key => $countries) {
              if($countries['id']==$country) { echo $countries['name']; } 
              } ?>
            </div>

           <div class="form-group col-md-3">
            <label  class="control-label">Zip<sup>*</sup></label><br>
            <?= $zip ?>
          </div>

          <div class="form-group col-md-3">
             <label  class="control-label">Phone<sup>*</sup></label><br>
            <?= $phone  ?>                 
          </div> 
           
           <div class="form-group col-md-3">
            <label  class="control-label">Fax<sup>*</sup></label><br>
            <?= $fax  ?>          
          </div>
          <div class="form-group col-md-3">
            <label  class="control-label">Email<sup>*</sup></label><br>
            <?= $email  ?>             
          </div> 

           <div class="form-group col-md-3">        
          <label  class="control-label">Website<sup>*</sup></label><br>
          <?= $website  ?>                
          </div>

        </div>
      </div>
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            <table id="" class="table table-condensed dataTable no-footer">
                       <thead>
                        <tr><th style="text-align:center" colspan=6>Sales Details</th></tr>
                        <tr>
                          <th>Sl. No.</th>
                          <th>Sale Date</th>
                          <th>Sale Amount</th>
                          <th>Payment Type</th>
                          <th>Amount Paid</th>
                          <th>Balance To Pay</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php  echo $output;  ?>
                      </tbody>
             </table>
             <table class="table table-bordered">
              <?php
                  $samount=0;
                  $spdamount=0;
                  $sblamount=0;
                  if (is_array($output1) || is_object($output1))
                  {
                  foreach($output1 as $tsum)
                  {
   
                    $samount    = $samount   + $tsum['samount1'];
                    $spdamount  = $spdamount + $tsum['spdamount1'];
                    $sblamount  = $sblamount + $tsum['sblamount1']; 
                  }
                }
              ?>
              <thead>
                <tr><th width="85%">Total Sale Amount</th><th><?="Rs. ".$samount ?></th></tr>
                <tr><th width="85%">Total Amount Paid</th><th><?="Rs. ".$spdamount ?></th></tr>
                <tr><th width="85%">Total Balance to Pay</th><th><?="Rs. ".$sblamount ?></th></tr>
              </thead>
             </table>
          </div>

        </div>
      </div>
      
    </div>
  </div>
</section>

</section><!-- /.right-side -->


