<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>

<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
      Booking Token
      <small> Booking Token </small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>">Dashboard</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."booking_token"; ?>">Booking Token</a></li>
      <li class="active"><?php echo $page_title; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <?php if($this->session->flashdata('Success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?php echo $page_title; ?>
       </h3>
       <div align="right">
         <a title="short key-ALT+B" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."booking_token"; ?>" accesskey="b"><i class="fa fa-mail-reply-all"></i> Back</a>
       </div>

     </div>

     <?php echo form_open_multipart($action) ?>
     <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-3">
          <div class="form-group required">
            <label  class="control-label">Doctor<sup>*</sup></label>
            <select class="form-control validate[required] input-sm" tabindex="1" name="dc_docid" value="<?=$dc_docid  ?>"> 
              <option value="Select">--Select--</option>
              <?php foreach ($doctors as $key => $doctors) {
                ?>
              <option value="<?php echo $doctors['u_emp_id']?>"<?php if($doctors['u_emp_id']==$dc_docid) { ?> selected="selected" <?php } ?>><?php echo $doctors['u_name']?></option>
                <?php
              }?>
            </select>               
          </div>
        </div>
          <div class="col-md-2">
          <div class="form-group required">
            <label  class="control-label">Morning Tokens<sup>*</sup></label>
            <input  class="form-control validate[required] input-sm"  data-prompt-position="topRight:110" tabindex="2" type="text" name="dc_morntok" value="<?=$dc_morntok  ?>">          
          </div>
        </div>
          <div class="col-md-2">
          <div class="form-group required">
            <label  class="control-label">Evening Tokens<sup>*</sup></label>
            <input  class="form-control validate[required] input-sm" data-prompt-position="bottomRight:150" tabindex="3" type="text" name="dc_evngtok" value="<?=$dc_evngtok  ?>">           
          </div>
        </div>
      </div>
      </div>
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            <div>
              <input class="btn-large btn-primary btn" tabindex="4" type="submit" value="Submit" tabindex="4" name="submit" accesskey="s" title="short key-ALT+S"> 
              <input class="btn-large btn-default btn" type="reset" tabindex="5" value="Reset">
            </div>
          </div>

        </div>
      </div>
      <?php echo form_close(); ?>
    </div>

  </div>
  <br><br><br><br><br><br><br><br><br><br><br> 
</section>

</section><!-- /.right-side -->


