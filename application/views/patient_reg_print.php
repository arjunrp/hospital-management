<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<html>
<head>
<link rel="stylesheet" href='<?= base_url() ?>asset/plugins/jvectormap/jquery-jvectormap-1.2.2.css' type="text/css"/>
  <link media="all" type="text/css" rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.css">
   <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.min.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.min.css' type="text/css"/>
   <link rel="stylesheet" href="<?= base_url() ?>asset/css/skins/_all-skins.min.css"> 
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
 
  <!-- <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datepicker/datepicker3.css"> -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<!-- <link rel="stylesheet" href="<?= base_url() ?>asset/css/AdminLTE.min.css" type="text/css"/>  -->
   <link rel="stylesheet" href="<?= base_url() ?>asset/bootstrap/css/AdminLTE.min.css" type="text/css"/>
   <script  src="<?= base_url() ?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
   <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
   <style type="text/css">
   .font_reduce
   {
     font-size:small;
   }
   </style>
</head>
 <body style="font-family: Arial Black, Georgia, Serif; margin-top:10%;display:block;height:100%;width:80%;margin-left:9%;margin-right:6% ">

<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->

    <div class="box-tools">
              <a title="Back" class="dontprint btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."patient/add"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
       <font size="+.5"> <center> <address>
          <b>MRD No : <font size="+2"><?=$p_mrd_no ?> </font></b><br></center>
          <b>Name : <?=$p_name ?>,&emsp;<?=$p_address?> </b><br>
            <b>&emsp;&emsp;&nbsp;  </b>  <br>  <br> 
           <b>Age : <?=$p_age ?> </b> &emsp;&emsp;
           <b>Sex : <?=$p_sex ?> </b>&emsp;&emsp;
           <b>Blood : <?=$p_blood?> </b><br> <br> 
           <b>Phone : <?=$p_phone ?> </b> </font>
        </address> 
        </div>
      <!-- /.col -->

      
    </div>


  </section>

</div>

 
 <style type="text/css" media="print">
.dontprint
{ display: none; }
</style>
</body>
</html>
