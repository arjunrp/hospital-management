
<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
 
<?php
foreach($porders as $porder)
{
  $po_id          = $porder['po_id'];
  $po_pono        = $porder['po_pono'];
  $po_date        = $porder['po_date'];
  $po_supplier    = $porder['po_supplier'];
  $sp_vendor      = $porder['sp_vendor'];
  $po_amount      = $porder['po_amount'];
  $po_dlvry_date  = $porder['po_dlvry_date'];
  $po_dlvry_type  = $porder['po_dlvry_type'];
  $po_avail       = $porder['po_avail'];
  // $u_emp_id       = $porder['u_name'];

}

?>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>

<section class="right-side" style="min-height:700px;">
  <section class="content-header">
    <h1>
     Purchase Order
      <small>View Purchase Order</small>
    </h1>

    <ol class="breadcrumb">
      <li><a href=" ">Dashboard</a></li>
      <li><a href=" ">Purchase Order</a></li>
      <li class="active"></li> <?=$page_title?>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i>  <?=$page_title?>
       </h3>
        <div class="box-tools">
              <a title="Back" class="btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."porder"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>
     </div>

     <form id="post_form">
     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">
           
            <div class="form-group col-md-2">
            <label  class="control-label">PO No. :</label> <?= $po_pono  ?><br>
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Date :</label> <?= date("d-m-Y",strtotime($po_date))  ?><br>
          </div>

          <div class="form-group col-md-2">
            <label  class="control-label">Supplier ID :</label> <?= $po_supplier  ?> <br>         
          </div>

          <div class="form-group col-md-3">
            <label  class="control-label">Vendor :</label> <?= $sp_vendor  ?><br>
          </div>

          <div class="form-group col-md-3">
            <label  class="control-label">Delivery Status :</label> <?php if($po_avail==0) { echo "Not Delivered"; } else if($po_avail==1) { echo "Delivered"; }   ?><br>
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Delivery On :</label> <?= date("d-m-Y",strtotime($po_dlvry_date))  ?><br>
          </div>
          <div class="form-group col-md-2">
            <label  class="control-label">Delivery Type :</label> <?= $po_dlvry_type  ?><br>
          </div>
<!-- 
          <div class="form-group col-md-3">    
          <label class="control-label">User ID : </label> <?= $u_emp_id ?>
          </div> -->

 <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
            </div>
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">

               
                   <table class="table table-condensed dataTable no-footer gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Sl. No </th>
                        <th>Product</th>
                        <th>Unit Price</th>
                        <th>Qty</th>
                        <th>Offer Qty</th>
                        <th>SGST</th>
                        <th>CGST</th>
                        <th>Sub Total</th>
                      </tr>
                    </thead>
                  <tbody> 
                    <?php
                    $slno=1;
                    $sum=0;
                    $cgst=0;
                    $sgst=0;
                    foreach($porders as $porder)
                      { ?> 
                    <tr>
                      <td><?= $slno++  ?></td>
                      <td><a href="<?= $this->config->item('admin_url')."product/view/".$porder['pod_item_id'] ?>"><?=$porder['pod_item'] ?></a></td>
                      <td><?=$porder['pod_price'] ?></td>
                      <td><?=$porder['pod_qty']." ".$porder['pod_unit']  ?></td>
                      <td><?=$porder['pod_offer_qty']." ".$porder['pod_unit']  ?></td>
                      <td><?=$porder['pod_sgsta']." (".$porder['pod_sgstp']."%)"  ?></td>
                      <td><?=$porder['pod_csgta']." (".$porder['pod_cgstp']."%)"  ?></td>
                      <td><?=$porder['pod_total'] ?></td>
                      <?php
                      $sum     =     $sum + ($porder['pod_price'] * $porder['pod_qty']);
                      $cgst    =     $cgst + $porder['pod_csgta'];
                      $sgst    =     $sgst + $porder['pod_sgsta'];
                      ?>
                    </tr>
                    <?php } ?>
                  </tbody>
                  </table> 
                
                  <table class="table table-bordered">
                     <tr><th width="80%">Total</th>
                      <th>Rs. <label><?= $sum ?></label></th></tr>
                      <tr><th width="80%">SGST Total</th>
                      <th>Rs. <label><?= $sgst ?></label></th></tr>
                      <tr><th width="80%">CGST Total</th>
                      <th>Rs. <label><?= $cgst ?></label></th></tr>
                    <tr><th width="80%">Grand Total </th>
                      <th>Rs. <label><?= $po_amount ?></label></th></tr>
                    </table>
   
<br>
<input type="hidden" id="Printid" value="<?=$po_id?>">
 <a href="#" class="btn btn-warning" onclick="print();"><i class="fa fa-print"></i> Print PO</a>
<!-- <input class="btn-large btn-primary btn" type="submit" value="Save and Print" name="submit">  -->

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
           

        </div>
      </div>
      
      
    </div>
  </form><br>
  </div>
</section>

</section><!-- /.right-side -->

<script>

function print(){

          var Printid         =  $('#Printid').val();
          window.location = "<?php echo base_url();?>index.php/porder/getPrint?pPrintid="+Printid;
}
</script>

