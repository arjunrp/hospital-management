<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>

<html>
<head>
<link rel="stylesheet" href='<?= base_url() ?>asset/plugins/jvectormap/jquery-jvectormap-1.2.2.css' type="text/css"/>
  <link media="all" type="text/css" rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.css">
   <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.min.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.min.css' type="text/css"/>
   <link rel="stylesheet" href="<?= base_url() ?>asset/css/skins/_all-skins.min.css"> 
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
 
  <!-- <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datepicker/datepicker3.css"> -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<!-- <link rel="stylesheet" href="<?= base_url() ?>asset/css/AdminLTE.min.css" type="text/css"/>  -->
   <link rel="stylesheet" href="<?= base_url() ?>asset/bootstrap/css/AdminLTE.min.css" type="text/css"/>
   <script  src="<?= base_url() ?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
   <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
   <style type="text/css">
   .font_reduce
   {
     font-size:small;
   }
   .font_reduce1
   {
     font-size:x-small;
   }
   .font_reduce2
   {
     font-size:xx-small;
   }
   .font_reduce3
   {
     font-size:medium;
   }

   .u {
    text-decoration: underline;
}
   </style></head>
 <body style="margin-top:0%;display:block;height:100%;width:80%;margin-left:4%;" onload="window.print();">

<!-- onload="window.print();"  -->
  <?php foreach($company as $company) {
    $company_name     = $company['company'];
    $company_address  = $company['address'];
    $company_street   = $company['street'];
    $company_city     = $company['ctname'];
    $company_state    = $company['stname'];
    $company_country  = $company['coname'];
    $company_zip      = $company['zip'];
    $company_phone    = $company['phone'];
    $company_email    = $company['email'];
    $company_website  = $company['website'];
    $company_gst_no   = $company['gst_no'];
    $company_drug_lic = $company['drug_lic'];
    $company_tags     = $company['tags'];

foreach ($discharges as $discharge) {

  $ve_vno         = $discharge['ve_vno'];
  $ve_date        = $discharge['ve_date'];
  $ve_customer    = $discharge['ve_customer'];
  $ve_mrd         = $discharge['ve_mrd'];
  $p_name         = $discharge['p_title']." ".$discharge['p_name'];
  $p_phone        = $discharge['p_phone'];
  $u_name         = $discharge['u_name'];
  $dp_department  = $discharge['dp_department'];
  $ve_user        = $discharge['ve_user'];
  $ve_amount      = $discharge['ve_amount'];
  $ve_discount    = $discharge['ve_discount'];
  $ve_discounta   = $discharge['ve_discounta'];
  $ve_apayable    = $discharge['ve_apayable'];
  $ve_round       = $discharge['ve_round'];
}

   }?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
 oTable = $('#category_table').dataTable({
});
});
</script>


<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h4 style="border-bottom: thin dotted #000; font-family: Arial Black, Georgia, Serif; padding-bottom:5px">
          <center class="font_reduce"><b><?= strtoupper($company_name) ?></b></center>
          <small class="font_reduce2"><center><b><?= "[ ".strtoupper($company_tags)." ]" ?></b></center></small>
           <center class="pull-center font_reduce2"><?= strtoupper($company_address).", ".strtoupper($company_street)." ,".strtoupper($company_city)." - ".$company_zip.", PH. : ".$company_phone  ?></center>
           <center class="pull-center font_reduce2"><?= "GST : ".strtoupper($company_gst_no).", Drug Lic No : ".strtoupper($company_drug_lic) ?> </center> </b>
                    </h4>

 <center class="font_reduce2"><b>DISCHARGE BILL </center>

  <div class="box-tools">
              <a title="Back" class="dontprint btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."discharge"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>
    </div> </div>
    <!-- info row -->

     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">
           <br>

           <table class="font_reduce1" width="100%">
            <tr>
            <th>Voucher No.    : <?= $ve_vno?></th>
            <th>Date           : <?= date("d-m-Y",strtotime($ve_date))?></th>
            <th>MRD            : <?= $ve_mrd?></th>
            <th>Phone No.      : <?= $p_phone?> </th>
          </tr>
          <tr><td><br></td></tr>
          <tr>
            <th>IP No.         : <?= $ve_customer?></th>
            <th>Patient : <?= $p_name?> </th>
            <th>Doctor         : <?= $u_name?></th>

            <th>Department     : <?= $dp_department?></th>
          </tr>
           </table>



    </div>
      <!-- /.col -->

    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12">
         <br>
       <table class="font_reduce1 table table-bordered" id="item_table" width="100%">
                    <thead>
                      <tr>
                        <th>Sl No.</th>
                        <th colspan="8">Particulars</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                  <tbody> 

                    <?php $slno = 1; $grand_total=0; if($rooms) { $slno1 =1; ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Rooms</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th>Rooms</th><th>Admission Date</th><th>Discharged Date</th><th>Total Days</th><th>Rent</th><th>Nursing Charge</th><th>House Keeping Charge</th><th>Credit Amount</th></tr>
                    <?php foreach ($rooms as $room) {
                      $diff       = date_create($room['rs_adm'])->diff(date_create($room['rs_dis']));
                      // $date_diff  =  $diff->format("%a days\n%h hours\n%i minutes\n%s seconds\n");
                      $days       = $diff->format("%a");
                      $hours      = $diff->format("%h");
                      $minutes    = $diff->format("%i");

                      if($hours>6)
                      {
                        $date_diff = $days + 1;
                      }
                      else if($hours == 6 && $minutes > 0 )
                      {
                        $date_diff = $days + 1;
                      }
                      else  if($hours <= 6 && $hours > 0 )
                      {
                        $date_diff = $days + .5;
                      }
                      else
                      {
                        $date_diff = $days;
                      }
                      $total = ($room['rs_rt'] + $room['rs_nc'] + $room['rs_hc']) * $room['rs_days'];
                      $grand_total = $grand_total + $total;
                      ?> <tr><td></td><td><?=$slno1?></td><td><?=$room['rm_no']?></td><td><?=$room['rs_adm']?></td><td><?=$room['rs_dis']?></td><td><?=$room['rs_days']?></td><td><?=$room['rs_rt']?></td><td><?=$room['rs_nc']?></td><td><?=$room['rs_hc']?></td><td><?=$total?></td></tr> <?php
                    $slno1++;  } $slno++; ?>

                    <?php } ?>


                    <?php if($voucher_bills!=0) { $slno3 =1; ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Lab</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Credit Amount</th></tr>
                    <?php foreach ($voucher_bills as $voucher_bill) {
                      $grand_total = $grand_total + $voucher_bill['ve_apayable'];
                      ?> <tr><td></td><td><?=$slno3?></td><td colspan="3"><?=$voucher_bill['ve_vno']?></td><td colspan="4"><?=$voucher_bill['ve_date']?></td><td><?=$voucher_bill['ve_apayable']?></td></tr>
                       <?php
                    $slno3++;  } $slno++;  ?>

                    <?php } ?>

                    <?php if($scanning_bills!=0) { $slno7 =1; ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Scanning</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Credit Amount</th></tr>
                    <?php foreach ($scanning_bills as $scanning_bill) {
                      $grand_total = $grand_total + $scanning_bill['ve_apayable'];
                      ?> <tr><td></td><td><?=$slno7?></td><td colspan="3"><?=$scanning_bill['ve_vno']?></td><td colspan="4"><?=$scanning_bill['ve_date']?></td><td><?=$scanning_bill['ve_apayable']?></td></tr>
                       <?php
                    $slno7++;  } $slno++;  ?>

                    <?php } ?>

                    <?php if($xray_bills!=0) { $slno8 =1; ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Xray</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Credit Amount</th></tr>
                    <?php foreach ($xray_bills as $xray_bill) {
                      $grand_total = $grand_total + $xray_bill['ve_apayable'];
                      ?> <tr><td></td><td><?=$slno8?></td><td colspan="3"><?=$xray_bill['ve_vno']?></td><td colspan="4"><?=$xray_bill['ve_date']?></td><td><?=$xray_bill['ve_apayable']?></td></tr>
                       <?php
                    $slno8++;  } $slno++;  ?>

                    <?php } ?>

                    <?php if($pharmacy_bills!=0) { $slno2 =1; ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Pharmacy</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Credit Amount</th></tr>
                    <?php foreach ($pharmacy_bills as $pharmacy_bill) {
                      $grand_total = $grand_total + $pharmacy_bill['ve_apayable'];
                      ?> <tr><td></td><td><?=$slno2?></td><td colspan="3"><?=$pharmacy_bill['ve_vno']?></td><td colspan="4"><?=$pharmacy_bill['ve_date']?></td><td><?=$pharmacy_bill['ve_apayable']?></td></tr>
                       <?php
                    $slno2++;  } $slno++;  ?>

                    <?php } ?>

                    <?php if($pharmacy_returns!=0) { $slno4 =1; ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Medicine Return</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Debit Amount</th></tr>
                    <?php foreach ($pharmacy_returns as $pharmacy_return) {
                      $grand_total = $grand_total - $pharmacy_return['ve_apayable'];
                      ?> <tr><td></td><td><?=$slno4?></td><td colspan="3"><?=$pharmacy_return['ve_vno']?></td><td colspan="4"><?=$pharmacy_return['ve_date']?></td><td><?=$pharmacy_return['ve_apayable']?></td></tr>
                       <?php
                    $slno4++;  } $slno++;  ?>

                    <?php } ?>

                    <?php if($advances!=0) { $total_advance=0; $slno5 =1;  ?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Advance Payment</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th colspan="3">Bill Date</th><th colspan="4">Bill No.</th><th>Debit Amount</th></tr>
                    <?php foreach ($advances as $advance) {
                      $total_advance = $total_advance + $advance['ve_apayable'];

                      ?> <tr><td></td><td><?=$slno5?></td><td colspan="3"><?=$advance['ve_vno']?></td><td colspan="4"><?=$advance['ve_date']?></td><td><?=$advance['ve_apayable']?></td></tr>
                       <?php
                    $slno5++;  } $slno++;  ?>

                    <?php } ?>

                    <?php if($additionals!=0) {  $slno6 =1;?>
                    <tr><th><?=$slno?></th>
                      <th colspan="9">Others</th></tr> 
                      <tr><th></th><th>Sl.no.</th><th colspan="4">Particulars</th><th colspan="2">Price</th><th colspan="1">Qty</th><th>Credit Amount</th></tr>
                    <?php foreach ($additionals as $additional) {
                      $grand_total = $grand_total + $additional['ved_gtotal'];
                      ?> <tr><td></td><td><?=$slno6?></td><td colspan="4"><?=$additional['ved_item']?></td>
                      <td colspan="2"><?=$additional['ved_price']?></td><td colspan="1"><?=$additional['ved_qty']?></td><td><?=$additional['ved_gtotal']?></td></tr>
                       <?php
                    $slno6++;  } $slno++;  ?>

                    <?php } ?>



                  </tbody>
                  </table> 
                  <?php if($advances==0)  { $total_advance=0;  }

                  $total_amount = $grand_total - $total_advance;
                  $advance_amt  = $total_amount * $ve_discount/100;
                  $amt_payable  = $total_amount - $advance_amt;
                  ?>

                  <table width="100%" class="font_reduce1" style="border-bottom: thin dashed #000; margin-bottom:10px">
                    <tr><th class="u">Total</th><th class="u">Advance Paid</th><th class="u">Discount</th><th class="u">Round Off</th><th class="u font_reduce3">Grand Total</th></tr>
                    <tr>
                      <th>Rs. <label><?= $grand_total ?></label></th>
                      <th>Rs. <label><?= $total_advance ?></label></th>
                      <th>Rs. <label><?= $advance_amt?></label></th>
                      <th>Rs. <label><?= $ve_round ?></label></th>
                      <th class="font_reduce3">Rs. <label><?= $amt_payable ?></label></th>
                    </tr>
                    </table>

        <div class="col-xs-12 font_reduce2" style="text-align:right">
          Authorized Signature  with Seal<br><br><br><br><br><br><br>
      </div>        </div>
      <!-- /.col -->
    </div>

     </div>
       
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
 
 <style type="text/css" media="print">
.dontprint
{ display: none; }
</style>
</body>
</html>