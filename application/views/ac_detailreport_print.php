
<html>
<head>
<link rel="stylesheet" href='<?= base_url() ?>asset/plugins/jvectormap/jquery-jvectormap-1.2.2.css' type="text/css"/>
  <link media="all" type="text/css" rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.css">
   <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/font-awesome-4.7.0/css/font-awesome.min.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.css' type="text/css"/>
  <link rel="stylesheet" href='<?= base_url() ?>asset/ionicons-2.0.1/css/ionicons.min.css' type="text/css"/>
   <link rel="stylesheet" href="<?= base_url() ?>asset/css/skins/_all-skins.min.css"> 
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datatables/jquery.dataTables.min.css">
 
  <!-- <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/datepicker/datepicker3.css"> -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- <link rel="stylesheet" href="<?= base_url() ?>asset/css/AdminLTE.min.css" type="text/css"/> -->
   <link rel="stylesheet" href="<?= base_url() ?>asset/bootstrap/css/AdminLTE.min.css" type="text/css"/>
   <script  src="<?= base_url() ?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
</head>
 <body onload="window.print();" style="margin-top:0%;display:block;height:100%;width: 100%;" >
  <?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>
  <script type="text/javascript" charset="utf-8">
$(document).ready(function() {
  $('.tbother').hide();
  $('.tbbal').hide();

var r_type= "<?= $r_type ?>";
if(r_type==1 || r_type==2 || r_type==3 || r_type==4)
{
  $('.tbbal').hide();
  $('.tbother').show();
}

else if(r_type==0)
{
  $('.tbother').hide();
  $('.tbbal').show();
}

});
</script>
  <?php foreach($company as $company) {
    $company_name     = $company['company'];
    $company_lic      = $company['licence_no'];
    $company_address  = $company['address'];
    $company_street   = $company['street'];
    $company_city     = $company['city'];
    $company_state    = $company['state'];
    $company_country  = $company['country'];
    $company_zip      = $company['zip'];
    $company_phone    = $company['phone'];
    $company_email    = $company['email'];
    $company_fax      = $company['fax'];
    $company_website  = $company['website'];

   }?>


  

<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i> <?= $company_name ?>
         <!--  <small class="pull-right">Invoice Date:</small> -->
        </h2>
      </div>
      <div class="dontprint" align="right"> 
        <a title="Back" class="btn btn-sm btn-success" href="<?php echo $this->config->item('admin_url')."accountreport/acdetail"; ?>"><i class="fa fa-mail-reply-all"></i> Back</a>
        </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        <address>
          <strong><?= $company_name ?></strong><br>
          <strong>Lic No.:<?= $company_lic ?></strong><br>
          <?= $company_address.", ".$company_street  ?><br>
          <?= $company_city.", ".$company_state.", ".$company_country." - ".$company_zip ?> <br>
          <b>Phone:</b> <?= $company_phone ?>, Fax:  <?= $company_fax ?><br>
          <b>Email:</b> <?= $company_email ?><br>
          <b>Website:</b> <?= $company_website ?>
        </address>
        </div>
      <!-- /.col -->
      
    </div>
   <div class="row">
              <div class="col-md-12">
               <div class="box-header  ">
                   <center><h3 class="box-title"><b><?php if($r_type==1) { echo "Purchase"; } else if($r_type==2) { echo "Sales"; } else if($r_type==0) { echo "Balance Sheet"; } else if($r_type==3) { echo "Purchase Return"; } else if($r_type==4) { echo "Sale Return"; }   ?></b> Report from <b><?=date("d-m-Y",strtotime($fdate)) ?></b>&nbsp;<b></b> to <b><?=date("d-m-Y",strtotime($tdate)) ?></b> of <b><?= $products ?></b></h3></center>
                </div>
                <div class="col-md-12"><hr></div>
                <center>     
                
              </center>

            <div class="row">
              
              <div class="col-md-12"><br>
                  <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-12 tbother">
                      <table id="patient_table" class="table table-condensed dataTable no-footer">
                       <thead><b>
                        <tr>
                          <th>Date</th>
                          <th>Product</th>
                          <th>Batch</th>
                          <th>Unit Price</th>
                          <th>Qty</th>
                          <th>Amount</th>
                          <th>SGST</th>
                          <th>SGST</th>
                          <th>Sub Total</th>
                        </tr>
                        </b>
                      </thead>
                      <tbody>
                      <?php if($r_type != "0") { echo $output; } ?>
                      <?php
                  $ve_discounta=0;
                  $ve_sgst=0;
                  $ve_cgst=0;
                  $ve_gtotal=0;                
                  $ve_apayable=0;
                  $ve_apaid=0;
                  $ve_balance=0;
                  $ve_round=0;
                  if (is_array($output1) || is_object($output1) )
                  {
                  foreach($output1 as $tsum)
                  {
                   

                    $ve_discounta = $ve_discounta   + $tsum['ve_discounta'];
                    $ve_gtotal    = $ve_gtotal      + $tsum['ve_gtotal'];
                    $ve_apayable  = $ve_apayable    + $tsum['ve_apayable'];
                    $ve_apaid     = $ve_apaid       + $tsum['ve_apaid'];
                    $ve_balance   = $ve_balance     + $tsum['ve_balance'];
                    $ve_round     = $ve_round       + $tsum['ve_round'];
                  } }
              ?>
               <?php if($product=="0" && !empty($output1)){ ?>
                <tr<?php if($r_type==0 || $r_type== "") { ?> style="display:none" <?php } ?>><th width="85%">Overall Discount</th><th><?="Rs. ".$ve_discounta ?></th></tr>
                <tr><th width="85%">Total Amount</th><th><?="Rs. ".$ve_gtotal ?></th></tr>
                <tr><th width="85%">Total Round Off</th><th><?="Rs. ".$ve_round ?></th></tr>
                <tr><th width="85%">Total Amount Payable</th><th><?php echo "Rs. "; echo $ve_apayable; ?></th></tr>
                <tr><th width="85%">Total Amount Paid</th><th><?="Rs. ".$ve_apaid ?></th></tr>
                <tr><th width="85%">Balance Amount</th><th><?="Rs. ".$ve_balance ?></th></tr>
                <?php } ?>
              </tbody>
             </table> 
          </div>

              <?php
                       if($r_type==0) {
                      foreach ($output as $output):  
                        ?>
             <div class="col-md-12">
             <table class="table table-condensed dataTable no-footer tbbal">
              <thead>
              <tr>
                <th colspan=2 style="text-align: center;"><?= date('d-m-Y', strtotime($output['psdate'])) ?></th></tr>
                <tr><td width="50%" style="text-align: center;"><b>Opening Balance : </b> <?= "Rs. ".$output['openingBalance'] ?></td><td style="text-align: center;"><b>Closing Balance : </b> <?= "Rs. ".$output['closingBalance'] ?></td></tr>
            </thead>
              <tr>
                <td>
                  <div class="col-md-12">
                  <table class="table table-condensed dataTable no-footer">
                    <tr><th>Income</th><th>Amount</th></tr>
                    <?php if(!empty($output['sales_sum'])) { ?>
                         <?php foreach ($output['sales_sum'] as $output2) {  ?>
                          <tr>
                         <td><?= $output2['ved_item']." - ".$output2['ved_batch']." - [Sale]" ?> </td>
                         <td><?= "Rs.".$output2['samount'] ?> </td>
                       </tr>
                       <?php } } ?>
                        <?php if(!empty($output['preturn_sum'])) { ?>
                         <?php foreach ($output['preturn_sum'] as $output3) {  ?>
                          <tr>
                         <td><?= $output3['ved_item']." - ".$output3['ved_batch']." - [Purchase Return]" ?> </td>
                         <td><?= "Rs.".$output3['pramount'] ?> </td>
                       </tr>
                       <?php } } ?>
                  </table>
                </div>
              </td>
              <td>
                  <div class="col-md-12">
                  <table class="table table-condensed dataTable no-footer">
                    <tr><th>Expence</th><th>Amount</th></tr>
                    <?php if(!empty($output['purchase_sum'])) { ?>
                         <?php foreach ($output['purchase_sum'] as $output4) {  ?>
                          <tr>
                         <td><?= $output4['ved_item']." - ".$output4['ved_batch']." - [Purchase]" ?> </td>
                         <td><?= "Rs.".$output4['pamount'] ?> </td>
                       </tr>
                       <?php } } ?>
                         <?php if(!empty($output['expense'])) { ?>
                         <?php foreach ($output['expense'] as $output5) {  ?>
                          <tr>
                         <td><?= $output5['category'] ?> </td>
                         <td><?= "Rs.".$output5['amount'] ?> </td>
                       </tr>
                       <?php } } ?>
                       <?php if(!empty($output['sreturn_sum'])) { ?>
                         <?php foreach ($output['sreturn_sum'] as $output6) {  ?>
                          <tr>
                         <td><?= $output6['ved_item']." - ".$output6['ved_batch']." - [Sale Return]" ?> </td>
                         <td><?= "Rs.".$output6['sramount'] ?> </td>
                       </tr>
                       <?php } } ?>
                  </table>
                </div>
                </td>
              </tr>
              <tr><td><br></td></tr>
             </table>
           </div>
           <?php endforeach; } ?>   
             
      
        </div>
      </div>
    </div>

  </div>
</div>

</div>
</div>

    <div class="row">
      <!-- accepted payments column -->
       <div class="col-xs-8">
       </div>
      
      

     </div>
       
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
 </body>
</html>
 <style type="text/css" media="print">
.dontprint
{ display: none; }
</style>