

<?php 
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300);
 ?>



 <!-- Amount Paid -->
  <script>
  $(document).ready(function() {
  $('#balance_paid').change(function() {

    var amount_paid     =  $('#amount_paid').val();
    var amountpaid      =  $('#amountpaid').val();
    var amount_balance  =  $('#amount_balance').val();
    var amountbalance   =  $('#amountbalance').val();
    var balancepaid     =  $('#balance_paid').val();

    if(balancepaid>=0) {
      amount_paid     =  parseFloat(amountpaid);
      amount_balance  =  parseFloat(amountbalance);
    }

    var balance_pay       = parseFloat(amount_balance) - parseFloat(balancepaid);
    var amount_paid       = parseFloat(amount_paid) + parseFloat(balancepaid);


    $('#amount_paid').val(amount_paid);
    $('#amount_balance').val(balance_pay);
    $('#amount_paid1').html(amount_paid);
    $('#amount_balance1').html(balance_pay);


    if(balance_pay<0)
    {
      var Printid   =  $('#Printid').val();
      alert("Invalid Amount");
      window.location="<?= base_url() ?>preturn/return_PView/"+Printid;
    }


  });
  });
  </script>

<?php
foreach($preturns as $preturn)
{
  $ve_id          = $preturn['ve_id'];
  $ve_bill_no     = $preturn['ve_bill_no'];
  $ve_vno         = $preturn['ve_vno'];
  $ve_date        = $preturn['ve_date'];
  $sp_id          = $preturn['sp_id'];
  $sp_vendor      = $preturn['sp_vendor'];
  $sp_phone       = $preturn['sp_phone'];
  $ve_amount      = $preturn['ve_amount'];
  $ve_sgst        = $preturn['ve_sgst'];
  $ve_cgst        = $preturn['ve_cgst'];
  $ve_gtotal      = $preturn['ve_gtotal']; 
  $ve_apayable    = $preturn['ve_apayable']; 
  $ve_round       = $preturn['ve_round']; 
  $ve_apaid       = $preturn['ve_apaid']; 
  $ve_balance     = $preturn['ve_balance']; 
  $ve_type        = $preturn['ve_user']; 
}

?>


<script type="text/javascript" charset="utf-8">
$(document).ready(function() {

 oTable = $('#category_table').dataTable({
});
});
</script>

<div class="content-wrapper">
  <section class="content-header">
    <h1>
     Purchase Return
      <small>View Purchase Return</small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo $this->config->item('admin_url')."dashboard"; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->config->item('admin_url')."preturn/return_Plist"; ?>">Sales Return</a></li>
      <li class="active"><?= $page_title?></li>
    </ol>
    <?php if($this->session->flashdata('Success')){ ?>
           <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('Success'); ?>
        </div>
        <?php }else if($this->session->flashdata('Error')){  ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('Error'); ?>
        </div>
        <?php } ?>
  </section>
  <section class="content">
    <div class="box box-primary">
      
      <div class="box-header">
        <h3 class="box-title">
         <i class="fa fa-th"></i> <?= $page_title?>
       </h3>
        <div class="box-tools">
              <a title="Back" class="btn btn-sm btn-success pull-right" href="<?php echo $this->config->item('admin_url')."preturn/return_Plist"; ?>">
                <i class="fa fa-mail-reply-all"></i> Back</a>
            </div>
     </div>


      <form id="post_form">
     <div class="box-body">
      <div class="row">
        <div class="col-md-12 ">

        <div class="form-group col-md-2">
            <label  class="control-label">Return No. : </label> <?=  $ve_vno ?><br>
          </div>
          <div class="form-group col-md-7">
            <label  class="control-label">Return Date : </label> <?= date("d-m-Y",strtotime($ve_date)) ?>
            <br><input type="hidden" name="purchase_date" value="<?= $ve_date ?>">
          </div>
          <div class="form-group col-md-3">
            <label  class="control-label">Puchase Bill No. : </label> <?= $ve_bill_no ?><br>
          </div>

          <div class="form-group col-md-2">
            <label  class="control-label">Vendor ID : </label> <?= $sp_id ?><br>      
          </div>
          <div class="form-group col-md-3">
            <label  class="control-label">Vendor : </label> <?= $sp_vendor ?><br>
          </div>

          <div class="form-group col-md-4">
            <label  class="control-label">Phone No. : </label> <?= $sp_phone ?> <br>            
          </div>
          <div class="form-group col-md-2">    
          <label class="control-label">User Name : </label> <?= $ve_type ?>
          </div>
            </div>
          </div>

          <div class="row">
          <div class="col-md-12">
             
            <br>
            <div class="col-md-12">
              <div class="panel panel-default">   
                <div class="panel-body">
                  <div class="table-responsive">
                  <table class="table table-condensed dataTable no-footer gridView" id="item_table">
                    <thead>
                      <tr>
                        <th>Sl. No </th>
                        <th>Product</th>
                        <th>Unit Price</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                        <th>Batch</th>
                        <th>Expiry</th>
                        <th>Sub Total</th>
                        <th>SGST</th>
                        <th>CGST</th>
                        <th>Sub Total(Inc. GST)</th>
                      </tr>
                    </thead>
                  <tbody>
                  <?php
                  $slno=1;
                  foreach($preturns as $preturn)
                  { 
                    $sgstp = ($preturn['ved_sgsta']*100)/$preturn['ved_total'];
                    $cgstp = ($preturn['ved_cgsta']*100)/$preturn['ved_total'];
                    ?> 
                    <tr>
                      <td align="center"><?= $slno++  ?></td>
                      <td><a href="<?= $this->config->item('admin_url')."product/view/".$preturn['ved_itemid'] ?>"><?=$preturn['ved_item'] ?></a></td>
                      <td><?=$preturn['ved_price'] ?></td>
                      <td><?=$preturn['ved_qty']." ".$preturn['ved_unit'] ?></td>
                      <td><?=$preturn['ved_batch'] ?></td>
                      <td><?=$preturn['ved_expiry'] ?></td>
                      <td><?=$preturn['ved_total'] ?></td>
                      <td><?=$preturn['ved_sgsta']." (".$preturn['ved_sgstp']." %)" ?></td>
                      <td><?=$preturn['ved_cgsta']." (".$preturn['ved_cgstp']." %)" ?></td>
                      <td><?=$preturn['ved_gtotal'] ?></td>
                    </tr>
                    <?php } ?>

                  </tbody>
                  </table> 
                
                  <table class="table table-bordered">
                    <tr><th width="80%">Total</th>
                      <th>Rs. <label><?=$ve_amount?></label></th></tr>
                      <tr><th width="80%">Total CGST</th>
                      <th>Rs. <label><?=$ve_cgst?></label></th></tr>
                      <tr><th width="80%">Total SGST</th>
                      <th>Rs. <label><?=$ve_sgst?></label></th></tr>
                      <tr><th width="80%">Total (Inc. GST)</th>
                      <th>Rs. <label><?=$ve_gtotal?></label></th></tr>

                      <tr><th width="80%">Amount Payable</th>
                      <th><input type="hidden" name="grandtotal" readonly id="grand_total" value="<?= $ve_apayable ?>">Rs. <label id="grandtotal1"><?= $ve_apayable ?></label></th></tr>

                      <tr><th width="80%">Round off Value</th>
                      <th>Rs. <label><?= $ve_round ?></label></th></tr>
                      <input type="hidden" class="form-control" id="Printid" value="<?= $ve_id ?>">

                     <tr><th width="80%">Amount Paid</th>
                      <input type="hidden" class="form-control" name="ve_apaid" readonly id="amount_paid" value="<?= $ve_apaid ?>">
                      <input type="hidden" class="form-control" readonly id="amountpaid" value="<?= $ve_apaid ?>">
                      <th>Rs.  <label><?= $ve_apaid ?></label></th></tr>
                      <?php if($ve_balance>0) { ?>
                    <tr><th width="80%">Balance to Pay</th><th>
                      <input type="hidden" class="form-control" name="amount_balance" readonly id="amount_balance" value="<?= $ve_balance ?>">
                      <input type="hidden" class="form-control" readonly id="amountbalance" value="<?= $ve_balance ?>">

                      Rs. <label id="amount_balance1"><?= $ve_balance ?></label></th></tr>
                      
                  <tr><th width="80%">Balance Payment</th>
                      <th>
                          <input <?php if($ve_balance==0) { ?> type="hidden" <?php } else { ?>type="input" <?php } ?> class="form-control" name="balance_paid" id="balance_paid" autofocus="autofocus"/>
                        <label><?php if($ve_balance==0) { echo $ve_apaid; } ?></label></th></tr>
                      <?php } ?>
                    </table>
   
<br><?php if($ve_balance!=0) { ?><a href="#" class="btn btn-success" onclick="save();"><i class="fa fa-floppy-o"></i> Save</a> <?php } ?>
<!-- <input type="input" id="Printid"> -->
 <a href="#" class="btn btn-warning" onclick="print();"><i class="fa fa-print"></i> Print</a>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
           

        </div>
      </div>
      <div class="box-footer">
        <div class="row">
          <div class="col-md-12">
            
          </div>

        </div>
      </div>
     
    </form>
   
</section>

</div><!-- /.right-side -->

<script>
function save(){

      var postData = $("#post_form").serializeArray();
      var amount_paid    =  $('#balance_paid').val();
      if(amount_paid=="")
          {
            alert("Enter the amount paid");
          }
          else
          {
      var Printid         =  $('#Printid').val();
      var formURL  = "<?= base_url() ?>preturn/preturnVUpdate/?pUpid="+Printid;
      $.ajax(
      {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
           window.location = "<?php echo base_url();?>index.php/preturn/return_PView/"+Printid;

        }

      });
    }
}

function print(){

          var Printid         =  $('#Printid').val();
          window.location = "<?php echo base_url();?>index.php/preturn/getPprint?sPrintid="+Printid;

}
</script>


